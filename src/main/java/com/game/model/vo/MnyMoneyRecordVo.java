package com.game.model.vo;

import java.util.Date;

public class MnyMoneyRecordVo {
	
	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 记录类型
	 */
	private Long type;

	private Long stationId;

	private Date begin;
	private Date end;

	private String parents;

	private Long accountType;
	
	// 是否查询自身
	private Boolean searchSelf;
	private Long selfId;
	// 订单号
	private String orderId;
	
	//是否显示人工加扣款
	private boolean showHandMoney;

	//是否显示回滚
	private boolean showRollBack;

	//是否显示改单记录
	private boolean showChangeOrder;
	
	private String remark;
	
	private Long reportType;

	//操作员
	private String operatorName;

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public MnyMoneyRecordVo() {
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public boolean isShowHandMoney() {
		return showHandMoney;
	}

	public void setShowHandMoney(boolean showHandMoney) {
		this.showHandMoney = showHandMoney;
	}

	public boolean isShowRollBack() {
		return showRollBack;
	}

	public void setShowRollBack(boolean showRollBack) {
		this.showRollBack = showRollBack;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public boolean isShowChangeOrder() {
		return showChangeOrder;
	}

	public void setShowChangeOrder(boolean showChangeOrder) {
		this.showChangeOrder = showChangeOrder;
	}
}
