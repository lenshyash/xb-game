package com.game.model.vo;

import java.math.BigDecimal;

import com.game.model.dictionary.ScoreRecordType;

public class MnyScoreVo {

	/**
	 * 金额
	 */
	private BigDecimal score;

	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 货币类型
	 */
	private ScoreRecordType scoreRecordType;

	/**
	 * 备注
	 */
	private String remark;

	private Long stationId;

	public MnyScoreVo() {
	}

	public ScoreRecordType getScoreRecordType() {
		return scoreRecordType;
	}

	public void setScoreRecordType(ScoreRecordType scoreRecordType) {
		this.scoreRecordType = scoreRecordType;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
}
