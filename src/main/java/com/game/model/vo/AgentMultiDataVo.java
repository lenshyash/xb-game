package com.game.model.vo;

import java.math.BigDecimal;

public class AgentMultiDataVo {
	private String name;
	private Integer type;
	private BigDecimal max;
	private BigDecimal min;
	private BigDecimal current;
	private String key;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getMax() {
		return max;
	}
	public BigDecimal getMin() {
		return min;
	}
	public void setMin(BigDecimal min) {
		this.min = min;
	}
	public BigDecimal getCurrent() {
		return current;
	}
	public void setCurrent(BigDecimal current) {
		this.current = current;
	}
	public void setMax(BigDecimal max) {
		this.max = max;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
}
