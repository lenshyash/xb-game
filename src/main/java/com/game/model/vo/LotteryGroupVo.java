package com.game.model.vo;

import java.util.List;

import com.game.controller.phone.PlayItem;

public class LotteryGroupVo {

	private Integer type;
	private String name;
	private List<PlayItem> PlayItems;
	
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<PlayItem> getPlayItems() {
		return PlayItems;
	}
	public void setPlayItems(List<PlayItem> playItems) {
		PlayItems = playItems;
	}
	
}
