package com.game.model.vo;

import java.math.BigDecimal;
import java.util.Date;

public class MnyComRecordVo {

	// 禁用
	public static long CHECKBETNUM_DISABLED = 1;
	// 启用
	public static long CHECKBETNUM_ENABLED = 2;

	/**
	 * ID
	 */
	private Long id;
	/**
	 * 会员ID
	 */
	private Long accountId;

	/**
	 * 会员账号
	 */
	private String account;

	/**
	 * 记录类型
	 */
	private Long type;
	/**
	 * 记录类型小类
	 */
	private String paySmallName;

	private Long stationId;
	private String agentName;

	private Date begin;
	private Date end;
	
	//报表类型(默认1普通， 2引导)
	private Long reportType;
	private Long accountType;
	
	private Long lockFlag;
	private Long status;
	private BigDecimal fee;
	private BigDecimal money;
	private String opDesc;
	private String remark;
	private String orderNo;
	private Date depositDate;
	private Date modifyDatetime;
	/**
	 * 是否加入出款需要验证的打码量（默认加入)
	 */
	private Long checkBetNum;

	private BigDecimal betNumMultiple;
	private Boolean searchSelf;
	private Long selfId;
	private Long searchType;
	private String parents;
	
	private Boolean firstDeposit;
	private BigDecimal minMoney;
	private BigDecimal maxMoney;
	private BigDecimal giftMoney;
	private String operator;
	/**
	 * 处理方式 （1、手动2、系统）
	 */
	private Long handlerType;
	private Long depositCount;
	/**
	 * 检查异步回调金额标识  
	 */
	private Boolean checkNotifyMoneyFlag;
	private Long comTimes;
	/**
	 * 数字货币使用
	 */
	private BigDecimal rate;
	private BigDecimal virtualCurrencyAmount;
	
	public MnyComRecordVo() {
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Boolean getSearchSelf() {
		return searchSelf;
	}

	public void setSearchSelf(Boolean searchSelf) {
		this.searchSelf = searchSelf;
	}

	public Long getSelfId() {
		return selfId;
	}

	public void setSelfId(Long selfId) {
		this.selfId = selfId;
	}

	public Long getSearchType() {
		return searchType;
	}

	public void setSearchType(Long searchType) {
		this.searchType = searchType;
	}

	public String getPaySmallName() {
		return paySmallName;
	}

	public void setPaySmallName(String paySmallName) {
		this.paySmallName = paySmallName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Long getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(Long lockFlag) {
		this.lockFlag = lockFlag;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getOpDesc() {
		return opDesc;
	}

	public void setOpDesc(String opDesc) {
		this.opDesc = opDesc;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public Long getCheckBetNum() {
		return checkBetNum;
	}

	public void setCheckBetNum(Long checkBetNum) {
		this.checkBetNum = checkBetNum;
	}

	public Long getHandlerType() {
		return handlerType;
	}

	public void setHandlerType(Long handlerType) {
		this.handlerType = handlerType;
	}

	public BigDecimal getBetNumMultiple() {
		return betNumMultiple;
	}

	public void setBetNumMultiple(BigDecimal betNumMultiple) {
		this.betNumMultiple = betNumMultiple;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Boolean getFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(Boolean firstDeposit) {
		this.firstDeposit = firstDeposit;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public BigDecimal getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(BigDecimal minMoney) {
		this.minMoney = minMoney;
	}

	public BigDecimal getMaxMoney() {
		return maxMoney;
	}

	public void setMaxMoney(BigDecimal maxMoney) {
		this.maxMoney = maxMoney;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public BigDecimal getGiftMoney() {
		return giftMoney;
	}

	public void setGiftMoney(BigDecimal giftMoney) {
		this.giftMoney = giftMoney;
	}

	public Long getDepositCount() {
		return depositCount;
	}

	public void setDepositCount(Long depositCount) {
		this.depositCount = depositCount;
	}

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Boolean getCheckNotifyMoneyFlag() {
		return checkNotifyMoneyFlag;
	}

	public void setCheckNotifyMoneyFlag(Boolean checkNotifyMoneyFlag) {
		this.checkNotifyMoneyFlag = checkNotifyMoneyFlag;
	}

	public Long getComTimes() {
		return comTimes;
	}

	public void setComTimes(Long comTimes) {
		this.comTimes = comTimes;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getVirtualCurrencyAmount() {
		return virtualCurrencyAmount;
	}

	public void setVirtualCurrencyAmount(BigDecimal virtualCurrencyAmount) {
		this.virtualCurrencyAmount = virtualCurrencyAmount;
	}
}
