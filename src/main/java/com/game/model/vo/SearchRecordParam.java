package com.game.model.vo;

import java.util.Date;

/**
 * 查询注单 传递条件对象
 * 
 * @author admin
 *
 */
public class SearchRecordParam {
	
	public static long DATETYPE_MATCHDATE = 1;//结算时间
	public static long DATETYPE_BETTINGDATE = 2;//投注时间

	private Long memberId;//会员id

	private Date begin;//开始时间

	private Date end;//结束时间

	private Long sportType;//类型
	
	private Long balanceStatus;//结算状态
	
	private Long resultStatus;//输赢状态
	
	private String memberAccount;//会员账号
	
	private String homeTeam; //主队
	
	private String guestTeam; //客队
	
	private String bettingCode;//单号
	
	private Long dateType; //查询时间类型
	
	private Long bettingStatus;//提交状态
	
	private String agentName;//代理账号
	
	private String children; //

	private String typeNames;//投注赛事类型
	
	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getGuestTeam() {
		return guestTeam;
	}

	public void setGuestTeam(String guestTeam) {
		this.guestTeam = guestTeam;
	}

	public String getBettingCode() {
		return bettingCode;
	}

	public void setBettingCode(String bettingCode) {
		this.bettingCode = bettingCode;
	}

	public String getMemberAccount() {
		return memberAccount;
	}

	public void setMemberAccount(String memberAccount) {
		this.memberAccount = memberAccount;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Long getSportType() {
		return sportType;
	}

	public void setSportType(Long sportType) {
		this.sportType = sportType;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
	public Long getBalanceStatus() {
		return balanceStatus;
	}

	public void setBalanceStatus(Long balanceStatus) {
		this.balanceStatus = balanceStatus;
	}

	public Long getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(Long resultStatus) {
		this.resultStatus = resultStatus;
	}

	public Long getDateType() {
		return dateType;
	}

	public void setDateType(Long dateType) {
		this.dateType = dateType;
	}

	public Long getBettingStatus() {
		return bettingStatus;
	}

	public void setBettingStatus(Long bettingStatus) {
		this.bettingStatus = bettingStatus;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getChildren() {
		return children;
	}

	public void setChildren(String children) {
		this.children = children;
	}

	public String getTypeNames() {
		return typeNames;
	}

	public void setTypeNames(String typeNames) {
		this.typeNames = typeNames;
	}
}
