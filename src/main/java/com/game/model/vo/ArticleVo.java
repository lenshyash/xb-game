package com.game.model.vo;

import java.util.List;

public class ArticleVo {

	private Long stationId;

	private List<Long> codes;

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public List<Long> getCodes() {
		return codes;
	}

	public void setCodes(List<Long> codes) {
		this.codes = codes;
	}

}
