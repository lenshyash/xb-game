package com.game.model.vo;

import java.math.BigDecimal;

public class ReportVo {

	/**
	 * 在线存款总计
	 */
	private BigDecimal depositTotal;
	/**
	 * 在线提款总计
	 */
	private BigDecimal withdrawTotal;
	/**
	 * 手动存款总计
	 */
	private BigDecimal manualDepositTotal;
	/**
	 * 手动存款总计
	 */
	private BigDecimal manualWithdrawTotal;
	/**
	 * 体育总计
	 */
	private BigDecimal sportTotal;
	/**
	 * 彩票总计
	 */
	private BigDecimal lotteryTotal;
	/**
	 * 六合彩总计
	 */
	private BigDecimal markSixTotal;
	/**
	 * 真人总计
	 */
	private BigDecimal realTotal;
	/**
	 * 电子
	 */
	private BigDecimal dianZiTotal;
	private BigDecimal dianZiAward;
	private BigDecimal dianZiBunko;
	/**
	 * 反水总计
	 */
	private BigDecimal rebateTotal;

	/**
	 * 代理返点总计
	 */
	private BigDecimal rebateAgentTotal;
	/**
	 * 存款赠送总计
	 */
	private BigDecimal giveTotal;
	/**
	 * 扣除彩金总计
	 */
	private BigDecimal withdrawGiveTotal;
	/**
	 * 注册赠送总计
	 */
	private BigDecimal giveRegisterTotal;
	/**
	 * 升级赠送总计
	 */
	private BigDecimal giveLevelUpTotal;
	/**
	 * 体育派彩
	 */
	private BigDecimal sportAward;
	/**
	 * 彩票派彩
	 */
	private BigDecimal lotteryAward;
	/**
	 * 六合彩派彩
	 */
	private BigDecimal markSixAward;
	/**
	 * 真人派彩
	 */
	private BigDecimal realAward;
	/**
	 * 体育输赢
	 */
	private BigDecimal sportBunko;
	/**
	 * 彩票输赢
	 */
	private BigDecimal lotteryBunko;
	/**
	 * 六合彩输赢
	 */
	private BigDecimal markSixBunko;
	/**
	 * 真人输赢
	 */
	private BigDecimal realBunko;
	/**
	 * 全部输赢
	 */
	private BigDecimal allBunko;
	/**
	 * 全部余额
	 */
	private BigDecimal sumMoney;
	/**
	 * 投注总人数
	 */
	private Long betCountTotal;
	/**
	 * 首充总人数
	 */
	private Long firstDepositTotal;
	
	/**
	 * 昨日注册人数
	 */
	private Long registerAccountYes;
	/**
	 * 今日注册人数
	 */
	private Long registerAccount;
	/**
	 * 全部手动确认充值金额
	 */
	private BigDecimal depositHandlerArtificial;
	
	private BigDecimal sysLotteryTotal;
	private BigDecimal sysLotteryAward;
	private BigDecimal sysLotteryBunko;
	
	private BigDecimal sfMarkSixTotal;
	private BigDecimal sfMarkSixAward;
	private BigDecimal sfMarkSixBunko;
	
	/**
	 * 第三方体育
	 */
	private BigDecimal thirdSportsTotal;
	private BigDecimal thirdSportsAward;
	private BigDecimal thirdSportsBunko;
	
	/**
	 * 第三方彩票
	 */
	private BigDecimal thirdLotteryTotal;
	private BigDecimal thirdLotteryAward;
	private BigDecimal thirdLotteryBunko;
	
	/**
	 * 棋牌
	 */
	private BigDecimal chessTotal;
	private BigDecimal chessAward;
	private BigDecimal chessBunko;
	
	/**
	 * 电竞
	 */
	private BigDecimal esportsTotal;
	private BigDecimal esportsAward;
	private BigDecimal esportsBunko;
	
	/**
	 * 全部活动中奖
	 */
	private BigDecimal activeAwardTotal;
	
	/**
	 * 余额宝盈利总计
	 */
	private BigDecimal balanceGemTotal;
	
	/**
	 * 充值总次数
	 */
	private Long depositTimes;
	
	/**
	 * 提款总次数
	 */
	private Long withdrawTimes;
	
	/**
	 * 充值总人数
	 */
	private Long depositMemberCount;
	
	/**
	 * 提款总人数
	 */
	private Long withdrawMemberCount;
	
	public Long getBetCountTotal() {
		return betCountTotal;
	}

	public void setBetCountTotal(Long betCountTotal) {
		this.betCountTotal = betCountTotal;
	}

	public BigDecimal getSumMoney() {
		return sumMoney;
	}

	public void setSumMoney(BigDecimal sumMoney) {
		this.sumMoney = sumMoney;
	}

	public BigDecimal getDepositTotal() {
		return depositTotal;
	}

	public void setDepositTotal(BigDecimal depositTotal) {
		this.depositTotal = depositTotal;
	}

	public BigDecimal getWithdrawTotal() {
		return withdrawTotal;
	}

	public void setWithdrawTotal(BigDecimal withdrawTotal) {
		this.withdrawTotal = withdrawTotal;
	}

	public BigDecimal getManualDepositTotal() {
		return manualDepositTotal;
	}

	public void setManualDepositTotal(BigDecimal manualDepositTotal) {
		this.manualDepositTotal = manualDepositTotal;
	}

	public BigDecimal getManualWithdrawTotal() {
		return manualWithdrawTotal;
	}

	public void setManualWithdrawTotal(BigDecimal manualWithdrawTotal) {
		this.manualWithdrawTotal = manualWithdrawTotal;
	}

	public BigDecimal getSportTotal() {
		return sportTotal;
	}

	public void setSportTotal(BigDecimal sportTotal) {
		this.sportTotal = sportTotal;
	}

	public BigDecimal getLotteryTotal() {
		return lotteryTotal;
	}

	public void setLotteryTotal(BigDecimal lotteryTotal) {
		this.lotteryTotal = lotteryTotal;
	}

	public BigDecimal getRebateTotal() {
		return rebateTotal;
	}

	public void setRebateTotal(BigDecimal rebateTotal) {
		this.rebateTotal = rebateTotal;
	}

	public BigDecimal getGiveTotal() {
		return giveTotal;
	}

	public void setGiveTotal(BigDecimal giveTotal) {
		this.giveTotal = giveTotal;
	}

	public BigDecimal getRealTotal() {
		return realTotal;
	}

	public void setRealTotal(BigDecimal realTotal) {
		this.realTotal = realTotal;
	}

	public BigDecimal getSportBunko() {
		return sportBunko;
	}

	public void setSportBunko(BigDecimal sportBunko) {
		this.sportBunko = sportBunko;
	}

	public BigDecimal getLotteryBunko() {
		return lotteryBunko;
	}

	public void setLotteryBunko(BigDecimal lotteryBunko) {
		this.lotteryBunko = lotteryBunko;
	}

	public BigDecimal getRealBunko() {
		return realBunko;
	}

	public void setRealBunko(BigDecimal realBunko) {
		this.realBunko = realBunko;
	}

	public BigDecimal getAllBunko() {
		return allBunko;
	}

	public void setAllBunko(BigDecimal allBunko) {
		this.allBunko = allBunko;
	}

	public BigDecimal getRebateAgentTotal() {
		return rebateAgentTotal;
	}

	public void setRebateAgentTotal(BigDecimal rebateAgentTotal) {
		this.rebateAgentTotal = rebateAgentTotal;
	}

	public BigDecimal getSportAward() {
		return sportAward;
	}

	public void setSportAward(BigDecimal sportAward) {
		this.sportAward = sportAward;
	}

	public BigDecimal getLotteryAward() {
		return lotteryAward;
	}

	public void setLotteryAward(BigDecimal lotteryAward) {
		this.lotteryAward = lotteryAward;
	}

	public BigDecimal getRealAward() {
		return realAward;
	}

	public void setRealAward(BigDecimal realAward) {
		this.realAward = realAward;
	}

	public BigDecimal getMarkSixTotal() {
		return markSixTotal;
	}

	public void setMarkSixTotal(BigDecimal markSixTotal) {
		this.markSixTotal = markSixTotal;
	}

	public BigDecimal getMarkSixAward() {
		return markSixAward;
	}

	public void setMarkSixAward(BigDecimal markSixAward) {
		this.markSixAward = markSixAward;
	}

	public BigDecimal getMarkSixBunko() {
		return markSixBunko;
	}

	public void setMarkSixBunko(BigDecimal markSixBunko) {
		this.markSixBunko = markSixBunko;
	}

	public BigDecimal getDianZiTotal() {
		return dianZiTotal;
	}

	public void setDianZiTotal(BigDecimal dianZiTotal) {
		this.dianZiTotal = dianZiTotal;
	}

	public BigDecimal getDianZiAward() {
		return dianZiAward;
	}

	public void setDianZiAward(BigDecimal dianZiAward) {
		this.dianZiAward = dianZiAward;
	}

	public BigDecimal getDianZiBunko() {
		return dianZiBunko;
	}

	public void setDianZiBunko(BigDecimal dianZiBunko) {
		this.dianZiBunko = dianZiBunko;
	}

	public BigDecimal getDepositHandlerArtificial() {
		return depositHandlerArtificial;
	}

	public void setDepositHandlerArtificial(BigDecimal depositHandlerArtificial) {
		this.depositHandlerArtificial = depositHandlerArtificial;
	}

	public BigDecimal getGiveRegisterTotal() {
		return giveRegisterTotal;
	}

	public void setGiveRegisterTotal(BigDecimal giveRegisterTotal) {
		this.giveRegisterTotal = giveRegisterTotal;
	}

	public BigDecimal getSysLotteryTotal() {
		return sysLotteryTotal;
	}

	public void setSysLotteryTotal(BigDecimal sysLotteryTotal) {
		this.sysLotteryTotal = sysLotteryTotal;
	}

	public BigDecimal getSysLotteryAward() {
		return sysLotteryAward;
	}

	public void setSysLotteryAward(BigDecimal sysLotteryAward) {
		this.sysLotteryAward = sysLotteryAward;
	}

	public BigDecimal getSysLotteryBunko() {
		return sysLotteryBunko;
	}

	public void setSysLotteryBunko(BigDecimal sysLotteryBunko) {
		this.sysLotteryBunko = sysLotteryBunko;
	}

	public BigDecimal getActiveAwardTotal() {
		return activeAwardTotal;
	}

	public void setActiveAwardTotal(BigDecimal activeAwardTotal) {
		this.activeAwardTotal = activeAwardTotal;
	}

	public BigDecimal getSfMarkSixTotal() {
		return sfMarkSixTotal;
	}

	public void setSfMarkSixTotal(BigDecimal sfMarkSixTotal) {
		this.sfMarkSixTotal = sfMarkSixTotal;
	}

	public BigDecimal getSfMarkSixAward() {
		return sfMarkSixAward;
	}

	public void setSfMarkSixAward(BigDecimal sfMarkSixAward) {
		this.sfMarkSixAward = sfMarkSixAward;
	}

	public BigDecimal getSfMarkSixBunko() {
		return sfMarkSixBunko;
	}

	public void setSfMarkSixBunko(BigDecimal sfMarkSixBunko) {
		this.sfMarkSixBunko = sfMarkSixBunko;
	}

	public BigDecimal getThirdSportsTotal() {
		return thirdSportsTotal;
	}

	public void setThirdSportsTotal(BigDecimal thirdSportsTotal) {
		this.thirdSportsTotal = thirdSportsTotal;
	}

	public BigDecimal getThirdSportsAward() {
		return thirdSportsAward;
	}

	public void setThirdSportsAward(BigDecimal thirdSportsAward) {
		this.thirdSportsAward = thirdSportsAward;
	}

	public BigDecimal getThirdSportsBunko() {
		return thirdSportsBunko;
	}

	public void setThirdSportsBunko(BigDecimal thirdSportsBunko) {
		this.thirdSportsBunko = thirdSportsBunko;
	}

	public BigDecimal getGiveLevelUpTotal() {
		return giveLevelUpTotal;
	}

	public void setGiveLevelUpTotal(BigDecimal giveLevelUpTotal) {
		this.giveLevelUpTotal = giveLevelUpTotal;
	}

	public BigDecimal getThirdLotteryTotal() {
		return thirdLotteryTotal;
	}

	public void setThirdLotteryTotal(BigDecimal thirdLotteryTotal) {
		this.thirdLotteryTotal = thirdLotteryTotal;
	}

	public BigDecimal getThirdLotteryAward() {
		return thirdLotteryAward;
	}

	public void setThirdLotteryAward(BigDecimal thirdLotteryAward) {
		this.thirdLotteryAward = thirdLotteryAward;
	}

	public BigDecimal getThirdLotteryBunko() {
		return thirdLotteryBunko;
	}

	public void setThirdLotteryBunko(BigDecimal thirdLotteryBunko) {
		this.thirdLotteryBunko = thirdLotteryBunko;
	}

	public BigDecimal getChessTotal() {
		return chessTotal;
	}

	public void setChessTotal(BigDecimal chessTotal) {
		this.chessTotal = chessTotal;
	}

	public BigDecimal getChessAward() {
		return chessAward;
	}

	public void setChessAward(BigDecimal chessAward) {
		this.chessAward = chessAward;
	}

	public BigDecimal getChessBunko() {
		return chessBunko;
	}

	public void setChessBunko(BigDecimal chessBunko) {
		this.chessBunko = chessBunko;
	}

	public Long getFirstDepositTotal() {
		return firstDepositTotal;
	}

	public void setFirstDepositTotal(Long firstDepositTotal) {
		this.firstDepositTotal = firstDepositTotal;
	}

	public Long getRegisterAccountYes() {
		return registerAccountYes;
	}

	public void setRegisterAccountYes(Long registerAccountYes) {
		this.registerAccountYes = registerAccountYes;
	}

	public Long getRegisterAccount() {
		return registerAccount;
	}

	public void setRegisterAccount(Long registerAccount) {
		this.registerAccount = registerAccount;
	}

	public BigDecimal getBalanceGemTotal() {
		return balanceGemTotal;
	}

	public void setBalanceGemTotal(BigDecimal balanceGemTotal) {
		this.balanceGemTotal = balanceGemTotal;
	}

	public BigDecimal getWithdrawGiveTotal() {
		return withdrawGiveTotal;
	}

	public void setWithdrawGiveTotal(BigDecimal withdrawGiveTotal) {
		this.withdrawGiveTotal = withdrawGiveTotal;
	}

	public BigDecimal getEsportsTotal() {
		return esportsTotal;
	}

	public void setEsportsTotal(BigDecimal esportsTotal) {
		this.esportsTotal = esportsTotal;
	}

	public BigDecimal getEsportsAward() {
		return esportsAward;
	}

	public void setEsportsAward(BigDecimal esportsAward) {
		this.esportsAward = esportsAward;
	}

	public BigDecimal getEsportsBunko() {
		return esportsBunko;
	}

	public void setEsportsBunko(BigDecimal esportsBunko) {
		this.esportsBunko = esportsBunko;
	}

	public Long getDepositTimes() {
		return depositTimes;
	}

	public void setDepositTimes(Long depositTimes) {
		this.depositTimes = depositTimes;
	}

	public Long getWithdrawTimes() {
		return withdrawTimes;
	}

	public void setWithdrawTimes(Long withdrawTimes) {
		this.withdrawTimes = withdrawTimes;
	}

	public Long getDepositMemberCount() {
		return depositMemberCount;
	}

	public void setDepositMemberCount(Long depositMemberCount) {
		this.depositMemberCount = depositMemberCount;
	}

	public Long getWithdrawMemberCount() {
		return withdrawMemberCount;
	}

	public void setWithdrawMemberCount(Long withdrawMemberCount) {
		this.withdrawMemberCount = withdrawMemberCount;
	}
	
	
}
