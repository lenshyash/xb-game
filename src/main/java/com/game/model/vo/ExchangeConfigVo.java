package com.game.model.vo;

public class ExchangeConfigVo {

	private Long configId;

	private Long stationId;

	private Long type;

	private Long status;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getActiveId() {
		return configId;
	}

	public void setActiveId(Long configId) {
		this.configId = configId;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
}
