package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_active_play_num")
public class MemberActivePlayNum {
	/**
	 * 用户ID
	 */
	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	/**
	 * 当前日期
	 */
	@Column(name = "cur_date")
	private Date curtDate;

	/**
	 * 参与活动次数
	 */
	@Column(name = "active_num")
	private Long activeNum;

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getCurrentDate() {
		return this.curtDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.curtDate = currentDate;
	}

	public Long getActiveNum() {
		return this.activeNum;
	}

	public void setActiveNum(Long activeNum) {
		this.activeNum = activeNum;
	}
}
