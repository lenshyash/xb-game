package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "bet_num_record")
public class BetNumRecord {
	public static final int TYPE_LOTTERY = 1;// 彩票
	public static final int TYPE_REAL = 2;// 真人
	public static final int TYPE_EGAME = 3;// 电子游艺
	public static final int TYPE_SPORT = 4;// 体育
	public static final int TYPE_MARK_SIX = 5;// 六合彩
	public static final int TYPE_DRAWNEED_ADD = 10;//人工增加
	public static final int TYPE_DRAWNEED_SUB = 11;//人工扣除
	public static final int TYPE_REGIST_GIFT = 12;//注册赠送
	public static final int TYPE_RED_PACKET = 13;//红包
	
	public static final int TYPE_BACKWATER = 21;//反水
	public static final int TYPE_DEPOSIT = 22;//存款
	public static final int TYPE_MONEYCHANGE_ADD = 87;//站内额度转入
	public static final int TYPE_API_DEPOSIT = 23;//系统接口存款
	public static final int TYPE_SPORT_THIRD = 99;//三方体育
	public static final int TYPE_LOTTERY_THIRD = 91;//三方彩票
	public static final int TYPE_CHESS = 90;//棋牌
	public static final int TYPE_ESPORT = 92;//电竞
	public static final int TYPE_Points_for_cash = 100;//积分兑换现金
	public static final int TYPE_SYSTEM_CLEAR_ZERO = 101;//系统操作
	public static final int TYPE_BALANCE_GEM_REBATE = 102;//余额宝返利
	

	@Column(name = "id", temp = true)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "before_num")
	private BigDecimal beforeNum;

	@Column(name = "after_num")
	private BigDecimal afterNum;

	@Column(name = "bet_num")
	private BigDecimal betNum;

	@Column(name = "type")
	private Integer type;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "remark")
	private String remark;
	/**
	 * 订单号
	 */
	@Column(name = "order_id")
	private String orderId;
	@Column(name = "parents")
	private String parents;// 用户的层级关系
	@Column(name = "biz_datetime")
	private Date bizDatetime;// 账变对应的订单时间

	@Column(name = "account")
	private String account;
	// 操作者id
	@Column(name = "operator_id")
	private Long operatorId;
	// 操作员账号
	@Column(name = "operator_name")
	private String operatorName;

	// 变动前提款所需打码量
	@Column(name = "before_draw_need")
	private BigDecimal beforeDrawNeed;
	// 变动后提款所需打码量
	@Column(name = "after_draw_need")
	private BigDecimal afterDrawNeed;
	// 变动的提款所需打码量
	@Column(name = "draw_need")
	private BigDecimal drawNeed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getBeforeNum() {
		return beforeNum;
	}

	public void setBeforeNum(BigDecimal beforeNum) {
		this.beforeNum = beforeNum;
	}

	public BigDecimal getAfterNum() {
		return afterNum;
	}

	public void setAfterNum(BigDecimal afterNum) {
		this.afterNum = afterNum;
	}

	public BigDecimal getBetNum() {
		return betNum;
	}

	public void setBetNum(BigDecimal betNum) {
		this.betNum = betNum;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Date getBizDatetime() {
		return bizDatetime;
	}

	public void setBizDatetime(Date bizDatetime) {
		this.bizDatetime = bizDatetime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public BigDecimal getDrawNeed() {
		return drawNeed;
	}

	public void setDrawNeed(BigDecimal drawNeed) {
		this.drawNeed = drawNeed;
	}

	public BigDecimal getBeforeDrawNeed() {
		return beforeDrawNeed;
	}

	public void setBeforeDrawNeed(BigDecimal beforeDrawNeed) {
		this.beforeDrawNeed = beforeDrawNeed;
	}

	public BigDecimal getAfterDrawNeed() {
		return afterDrawNeed;
	}

	public void setAfterDrawNeed(BigDecimal afterDrawNeed) {
		this.afterDrawNeed = afterDrawNeed;
	}

}