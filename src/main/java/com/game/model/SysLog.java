package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_log")
public class SysLog {

	// 总控日志
	public static long PLATFORM_ADMIN = 1l;
	// 平台日志
	public static long PLATFORM_PLATFORM = 2l;

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 操作员ID
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 操作员
	 */
	@Column(name = "account", length = 20)
	private String account;

	/**
	 * 日志类型 （1:总控、2:租户）
	 */
	@Column(name = "log_type")
	private Long logType;

	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 操作员IP
	 */
	@Column(name = "account_ip")
	private String accountIp;

	@Column(name = "log_content")
	private String logContent;

	@Column(name = "remark")
	private String remark;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "platform")
	private Long platform;

	@Column(name = "params")
	private String params;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getLogType() {
		return this.logType;
	}

	public void setLogType(Long logType) {
		this.logType = logType;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getAccountIp() {
		return this.accountIp;
	}

	public void setAccountIp(String accountIp) {
		this.accountIp = accountIp;
	}

	public String getLogContent() {
		return this.logContent;
	}

	public void setLogContent(String logContent) {
		this.logContent = logContent;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
}
