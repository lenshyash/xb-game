package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_user_group")
public class AgentUserGroup {

	public static final int TYPE_HIDDEN = 1;// 对租户隐藏，只有超级租户看得到
	public static final int TYPE_EDITABLE = 2;// 租户可编辑权限
	public static final int TYPE_SHOWED = 3;// 租户只看，不可编辑权限

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "station_id")
	private Long stationId;
	@InsertValue(value = "2")
	@Column(name = "type")
	private Integer type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
