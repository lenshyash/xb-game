package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "sys_login_log")
public class SysLoginLog {

	// 总控用户
	public static long PLATFORM_ADMIN = 1l;
	// 平台用户
	public static long PLATFORM_PLATFORM = 2l;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "account", length = 20)
	private String account;

	/**
	 * 用户账号类型（1、总控,2、平台用户）
	 */
	@Column(name = "account_type")
	private Long accountType;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "content")
	private String content;

	@Column(name = "account_ip", length = 20)
	private String accountIp;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "platform")
	private Long platform;
	@Column(name = "login_os")
	private String loginOs;
	
	@Column(name = "domain")
	private String domain;
	
	@Column(name = "device_id")
	private String deviceId;
	
	@Column(name = "device_type")
	private String deviceType;
	
	@Column(name = "user_agent")
	private String userAgent;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountType() {
		return this.accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAccountIp() {
		return this.accountIp;
	}

	public void setAccountIp(String accountIp) {
		this.accountIp = accountIp;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}

	public String getLoginOs() {
		return loginOs;
	}

	public void setLoginOs(String loginOs) {
		this.loginOs = loginOs;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	
	
}
