package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

/**
 * 拼红气红包
 * 
 * @author apple
 *
 */
@Table(name = "common_redpacket")
public class CommonRedpacket {

	public static long STATUS_DISABLED = 1;
	public static long STATUS_ENDALBED = 2;
	//过期
	public static long STATUS_EXPIRED = 3;
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 发起人
	 */
	@Column(name = "creator", length = 50)
	private String creator;

	/**
	 * 发起人Id
	 */
	@Column(name = "account_id", length = 50)
	private Long accountId;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 状态
	 */
	@Column(name = "status")
	private Long status;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 结束时间
	 */
	@Column(name = "end_datetime")
	private Date endDatetime;

	/**
	 * 红包短语
	 */
	@Column(name = "title")
	private String title;

	@Column(name = "total_number")
	private Integer totalNumber;

	@Column(name = "total_money")
	private BigDecimal totalMoney;

	@Column(name = "min_money")
	private BigDecimal minMoney;

	@Column(name = "remain_money")
	private BigDecimal remainMoney;

	@Column(name = "remain_number")
	private Integer remainNumber;
	
	private String redPacketPassword;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public BigDecimal getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(BigDecimal minMoney) {
		this.minMoney = minMoney;
	}

	public BigDecimal getRemainMoney() {
		return remainMoney;
	}

	public void setRemainMoney(BigDecimal remainMoney) {
		this.remainMoney = remainMoney;
	}

	public Integer getRemainNumber() {
		return remainNumber;
	}

	public void setRemainNumber(Integer remainNumber) {
		this.remainNumber = remainNumber;
	}

	public String getRedPacketPassword() {
		return redPacketPassword;
	}

	public void setRedPacketPassword(String redPacketPassword) {
		this.redPacketPassword = redPacketPassword;
	}
}
