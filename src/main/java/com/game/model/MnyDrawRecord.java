package com.game.model;

import java.math.BigDecimal;

import lombok.Data;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

import javax.persistence.Id;
@Data
@Table(name = "mny_draw_record")
public class MnyDrawRecord extends BaseModel {

	// 未处理
	public static long STATUS_UNTREATED = 1l;
	// 处理成功
	public static long STATUS_SUCCESS = 2l;
	// 处理失败
	public static long STATUS_FAILED = 3l;
	// 已取消
	public static long STATUS_CANCELED = 4l;
	// 已过期
	public static long STATUS_EXPIRED = 5l;
	// 等待通知
	public static long STATUS_WAIT = 6l;
	
	

	// 未锁定
	public static long LOCK_FLAG_UNLOCKED = 1l;
	// 锁定
	public static long LOCK_FLAG_LOCKED = 2l;
	// 已完成
	public static long LOCK_FLAG_COMPLETED = 3l;
	// 锁定等待通知
	public static long LOCK_FLAG_LOCKED_WAIT = 4l;
	
	// 首提
	public static long FIRST_DRAW = 2l;
	// 普提
	public static long FIRST_DRAW_NONE = 1l;


	@Id
	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "order_no")
	private String orderNo;

	@Column(name = "member_id")
	private Long memberId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "draw_money")
	private BigDecimal drawMoney;

	@Column(name = "true_money")
	private BigDecimal trueMoney;

	@Column(name = "fee")
	private BigDecimal fee;

	@Column(name = "status")
	private Long stauts;

	@Column(name = "type")
	private Long type;

	@Column(name = "lock_flag")
	private Long lockFlag;

	@Column(name = "account")
	private String account;

	@Column(name = "remark")
	private String remark;

	@Column(name = "money_record_id")
	private Long moneyRecordId;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "card_no")
	private String cardNo;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "pay_id")
	private Long payId;
	
	@Column(name = "first_draw")
	private Long firstDraw;
	
	@Column(name="draw_times")
	private Long drawTimes;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getStauts() {
		return stauts;
	}

	public void setStauts(Long stauts) {
		this.stauts = stauts;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public BigDecimal getDrawMoney() {
		return drawMoney;
	}

	public void setDrawMoney(BigDecimal drawMoney) {
		this.drawMoney = drawMoney;
	}

	public BigDecimal getTrueMoney() {
		return trueMoney;
	}

	public void setTrueMoney(BigDecimal trueMoney) {
		this.trueMoney = trueMoney;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Long getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(Long lockFlag) {
		this.lockFlag = lockFlag;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getMoneyRecordId() {
		return moneyRecordId;
	}

	public void setMoneyRecordId(Long moneyRecordId) {
		this.moneyRecordId = moneyRecordId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public Long getFirstDraw() {
		return firstDraw;
	}

	public void setFirstDraw(Long firstDraw) {
		this.firstDraw = firstDraw;
	}

	public Long getDrawTimes() {
		return drawTimes;
	}

	public void setDrawTimes(Long drawTimes) {
		this.drawTimes = drawTimes;
	}
	
	
}
