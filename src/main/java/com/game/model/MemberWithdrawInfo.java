package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_withdraw_info")
public class MemberWithdrawInfo {
	/**
	 * 用户ID
	 */
	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	/**
	 * 提款次数
	 */
	@Column(name = "withdraw_count")
	private Long withdrawCount;

	/**
	 * 提款总额
	 */
	@Column(name = "withdraw_total")
	private BigDecimal withdrawTotal;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getWithdrawCount() {
		return withdrawCount;
	}

	public void setWithdrawCount(Long withdrawCount) {
		this.withdrawCount = withdrawCount;
	}

	public BigDecimal getWithdrawTotal() {
		return withdrawTotal;
	}

	public void setWithdrawTotal(BigDecimal withdrawTotal) {
		this.withdrawTotal = withdrawTotal;
	}

	

	
}
