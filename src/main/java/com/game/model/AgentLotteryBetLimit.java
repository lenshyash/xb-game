package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="agent_lottery_bet_limit")
public class AgentLotteryBetLimit {
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="group_name")
	private String groupName;
	
	@Column(name="bet_limit")
	private Long betLimit;

	@Column(name = "status")
	private Long status;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "limit_type")
	private Long limitType;
	
	@Column(name = "lot_type")
	private Long lotType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getBetLimit() {
		return betLimit;
	}

	public void setBetLimit(Long betLimit) {
		this.betLimit = betLimit;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getLimitType() {
		return limitType;
	}

	public void setLimitType(Long limitType) {
		this.limitType = limitType;
	}

	public Long getLotType() {
		return lotType;
	}

	public void setLotType(Long lotType) {
		this.lotType = lotType;
	}
	
	
}
