package com.game.model.third;

import java.io.Serializable;
import java.math.BigDecimal;

public class TranLimitCache implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 最大额度
	 */
	private Integer maxQuota;
	/**
	 * 已经使用额度
	 */
	private Integer useQuota;

	/**
	 * 转入总额
	 */
	private BigDecimal intoMoney;

	/**
	 * 转出总额
	 */
	private BigDecimal outMoney;

	public Integer getMaxQuota() {
		return maxQuota;
	}

	public void setMaxQuota(Integer maxQuota) {
		this.maxQuota = maxQuota;
	}

	public Integer getUseQuota() {
		return useQuota;
	}

	public void setUseQuota(Integer useQuota) {
		this.useQuota = useQuota;
	}

	public BigDecimal getIntoMoney() {
		return intoMoney;
	}

	public void setIntoMoney(BigDecimal intoMoney) {
		this.intoMoney = intoMoney;
	}

	public BigDecimal getOutMoney() {
		return outMoney;
	}

	public void setOutMoney(BigDecimal outMoney) {
		this.outMoney = outMoney;
	}

	/**
	 * 获取剩余额度
	 * 
	 * @return
	 */
	public Integer getRemainingQuota() {
		if (maxQuota != null) {
			int i = maxQuota;
			if (outMoney != null) {
				i = i - outMoney.intValue();
			}
			if (intoMoney != null) {
				i = i + intoMoney.intValue();
			}
			return i;
		} else {
			return 0;
		}
	}

}
