package com.game.model.third;

import java.math.BigDecimal;

public class BetRecordVo {
	public static final int TYPE_REAL = 2;// 真人
	public static final int TYPE_EGAME = 3;// 电子游艺

	private Long id;
	private Integer type;
	private String orderId;
	private BigDecimal rakebackMoney;
	private BigDecimal rakebackRate;
	private BigDecimal drawNeed;
	private String rakebackDesc;
	private String account;
	private BigDecimal betMoney;
	private BigDecimal winMoney;
	private Long stationId;
	private Long accountId;
	private Long serverId;
	private String betTime;
	private Long agentId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getRakebackMoney() {
		return rakebackMoney;
	}

	public void setRakebackMoney(BigDecimal rakebackMoney) {
		this.rakebackMoney = rakebackMoney;
	}

	public BigDecimal getRakebackRate() {
		return rakebackRate;
	}

	public void setRakebackRate(BigDecimal rakebackRate) {
		this.rakebackRate = rakebackRate;
	}

	public BigDecimal getDrawNeed() {
		return drawNeed;
	}

	public void setDrawNeed(BigDecimal drawNeed) {
		this.drawNeed = drawNeed;
	}

	public String getRakebackDesc() {
		return rakebackDesc;
	}

	public void setRakebackDesc(String rakebackDesc) {
		this.rakebackDesc = rakebackDesc;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(BigDecimal betMoney) {
		this.betMoney = betMoney;
	}

	public BigDecimal getWinMoney() {
		return winMoney;
	}

	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getServerId() {
		return serverId;
	}

	public void setServerId(Long serverId) {
		this.serverId = serverId;
	}

	public String getBetTime() {
		return betTime;
	}

	public void setBetTime(String betTime) {
		this.betTime = betTime;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

}
