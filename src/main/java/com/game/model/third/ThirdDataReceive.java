package com.game.model.third;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "third_data_receive")
public class ThirdDataReceive {
	public static final int status_undo = 1;// 未解析
	public static final int status_success = 2;// 解析成功
	public static final Integer status_doing = 3;// 解析中

	@Column(name = "id", primarykey = true)
	private Long id;
	@Column(name = "unique_key")
	private String uniqueKey;
	@Column(name = "content")
	private String content;
	@Column(name = "create_time")
	private Date createTime;
	@Column(name = "parse_time")
	private Date parseTime;
	@Column(name = "status")
	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUniqueKey() {
		return uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getParseTime() {
		return parseTime;
	}

	public void setParseTime(Date parseTime) {
		this.parseTime = parseTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
