package com.game.model.third;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="real_game_trans_log")
public class RealGameTransLog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	/**
	 *转账成功
	 */
	public static final int TRANS_STATUS_SUCCESS = 1;
	/**
	 *转账失败
	 */
	public static final int TRANS_STATUS_FAIL = 2;
	
	/**
	 *转账未知
	 */
	public static final int TRANS_STATUS_UNKNOW = 3;
	
	//从第三方转出
	public static final int TRANS_TYPE_SUB = 1;
	
	//转入第三方
	public static final int TRANS_TYPE_ADD = 2;
	
	
	
	@Column(name="game_type")
	private Integer gameType;
	
	@Column(name="trans_type")
	private Integer transType;
	
	@Column(name="trans_money")
	private Double transMoney;
	/**
	 * '1，表示转账成功 2，表示转账失败 ';
	 */
	@Column(name="trans_status")
	private Integer transStatus;
	
	@Column(name="result_time")
	private Date resultTime;
	
	@Column(name="trans_id",length=32)
	private String transId;
	
	@Column(name="third_trans_id",length=32)
	private String thirdTransId;
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="account_id")
	private Long accountId;
	@Column(name="create_datetime")
	private Date createDatetime;
	
	
	
	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	/*------拓展字段----------*/
	@Column(temp=true, name = "account")
	private String account;
	@Column(temp=true, name = "domain")
	private String domain;
	
	@Column(temp=true, name = "floder")
	private String floder;
	/**
	 * 对应站点
	 */
	@Column(name="station_id",temp=true)
	private Long stationId;
	
	
	public String getFloder() {
		return floder;
	}

	public void setFloder(String floder) {
		this.floder = floder;
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getGameType(){
		return this.gameType;
	}
	
	public void setGameType(Integer gameType){
		this.gameType = gameType;
	}

	public Integer getTransType(){
		return this.transType;
	}
	
	public void setTransType(Integer transType){
		this.transType = transType;
	}

	public Double getTransMoney(){
		return this.transMoney;
	}
	
	public void setTransMoney(Double transMoney){
		this.transMoney = transMoney;
	}

	public Integer getTransStatus(){
		return this.transStatus;
	}
	
	public void setTransStatus(Integer transStatus){
		this.transStatus = transStatus;
	}

	public Date getResultTime(){
		return this.resultTime;
	}
	
	public void setResultTime(Date resultTime){
		this.resultTime = resultTime;
	}

	public String getTransId(){
		return this.transId;
	}
	
	public void setTransId(String transId){
		this.transId = transId;
	}

	public String getThirdTransId(){
		return this.thirdTransId;
	}
	
	public void setThirdTransId(String thirdTransId){
		this.thirdTransId = thirdTransId;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getAccountId(){
		return this.accountId;
	}
	
	public void setAccountId(Long accountId){
		this.accountId = accountId;
	}
}
