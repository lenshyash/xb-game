package com.game.model.third;

import java.util.Date;

import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "real_center_config")
public class RealCenterConfig extends Page<RealCenterConfig>{
	
	
	/**
	 * r
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_datetime")
	private Date modifyDatetime;

	/**
	 * 创建者
	 */
	@Column(name = "create_user_id")
	private Long createUserId;

	/**
	 * 修改者
	 */
	@Column(name = "modify_user_id")
	private Long modifyUserId;

	/**
	 * 主键
	 */
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 对应站点
	 */
	@Column(name = "station_id")
	private Long stationId;
	
	/**
	 * 三方租户配置id
	 */
	@Column(name = "tenant_id", length = 32)
	private String tenantId;

	/**
	 * 三方租户对应账户名称即前缀(5位)
	 */
	@Column(name = "tenant_name", length = 5)
	private String tenantName;

	/**
	 * 租户秘钥
	 */
	@Column(name = "tenant_secret", length = 64)
	private String tenantSecret;

	/**
	 * 真人中心接口url
	 */
	@Column(name = "real_center_api_url", length = 255)
	private String realCenterApiUrl;
	
	
	
	/**
		状态:(1,启用2,禁用)
	*/
	@Column(name="status")
	private Integer status;
	
	/**
	 * 状态:(2,启用1,禁用)
	 */
	public static final Integer STATUS_ENABLE  = 2;
	/**
	 * 禁用
	 */
	public static final Integer STATUS_DISABLE = 1;
	
	@Column(updatable=false,insertable=false,nullable=true, name = "domain")
	private String domain;
	@Column(updatable=false,insertable=false,nullable=true, name = "floder")
	private String floder;
	
	
	public String getFloder() {
		return floder;
	}

	public void setFloder(String floder) {
		this.floder = floder;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getModifyDatetime() {
		return this.modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public Long getCreateUserId() {
		return this.createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Long getModifyUserId() {
		return this.modifyUserId;
	}

	public void setModifyUserId(Long modifyUserId) {
		this.modifyUserId = modifyUserId;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getTenantId() {
		return this.tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return this.tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantSecret() {
		return this.tenantSecret;
	}

	public void setTenantSecret(String tenantSecret) {
		this.tenantSecret = tenantSecret;
	}


	public String getRealCenterApiUrl() {
		return this.realCenterApiUrl;
	}

	public void setRealCenterApiUrl(String realCenterApiUrl) {
		this.realCenterApiUrl = realCenterApiUrl;
	}
	public Integer getStatus(){
		return this.status;
	}
	
	public void setStatus(Integer status){
		this.status = status;
	}
}
