package com.game.model.third;


import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="real_game_user")
public class RealGameUser{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3780586646347420610L;
	/**
	 * 主键id
	 */
	@Column(name="id",primarykey=true)
	private Long id;
	/**
	 * 本地账户id
	 */	
	@Column(name="account_id")
	private Long accountId;
	/**
	 * 三方账户username
	 */	
	@Column(name="username",length=32)
	private String username;
	/**
	 * 三方账户密码
	 */	
	@Column(name="password",length=64)
	private String password;
	/**
	 * 三方游戏类型(1,ag 2,bbin 3,mg)
	 */	
	@Column(name="game_type")
	private Integer gameType;
	/**
	 * 乐观锁版本号
	 */	
	@Column(name="version")
	private Long version;
	/**
	 * 三方余额
	 */	
	@Column(name="balance")
	private Double balance;
	
	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_datetime")
	private Date modifyDatetime;
	
	
	/*------拓展字段----------*/
	@Column(updatable=false,insertable=false,nullable=true, name = "account")
	private String account;
	@Column(updatable=false,insertable=false,nullable=true, name = "domain")
	private String domain;
	
	@Column(updatable=false,insertable=false,nullable=true, name = "floder")
	private String floder;
	
	
	public String getFloder() {
		return floder;
	}

	public void setFloder(String floder) {
		this.floder = floder;
	}

	/**
	 * 对应站点
	 */
	@Column(name = "station_id",updatable=false,insertable=false,nullable=true)
	private Long stationId;

	
	
	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}
	

	public Integer getGameType() {
		return gameType;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getAccountId(){
		return this.accountId;
	}
	
	public void setAccountId(Long accountId){
		this.accountId = accountId;
	}

	public String getUsername(){
		return this.username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}

	public String getPassword(){
		return this.password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}

	public Double getBalance(){
		return this.balance;
	}
	
	public void setBalance(Double balance){
		this.balance = balance;
	}
}
