package com.game.model.dictionary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 金额来源类型
 * 
 * @author admin
 *
 */
public enum MoneyRecordType {

	/**
	 * 人工入款
	 */
	DEPOSIT_ARTIFICIAL(1L, true, "人工加款"),
	/**
	 * 人工出款
	 */
	WITHDRAW_ARTIFICIAL(2L, false, "人工扣款"),
	/**
	 * 在线出款
	 */
	WITHDRAW_ONLINE_FAILED(3L, true, "在线取款失败"),
	/**
	 * 在线出款
	 */
	WITHDRAW_ONLINE(4L, false, "在线取款"),
	/**
	 * 在线支付
	 */
	DEPOSIT_ONLINE_THIRD(5L, true, "在线支付"),
	/**
	 * 快速入款
	 */
	DEPOSIT_ONLINE_FAST(6L, true, "快速入款"),
	/**
	 * 一般入款
	 */
	DEPOSIT_ONLINE_BANK(7L, true, "一般入款"),
	/**
	 * 虚拟币入款
	 */
	DEPOSIT_VIRTUAL_BANK(8L, true, "虚拟币入款"),

	MEMBER_ROLL_BACK_ADD(9L, true, "反水加钱"),

	MEMBER_ROLL_BACK_SUB(10L, false, "反水回滚"),

	PROXY_REBATE_ADD(11L, true, "代理返点加钱"),

	PROXY_REBATE_SUB(12L, false, "代理返点回滚"),

	PROXY_MULTI_REBATE_ADD(13L, true, "多级代理返点加钱"),
	PROXY_MULTI_REBATE_SUB(14L, false, "多级代理返点回滚"),
	
	AGENT_PROFIT_SHARE_ADD(113L, true, "代理占成加钱"),

	REAL_GAME_ADD(15L, true, "三方额度转入系统额度"),

	REAL_GAME_SUB(16L, false, "系统额度转入三方额度"),
	
	MONEY_CHANGE_ADD(87L, true, "站内额度转入"),

	MONEY_CHANGE_SUB(88L, false, "站内额度转出"),
	
	DEPOSIT_CANCLE(55L, false, "存款取消"),

	WITHDRAW_CANCLE(54L, true, "提款取消"),
	/**
	 * 晋级赠送
	 */
	LEVEL_UP_GIFT(78L, true, "晋级赠送"),
	/**
	 * 注册赠送
	 */
	REGISTER_GIFT_ACTIVITY(79L, true, "注册赠送"),

	/**
	 * 存款赠送
	 */
	DEPOSIT_GIFT_ACTIVITY(80L, true, "存款赠送"),
	
	WITHDRAW_GIFT_ACTIVITY(81L, false, "彩金扣除"),

	SPORT_BETTING(201L, false, "体育投注"),

	SPORT_AWARD(202L, true, "体育派奖"),

	SPORT_CANCEL_ORDER(203L, true, "体育撤单"),

	SPORT_ROLLBACK(204, false, "体育派奖回滚"),
	/**
	 * 彩票投注
	 */
	LOTTERY_BETTING(130L, false, "彩票投注"),
	/**
	 * 彩票派奖
	 */
	LOTTERY_AWARD(131L, true, "彩票派奖"),
	/**
	 * 彩票撤单
	 */
	LOTTERY_ORDER_CANCEL(132L, true, "彩票撤单"),
	/**
	 * 彩票派奖回滚
	 */
	LOTTERY_ROLLBACK(133L, false, "彩票派奖回滚"),

	/**
	 * 参与合买
	 */
	LOTTERY_JOINT_PURCHASE(134L, false, "参与彩票合买"),

	/**
	 * 合买满员
	 */
	LOTTERY_JOINT_PURCHASE_PATE(135L, true, "彩票合买满员"),

	/**
	 * 合买方案失效(截止时间后还未满员，且未保底)
	 */
	LOTTERY_JOINT_INVALID(136L, true, "彩票合买失效"),

	/**
	 * 合买方案撤单
	 */
	LOTTERY_JOINT_CANCEL(137L, true, "彩票合买撤单"),
	/**
	 * 彩票合买派奖
	 */
	LOTTERY_JOINT_AWARD(139L, true, "彩票合买派奖"),

	/**
	 * 合买时间截止，使用保底金额
	 */
	LOTTERY_JOINT_PURCHASE_END_THEM(138L, true, "彩票合买截止"),
	/**
	 * 六合彩投注
	 */
	MARK_SIX_BETTING(140L, false, "六合彩投注"),
	/**
	 * 六合彩派奖
	 */
	MARK_SIX_AWARD(141L, true, "六合彩派奖"),
	/**
	 * 六合彩派奖回滚
	 */
	MARK_SIX_ROLLBACK(142L, false, "六合彩派奖回滚"),
	/**
	 * li六合彩撤单
	 */
	MARK_SIX_ORDER_CANCEL(143L, true, "六合彩撤单"),
	/**
	 * 
	 */
	WITHDRAW_FAILED_ROLLBACK(144L, false, "提款失败回滚"),
	/**
	 * 活动中奖
	 */
	ACTIVE_AWARD(18L, true, "活动中奖"),
	/**
	 * 现金兑换积分
	 */
	EXCHANGE_MNY_TO_SCORE(19L, false, "现金兑换积分"),
	/**
	 * 积分兑换现金
	 */
	EXCHANGE_SCORE_TO_MNY(20L, true, "积分兑换现金"),
	/**
	 * 三方转账失败
	 */
	REAL_GAME_FAILED(17L, true, "三方转账失败"),
	
	/**
	 * 三方彩票增加
	 */
	THIRD_LOTTERY_ADD(1130L, false, "三方彩票增加"),
	/**
	 * 三方彩票减少
	 */
	THIRD_LOTTERY_SUB(1131L, true, "三方彩票减少"),
	
	
	/**
	 * 抢红包 增加
	 */
	REDPACKET_ADD(168, true, "抢红包"),
	/**
	 * 发红包 减少
	 */
	REDPACKET_SUB(169, false, "发红包"),
	/**
	 * 红包金额返还
	 */
	REDPACKET_RETURN(170, true, "红包金额返还"),
	
	/**
	 * 通过接口加款
	 */
	DEPOSIT_BY_SYS_API(23L, true, "系统接口加款"),
	
	/**
	 * 彩票改单增加
	 */
	LOTTERY_CHANGE_OREDE_ADD(171L, true, "彩票"),
	 
	/**
	 * 彩票改单减少
	 */
	LOTTERY_CHANGE_OREDE_SUB(172L, false, "彩票"),
	
	/**
	 * 余额宝盈利
	 */
	BALANCE_GEM_REBATE(997L, true, "余额宝盈利"),
	
	/**
	 * 余额宝转入
	 */
	BALANCE_GEM_TRAN(998L, false, "余额宝转入"),

	/**
	 * 余额宝转出
	 */
	BALANCE_GEM_TRAN_OUT(999L, true, "余额宝转出"),

	/**
	 * 首提
	 */
	FIRST_WITHDRAW(996L, true, "首提"),
	/**
	 * 三方登录自动转入
	 */
	THIRD_LOGIN_AUTO_TRAN_INTO(1000L, true, "三方登录自动转入"),
	/**
	 * 三方下线自动转出
	 */
	THIRD_OFFLINE_AUTO_TRAN_OUT(1001L, false, "三方下线自动转出");

	private long type;
	private boolean add;// true : 加款操作 false：扣款操作
	private String name;

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	private MoneyRecordType(long type, boolean add, String name) {
		this.type = type;
		this.add = add;
		this.name = name;
	}

	public boolean isAdd() {
		return add;
	}

	public void setAdd(boolean add) {
		this.add = add;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static MoneyRecordType getMoneyRecordType(long val) {
		MoneyRecordType[] types = MoneyRecordType.values();
		for (int i = 0; i < types.length; i++) {
			if (types[i].getType() == val) {
				return types[i];
			}
		}
		return null;
	}

	public static boolean isNotUnique() {
		MoneyRecordType[] types = MoneyRecordType.values();
		Set<Long> typeVals = new HashSet<Long>();
		for (MoneyRecordType type : types) {
			typeVals.add(type.getType());
		}
		return types.length != typeVals.size();
	}

//	public static void main(String[] args) {
//		List<MoneyRecordType> list = new ArrayList<>();
//		for (MoneyRecordType t : MoneyRecordType.values()) {
//			list.add(t);
//		}
//		Collections.sort(list,new Comparator<MoneyRecordType>() {
//			@Override
//			public int compare(MoneyRecordType o1, MoneyRecordType o2) {
//				return (int)(o1.getType()-o2.getType());
//			}
//		});
//		for (MoneyRecordType t : list) {
//			System.out.println(t.getType()+":\""+t.getName()+"\",");
//		}
//	}
}
