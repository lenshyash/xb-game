package com.game.model.dictionary;

/**
 * 积分来源类型
 * 
 * @author admin
 *
 */
public enum RealGameType {

	/**
	 * 活动积分扣除
	 */
	IBS(1L, "ibs", "沙巴体育","onoff_ibc_game"),
	
	 AG(2L, "ag", "ag体育","onoff_ag_game"),
	 BBIN(3L, "bbin", "BBIN真人娱乐体育","onoff_bbin_game"),
	 VR(4L, "vr", "VR娱乐游戏","onoff_vr_game"),
	 KY(5L, "ky", "KY棋牌","onoff_ky_game"),
	 BG(6L, "bg", "BG真人娱乐","onoff_bg_game"),
	 MG(7L, "mg", "MG真人娱乐","onoff_mg_game"),
	 AB(8L, "ab", "AB电子","onoff_ab_game"),
	 PT(9L, "pt", "PT电子","onoff_pt_game"),
	 QT(10L, "qt", "QT电子","onoff_qt_game"),
	 OG(11L, "og", "OG电子","onoff_og_game"),
	 CQ9(12L, "cq9", "CQ9电子","onoff_cq9_game"),
	 JDB(13L, "jdb", "JDB电子","onoff_jdb_game"),
	 MW(14L,"mw","MW","onoff_mw_game"),
	 ISB(15L,"isb","ISB","onoff_isb_game"),
	 M8(16L,"m8","M8","onoff_m8_game"),
	 M8H(17L,"m8h","M8H","onoff_m8h_game"),
	 EBET(18L, "ebet", "EBET真人娱乐","onoff_ebet_game");
	 
	
	private long type;
	private String key;// true : 加积分操作 false：扣积分操作
	private String name;
	private String openFlag;
	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	private RealGameType(long type, String key , String name ,String openFlag) {
		this.type = type;
		this.key = key;
		this.name = name;
		this.openFlag = openFlag;
	}

	

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getOpenFlag() {
		return openFlag;
	}

	public void setOpenFlag(String openFlag) {
		this.openFlag = openFlag;
	}

	public static RealGameType getScoreRecordType(long val) {
		RealGameType[] types = RealGameType.values();
		for (int i = 0; i < types.length; i++) {
			if (types[i].getType() == val) {
				return types[i];
			}
		}
		return null;
	}
}
