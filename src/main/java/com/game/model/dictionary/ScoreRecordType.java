package com.game.model.dictionary;

/**
 * 积分来源类型
 * 
 * @author admin
 *
 */
public enum ScoreRecordType {

	/**
	 * 活动积分扣除
	 */
	ACTIVE_SUB(1L, false, "活动积分扣除"),
	/**
	 * 会员签到
	 */
	SIGN_IN(2L, true, "会员签到"),
	/**
	 * 现金兑换积分
	 */
	EXCHANGE_MNY_TO_SCORE(3L, true, "现金兑换积分"),
	/**
	 * 积分兑换现金
	 */
	EXCHANGE_SCORE_TO_MNY(4L, false, "积分兑换现金"),
	/**
	 * 存款赠送
	 */
	DEPOSIT_GIFT_ACTIVITY(5L, true, "存款赠送"),
	/**
	 * 大转盘活动
	 */
	ACTIVE_BIG_WHEEL(11L, true, "大转盘活动"),
	/**
	 * 人工入款
	 */
	ADD_ARTIFICIAL(6L, true, "人工添加"),
	/**
	 * 人工出款
	 */
	SUB_ARTIFICIAL(7L, false, "人工扣除");

	private long type;
	private boolean add;// true : 加积分操作 false：扣积分操作
	private String name;

	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	private ScoreRecordType(long type, boolean add, String name) {
		this.type = type;
		this.add = add;
		this.name = name;
	}

	public boolean isAdd() {
		return add;
	}

	public void setAdd(boolean add) {
		this.add = add;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static ScoreRecordType getScoreRecordType(long val) {
		ScoreRecordType[] types = ScoreRecordType.values();
		for (int i = 0; i < types.length; i++) {
			if (types[i].getType() == val) {
				return types[i];
			}
		}
		return null;
	}
}
