package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_api_deposit")
public class SysApiDeposit {

	@Column(name = "id", primarykey = true)
	private Long id;
	@Column(name = "station_id")
	private Long stationId;
	@Column(name = "money")
	private BigDecimal money;
	@Column(name = "create_datetime")
	private Date createDatetime;
	@Column(name = "ip")
	private String ip;
	@Column(name = "account")
	private String account;
	@Column(name = "account_id")
	private Long accountId;
	@Column(name = "order_id")
	private String orderId;
	@Column(name = "remark")
	private String remark;
	@Column(name = "bet_multiple")
	private BigDecimal betMultiple;

	private String ipStr;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getBetMultiple() {
		return betMultiple;
	}

	public void setBetMultiple(BigDecimal betMultiple) {
		this.betMultiple = betMultiple;
	}

	public String getIpStr() {
		return ipStr;
	}

	public void setIpStr(String ipStr) {
		this.ipStr = ipStr;
	}

}
