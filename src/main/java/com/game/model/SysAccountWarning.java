package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "sys_account_warning")
public class SysAccountWarning {
	
	public static long TYPE = 1;
	
	public static long STAUS_ENABLE = 2l;

	public static long STATUS_DISABLE = 1l;
	//普通
	public static long LEVEL_GENERAL = 1l;
	//高危
	public static long LEVEL_HIGH = 2l;
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 用户ID
	 */
	@Column(name = "account_id")
	private Long accountId;
	
	
	@Column(name = "account")
	private String account;
	
	
	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;
	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name = "modify_time")
	private Date modifyTime;
	
	/**
	 * 修改时间
	 */
	@Column(name = "modifyed")
	private String modifyed;
	
	@Column(name = "type")
	private Long type;
	
	@Column(name = "status")
	private Long status;
	
	/**
	 * 信息内容
	 */
	@Column(name = "remark")
	private String remark;
	/**
	 * 危险等级 1普通 2高危
	 */
	@Column(name = "level")
	private Long level;
	
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getModifyed() {
		return modifyed;
	}

	public void setModifyed(String modifyed) {
		this.modifyed = modifyed;
	}
	
}
