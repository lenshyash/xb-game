package com.game.model;

import java.math.BigDecimal;

import com.game.util.BigDecimalUtil;
import lombok.Data;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

import javax.persistence.Id;

@Data
@Table(name = "mny_com_record")
public class MnyComRecord extends BaseModel {

	// 未处理
	public static long STATUS_UNTREATED = 1l;
	// 处理成功
	public static long STATUS_SUCCESS = 2l;
	// 处理失败
	public static long STATUS_FAILED = 3l;
	// 已取消
	public static long STATUS_CANCELED = 4l;
	// 已过期
	public static long STATUS_EXPIRED = 5l;

	// 未锁定
	public static long LOCK_FLAG_UNLOCKED = 1l;
	// 锁定
	public static long LOCK_FLAG_LOCKED = 2l;
	// 已完成
	public static long LOCK_FLAG_COMPLETED = 3l;

	// 不接受赠送
	public static long ACCEPT_GIVE_NO = 1l;
	// 接受赠送
	public static long ACCEPT_GIVE_YES = 2l;

	// 处理方式 系统
	public static long HANDLER_TYPE_SYS = 2l;
	// 处理方式 手动
	public static long HANDLER_TYPE_HAND = 1l;
	
	// 首充
	public static long FIRST_DEPOSIT = 2l;
	// 普充
	public static long FIRST_DEPOSIT_NONE = 1l;
	@Id
	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "order_no")
	private String orderNo;

	@Column(name = "member_id")
	private Long memberId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "money")
	private BigDecimal money;

	@Column(name = "status")
	private Long stauts;

	@Column(name = "type")
	private Long type;

	@Column(name = "remark")
	private String remark;

	@Column(name = "account")
	private String account;

	@Column(name = "depositor")
	private String depositor;

	@Column(name = "lock_flag")
	private Long lockFlag;

	@Column(name = "pay_id")
	private Long payId;

	@Column(name = "pay_name")
	private String payName;

	@Column(name = "fee")
	private BigDecimal fee;

	@Column(name = "op_desc")
	private String opDesc;
	
	@Column(name="bank_code")
	private String bankCode;
	
	@Column(name="user_name",temp=true)
	private String userName;
	
	@Column(name="agent_name",temp=true)
	private String agentName;
	
	@Column(name="level_name",temp=true)
	private String levelName;
	
	@Column(name="first_deposit")
	private Long firstDeposit;
	
	@Column(name="report_type",temp=true)
	private Long reportType;
	
	@Column(name="account_type",temp=true)
	private Long accountType;
	
	@Column(name="com_times")
	private Long comTimes;

	@Column(name="abnormal_flag",temp=true)
	private Long abnormalFlag;

	/**
	 * 数字货币充值订单兑换汇率
	 */
	@Column(name="rate")
	private BigDecimal rate;


	/**
	 * 数字货币充值数量
	 */
	@Column(name="virtual_currency_amount")
	private BigDecimal virtualCurrencyAmount;

	/**
	 * 用作快速入款的支付账号
	 */
	@Column(name="pay_account")
	private String payAccount;

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Column(name = "handler_type")
	@InsertValue(value = "2")
	private Long handlerType;

	@Column(name="modify_user",temp=true)
	private String modifyUser;
	private String bankAccount;
	private String memberUsername;
	private String iconCss;
	@Column(name="parent_names",temp=true)
	private String parentNames;
	@Column(name="parents",temp=true)
	private String parents;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Long getStauts() {
		return stauts;
	}

	public void setStauts(Long stauts) {
		this.stauts = stauts;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getLockFlag() {
		return lockFlag;
	}

	public void setLockFlag(Long lockFlag) {
		this.lockFlag = lockFlag;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getOpDesc() {
		return opDesc;
	}

	public void setOpDesc(String opDesc) {
		this.opDesc = opDesc;
	}

	public Long getHandlerType() {
		return handlerType;
	}

	public void setHandlerType(Long handlerType) {
		this.handlerType = handlerType;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public Long getStatus() {
		return stauts;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getMemberUsername() {
		return memberUsername;
	}

	public void setMemberUsername(String memberUsername) {
		this.memberUsername = memberUsername;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Long getFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(Long firstDeposit) {
		this.firstDeposit = firstDeposit;
	}

	public Long getComTimes() {
		return comTimes;
	}

	public void setComTimes(Long comTimes) {
		this.comTimes = comTimes;
	}

	public Long getAbnormalFlag() {
		return abnormalFlag;
	}

	public void setAbnormalFlag(Long abnormalFlag) {
		this.abnormalFlag = abnormalFlag;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getVirtualCurrencyAmount() {
		return virtualCurrencyAmount;
	}

	public void setVirtualCurrencyAmount(BigDecimal virtualCurrencyAmount) {
		this.virtualCurrencyAmount = virtualCurrencyAmount;
	}

	public String getIconCss() {
		return iconCss;
	}

	public void setIconCss(String iconCss) {
		this.iconCss = iconCss;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

	public String getParentNames() {
		return parentNames;
	}

	public void setParentNames(String parentNames) {
		this.parentNames = parentNames;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}
}
