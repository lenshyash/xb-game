package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_red_packet_record")
public class MemberRedPacketRecord {

	// 未处理
	public static int STATUS_UNTREATED = 1;
	// 处理成功
	public static int STATUS_SUCCESS = 2;
	// 处理失败
	public static int STATUS_FAILED = 3;
	// 已取消
	public static int STATUS_CANCELED = 4;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "account")
	private String account;

	@Column(name = "money")
	private BigDecimal money;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "red_packet_id")
	private Long redPacketId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "status")
	private Integer status;

	@Column(name = "remark")
	private String remark;
	@Column(name = "red_packet_name")
	private String redPacketName;
	@Column(name = "ip")
	private String ip;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getMoney() {
		return this.money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getRedPacketId() {
		return this.redPacketId;
	}

	public void setRedPacketId(Long redPacketId) {
		this.redPacketId = redPacketId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRedPacketName() {
		return redPacketName;
	}

	public void setRedPacketName(String redPacketName) {
		this.redPacketName = redPacketName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
