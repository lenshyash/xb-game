package com.game.model;

import java.math.BigDecimal;
import java.sql.Time;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_deposit_bank")
public class AgentDepositBank {

	/**
	 * 禁用
	 */
	public static long STATUS_DISABLED = 1L;

	/**
	 * 启用
	 */
	public static long STATUS_ENABLE = 2L;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "bank_card")
	private String bankCard;

	@Column(name = "creator_name")
	private String creatorName;

	@Column(name = "bank_address")
	private String bankAddress;

	@Column(name = "min")
	private BigDecimal min;

	@Column(name = "max")
	private BigDecimal max;
	
	@Column(name = "sort_no")
	private Integer sortNo;

	@Column(name = "status")
	private Long status;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "pay_platform_id")
	private Long payPlatformId;

	@Column(name = "icon")
	private String icon;
	
	@Column(name = "bank_desc")
	private String bankDesc;
	
	@Column(name = "pay_starttime")
	private String payStarttime;
	
	@Column(name = "pay_endtime")
	private String payEndtime;
	
	public String getPayStarttime() {
		if(StringUtils.isEmpty(payStarttime)){
			return "00:00:00";
		}
		return payStarttime;
	}

	public void setPayStarttime(String payStarttime) {
		this.payStarttime = payStarttime;
	}

	public String getPayEndtime() {
		if(StringUtils.isEmpty(payEndtime)) {
			return "23:59:59";
		}
		return payEndtime;
	}

	public void setPayEndtime(String payEndtime) {
		this.payEndtime = payEndtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankCard() {
		return bankCard;
	}

	public void setBankCard(String bankCard) {
		this.bankCard = bankCard;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getBankAddress() {
		return bankAddress;
	}

	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	public BigDecimal getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getPayPlatformId() {
		return payPlatformId;
	}

	public void setPayPlatformId(Long payPlatformId) {
		this.payPlatformId = payPlatformId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}
	
	public String getBankDesc() {
		return bankDesc;
	}

	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}
}
