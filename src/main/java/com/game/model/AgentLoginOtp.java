package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_login_otp")
public class AgentLoginOtp {
	
	public static long type_one = 1l;
	public static long type_two = 2l;

	@Column(name = "id", primarykey = true)
	private Long id;
	@Column(name = "iccid")
	private String iccid;
	@Column(name = "password")
	private String password;
	@Column(name = "station_id")
	private Long stationId;
	@Column(name = "create_datetime")
	private Date createDatetime;
	/**
	 * 1 默认验证 2谷歌验证码
	 */
	@Column(name = "type")
	private Long type;
	
	/**
	 * 邮箱
	 */
	@Column(name = "email")
	private String email;
	
	/**
	 * 扫码地址
	 */
	@Column(name = "url")
	private String url;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIccid() {
		return iccid;
	}

	public void setIccid(String iccid) {
		this.iccid = iccid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
