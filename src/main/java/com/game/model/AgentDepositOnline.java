package com.game.model;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import com.game.util.DateUtil;

@Table(name = "agent_deposit_online")
public class AgentDepositOnline {

	/**
	 * 京东扫码
	 */
	public static String PAY_TYPE_JDPAY = "6";

	/**
	 * QQ钱包
	 */
	public static String PAY_TYPE_QQPAY = "5";

	/**
	 * 支付宝
	 */
	public static String PAY_TYPE_ALIPAY = "4";

	/**
	 * 微信
	 */
	public static String PAY_TYPE_WECHAT = "3";

	/**
	 * 银行卡 （收银台）
	 */
	public static String PAY_TYPE_BANK = "1";

	/**
	 * 禁用
	 */
	public static long STATUS_DISABLED = 1L;

	/**
	 * 启用
	 */
	public static long STATUS_ENABLE = 2L;

	/**
	 * 所有终端都显示
	 */
	public static String SHOW_TYPE_ALL = "all";
	/**
	 * pc端显示
	 */
	public static String SHOW_TYPE_PC = "pc";
	/**
	 * 手机端显示
	 */
	public static String SHOW_TYPE_MOBILE = "mobile";
	/**
	 * app端显示
	 */
	public static String SHOW_TYPE_APP = "app";
	
	/**
	 * 随机金额开
	 */
	public static long RANDOM_FLAG_ENABLE = 2L;

	/**
	 * 随机金额关
	 */
	public static long RANDOM_FLAG_DISABLED = 1L;
	
	/**
	 * 固定金额开
	 */
	public static long FIXED_FLAG_ENABLE = 2L;

	/**
	 * 固定金额关
	 */
	public static long FIXED_FLAG_DISABLED = 1L;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "merchant_code")
	private String merchantCode;

	@Column(name = "merchant_key")
	private String merchantKey;

	@Column(name = "url")
	private String url;

	@Column(name = "min")
	private BigDecimal min;

	@Column(name = "max")
	private BigDecimal max;

	@Column(name = "account")
	private String account;

	@Column(name = "def")
	private Long def;

	@Column(name = "status")
	private Long status;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "pay_platform_id")
	private Long payPlatformId;

	@Column(name = "icon")
	private String icon;

	@Column(name = "pay_type")
	private String payType;

	@Column(name = "pay_getway")
	private String payGetway;

	@Column(name = "appid")
	private String appid;

	@Column(name = "sort_no")
	private Integer sortNo;
	
	@Column(name = "show_type")
	private String showType;
	
	@Column(name = "pay_desc")
	private String payDesc;
	
	@Column(name = "pay_starttime")
	private String payStarttime;
	
	@Column(name = "pay_endtime")
	private String payEndtime;
	
	@Column(name = "random_flag")
	private Long randomFlag;
	
	@Column(name = "fixed_flag")
	private Long fixedFlag;
	
	@Column(name = "fixed_amount")
	private String fixedAmount;
	
	@Column(name = "random_amount")
	private String randomAmount;
	
	@Column(name = "pay_channel")
	private String payChannel;
	
	public String getPayStarttime() {
		if(StringUtils.isEmpty(payStarttime)){
			return "00:00:00";
		}
		return payStarttime;
	}
	
	public void setPayStarttime(String payStarttime) {
		this.payStarttime = payStarttime;
	}

	public String getPayEndtime() {
		if(StringUtils.isEmpty(payEndtime)) {
			return "23:59:59";
		}
		return payEndtime;
	}

	public void setPayEndtime(String payEndtime) {
		this.payEndtime = payEndtime;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantKey() {
		return merchantKey;
	}

	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getDef() {
		return def;
	}

	public void setDef(Long def) {
		this.def = def;
	}

	public Long getPayPlatformId() {
		return payPlatformId;
	}

	public void setPayPlatformId(Long payPlatformId) {
		this.payPlatformId = payPlatformId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPayGetway() {
		return payGetway;
	}

	public void setPayGetway(String payGetway) {
		this.payGetway = payGetway;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getShowType() {
		return showType;
	}

	public void setShowType(String showType) {
		this.showType = showType;
	}
	
	public String getPayDesc() {
		return payDesc;
	}

	public void setPayDesc(String payDesc) {
		this.payDesc = payDesc;
	}

	public Long getRandomFlag() {
		return randomFlag;
	}

	public void setRandomFlag(Long randomFlag) {
		this.randomFlag = randomFlag;
	}

	public Long getFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(Long fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	public String getFixedAmount() {
		return fixedAmount;
	}

	public void setFixedAmount(String fixedAmount) {
		this.fixedAmount = fixedAmount;
	}

	public String getRandomAmount() {
		return randomAmount;
	}

	public void setRandomAmount(String randomAmount) {
		this.randomAmount = randomAmount;
	}

	public String getPayChannel() {
		return payChannel;
	}

	public void setPayChannel(String payChannel) {
		this.payChannel = payChannel;
	}
}
