package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "mny_exchange_config")
public class MnyExchangeConfig {

	public static long STATUS_DISABLED = 1;

	public static long STATUS_ENABLED = 2;

	public static long TYPE_MNY_TO_SCORE = 1;// 现金兑换积分

	public static long TYPE_SCORE_TO_MNY = 2;// 积分兑换现金

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 配置名称
	 */
	@Column(name = "name", length = 25)
	private String name;

	/**
	 * 配置类型
	 */
	@Column(name = "type")
	private Long type;

	/**
	 * 兑换分子数
	 */
	@Column(name = "numerator")
	private BigDecimal numerator;

	/**
	 * 兑换分母数
	 */
	@Column(name = "denominator")
	private BigDecimal denominator;

	/**
	 * 单次兑换最大值
	 */
	@Column(name = "max_val")
	private BigDecimal maxVal;

	/**
	 * 单次兑换最小值
	 */
	@Column(name = "min_val")
	private BigDecimal minVal;

	/**
	 * 站点Id
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 状态(1、禁用，2、启用)
	 */
	@Column(name = "status")
	private Long status;
	

	/**
	 * 积分兑换现金时设置打码量
	 */
	@Column(name = "bet_num")
	private BigDecimal betNum;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getType() {
		return this.type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public BigDecimal getNumerator() {
		return this.numerator;
	}

	public void setNumerator(BigDecimal numerator) {
		this.numerator = numerator;
	}

	public BigDecimal getDenominator() {
		return this.denominator;
	}

	public void setDenominator(BigDecimal denominator) {
		this.denominator = denominator;
	}

	public BigDecimal getMaxVal() {
		return this.maxVal;
	}

	public void setMaxVal(BigDecimal maxVal) {
		this.maxVal = maxVal;
	}

	public BigDecimal getMinVal() {
		return this.minVal;
	}

	public void setMinVal(BigDecimal minVal) {
		this.minVal = minVal;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getBetNum() {
		return betNum;
	}

	public void setBetNum(BigDecimal betNum) {
		this.betNum = betNum;
	}
	
}
