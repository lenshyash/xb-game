package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="agent_app_key_config")
public class AgentAppKeyConfig
{
	
	/**
	 * 关闭
	 */
	public static long AGENT_APP_PAY_CONFIG_DISABLED = 0L;

	/**
	 * 开启
	 */
	public static long AGENT_APP_PAY_CONFIG_ENABLE = 1L;
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 钥匙名称
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 钥匙版本号
	 */
	@Column(name = "version")
	private String version;

	/**
	 * 钥匙url
	 */
	@Column(name = "url")
	private String url;
	
	/**
	 * 钥匙应用开关，0=关，1=开
	 */
	@Column(name="status")
	private Long status;
	
	@Column(name="station_id")
	private Long stationId;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVersion()
	{
		return version;
	}

	public void setVersion(String version)
	{
		this.version = version;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public Long getStatus()
	{
		return status;
	}

	public void setStatus(Long status)
	{
		this.status = status;
	}

	public Long getStationId()
	{
		return stationId;
	}

	public void setStationId(Long stationId)
	{
		this.stationId = stationId;
	}
	
	

}
