package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_payment")
public class AgentPayment {

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "merchant_code")
	private String merchantCode;

	@Column(name = "merchant_key")
	private String merchantKey;

	@Column(name = "url")
	private String url;

	@Column(name = "account")
	private String account;

	@Column(name = "status")
	private Long status;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "pay_platform_id")
	private Long payPlatformId;

	@Column(name = "pay_getway")
	private String payGetway;

	@Column(name = "appid")
	private String appid;

	@Column(name = "sort_no")
	private Integer sortNo;

	@Column(name = "is_notity")
	private Long isNotity;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantKey() {
		return merchantKey;
	}

	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getPayPlatformId() {
		return payPlatformId;
	}

	public void setPayPlatformId(Long payPlatformId) {
		this.payPlatformId = payPlatformId;
	}

	public String getPayGetway() {
		return payGetway;
	}

	public void setPayGetway(String payGetway) {
		this.payGetway = payGetway;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getIsNotity() {
		return isNotity;
	}

	public void setIsNotity(Long isNotity) {
		this.isNotity = isNotity;
	}
	
}
