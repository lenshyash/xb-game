package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

@Table(name = "sys_pay_platform")
public class SysPayPlatform extends BaseModel {

	/**
	 * 账号启用状态
	 */
	public static final long STATUS_DISABLED = 1L;

	/**
	 * 账号禁用状态
	 */
	public static final long STATUS_ENABLED = 2L;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "company")
	private String company;

	@Column(name = "type")
	private Long type;

	@Column(name = "icon_css")
	private String iconCss;

	@Column(name = "img_path")
	private String imgPath;

	@Column(name = "remark")
	private String remark;

	@Column(name = "status")
	private Long status;
	
	@Column(name = "sort_no")
	private Integer sortNo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getIconCss() {
		return iconCss;
	}

	public void setIconCss(String iconCss) {
		this.iconCss = iconCss;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}
}
