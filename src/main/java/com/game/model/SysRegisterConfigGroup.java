package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_register_config_group")
public class SysRegisterConfigGroup {

	// 否
	public static long VALUE_DISABLED = 1l;
	// 是
	public static long VALUE_ENALBED = 2l;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "conf_id")
	private Long configId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "show_val")
	private Long showVal;

	@Column(name = "validate_val")
	private Long validateVal;

	@Column(name = "required_val")
	private Long requiredVal;
	
	@Column(name = "unique_val")
	private Long uniqueVal;
	
	@Column(name = "name_val")
	private String nameVal;
	
	@Column(name = "remind_text")
	private String remindText;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getConfigId() {
		return configId;
	}

	public void setConfigId(Long configId) {
		this.configId = configId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getShowVal() {
		return showVal;
	}

	public void setShowVal(Long showVal) {
		this.showVal = showVal;
	}

	public Long getValidateVal() {
		return validateVal;
	}

	public void setValidateVal(Long validateVal) {
		this.validateVal = validateVal;
	}

	public Long getRequiredVal() {
		return requiredVal;
	}

	public void setRequiredVal(Long requiredVal) {
		this.requiredVal = requiredVal;
	}

	public Long getUniqueVal() {
		return uniqueVal;
	}

	public void setUniqueVal(Long uniqueVal) {
		this.uniqueVal = uniqueVal;
	}

	public String getNameVal() {
		return nameVal;
	}

	public void setNameVal(String nameVal) {
		this.nameVal = nameVal;
	}

	public String getRemindText() {
		return remindText;
	}

	public void setRemindText(String remindText) {
		this.remindText = remindText;
	}
	
}
