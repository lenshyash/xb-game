package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_backwater_strategy_level")
public class MemberBackwaterStrategyLevel {

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "strategy_id")
	private Long strategyId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "member_level_id")
	private Long memberLevelId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(Long strategyId) {
		this.strategyId = strategyId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getMemberLevelId() {
		return memberLevelId;
	}

	public void setMemberLevelId(Long memberLevelId) {
		this.memberLevelId = memberLevelId;
	}

}
