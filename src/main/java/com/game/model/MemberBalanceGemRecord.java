package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;
import org.springframework.format.annotation.DateTimeFormat;

@Table(name = "member_balance_gem_record")
public class MemberBalanceGemRecord{
	
	// 余额宝盈利
	public static long TYPE_REBATE = 1l;
	// 转入金额
	public static long TYPE_TRANSFER = 2l;
	// 转出金额
	public static long TYPE_TRANSFER_OUT = 3l;
	//成功
	public static long STATUS_SUCCESS = 2l;
	//失败
	public static long STATUS_FAIL = 1l;
	
	@Column(name = "id", primarykey = true)
	private Long id;
	/**
	 * 订单号
	 */
	@Column(name = "order_no")
	private String orderNo;
	/**
	 * 会员ID
	 */
	@Column(name = "account_id")
	private Long accountId;
	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;
	/**
	 * 金额
	 */
	@Column(name = "money")
	private BigDecimal money;
	/**
	 * 变动前金额
	 */
	@Column(name = "before_money")
	private BigDecimal beforeMoney;
	/**
	 * 变动后金额
	 */
	@Column(name = "back_money")
	private BigDecimal backMoney;
	/**
	 * 状态
	 */
	@Column(name = "status")
	private Long stauts;
	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;
	/**
	 * 会员账号
	 */
	@Column(name = "account")
	private String account;
	/**
	 * 类型
	 */
	@Column(name = "type")
	@InsertValue(value = "1")
	private Long type;
	/**
	 * 创建人ID
	 */
	@Column(name = "create_user_id")
	private Long createUserId;
	
	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;
	/**
	 * 返利日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "rebate_date")
	private Date rebateDate;
	
	@Column(name = "report_type", temp=true)
	private Integer reportType;
	
	@Column(name = "account_type", temp=true)
	private Integer accountType;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public BigDecimal getBeforeMoney() {
		return beforeMoney;
	}
	public void setBeforeMoney(BigDecimal beforeMoney) {
		this.beforeMoney = beforeMoney;
	}
	public BigDecimal getBackMoney() {
		return backMoney;
	}
	public void setBackMoney(BigDecimal backMoney) {
		this.backMoney = backMoney;
	}
	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Long getStauts() {
		return stauts;
	}

	public void setStauts(Long stauts) {
		this.stauts = stauts;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	public Date getCreateDatetime() {
		return createDatetime;
	}
	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}
	public Date getRebateDate() {
		return rebateDate;
	}
	public void setRebateDate(Date rebateDate) {
		this.rebateDate = rebateDate;
	}
	public Integer getReportType() {
		return reportType;
	}
	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
}
