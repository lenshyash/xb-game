package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_station")
public class SysStation {
	/**
	 * 禁用 、关闭
	 */
	public static final long STATUS_DISABLE = 1L;
	/**
	 * 启用
	 */
	public static final long STATUS_ENABLE = 2L;
	
	public static String MULTI_FOLDER_KEY ="_multi_folder_";

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "status")
	private Long status;

	/**
	 * 站点对应的文件夹目录
	 */
	@Column(name = "floder", length = 50)
	private String floder;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "close_time")
	private Date closeTime;

	@Column(name = "open_time")
	private Date openTime;

	@Column(name = "account", temp = true)
	private String account;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "agentName", temp = true)
	private String agentName;

	@Column(name = "agent_id")
	private Long agentId;

	@Column(name = "domain_agent_id", temp = true)
	private Long domainAgentId;

	@Column(name = "is_reg_switch", temp = true)
	private Long regSwitch;

	@Column(name = "type", temp = true)
	private Long type;

	@Column(name = "default_home", temp = true)
	private String defaultHome;
	
	@Column(name = "domain_station_name", temp = true)
	private String domainStationName;
	
	@Column(name = "domain_folder", temp = true)
	private String domainFolder;
	
	@Column(name = "domain_plat_token", temp = true)
	private String domainPlatToken;
	
	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getFloder() {
		return this.floder;
	}

	public void setFloder(String floder) {
		this.floder = floder;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getCloseTime() {
		return this.closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Long getDomainAgentId() {
		return domainAgentId;
	}

	public void setDomainAgentId(Long domainAgentId) {
		this.domainAgentId = domainAgentId;
	}

	public Long getRegSwitch() {
		return regSwitch;
	}

	public void setRegSwitch(Long regSwitch) {
		this.regSwitch = regSwitch;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public String getDefaultHome() {
		return defaultHome;
	}

	public void setDefaultHome(String defaultHome) {
		this.defaultHome = defaultHome;
	}

	public String getDomainStationName() {
		return domainStationName;
	}

	public void setDomainStationName(String domainStationName) {
		this.domainStationName = domainStationName;
	}

	public String getDomainFolder() {
		return domainFolder;
	}

	public void setDomainFolder(String domainFolder) {
		this.domainFolder = domainFolder;
	}
	
	public String getDomainPlatToken() {
		return domainPlatToken;
	}

	public void setDomainPlatToken(String domainPlatToken) {
		this.domainPlatToken = domainPlatToken;
	}

}