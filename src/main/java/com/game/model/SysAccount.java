package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.AbstractUser;
import org.jay.frame.util.StringUtil;

@Table(name = "sys_account")
public class SysAccount extends AbstractUser {

	private static final long serialVersionUID = 1L;
	/**
	 * 账号禁用状态
	 */
	public static final long ACCOUNT_STATUS_DISABLED = 1L;
	/**
	 * 账号启用状态
	 */
	public static final long ACCOUNT_STATUS_ENABLED = 2L;
	/**
	 * 待审核
	 */
	public static final long ACCOUNT_STATUS_REVIEW = 3L;
	
	/**
	 * 代理平台账号
	 */
	public static final long ACCOUNT_PLATFORM_AGENT = 4;
	
	/**
	 * 平台引导账号
	 */
	public static final long ACCOUNT_PLATFORM_GUIDE = 9;
	/**
	 * 租户管理员
	 */
	public static final long ACCOUNT_PLATFORM_AGENT_MANAGER = 3;
	/**
	 * 租户超级管理员
	 */
	public static final long ACCOUNT_PLATFORM_AGENT_SUPER = 2;

	/**
	 * 总代理平台账号
	 */
	public static final long ACCOUNT_PLATFORM_AGENT_GENERAL = 5;
	
	/**
	 * 平台试玩账号
	 */
	public static final long ACCOUNT_PLATFORM_TEST_GUEST = 6;
	/**
	 * 会员平台账号
	 */
	public static final long ACCOUNT_PLATFORM_MEMBER = 1;
	/**
	 * 取款密码
	 */
	public static final long PASSWORD_TYPE_RECEIPT = 2;
	/**
	 * 登录密码
	 */
	public static final long PASSWORD_TYPE_LOGIN = 1;

	/**
	 * 在线
	 */
	public static final long ONLINE_FLAG_ON = 2;

	/**
	 * 离线
	 */
	public static final long ONLINE_FLAG_OFF = 1;
	/**
	 * 允许
	 */
	public static final long REDPACKET_ENABLED = 2;

	/**
	 * 禁止
	 */
	public static final long REDPACKET_DISABLED = 1;
	
	/**
	 * 普通
	 */
	public static final long REPORT_TYPE_NORMAL = 1;

	/**
	 * 引导
	 */
	public static final long REPORT_TYPE_GUIDE = 2;
	
	/**
	 * 账号异常
	 */
	public static final long ACCOUNT_ABNORMAL = 2L;
	
	/**
	 * 账号正常
	 */
	public static final long ACCOUNT_ABNORMAL_NONE = 1L;
	

	@Column(name = "account_type")
	private Long accountType;

	@Column(name = "account_status")
	private Long accountStatus;

	@Column(name = "agent_id")
	private Long agentId;

	@Column(name = "agent_name")
	private String agentName;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "level")
	private Long level;

	@Column(name = "group_id")
	private Long groupId;

	@Column(name = "parents")
	private String parents;

	@Column(name = "parent_names")
	private String parentNames;

	@InsertValue(value = "0")
	@Column(name = "bet_num")
	private BigDecimal betNum;

	@Column(name = "register_ip")
	private String registerIp;

	@Column(name = "last_login_ip")
	private String lastLoginIp;

	@InsertValue(value = "0")
	@Column(name = "draw_need")
	private BigDecimal drawNeed;

	@InsertValue(value = "0")
	@Column(name = "score")
	private BigDecimal score;
	
	@Column(name = "level_group")
	private Long levelGroup;
	
	/**
	 * 连续签到次数
	 */
	@InsertValue(value = "0")
	@Column(name = "sign_count")
	private Long signCount;

	/**
	 * 最近一次签到时间
	 */
	@Column(name = "last_sign_date")
	private Date lastSignDate;

	@InsertValue(value = "1")
	@Column(name = "online")
	private Long online;
	
	/**
	 * 最近一次登录时间
	 */
	@Column(name = "last_login_datetime")
	private Date lastLoginDatetime;

	@Column(name = "register_url")
	private String registerUrl;
	
	@Column(name = "register_os")
	private String registerOs;
	
	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;
	
	/**
	 * 赔率点数
	 */
	@InsertValue(value = "0")
	@Column(name = "rate")
	private BigDecimal rate;
	/**
	 * 限制发红包功能 
	 */
	@InsertValue(value = "2")
	@Column(name = "redpacket_limit")
	private Long redpacketLimit;
	
	/**
	 * 数据统计类型
	 */
	@InsertValue(value = "1")
	@Column(name = "report_type")
	private Long reportType;
	
	/**
	 * 用户指定聊天室token
	 */
	@Column(name = "chat_token")
	private String chatToken;
	
	/**
	 * 告警用户标志，手动设置
	 */
	@InsertValue(value = "0")
	@Column(name = "abnormal_flag")
	private Long abnormalFlag;
	
	
	/**
	 * 最后登录设备
	 */
	@Column(name = "last_login_device")
	private String lastLoginDevice;

	@Column(name = "im_user_id")
	private String imUserId;

	@Column(name = "im_chat_id")
	private String imChatId;

	public static final long LOGIN_ODD_NORMAL = 1L; //正常状态
	public static final long LOGIN_ODD_EXECUTE = 2L;//被标识状态
	public static final long LOGIN_ODD_LIFT = 3L;//解除状态

	/**
	 * 登陆异常标识，系统自动判断
	 */
	@InsertValue(value = "1")
	@Column(name = "login_odd_flag")
	private Long loginOddFlag;


	public String getRegisterUrl() {
		return registerUrl;
	}

	public void setRegisterUrl(String registerUrl) {
		this.registerUrl = registerUrl;
	}

	public String getRegisterOs() {
		return registerOs;
	}

	public void setRegisterOs(String registerOs) {
		this.registerOs = registerOs;
	}

	public Long getSignCount() {
		return signCount;
	}

	public void setSignCount(Long signCount) {
		this.signCount = signCount;
	}

	public Date getLastSignDate() {
		return lastSignDate;
	}

	public void setLastSignDate(Date lastSignDate) {
		this.lastSignDate = lastSignDate;
	}

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Long getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(Long accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public String getParentNames() {
		return parentNames;
	}

	public void setParentNames(String parentNames) {
		this.parentNames = parentNames;
	}

	public BigDecimal getBetNum() {
		return betNum;
	}

	public void setBetNum(BigDecimal betNum) {
		this.betNum = betNum;
	}

	@Override
	public void setAccount(String account) {
		if (StringUtil.isNotEmpty(account)) {
			this.account = account.trim().toLowerCase();
		} else {
			this.account = account;
		}
	}

	public String getRegisterIp() {
		return registerIp;
	}

	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public BigDecimal getDrawNeed() {
		return drawNeed;
	}

	public void setDrawNeed(BigDecimal drawNeed) {
		this.drawNeed = drawNeed;
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public Long getOnline() {
		return online;
	}

	public void setOnline(Long online) {
		this.online = online;
	}

	public Long getLevelGroup() {
		return levelGroup;
	}

	public void setLevelGroup(Long levelGroup) {
		this.levelGroup = levelGroup;
	}

	public Date getLastLoginDatetime() {
		return lastLoginDatetime;
	}

	public void setLastLoginDatetime(Date lastLoginDatetime) {
		this.lastLoginDatetime = lastLoginDatetime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Long getRedpacketLimit() {
		return redpacketLimit;
	}

	public void setRedpacketLimit(Long redpacketLimit) {
		this.redpacketLimit = redpacketLimit;
	}

	public String getChatToken() {
		return chatToken;
	}

	public void setChatToken(String chatToken) {
		this.chatToken = chatToken;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public Long getAbnormalFlag() {
		return abnormalFlag;
	}

	public void setAbnormalFlag(Long abnormalFlag) {
		this.abnormalFlag = abnormalFlag;
	}

	public String getLastLoginDevice() {
		return lastLoginDevice;
	}

	public void setLastLoginDevice(String lastLoginDevice) {
		this.lastLoginDevice = lastLoginDevice;
	}

	public String getImUserId() {
		return imUserId;
	}

	public void setImUserId(String imUserId) {
		this.imUserId = imUserId;
	}

	public String getImChatId() {
		return imChatId;
	}

	public void setImChatId(String imChatId) {
		this.imChatId = imChatId;
	}


	public Long getLoginOddFlag() {
		return loginOddFlag;
	}

	public void setLoginOddFlag(Long loginOddFlag) {
		this.loginOddFlag = loginOddFlag;
	}
}