package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "station_platform_transfer")
public class StationPlatformTransfer {

	@Column(name = "id", primarykey = true)
	private Long id;
	/**
	 * 转出总额
	 */
	@Column(name = "out_money")
	@InsertValue(value = "0")
	private BigDecimal outMoney;
	
	/**
	 * 转入总额
	 */
	@Column(name = "into_money")
	@InsertValue(value = "0")
	private BigDecimal intoMoney;

	/**
	 * 平台 ID
	 */
	@Column(name = "platform")
	private Integer platform;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPlatform() {
		return this.platform;
	}

	public void setPlatform(Integer platform) {
		this.platform = platform;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getOutMoney() {
		return outMoney;
	}

	public void setOutMoney(BigDecimal outMoney) {
		this.outMoney = outMoney;
	}

	public BigDecimal getIntoMoney() {
		return intoMoney;
	}

	public void setIntoMoney(BigDecimal intoMoney) {
		this.intoMoney = intoMoney;
	}
}
