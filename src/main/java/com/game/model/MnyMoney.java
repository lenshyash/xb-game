package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "mny_money")
public class MnyMoney {
	
	public static long MONEY_RMB = 1l;

	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	@Column(name = "money")
	private BigDecimal money;

	@Column(name = "money_type_id")
	private Long moneyTypeId;
	/**
	 * 当前余额宝余额
	 */
	@Column(name = "balance_gem_money")
	private BigDecimal balanceGemMoney;
	/**
	 * 余额宝累计收益
	 */
	@Column(name = "balance_gem_income")
	private BigDecimal balanceGemIncome;
	
	@Column(name = "rate",temp = true)
	private BigDecimal rate;
	
	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Long getMoneyTypeId() {
		return moneyTypeId;
	}

	public void setMoneyTypeId(Long moneyTypeId) {
		this.moneyTypeId = moneyTypeId;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getBalanceGemMoney() {
		return balanceGemMoney;
	}

	public void setBalanceGemMoney(BigDecimal balanceGemMoney) {
		this.balanceGemMoney = balanceGemMoney;
	}

	public BigDecimal getBalanceGemIncome() {
		return balanceGemIncome;
	}

	public void setBalanceGemIncome(BigDecimal balanceGemIncome) {
		this.balanceGemIncome = balanceGemIncome;
	}
}
