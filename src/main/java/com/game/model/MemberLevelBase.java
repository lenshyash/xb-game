package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_level_base")
public class MemberLevelBase {
	
	public static int LEVEL_DEFALUT_NONE = 1;
	public static int LEVEL_DEFALUT = 2;
	
	public static int STATUS_DISABLED = 1;
	public static int STATUS_ENALBED = 2;
	
	public static long IMPORT_TYPE_COMMON = 1;//会员代理混合导入
	public static long IMPORT_TYPE_AGENT = 2;//导下该代理所有下线
	
	public static String LEVEL_DEFALUT_NAME = "未分层";
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 成员数量
	 */
	@Column(name = "member_count")
	private Integer memberCount;

	/**
	 * 充值金额
	 */
	@Column(name = "deposit_money")
	private BigDecimal depositMoney;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 状态(1禁用，2启用)
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 是否默认未分层(1否，2是)
	 */
	@Column(name = "level_default")
	private Integer levelDefault;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 等级名称
	 */
	@Column(name = "level_name", length = 50)
	private String levelName;

	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;
	/**
	 * 用户等级图标
	 */
	@Column(name = "icon")
	private String icon;
	
	/**
	 * 晋级彩金
	 */
	@Column(name = "gift_money")
	@InsertValue(value ="0")
	private BigDecimal giftMoney;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMemberCount() {
		return this.memberCount;
	}

	public void setMemberCount(Integer memberCount) {
		this.memberCount = memberCount;
	}

	public BigDecimal getDepositMoney() {
		return this.depositMoney;
	}

	public void setDepositMoney(BigDecimal depositMoney) {
		this.depositMoney = depositMoney;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getLevelName() {
		return this.levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getLevelDefault() {
		return levelDefault;
	}

	public void setLevelDefault(Integer levelDefault) {
		this.levelDefault = levelDefault;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public BigDecimal getGiftMoney() {
		return giftMoney;
	}

	public void setGiftMoney(BigDecimal giftMoney) {
		this.giftMoney = giftMoney;
	}
}
