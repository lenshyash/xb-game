package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

@Table(name = "proxy_multi_rebate_setting")
public class ProxyMultiRebateSetting extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6478354417522380238L;

	@Column(name = "account_id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long accountId;

	@Column(name = "game_share")
	private BigDecimal gameShare;

	@InsertValue(value = "0")
	@Column(name = "profit_share")
	private BigDecimal profitShare;

	/*------拓展字段----------*/
	@Column(name = "account", insertable = false, nullable = true, updatable = false, temp = true)
	private String account;
	/**
	 * 对应站点ID
	 */
	@Column(name = "station_id", insertable = false, nullable = true, updatable = false, temp = true)
	private Long stationId;
	/**
	 * 对应上级账号
	 */
	@Column(name = "agent_name", insertable = false, nullable = true, updatable = false, temp = true)
	private String agent_name;
	/**
	 * 对应动态赔率值
	 */
	@Column(name = "dynamic_rate", insertable = false, nullable = true, updatable = false, temp = true)
	private BigDecimal dynamicRate;
	

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getGameShare() {
		return gameShare;
	}

	public void setGameShare(BigDecimal gameShare) {
		this.gameShare = gameShare;
	}

	public BigDecimal getProfitShare() {
		return profitShare;
	}

	public void setProfitShare(BigDecimal profitShare) {
		this.profitShare = profitShare;
	}

	public BigDecimal getDynamicRate() {
		return dynamicRate;
	}

	public void setDynamicRate(BigDecimal dynamicRate) {
		this.dynamicRate = dynamicRate;
	}
	
}