package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "common_redpacket_record")
public class CommonRedpacketRecord {

	// 未处理
	public static int STATUS_UNTREATED = 1;
	// 处理成功
	public static int STATUS_SUCCESS = 2;
	// 处理失败
	public static int STATUS_FAILED = 3;
	// 已取消
	public static int STATUS_CANCELED = 4;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "account")
	private String account;

	@Column(name = "money")
	private BigDecimal money;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "redpacket_id")
	private Long redpacketId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "status")
	private Integer status;

	@Column(name = "remark")
	private String remark;
	@Column(name = "redpacket_name")
	private String redpacketName;
	
	@Column(name = "im_user_id")
	private String imUserId;
	
	@Column(name = "im_chat_id")
	private String imChatId;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getMoney() {
		return this.money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getRedpacketId() {
		return redpacketId;
	}

	public void setRedpacketId(Long redpacketId) {
		this.redpacketId = redpacketId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRedpacketName() {
		return redpacketName;
	}

	public void setRedpacketName(String redpacketName) {
		this.redpacketName = redpacketName;
	}

	public String getImUserId() {
		return imUserId;
	}

	public void setImUserId(String imUserId) {
		this.imUserId = imUserId;
	}

	public String getImChatId() {
		return imChatId;
	}

	public void setImChatId(String imChatId) {
		this.imChatId = imChatId;
	}
	
}
