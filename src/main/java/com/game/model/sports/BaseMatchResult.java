package com.game.model.sports;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.ParentModel;

import com.alibaba.fastjson.annotation.JSONField;

@ParentModel
public class BaseMatchResult {
	
	
	@JSONField(name="scoreH1H")
	@Column(name="score_h1_h",length=30)
	private String scoreH1H;
	
	@JSONField(name="scoreH1G")
	@Column(name="score_h1_g",length=30)
	private String scoreH1G;
	
	@JSONField(name="scoreH2H")
	@Column(name="score_h2_h",length=30)
	private String scoreH2H;
	
	@JSONField(name="scoreH2G")
	@Column(name="score_h2_g",length=30)
	private String scoreH2G;
	
	@JSONField(name="scoreAddH")
	@Column(name="score_add_h",length=30)
	private String scoreAddH;
	
	@JSONField(name="scoreAddG")
	@Column(name="score_add_g",length=30)
	private String scoreAddG;
	
	@JSONField(name="scoreTime1H")
	@Column(name="score_time1_h",length=30)
	private String scoreTime1H;
	
	@JSONField(name="scoreTime1G")
	@Column(name="score_time1_g",length=30)
	private String scoreTime1G;
	
	@JSONField(name="scoreTime2H")
	@Column(name="score_time2_h",length=30)
	private String scoreTime2H;
	
	@JSONField(name="scoreTime2G")
	@Column(name="score_time2_g",length=30)
	private String scoreTime2G;
	
	@JSONField(name="scoreTime3H")
	@Column(name="score_time3_h",length=30)
	private String scoreTime3H;
	
	@JSONField(name="scoreTime3G")
	@Column(name="score_time3_g",length=30)
	private String scoreTime3G;
	
	@JSONField(name="scoreTime4H")
	@Column(name="score_time4_h",length=30)
	private String scoreTime4H;
	
	@JSONField(name="scoreTime4G")
	@Column(name="score_time4_g",length=30)
	private String scoreTime4G;
	
	@JSONField(name="scoreTime5H")
	@Column(name="score_time5_h",length=30)
	private String scoreTime5H;
	
	@JSONField(name="scoreTime5G")
	@Column(name="score_time5_g",length=30)
	private String scoreTime5G;
	
	@JSONField(name="scoreTime6H")
	@Column(name="score_time6_h",length=30)
	private String scoreTime6H;
	
	@JSONField(name="scoreTime6G")
	@Column(name="score_time6_g",length=30)
	private String scoreTime6G;
	
	@JSONField(name="scoreTime7H")
	@Column(name="score_time7_h",length=30)
	private String scoreTime7H;
	
	@JSONField(name="scoreTime7G")
	@Column(name="score_time7_g",length=30)
	private String scoreTime7G;
	
	@JSONField(name="scoreFullH")
	@Column(name="score_full_h",length=30)
	private String scoreFullH;
	
	@JSONField(name="scoreFullG")
	@Column(name="score_full_g",length=30)
	private String scoreFullG;
	
	
	public String getScoreH1H(){
		return this.scoreH1H;
	}
	
	public void setScoreH1H(String scoreH1H){
		this.scoreH1H = scoreH1H;
	}

	public String getScoreH1G(){
		return this.scoreH1G;
	}
	
	public void setScoreH1G(String scoreH1G){
		this.scoreH1G = scoreH1G;
	}

	public String getScoreH2H(){
		return this.scoreH2H;
	}
	
	public void setScoreH2H(String scoreH2H){
		this.scoreH2H = scoreH2H;
	}

	public String getScoreH2G(){
		return this.scoreH2G;
	}
	
	public void setScoreH2G(String scoreH2G){
		this.scoreH2G = scoreH2G;
	}

	public String getScoreAddH(){
		return this.scoreAddH;
	}
	
	public void setScoreAddH(String scoreAddH){
		this.scoreAddH = scoreAddH;
	}

	public String getScoreAddG(){
		return this.scoreAddG;
	}
	
	public void setScoreAddG(String scoreAddG){
		this.scoreAddG = scoreAddG;
	}

	public String getScoreTime1H(){
		return this.scoreTime1H;
	}
	
	public void setScoreTime1H(String scoreTime1H){
		this.scoreTime1H = scoreTime1H;
	}

	public String getScoreTime1G(){
		return this.scoreTime1G;
	}
	
	public void setScoreTime1G(String scoreTime1G){
		this.scoreTime1G = scoreTime1G;
	}

	public String getScoreTime2H(){
		return this.scoreTime2H;
	}
	
	public void setScoreTime2H(String scoreTime2H){
		this.scoreTime2H = scoreTime2H;
	}

	public String getScoreTime2G(){
		return this.scoreTime2G;
	}
	
	public void setScoreTime2G(String scoreTime2G){
		this.scoreTime2G = scoreTime2G;
	}

	public String getScoreTime3H(){
		return this.scoreTime3H;
	}
	
	public void setScoreTime3H(String scoreTime3H){
		this.scoreTime3H = scoreTime3H;
	}

	public String getScoreTime3G(){
		return this.scoreTime3G;
	}
	
	public void setScoreTime3G(String scoreTime3G){
		this.scoreTime3G = scoreTime3G;
	}

	public String getScoreTime4H(){
		return this.scoreTime4H;
	}
	
	public void setScoreTime4H(String scoreTime4H){
		this.scoreTime4H = scoreTime4H;
	}

	public String getScoreTime4G(){
		return this.scoreTime4G;
	}
	
	public void setScoreTime4G(String scoreTime4G){
		this.scoreTime4G = scoreTime4G;
	}

	public String getScoreTime5H(){
		return this.scoreTime5H;
	}
	
	public void setScoreTime5H(String scoreTime5H){
		this.scoreTime5H = scoreTime5H;
	}

	public String getScoreTime5G(){
		return this.scoreTime5G;
	}
	
	public void setScoreTime5G(String scoreTime5G){
		this.scoreTime5G = scoreTime5G;
	}

	public String getScoreTime6H(){
		return this.scoreTime6H;
	}
	
	public void setScoreTime6H(String scoreTime6H){
		this.scoreTime6H = scoreTime6H;
	}

	public String getScoreTime6G(){
		return this.scoreTime6G;
	}
	
	public void setScoreTime6G(String scoreTime6G){
		this.scoreTime6G = scoreTime6G;
	}

	public String getScoreTime7H(){
		return this.scoreTime7H;
	}
	
	public void setScoreTime7H(String scoreTime7H){
		this.scoreTime7H = scoreTime7H;
	}

	public String getScoreTime7G(){
		return this.scoreTime7G;
	}
	
	public void setScoreTime7G(String scoreTime7G){
		this.scoreTime7G = scoreTime7G;
	}

	public String getScoreFullH(){
		return this.scoreFullH;
	}
	
	public void setScoreFullH(String scoreFullH){
		this.scoreFullH = scoreFullH;
	}

	public String getScoreFullG(){
		return this.scoreFullG;
	}
	
	public void setScoreFullG(String scoreFullG){
		this.scoreFullG = scoreFullG;
	}
}
