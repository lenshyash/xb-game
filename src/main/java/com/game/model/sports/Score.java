package com.game.model.sports;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.ParentModel;
import org.jay.frame.jdbc.annotation.UpdateValue;


@ParentModel
public class Score {
	
	public static long SCORE_NULL = -1L;//无数据
	
	/**
	 *  生成规则 ：
	 *  	sport_type  +   match_id  * 10  
	 *  	
	 *  注意事项： sport_type 定义要 0 - 9之间
	 */
	@Column(name="id",primarykey=true,generator=Column.PK_BY_HAND)
	private Long id;
	
	@Column(name="match_id")
	private Long matchId;
	
	@Column(name="sport_type")
	private Long sportType;
	
	@Column(name="game_status")
	private Long gameStatus;

	@UpdateValue(generator=UpdateValue.GENERATOR_NEW_DATE)
	@Column(name="modify_datetime")
	private Date modifyDatetime;
	
	@InsertValue(generator=InsertValue.GENERATOR_NEW_DATE)
	@Column(name="create_datetime")
	private Date createDatetime;
	
	@Column(name="home_team")
	private String homeTeam;
	
	@Column(name="guest_team")
	private String guestTeam;
	
	@Column(name="plan_start_time")
	private Date planStartTime;
	
	@Column(name="league")
	private String league;
	
	
	
	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getGuestTeam() {
		return guestTeam;
	}

	public void setGuestTeam(String guestTeam) {
		this.guestTeam = guestTeam;
	}

	public Date getPlanStartTime() {
		return planStartTime;
	}

	public void setPlanStartTime(Date planStartTime) {
		this.planStartTime = planStartTime;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getMatchId() {
		return matchId;
	}
	
	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getSportType() {
		return sportType;
	}

	public void setSportType(Long sportType) {
		this.sportType = sportType;
	}
	
	public void setSportType(SportType sportType) {
		this.sportType = new Long(sportType.getType());
	}
	
	public Long getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(Long gameStatus) {
		this.gameStatus = gameStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
