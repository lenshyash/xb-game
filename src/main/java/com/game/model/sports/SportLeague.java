package com.game.model.sports;


import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name="sport_league")
public class SportLeague  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		联赛名称
	*/
	@Column(name="name",length=500)
	private String name;
	
	/**
		创建时间
	*/
	@Column(name="create_datetime")
	private Date createDatetime;
	
	/**
		球类 参加sport_type表
	*/
	@Column(name="sport_type_id")
	private Long sportTypeId;
	


	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public Date getCreateDatetime(){
		return this.createDatetime;
	}
	
	public void setCreateDatetime(Date createDatetime){
		this.createDatetime = createDatetime;
	}

	public Long getSportTypeId(){
		return this.sportTypeId;
	}
	
	public void setSportTypeId(Long sportTypeId){
		this.sportTypeId = sportTypeId;
	}
}
