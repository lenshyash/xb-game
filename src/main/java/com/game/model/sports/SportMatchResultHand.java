package com.game.model.sports;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="sport_match_result_hand")
public class SportMatchResultHand extends BaseMatchResult {
	
	/**
	 * 未结算
	 */
	public static final long BALANCE_UNDO = 1L;

	/**
	 * 已结算 
	 */
	public static final long BALANCE_DONE = 2L;
	
	/**
	 * 正常
	 */
	public static final long STATUS_NORMAL = 1L;
	/**
	 * 赛事腰斩
	 */
	public static final long STATUS_CUT_GAME = 2L;
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="match_id")
	private Long matchId;
	
	/**
		总控增加的赛果 station_id 为0
	*/
	@Column(name="station_id")
	private Long stationId;
	
	/**
		1：未结算 2：已结算
	*/
	@Column(name="balance")
	private Long balance;

	@Column(name="status")
	private Long status;
	
	@Column(name="status_remark")
	private String statusRemark;
	
	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getStatusRemark() {
		return statusRemark;
	}

	public void setStatusRemark(String statusRemark) {
		this.statusRemark = statusRemark;
	}

	public Long getMatchId(){
		return this.matchId;
	}
	
	public void setMatchId(Long matchId){
		this.matchId = matchId;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}


	public Long getBalance(){
		return this.balance;
	}
	
	public void setBalance(Long balance){
		this.balance = balance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
