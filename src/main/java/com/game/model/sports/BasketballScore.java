package com.game.model.sports;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bfw_match_result")
public class BasketballScore extends Score{
	
	
	//	state[0] = "<b>推迟</b>,<b>推迟</b>,<b>Postponed</b>";
	//	state[1] = "<b>取消</b>,<b>取消</b>,<b>Cancel</b>";
	//	state[2] = "<b>中断</b>,<b>中断</b>,<b>Pause</b>";
	//	state[3] = "<b>待定</b>,<b>待定</b>,<b>Undecided</b>";
	//	state[4] = "<b>完</b>,<b>完</b>,<b>FT</b>";
	//	state[5] = "未开场,未开場,";
	//	state[6] = "第1节,第1節,1st";
	//	state[7] = "第2节,第2節,2nd";
	//	state[8] = "第3节,第3節,3rd";
	//	state[9] = "第4节,第4節,4th";
	//	state[10] = "第1'OT,第1'OT,1'OT";
	//	state[11] = "第2'OT,第2'OT,2'OT";
	//	state[12] = "第3'OT,第3'OT,3'OT";
	//	state[15] = "上半场,上半場,1st";
	//	state[16] = "下半场,下半場,2nd";
	//	state[17] = "下半场,下半場,2nd";
	//	state[55] = "中场,中場,Half";
	
	public static final long GAME_STATUS_POSTPONED = 0;
	public static final long GAME_STATUS_CANCEL = 1;
	public static final long GAME_STATUS_PAUSE = 2;
	public static final long GAME_STATUS_UNDECIDED = 3;
	public static final long GAME_STATUS_FINISH = 4;
	public static final long GAME_STATUS_NO_START = 5;
	public static final long GAME_STATUS_PART_1 = 6;
	public static final long GAME_STATUS_PART_2 = 7;
	public static final long GAME_STATUS_PART_3 = 8;
	public static final long GAME_STATUS_PART_4 = 9;
	public static final long GAME_STATUS_ADD_1 = 10;
	public static final long GAME_STATUS_ADD_2 = 11;
	public static final long GAME_STATUS_ADD_3 = 12;
	public static final long GAME_STATUS_1ST = 15;
	public static final long GAME_STATUS_2ND = 16;
	public static final long GAME_STATUS_2ND_SAME = 17;
	public static final long GAME_STATUS_MIDDLE = 55;
	
	//第一节
	@Column(name="hs_1")
	private Long h1;
	@Column(name="gs_1")
	private Long g1;
	
	//第二节
	@Column(name="hs_2")
	private Long h2;
	@Column(name="gs_2")
	private Long g2;
	
	//第三节
	@Column(name="hs_3")
	private Long h3;
	@Column(name="gs_3")
	private Long g3;
	
	//第四节
	@Column(name="hs_4")
	private Long h4;
	@Column(name="gs_4")
	private Long g4;
	
	//全场
	@Column(name="hs_5")
	private Long hscore;
	@Column(name="gs_5")
	private Long gscore;

	//加时1
	@Column(name="hs_6")
	private Long addH1;
	@Column(name="gs_6")
	private Long addG1;
	
	//加时2
	@Column(name="hs_7")
	private Long addH2;
	@Column(name="gs_7")
	private Long addG2;
	
	//加时3
	@Column(name="hs_8")
	private Long addH3;
	@Column(name="gs_8")
	private Long addG3;
	
	//篮球有的比赛只有 两节  
	//两节比分分别存储于 第一节、跟第三节  
	@Column(name="int_field_1")
	private Long partCount;

	public Long getH1() {
		return h1;
	}

	public void setH1(Long h1) {
		this.h1 = h1;
	}

	public Long getG1() {
		return g1;
	}

	public void setG1(Long g1) {
		this.g1 = g1;
	}

	public Long getH2() {
		return h2;
	}

	public void setH2(Long h2) {
		this.h2 = h2;
	}

	public Long getG2() {
		return g2;
	}

	public void setG2(Long g2) {
		this.g2 = g2;
	}

	public Long getH3() {
		return h3;
	}

	public void setH3(Long h3) {
		this.h3 = h3;
	}

	public Long getG3() {
		return g3;
	}

	public void setG3(Long g3) {
		this.g3 = g3;
	}

	public Long getH4() {
		return h4;
	}

	public void setH4(Long h4) {
		this.h4 = h4;
	}

	public Long getG4() {
		return g4;
	}

	public void setG4(Long g4) {
		this.g4 = g4;
	}

	public Long getAddH1() {
		return addH1;
	}

	public void setAddH1(Long addH1) {
		this.addH1 = addH1;
	}

	public Long getAddG1() {
		return addG1;
	}

	public void setAddG1(Long addG1) {
		this.addG1 = addG1;
	}

	public Long getAddH2() {
		return addH2;
	}

	public void setAddH2(Long addH2) {
		this.addH2 = addH2;
	}

	public Long getAddG2() {
		return addG2;
	}

	public void setAddG2(Long addG2) {
		this.addG2 = addG2;
	}

	public Long getAddH3() {
		return addH3;
	}

	public void setAddH3(Long addH3) {
		this.addH3 = addH3;
	}

	public Long getAddG3() {
		return addG3;
	}

	public void setAddG3(Long addG3) {
		this.addG3 = addG3;
	}

	public Long getPartCount() {
		return partCount;
	}

	public void setPartCount(Long partCount) {
		this.partCount = partCount;
	}
	
	public Long getHscore() {
		return hscore;
	}

	public void setHscore(Long hscore) {
		this.hscore = hscore;
	}

	public Long getGscore() {
		return gscore;
	}

	public void setGscore(Long gscore) {
		this.gscore = gscore;
	}
}
