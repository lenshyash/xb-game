package com.game.model.sports;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bfw_match_result")
public class FootballScore extends Score{
	
	/**
	 * 
	 	0:"推迟,推遲,Defer"
		1:"中断,中斷,Halt"
		2:"腰斩,腰斬,Halt"
		3:"<font color=green>待定</font>,<font color=green>待定</font>,<font color=green>Wait</font>"
		4:"取消,取消,Cancel"
		13:"<b>完</b>,<b>完</b>,<b>Ft</b>"
		14:",," 待开赛
		15:"上,上,Part1"
		16:"<font color=blue>中</font>,<font color=blue>中</font>,<font color=blue>Half</font>"
		17:"下,下,Part2"
		18:"加,加,Ot"
	 */
	
	public static final long GAME_STATUS_DEFER = 0;
	public static final long GAME_STATUS_HALT = 1;
	public static final long GAME_STATUS_CUT = 2;
	public static final long GAME_STATUS_UNDECIDED = 3;
	public static final long GAME_STATUS_CANCEL = 4;
	
	public static final long GAME_STATUS_FINISH = 13;
	public static final long GAME_STATUS_NO_START = 14;
	public static final long GAME_STATUS_PART_1 = 15;
	public static final long GAME_STATUS_MID = 16;
	public static final long GAME_STATUS_PART_2 = 17;
	public static final long GAME_STATUS_ADD_TIME = 18;
	
	//包含角球数据
	public static final long HAS_FOOT_TRUE = 2;
	//不包含角球数据
	public static final long HAS_FOOT_FALSE = 1;
	
	//上半场得分
	@Column(name="hs_1")
	private Long hh1Score;
	@Column(name="gs_1")
	private Long gg1Score;
	
	@Column(name="hs_2")
	private Long hh1Foot;
	@Column(name="gs_2")
	private Long gg1Foot;
	
	@Column(name="hs_3")
	private Long hh1Red;
	@Column(name="gs_3")
	private Long gg1Red;
	
	@Column(name="hs_4")
	private Long hh1Yellow;
	@Column(name="gs_4")
	private Long gg1Yellow;
	
	//全场 (不包含加时)
	@Column(name="hs_5")
	private Long hhScore;
	@Column(name="gs_5")
	private Long ggScore;
	
	@Column(name="hs_6")
	private Long hhFoot;
	@Column(name="gs_6")
	private Long ggFoot;
	
	@Column(name="hs_7")
	private Long hhRed;
	@Column(name="gs_7")
	private Long ggRed;
	
	@Column(name="hs_8")
	private Long hhYellow;
	@Column(name="gs_8")
	private Long ggYellow;
	
	@Column(name="int_field_1")
	private Long hasFoot;
	
	
	public Long getHasFoot() {
		return hasFoot;
	}

	public void setHasFoot(Long hasFoot) {
		this.hasFoot = hasFoot;
	}

	public Long getHh1Score() {
		return hh1Score;
	}

	public void setHh1Score(Long hh1Score) {
		this.hh1Score = hh1Score;
	}

	public Long getGg1Score() {
		return gg1Score;
	}

	public void setGg1Score(Long gg1Score) {
		this.gg1Score = gg1Score;
	}

	public Long getHh1Foot() {
		return hh1Foot;
	}

	public void setHh1Foot(Long hh1Foot) {
		this.hh1Foot = hh1Foot;
	}

	public Long getGg1Foot() {
		return gg1Foot;
	}

	public void setGg1Foot(Long gg1Foot) {
		this.gg1Foot = gg1Foot;
	}

	public Long getHh1Red() {
		return hh1Red;
	}

	public void setHh1Red(Long hh1Red) {
		this.hh1Red = hh1Red;
	}

	public Long getGg1Red() {
		return gg1Red;
	}

	public void setGg1Red(Long gg1Red) {
		this.gg1Red = gg1Red;
	}

	public Long getHh1Yellow() {
		return hh1Yellow;
	}

	public void setHh1Yellow(Long hh1Yellow) {
		this.hh1Yellow = hh1Yellow;
	}

	public Long getGg1Yellow() {
		return gg1Yellow;
	}

	public void setGg1Yellow(Long gg1Yellow) {
		this.gg1Yellow = gg1Yellow;
	}

	public Long getHhScore() {
		return hhScore;
	}

	public void setHhScore(Long hhScore) {
		this.hhScore = hhScore;
	}

	public Long getGgScore() {
		return ggScore;
	}

	public void setGgScore(Long ggScore) {
		this.ggScore = ggScore;
	}

	public Long getHhFoot() {
		return hhFoot;
	}

	public void setHhFoot(Long hhFoot) {
		this.hhFoot = hhFoot;
	}

	public Long getGgFoot() {
		return ggFoot;
	}

	public void setGgFoot(Long ggFoot) {
		this.ggFoot = ggFoot;
	}

	public Long getHhRed() {
		return hhRed;
	}

	public void setHhRed(Long hhRed) {
		this.hhRed = hhRed;
	}

	public Long getGgRed() {
		return ggRed;
	}

	public void setGgRed(Long ggRed) {
		this.ggRed = ggRed;
	}

	public Long getHhYellow() {
		return hhYellow;
	}

	public void setHhYellow(Long hhYellow) {
		this.hhYellow = hhYellow;
	}

	public Long getGgYellow() {
		return ggYellow;
	}

	public void setGgYellow(Long ggYellow) {
		this.ggYellow = ggYellow;
	}
}