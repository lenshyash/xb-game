package com.game.model.sports;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Table(name="sport_betting")
public class SportBettingOrder  {

	//待确认 
	public final static long BETTING_STATUS_UNCONFIRMED = 1L;
	//已确认  
	public final static long BETTING_STATUS_CONFIRM = 2L;
	//系统自动取消
	public final static long BETTING_STATUS_SYS_REJECT = 3L;
	//手动取消
	public final static long BETTING_STATUS_REJECT = 4L;
	
	//未结算
	public final static long BALANCE_UNDO = 1;
	//系统结算
	public final static long BALANCE_DONE = 2;
	
	//结算失败
	public final static long BALANCE_ERROR = 3;
	//比赛腰斩
	public final static long BALANCE_CUT_GAME = 4;
	//租户 手动结算
	public final static long BALANCE_AGENT_HAND_DONE = 5;
	//比分网结算
	public final static long BALANCE_BFW_DONE = 6;
	
	//单注
	public final static long MIX_SINGLE = 1;
	//混合过关 主单
	public final static long MIX_MAIN = 2;
	//混合过关 子单
	public final static long MIX_CHILD = 3;
	
	//确认状态 系统确认
	public final static long HANDLER_TYPE_SYS = 1;
	//确认状态 人工确认
	public final static long HANDLER_TYPE_NORMAL = 2;
	
	/**
	 * 注单锁
	 */
//	//未加锁
//	public final static long ORDER_LOCK_UNLOCK = 1L;
//	
//	//系统结算锁
//	public final static long ORDER_LOCK_SYSTEM = 2L;
//	
//	//手动订单锁
//	public final static long ORDER_LOCK_SYSTEM
	
	/**
		1全输 2输一半 3平 4赢一半 5全赢
	*/
	public final static long RESULT_STATUS_LOST = 1;
	public final static long RESULT_STATUS_LOST_HALF = 2;
	public final static long RESULT_STATUS_DRAW = 3;
	public final static long RESULT_STATUS_WIN_HALF = 4;
	public final static long RESULT_STATUS_WIN = 5;
	
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		sport_type 表
	*/
	@Column(name="sport_type")
	private Long sportType;
	
	/**
		1:滚球   2:今日  3:早盘
	*/
	@Column(name="game_time_type")
	private Long gameTimeType;
	
	/**
		1:独赢 ＆ 让球 ＆ 大小 & 单 / 双    
		2：波胆  上半场 
		3：波胆  全场 
		4：总入球数 
		5：全场半场输赢 
		6：混合过关      （参照枚举类DataType）
	*/
	@Column(name="data_type")
	private Long dataType;
	
	/**
		1:全场独赢  
		2:全场大小球 
		3:全场让球盘 
		4：全场得分单双  
		5：全场波胆  
		6：半场波胆 
		7：总入球 
		8：半场 全场 胜负关系 
		9：主队全场分数大小 
		10：客队全场分数大小
	*/
	@Column(name="bet_type")
	private Long betType;
	
	/**
		1：主胜 
		2：主负 
		3：平  
		4：总得分单 
		5：总得分双 
		6：总得分大于 
		7：总得分小于 
		8：让球主队赢 
		9：让球主队输 
		10：波胆具体比分 
		11:波胆其他比分 
		12：总入球数 具体分数 
		13：全场 半场胜负关系
	*/
	@Column(name="bet_item_type")
	private Long betItemType;
	
	@Column(name="project",length=50)
	private String project;
	
	/**
		滚球时使用，主队分数
	*/
	@Column(name="score_h")
	private Long scoreH;
	
	/**
		滚球时使用，客队分数
	*/
	@Column(name="score_c")
	private Long scoreC;
	
	/**
		赛事id
	*/
	@Column(name="match_id")
	private Long matchId;
	
	/**
		主队
	*/
	@Column(name="home_team",length=100)
	private String homeTeam;
	
	/**
		客队名称
	*/
	@Column(name="guest_team",length=100)
	private String guestTeam;
	
	/**
		联赛id
	*/
	@Column(name="league_id")
	private Long leagueId;
	
	/**
		联赛
	*/
	@Column(name="league",length=100)
	private String league;
	
	/**
		H:亚洲盘  I:印尼盘 E:欧洲盘 M:马来西亚盘 
	*/
	@Column(name="plate",length=2)
	private String plate;
	
	@Column(name="item_key",length=20)
	private String itemKey;
	
	@Column(name="game_key",length=20)
	private String gameKey;
	
	/**
		混合过关的时候  子单
	*/
	@Column(name="parent_id")
	private Long parentId;
	
	/**
		用于前端显示
	*/
	@Column(name="remark",length=200)
	private String remark;
	
	/**
		赔率
	*/
	@Column(name="odds")
	private BigDecimal odds;
	
	/**
		1 待确认  
		2：已确认  
		3：已取消 (滚球系统自动取消)  
		4: 手动取消
	*/
	@Column(name="betting_status")
	private Long bettingStatus;
	
	
	
	/**
		1：未结算 2：已结算
	*/
	@Column(name="balance")
	private Long balance;
	
	/**
		下注时间
	*/
	@Column(name="betting_date")
	private Date bettingDate;
	
	/**
		投注结果
	*/
	@Column(name="betting_result")
	private BigDecimal bettingResult;
	
	/**
		结算时间
	*/
	@Column(name="account_datetime")
	private Date accountDatetime;
	
	/**
	 * 皇冠赛事ID 
	 */
	@Column(name="gid")
	private Long gid;
	
	/**
		1全输 2输一半 3平 4赢一半 5全赢
	*/
	@Column(name="result_status")
	private Long resultStatus;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="member_id")
	private Long memberId;
	
	/**
	 * 注单编码
	 */
	@Column(name="betting_code")
	private String bettingCode;
	
	/**
	 * 投注金额
	 */
	@Column(name="betting_money")
	private BigDecimal bettingMoney;
	
	/**
	 * 注单状态说明，取消注单时写入取消原因
	 */
	@Column(name="status_remark")
	private String statusRemark;
	
	/**
	 *  1:单注  2：混合过关 主单  3：混合过关子单
	 */
	@Column(name="mix")
	private Long mix;
	
	@Column(name="member_name")
	private String memberName;
	
	@Column(name="station_name")
	private String stationName;
	
	/**
	 * 类别名称
	 */
	@Column(name="type_names")
	private String typeNames;
	
	
	/**
	 * 赛果，结算的时候回填  只用于页面显示使用
	 */
	@Column(name="result")
	private String result;
	
	/**
	 * 数据乐观锁
	 */
	@Column(name="data_version")
	private Long dataVersion;
	
	/**
	 * 会员返水状态 （1，还未返水 2，已经返水,还未到账 3，返水已经回滚 ,4 反水已经到账 ）多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
	 */
	@Column(name="roll_back_status")
	private Integer rollBackStatus;
	
	private BigDecimal rollBackMoney;
	
	
	public BigDecimal getRollBackMoney() {
		return rollBackMoney;
	}

	public void setRollBackMoney(BigDecimal rollBackMoney) {
		this.rollBackMoney = rollBackMoney;
	}

	public Long getDataVersion() {
		return dataVersion;
	}

	public void setDataVersion(Long dataVersion) {
		this.dataVersion = dataVersion;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public String getTypeNames() {
		return typeNames;
	}

	public void setTypeNames(String typeNames) {
		this.typeNames = typeNames;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public Long getMix() {
		return mix;
	}

	public void setMix(Long mix) {
		this.mix = mix;
	}

	public BigDecimal getBettingMoney() {
		return bettingMoney;
	}

	public void setBettingMoney(BigDecimal bettingMoney) {
		this.bettingMoney = bettingMoney;
	}

	public String getBettingCode() {
		return bettingCode;
	}

	public void setBettingCode(String bettingCode) {
		this.bettingCode = bettingCode;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getSportType(){
		return this.sportType;
	}
	
	public void setSportType(Long sportType){
		this.sportType = sportType;
	}

	public Long getGameTimeType(){
		return this.gameTimeType;
	}
	
	public void setGameTimeType(Long gameTimeType){
		this.gameTimeType = gameTimeType;
	}

	public Long getDataType(){
		return this.dataType;
	}
	
	public void setDataType(Long dataType){
		this.dataType = dataType;
	}

	public Long getBetType(){
		return this.betType;
	}
	
	public void setBetType(Long betType){
		this.betType = betType;
	}

	public Long getBetItemType(){
		return this.betItemType;
	}
	
	public void setBetItemType(Long betItemType){
		this.betItemType = betItemType;
	}

	public String getProject(){
		return this.project;
	}
	
	public void setProject(String project){
		this.project = project;
	}

	public Long getScoreH(){
		return this.scoreH;
	}
	
	public void setScoreH(Long scoreH){
		this.scoreH = scoreH;
	}

	public Long getScoreC(){
		return this.scoreC;
	}
	
	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public void setScoreC(Long scoreC){
		this.scoreC = scoreC;
	}

	public Long getMatchId(){
		return this.matchId;
	}
	
	public void setMatchId(Long matchId){
		this.matchId = matchId;
	}

	public String getStatusRemark() {
		return statusRemark;
	}

	public void setStatusRemark(String statusRemark) {
		this.statusRemark = statusRemark;
	}

	public String getHomeTeam(){
		return this.homeTeam;
	}
	
	public void setHomeTeam(String homeTeam){
		this.homeTeam = homeTeam;
	}

	public String getGuestTeam(){
		return this.guestTeam;
	}
	
	public void setGuestTeam(String guestTeam){
		this.guestTeam = guestTeam;
	}

	public Long getLeagueId(){
		return this.leagueId;
	}
	
	public void setLeagueId(Long leagueId){
		this.leagueId = leagueId;
	}

	public String getLeague(){
		return this.league;
	}
	
	public void setLeague(String league){
		this.league = league;
	}

	public String getPlate(){
		return this.plate;
	}
	
	public void setPlate(String plate){
		this.plate = plate;
	}

	public String getItemKey(){
		return this.itemKey;
	}
	
	public void setItemKey(String itemKey){
		this.itemKey = itemKey;
	}

	public String getGameKey(){
		return this.gameKey;
	}
	
	public void setGameKey(String gameKey){
		this.gameKey = gameKey;
	}

	public Long getParentId(){
		return this.parentId;
	}
	
	public void setParentId(Long parentId){
		this.parentId = parentId;
	}

	public String getRemark(){
		return this.remark;
	}
	
	public void setRemark(String remark){
		this.remark = remark;
	}

	public BigDecimal getOdds(){
		return this.odds;
	}
	
	public void setOdds(BigDecimal odds){
		this.odds = odds;
	}

	public Long getBettingStatus(){
		return this.bettingStatus;
	}
	
	public void setBettingStatus(Long bettingStatus){
		this.bettingStatus = bettingStatus;
	}

	public Date getBettingDate(){
		return this.bettingDate;
	}
	
	public void setBettingDate(Date bettingDate){
		this.bettingDate = bettingDate;
	}

	public BigDecimal getBettingResult(){
		return this.bettingResult;
	}
	
	public void setBettingResult(BigDecimal bettingResult){
		this.bettingResult = bettingResult;
	}

	public Date getAccountDatetime(){
		return this.accountDatetime;
	}
	
	public void setAccountDatetime(Date accountDatetime){
		this.accountDatetime = accountDatetime;
	}

	public Long getResultStatus(){
		return this.resultStatus;
	}
	
	public void setResultStatus(Long resultStatus){
		this.resultStatus = resultStatus;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public Long getMemberId(){
		return this.memberId;
	}
	
	public void setMemberId(Long memberId){
		this.memberId = memberId;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public Integer getRollBackStatus() {
		return rollBackStatus;
	}

	public void setRollBackStatus(Integer rollBackStatus) {
		this.rollBackStatus = rollBackStatus;
	}
}
