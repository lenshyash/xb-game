package com.game.model.sports;


import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import com.alibaba.fastjson.annotation.JSONField;
import com.game.util.SportUtil;

@Table(name="sport_match_result")
public class SportMatchResult extends BaseMatchResult {
	
	//未结算
	public static long BALANCE_UNDO = 1L;
	
	//已结算
	public static long BALANCE_DONE = 2L;
	
	//结算中  锁定记录  保证只有一条线程在结算
	public static long BALANCE_DOING = 3L;
	
	//结算出错
	public static long BALANCE_ERROR = 4L;
	//半场结算
	public static long BALANCE_HALF = 5L;
	
	@JSONField(serialize=false)
	@Column(name="gid",primarykey=true,generator=Column.PK_BY_HAND)
	private Long gid;
	
	@Column(name="home_team",length=50)
	private String homeTeam;
	
	@Column(name="guest_team",length=50)
	private String guestTeam;
	
	@Column(name="league",length=100)
	private String league;
	
	@Column(name="start_time")
	private Date startTime;
	
	@JSONField(serialize=false)
	@Column(name="catch_time")
	private Date catchTime;
	
	@JSONField(serialize=false)
	@Column(name="sport_type")
	private Long sportType;
	
	/**
		1：未结算  2：已经结算  3：结算中
	*/
	@JSONField(serialize=false)
	@Column(name="balance")
	private Long balance;
	
	@JSONField(serialize=false)
	@Column(name="lock_time")
	private Date lockTime;
	
	@JSONField(serialize=false)
	@Column(name="lock_mac_name")
	private String lockMacName;
	
	public Date getLockTime() {
		return lockTime;
	}

	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}

	public String getLockMacName() {
		return lockMacName;
	}

	public void setLockMacName(String lockMacName) {
		this.lockMacName = lockMacName;
	}

	public Long getGid(){
		return this.gid;
	}
	
	public void setGid(Long gid){
		this.gid = gid;
	}

	public String getHomeTeam(){
		return this.homeTeam;
	}
	
	public void setHomeTeam(String homeTeam){
		if(homeTeam != null){
			this.homeTeam = SportUtil.trim(homeTeam);
			return;
		}
		this.homeTeam = homeTeam;
	}

	public String getGuestTeam(){
		return this.guestTeam;
	}
	
	public void setGuestTeam(String guestTeam){
		if(guestTeam != null){
			this.guestTeam = SportUtil.trim(guestTeam);
			return;
		}
		this.guestTeam = guestTeam;
	}

	public String getLeague(){
		return this.league;
	}
	
	public void setLeague(String league){
		this.league = league;
	}

	public Date getStartTime(){
		return this.startTime;
	}
	
	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}

	public Date getCatchTime(){
		return this.catchTime;
	}
	
	public void setCatchTime(Date catchTime){
		this.catchTime = catchTime;
	}

	public Long getSportType(){
		return this.sportType;
	}
	
	public void setSportType(Long sportType){
		this.sportType = sportType;
	}

	public Long getBalance(){
		return this.balance;
	}
	
	public void setBalance(Long balance){
		this.balance = balance;
	}
}
