package com.game.model.sports;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="sport_betting_quota")
public class SportBettingQuota {
	
	@Column(name="member_id")
	private Long memberId;
	
	@Column(name="gid")
	private Long gid;
	
	@Column(name="stationId")
	private Long stationId;
	
	@Column(name="betting_money")
	private BigDecimal bettingMoney;
	
	@Column(name="betting_count")
	private Long bettingCount;
	
	@Column(name="create_datetime")
	private Date createDatetime;
	
	@Column(name="modify_datetime")
	private Date modifyDatetime;

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getBettingMoney() {
		return bettingMoney;
	}

	public void setBettingMoney(BigDecimal bettingMoney) {
		this.bettingMoney = bettingMoney;
	}

	public Long getBettingCount() {
		return bettingCount;
	}

	public void setBettingCount(Long bettingCount) {
		this.bettingCount = bettingCount;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}
}
