package com.game.model.sports;


import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name="sport_match")
public class SportMatch  {
	
	//全场
	public final static long MATCH_TYPE_FULL = 100;
	//上半场
	public final static long MATCH_TYPE_HALF_1 = 101; 	
	//加时赛
	public final static long MATCH_TYPE_ADD_TIME = 102; 
	
	//下班场
	public final static long MATCH_TYPE_HALF_2 = 103; 	
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		赛事皇冠投注记录ID  同一场比赛多个gid 
	*/
	@Column(name="gid")
	private Long gid;
	
	/**
		皇冠赛事ID  唯一的
	*/
	@Column(name="mid")
	private Long mid;
	
	/**
		主队名称
	*/
	@Column(name="h_team",length=100)
	private String teamH;
	
	/**
		客队名称
	*/
	@Column(name="g_team",length=100)
	private String teamC;
	
	@Column(name="league",length=100)
	private String league;
	
	/**
		数据创建时间
	*/
	@Column(name="create_datetime")
	private Date createDatetime;
	
	/**
		比赛开始时间
	*/
	@Column(name="start_time")
	private Date startTime;
	
	/**
		球类
	*/
	@Column(name="sport_type_id")
	private Long sportTypeId;
	
	/**
		赛事的唯一标识
	*/
	@Column(name="only_key",length=200)
	private String onlyKey;
	
	/**
	 * 赛事类型
	 * 1:第1节、第1局  
	 * 2：第2节、第2局   
	 * 3：第3节、第3局  ....     
	 * 100：全场   101：上半场   
	 */
	@Column(name="match_type")
	private Long matchType;

	@Column(name="match_result_gid")
	private Long matchResultGid;
	
	@Column(name="bfw_result_id")
	private Long bfwResultId;
	
	@Column(name="bfw_last_time")
	private Date bfwLastTime;
	
	@Column(name="bfw_balance_time")
	private Date bfwBalanceTime;
	
	@Column(name="bfw_only_key")
	private String bfwOnlyKey;
	

	public Date getBfwBalanceTime() {
		return bfwBalanceTime;
	}

	public void setBfwBalanceTime(Date bfwBalanceTime) {
		this.bfwBalanceTime = bfwBalanceTime;
	}

	public Long getBfwResultId() {
		return bfwResultId;
	}

	public void setBfwResultId(Long bfwResultId) {
		this.bfwResultId = bfwResultId;
	}

	public Date getBfwLastTime() {
		return bfwLastTime;
	}

	public void setBfwLastTime(Date bfwLastTime) {
		this.bfwLastTime = bfwLastTime;
	}

	public Long getMatchResultGid() {
		return matchResultGid;
	}

	public void setMatchResultGid(Long matchResultGid) {
		this.matchResultGid = matchResultGid;
	}
	
	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getGid(){
		return this.gid;
	}
	
	public void setGid(Long gid){
		this.gid = gid;
	}

	public Long getMid(){
		return this.mid;
	}
	
	public void setMid(Long mid){
		this.mid = mid;
	}

	public Date getCreateDatetime(){
		return this.createDatetime;
	}
	
	public void setCreateDatetime(Date createDatetime){
		this.createDatetime = createDatetime;
	}

	public Date getStartTime(){
		return this.startTime;
	}
	
	public void setStartTime(Date startTime){
		this.startTime = startTime;
	}

	public Long getSportTypeId(){
		return this.sportTypeId;
	}
	
	public void setSportTypeId(Long sportTypeId){
		this.sportTypeId = sportTypeId;
	}

	public String getOnlyKey(){
		return this.onlyKey;
	}
	
	public void setOnlyKey(String onlyKey){
		this.onlyKey = onlyKey;
	}

	public Long getMatchType() {
		return matchType;
	}

	public void setMatchType(Long matchType) {
		this.matchType = matchType;
	}
	
	public String getTeamH() {
		return teamH;
	}

	public void setTeamH(String teamH) {
		this.teamH = teamH;
	}

	public String getTeamC() {
		return teamC;
	}

	public void setTeamC(String teamG) {
		this.teamC = teamG;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}
	
	public String getBfwOnlyKey() {
		return bfwOnlyKey;
	}

	public void setBfwOnlyKey(String bfwOnlyKey) {
		this.bfwOnlyKey = bfwOnlyKey;
	}
}

