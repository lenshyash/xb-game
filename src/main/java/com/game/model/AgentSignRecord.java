package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name="agent_sign_record")
public class AgentSignRecord  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		会员id
	*/
	@Column(name="member_id")
	private Long memberId;
	
	/**
		站点id
	*/
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="sign_date")
	private Date signDate;
	
	/**
		签到获得积分
	*/
	@Column(name="score")
	private Long score;
	
	/**
		连续签到天数
	*/
	@Column(name="sign_days")
	private Long signDays;
	
	/**
	 * 会员名称
	 */
	@Column(name="member_name")
	private String memberName;

	
	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getMemberId(){
		return this.memberId;
	}
	
	public void setMemberId(Long memberId){
		this.memberId = memberId;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public Date getSignDate(){
		return this.signDate;
	}
	
	public void setSignDate(Date signDate){
		this.signDate = signDate;
	}

	public Long getScore(){
		return this.score;
	}
	
	public void setScore(Long score){
		this.score = score;
	}

	public Long getSignDays(){
		return this.signDays;
	}
	
	public void setSignDays(Long signDays){
		this.signDays = signDays;
	}
}
