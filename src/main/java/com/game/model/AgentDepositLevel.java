package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_deposit_level")
public class AgentDepositLevel {

	public static final int TYPE_ONLINE = 1;// 1=在线支付
	public static final int TYPE_FAST = 2;// 2=快速支付
	public static final int TYPE_BANK = 3;// 3=一般支付
	public static final int TYPE_VIRTAUL = 4;// 4=虚拟币支付

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "type")
	private Integer type;

	@Column(name = "deposit_id")
	private Long depositId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "member_level_id")
	private Long memberLevelId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getDepositId() {
		return depositId;
	}

	public void setDepositId(Long depositId) {
		this.depositId = depositId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getMemberLevelId() {
		return memberLevelId;
	}

	public void setMemberLevelId(Long memberLevelId) {
		this.memberLevelId = memberLevelId;
	}

}
