package com.game.model.platform;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

import com.alibaba.fastjson.annotation.JSONField;

@Table(name = "agent_profit_share_handler_record")
public class AgentProfitShareHandlerRecord implements Serializable {

	private static final long serialVersionUID = -6968040394576420631L;
	public static final int STATUS_NO = 1;// 未反水
	public static final int STATUS_OK = 2;// 已经反水

	@Column(name = "id", primarykey=true)
	private Long id;
	/**
	 * 对应站点
	 */
	@Column(name = "station_id")
	private Long stationId;
	@Column(name = "account_id")
	private Long accountId;
	@Column(name = "modify_datetime")
	private Date modifyDatetime;
	
	@Column(name = "create_datetime")
	private Date createDatetime;
	
	@Column(name = "account")
	private String account;

	/**
	 * 占成金额
	 */
	@InsertValue(value = "0")
	@Column(name = "money")
	private BigDecimal money;
	/**
	 * 代理占成状态 （1，未发放 2，已发放 ）
	 */
	@InsertValue(value = "1")
	@Column(name = "status")
	private Integer status;

	@Column(name = "agent_id")
	private Long agentId;

	@Column(name = "agent_name")
	private String agentName;

	@Column(name = "operator")
	private String operator;

	@Column(name = "parents")
	private String parents;
	
	@Column(name = "parent_names")
	private String parentNames;
	
	@Column(name = "remark")
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getParentNames() {
		return parentNames;
	}

	public void setParentNames(String parentNames) {
		this.parentNames = parentNames;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
