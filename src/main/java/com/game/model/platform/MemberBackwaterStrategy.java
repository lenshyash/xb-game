package com.game.model.platform;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_backwater_strategy")
public class MemberBackwaterStrategy {
	public static long STATUS_DISABLED = 1;
	public static long STATUS_ENALBED = 2;

	@Column(name = "id", primarykey = true)
	private Long id;
	// 反水类型（1、彩票，2、真人，3、电子，4、体育，5、六合特码B，6、六合正码B,11系统彩,15十分六合彩特码B,16十分六合彩正码B)
	@Column(name = "type")
	private Long type;
	// 反水比例 (0-99.99%) 例:5.5表示5.5%
	@Column(name = "rate")
	private BigDecimal rate;
	// 有效投注，大于该值为达标
	@Column(name = "qualified")
	private BigDecimal qualified;
	// 返水上线 默认0 表示无限制 单位（元）
	@Column(name = "upper_limit")
	private BigDecimal upperLimit;
	// 状态（1，禁用 2，启用）
	@Column(name = "status")
	private Long status;

	@Column(name = "memo")
	private String memo;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "station_id")
	private Long stationId;
	// 打码量倍数
	@Column(name = "multiple")
	private BigDecimal multiple;
	
	//房间号标识，仅支持v4版本
	@Column(name="room_id")
	private Long roomId;
	
	private String roomName;
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getType() {
		return this.type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getQualified() {
		return this.qualified;
	}

	public void setQualified(BigDecimal qualified) {
		this.qualified = qualified;
	}

	public BigDecimal getUpperLimit() {
		return this.upperLimit;
	}

	public void setUpperLimit(BigDecimal upperLimit) {
		this.upperLimit = upperLimit;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getMultiple() {
		return multiple;
	}

	public void setMultiple(BigDecimal multiple) {
		this.multiple = multiple;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	
	
}
