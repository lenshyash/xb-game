package com.game.model.platform;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Table(name = "member_deposit_strategy")
public class MemberDepositStrategy {

	public static int STATUS_DISABLED = 1;

	public static int STATUS_ENABLED = 2;

	public static long GIVE_TYPE_MONEY = 1;// 金额
	public static long GIVE_TYPE_RATIO = 2;// 比例

	public static long VALUE_TYPE_MONEY = 1;// 彩金
	public static long VALUE_TYPE_SCORE = 2;// 积分

	public static long DEPOSIT_COUNT_DEFAULT = 0;// 充值频率每次

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 赠送比例(0-100)%
	 */
	@Column(name = "roll_back_rate")
	private Float rollBackRate;

	/**
	 * 活动期间赠送上限
	 */
	@Column(name = "upper_limit")
	private BigDecimal upperLimit;

	/**
	 * 状态（1，禁用 2，启用）
	 */
	@Column(name = "status")
	private int status;

	/**
	 * 活动描述
	 */
	@Column(name = "memo")
	private String memo;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 活动开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "start_time")
	private Date startTime;

	/**
	 * 活动结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "end_time")
	private Date endTime;

	/**
	 * 充值类型
	 */
	@Column(name = "deposit_type")
	private Long depositType;

	/**
	 * 赠送方式
	 */
	@Column(name = "give_type")
	private Long giveType;

	/**
	 * 流水倍数
	 */
	@Column(name = "bet_multiple")
	private BigDecimal betMultiple;

	/**
	 * 充值频率
	 */
	@Column(name = "deposit_count")
	private Long depositCount;

	/**
	 * 赠送额度
	 */
	@Column(name = "give_value")
	private BigDecimal giveValue;

	/**
	 * sys_station表 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 赠送类型
	 */
	@Column(name = "value_type")
	private Long valueType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getRollBackRate() {
		return rollBackRate;
	}

	public void setRollBackRate(Float rollBackRate) {
		this.rollBackRate = rollBackRate;
	}

	public BigDecimal getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(BigDecimal upperLimit) {
		this.upperLimit = upperLimit;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getDepositType() {
		return depositType;
	}

	public void setDepositType(Long depositType) {
		this.depositType = depositType;
	}

	public Long getGiveType() {
		return giveType;
	}

	public void setGiveType(Long giveType) {
		this.giveType = giveType;
	}

	public Long getDepositCount() {
		return depositCount;
	}

	public void setDepositCount(Long depositCount) {
		this.depositCount = depositCount;
	}

	public BigDecimal getBetMultiple() {
		return betMultiple;
	}

	public void setBetMultiple(BigDecimal betMultiple) {
		this.betMultiple = betMultiple;
	}

	public BigDecimal getGiveValue() {
		return giveValue;
	}

	public void setGiveValue(BigDecimal giveValue) {
		this.giveValue = giveValue;
	}

	public Long getValueType() {
		return valueType;
	}

	public void setValueType(Long valueType) {
		this.valueType = valueType;
	}
}
