package com.game.model.platform;


import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="proxy_multi_rebate_record")
public class ProxyMultiRebateRecord  {
	
	public static Integer REBATE_TYPE_1 = 1;//多级代理返点
	
	public static Integer REBATE_TYPE_2 = 2;//动态赔率返点
	
	public static Integer REBATE_TYPE_3 = 3;//固定百分比返点
	
	
	@Column(name="id",temp = true)
	private Long id;
	/**
	 * 代理账号
	 */
	@Column(name="account",length=64)
	private String account;
	/**
	 * 下级账号
	 */
	@Column(name="child",length=64)
	private String child;
	/**
	 * 投注id
	 */
	@Column(name="bet_id")
	private Long betId;
	/**
	 * 投注金额
	 */
	@Column(name="bet_money")
	private BigDecimal betMoney;
	/**
	 * 返点金额
	 */
	@Column(name="rebate_money")
	private BigDecimal rebateMoney;
	/**
	 * 下级返点点数
	 */
	@Column(name="child_rebate_rate")
	private Float childRebateRate;
	/**
	 * 自身返点点数
	 */
	@Column(name="self_rebate_rate")
	private Float selfRebateRate;
	/**
	 * 创建时间
	 */
	@Column(name="create_datetime")
	private Date createDatetime;
	/**
	 * 修改时间
	 */
	@Column(name="modify_datetime")
	private Date modifyDatetime;
	/**
	 * 站点id
	 */
	@Column(name="station_id")
	private Long stationId;
	@Column(  name = "bet_order_id")
	private String betOrderId;
	@Column(name="bet_order_datetime")
	private Date betOrderDatetime;
	/**
	 * 反钱类型 1多级代理返点 2动态赔率返点
	 */
	@Column(name="rebate_type")
	private Integer rebateType;
	
	@Column(updatable=false,insertable=false,nullable=true, name = "domain")
	private String domain;
	
	
	public String getBetOrderId() {
		return betOrderId;
	}

	public void setBetOrderId(String betOrderId) {
		this.betOrderId = betOrderId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public static final int STATUS_NOT_REBATE = 1;//未反点
	public static final int STATUS_ALREADY_REBATE =2;//已经反点
	public static final int STATUS_CANCLE_REBATE =3;//反点已经回滚

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getChild() {
		return child;
	}

	public void setChild(String child) {
		this.child = child;
	}

	public Long getBetId() {
		return betId;
	}

	public void setBetId(Long betId) {
		this.betId = betId;
	}

	public BigDecimal getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(BigDecimal betMoney) {
		this.betMoney = betMoney;
	}

	public BigDecimal getRebateMoney() {
		return rebateMoney;
	}

	public void setRebateMoney(BigDecimal rebateMoney) {
		this.rebateMoney = rebateMoney;
	}

	public Float getChildRebateRate() {
		return childRebateRate;
	}

	public void setChildRebateRate(Float childRebateRate) {
		this.childRebateRate = childRebateRate;
	}

	public Float getSelfRebateRate() {
		return selfRebateRate;
	}

	public void setSelfRebateRate(Float selfRebateRate) {
		this.selfRebateRate = selfRebateRate;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Date getModifyDatetime() {
		return modifyDatetime;
	}

	public void setModifyDatetime(Date modifyDatetime) {
		this.modifyDatetime = modifyDatetime;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getBetOrderDatetime() {
		return betOrderDatetime;
	}

	public void setBetOrderDatetime(Date betOrderDatetime) {
		this.betOrderDatetime = betOrderDatetime;
	}

	public Integer getRebateType() {
		return rebateType;
	}

	public void setRebateType(Integer rebateType) {
		this.rebateType = rebateType;
	}
	
}

