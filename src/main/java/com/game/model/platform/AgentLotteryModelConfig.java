package com.game.model.platform;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_lottery_model_config")
public class AgentLotteryModelConfig {
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 彩种Code
	 */
	@Column(name = "lot_code")
	private String lotCode;

	/**
	 * 开奖模式1正式站推送2百分比3随机
	 */
	@Column(name = "lottery_mode")
	private Long lotteryMode;

	/**
	 * 百分比
	 */
	@Column(name = "percentage")
	private BigDecimal percentage;
	
	

	/**
	 * 彩种类型
	 */
	@Column(name = "lot_type")
	private Long lotType;
	
	
	/**
	 * 彩种ID
	 */
	@Column(name = "lot_id")
	private Long lotId;
	
	/**
	 * 彩种名称
	 */
	@Column(temp = true, name = "lot_name")
	private String lotName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public Long getLotteryMode() {
		return lotteryMode;
	}

	public void setLotteryMode(Long lotteryMode) {
		this.lotteryMode = lotteryMode;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}
	public Long getLotType() {
		return lotType;
	}

	public void setLotType(Long lotType) {
		this.lotType = lotType;
	}

	public Long getLotId() {
		return lotId;
	}

	public void setLotId(Long lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	
}
