package com.game.model.platform;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Table(name="agent_lunbo")
public class AgentLunBo  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		标题
	*/
	@Column(name="title")
	private String title;
	
	/**
		标题图片地址
	*/
	@Column(name="title_img")
	private String titleImg;
	
	/**
		轮播链接地址
	*/
	@Column(name="title_url")
	private String titleUrl;
	
	/**
		更新时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(name="update_time")
	private Date updateTime;
	
	/**
		结束时间
	*/
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(name="over_time")
	private Date overTime;
	
	/**
		站点ID
	*/
	@Column(name="station_id")
	private Long stationId;
	
	/**
		模板状态
	*/
	@Column(name="model_status")
	private Integer modelStatus;
	
	/**
		状态
	*/
	@Column(name="status")
	private Integer status;
	
	/**
		类型，留着备用
	*/
	@Column(name="code")
	private Integer code;
	
	/**
		排序
	*/
	@Column(name="pai_xu")
	private Integer paiXu;
	
	/**
	域名ID
	*/
	@Column(name="domain_id")
	@InsertValue(value = "0")
	private Long domainId;
	
	@Column(name="folder_code")
	private String folderCode;

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}
	
	public void setTitle(String title){
		this.title = title;
	}

	public String getTitleImg(){
		return this.titleImg;
	}
	
	public void setTitleImg(String titleImg){
		this.titleImg = titleImg;
	}

	public String getTitleUrl(){
		return this.titleUrl;
	}
	
	public void setTitleUrl(String titleUrl){
		this.titleUrl = titleUrl;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}
	
	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getOverTime(){
		return this.overTime;
	}
	
	public void setOverTime(Date overTime){
		this.overTime = overTime;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public Integer getModelStatus(){
		return this.modelStatus;
	}
	
	public void setModelStatus(Integer modelStatus){
		this.modelStatus = modelStatus;
	}

	public Integer getStatus(){
		return this.status;
	}
	
	public void setStatus(Integer status){
		this.status = status;
	}

	public Integer getCode(){
		return this.code;
	}
	
	public void setCode(Integer code){
		this.code = code;
	}

	public Integer getPaiXu() {
		return paiXu;
	}

	public void setPaiXu(Integer paiXu) {
		this.paiXu = paiXu;
	}

	public String getFolderCode() {
		return folderCode;
	}

	public void setFolderCode(String folderCode) {
		this.folderCode = folderCode;
	}
}
