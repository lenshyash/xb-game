package com.game.model.platform;


import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Table(name="agent_win_data")
public class AgentWinData  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="account",length=255)
	private String account;
	
	@Column(name="win_money")
	private BigDecimal winMoney;
	
	@Column(name="win_date")
	private Date winDate;
	
	@Column(name="type",length=50)
	private String type;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name = "head_url")
	private String headUrl;
	


	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public String getAccount(){
		return this.account;
	}
	
	public void setAccount(String account){
		this.account = account;
	}

	public BigDecimal getWinMoney(){
		return this.winMoney;
	}
	
	public void setWinMoney(BigDecimal winMoney){
		this.winMoney = winMoney;
	}

	public Date getWinDate(){
		return this.winDate;
	}
	
	public void setWinDate(Date winDate){
		this.winDate = winDate;
	}

	public String getType(){
		return this.type;
	}
	
	public void setType(String type){
		this.type = type;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}
}

