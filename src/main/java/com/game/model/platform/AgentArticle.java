package com.game.model.platform;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 系统公告、玩法介绍、关于我们
 */
@Table(name="agent_article")
public class AgentArticle  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		标题
	*/
	@Column(name="title")
	private String title;
	
	/**
		类型 1:关于我们,2:取款帮助,3:存款帮助,4:合作伙伴->联盟方案,5:合作伙伴->联盟协议,6:联系我们,7:常见问题 ,8:玩法介绍,9:彩票公告,10:视讯公告,11:体育公告,12:电子公告,13:最新公告,23:博采责任,77:棋牌公告
	*/
	@Column(name="code")
	private Integer code;
	
	/**
		文本内容
	*/
	@Column(name="content")
	private String content;
	
	/**
		状态 1:禁用  2:启用
	*/
	@Column(name="status")
	private Integer status;
	
	/**
		更新/插入时间
	*/@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="update_time")
	private Date updateTime;
	
	/**
		过期时间
	*/@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="over_time")
	private Date overTime;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="frame_width")
	private Integer frameWidth;
	
	@Column(name="frame_height")
	private Integer frameHeight;
	
	@Column(name="model_status")
	private Integer modelStatus;
	
	/** 以下字段只作用到首页弹框 **/
	@Column(name="is_index")
	private boolean isIndex;
	@Column(name="is_reg")
	private boolean isReg;
	@Column(name="is_mutil")
	private boolean isMutil;
	@Column(name="is_cp")
	private boolean isCp;
	
	@Column(name="folder_code")
	private String folderCode;
	
	@Column(name="sort")
	private Integer sort;
	
	public Integer getFrameWidth() {
		return frameWidth;
	}

	public void setFrameWidth(Integer frameWidth) {
		this.frameWidth = frameWidth;
	}

	public Integer getFrameHeight() {
		return frameHeight;
	}

	public void setFrameHeight(Integer frameHeight) {
		this.frameHeight = frameHeight;
	}

	public boolean isCp() {
		return isCp;
	}

	public void setCp(boolean isCp) {
		this.isCp = isCp;
	}

	public boolean isIndex() {
		return isIndex;
	}

	public void setIndex(boolean isIndex) {
		this.isIndex = isIndex;
	}

	public boolean isReg() {
		return isReg;
	}

	public void setReg(boolean isReg) {
		this.isReg = isReg;
	}

	public boolean isMutil() {
		return isMutil;
	}

	public void setMutil(boolean isMutil) {
		this.isMutil = isMutil;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getModelStatus() {
		return modelStatus;
	}

	public void setModelStatus(Integer modelStatus) {
		this.modelStatus = modelStatus;
	}

	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return this.title;
	}
	
	public void setTitle(String title){
		this.title = title;
	}

	public String getContent(){
		return this.content;
	}
	
	public void setContent(String content){
		this.content = content;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}
	
	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getOverTime(){
		return this.overTime;
	}
	
	public void setOverTime(Date overTime){
		this.overTime = overTime;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFolderCode() {
		return folderCode;
	}

	public void setFolderCode(String folderCode) {
		this.folderCode = folderCode;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	
}
