package com.game.model.platform;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "proxy_manual_rebate_record")
public class ProxyManualRebateRecord {
	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "lottery_betting_amount")
	private BigDecimal lotteryBettingAmount;

	@Column(name = "lottery_rebate")
	private BigDecimal lotteryRebate;

	@Column(name = "lottery_rebate_amount")
	private BigDecimal lotteryRebateAmount;

	@Column(name = "mark_six_betting_amount")
	private BigDecimal markSixBettingAmount;

	@Column(name = "mark_six_rebate")
	private BigDecimal markSixRebate;

	@Column(name = "mark_six_rebate_amount")
	private BigDecimal markSixRebateAmount;
	@Column(name = "sport_profit_amount")
	private BigDecimal sportProfitAmount;

	@Column(name = "sport_rebate")
	private BigDecimal sportRebate;

	@Column(name = "sport_rebate_amount")
	private BigDecimal sportRebateAmount;

	@Column(name = "real_profit_amount")
	private BigDecimal realProfitAmount;

	@Column(name = "real_rebate")
	private BigDecimal realRebate;

	@Column(name = "real_rebate_amount")
	private BigDecimal realRebateAmount;

	@Column(name = "egame_profit_amount")
	private BigDecimal egameProfitAmount;

	@Column(name = "egame_rebate")
	private BigDecimal egameRebate;

	@Column(name = "egame_rebate_amount")
	private BigDecimal egameRebateAmount;
	
	@Column(name = "chess_profit_amount")
	private BigDecimal chessProfitAmount;

	@Column(name = "chess_rebate")
	private BigDecimal chessRebate;

	@Column(name = "chess_rebate_amount")
	private BigDecimal chessRebateAmount;
	
	@Column(name = "esports_profit_amount")
	private BigDecimal esportsProfitAmount;

	@Column(name = "esports_rebate")
	private BigDecimal esportsRebate;

	@Column(name = "esports_rebate_amount")
	private BigDecimal esportsRebateAmount;

	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getLotteryBettingAmount() {
		return this.lotteryBettingAmount;
	}

	public void setLotteryBettingAmount(BigDecimal lotteryBettingAmount) {
		this.lotteryBettingAmount = lotteryBettingAmount;
	}

	public BigDecimal getLotteryRebate() {
		return this.lotteryRebate;
	}

	public void setLotteryRebate(BigDecimal lotteryRebate) {
		this.lotteryRebate = lotteryRebate;
	}

	public BigDecimal getLotteryRebateAmount() {
		return this.lotteryRebateAmount;
	}

	public void setLotteryRebateAmount(BigDecimal lotteryRebateAmount) {
		this.lotteryRebateAmount = lotteryRebateAmount;
	}

	public BigDecimal getSportProfitAmount() {
		return this.sportProfitAmount;
	}

	public void setSportProfitAmount(BigDecimal sportProfitAmount) {
		this.sportProfitAmount = sportProfitAmount;
	}

	public BigDecimal getMarkSixBettingAmount() {
		return markSixBettingAmount;
	}

	public void setMarkSixBettingAmount(BigDecimal markSixBettingAmount) {
		this.markSixBettingAmount = markSixBettingAmount;
	}

	public BigDecimal getMarkSixRebate() {
		return markSixRebate;
	}

	public void setMarkSixRebate(BigDecimal markSixRebate) {
		this.markSixRebate = markSixRebate;
	}

	public BigDecimal getMarkSixRebateAmount() {
		return markSixRebateAmount;
	}

	public void setMarkSixRebateAmount(BigDecimal markSixRebateAmount) {
		this.markSixRebateAmount = markSixRebateAmount;
	}

	public BigDecimal getSportRebate() {
		return this.sportRebate;
	}

	public void setSportRebate(BigDecimal sportRebate) {
		this.sportRebate = sportRebate;
	}

	public BigDecimal getSportRebateAmount() {
		return this.sportRebateAmount;
	}

	public void setSportRebateAmount(BigDecimal sportRebateAmount) {
		this.sportRebateAmount = sportRebateAmount;
	}

	public BigDecimal getRealProfitAmount() {
		return this.realProfitAmount;
	}

	public void setRealProfitAmount(BigDecimal realProfitAmount) {
		this.realProfitAmount = realProfitAmount;
	}

	public BigDecimal getRealRebate() {
		return this.realRebate;
	}

	public void setRealRebate(BigDecimal realRebate) {
		this.realRebate = realRebate;
	}

	public BigDecimal getRealRebateAmount() {
		return this.realRebateAmount;
	}

	public void setRealRebateAmount(BigDecimal realRebateAmount) {
		this.realRebateAmount = realRebateAmount;
	}

	public BigDecimal getEgameProfitAmount() {
		return this.egameProfitAmount;
	}

	public void setEgameProfitAmount(BigDecimal egameProfitAmount) {
		this.egameProfitAmount = egameProfitAmount;
	}

	public BigDecimal getEgameRebate() {
		return this.egameRebate;
	}

	public void setEgameRebate(BigDecimal egameRebate) {
		this.egameRebate = egameRebate;
	}

	public BigDecimal getEgameRebateAmount() {
		return this.egameRebateAmount;
	}

	public void setEgameRebateAmount(BigDecimal egameRebateAmount) {
		this.egameRebateAmount = egameRebateAmount;
	}

	public BigDecimal getChessProfitAmount() {
		return chessProfitAmount;
	}

	public void setChessProfitAmount(BigDecimal chessProfitAmount) {
		this.chessProfitAmount = chessProfitAmount;
	}

	public BigDecimal getChessRebate() {
		return chessRebate;
	}

	public void setChessRebate(BigDecimal chessRebate) {
		this.chessRebate = chessRebate;
	}

	public BigDecimal getChessRebateAmount() {
		return chessRebateAmount;
	}

	public void setChessRebateAmount(BigDecimal chessRebateAmount) {
		this.chessRebateAmount = chessRebateAmount;
	}

	public BigDecimal getEsportsProfitAmount() {
		return esportsProfitAmount;
	}

	public void setEsportsProfitAmount(BigDecimal esportsProfitAmount) {
		this.esportsProfitAmount = esportsProfitAmount;
	}

	public BigDecimal getEsportsRebate() {
		return esportsRebate;
	}

	public void setEsportsRebate(BigDecimal esportsRebate) {
		this.esportsRebate = esportsRebate;
	}

	public BigDecimal getEsportsRebateAmount() {
		return esportsRebateAmount;
	}

	public void setEsportsRebateAmount(BigDecimal esportsRebateAmount) {
		this.esportsRebateAmount = esportsRebateAmount;
	}
	
}
