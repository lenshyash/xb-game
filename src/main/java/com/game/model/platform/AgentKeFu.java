package com.game.model.platform;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

@Table(name="agent_kefu")
public class AgentKeFu  {
	public static final Integer KE_FU_TYPE_QQ = 1;
	public static final Integer KE_FU_TYPE_WX = 2;
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="head_url")
	private String headUrl;
	
	@Column(name="type")
	private Integer type;
	
	@Column(name="hao_ma")
	private String haoMa;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="status")
	private Integer status;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getHaoMa() {
		return haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
