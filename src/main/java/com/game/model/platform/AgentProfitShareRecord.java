package com.game.model.platform;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

import com.alibaba.fastjson.annotation.JSONField;

@Table(name = "agent_profit_share_record")
public class AgentProfitShareRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6968040394576420631L;
	public static final int STATUS_NOT_PROFIT = 1;// 未统计
	public static final int STATUS_OK = 4;// 已统计

	@Column(name = "id", primarykey=true)
	private Long id;
	/**
	 * 对应站点
	 */
	@Column(name = "station_id")
	private Long stationId;
	@Column(name = "account_id")
	private Long accountId;
	@JSONField(format = "yyyy-MM-dd")
	@Column(name = "bet_date")
	private Date betDate;
	@Column(name = "account")
	private String account;

	/**
	 * 真人平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "live_platform_fee")
	private BigDecimal livePlatformFee;
	/**
	 * 真人投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "live_bet_money")
	private BigDecimal liveBetMoney;
	/**
	 * 真人中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "live_win_money")
	private BigDecimal liveWinMoney;
	/**
	 * 真人反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "live_rebate_money")
	private BigDecimal liveRebateMoney;

	/**
	 * 体育平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "sport_platform_fee")
	private BigDecimal sportPlatformFee;
	/**
	 * 体育投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "sport_bet_money")
	private BigDecimal sportBetMoney;
	/**
	 * 体育中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "sport_win_money")
	private BigDecimal sportWinMoney;

	/**
	 * 体育反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "sport_rebate_money")
	private BigDecimal sportRebateMoney;

	/**
	 * 电子平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "egame_platform_fee")
	private BigDecimal egamePlatformFee;
	/**
	 * 电子投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "egame_bet_money")
	private BigDecimal egameBetMoney;
	/**
	 * 电子中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "egame_win_money")
	private BigDecimal egameWinMoney;

	/**
	 * 电子反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "egame_rebate_money")
	private BigDecimal egameRebateMoney;

	/**
	 * 棋牌平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "chess_platform_fee")
	private BigDecimal chessPlatformFee;
	/**
	 * 棋牌投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "chess_bet_money")
	private BigDecimal chessBetMoney;
	/**
	 * 棋牌中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "chess_win_money")
	private BigDecimal chessWinMoney;

	/**
	 * 棋牌反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "chess_rebate_money")
	private BigDecimal chessRebateMoney;

	/**
	 * 三方体育平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "third_sport_platform_fee")
	private BigDecimal thirdSportPlatformFee;
	/**
	 * 三方体育投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_sport_bet_money")
	private BigDecimal thirdSportBetMoney;
	/**
	 * 三方体育中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_sport_win_money")
	private BigDecimal thirdSportWinMoney;

	/**
	 * 三方体育反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_sport_rebate_money")
	private BigDecimal thirdSportRebateMoney;

	/**
	 * 三方彩票平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "third_lottery_platform_fee")
	private BigDecimal thirdLotteryPlatformFee;
	/**
	 * 三方彩票投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_lottery_bet_money")
	private BigDecimal thirdLotteryBetMoney;
	/**
	 * 三方彩票中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_lottery_win_money")
	private BigDecimal thirdLotteryWinMoney;

	/**
	 * 三方彩票反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_lottery_rebate_money")
	private BigDecimal thirdLotteryRebateMoney;

	/**
	 * 彩票平台抽点费
	 */
	@InsertValue(value = "0")
	@Column(name = "lottery_platform_fee")
	private BigDecimal lotteryPlatformFee;
	/**
	 * 彩票投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "lottery_bet_money")
	private BigDecimal lotteryBetMoney;
	/**
	 * 彩票中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "lottery_win_money")
	private BigDecimal lotteryWinMoney;

	/**
	 * 彩票反水金额
	 */
	@InsertValue(value = "0")
	@Column(name = "lottery_rebate_money")
	private BigDecimal lotteryRebateMoney;

	/**
	 * 第三方充值金额
	 */
	@InsertValue(value = "0")
	@Column(name = "third_deposit")
	private BigDecimal thirdDeposit;
	
	/**
	 * 赠送金额
	 */
	@InsertValue(value = "0")
	@Column(name = "give_money")
	private BigDecimal giveMoney;

	@Column(name = "profit_share_desc", length = 128)
	private String profitShareDesc;

	/**
	 * 代理占成状态 （1，还未到账 3，已经回滚 4,占成已到账 ）
	 */
	@InsertValue(value = "1")
	@Column(name = "profit_share_status")
	private Integer profitShareStatus;

	@Column(name = "agent_id")
	private Long agentId;

	@Column(name = "agent_name")
	private String agentName;
	
	@Column(name = "operator")
	private String operator;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getBetDate() {
		return betDate;
	}

	public void setBetDate(Date betDate) {
		this.betDate = betDate;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getProfitShareDesc() {
		return profitShareDesc;
	}

	public void setProfitShareDesc(String profitShareDesc) {
		this.profitShareDesc = profitShareDesc;
	}

	public Integer getProfitShareStatus() {
		return profitShareStatus;
	}

	public void setProfitShareStatus(Integer profitShareStatus) {
		this.profitShareStatus = profitShareStatus;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public BigDecimal getLivePlatformFee() {
		return livePlatformFee;
	}

	public void setLivePlatformFee(BigDecimal livePlatformFee) {
		this.livePlatformFee = livePlatformFee;
	}

	public BigDecimal getLiveBetMoney() {
		return liveBetMoney;
	}

	public void setLiveBetMoney(BigDecimal liveBetMoney) {
		this.liveBetMoney = liveBetMoney;
	}

	public BigDecimal getLiveWinMoney() {
		return liveWinMoney;
	}

	public void setLiveWinMoney(BigDecimal liveWinMoney) {
		this.liveWinMoney = liveWinMoney;
	}

	public BigDecimal getSportPlatformFee() {
		return sportPlatformFee;
	}

	public void setSportPlatformFee(BigDecimal sportPlatformFee) {
		this.sportPlatformFee = sportPlatformFee;
	}

	public BigDecimal getSportBetMoney() {
		return sportBetMoney;
	}

	public void setSportBetMoney(BigDecimal sportBetMoney) {
		this.sportBetMoney = sportBetMoney;
	}

	public BigDecimal getSportWinMoney() {
		return sportWinMoney;
	}

	public void setSportWinMoney(BigDecimal sportWinMoney) {
		this.sportWinMoney = sportWinMoney;
	}

	public BigDecimal getEgamePlatformFee() {
		return egamePlatformFee;
	}

	public void setEgamePlatformFee(BigDecimal egamePlatformFee) {
		this.egamePlatformFee = egamePlatformFee;
	}

	public BigDecimal getEgameBetMoney() {
		return egameBetMoney;
	}

	public void setEgameBetMoney(BigDecimal egameBetMoney) {
		this.egameBetMoney = egameBetMoney;
	}

	public BigDecimal getEgameWinMoney() {
		return egameWinMoney;
	}

	public void setEgameWinMoney(BigDecimal egameWinMoney) {
		this.egameWinMoney = egameWinMoney;
	}

	public BigDecimal getChessPlatformFee() {
		return chessPlatformFee;
	}

	public void setChessPlatformFee(BigDecimal chessPlatformFee) {
		this.chessPlatformFee = chessPlatformFee;
	}

	public BigDecimal getChessBetMoney() {
		return chessBetMoney;
	}

	public void setChessBetMoney(BigDecimal chessBetMoney) {
		this.chessBetMoney = chessBetMoney;
	}

	public BigDecimal getChessWinMoney() {
		return chessWinMoney;
	}

	public void setChessWinMoney(BigDecimal chessWinMoney) {
		this.chessWinMoney = chessWinMoney;
	}

	public BigDecimal getThirdSportPlatformFee() {
		return thirdSportPlatformFee;
	}

	public void setThirdSportPlatformFee(BigDecimal thirdSportPlatformFee) {
		this.thirdSportPlatformFee = thirdSportPlatformFee;
	}

	public BigDecimal getThirdSportBetMoney() {
		return thirdSportBetMoney;
	}

	public void setThirdSportBetMoney(BigDecimal thirdSportBetMoney) {
		this.thirdSportBetMoney = thirdSportBetMoney;
	}

	public BigDecimal getThirdSportWinMoney() {
		return thirdSportWinMoney;
	}

	public void setThirdSportWinMoney(BigDecimal thirdSportWinMoney) {
		this.thirdSportWinMoney = thirdSportWinMoney;
	}

	public BigDecimal getThirdLotteryPlatformFee() {
		return thirdLotteryPlatformFee;
	}

	public void setThirdLotteryPlatformFee(BigDecimal thirdLotteryPlatformFee) {
		this.thirdLotteryPlatformFee = thirdLotteryPlatformFee;
	}

	public BigDecimal getThirdLotteryBetMoney() {
		return thirdLotteryBetMoney;
	}

	public void setThirdLotteryBetMoney(BigDecimal thirdLotteryBetMoney) {
		this.thirdLotteryBetMoney = thirdLotteryBetMoney;
	}

	public BigDecimal getThirdLotteryWinMoney() {
		return thirdLotteryWinMoney;
	}

	public void setThirdLotteryWinMoney(BigDecimal thirdLotteryWinMoney) {
		this.thirdLotteryWinMoney = thirdLotteryWinMoney;
	}

	public BigDecimal getThirdDeposit() {
		return thirdDeposit;
	}

	public void setThirdDeposit(BigDecimal thirdDeposit) {
		this.thirdDeposit = thirdDeposit;
	}

	public BigDecimal getLotteryPlatformFee() {
		return lotteryPlatformFee;
	}

	public void setLotteryPlatformFee(BigDecimal lotteryPlatformFee) {
		this.lotteryPlatformFee = lotteryPlatformFee;
	}

	public BigDecimal getLotteryBetMoney() {
		return lotteryBetMoney;
	}

	public void setLotteryBetMoney(BigDecimal lotteryBetMoney) {
		this.lotteryBetMoney = lotteryBetMoney;
	}

	public BigDecimal getLotteryWinMoney() {
		return lotteryWinMoney;
	}

	public void setLotteryWinMoney(BigDecimal lotteryWinMoney) {
		this.lotteryWinMoney = lotteryWinMoney;
	}

	public BigDecimal getLiveRebateMoney() {
		return liveRebateMoney;
	}

	public void setLiveRebateMoney(BigDecimal liveRebateMoney) {
		this.liveRebateMoney = liveRebateMoney;
	}

	public BigDecimal getSportRebateMoney() {
		return sportRebateMoney;
	}

	public void setSportRebateMoney(BigDecimal sportRebateMoney) {
		this.sportRebateMoney = sportRebateMoney;
	}

	public BigDecimal getEgameRebateMoney() {
		return egameRebateMoney;
	}

	public void setEgameRebateMoney(BigDecimal egameRebateMoney) {
		this.egameRebateMoney = egameRebateMoney;
	}

	public BigDecimal getChessRebateMoney() {
		return chessRebateMoney;
	}

	public void setChessRebateMoney(BigDecimal chessRebateMoney) {
		this.chessRebateMoney = chessRebateMoney;
	}

	public BigDecimal getThirdSportRebateMoney() {
		return thirdSportRebateMoney;
	}

	public void setThirdSportRebateMoney(BigDecimal thirdSportRebateMoney) {
		this.thirdSportRebateMoney = thirdSportRebateMoney;
	}

	public BigDecimal getThirdLotteryRebateMoney() {
		return thirdLotteryRebateMoney;
	}

	public void setThirdLotteryRebateMoney(BigDecimal thirdLotteryRebateMoney) {
		this.thirdLotteryRebateMoney = thirdLotteryRebateMoney;
	}

	public BigDecimal getLotteryRebateMoney() {
		return lotteryRebateMoney;
	}

	public void setLotteryRebateMoney(BigDecimal lotteryRebateMoney) {
		this.lotteryRebateMoney = lotteryRebateMoney;
	}

	public BigDecimal getGiveMoney() {
		return giveMoney;
	}

	public void setGiveMoney(BigDecimal giveMoney) {
		this.giveMoney = giveMoney;
	}
}
