package com.game.model.platform;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Table(name = "agent_activity")
public class AgentActivity {

	public static final int UNREAD_STATUS = 0;
	public static final int READ_STATUS = 1;
	
	public static final int APPLY_FLAG_OFF = 1;
	public static final int APPLY_FLAG_ON = 2;

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 标题
	 */
	@Column(name = "title")
	private String title;

	/**
	 * 内容
	 */
	@Column(name = "content")
	private String content;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 过期时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "over_time")
	private Date overTime;

	/**
	 * 总状态
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 站点id
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 模板状态
	 */
	@Column(name = "model_status")
	private Integer modelStatus;

	/**
	 * 标题图片
	 */
	@Column(name = "title_img_url")
	private String titleImgUrl;

	/**
	 * 排序
	 */
	@Column(name = "pai_xu")
	private Integer paiXu;
	
	/**
	域名ID
	*/
	@Column(name="domain_id")
	@InsertValue(value = "0")
	private Long domainId;
	
	@Column(name="folder_code")
	private String folderCode;

	@Column(name = "read_flag")
	private Integer readFlag;

	
	@Column(name = "apply_selected")
	private String applySelected;
	
	@Column(name = "apply_flag")
	private Integer applyFlag;
	
	public Integer getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(Integer readFlag) {
		this.readFlag = readFlag;
	}

	public String getTitleImgUrl() {
		return titleImgUrl;
	}

	public void setTitleImgUrl(String titleImgUrl) {
		this.titleImgUrl = titleImgUrl;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getOverTime() {
		return overTime;
	}

	public void setOverTime(Date overTime) {
		this.overTime = overTime;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getModelStatus() {
		return this.modelStatus;
	}

	public void setModelStatus(Integer modelStatus) {
		this.modelStatus = modelStatus;
	}

	public Integer getPaiXu() {
		return paiXu;
	}

	public void setPaiXu(Integer paiXu) {
		this.paiXu = paiXu;
	}

	public Long getDomainId() {
		return domainId;
	}

	public void setDomainId(Long domainId) {
		this.domainId = domainId;
	}

	public String getFolderCode() {
		return folderCode;
	}

	public void setFolderCode(String folderCode) {
		this.folderCode = folderCode;
	}

	public String getApplySelected() {
		return applySelected;
	}

	public void setApplySelected(String applySelected) {
		this.applySelected = applySelected;
	}

	public Integer getApplyFlag() {
		return applyFlag;
	}

	public void setApplyFlag(Integer applyFlag) {
		this.applyFlag = applyFlag;
	}
	
	
}
