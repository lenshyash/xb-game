package com.game.model.platform;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_profit_share_strategy")
public class AgentProfitShareStrategy {
	public static long STATUS_DISABLED = 1;
	public static long STATUS_ENALBED = 2;
	public static long PLATFORM_FEE_TYPE_YL = 1;//盈利
	public static long PLATFORM_FEE_TYPE_LS = 2;//流水

	@Column(name = "id", primarykey = true)
	private Long id;
	// 占成类型（1、彩票，2、真人，3、电子，4、体育，5、三方体育，6、三方彩票，7、棋牌) 逗号隔开
	@Column(name = "type")
	private Long type;
	// 状态（1，禁用 2，启用）	
	@Column(name = "status")
	private Long status;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "memo")
	private String memo;
	/**
	 * 平台抽点类型
	 */
	@Column(name = "platform_fee_type")
	private Long platformFeeType;

	/**
	 * 平台抽点费率
	 */
	@Column(name = "platform_fee_rate")
	private BigDecimal platformFeeRate;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public BigDecimal getPlatformFeeRate() {
		return platformFeeRate;
	}

	public void setPlatformFeeRate(BigDecimal platformFeeRate) {
		this.platformFeeRate = platformFeeRate;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getPlatformFeeType() {
		return platformFeeType;
	}

	public void setPlatformFeeType(Long platformFeeType) {
		this.platformFeeType = platformFeeType;
	}
}
