package com.game.model.platform;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

/**
 * 推广链接
 */
@Table(name = "member_genrt_link")
public class MemberGenrtLink {

	public static long GENERALIZE_TYPE_RANDOM = 1;//随机生成
	
	public static long GENERALIZE_TYPE_INPUT = 2;//自选输入
	
	public static long DEFAULT_LINK = 2;//默认链接
	
	public static long NOT_DEFAULT_LINK = 1;//不是默认链接

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 用户ID
	 */
	@Column(name = "user_account")
	private String userAccount;

	@Column(name = "user_id")
	private Long userId;

	/**
	 * 生成会员类型（1，代理，2 会员）
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 彩票返点
	 */
	@Column(name = "cp_rolling")
	private BigDecimal cpRolling;

	/**
	 * 占成数
	 */
	@Column(name = "profit_share")
	private BigDecimal profitShare;

	/**
	 * 动态赔率点
	 */
	@Column(name = "dynamic_rate")
	private BigDecimal dynamicRate;

	/**
	 * 短连接码
	 */
	@Column(name = "link_key")
	private String linkKey;

	/**
	 * 1,禁用 2,启用
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 生成类型(1 随机 2 自选)
	 */
	@Column(name = "generalize_type")
	@InsertValue(value = "1")
	private Long generalizeType;

	/**
	 * 短链接1
	 */
	@Column(name = "short_url1")
	private String shortUrl1;
	/**
	 * 短链接2
	 */
	@Column(name = "short_url2")
	private String shortUrl2;

	@Column(name = "default_link")
	private Long defaultLink;
	
	/**
	 * 不存库
	 */
	private String linkUrl;

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getCpRolling() {
		return cpRolling;
	}

	public void setCpRolling(BigDecimal cpRolling) {
		this.cpRolling = cpRolling;
	}

	public String getLinkKey() {
		return linkKey;
	}

	public void setLinkKey(String linkKey) {
		this.linkKey = linkKey;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getShortUrl1() {
		return shortUrl1;
	}

	public void setShortUrl1(String shortUrl1) {
		this.shortUrl1 = shortUrl1;
	}

	public String getShortUrl2() {
		return shortUrl2;
	}

	public void setShortUrl2(String shortUrl2) {
		this.shortUrl2 = shortUrl2;
	}

	public BigDecimal getProfitShare() {
		return profitShare;
	}

	public void setProfitShare(BigDecimal profitShare) {
		this.profitShare = profitShare;
	}

	public BigDecimal getDynamicRate() {
		return dynamicRate;
	}

	public void setDynamicRate(BigDecimal dynamicRate) {
		this.dynamicRate = dynamicRate;
	}

	public Long getGeneralizeType() {
		return generalizeType;
	}

	public void setGeneralizeType(Long generalizeType) {
		this.generalizeType = generalizeType;
	}

	public Long getDefaultLink() {
		return defaultLink;
	}

	public void setDefaultLink(Long defaultLink) {
		this.defaultLink = defaultLink;
	}
	
	
}
