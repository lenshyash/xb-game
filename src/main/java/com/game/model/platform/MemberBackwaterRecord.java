package com.game.model.platform;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

import com.alibaba.fastjson.annotation.JSONField;

@Table(name = "member_backwater_record")
public class MemberBackwaterRecord implements Serializable {
	public static final int STATUS_NOT_ROLL = 1;// 未反水
	public static final int STATUS_CANCLE_ROLL_BACK = 3;// 反水已经回滚
	public static final int STATUS_ALREADY_ROLL_BACK_AND_MONEY = 4;// 已经反水
	public static final int STATUS_MONEY_EXPIRED = 8;// 反水过期
	public static final int STATUS_MONEY_UNCLAIMED = 9;// 反水待领取

	@Column(name = "id", temp=true)
	private Long id;
	/**
	 * 对应站点
	 */
	@Column(name = "station_id")
	private Long stationId;
	@Column(name = "account_id")
	private Long accountId;
	@JSONField(format = "yyyy-MM-dd")
	@Column(name = "bet_date")
	private Date betDate;
	@Column(name = "account")
	private String account;

	/**
	 * 投注类型(1,彩票 2,真人,3,电子游艺,4,体育,5六合特码B,6六合正码B,11系统彩,15十分六合彩特码B,16十分六合彩正码B)
	 */
	@Column(name = "bet_type")
	private Integer betType;
	/**
	 * 投注金额
	 */
	@InsertValue(value = "0")
	@Column(name = "bet_money")
	private BigDecimal betMoney;
	/**
	 * 中奖金额
	 */
	@InsertValue(value = "0")
	@Column(name = "win_money")
	private BigDecimal winMoney;

	@Column(name = "backwater_money")
	private BigDecimal backwaterMoney;

	@Column(name = "backwater_rate")
	private BigDecimal backwaterRate;

	@Column(name = "backwater_desc", length = 128)
	private String backwaterDesc;

	/**
	 * 会员返水状态 （1，还未返水 3，返水已经回滚 4,返水已到账 ）
	 */
	@InsertValue(value = "1")
	@Column(name = "backwater_status")
	private Integer backwaterStatus;
	/**
	 * 反水需要的打码量
	 */
	@Column(name = "draw_need")
	private BigDecimal drawNeed;
	@Column(name = "agent_id")
	private Long agentId;
	
	@Column(name = "agent_name")
	private String agentName;
	
	@Column(name = "operator_id")
	private Long operatorId;
	@Column(name = "operator")
	private String operator;
	
	@Column(name="backwater_room_id")
	private Long backwaterRoomId;
	
	@Column(name = "report_type")
	private Long reportType;
	
	private String roomName;
	

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Long getBackwaterRoomId() {
		return backwaterRoomId;
	}

	public void setBackwaterRoomId(Long backwaterRoomId) {
		this.backwaterRoomId = backwaterRoomId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getBetDate() {
		return betDate;
	}

	public void setBetDate(Date betDate) {
		this.betDate = betDate;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Integer getBetType() {
		return betType;
	}

	public void setBetType(Integer betType) {
		this.betType = betType;
	}

	public BigDecimal getBetMoney() {
		return betMoney;
	}

	public void setBetMoney(BigDecimal betMoney) {
		this.betMoney = betMoney;
	}

	public BigDecimal getWinMoney() {
		return winMoney;
	}

	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}

	public BigDecimal getBackwaterMoney() {
		return backwaterMoney;
	}

	public void setBackwaterMoney(BigDecimal backwaterMoney) {
		this.backwaterMoney = backwaterMoney;
	}

	public BigDecimal getBackwaterRate() {
		return backwaterRate;
	}

	public void setBackwaterRate(BigDecimal backwaterRate) {
		this.backwaterRate = backwaterRate;
	}

	public String getBackwaterDesc() {
		return backwaterDesc;
	}

	public void setBackwaterDesc(String backwaterDesc) {
		this.backwaterDesc = backwaterDesc;
	}

	public Integer getBackwaterStatus() {
		return backwaterStatus;
	}

	public void setBackwaterStatus(Integer backwaterStatus) {
		this.backwaterStatus = backwaterStatus;
	}

	public BigDecimal getDrawNeed() {
		return drawNeed;
	}

	public void setDrawNeed(BigDecimal drawNeed) {
		this.drawNeed = drawNeed;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public Long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}
	
	
}
