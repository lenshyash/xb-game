package com.game.model.platform;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.math.BigDecimal;
import java.util.Date;

@Table(name = "proxy_daily_betting_statistics")
public class ProxyDailyBettingStatistics {
	public static int status_unroll = 1;// 代理未返点
	public static int status_rollback = 2;// 代理已经返点
	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "stat_date")
	private Date statDate;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "lottery_bet_amount")
	private BigDecimal lotteryBetAmount;

	@Column(name = "lottery_profit_amount")
	private BigDecimal lotteryProfitAmount;

	@Column(name = "lottery_rebate_amount")
	private BigDecimal lotteryRebateAmount;

	@Column(name = "mark_six_bet_amount")
	private BigDecimal markSixBetAmount;

	@Column(name = "mark_six_profit_amount")
	private BigDecimal markSixProfitAmount;

	@Column(name = "mark_six_rebate_amount")
	private BigDecimal markSixRebateAmount;

	@Column(name = "real_bet_amount")
	private BigDecimal realBetAmount;

	@Column(name = "real_profit_amount")
	private BigDecimal realProfitAmount;

	@Column(name = "real_rebate_amount")
	private BigDecimal realRebateAmount;

	@Column(name = "egame_bet_amount")
	private BigDecimal egameBetAmount;

	@Column(name = "egame_profit_amount")
	private BigDecimal egameProfitAmount;

	@Column(name = "egame_rebate_amount")
	private BigDecimal egameRebateAmount;

	@Column(name = "sports_bet_amount")
	private BigDecimal sportsBetAmount;

	@Column(name = "sports_profit_amount")
	private BigDecimal sportsProfitAmount;

	@Column(name = "sports_rebate_amount")
	private BigDecimal sportsRebateAmount;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "status")
	private Integer status;
	@Column(name = "rebate_datetime")
	private Date rebateDatetime;

	@Column(name = "rebate_record_id")
	private Long rebateRecordId;
	
	@Column(name = "chess_bet_amount")
	private BigDecimal chessBetAmount;

	@Column(name = "chess_profit_amount")
	private BigDecimal chessProfitAmount;

	@Column(name = "chess_rebate_amount")
	private BigDecimal chessRebateAmount;
	
	
	@Column(name = "esports_bet_amount")
	private BigDecimal esportsBetAmount;

	@Column(name = "esports_profit_amount")
	private BigDecimal esportsProfitAmount;

	@Column(name = "esports_rebate_amount")
	private BigDecimal esportsRebateAmount;

	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStatDate() {
		return statDate;
	}

	public void setStatDate(Date statDate) {
		this.statDate = statDate;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getLotteryBetAmount() {
		return lotteryBetAmount;
	}

	public void setLotteryBetAmount(BigDecimal lotteryBetAmount) {
		this.lotteryBetAmount = lotteryBetAmount;
	}

	public BigDecimal getLotteryProfitAmount() {
		return lotteryProfitAmount;
	}

	public void setLotteryProfitAmount(BigDecimal lotteryProfitAmount) {
		this.lotteryProfitAmount = lotteryProfitAmount;
	}

	public BigDecimal getMarkSixBetAmount() {
		return markSixBetAmount;
	}

	public void setMarkSixBetAmount(BigDecimal markSixBetAmount) {
		this.markSixBetAmount = markSixBetAmount;
	}

	public BigDecimal getMarkSixProfitAmount() {
		return markSixProfitAmount;
	}

	public void setMarkSixProfitAmount(BigDecimal markSixProfitAmount) {
		this.markSixProfitAmount = markSixProfitAmount;
	}

	public BigDecimal getMarkSixRebateAmount() {
		return markSixRebateAmount;
	}

	public void setMarkSixRebateAmount(BigDecimal markSixRebateAmount) {
		this.markSixRebateAmount = markSixRebateAmount;
	}

	public BigDecimal getRealBetAmount() {
		return realBetAmount;
	}

	public void setRealBetAmount(BigDecimal realBetAmount) {
		this.realBetAmount = realBetAmount;
	}

	public BigDecimal getRealProfitAmount() {
		return realProfitAmount;
	}

	public void setRealProfitAmount(BigDecimal realProfitAmount) {
		this.realProfitAmount = realProfitAmount;
	}

	public BigDecimal getEgameBetAmount() {
		return egameBetAmount;
	}

	public void setEgameBetAmount(BigDecimal egameBetAmount) {
		this.egameBetAmount = egameBetAmount;
	}

	public BigDecimal getEgameProfitAmount() {
		return egameProfitAmount;
	}

	public void setEgameProfitAmount(BigDecimal egameProfitAmount) {
		this.egameProfitAmount = egameProfitAmount;
	}

	public BigDecimal getSportsBetAmount() {
		return sportsBetAmount;
	}

	public void setSportsBetAmount(BigDecimal sportsBetAmount) {
		this.sportsBetAmount = sportsBetAmount;
	}

	public BigDecimal getSportsProfitAmount() {
		return sportsProfitAmount;
	}

	public void setSportsProfitAmount(BigDecimal sportsProfitAmount) {
		this.sportsProfitAmount = sportsProfitAmount;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getRebateDatetime() {
		return rebateDatetime;
	}

	public void setRebateDatetime(Date rebateDatetime) {
		this.rebateDatetime = rebateDatetime;
	}

	public BigDecimal getLotteryRebateAmount() {
		return lotteryRebateAmount;
	}

	public void setLotteryRebateAmount(BigDecimal lotteryRebateAmount) {
		this.lotteryRebateAmount = lotteryRebateAmount;
	}

	public BigDecimal getRealRebateAmount() {
		return realRebateAmount;
	}

	public void setRealRebateAmount(BigDecimal realRebateAmount) {
		this.realRebateAmount = realRebateAmount;
	}

	public BigDecimal getEgameRebateAmount() {
		return egameRebateAmount;
	}

	public void setEgameRebateAmount(BigDecimal egameRebateAmount) {
		this.egameRebateAmount = egameRebateAmount;
	}

	public BigDecimal getSportsRebateAmount() {
		return sportsRebateAmount;
	}

	public void setSportsRebateAmount(BigDecimal sportsRebateAmount) {
		this.sportsRebateAmount = sportsRebateAmount;
	}

	public Long getRebateRecordId() {
		return rebateRecordId;
	}

	public void setRebateRecordId(Long rebateRecordId) {
		this.rebateRecordId = rebateRecordId;
	}

	public BigDecimal getChessBetAmount() {
		return chessBetAmount;
	}

	public void setChessBetAmount(BigDecimal chessBetAmount) {
		this.chessBetAmount = chessBetAmount;
	}

	public BigDecimal getChessProfitAmount() {
		return chessProfitAmount;
	}

	public void setChessProfitAmount(BigDecimal chessProfitAmount) {
		this.chessProfitAmount = chessProfitAmount;
	}

	public BigDecimal getChessRebateAmount() {
		return chessRebateAmount;
	}

	public void setChessRebateAmount(BigDecimal chessRebateAmount) {
		this.chessRebateAmount = chessRebateAmount;
	}

	public BigDecimal getEsportsBetAmount() {
		return esportsBetAmount;
	}

	public void setEsportsBetAmount(BigDecimal esportsBetAmount) {
		this.esportsBetAmount = esportsBetAmount;
	}

	public BigDecimal getEsportsProfitAmount() {
		return esportsProfitAmount;
	}

	public void setEsportsProfitAmount(BigDecimal esportsProfitAmount) {
		this.esportsProfitAmount = esportsProfitAmount;
	}

	public BigDecimal getEsportsRebateAmount() {
		return esportsRebateAmount;
	}

	public void setEsportsRebateAmount(BigDecimal esportsRebateAmount) {
		this.esportsRebateAmount = esportsRebateAmount;
	}
	
}
