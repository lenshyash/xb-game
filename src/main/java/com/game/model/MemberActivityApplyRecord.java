package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

@Table(name = "member_activity_apply_record")
public class MemberActivityApplyRecord{
	
	// 未处理
	public static long STATUS_UNTREATED = 1l;
	// 处理成功
	public static long STATUS_SUCCESS = 2l;
	// 处理失败
	public static long STATUS_FAILED = 3l;
	
	@Column(name="id",primarykey=true)
	private Long id;

	@Column(name = "activity_name")
	private String activityName;
	
	@Column(name = "activity_id")
	private Long activityId;
	
	@Column(name = "status")
	private Long status;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "remark")
	private String remark;
	
	@Column(name = "account_id")
	private Long accountId;
	
	@Column(name = "account")
	private String account;
	
	@Column(name = "create_time")
	private Date createTime;
	
	@Column(name = "activity_option")
	private String activityOption;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getActivityOption() {
		return activityOption;
	}

	public void setActivityOption(String activityOption) {
		this.activityOption = activityOption;
	}
	
	
}
