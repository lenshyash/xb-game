package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_sign_rule")
public class AgentSignRule {
	@Column(name = "id", primarykey = true)
	private Long id;

	// 循环赠送（取最大效益）
	public static long SIGN_TYPE_LOOP = 1;
	// 限定赠送
	public static long SIGN_TYPE_UNLOOP = 2;
	
	// 不清零
	public static long SIGN_CLEAR_NONE = 1;
	// 清零
	public static long SIGN_CLEAR_YES = 2;
	

	@Column(name = "days")
	private Long days;

	@Column(name = "score")
	private Long score;

	/**
	 * 签到类型
	 */
	@InsertValue(value = "1")
	@Column(name = "sign_type")
	private Long signType;

	@Column(name = "station_id")
	private Long stationId;


	@Column(name = "sign_clear")
	private Long signClear;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDays() {
		return this.days;
	}

	public void setDays(Long days) {
		this.days = days;
	}

	public Long getScore() {
		return this.score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getSignType() {
		return signType;
	}

	public void setSignType(Long signType) {
		this.signType = signType;
	}

	public Long getSignClear() {
		return signClear;
	}

	public void setSignClear(Long signClear) {
		this.signClear = signClear;
	}
	
	
}
