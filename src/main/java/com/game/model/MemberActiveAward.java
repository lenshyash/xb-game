package com.game.model;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_active_award")
public class MemberActiveAward {

	public static long AWARD_TYPE_NONE = 1;// 未中奖
	public static long AWARD_TYPE_MONEY = 2;// 现金
	public static long AWARD_TYPE_PRODUCT = 3;// 商品
	public static long AWARD_TYPE_SCORE = 4;// 积分

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 奖项名称
	 */
	@Column(name = "award_name", length = 100)
	private String awardName;

	/**
	 * 商品ID
	 */
	@Column(name = "product_id")
	private Long productId;

	/**
	 * 概率基数
	 */
	@Column(name = "chance")
	private BigDecimal chance;

	/**
	 * 奖项类型（1、不中奖，2、现金，3、商品，4、积分）
	 */
	@Column(name = "award_type")
	private Long awardType;

	/**
	 * 奖项数量
	 */
	@Column(name = "award_count")
	private Long awardCount;

	/**
	 * 面值
	 */
	@Column(name = "award_value")
	private BigDecimal awardValue;

	/**
	 * 奖项索引
	 */
	@Column(name = "award_index")
	private Long awardIndex;

	/**
	 * 活动ID
	 */
	@Column(name = "active_id")
	private Long activeId;

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAwardName() {
		return this.awardName;
	}

	public void setAwardName(String awardName) {
		this.awardName = awardName;
	}

	public Long getProductId() {
		return this.productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public BigDecimal getChance() {
		return this.chance;
	}

	public void setChance(BigDecimal chance) {
		this.chance = chance;
	}

	public Long getAwardType() {
		return this.awardType;
	}

	public void setAwardType(Long awardType) {
		this.awardType = awardType;
	}

	public Long getAwardCount() {
		return this.awardCount;
	}

	public void setAwardCount(Long awardCount) {
		this.awardCount = awardCount;
	}

	public BigDecimal getAwardValue() {
		return this.awardValue;
	}

	public void setAwardValue(BigDecimal awardValue) {
		this.awardValue = awardValue;
	}

	public Long getAwardIndex() {
		return this.awardIndex;
	}

	public void setAwardIndex(Long awardIndex) {
		this.awardIndex = awardIndex;
	}
}
