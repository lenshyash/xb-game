package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import com.game.util.DateUtil;

//用户充值赠送策略
@Table(name = "member_balance_gem_strategy")
public class MemberBalanceGemStrategy {
	// 状态 1=禁用，2=启用
	final public static int STATUS_DISABLED = 1;
	final public static int STATUS_NORMAL = 2;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "station_id")
	private Long stationId;

	// 打码量倍数。(充值金额+赠送)x流水倍数=出款需要达到的投注量
	@Column(name = "bet_multiple")
	private BigDecimal betMultiple;
	// 创建时间
	@Column(name = "create_datetime")
	private Date createDatetime;
	// 备注
	@Column(name = "remark")
	private String remark;
	//状态
	@Column(name = "status")
	private Integer status;
	// 最小充值金额，=0代表不限制
	@Column(name = "min_money")
	private BigDecimal minMoney;
	// 最大充值金额，=0代表不限制
	@Column(name = "max_money")
	private BigDecimal maxMoney;
	// 基准年化收益率
	@Column(name = "benchmark_annual_yield")
	private BigDecimal benchmarkAnnualYield;
	// 红运年化收益率
	@Column(name = "hongyun_annual_yield")
	private BigDecimal hongyunAnnualYield;
	// 基准万分收益
	@Column(name="benchmarkThousandGains",temp=true)
	private BigDecimal benchmarkThousandGains;
	// 红运万分收益
	@Column(name="benchmarkThousandGains",temp=true)
	private BigDecimal hongyunThousandGains;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getBetMultiple() {
		return betMultiple;
	}

	public void setBetMultiple(BigDecimal betMultiple) {
		this.betMultiple = betMultiple;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(BigDecimal minMoney) {
		this.minMoney = minMoney;
	}

	public BigDecimal getMaxMoney() {
		return maxMoney;
	}

	public void setMaxMoney(BigDecimal maxMoney) {
		this.maxMoney = maxMoney;
	}

	public BigDecimal getBenchmarkAnnualYield() {
		return benchmarkAnnualYield;
	}

	public void setBenchmarkAnnualYield(BigDecimal benchmarkAnnualYield) {
		this.benchmarkAnnualYield = benchmarkAnnualYield;
	}

	public BigDecimal getHongyunAnnualYield() {
		return hongyunAnnualYield;
	}

	public void setHongyunAnnualYield(BigDecimal hongyunAnnualYield) {
		this.hongyunAnnualYield = hongyunAnnualYield;
	}

	public BigDecimal getBenchmarkThousandGains() {
		return benchmarkThousandGains;
	}

	public void setBenchmarkThousandGains(BigDecimal benchmarkThousandGains) {
		this.benchmarkThousandGains = benchmarkThousandGains;
	}

	public BigDecimal getHongyunThousandGains() {
		return hongyunThousandGains;
	}

	public void setHongyunThousandGains(BigDecimal hongyunThousandGains) {
		this.hongyunThousandGains = hongyunThousandGains;
	}
	
}
