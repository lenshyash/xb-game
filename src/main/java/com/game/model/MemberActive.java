package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_active")
public class MemberActive {

	public static long STATUS_DISABLED = 1;

	public static long STATUS_ENABLED = 2;

	public static long ACTIVE_TYPE_TURNTABLE = 1;// 大转盘

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 活动名称
	 */
	@Column(name = "active_name", length = 100)
	private String activeName;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 状态
	 */
	@Column(name = "status")
	private Long status;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 活动类型
	 */
	@Column(name = "active_type")
	private Long activeType;

	/**
	 * 开始时间
	 */
	@Column(name = "begin_datetime")
	private Date beginDatetime;

	/**
	 * 结束时间
	 */
	@Column(name = "end_datetime")
	private Date endDatetime;

	/**
	 * 消耗积分
	 */
	@Column(name = "score")
	private BigDecimal score;

	/**
	 * 单天可玩次数
	 */
	@Column(name = "play_count")
	private Long playCount;

	/**
	 * 活动规则
	 */
	@Column(name = "active_help")
	private String activeHelp;

	/**
	 * 图片路径
	 */
	@Column(name = "img_path")
	private String imgPath;

	/**
	 * 奖项数量
	 */
	@Column(name = "award_count")
	private Integer awardCount;
	
	/**
	 * 抽奖资格
	 */
	@Column(name = "active_role")
	private String activeRole;
	
	/**
	 * 活动声明
	 */
	@Column(name = "active_remark")
	private String activeRemark;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActiveName() {
		return this.activeName;
	}

	public void setActiveName(String activeName) {
		this.activeName = activeName;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getActiveType() {
		return this.activeType;
	}

	public void setActiveType(Long activeType) {
		this.activeType = activeType;
	}

	public Date getBeginDatetime() {
		return this.beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public BigDecimal getScore() {
		return this.score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public Long getPlayCount() {
		return this.playCount;
	}

	public void setPlayCount(Long playCount) {
		this.playCount = playCount;
	}

	public String getActiveHelp() {
		return activeHelp;
	}

	public void setActiveHelp(String activeHelp) {
		this.activeHelp = activeHelp;
	}

	public Integer getAwardCount() {
		return awardCount;
	}

	public void setAwardCount(Integer awardCount) {
		this.awardCount = awardCount;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getActiveRole() {
		return activeRole;
	}

	public void setActiveRole(String activeRole) {
		this.activeRole = activeRole;
	}

	public String getActiveRemark() {
		return activeRemark;
	}

	public void setActiveRemark(String activeRemark) {
		this.activeRemark = activeRemark;
	}
}
