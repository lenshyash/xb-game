package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

@Table(name = "qz_group")
public class Qzgroup{
	
	/*
	 * 正常群组
	 */
	public static Long GROUP_STATUS_NORMAL = 1L;
	/*
	 * 已解散群组
	 */
	public static Long GROUP_STATUS_CLOSE = 2L;
	
	@Column(name="id",primarykey=true)
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name = "status")
	private Long status;
	
	@Column(name = "remark")
	private String remark;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	
	
}
