package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="sys_code_maker")
public class SysCodeMaker {
	
	@Column(primarykey=true,name="id")
	private Long id;
	
	@Column(name="key")
	private String key;
	
	@Column(name="seq")
	private Long seq;
	
	@Column(name="create_datetime")
	private Date createDatetime;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
