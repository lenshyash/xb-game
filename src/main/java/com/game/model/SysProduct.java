package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_product")
public class SysProduct {
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 商品名称
	 */
	@Column(name = "product_name", length = 200)
	private String productName;

	/**
	 * 商品描述
	 */
	@Column(name = "product_desc")
	private String productDesc;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 价值
	 */
	@Column(name = "price")
	private BigDecimal price;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDesc() {
		return this.productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}
}
