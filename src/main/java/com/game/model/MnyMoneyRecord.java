package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
import org.jay.frame.jdbc.model.BaseModel;

import javax.persistence.Id;

@Data
@Table(name = "mny_money_record")
public class MnyMoneyRecord extends BaseModel {

	@Id
	@Column(name = "id", primarykey = true, generator = Column.PK_BY_HAND)
	private Long id;

	@Column(name = "account_id")
	private Long accountId;

	@Column(name = "before_money")
	private BigDecimal beforeMoney;

	@Column(name = "after_money")
	private BigDecimal afterMoney;

	@Column(name = "money")
	private BigDecimal money;

	@Column(name = "fee")
	private BigDecimal fee;

	@Column(name = "type")
	private Long type;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "create_user_id")
	private Long createUserId;

	@Column(name = "create_datetime")
	private Date createDatetime;

	@Column(name = "remark")
	private String remark;
	/**
	 * 订单号
	 */
	@Column(name = "order_id")
	private String orderId;
	@Column(name = "parents")
	private String parents;// 用户的层级关系
	@Column(name = "biz_datetime")
	private Date bizDatetime;// 账变对应的订单时间
	
	@Column(name = "account")
	private String account;
	//操作者id
	@Column(name = "operator_id")
	private Long operatorId;
	//操作员账号
	@Column(name = "operator_name")
	private String operatorName;

	@Column(name = "account_type",temp = true)
	private Long accountType;

	@Column(name = "report_type",temp = true)
	private Long reportType;

	@Column(name = "moneyType",temp = true)
	private Long moneyType;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getBeforeMoney() {
		return beforeMoney;
	}

	public void setBeforeMoney(BigDecimal beforeMoney) {
		this.beforeMoney = beforeMoney;
	}

	public BigDecimal getAfterMoney() {
		return afterMoney;
	}

	public void setAfterMoney(BigDecimal afterMoney) {
		this.afterMoney = afterMoney;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public Long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Date getBizDatetime() {
		return bizDatetime;
	}

	public void setBizDatetime(Date bizDatetime) {
		this.bizDatetime = bizDatetime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(Long operatorId) {
		this.operatorId = operatorId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public Long getAccountType() {
		return accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public Long getMoneyType() {
		return moneyType;
	}

	public void setMoneyType(Long moneyType) {
		this.moneyType = moneyType;
	}
}