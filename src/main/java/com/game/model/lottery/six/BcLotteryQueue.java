package com.game.model.lottery.six;

import java.util.List;

import com.game.model.lottery.LotteryEnum;

public class BcLotteryQueue
{

	private String name;	//名次或者位数
	
	private String names;	//大，小，单，双，龙，虎 等
	
	private Integer sortNum;	//长龙次数
	
	private LotteryEnum le;	//code
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNames()
	{
		return names;
	}

	public void setNames(String names)
	{
		this.names = names;
	}

	public Integer getSortNum()
	{
		return sortNum;
	}

	public void setSortNum(Integer sortNum)
	{
		this.sortNum = sortNum;
	}

	public LotteryEnum getLe()
	{
		return le;
	}

	public void setLe(LotteryEnum le)
	{
		this.le = le;
	}
	
}
