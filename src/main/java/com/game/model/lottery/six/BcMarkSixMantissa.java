package com.game.model.lottery.six;

public enum BcMarkSixMantissa
{
	$0("10,20,30,40","0尾",1),
	$1("01,11,21,31,41","1尾",2),
	$2("02,12,22,32,42","2尾",3),
	$3("03,13,23,33,43","3尾",4),
	$4("04,14,24,34,44","4尾",5),
	$5("05,15,25,35,45","5尾",6),
	$6("06,16,26,36,46","6尾",7),
	$7("07,17,27,37,47","7尾",8),
	$8("08,18,28,38,48","8尾",9),
	$9("09,19,29,39,49","9尾",10);
	
	private String info;
	private String infoName;
	private Integer sortNo;
	
	private BcMarkSixMantissa(String info,String infoName,Integer sortNo){
		this.info = info;
		this.infoName = infoName;
		this.sortNo = sortNo;
	}
	public String getInfoName(){
		return infoName;
	}
	public String getInfo(){
		return info;
	}
	public Integer getSortNo(){
		return sortNo;
	}

}
