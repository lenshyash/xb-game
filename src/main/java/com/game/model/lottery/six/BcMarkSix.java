package com.game.model.lottery.six;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bc_mark_six")
public class BcMarkSix
{
	@Column(name = "id", primarykey = true)
	private Long id;
	
	//数字号码或者类型(例如：波类，单双，大小，尾数)
	@Column(name="mark_type")
	private String markType;
	
	//名称(例如：特码大，特么小)
	@Column(name="name")
	private String name;

	//赔率
	@Column(name="odds")
	private BigDecimal odds;
	
	//最大下注金额
	@Column(name="max_bet_ammount")
	private BigDecimal maxBetAmmount;
	
	//最小下注金额
	@Column(name="min_bet_ammount")
	private BigDecimal minBetAmmount;
	
	//相对应的数字，可以为空，会自动生成
	@Column(name="correspond_num")
	private String correspondNum;
	
	//六合彩小类code
	@Column(name="play_code")
	private String playCode;
	
	//序号
	@Column(name="sort_no")
	private Integer sortNo;
	
	//状态
	@Column(name="status")
	private Integer status;
	
	//模版状态
	@Column(name="module_status")
	private Integer moduleStatus;
	
	//租户Id
	@Column(name="station_id")
	private Long stationId;
	
	//当为本命年和尾数0时，状态为1，其余状态0
	@Column(name="is_now_year")
	private Integer isNowYear;
	
	//如果是选中按钮时，提供最少选择个数
	@Column(name="min_selected")
	private Integer minSelected;
	
	//反水
	@Column(name="rake_back")
	private Integer rakeBack;
	
	@Column(name="lot_type")
	private Integer lotType;
	
	@Column(name="room_id")
	private Long roomId;
	

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public Integer getRakeBack()
	{
		return rakeBack;
	}

	public void setRakeBack(Integer rakeBack)
	{
		this.rakeBack = rakeBack;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getModuleStatus()
	{
		return moduleStatus;
	}

	public void setModuleStatus(Integer moduleStatus)
	{
		this.moduleStatus = moduleStatus;
	}

	public String getMarkType()
	{
		return markType;
	}

	public void setMarkType(String markType)
	{
		this.markType = markType;
	}

	public BigDecimal getOdds()
	{
		return odds;
	}

	public void setOdds(BigDecimal odds)
	{
		this.odds = odds;
	}

	public BigDecimal getMaxBetAmmount()
	{
		return maxBetAmmount;
	}

	public void setMaxBetAmmount(BigDecimal maxBetAmmount)
	{
		this.maxBetAmmount = maxBetAmmount;
	}

	public BigDecimal getMinBetAmmount()
	{
		return minBetAmmount;
	}

	public void setMinBetAmmount(BigDecimal minBetAmmount)
	{
		this.minBetAmmount = minBetAmmount;
	}

	public String getCorrespondNum()
	{
		return correspondNum;
	}

	public void setCorrespondNum(String correspondNum)
	{
		this.correspondNum = correspondNum;
	}


	public String getPlayCode()
	{
		return playCode;
	}

	public void setPlayCode(String playCode)
	{
		this.playCode = playCode;
	}

	public Integer getSortNo()
	{
		return sortNo;
	}

	public void setSortNo(Integer sortNo)
	{
		this.sortNo = sortNo;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public Long getStationId()
	{
		return stationId;
	}

	public void setStationId(Long stationId)
	{
		this.stationId = stationId;
	}

	public Integer getIsNowYear()
	{
		return isNowYear;
	}

	public void setIsNowYear(Integer isNowYear)
	{
		this.isNowYear = isNowYear;
	}

	public Integer getMinSelected()
	{
		return minSelected;
	}

	public void setMinSelected(Integer minSelected)
	{
		this.minSelected = minSelected;
	}

	public Integer getLotType()
	{
		return lotType;
	}

	public void setLotType(Integer lotType)
	{
		this.lotType = lotType;
	}
	
}
