package com.game.model.lottery.six;

import org.apache.commons.lang3.StringUtils;

import com.game.util.MarkSixTool;


/**
 * 生肖
 * @author Administrator
 *
 */
public class BcMarkSixChineseZodiac
{
	//定义当年的生肖
//	public static String NOW_YEAR = MarkSixTool.getYear();
	
	public static enum BcMarkSixZodiac{
		shu("鼠",1),niu("牛",2),hu("虎",3),tu("兔",4),lonG("龙",5),she("蛇",6),ma("马",7),yang("羊",8),hou("猴",9),ji("鸡",10),gou("狗",11),zhu("猪",12);
		
		private String info;
		private Integer sortNO;

		private BcMarkSixZodiac(String info,Integer sortNO){
			this.info = info;
			this.sortNO = sortNO;
		}
		
		public String getInfo(){
			return this.info;
		}
		public Integer getSortNO(){
			return this.sortNO;
		}
		
		public static BcMarkSixZodiac getNowYear(){
			return valueOf(MarkSixTool.getYear());
		}
	}
	
	//根据传入的枚举生肖获取到相应的生肖号码
	public static String getNumArray(String zodiac,boolean flag){
		if(StringUtils.isEmpty(zodiac)){
			return null;
		}
		String NOW_YEAR = MarkSixTool.getYear();
		String array = "";
		//获取本命年的生肖索引
		Integer index = BcMarkSixZodiac.valueOf(NOW_YEAR).ordinal();
		int lun = 12;
		int chazhi  = index + 1;
		for(int x=0;x<4;x++){
			int i=0;
		for(BcMarkSixZodiac mark:BcMarkSixZodiac.values()){
			if(mark.name().equals(NOW_YEAR)){
				if(mark.name().equals(zodiac)){
				array = array + resultAdd0(((x*lun)+i+1)) + ",";
				}
			}else{
				if(mark.ordinal() > index){
					if(mark.name().equals(zodiac)){
						array = array + resultAdd0(((x*lun)+(lun-i))) + ",";
					}
				i = i+1;
				}else{
					if(mark.name().equals(zodiac)){
						array = array + resultAdd0(((x*lun)+(chazhi-mark.ordinal()))) + ",";
					}
				}
			}
		}
		}
		if(zodiac.equals(NOW_YEAR) && flag){
			array = array + "49,";
		}
		array = array.substring(0, array.length()-1);
		return array;
	}
	
	//根据传入的枚举生肖获取到相应的生肖号码
		public static String getNumArray(String zodiac){
			return getNumArray(zodiac,true);
		}
	
	//不足10补零
	public static String resultAdd0(int i){
		String res = "0";
		if(i < 10){
			return res+i;
		}
		return String.valueOf(i);
	}
	
	
	public static void main(String[] args)
	{
		for(BcMarkSixZodiac z:BcMarkSixZodiac.values()){
			//System.out.println(z.getInfo() + "--"+BcMarkSixChineseZodiac.getNumArray(z.name()));
		}
		System.out.println(BcMarkSixChineseZodiac.resultAdd0(5));
		System.out.println(BcMarkSixChineseZodiac.getNumArray(MarkSixTool.getYear()));
//		System.out.println(MarkSixZodiac.getNumArray("lonG"));
//		
//		MarkSixZodiac markSixZodiac = MarkSixZodiac.valueOf("she");
//		System.out.println(markSixZodiac.name());
//		System.out.println(MarkSixZodiac.values().length);
//		int i = 5;
//		int j = 3;
//		int sum = 1;
//		for(int x=i;x>i-j;x--){
//			int a = x;
//			System.out.println(a);
//			sum = sum*a;
//		}
//		System.out.println(sum);

	}
	
}

