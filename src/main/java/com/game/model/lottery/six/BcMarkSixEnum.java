package com.game.model.lottery.six;

public enum BcMarkSixEnum
{
	//如果是数字，则需要循环输出1-49，如果是生肖，则需要循环12生肖
	//如果是尾数则需要循环输出0-9，如果是其他则无需处理。只需要相应的循环输出
	shuzi("数字"),qita("其他"),shengxiao("生肖"),weishu("尾数");
	
	private String info;
	
	private BcMarkSixEnum(String info){
		this.info = info;
	}
	
	public String getInfo(){
		return info;
	}
}
