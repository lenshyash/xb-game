package com.game.model.lottery;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class BcLotteryTimeVO {
	private Long id;

	/**
	 * 彩票编码
	 */
	private String lotCode;

	/**
	 * 期号，每天的期号
	 */
	private Integer actionNo;

	/**
	 * 开奖时间,
	 * 
	 * fastjson 转成json是，需要给定格式
	 */
	private Date actionTime;
	/**
	 * 期号
	 */
	private String qiHao;
	
	private String lastHaoMa;
	
	private String lastQiHao;
	
	private Date startTime;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public Integer getActionNo() {
		return actionNo;
	}

	public void setActionNo(Integer actionNo) {
		this.actionNo = actionNo;
	}

	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public String getQiHao() {
		return qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getLastHaoMa()
	{
		return lastHaoMa;
	}

	public void setLastHaoMa(String lastHaoMa)
	{
		this.lastHaoMa = lastHaoMa;
	}

	public String getLastQiHao()
	{
		return lastQiHao;
	}

	public void setLastQiHao(String lastQiHao)
	{
		this.lastQiHao = lastQiHao;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

}
