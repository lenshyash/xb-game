package com.game.model.lottery;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "bc_lottery_data_last")
public class BcLotteryDataLast {
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 彩票编码
	 */
	@Column(name = "lot_code", length = 20)
	private String lotCode;

	/**
	 * 期号
	 */
	@Column(name = "qi_hao", length = 20)
	private String qiHao;

	/**
	 * 号码
	 */
	@Column(name = "hao_ma", length = 40)
	private String haoMa;

	/**
	 * 开始投注时间
	 */
	@Column(name = "start_time")
	private Date startTime;
	/**
	 * 投注结束后，开盘时间
	 */
	@Column(name = "end_time")
	private Date endTime;

	public String getLotCode() {
		return this.lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getQiHao() {
		return this.qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getHaoMa() {
		return this.haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
