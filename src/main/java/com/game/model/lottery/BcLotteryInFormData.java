package com.game.model.lottery;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bc_lottery_inform_data")
public class BcLotteryInFormData {
	
	/**
	 * 禁用 、关闭
	 */
	public static final Integer STATUS_DISABLED = 1;
	/**
	 * 启用
	 */
	public static final Integer STATUS_ENABLED = 2;

	@Column(name = "id", primarykey = true)
	private Long id;
	
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="content")
	private String content;
	
	@Column(name="type_id")
	private Integer typeId;
	
	@Column(name="status")
	private Integer status;
	
	@Column(name="aporator")
	private String aporator;
	
	@Column(name="create_time")
	private Date createTime;
	
	@Column(name="update_time")
	private Date updateTime;
	
	@Column(name="room_id")
	private Long roomId;
	
	private String typeName;
	
	private String roomName;
	

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAporator() {
		return aporator;
	}

	public void setAporator(String aporator) {
		this.aporator = aporator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	
	
}
