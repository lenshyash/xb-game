
package com.game.model.lottery;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bc_lottery_robot")
public class BcLotteryRobot {
	
	/**
	 * 禁用 、关闭
	 */
	public static final Integer STATUS_INT = 1;
	/**
	 * 启用
	 */
	public static final Integer STATUS_FLOAT = 2;
	
	@Column(name = "id", primarykey = true)
	private Long id;
	
	/**
	 * 机器人名称
	 */
	@Column(name="account")
	private String account;
	
	/**
	 * 图片Id，限制在1-111内
	 */
	@Column(name="img_id")
	private String imgId;
	
	/**
	 * 所属房间Id
	 */
	@Column(name="room_id")
	private Long roomId;
	
	/**该机器人最小投注金额
	 * 
	 */
	@Column(name="bet_min_money")
	private BigDecimal betMinMoney;
	
	/**
	 * 该机器人最大投注金额
	 */
	@Column(name="bet_max_money")
	private BigDecimal betMaxMoney;
	
	/**
	 * 机器人所属分层
	 */
	@Column(name="account_level")
	private Long accountLevel;
	
	/**
	 * 站点Id
	 */
	@Column(name="station_id")
	private Long stationId;
	
	@Column(name="status")
	private Integer status;
	
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	private String roomName;
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	
	public String levelName;
	

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getImgId() {
		return imgId;
	}

	public void setImgId(String imgId) {
		this.imgId = imgId;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	public BigDecimal getBetMinMoney() {
		return betMinMoney;
	}

	public void setBetMinMoney(BigDecimal betMinMoney) {
		this.betMinMoney = betMinMoney;
	}

	public BigDecimal getBetMaxMoney() {
		return betMaxMoney;
	}

	public void setBetMaxMoney(BigDecimal betMaxMoney) {
		this.betMaxMoney = betMaxMoney;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountLevel() {
		return accountLevel;
	}

	public void setAccountLevel(Long accountLevel) {
		this.accountLevel = accountLevel;
	}
	
	
}
