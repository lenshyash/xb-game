package com.game.model.lottery;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

/**
 * 发起合买
 * @author Administrator
 *
 */
@Table(name = "bc_lottery_joint_purchase")
public class BcLotteryJointPurchase {

	@Column(name = "id", primarykey = true)
	private Long id;
	
	/**
	 * 方案编号
	 */
	@Column(name="program_id")
	private String programId;
	
	/**
	 * 会员（发起人）帐号
	 */
	@Column(name="account")
	private String account;
	
	/**
	 * 会员（发起人）id
	 */
	@Column(name="account_id")
	private Long accountId;
	
	/**
	 * 彩种玩法
	 */
	@Column(name="lottery_name")
	private String lotteryName;
	
	/**
	 * 总金额 ,每份金额=总金额/总份额  =xx,结果取两位小数
	 */
	@Column(name="total_money")
	private BigDecimal totalMoney;
	
	/**
	 * 总份数
	 */
	@Column(name="branch_num")
	private Integer branchNum;
	
	/**
	 * 提成--系统设定默认1%-8% 后期可考虑根据系统的vip等级来设定或者给租户自己设定范围
	 */
	@Column(name="commission")
	private Integer commission;
	
	/**
	 * 是否公开，1完全公开，2截止后公开，3完全保密
	 */
	@Column(name="is_open")
	private Integer isOpen;
	
	/**
	 * 认购时间
	 */
	@Column(name="create_time")
	private Date createTime;
	
	/**
	 * 认购截止时间
	 */
	@Column(name="end_time")
	private Date endTime;
	
	/**
	 * 是否保底，0未保底，其余的为保底的份数
	 */
	@Column(name="them")
	private Integer them;
	
	/**
	 * 方案描述
	 */
	@Column(name="describe",length=200)
	private String describe;
	
	/**
	 * 状态类型 ,1可认购，2满员，3截止(未满员情况封盘时间就截止)，4完成，5方案失效(未满员),6撤单
	 */
	@Column(name="type")
	private Integer type;
	
	/**
	 * 状态，1未开奖，2未结算，3已结算，4未派奖完，5已经取消，6已经回滚
	 */
	@Column(name="status")
	private Integer status;
	
	/**
	 * 站点Id
	 */
	@Column(name="station_id")
	private Long stationId;
	
	/**
	 * 彩种类型
	 */
	@Column(name="lot_type")
	private Integer lotType;
	
	/**
	 * 彩种code
	 */
	@Column(name="lot_code")
	private String lotCode;
	
	/**
	 * 当前期号
	 */
	@Column(name="qi_hao")
	private String qiHao;
	
	/**
	 * 预留字段
	 */
	@Column(name="ext1")
	private String ext1;
	
	/**
	 * 计算注数
	 */
	@Column(name="cal_num")
	private Integer calNum;
	
	/**
	 * 中奖总金额
	 */
	@Column(name="win_total_money")
	private BigDecimal winTotalMoney;
	
	/**
	 * 中奖注数
	 */
	@Column(name="win_num")
	private Integer winNum;
	
	/**
	 * 发起合买时所下注的订单注数
	 */
	@Column(name="buy_num")
	private Integer buyNum;
	
	//不存储数据库
	private BigDecimal oneMoney;	//购买每份金额
	private Integer surNum;		//剩余份数
	private Integer oneBFL;	//每份百分率
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}


	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getLotteryName() {
		return lotteryName;
	}

	public void setLotteryName(String lotteryName) {
		this.lotteryName = lotteryName;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public Integer getBranchNum() {
		return branchNum;
	}

	public void setBranchNum(Integer branchNum) {
		this.branchNum = branchNum;
	}

	public Integer getCommission() {
		return commission;
	}

	public void setCommission(Integer commission) {
		this.commission = commission;
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getThem() {
		return them;
	}

	public void setThem(Integer them) {
		this.them = them;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLotType() {
		return lotType;
	}

	public void setLotType(Integer lotType) {
		this.lotType = lotType;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getQiHao() {
		return qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public BigDecimal getOneMoney() {
		return oneMoney;
	}

	public void setOneMoney(BigDecimal oneMoney) {
		this.oneMoney = oneMoney;
	}

	public Integer getSurNum() {
		return surNum;
	}

	public void setSurNum(Integer surNum) {
		this.surNum = surNum;
	}

	public Integer getOneBFL() {
		return oneBFL;
	}

	public void setOneBFL(Integer oneBFL) {
		this.oneBFL = oneBFL;
	}

	public Integer getCalNum() {
		return calNum;
	}

	public void setCalNum(Integer calNum) {
		this.calNum = calNum;
	}

	public BigDecimal getWinTotalMoney() {
		return winTotalMoney;
	}

	public void setWinTotalMoney(BigDecimal winTotalMoney) {
		this.winTotalMoney = winTotalMoney;
	}

	public Integer getWinNum() {
		return winNum;
	}

	public void setWinNum(Integer winNum) {
		this.winNum = winNum;
	}

	public Integer getBuyNum() {
		return buyNum;
	}

	public void setBuyNum(Integer buyNum) {
		this.buyNum = buyNum;
	}
	
}
