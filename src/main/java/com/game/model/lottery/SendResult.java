package com.game.model.lottery;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

/**
 * 推送结果
 */
@Table(name="bc_lottery_send_result")
public class SendResult  {
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		彩种编码
	*/
	@Column(name="code")
	private String code;
	
	/**
		开奖号码
	*/
	@Column(name="hao_ma")
	private String haoMa;
	
	/**
		遗漏
	*/
	@Column(name="ommit")
	private String ommit;
	
	/**
		期号
	*/
	@Column(name="qi_hao")
	private String qiHao;
	
	/**
		状态
	*/
	@Column(name="status")
	private Integer status;
	
	/**
		站点ID
	*/
	@Column(name="station_id")
	private Long stationId;
	
	/**
		最后推送时间
	*/
	@Column(name="update_time")
	private Date updateTime;
	
	/**
		开始推送时间
	*/
	@Column(name="create_time")
	private Date createTime;
	
	/**
		失败次数
	*/
	@Column(name="faile_num")
	private Integer faileNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getHaoMa() {
		return haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public String getOmmit() {
		return ommit;
	}

	public void setOmmit(String ommit) {
		this.ommit = ommit;
	}

	public String getQiHao() {
		return qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getFaileNum() {
		return faileNum;
	}

	public void setFaileNum(Integer faileNum) {
		this.faileNum = faileNum;
	}
	
	
}

