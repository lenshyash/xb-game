package com.game.model.lottery;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "bc_lottery_data_station")
public class BcLotteryDataStation {
	@Column(name = "id", primarykey = true)
	private Long id;
	@Column(name = "lot_data_id")
	private Long lotDataId;
	/**
	 * 彩票编码
	 */
	@Column(name = "lot_code", length = 20)
	private String lotCode;

	/**
	 * 期号
	 */
	@Column(name = "qi_hao", length = 20)
	private String qiHao;

	/**
	 * 号码
	 */
	@Column(name = "hao_ma", length = 40)
	private String haoMa;

	/**
	 * 开奖状态{1未开奖,2未派奖,3已派奖, 4未派奖完，5已经取消,6已经回滚}
	 */
	@Column(name = "open_status")
	private Integer openStatus;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLotCode() {
		return this.lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getQiHao() {
		return this.qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getHaoMa() {
		return this.haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public Integer getOpenStatus() {
		return this.openStatus;
	}

	public void setOpenStatus(Integer openStatus) {
		this.openStatus = openStatus;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getLotDataId() {
		return lotDataId;
	}

	public void setLotDataId(Long lotDataId) {
		this.lotDataId = lotDataId;
	}

}
