package com.game.model.lottery;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

import com.game.model.vo.AccountVo;

@Table(name = "bc_lottery_order")
public class BcLotteryOrder {
	public static final int tz_status_end = 1;// 投注结束
	public static final int tz_status_betting = 3;// 投注中
	public static final String joint_status_cg = "1";// 常规投注
	public static final String joint_status_hm = "2";// 合买投注

	public static final String SYS_ALL = "sysAll";// 所有系统彩
	public static final String SYS_NO_SF = "sysNoSF";// 所有系统彩不含十分六合
	public static final String COMMON = "common";// 官方彩
	public static final String OFFICIALLHC = "officialLHC";// 官方六合彩
	
	public static final Integer cancel_switch_on = 1;//开启撤单
	public static final Integer cancel_switch_off = 0;//关闭撤单

	@Column(name = "id", temp = true)
	private Long id;
	/**
	 * 订单号
	 */
	@Column(name = "order_id", length = 36)
	private String orderId;

	/**
	 * 会员账号名
	 */
	@Column(name = "account", length = 50)
	private String account;
	@Column(name = "account_id")
	private Long accountId;
	/**
	 * 彩票编号
	 */
	@Column(name = "lot_code", length = 20)
	private String lotCode;
	/**
	 * 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋
	 */
	@Column(name = "lot_type")
	private Integer lotType;

	/**
	 * 彩票期号
	 */
	@Column(name = "qi_hao", length = 20)
	private String qiHao;

	/**
	 * 玩法编码
	 */
	@Column(name = "play_code", length = 20)
	private String playCode;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 购买的号码
	 */
	@Column(name = "hao_ma")
	private String haoMa;

	/**
	 * 购买时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 开奖时间
	 */
	@Column(name = "open_time")
	private Date openTime;

	/**
	 * 购买注数
	 */
	@Column(name = "buy_zhu_shu")
	private Integer buyZhuShu;

	/**
	 * 中奖注数
	 */
	@Column(name = "win_zhu_shu")
	private Integer winZhuShu;

	/**
	 * 购买倍数
	 */
	@Column(name = "multiple")
	private Integer multiple;

	/**
	 * 购买金额
	 */
	@Column(name = "buy_money")
	private BigDecimal buyMoney;

	/**
	 * 中奖金额
	 */
	@Column(name = "win_money")
	private BigDecimal winMoney;

	/**
	 * 状态 1等待开奖 2已中奖 3未中奖 4撤单 5派奖回滚成功 6回滚异常的 7开奖异常
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 模式 1元 10角 100分
	 */
	@Column(name = "model")
	private Integer model;

	/**
	 * 追号标识，不是追号＝1，中奖继续＝2，中奖中止＝追号第一期的order_id
	 */
	@Column(name = "zhui_hao", length = 36)
	private String zhuiHao;

	/**
	 * 会员返水状态 （1，还未返水 2，已经返水,还未到账 3，返水已经回滚 ,4 反水已经到账 ）多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
	 */
	@Column(name = "roll_back_status")
	private Integer rollBackStatus;

	/**
	 * 多级表示返点(1，还未返点 2，已经返点 3，返点已经回滚)
	 */
	@Column(name = "agent_rebate")
	@InsertValue(value = "1")
	private Integer agentRebate;

	/**
	 * 六合彩赔率表的id
	 */
	@Column(name = "mark_six_id")
	private Long markSixId;

	/**
	 * 开奖时间跟封盘时间差，单位秒
	 */
	@Column(name = "ago")
	private Integer ago;
	@Column(name = "parents")
	private String parents;

	@Column(name = "odds")
	private BigDecimal odds;
	/**
	 * 1常规(追号)下注，合买=合买方案编号
	 */
	@Column(name = "joint_purchase")
	private String jointPurchase;

	/**
	 * v4版本房间号标识
	 */
	@Column(name = "room_id")
	private Long roomId;

	/**
	 * 日志对应ID
	 */
	@Column(name = "log_id")
	private Long logId;
	
	/**
	 * 群组名称
	 */
	@Column(name = "qz_group_name")
	private String qzGroupName;
	
	/**
	 * 投注IP
	 */
	@Column(name = "bet_ip")
	private String betIp;
	/**
	 * 会员备注
	 */
	@Column(name = "remark", temp = true)
	private String remark;

	public String getQzGroupName() {
		return qzGroupName;
	}

	public void setQzGroupName(String qzGroupName) {
		this.qzGroupName = qzGroupName;
	}

	public Long getRoomId() {
		return roomId;
	}

	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}

	private String peilv;// 赔率,不存库
	private Date sellingTime;// 该期开盘时间
	private Date sealTime;// 该期封盘时间
	public String groupName;// 大类名字
	public String groupCode;// 大类Code
	public Integer tzStatus;// 投注状态 1＝投注结束，3=投注中
	private BigDecimal xBet;
	private BigDecimal xWin;
	private BigDecimal jointWinOneMoney;
	private String lhcSx;
	private String roomName;
	private String showOdds;
	private Integer cancelSwitch;//1开启，0关闭
	@Column(name="agent_name",temp=true)
	private String agentName;
	public String getLhcSx() {
		return lhcSx;
	}

	public void setLhcSx(String lhcSx) {
		this.lhcSx = lhcSx;
	}

	public String getPeilv() {
		return peilv;
	}

	public void setPeilv(String peilv) {
		this.peilv = peilv;
	}

	/**
	 * 玩法小类名称
	 * 
	 * @return
	 */
	private String playName;

	// 做辅助展示用
	private AccountVo avo;

	public AccountVo getAvo() {
		return avo;
	}

	public void setAvo(AccountVo avo) {
		this.avo = avo;
	}

	/**
	 * 开奖号码
	 */
	private String lotteryHaoMa;

	/**
	 * 彩种名称
	 * 
	 * @return
	 */
	private String lotName;

	/**
	 * 返水比例
	 * 
	 * @return
	 */
	private String rollBackRate;

	/**
	 * 返水金额
	 * 
	 * @return
	 */
	private BigDecimal rollBackMoney;

	/**
	 * 盈亏输赢
	 * 
	 * @return
	 */
	private BigDecimal yingKui;

	/**
	 * 彩种奖金
	 * 
	 * @return
	 */
	private BigDecimal minBonusOdds;
	/**
	 * 彩种图标
	 * 
	 * @return
	 */
	private String lotUrl;
	private String imgUrl;
	@Column(name = "abnormal_flag", temp = true)
	private Integer abnormalFlag;

	public BigDecimal getMinBonusOdds() {
		return minBonusOdds;
	}

	public void setMinBonusOdds(BigDecimal minBonusOdds) {
		this.minBonusOdds = minBonusOdds;
	}

	public BigDecimal getYingKui() {
		return yingKui;
	}

	public void setYingKui(BigDecimal yingKui) {
		this.yingKui = yingKui;
	}

	public String getRollBackRate() {
		return rollBackRate;
	}

	public void setRollBackRate(String rollBackRate) {
		this.rollBackRate = rollBackRate;
	}

	public BigDecimal getRollBackMoney() {
		return rollBackMoney;
	}

	public void setRollBackMoney(BigDecimal rollBackMoney) {
		this.rollBackMoney = rollBackMoney;
	}

	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotteryHaoMa() {
		return lotteryHaoMa;
	}

	public void setLotteryHaoMa(String lotteryHaoMa) {
		this.lotteryHaoMa = lotteryHaoMa;
	}

	public String getPlayName() {
		return playName;
	}

	public void setPlayName(String playName) {
		this.playName = playName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public Integer getLotType() {
		return lotType;
	}

	public void setLotType(Integer lotType) {
		this.lotType = lotType;
	}

	public String getQiHao() {
		return qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getPlayCode() {
		return playCode;
	}

	public void setPlayCode(String playCode) {
		this.playCode = playCode;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getHaoMa() {
		return haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getOpenTime() {
		return openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public Integer getBuyZhuShu() {
		return buyZhuShu;
	}

	public void setBuyZhuShu(Integer buyZhuShu) {
		this.buyZhuShu = buyZhuShu;
	}

	public Integer getWinZhuShu() {
		return winZhuShu;
	}

	public void setWinZhuShu(Integer winZhuShu) {
		this.winZhuShu = winZhuShu;
	}

	public Integer getMultiple() {
		return multiple;
	}

	public void setMultiple(Integer multiple) {
		this.multiple = multiple;
	}

	public BigDecimal getBuyMoney() {
		return buyMoney;
	}

	public void setBuyMoney(BigDecimal buyMoney) {
		this.buyMoney = buyMoney;
	}

	public BigDecimal getWinMoney() {
		return winMoney;
	}

	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getModel() {
		return model;
	}

	public void setModel(Integer model) {
		this.model = model;
	}

	public String getZhuiHao() {
		return zhuiHao;
	}

	public void setZhuiHao(String zhuiHao) {
		this.zhuiHao = zhuiHao;
	}

	public Integer getRollBackStatus() {
		return rollBackStatus;
	}

	public void setRollBackStatus(Integer rollBackStatus) {
		this.rollBackStatus = rollBackStatus;
	}

	public Long getMarkSixId() {
		return markSixId;
	}

	public void setMarkSixId(Long markSixId) {
		this.markSixId = markSixId;
	}

	public Date getSellingTime() {
		return sellingTime;
	}

	public void setSellingTime(Date sellingTime) {
		this.sellingTime = sellingTime;
	}

	public Date getSealTime() {
		return sealTime;
	}

	public void setSealTime(Date sealTime) {
		this.sealTime = sealTime;
	}

	public Integer getAgo() {
		return ago;
	}

	public void setAgo(Integer ago) {
		this.ago = ago;
	}

	public String getParents() {
		return parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Integer getTzStatus() {
		return tzStatus;
	}

	public void setTzStatus(Integer tzStatus) {
		this.tzStatus = tzStatus;
	}

	public BigDecimal getxBet() {
		return xBet;
	}

	public void setxBet(BigDecimal xBet) {
		this.xBet = xBet;
	}

	public BigDecimal getxWin() {
		return xWin;
	}

	public void setxWin(BigDecimal xWin) {
		this.xWin = xWin;
	}

	public BigDecimal getOdds() {
		return odds;
	}

	public void setOdds(BigDecimal odds) {
		this.odds = odds;
	}

	public String getJointPurchase() {
		return jointPurchase;
	}

	public void setJointPurchase(String jointPurchase) {
		this.jointPurchase = jointPurchase;
	}

	public BigDecimal getJointWinOneMoney() {
		return jointWinOneMoney;
	}

	public void setJointWinOneMoney(BigDecimal jointWinOneMoney) {
		this.jointWinOneMoney = jointWinOneMoney;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getShowOdds() {
		return showOdds;
	}

	public void setShowOdds(String showOdds) {
		this.showOdds = showOdds;
	}

	public Integer isCancelSwitch() {
		return cancelSwitch;
	}

	public void setCancelSwitch(Integer cancelSwitch) {
		this.cancelSwitch = cancelSwitch;
	}

	public String getLotUrl() {
		return lotUrl;
	}

	public void setLotUrl(String lotUrl) {
		this.lotUrl = lotUrl;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getBetIp() {
		return betIp;
	}

	public void setBetIp(String betIp) {
		this.betIp = betIp;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getAbnormalFlag() {
		return abnormalFlag;
	}

	public void setAbnormalFlag(Integer abnormalFlag) {
		this.abnormalFlag = abnormalFlag;
	}
}
