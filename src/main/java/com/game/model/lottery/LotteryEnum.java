package com.game.model.lottery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.StringUtil;

public enum LotteryEnum {
	BJSC("北京赛车", 10,44, "20分钟"), XYFT("幸运飞艇", 10, 180, "5分钟"), LXYFT("老幸运飞艇", 10, 180, "5分钟"), AZXY10("澳洲幸运10", 10, 288, "5分钟"), SFFT("极速飞艇", 10, 480, "3分钟", true), SFSC("极速赛车", 10, 480, "3分钟", true), WFSC("五分赛车", 10, 288, "5分钟", true),WFFT("五分飞艇", 10, 179, "5分钟", true),LBJSC("老北京赛车", 10, 179, "5分钟", true),
	XSFSC("新极速赛车", 10, 480, "3分钟", true), XWFSC("幸运赛车", 10, 288, "5分钟", true),
	SH11X5("上海11选5", 5, 45, "20分钟"), JX11X5("江西11选5", 5, 42, "20分钟"), SD11X5("山东11选5", 5, 43, "20分钟"), GD11X5("广东11选5", 5, 42, "20分钟"),GX11X5("广西11选5", 5, 45, "20分钟"),
	FF11X5("极速11选5", 5, 1440, "1分钟",true),SF11X5("三分11选5", 5, 480, "3分钟",true),WF11X5("五分11选5", 5, 288, "5分钟",true),


	PL3("排列三", 3, 1, "24小时"), FC3D("福彩3D", 3, 1, "24小时"),FF3D("极速3D", 3, 1440, "1分钟",true),WF3D("五分3D", 3, 288, "五分钟",true),

	CQSSC("重庆时时彩", 5, 59, "20分钟"), XJSSC("新疆时时彩", 5, 48, "20分钟"), TJSSC("天津时时彩", 5, 42, "20分钟"),

	FFC("分分彩", 5, 1440, "1分钟", true), EFC("二分彩", 5, 720, "2分钟", true), AMWFC("澳门五分彩", 5, 288, "5分钟", true), HKWFC("香港五分彩", 5, 288, "5分钟", true), WFC("五分彩", 5, 288, "5分钟", true),
	SFC("十分彩", 5, 144, "10分钟", true), ESFC("二十分彩", 5, 72, "20分钟", true),
	
	LHC("六合彩", 7, 1, "二 四 六"),AMLHC("澳门六合彩", 7, 1, "24小时"), SFLHC("10分六合彩", 7, 144, "10分钟", true),WFLHC("五分六合彩", 7, 288, "5分钟", true),HKMHLHC("香港马会六合彩", 7, 1, "24小时", true),TMLHC("三分六合彩", 7, 480, "3分钟", true),

	HNKLSF("湖南快乐十分", 8, 84, "10分钟"), GDKLSF("广东快乐十分", 8, 42, "20分钟"), CQXYNC("重庆幸运农场", 8, 97, "10分钟"),SFKLSF("三分快乐十分", 8, 480, "3分钟",true),WFKLSF("五分快乐十分", 8, 288, "5分钟",true),

	JSSB3("江苏骰宝(快3)", 3, 41, "20分钟"), AHK3("安徽快三", 3, 40, "20分钟"), HBK3("湖北快三", 3, 39, "20分钟"), 
	
	HEBK3("河北快三", 3, 81, "20分钟"), GXK3("广西快三", 3, 39, "20分钟"), SHHK3("上海快3", 3, 41, "20分钟"), 
	
	BJK3("北京快三", 3, 89, "20分钟"), JXK3("江西快三", 3, 42, "20分钟"), GSK3("甘肃快三", 3, 36, "20分钟"), 
	
	FFK3("极速快三", 3, 1440, "1分钟", true), WFK3("幸运快三", 3, 288, "5分钟", true), JPK3("日本快三", 3, 288, "5分钟", true), KRK3("韩国快三", 3, 288, "5分钟", true), JLK3("吉林快三", 3, 87, "10分钟"), GZK3("贵州快三", 3, 39, "20分钟"), 
	
	PCEGG("PC蛋蛋", 3, 179, "5分钟"), JND28("加拿大28", 3, 396, "3分钟半"), HNFFC("河内分分彩", 5, 1440, "1分钟"), HNWFC("河内五分彩", 5, 288, "5分钟"), AZXY5("澳洲幸运5", 5, 288, "5分钟"),
	
	TXFFC("腾讯分分彩", 5, 1440, "1分钟",false), FFSC("疯狂赛车",10, 1152, "1分15秒",true),JSSC168("168极速赛车", 10, 1152, "1分15秒"),
	
	JSSSC168("168极速时时彩",5, 1152, "1分15秒"), SFK3("十分快三", 3, 143, "10分钟", true), ESK3("二十快三", 3, 39, "20分钟", true), 
	HKK3("香港快三", 3, 72, "20分钟", true),  AMK3("澳门快三", 3, 72, "20分钟", true), 
	
	SF28("极速28", 3, 480, "3分钟", true),TMK3("三分快三", 3, 480, "3分钟", true);

	private String lotName;// 彩票名称
	private Integer ballNums;// 球数
	private Integer qiShu;// 一天期数
	private String pinLv; // 开奖频率
	private boolean sysLot = false;// 系统彩

	private LotteryEnum(String lotName, Integer ballNums, Integer qiShu, String pinLv) {
		this.lotName = lotName;
		this.ballNums = ballNums;
		this.qiShu = qiShu;
		this.pinLv = pinLv;
		this.sysLot = false;
	}

	private LotteryEnum(String lotName, Integer ballNums, Integer qiShu, String pinLv, boolean sysLot) {
		this.lotName = lotName;
		this.ballNums = ballNums;
		this.qiShu = qiShu;
		this.pinLv = pinLv;
		this.sysLot = sysLot;
	}

	public String getLotName() {
		return lotName;
	}

	public Integer getBallNums() {
		return ballNums;
	}

	public Integer getQiShu() {
		return qiShu;
	}

	public String getPinLv() {
		return pinLv;
	}

	public boolean isSysLot() {
		return sysLot;
	}
	
	public static boolean isSysLot(String name) {
		if (StringUtils.isEmpty(name))
			return false;
		try {
			LotteryEnum le = LotteryEnum.valueOf(name.toUpperCase());
			if (le != null) {
				return le.isSysLot();
			}
		} catch (Exception e) {
		}
		return false;
	}

	public static LotteryEnum getEnum(String name) {
		if (StringUtils.isEmpty(name))
			return null;
		try {
			return LotteryEnum.valueOf(name.toUpperCase());
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 获取所有官方彩
	 * @return
	 */
	public static List<LotteryEnum> getNormalLottery() {
		LotteryEnum[] lotterys = LotteryEnum.values();
		List<LotteryEnum> newLotterys = new ArrayList<>();
		for (LotteryEnum lotteryEnum : lotterys) {
			if(!lotteryEnum.isSysLot()) {
				newLotterys.add(lotteryEnum);
			}
		}
		return newLotterys;
	}
	
	/**
	 * 获取所有系统彩
	 * @return
	 */
	public static List<LotteryEnum> getSysLottery() {
		LotteryEnum[] lotterys = LotteryEnum.values();
		List<LotteryEnum> newLotterys = new ArrayList<>();
		for (LotteryEnum lotteryEnum : lotterys) {
			if(lotteryEnum.isSysLot()) {
				newLotterys.add(lotteryEnum);
			}
		}
		return newLotterys;
	}
	
	/**
	 * 判断是不是六合彩类型的彩票
	 * @param name
	 * @return
	 */
	public static boolean isMarkSix(String name) {
		if(StringUtil.isEmpty(name)) {
			return false;
		}
		LotteryEnum lotteryEnum = getEnum(name);
		if(lotteryEnum==LotteryEnum.LHC || lotteryEnum==LotteryEnum.SFLHC ||lotteryEnum==LotteryEnum.WFLHC ||lotteryEnum==LotteryEnum.TMLHC ||lotteryEnum==LotteryEnum.HKMHLHC ||lotteryEnum==LotteryEnum.AMLHC) {
			return true;
		}else {
			return false;
		}
	}
	
	// 生成彩种时间 bc_lottery_time
	public static void main(String[] args) {
//		
//		//什么时候开始
//		Calendar c = Calendar.getInstance();
//		c.set(Calendar.HOUR_OF_DAY, 9);
//		c.set(Calendar.MINUTE, 7);
//		c.set(Calendar.SECOND, 0);
//		c.set(Calendar.MILLISECOND, 0);
//		
//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
//		String str = "";
//		for (int i = 1; i < 100000; i++) {
//			
//			if(i!=1) {
//				c.add(Calendar.MINUTE, 5);  //每期多少分
//			}
//			
//			String format = sdf.format(c.getTime());
//			
//			str = "INSERT INTO bc_lottery_time(\"lot_code\", \"action_no\", \"action_time\") VALUES ('LBJSC', "+i+", '"+format+"');";
//			
//			System.out.println(str);
//			
//			if(i>250) {
//				break;
//			}
//		}
		String name = "amlhc";
		boolean markSix = LotteryEnum.isMarkSix(name);
		System.out.println(markSix);
		
	}
}
