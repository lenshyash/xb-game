package com.game.model.lottery;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;
/**
 * 参与合买
 * @author Administrator
 *
 */
@Table(name="bc_lottery_joint_purchase_pate")
public class BcLotteryJointPurchasePate {
	
	
	public static final int STATUS_SUCCESS = 1;	//认购成功
	public static final int STATUS_Win = 2;	//已中奖
	public static final int STATUS_INVALID= 3;	//认购失效Invalid
	
	@Column(name = "id", primarykey = true)
	private Long id;
	
	/**
	 * 发起者-----方案编号
	 */
	@Column(name="program_id")
	private String programId;
	
	/**
	 * 会员（参与人）帐号
	 */
	@Column(name="account")
	private String account;
	
	/**
	 * 会员（参与人）id
	 */
	@Column(name="account_id")
	private Long accountId;
	
	/**
	 * 参与人认购---总金额  = 认购份数*每份金额
	 */
	@Column(name="total_money")
	private BigDecimal totalMoney;
	
	/**
	 * 参与人认购--份数
	 */
	@Column(name="branch_num")
	private Integer branchNum;
	
	/**
	 * 参与人--认购时间
	 */
	@Column(name="create_time")
	private Date createTime;
	
	/**
	 * 参与人--分得奖金
	 */
	@Column(name="win_money")
	private BigDecimal winMoney;
	
	/**
	 * 中奖积分
	 */
	@Column(name="win_gral")
	private Integer winGral;
	
	/**
	 * 站点Id
	 */
	@Column(name="station_id")
	private Long stationId;
	
	/**
	 * 认购状态	1认购成功，2认购失败，3认购未生效（合买方案失败）
	 */
	@Column(name="status")
	private Integer status;
	
	/**
	 * 预留字段
	 */
	@Column(name="ext1")
	private String ext1;
	
	private String szBFL;	//所占比例

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public Integer getBranchNum() {
		return branchNum;
	}

	public void setBranchNum(Integer branchNum) {
		this.branchNum = branchNum;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public BigDecimal getWinMoney() {
		return winMoney;
	}

	public void setWinMoney(BigDecimal winMoney) {
		this.winMoney = winMoney;
	}

	public Integer getWinGral() {
		return winGral;
	}

	public void setWinGral(Integer winGral) {
		this.winGral = winGral;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getSzBFL() {
		return szBFL;
	}

	public void setSzBFL(String szBFL) {
		this.szBFL = szBFL;
	}
	
}
