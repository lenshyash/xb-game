package com.game.model.lottery;

import java.math.BigDecimal;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="bc_lottery_room")
public class BcLotteryRoom {

	/**
	 * 禁用 、关闭
	 */
	public static final Integer STATUS_DISABLE = 1;
	/**
	 * 启用
	 */
	public static final Integer STATUS_ENABLE = 2;
	
	@Column(name = "id", primarykey = true)
	private Long id;
	
	/**
	 * 房间名称
	 */
	@Column(name="name")
	private String name;
	
	/**
	 * 站点Id
	 */
	@Column(name="station_id")
	private Long stationId;
	
	/**
	 * 房间机器人数
	 */
	@Column(name="robot")
	private Integer robot;
	
	/**
	 * 房间标识
	 */
	@Column(name="room_house")
	private String roomHouse;
	/**
	 * 房间管理员
	 */
	@Column(name="room_admin")
	private String roomAdmin;
	
	/**
	 * 初始人数
	 */
	@Column(name="initial_count")
	private Integer initialCount;
	
	/**
	 * 最大人数
	 */
	@Column(name="max_count")
	private Integer maxCount;
	
	/**
	 * 所属彩种
	 */
	@Column(name="lot_code")
	private String lotCode;
	
	/**
	 * 简介
	 */
	@Column(name="brief")
	private String brief;
	
	/**
	 * 投注开启关闭状态
	 */
	@Column(name="bet_status")
	private Integer betStatus;
	/**
	 * 机器人开启关闭状态
	 */
	@Column(name="robot_status")
	private Integer robotStatus;
	
	/**
	 * 进入房间条件（账户金额限制）
	 */
	@Column(name="max_money")
	private BigDecimal maxMoney;
	
	/**
	 * 进入房间条件（权限等级限制）
	 */
	@Column(name="level_id")
	private Long levelId;
	
	/**
	 * 发言条件(账户金额限制)
	 */
	@Column(name="speak_money")
	private BigDecimal speakMoney;
	
	/**
	 * 房间图片设置
	 */
	@Column(name="room_img")
	private String roomImg;
	
	/**
	 * 房间状态
	 */
	@Column(name="status")
	private Integer status;
	
	/**
	 * 暗语（仅适用于加密房间）
	 */
	@Column(name="argot")
	private String argot;
	
	@Column(name="lot_type")
	private Integer lotType;
	
	/**
	 * 排序
	 * @return
	 */
	@Column(name="sort_no")
	private Integer sortNO;
	
	//后续可能需要添加反水设定等机制

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getRobot() {
		return robot;
	}

	public void setRobot(Integer robot) {
		this.robot = robot;
	}

	public String getRoomAdmin() {
		return roomAdmin;
	}

	public void setRoomAdmin(String roomAdmin) {
		this.roomAdmin = roomAdmin;
	}

	public Integer getInitialCount() {
		return initialCount;
	}

	public void setInitialCount(Integer initialCount) {
		this.initialCount = initialCount;
	}

	public Integer getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}

	public String getLotCode() {
		return lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public Integer getBetStatus() {
		return betStatus;
	}

	public void setBetStatus(Integer betStatus) {
		this.betStatus = betStatus;
	}

	public Integer getRobotStatus() {
		return robotStatus;
	}

	public void setRobotStatus(Integer robotStatus) {
		this.robotStatus = robotStatus;
	}

	public BigDecimal getMaxMoney() {
		return maxMoney;
	}

	public void setMaxMoney(BigDecimal maxMoney) {
		this.maxMoney = maxMoney;
	}

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public BigDecimal getSpeakMoney() {
		return speakMoney;
	}

	public void setSpeakMoney(BigDecimal speakMoney) {
		this.speakMoney = speakMoney;
	}

	public String getRoomImg() {
		return roomImg;
	}

	public void setRoomImg(String roomImg) {
		this.roomImg = roomImg;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRoomHouse() {
		return roomHouse;
	}

	public void setRoomHouse(String roomHouse) {
		this.roomHouse = roomHouse;
	}

	public String getArgot() {
		return argot;
	}

	public void setArgot(String argot) {
		this.argot = argot;
	}

	public Integer getLotType() {
		return lotType;
	}

	public void setLotType(Integer lotType) {
		this.lotType = lotType;
	}

	public Integer getSortNO() {
		return sortNO;
	}

	public void setSortNO(Integer sortNO) {
		this.sortNO = sortNO;
	}
	
	
}
