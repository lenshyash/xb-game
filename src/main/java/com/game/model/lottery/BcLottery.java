package com.game.model.lottery;

import java.util.List;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "bc_lottery")
public class BcLottery {
	/**
	 * 禁用 、关闭
	 */
	public static final Integer STATUS_DISABLE = 1;
	/**
	 * 启用
	 */
	public static final Integer STATUS_ENABLE = 2;
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 彩票名称，中文名称
	 */
	@Column(name = "name", length = 20)
	private String name;

	/**
	 * 彩种状态1=开启，2=关闭
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 彩票编码，如CQSSC,FFC
	 */
	@Column(name = "code", length = 20)
	private String code;

	/**
	 * 开奖时间跟封盘时间差，单位秒
	 */
	@Column(name = "ago")
	private Integer ago;

	/**
	 * 序号
	 */
	@Column(name = "sort_no")
	private Integer sortNo;

	/**
	 * 显示时分组，1=时时彩，2=低频彩，3=快开，4=快三，5=11选5，6=香港彩
	 */
	@Column(name = "view_group")
	private Integer viewGroup;

	/**
	 * 球数
	 */
	@Column(name = "balls")
	private Integer balls;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋
	 */
	@Column(name = "type")
	private Integer type;
	
	@Column(name="identify")
	private Integer identify;
	
	/**
	 * 模板状态
	 */
	@Column(name = "model_status")
	private Integer modelStatus;
	
	/**
	 * 图标链接
	 */
	@Column(name = "img_url")
	private String imgUrl;
	/**
	 * 右上角gif
	 */
	@Column(name = "gif_url")
	private String gifUrl;

	/**
	 * 优先展示快捷下注还是普通下注
	 * 1 普通
	 * 2 快捷
	 */
	@Column(name = "show_flag")
	private Integer showFlag;
	/**
	 *key
	 */
	private String key;
	
	private Integer qiShu;// 一天期数
	
	private String pinLv; // 开奖频率
	
	private boolean isTradition; // 
	
	private List<BcLotteryPlayGroup> groupList;
	
	private BcLotteryTimeVO timeVO;
	
	private Integer frequency; //区分高低频
	
	@Column(name = "hot_game")
	private Integer hotGame; //热门游戏
	
	@Column(name = "new_game")
	private Integer newGame; //最新游戏
	
	@Column(name = "limit_account")
	private String limitAccount; //限制玩家下注的彩种
	
	public List<BcLotteryPlayGroup> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<BcLotteryPlayGroup> groupList) {
		this.groupList = groupList;
	}

	public Integer getModelStatus() {
		return modelStatus;
	}

	public void setModelStatus(Integer modelStatus) {
		this.modelStatus = modelStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getAgo() {
		return ago;
	}

	public void setAgo(Integer ago) {
		this.ago = ago;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public Integer getViewGroup() {
		return viewGroup;
	}

	public void setViewGroup(Integer viewGroup) {
		this.viewGroup = viewGroup;
	}

	public Integer getBalls() {
		return balls;
	}

	public void setBalls(Integer balls) {
		this.balls = balls;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIdentify()
	{
		return identify;
	}

	public void setIdentify(Integer identify)
	{
		this.identify = identify;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public BcLotteryTimeVO getTimeVO() {
		return timeVO;
	}

	public void setTimeVO(BcLotteryTimeVO timeVO) {
		this.timeVO = timeVO;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public Integer getQiShu() {
		return qiShu;
	}

	public void setQiShu(Integer qiShu) {
		this.qiShu = qiShu;
	}

	public String getPinLv() {
		return pinLv;
	}

	public void setPinLv(String pinLv) {
		this.pinLv = pinLv;
	}

	public boolean isTradition() {
		return isTradition;
	}

	public void setTradition(boolean isTradition) {
		this.isTradition = isTradition;
	}

	public Integer getHotGame() {
		return hotGame;
	}

	public void setHotGame(Integer hotGame) {
		this.hotGame = hotGame;
	}

	public Integer getNewGame() {
		return newGame;
	}

	public void setNewGame(Integer newGame) {
		this.newGame = newGame;
	}

	public String getGifUrl() {
		return gifUrl;
	}

	public void setGifUrl(String gifUrl) {
		this.gifUrl = gifUrl;
	}

	public String getLimitAccount() {
		return limitAccount;
	}

	public void setLimitAccount(String limitAccount) {
		this.limitAccount = limitAccount;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Integer getShowFlag() {
		return showFlag==null?1:showFlag;
	}

	public void setShowFlag(Integer showFlag) {
		this.showFlag = showFlag;
	}
}
