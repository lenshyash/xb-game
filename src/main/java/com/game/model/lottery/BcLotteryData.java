package com.game.model.lottery;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import javax.persistence.Id;
import lombok.Data;
import java.util.Date;


@Data
@Table(name = "bc_lottery_data")
public class BcLotteryData {
	@Id
	@Column(name = "id",temp = true)
	private Long id;

	/**
	 * 彩票编码
	 */
	@Column(name = "lot_code", length = 20)
	private String lotCode;

	/**
	 * 期号
	 */
	@Column(name = "qi_hao", length = 20)
	private String qiHao;

	/**
	 * 号码
	 */
	@Column(name = "hao_ma", length = 40)
	private String haoMa;

	/**
	 * 遗漏
	 */
	@Column(name = "ommit", length = 500)
	private String ommit;

	/**
	 * 开始投注时间
	 */
	@Column(name = "start_time")
	private Date startTime;
	/**
	 * 投注结束后，开盘时间
	 */
	@Column(name = "end_time")
	private Date endTime;

	/**
	 * 开奖状态{1未开奖,2未派奖,3已派奖, 4未派奖完，5已经取消,6已经回滚}
	 */
	@Column(name = "open_status")
	private Integer openStatus;

	/**
	 * 开奖时间
	 */
	@Column(name = "open_time")
	private Date openTime;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;
	/**
	 * bc_lottery_time表中的action_no
	 */
	@Column(name = "action_no")
	private Integer actionNo;
	
	/**
	 * 限制当期不能投注的会员帐号记录，多个以逗号隔开
	 */
	@Column(name="limit_bet_account")
	private String limitBetAccount;
	
	//大小
	@Column(name="size",temp=true)
	private String size;
	
	//单双
	@Column(name="single_double",temp=true)
	private String singleDouble;
	
	//龙虎和
	@Column(name="com_dto",temp=true)
	private String comDto;
	
	//总和 
	@Column(name="gy_sum",temp=true)
	private Integer GySum;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLotCode() {
		return this.lotCode;
	}

	public void setLotCode(String lotCode) {
		this.lotCode = lotCode;
	}

	public String getQiHao() {
		return this.qiHao;
	}

	public void setQiHao(String qiHao) {
		this.qiHao = qiHao;
	}

	public String getHaoMa() {
		return this.haoMa;
	}

	public void setHaoMa(String haoMa) {
		this.haoMa = haoMa;
	}

	public String getOmmit() {
		return this.ommit;
	}

	public void setOmmit(String ommit) {
		this.ommit = ommit;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getOpenStatus() {
		return this.openStatus;
	}

	public void setOpenStatus(Integer openStatus) {
		this.openStatus = openStatus;
	}

	public Date getOpenTime() {
		return this.openTime;
	}

	public void setOpenTime(Date openTime) {
		this.openTime = openTime;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getActionNo() {
		return actionNo;
	}

	public void setActionNo(Integer actionNo) {
		this.actionNo = actionNo;
	}

	public String getLimitBetAccount() {
		return limitBetAccount;
	}

	public void setLimitBetAccount(String limitBetAccount) {
		this.limitBetAccount = limitBetAccount;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getSingleDouble() {
		return singleDouble;
	}

	public void setSingleDouble(String singleDouble) {
		this.singleDouble = singleDouble;
	}

	public String getComDto() {
		return comDto;
	}

	public void setComDto(String comDto) {
		this.comDto = comDto;
	}

	public Integer getGySum() {
		return GySum;
	}

	public void setGySum(Integer gySum) {
		GySum = gySum;
	}
	
	
}
