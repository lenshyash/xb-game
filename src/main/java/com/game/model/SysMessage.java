package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name = "sys_message")
public class SysMessage {
	
	public static long TYPE_DEFAULT = 1;//会员个人
	public static long TYPE_ALL = 2;//会员群发
	
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 信息内容
	 */
	@Column(name = "message")
	private String message;

	/**
	 * 创建时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 信息标题
	 */
	@Column(name = "title")
	private String title;
	
	/**
	 * 站内信类型  2是群发   1是个人
	 */
	@Column(name = "type")
	private Long type;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}
	
	
}
