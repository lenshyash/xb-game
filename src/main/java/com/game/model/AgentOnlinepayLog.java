package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name="agent_onlinepay_log")
public class AgentOnlinepayLog {
	/**
		在线充值通知日志id
	*/
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		站点id
	*/
	@Column(name="station_id")
	private Long stationId;
	
	/**
		创建时间
	*/
	@Column(name="create_datetime")
	private Date createDatetime;
	
	/**
		支付类型
	*/
	@Column(name="pay_type",length=20)
	private String payType;
	
	/**
		通知服务器ip
	*/
	@Column(name="notify_ip",length=20)
	private String notifyIp;
	
	/**
		日志内容
	*/
	@Column(name="content",length=2147483647)
	private String content;
	
	/**
		备注
	*/
	@Column(name="remark",length=2147483647)
	private String remark;
	
	/**
		订单号
	*/
	@Column(name="order_no",length=20)
	private String orderNo;
	
	/**
		通知的URL，主要是兼容get方式通知的形式
	*/
	@Column(name="notify_url",length=1000)
	private String notifyUrl;
	
	/**
		http通知类型，GET、POST等
	*/
	@Column(name="method",length=20)
	private String method;
	
	/**
		请求参数－json格式
	*/
	@Column(name="params",length=1000)
	private String params;
	
	/**
		请求头－json格式
	*/
	@Column(name="headers",length=500)
	private String headers;
	


	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public Date getCreateDatetime(){
		return this.createDatetime;
	}
	
	public void setCreateDatetime(Date createDatetime){
		this.createDatetime = createDatetime;
	}

	public String getPayType(){
		return this.payType;
	}
	
	public void setPayType(String payType){
		this.payType = payType;
	}

	public String getNotifyIp(){
		return this.notifyIp;
	}
	
	public void setNotifyIp(String notifyIp){
		this.notifyIp = notifyIp;
	}

	public String getContent(){
		return this.content;
	}
	
	public void setContent(String content){
		this.content = content;
	}

	public String getRemark(){
		return this.remark;
	}
	
	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getOrderNo(){
		return this.orderNo;
	}
	
	public void setOrderNo(String orderNo){
		this.orderNo = orderNo;
	}

	public String getNotifyUrl(){
		return this.notifyUrl;
	}
	
	public void setNotifyUrl(String notifyUrl){
		this.notifyUrl = notifyUrl;
	}

	public String getMethod(){
		return this.method;
	}
	
	public void setMethod(String method){
		this.method = method;
	}

	public String getParams(){
		return this.params;
	}
	
	public void setParams(String params){
		this.params = params;
	}

	public String getHeaders(){
		return this.headers;
	}
	
	public void setHeaders(String headers){
		this.headers = headers;
	}
}
