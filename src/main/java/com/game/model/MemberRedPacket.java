package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_red_packet")
public class MemberRedPacket {

	public static int STATUS_DISABLED = 1;
	public static int STATUS_ENDALBED = 2;

	public static int TODAY_DEPOSIT_NORMAL = 1;// 不限制
	public static int TODAY_DEPOSIT_YES = 2;// 需要当天有充值

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "total_number")
	private Integer totalNumber;

	@Column(name = "total_money")
	private BigDecimal totalMoney;

	@Column(name = "min_money")
	private BigDecimal minMoney;

	@Column(name = "remain_money")
	private BigDecimal remainMoney;

	@Column(name = "remain_number")
	private Integer remainNumber;

	@Column(name = "status")
	private Integer status;

	@Column(name = "begin_datetime")
	private Date beginDatetime;

	@Column(name = "end_datetime")
	private Date endDatetime;

	@Column(name = "title")
	private String title;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "ip_number")
	private Integer ipNumber;

	@Column(name = "bet_rate")
	private BigDecimal betRate;// 打码量倍数

	@Column(name = "today_deposit")
	private BigDecimal todayDeposit;

	private String levelNames;
	
	//可以多次抢红包
	@Column(name = "deposit_grab_number1")
	private String depositGrabNumber1;
	
	@Column(name = "deposit_grab_number2")
	private String depositGrabNumber2;
	
	@Column(name = "deposit_grab_number3")
	private String depositGrabNumber3;
	
	@Column(name = "deposit_grab_number4")
	private String depositGrabNumber4;
	
	@Column(name = "deposit_grab_number5")
	private String depositGrabNumber5;
	
	private String manyRedPacketSet;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(Integer totalNumber) {
		this.totalNumber = totalNumber;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public BigDecimal getMinMoney() {
		return minMoney;
	}

	public void setMinMoney(BigDecimal minMoney) {
		this.minMoney = minMoney;
	}

	public BigDecimal getRemainMoney() {
		return remainMoney;
	}

	public void setRemainMoney(BigDecimal remainMoney) {
		this.remainMoney = remainMoney;
	}

	public Integer getRemainNumber() {
		return remainNumber;
	}

	public void setRemainNumber(Integer remainNumber) {
		this.remainNumber = remainNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getBeginDatetime() {
		return beginDatetime;
	}

	public void setBeginDatetime(Date beginDatetime) {
		this.beginDatetime = beginDatetime;
	}

	public Date getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Integer getIpNumber() {
		return ipNumber;
	}

	public void setIpNumber(Integer ipNumber) {
		this.ipNumber = ipNumber;
	}

	public BigDecimal getBetRate() {
		return betRate;
	}

	public void setBetRate(BigDecimal betRate) {
		this.betRate = betRate;
	}

	public String getLevelNames() {
		return levelNames;
	}

	public void setLevelNames(String levelNames) {
		this.levelNames = levelNames;
	}

	public BigDecimal getTodayDeposit() {
		return todayDeposit;
	}

	public void setTodayDeposit(BigDecimal todayDeposit) {
		this.todayDeposit = todayDeposit;
	}

	public String getDepositGrabNumber1() {
		return depositGrabNumber1;
	}

	public void setDepositGrabNumber1(String depositGrabNumber1) {
		this.depositGrabNumber1 = depositGrabNumber1;
	}

	public String getDepositGrabNumber2() {
		return depositGrabNumber2;
	}

	public void setDepositGrabNumber2(String depositGrabNumber2) {
		this.depositGrabNumber2 = depositGrabNumber2;
	}

	public String getDepositGrabNumber3() {
		return depositGrabNumber3;
	}

	public void setDepositGrabNumber3(String depositGrabNumber3) {
		this.depositGrabNumber3 = depositGrabNumber3;
	}

	public String getDepositGrabNumber4() {
		return depositGrabNumber4;
	}

	public void setDepositGrabNumber4(String depositGrabNumber4) {
		this.depositGrabNumber4 = depositGrabNumber4;
	}

	public String getDepositGrabNumber5() {
		return depositGrabNumber5;
	}

	public void setDepositGrabNumber5(String depositGrabNumber5) {
		this.depositGrabNumber5 = depositGrabNumber5;
	}

	public String getManyRedPacketSet() {
		return manyRedPacketSet;
	}

	public void setManyRedPacketSet(String manyRedPacketSet) {
		this.manyRedPacketSet = manyRedPacketSet;
	}

	
	

}
