package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="app_update")
public class AppUpdate {

    /**
     * 菜单禁用
     */
    public static final long STATUS_DISABLED = 1L;
    /**
     * 菜单启用
     */
    public static final long STATUS_ENABLED = 2L;
    /**
     * 苹果
     */
    public static final String IOS = "ios";
    /**
     * 安卓
     */
    public static final String ANDROID = "android";
    /**
     * 103 完美板
     */
    public static final String PERFECT = "103";
    /**
     主键ID
     */
    @Column(name="id",primarykey=true)
    private Long id;

    /**
     版本号
     */
    @Column(name="version",length=50)
    private String version;

    /**
     更新内容
     */
    @Column(name="content",length=500)
    private String content;

    @Column(name="station_ids",length = 5000)
    private String stationIds;

    /**
     * 启用状态
     */
    @Column(name="status")
    private Long status;
    /**
     * app标识 苹果：ios 安卓：android
     */
    @Column(name="flag")
    private String flag;

    @Column(name="create_time")
    private Date createTime;
    /**
     * app下载链接
     */
    @Column(name="url")
    private String url;
    /**
     *是否强制更新 1是0否
     */
    @Column(name="is_update")
    private Integer isUpdate;
    
    /**
     *站点ID
     */
    @Column(name="station_id")
    private Long stationId;
    public Long getId() {
        return id;
    }
    /**
     *站点域名
     */
    @Column(name="domain")
    private String domain;
    
    /**
     *站点域名
     */
    @Column(name="type")
    private String type;
    
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getStationIds() {
        return stationIds;
    }

    public void setStationIds(String stationIds) {
        this.stationIds = stationIds;
    }

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(Integer isUpdate) {
		this.isUpdate = isUpdate;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
