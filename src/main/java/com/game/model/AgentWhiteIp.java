package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name="agent_white_ip")
public class AgentWhiteIp {
	
	public static final long STATUS_ENABLED = 2L;
	
	public static final long STATUS_DISABLED = 1L;
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	@Column(name="status")
	private Long status;
	
	@Column(name="station_id")
	private Long stationId;
	
	@InsertValue(generator=InsertValue.GENERATOR_NEW_DATE)
	@Column(name="create_datetime")
	private Date createDatetime;
	
	@Column(name="ip")
	private String ip;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}
