package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

import java.util.Date;

@Table(name="sec_card")
public class SecCard  {
	
	//租户平台
	public static final long STATION_TYPE_AGENT = 1L;
	//总控
	public static final long STATION_TYPE_CONTROL = 2L;
	
	@Column(name="id",primarykey=true)
	private Long id;
	
	/**
		卡号
	*/
	@Column(name="code",length=20)
	private String code;
	
	/**
		站点id
	*/
	@Column(name="station_id")
	private Long stationId;
	
	/**
		被使用次数
	*/
	@Column(name="use_count")
	private Long useCount;
	
	/**
		创建时间
	*/
	@Column(name="create_datetime")
	private Date createDatetime;
	
	@Column(name="card_password",length=1000)
	private String cardPassword;


	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){
		this.id = id;
	}

	public String getCode(){
		return this.code;
	}
	
	public void setCode(String code){
		this.code = code;
	}

	public Long getStationId(){
		return this.stationId;
	}
	
	public void setStationId(Long stationId){
		this.stationId = stationId;
	}

	public Long getUseCount(){
		return this.useCount;
	}
	
	public void setUseCount(Long useCount){
		this.useCount = useCount;
	}

	public Date getCreateDatetime(){
		return this.createDatetime;
	}
	
	public void setCreateDatetime(Date createDatetime){
		this.createDatetime = createDatetime;
	}

	public String getCardPassword(){
		return this.cardPassword;
	}
	
	public void setCardPassword(String cardPassword){
		this.cardPassword = cardPassword;
	}
}
