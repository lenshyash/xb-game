package com.game.model;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_deposit_fast")
public class AgentDepositFast {

	/**
	 * 禁用
	 */
	public static long STATUS_DISABLED = 1L;

	/**
	 * 启用
	 */
	public static long STATUS_ENABLE = 2L;
	
	/**
	 * 随机单次出现禁用
	 */
	public static long RANNDOM_DISABLED = 1L;
	
	/**
	 * 随机单次出现启用
	 */
	public static long RANNDOM_ENABLE = 2L;

	@Column(name = "id", primarykey = true)
	private Long id;

	@Column(name = "pay_account")
	private String payAccount;

	@Column(name = "pay_user_name")
	private String payUserName;

	@Column(name = "qr_code_img")
	private String qrCodeImg;

	@Column(name = "min")
	private BigDecimal min;
	
	@Column(name = "sort_no")
	private Integer sortNo;

	@Column(name = "max")
	private BigDecimal max;

	@Column(name = "status")
	private Long status;

	@Column(name = "station_id")
	private Long stationId;

	@Column(name = "pay_platform_id")
	private Long payPlatformId;

	@Column(name = "icon")
	private String icon;

	@Column(name = "front_label")
	private String frontLabel;
	
	@Column(name = "qrcode_desc")
	private String qrcodeDesc;
	
	@Column(name = "pay_starttime")
	private String payStarttime;
	
	@Column(name = "pay_endtime")
	private String payEndtime;
	
	@Column(name = "random_status")
	private Long randomStatus;

	@Column(name = "wallet_type")
	private String walletType;


	@Column(name = "rate")
	private BigDecimal rate;

	@Column(name = "platform_address")
	private String platformAddress;

	private String iconCss;



	public String getPayStarttime() {
		if(StringUtils.isEmpty(payStarttime)){
			return "00:00:00";
		}
		return payStarttime;
	}

	public void setPayStarttime(String payStarttime) {
		this.payStarttime = payStarttime;
	}

	public String getPayEndtime() {
		if(StringUtils.isEmpty(payEndtime)) {
			return "23:59:59";
		}
		return payEndtime;
	}

	public void setPayEndtime(String payEndtime) {
		this.payEndtime = payEndtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPayAccount() {
		return payAccount;
	}

	public void setPayAccount(String payAccount) {
		this.payAccount = payAccount;
	}

	public String getPayUserName() {
		return payUserName;
	}

	public void setPayUserName(String payUserName) {
		this.payUserName = payUserName;
	}

	public String getQrCodeImg() {
		return qrCodeImg;
	}

	public void setQrCodeImg(String qrCodeImg) {
		this.qrCodeImg = qrCodeImg;
	}

	public BigDecimal getMin() {
		return min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	public BigDecimal getMax() {
		return max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getPayPlatformId() {
		return payPlatformId;
	}

	public void setPayPlatformId(Long payPlatformId) {
		this.payPlatformId = payPlatformId;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getFrontLabel() {
		return frontLabel;
	}

	public void setFrontLabel(String frontLabel) {
		this.frontLabel = frontLabel;
	}

	public Integer getSortNo() {
		return sortNo;
	}

	public void setSortNo(Integer sortNo) {
		this.sortNo = sortNo;
	}

	public String getQrcodeDesc() {
		return qrcodeDesc;
	}

	public void setQrcodeDesc(String qrcodeDesc) {
		this.qrcodeDesc = qrcodeDesc;
	}

	public Long getRandomStatus() {
		return randomStatus;
	}

	public void setRandomStatus(Long randomStatus) {
		this.randomStatus = randomStatus;
	}

	public String getWalletType() {
		return walletType;
	}

	public void setWalletType(String walletType) {
		this.walletType = walletType;
	}


	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getPlatformAddress() {
		return platformAddress;
	}

	public void setPlatformAddress(String platformAddress) {
		this.platformAddress = platformAddress;
	}

	public String getIconCss() {
		return iconCss;
	}

	public void setIconCss(String iconCss) {
		this.iconCss = iconCss;
	}
}
