package com.game.model;

import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_station_folder_url")
public class SysStationFolderUrl {
	/**
	 * 禁用 、关闭
	 */
	public static final long STATUS_DISABLE = 1L;
	/**
	 * 启用
	 */
	public static final long STATUS_ENABLE = 2L;
	
//	public static String MULTI_FOLDER_KEY ="_multi_folder_";

	@Column(name = "id", primarykey = true)
	private Long id;
	/**
	 * 模板名
	 */
	@Column(name = "folder", length = 50)
	private String folder;
	/**
	 * 启用状态
	 */
	@Column(name = "status")
	private Long status;
	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * IOS版本app下载地址
	 */
	@Column(name = "ios_dl_url", length = 200)
	private String iosDlUrl;

	/**
	 * Android版本app下载地址
	 */
	@Column(name = "android_dl_url", length = 200)
	private String androidDlUrl;

	/**
	 * IOS版本app二维码地址
	 */
	@Column(name = "ios_qr_url", length = 200)
	private String iosQrUrl;

	/**
	 * Android版本app二维码地址
	 */
	@Column(name = "android_qr_url", length = 200)
	private String androidQrUrl;
	
	/**
	 * 客服Url
	 */
	@Column(name = "kf_url", length = 200)
	private String kfUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public String getIosDlUrl() {
		return iosDlUrl;
	}

	public void setIosDlUrl(String iosDlUrl) {
		this.iosDlUrl = iosDlUrl;
	}

	public String getAndroidDlUrl() {
		return androidDlUrl;
	}

	public void setAndroidDlUrl(String androidDlUrl) {
		this.androidDlUrl = androidDlUrl;
	}

	public String getIosQrUrl() {
		return iosQrUrl;
	}

	public void setIosQrUrl(String iosQrUrl) {
		this.iosQrUrl = iosQrUrl;
	}

	public String getAndroidQrUrl() {
		return androidQrUrl;
	}

	public void setAndroidQrUrl(String androidQrUrl) {
		this.androidQrUrl = androidQrUrl;
	}

	public String getKfUrl() {
		return kfUrl;
	}

	public void setKfUrl(String kfUrl) {
		this.kfUrl = kfUrl;
	}
	
}