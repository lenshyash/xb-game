package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Id;

@Data
@Table(name = "sys_account_daily_money")
public class DailyMoney {
	@Id
	@Column(name = "id",temp = true)
	private Long id;

	/**
	 * 统计日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "stat_date")
	private Date statDate;

	/**
	 * 系统用户id
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 系统用户名
	 */
	@Column(name = "account", length = 50)
	private String account;

	/**
	 * 系统用户类型
	 */
	@Column(name = "account_type")
	private Long accountType;
	
	/**
	 * 报表类型
	 */
	@Column(name = "report_type")
	@InsertValue(value = "1")
	private Long reportType;

	/**
	 * 系统用户层级关系
	 */
	@Column(name = "parents")
	private String parents;

	/**
	 * 站点id
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 上级id
	 */
	@Column(name = "agent_id")
	private Long agentId;
	@Column(name = "agent_name")
	private String agentName;
	/**
	 * 取款金额
	 */
	@Column(name = "withdraw_amount")
	@InsertValue(value = "0")
	private BigDecimal withdrawAmount;

	/**
	 * 取款次数
	 */
	@Column(name = "withdraw_times")
	@InsertValue(value = "0")
	private Long withdrawTimes;

	/**
	 * 手动扣款金额
	 */
	@Column(name = "withdraw_artificial")
	@InsertValue(value = "0")
	private BigDecimal withdrawArtificial;

	/**
	 * 存款金额
	 */
	@Column(name = "deposit_amount")
	@InsertValue(value = "0")
	private BigDecimal depositAmount;

	/**
	 * 存款次数
	 */
	@Column(name = "deposit_times")
	@InsertValue(value = "0")
	private Long depositTimes;
	/**
	 * 存款金额
	 */
	@Column(name = "sys_api_deposit_amount")
	@InsertValue(value = "0")
	private BigDecimal sysApiDepositAmount;

	/**
	 * 存款次数
	 */
	@Column(name = "sys_api_deposit_times")
	@InsertValue(value = "0")
	private Long sysApiDepositTimes;
	/**
	 * 手动加款金额
	 */
	@Column(name = "deposit_artificial")
	@InsertValue(value = "0")
	private BigDecimal depositArtificial;

	/**
	 * 存款赠送金额
	 */
	@Column(name = "deposit_gift_amount")
	@InsertValue(value = "0")
	private BigDecimal depositGiftAmount;

	/**
	 * 注册赠送金额
	 */
	@Column(name = "register_gift_amount")
	@InsertValue(value = "0")
	private BigDecimal registerGiftAmount;
	
	/**
	 * 升级赠送金额
	 */
	@Column(name = "level_up_gift_amount")
	@InsertValue(value = "0")
	private BigDecimal levelUpGiftAmount;

	/**
	 * 存款赠送次数
	 */
	@Column(name = "deposit_gift_times")
	@InsertValue(value = "0")
	private Long depositGiftTimes;

	/**
	 * 彩票投注
	 */
	@Column(name = "lottery_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal lotteryBetAmount;

	/**
	 * 彩票中奖
	 */
	@Column(name = "lottery_win_amount")
	@InsertValue(value = "0")
	private BigDecimal lotteryWinAmount;

	/**
	 * 彩票反水
	 */
	@Column(name = "lottery_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal lotteryRebateAmount;

	/**
	 * 六合彩投注
	 */
	@Column(name = "mark_six_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal markSixBetAmount;

	/**
	 * 六合彩中奖
	 */
	@Column(name = "mark_six_win_amount")
	@InsertValue(value = "0")
	private BigDecimal markSixWinAmount;

	/**
	 * 六合彩反水
	 */
	@Column(name = "mark_six_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal markSixRebateAmount;

	/**
	 * 真人投注
	 */
	@Column(name = "real_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal realBetAmount;

	/**
	 * 真热中奖
	 */
	@Column(name = "real_win_amount")
	@InsertValue(value = "0")
	private BigDecimal realWinAmount;

	/**
	 * 真人反水
	 */
	@Column(name = "real_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal realRebateAmount;

	/**
	 * 电子投注
	 */
	@Column(name = "egame_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal egameBetAmount;

	/**
	 * 电子中奖
	 */
	@Column(name = "egame_win_amount")
	@InsertValue(value = "0")
	private BigDecimal egameWinAmount;

	/**
	 * 捕鱼王反水
	 */
	@Column(name = "hunter_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal hunterRebateAmount;

	/**
	 * 捕鱼王投注
	 */
	@Column(name = "hunter_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal hunterBetAmount;

	/**
	 * 捕鱼王中奖
	 */
	@Column(name = "hunter_win_amount")
	@InsertValue(value = "0")
	private BigDecimal hunterWinAmount;

	/**
	 * 电子反水
	 */
	@Column(name = "egame_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal egameRebateAmount;

	/**
	 * 体育投注
	 */
	@Column(name = "sports_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal sportsBetAmount;

	/**
	 * 体育中奖
	 */
	@Column(name = "sports_win_amount")
	@InsertValue(value = "0")
	private BigDecimal sportsWinAmount;

	/**
	 * 体育反水
	 */
	@Column(name = "sports_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal sportsRebateAmount;

	/**
	 * 活动中奖
	 */
	@Column(name = "active_award_amount")
	@InsertValue(value = "0")
	private BigDecimal activeAwardAmount;

	/**
	 * 活动中奖次数
	 */
	@Column(name = "active_award_times")
	@InsertValue(value = "0")
	private Long activeAwardTimes;

	/**
	 * 系统用户账号上的余额
	 */
	@Column(name = "balance")
	@InsertValue(value = "0")
	private BigDecimal balance;

	/**
	 * 代理反水
	 */
	@Column(name = "proxy_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal proxyRebateAmount;

	/**
	 * 彩票投注次数
	 */
	@Column(name = "lottery_bet_times")
	@InsertValue(value = "0")
	private Long lotteryBetTimes;

	/**
	 * 彩票中奖次数
	 */
	@Column(name = "lottery_win_times")
	@InsertValue(value = "0")
	private Long lotteryWinTimes;

	/**
	 * 六合彩投注次数
	 */
	@Column(name = "mark_six_bet_times")
	@InsertValue(value = "0")
	private Long markSixBetTimes;

	/**
	 * 六合彩中奖次数
	 */
	@Column(name = "mark_six_win_times")
	@InsertValue(value = "0")
	private Long markSixWinTimes;

	/**
	 * 体育投注次数
	 */
	@Column(name = "sports_bet_times")
	@InsertValue(value = "0")
	private Long sportsBetTimes;

	/**
	 * 体育中奖次数
	 */
	@Column(name = "sports_win_times")
	@InsertValue(value = "0")
	private Long sportsWinTimes;

	/**
	 * 电子投注次数
	 */
	@Column(name = "egame_bet_times")
	@InsertValue(value = "0")
	private Long egameBetTimes;

	/**
	 * 电子中奖次数
	 */
	@Column(name = "egame_win_times")
	@InsertValue(value = "0")
	private Long egameWinTimes;

	/**
	 * 真人投注次数
	 */
	@Column(name = "real_bet_times")
	@InsertValue(value = "0")
	private Long realBetTimes;

	/**
	 * 真人中奖次数
	 */
	@Column(name = "real_win_times")
	@InsertValue(value = "0")
	private Long realWinTimes;

	/**
	 * 捕鱼王投注次数
	 */
	@Column(name = "hunter_bet_times")
	@InsertValue(value = "0")
	private Long hunterBetTimes;

	/**
	 * 捕鱼王中奖次数
	 */
	@Column(name = "hunter_win_times")
	@InsertValue(value = "0")
	private Long hunterWinTimes;

	/**
	 * 手动处理存款金额
	 */
	@Column(name = "deposit_handler_artificial")
	@InsertValue(value = "0")
	private BigDecimal depositHandlerArtificial;

	/**
	 * 手动处理存款次数
	 */
	@Column(name = "deposit_handler_artificial_times")
	@InsertValue(value = "0")
	private Long depositHandlerArtificialTimes;
	/**
	 * 彩票投注
	 */
	@Column(name = "sys_lottery_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal sysLotteryBetAmount;

	/**
	 * 彩票中奖
	 */
	@Column(name = "sys_lottery_win_amount")
	@InsertValue(value = "0")
	private BigDecimal sysLotteryWinAmount;

	/**
	 * 彩票反水
	 */
	@Column(name = "sys_lottery_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal sysLotteryRebateAmount;
	/**
	 * 彩票投注次数
	 */
	@Column(name = "sys_lottery_bet_times")
	@InsertValue(value = "0")
	private Long sysLotteryBetTimes;

	/**
	 * 彩票中奖次数
	 */
	@Column(name = "sys_lottery_win_times")
	@InsertValue(value = "0")
	private Long sysLotteryWinTimes;

	/**
	 * 十分六合彩投注
	 */
	@Column(name = "sf_mark_six_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal sfMarkSixBetAmount;

	/**
	 * 十分六合彩中奖
	 */
	@Column(name = "sf_mark_six_win_amount")
	@InsertValue(value = "0")
	private BigDecimal sfMarkSixWinAmount;

	/**
	 * 十分六合彩反水
	 */
	@Column(name = "sf_mark_six_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal sfMarkSixRebateAmount;
	/**
	 * 十分六合彩投注次数
	 */
	@Column(name = "sf_mark_six_bet_times")
	@InsertValue(value = "0")
	private Long sfMarkSixBetTimes;

	/**
	 * 十分六合彩中奖次数
	 */
	@Column(name = "sf_mark_six_win_times")
	@InsertValue(value = "0")
	private Long sfMarkSixWinTimes;
	
	
	/**
	 * 三方体育投注
	 */
	@Column(name = "third_sports_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdSportsBetAmount;

	/**
	 * 三方体育中奖
	 */
	@Column(name = "third_sports_win_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdSportsWinAmount;
	
	/**
	 * 三方体育投注次数
	 */
	@Column(name = "third_sports_bet_times")
	@InsertValue(value = "0")
	private Long thirdSportsBetTimes;

	/**
	 * 三方体育中奖次数
	 */
	@Column(name = "third_sports_win_times")
	@InsertValue(value = "0")
	private Long thirdSportsWinTimes;
	
	/**
	 * 三方体育反水
	 */
	@Column(name = "third_sports_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdSportsRebateAmount;
	
	/**
	 * 棋牌投注
	 */
	@Column(name = "chess_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal chessBetAmount;

	/**
	 * 棋牌中奖
	 */
	@Column(name = "chess_win_amount")
	@InsertValue(value = "0")
	private BigDecimal chessWinAmount;
	
	/**
	 * 棋牌投注次数
	 */
	@Column(name = "chess_bet_times")
	@InsertValue(value = "0")
	private Long chessBetTimes;

	/**
	 * 棋牌中奖次数
	 */
	@Column(name = "chess_win_times")
	@InsertValue(value = "0")
	private Long chessWinTimes;
	
	/**
	 * 棋牌反水
	 */
	@Column(name = "chess_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal chessRebateAmount;
	
	/**
	 * 电竞投注
	 */
	@Column(name = "esports_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal esportsBetAmount;

	/**
	 * 电竞中奖
	 */
	@Column(name = "esports_win_amount")
	@InsertValue(value = "0")
	private BigDecimal esportsWinAmount;
	
	/**
	 * 电竞投注次数
	 */
	@Column(name = "esports_bet_times")
	@InsertValue(value = "0")
	private Long esportsBetTimes;

	/**
	 * 电竞中奖次数
	 */
	@Column(name = "esports_win_times")
	@InsertValue(value = "0")
	private Long esportsWinTimes;
	
	/**
	 * 电竞反水
	 */
	@Column(name = "esports_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal esportsRebateAmount;
	
	/**
	 * 三方彩票投注
	 */
	@Column(name = "third_lottery_bet_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdLotteryBetAmount;

	/**
	 * 三方彩票中奖
	 */
	@Column(name = "third_lottery_win_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdLotteryWinAmount;
	
	/**
	 * 三方彩票投注次数
	 */
	@Column(name = "third_lottery_bet_times")
	@InsertValue(value = "0")
	private Long thirdLotteryBetTimes;

	/**
	 * 三方彩票中奖次数
	 */
	@Column(name = "third_lottery_win_times")
	@InsertValue(value = "0")
	private Long thirdLotteryWinTimes;
	
	/**
	 * 三方彩票反水
	 */
	@Column(name = "third_lottery_rebate_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdLotteryRebateAmount;
	
	/**
	 * 首充
	 */
	@Column(name = "first_deposit")
	@InsertValue(value = "0")
	private Long firstDeposit;
	
	/**
	 * 三方充值额
	 */
	@Column(name = "third_deposit_amount")
	@InsertValue(value = "0")
	private BigDecimal thirdDepositAmount;
	
	/**
	 * 首充金额
	 */
	@Column(name = "first_deposit_amount")
	@InsertValue(value = "0")
	private BigDecimal firstDepositAmount;
	
	/**
	 * 余额宝返利
	 */
	@Column(name = "balance_gem_amount")
	@InsertValue(value = "0")
	private BigDecimal balanceGemAmount;
	
	/**
	 * 首提
	 */
	@Column(name = "first_withdraw")
	@InsertValue(value = "0")
	private Long firstWithdraw;
	
	/**
	 * 首提金额
	 */
	@Column(name = "first_withdraw_amount")
	@InsertValue(value = "0")
	private BigDecimal firstWithdrawAmount;
	
	/**
	 * 扣除彩金次数
	 */
	@Column(name = "withdraw_gift_times")
	@InsertValue(value = "0")
	private Long withdrawGiftTimes;
	
	/**
	 * 存款赠送金额
	 */
	@Column(name = "withdraw_gift_amount")
	@InsertValue(value = "0")
	private BigDecimal withdrawGiftAmount;
	
	/**
	 * 手动加款次数
	 */
	@Column(name = "deposit_handler_times")
	@InsertValue(value = "0")
	private Long depositHandlerTimes;
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStatDate() {
		return this.statDate;
	}

	public void setStatDate(Date statDate) {
		this.statDate = statDate;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAccountType() {
		return this.accountType;
	}

	public void setAccountType(Long accountType) {
		this.accountType = accountType;
	}

	public String getParents() {
		return this.parents;
	}

	public void setParents(String parents) {
		this.parents = parents;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public BigDecimal getWithdrawAmount() {
		return this.withdrawAmount;
	}

	public void setWithdrawAmount(BigDecimal withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public Long getWithdrawTimes() {
		return this.withdrawTimes;
	}

	public void setWithdrawTimes(Long withdrawTimes) {
		this.withdrawTimes = withdrawTimes;
	}

	public BigDecimal getWithdrawArtificial() {
		return this.withdrawArtificial;
	}

	public void setWithdrawArtificial(BigDecimal withdrawArtificial) {
		this.withdrawArtificial = withdrawArtificial;
	}

	public BigDecimal getDepositAmount() {
		return this.depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public Long getDepositTimes() {
		return this.depositTimes;
	}

	public void setDepositTimes(Long depositTimes) {
		this.depositTimes = depositTimes;
	}

	public BigDecimal getSysApiDepositAmount() {
		return sysApiDepositAmount;
	}

	public void setSysApiDepositAmount(BigDecimal sysApiDepositAmount) {
		this.sysApiDepositAmount = sysApiDepositAmount;
	}

	public Long getSysApiDepositTimes() {
		return sysApiDepositTimes;
	}

	public void setSysApiDepositTimes(Long sysApiDepositTimes) {
		this.sysApiDepositTimes = sysApiDepositTimes;
	}

	public BigDecimal getDepositArtificial() {
		return this.depositArtificial;
	}

	public void setDepositArtificial(BigDecimal depositArtificial) {
		this.depositArtificial = depositArtificial;
	}

	public BigDecimal getDepositGiftAmount() {
		return this.depositGiftAmount;
	}

	public void setDepositGiftAmount(BigDecimal depositGiftAmount) {
		this.depositGiftAmount = depositGiftAmount;
	}

	public Long getDepositGiftTimes() {
		return this.depositGiftTimes;
	}

	public void setDepositGiftTimes(Long depositGiftTimes) {
		this.depositGiftTimes = depositGiftTimes;
	}

	public BigDecimal getLotteryBetAmount() {
		return this.lotteryBetAmount;
	}

	public void setLotteryBetAmount(BigDecimal lotteryBetAmount) {
		this.lotteryBetAmount = lotteryBetAmount;
	}

	public BigDecimal getLotteryWinAmount() {
		return this.lotteryWinAmount;
	}

	public void setLotteryWinAmount(BigDecimal lotteryWinAmount) {
		this.lotteryWinAmount = lotteryWinAmount;
	}

	public BigDecimal getLotteryRebateAmount() {
		return this.lotteryRebateAmount;
	}

	public void setLotteryRebateAmount(BigDecimal lotteryRebateAmount) {
		this.lotteryRebateAmount = lotteryRebateAmount;
	}

	public BigDecimal getMarkSixBetAmount() {
		return this.markSixBetAmount;
	}

	public void setMarkSixBetAmount(BigDecimal markSixBetAmount) {
		this.markSixBetAmount = markSixBetAmount;
	}

	public BigDecimal getMarkSixWinAmount() {
		return this.markSixWinAmount;
	}

	public void setMarkSixWinAmount(BigDecimal markSixWinAmount) {
		this.markSixWinAmount = markSixWinAmount;
	}

	public BigDecimal getMarkSixRebateAmount() {
		return this.markSixRebateAmount;
	}

	public void setMarkSixRebateAmount(BigDecimal markSixRebateAmount) {
		this.markSixRebateAmount = markSixRebateAmount;
	}

	public BigDecimal getRealBetAmount() {
		return this.realBetAmount;
	}

	public void setRealBetAmount(BigDecimal realBetAmount) {
		this.realBetAmount = realBetAmount;
	}

	public BigDecimal getRealWinAmount() {
		return this.realWinAmount;
	}

	public void setRealWinAmount(BigDecimal realWinAmount) {
		this.realWinAmount = realWinAmount;
	}

	public BigDecimal getRealRebateAmount() {
		return this.realRebateAmount;
	}

	public void setRealRebateAmount(BigDecimal realRebateAmount) {
		this.realRebateAmount = realRebateAmount;
	}

	public BigDecimal getEgameBetAmount() {
		return this.egameBetAmount;
	}

	public void setEgameBetAmount(BigDecimal egameBetAmount) {
		this.egameBetAmount = egameBetAmount;
	}

	public BigDecimal getEgameWinAmount() {
		return this.egameWinAmount;
	}

	public void setEgameWinAmount(BigDecimal egameWinAmount) {
		this.egameWinAmount = egameWinAmount;
	}

	public BigDecimal getEgameRebateAmount() {
		return this.egameRebateAmount;
	}

	public void setEgameRebateAmount(BigDecimal egameRebateAmount) {
		this.egameRebateAmount = egameRebateAmount;
	}

	public BigDecimal getSportsBetAmount() {
		return this.sportsBetAmount;
	}

	public void setSportsBetAmount(BigDecimal sportsBetAmount) {
		this.sportsBetAmount = sportsBetAmount;
	}

	public BigDecimal getSportsWinAmount() {
		return this.sportsWinAmount;
	}

	public void setSportsWinAmount(BigDecimal sportsWinAmount) {
		this.sportsWinAmount = sportsWinAmount;
	}

	public BigDecimal getSportsRebateAmount() {
		return this.sportsRebateAmount;
	}

	public void setSportsRebateAmount(BigDecimal sportsRebateAmount) {
		this.sportsRebateAmount = sportsRebateAmount;
	}

	public BigDecimal getBalance() {
		return this.balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getProxyRebateAmount() {
		return this.proxyRebateAmount;
	}

	public void setProxyRebateAmount(BigDecimal proxyRebateAmount) {
		this.proxyRebateAmount = proxyRebateAmount;
	}

	public BigDecimal getHunterRebateAmount() {
		return hunterRebateAmount;
	}

	public void setHunterRebateAmount(BigDecimal hunterRebateAmount) {
		this.hunterRebateAmount = hunterRebateAmount;
	}

	public BigDecimal getHunterBetAmount() {
		return hunterBetAmount;
	}

	public void setHunterBetAmount(BigDecimal hunterBetAmount) {
		this.hunterBetAmount = hunterBetAmount;
	}

	public BigDecimal getHunterWinAmount() {
		return hunterWinAmount;
	}

	public void setHunterWinAmount(BigDecimal hunterWinAmount) {
		this.hunterWinAmount = hunterWinAmount;
	}

	public Long getLotteryBetTimes() {
		return lotteryBetTimes;
	}

	public void setLotteryBetTimes(Long lotteryBetTimes) {
		this.lotteryBetTimes = lotteryBetTimes;
	}

	public Long getLotteryWinTimes() {
		return lotteryWinTimes;
	}

	public void setLotteryWinTimes(Long lotteryWinTimes) {
		this.lotteryWinTimes = lotteryWinTimes;
	}

	public Long getMarkSixBetTimes() {
		return markSixBetTimes;
	}

	public void setMarkSixBetTimes(Long markSixBetTimes) {
		this.markSixBetTimes = markSixBetTimes;
	}

	public Long getMarkSixWinTimes() {
		return markSixWinTimes;
	}

	public void setMarkSixWinTimes(Long markSixWinTimes) {
		this.markSixWinTimes = markSixWinTimes;
	}

	public Long getSportsBetTimes() {
		return sportsBetTimes;
	}

	public void setSportsBetTimes(Long sportsBetTimes) {
		this.sportsBetTimes = sportsBetTimes;
	}

	public Long getSportsWinTimes() {
		return sportsWinTimes;
	}

	public void setSportsWinTimes(Long sportsWinTimes) {
		this.sportsWinTimes = sportsWinTimes;
	}

	public Long getEgameBetTimes() {
		return egameBetTimes;
	}

	public void setEgameBetTimes(Long egameBetTimes) {
		this.egameBetTimes = egameBetTimes;
	}

	public Long getEgameWinTimes() {
		return egameWinTimes;
	}

	public void setEgameWinTimes(Long egameWinTimes) {
		this.egameWinTimes = egameWinTimes;
	}

	public Long getRealBetTimes() {
		return realBetTimes;
	}

	public void setRealBetTimes(Long realBetTimes) {
		this.realBetTimes = realBetTimes;
	}

	public Long getRealWinTimes() {
		return realWinTimes;
	}

	public void setRealWinTimes(Long realWinTimes) {
		this.realWinTimes = realWinTimes;
	}

	public Long getHunterBetTimes() {
		return hunterBetTimes;
	}

	public void setHunterBetTimes(Long hunterBetTimes) {
		this.hunterBetTimes = hunterBetTimes;
	}

	public Long getHunterWinTimes() {
		return hunterWinTimes;
	}

	public void setHunterWinTimes(Long hunterWinTimes) {
		this.hunterWinTimes = hunterWinTimes;
	}

	public BigDecimal getDepositHandlerArtificial() {
		return depositHandlerArtificial;
	}

	public void setDepositHandlerArtificial(BigDecimal depositHandlerArtificial) {
		this.depositHandlerArtificial = depositHandlerArtificial;
	}

	public Long getDepositHandlerArtificialTimes() {
		return depositHandlerArtificialTimes;
	}

	public void setDepositHandlerArtificialTimes(Long depositHandlerArtificialTimes) {
		this.depositHandlerArtificialTimes = depositHandlerArtificialTimes;
	}

	public BigDecimal getRegisterGiftAmount() {
		return registerGiftAmount;
	}

	public void setRegisterGiftAmount(BigDecimal registerGiftAmount) {
		this.registerGiftAmount = registerGiftAmount;
	}

	public BigDecimal getSysLotteryBetAmount() {
		return sysLotteryBetAmount;
	}

	public void setSysLotteryBetAmount(BigDecimal sysLotteryBetAmount) {
		this.sysLotteryBetAmount = sysLotteryBetAmount;
	}

	public BigDecimal getSysLotteryWinAmount() {
		return sysLotteryWinAmount;
	}

	public void setSysLotteryWinAmount(BigDecimal sysLotteryWinAmount) {
		this.sysLotteryWinAmount = sysLotteryWinAmount;
	}

	public BigDecimal getSysLotteryRebateAmount() {
		return sysLotteryRebateAmount;
	}

	public void setSysLotteryRebateAmount(BigDecimal sysLotteryRebateAmount) {
		this.sysLotteryRebateAmount = sysLotteryRebateAmount;
	}

	public Long getSysLotteryBetTimes() {
		return sysLotteryBetTimes;
	}

	public void setSysLotteryBetTimes(Long sysLotteryBetTimes) {
		this.sysLotteryBetTimes = sysLotteryBetTimes;
	}

	public Long getSysLotteryWinTimes() {
		return sysLotteryWinTimes;
	}

	public void setSysLotteryWinTimes(Long sysLotteryWinTimes) {
		this.sysLotteryWinTimes = sysLotteryWinTimes;
	}

	public BigDecimal getActiveAwardAmount() {
		return activeAwardAmount;
	}

	public void setActiveAwardAmount(BigDecimal activeAwardAmount) {
		this.activeAwardAmount = activeAwardAmount;
	}

	public Long getActiveAwardTimes() {
		return activeAwardTimes;
	}

	public void setActiveAwardTimes(Long activeAwardTimes) {
		this.activeAwardTimes = activeAwardTimes;
	}

	public BigDecimal getSfMarkSixBetAmount() {
		return sfMarkSixBetAmount;
	}

	public void setSfMarkSixBetAmount(BigDecimal sfMarkSixBetAmount) {
		this.sfMarkSixBetAmount = sfMarkSixBetAmount;
	}

	public BigDecimal getSfMarkSixWinAmount() {
		return sfMarkSixWinAmount;
	}

	public void setSfMarkSixWinAmount(BigDecimal sfMarkSixWinAmount) {
		this.sfMarkSixWinAmount = sfMarkSixWinAmount;
	}

	public BigDecimal getSfMarkSixRebateAmount() {
		return sfMarkSixRebateAmount;
	}

	public void setSfMarkSixRebateAmount(BigDecimal sfMarkSixRebateAmount) {
		this.sfMarkSixRebateAmount = sfMarkSixRebateAmount;
	}

	public Long getSfMarkSixBetTimes() {
		return sfMarkSixBetTimes;
	}

	public void setSfMarkSixBetTimes(Long sfMarkSixBetTimes) {
		this.sfMarkSixBetTimes = sfMarkSixBetTimes;
	}

	public Long getSfMarkSixWinTimes() {
		return sfMarkSixWinTimes;
	}

	public void setSfMarkSixWinTimes(Long sfMarkSixWinTimes) {
		this.sfMarkSixWinTimes = sfMarkSixWinTimes;
	}

	public BigDecimal getThirdSportsBetAmount() {
		return thirdSportsBetAmount;
	}

	public void setThirdSportsBetAmount(BigDecimal thirdSportsBetAmount) {
		this.thirdSportsBetAmount = thirdSportsBetAmount;
	}

	public BigDecimal getThirdSportsWinAmount() {
		return thirdSportsWinAmount;
	}

	public void setThirdSportsWinAmount(BigDecimal thirdSportsWinAmount) {
		this.thirdSportsWinAmount = thirdSportsWinAmount;
	}

	public Long getThirdSportsBetTimes() {
		return thirdSportsBetTimes;
	}

	public void setThirdSportsBetTimes(Long thirdSportsBetTimes) {
		this.thirdSportsBetTimes = thirdSportsBetTimes;
	}

	public Long getThirdSportsWinTimes() {
		return thirdSportsWinTimes;
	}

	public void setThirdSportsWinTimes(Long thirdSportsWinTimes) {
		this.thirdSportsWinTimes = thirdSportsWinTimes;
	}

	public BigDecimal getLevelUpGiftAmount() {
		return levelUpGiftAmount;
	}

	public void setLevelUpGiftAmount(BigDecimal levelUpGiftAmount) {
		this.levelUpGiftAmount = levelUpGiftAmount;
	}

	public BigDecimal getChessBetAmount() {
		return chessBetAmount;
	}

	public void setChessBetAmount(BigDecimal chessBetAmount) {
		this.chessBetAmount = chessBetAmount;
	}

	public BigDecimal getChessWinAmount() {
		return chessWinAmount;
	}

	public void setChessWinAmount(BigDecimal chessWinAmount) {
		this.chessWinAmount = chessWinAmount;
	}

	public Long getChessBetTimes() {
		return chessBetTimes;
	}

	public void setChessBetTimes(Long chessBetTimes) {
		this.chessBetTimes = chessBetTimes;
	}

	public Long getChessWinTimes() {
		return chessWinTimes;
	}

	public void setChessWinTimes(Long chessWinTimes) {
		this.chessWinTimes = chessWinTimes;
	}

	public BigDecimal getThirdLotteryBetAmount() {
		return thirdLotteryBetAmount;
	}

	public void setThirdLotteryBetAmount(BigDecimal thirdLotteryBetAmount) {
		this.thirdLotteryBetAmount = thirdLotteryBetAmount;
	}

	public BigDecimal getThirdLotteryWinAmount() {
		return thirdLotteryWinAmount;
	}

	public void setThirdLotteryWinAmount(BigDecimal thirdLotteryWinAmount) {
		this.thirdLotteryWinAmount = thirdLotteryWinAmount;
	}

	public Long getThirdLotteryBetTimes() {
		return thirdLotteryBetTimes;
	}

	public void setThirdLotteryBetTimes(Long thirdLotteryBetTimes) {
		this.thirdLotteryBetTimes = thirdLotteryBetTimes;
	}

	public Long getThirdLotteryWinTimes() {
		return thirdLotteryWinTimes;
	}

	public void setThirdLotteryWinTimes(Long thirdLotteryWinTimes) {
		this.thirdLotteryWinTimes = thirdLotteryWinTimes;
	}

	public BigDecimal getThirdSportsRebateAmount() {
		return thirdSportsRebateAmount;
	}

	public void setThirdSportsRebateAmount(BigDecimal thirdSportsRebateAmount) {
		this.thirdSportsRebateAmount = thirdSportsRebateAmount;
	}

	public BigDecimal getChessRebateAmount() {
		return chessRebateAmount;
	}

	public void setChessRebateAmount(BigDecimal chessRebateAmount) {
		this.chessRebateAmount = chessRebateAmount;
	}

	public BigDecimal getThirdLotteryRebateAmount() {
		return thirdLotteryRebateAmount;
	}

	public void setThirdLotteryRebateAmount(BigDecimal thirdLotteryRebateAmount) {
		this.thirdLotteryRebateAmount = thirdLotteryRebateAmount;
	}

	public Long getFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(Long firstDeposit) {
		this.firstDeposit = firstDeposit;
	}

	public BigDecimal getThirdDepositAmount() {
		return thirdDepositAmount;
	}

	public void setThirdDepositAmount(BigDecimal thirdDepositAmount) {
		this.thirdDepositAmount = thirdDepositAmount;
	}

	public Long getReportType() {
		return reportType;
	}

	public void setReportType(Long reportType) {
		this.reportType = reportType;
	}

	public BigDecimal getFirstDepositAmount() {
		return firstDepositAmount;
	}

	public void setFirstDepositAmount(BigDecimal firstDepositAmount) {
		this.firstDepositAmount = firstDepositAmount;
	}

	public BigDecimal getBalanceGemAmount() {
		return balanceGemAmount;
	}

	public void setBalanceGemAmount(BigDecimal balanceGemAmount) {
		this.balanceGemAmount = balanceGemAmount;
	}

	public Long getFirstWithdraw() {
		return firstWithdraw;
	}

	public void setFirstWithdraw(Long firstWithdraw) {
		this.firstWithdraw = firstWithdraw;
	}

	public BigDecimal getFirstWithdrawAmount() {
		return firstWithdrawAmount;
	}

	public void setFirstWithdrawAmount(BigDecimal firstWithdrawAmount) {
		this.firstWithdrawAmount = firstWithdrawAmount;
	}

	public Long getWithdrawGiftTimes() {
		return withdrawGiftTimes;
	}

	public void setWithdrawGiftTimes(Long withdrawGiftTimes) {
		this.withdrawGiftTimes = withdrawGiftTimes;
	}

	public BigDecimal getWithdrawGiftAmount() {
		return withdrawGiftAmount;
	}

	public void setWithdrawGiftAmount(BigDecimal withdrawGiftAmount) {
		this.withdrawGiftAmount = withdrawGiftAmount;
	}

	public BigDecimal getEsportsWinAmount() {
		return esportsWinAmount;
	}

	public void setEsportsWinAmount(BigDecimal esportsWinAmount) {
		this.esportsWinAmount = esportsWinAmount;
	}

	public Long getEsportsBetTimes() {
		return esportsBetTimes;
	}

	public void setEsportsBetTimes(Long esportsBetTimes) {
		this.esportsBetTimes = esportsBetTimes;
	}

	public Long getEsportsWinTimes() {
		return esportsWinTimes;
	}

	public void setEsportsWinTimes(Long esportsWinTimes) {
		this.esportsWinTimes = esportsWinTimes;
	}

	public BigDecimal getEsportsRebateAmount() {
		return esportsRebateAmount;
	}

	public void setEsportsRebateAmount(BigDecimal esportsRebateAmount) {
		this.esportsRebateAmount = esportsRebateAmount;
	}

	public BigDecimal getEsportsBetAmount() {
		return esportsBetAmount;
	}

	public void setEsportsBetAmount(BigDecimal esportsBetAmount) {
		this.esportsBetAmount = esportsBetAmount;
	}

	public Long getDepositHandlerTimes() {
		return depositHandlerTimes;
	}

	public void setDepositHandlerTimes(Long depositHandlerTimes) {
		this.depositHandlerTimes = depositHandlerTimes;
	}
}
