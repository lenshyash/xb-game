package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "agent_comprehensive_count")
public class AgentComprehensiveCount {

	/**
	 * 主键ID
	 */
	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 站点
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 代理数
	 */
	@Column(name = "agent_count")
	private Long agentCount;

	/**
	 * 会员数
	 */
	@Column(name = "member_count")
	private Long memberCount;

	/**
	 * 今日注册人数
	 */
	@Column(name = "register_account")
	private Long registerAccount;

	/**
	 * 今日投注额
	 */
	@Column(name = "bet_amount")
	private BigDecimal betAmount;

	/**
	 * 今日中奖额
	 */
	@Column(name = "award_amount")
	private BigDecimal awardAmount;

	/**
	 * 今日返点
	 */
	@Column(name = "agent_rebate")
	private BigDecimal agentRebate;

	/**
	 * 今日反水
	 */
	@Column(name = "member_rebate")
	private BigDecimal memberRebate;

	/**
	 * 今日充值额
	 */
	@Column(name = "deposit_amount")
	private BigDecimal depositAmount;

	/**
	 * 今日提款额
	 */
	@Column(name = "withdraw_amount")
	private BigDecimal withdrawAmount;

	/**
	 * 在线人数
	 */
	@Column(name = "account_num_online")
	private Long accountNumOnline;

	/**
	 * 统计日期
	 */
	@Column(name = "count_date")
	private Date countDate;

	/**
	 * 彩票盈亏
	 */
	@Column(name = "lottery_profit")
	private BigDecimal lotteryProfit;
	/**
	 * 体育盈亏
	 */
	@Column(name = "sport_profit")
	private BigDecimal sportProfit;
	/**
	 * 真人盈亏
	 */
	@Column(name = "real_game_profit")
	private BigDecimal realGameProfit;
	/**
	 * 电子盈亏
	 */
	@Column(name = "electron_profit")
	private BigDecimal electronProfit;

	/**
	 * 六合彩盈亏
	 */
	@Column(name = "mark_six_profit")
	private BigDecimal markSixProfit;
	/**
	 * 充值赠送
	 */
	@Column(name = "deposit_gift_acmount")
	private BigDecimal depositGiftAcmount;

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAgentCount() {
		return agentCount;
	}

	public void setAgentCount(Long agentCount) {
		this.agentCount = agentCount;
	}

	public Long getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(Long memberCount) {
		this.memberCount = memberCount;
	}

	public Long getRegisterAccount() {
		return registerAccount;
	}

	public void setRegisterAccount(Long registerAccount) {
		this.registerAccount = registerAccount;
	}

	public BigDecimal getBetAmount() {
		return betAmount;
	}

	public void setBetAmount(BigDecimal betAmount) {
		this.betAmount = betAmount;
	}

	public BigDecimal getAwardAmount() {
		return awardAmount;
	}

	public void setAwardAmount(BigDecimal awardAmount) {
		this.awardAmount = awardAmount;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public BigDecimal getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(BigDecimal withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}

	public Long getAccountNumOnline() {
		return accountNumOnline;
	}

	public void setAccountNumOnline(Long accountNumOnline) {
		this.accountNumOnline = accountNumOnline;
	}

	public Date getCountDate() {
		return countDate;
	}

	public void setCountDate(Date countDate) {
		this.countDate = countDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAgentRebate() {
		return agentRebate;
	}

	public void setAgentRebate(BigDecimal agentRebate) {
		this.agentRebate = agentRebate;
	}

	public BigDecimal getMemberRebate() {
		return memberRebate;
	}

	public void setMemberRebate(BigDecimal memberRebate) {
		this.memberRebate = memberRebate;
	}

	public BigDecimal getLotteryProfit() {
		return lotteryProfit;
	}

	public void setLotteryProfit(BigDecimal lotteryProfit) {
		this.lotteryProfit = lotteryProfit;
	}

	public BigDecimal getSportProfit() {
		return sportProfit;
	}

	public void setSportProfit(BigDecimal sportProfit) {
		this.sportProfit = sportProfit;
	}

	public BigDecimal getRealGameProfit() {
		return realGameProfit;
	}

	public void setRealGameProfit(BigDecimal realGameProfit) {
		this.realGameProfit = realGameProfit;
	}

	public BigDecimal getElectronProfit() {
		return electronProfit;
	}

	public void setElectronProfit(BigDecimal electronProfit) {
		this.electronProfit = electronProfit;
	}

	public BigDecimal getMarkSixProfit() {
		return markSixProfit;
	}

	public void setMarkSixProfit(BigDecimal markSixProfit) {
		this.markSixProfit = markSixProfit;
	}

	public BigDecimal getDepositGiftAcmount() {
		return depositGiftAcmount;
	}

	public void setDepositGiftAcmount(BigDecimal depositGiftAcmount) {
		this.depositGiftAcmount = depositGiftAcmount;
	}
}
