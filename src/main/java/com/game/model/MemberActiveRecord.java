package com.game.model;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.InsertValue;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "member_active_record")
public class MemberActiveRecord {

	// 未处理
	public static long STATUS_UNTREATED = 1l;
	// 处理成功
	public static long STATUS_SUCCESS = 2l;
	// 处理失败
	public static long STATUS_FAILED = 3l;
	// 已取消
	public static long STATUS_CANCELED = 4l;

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 活动ID
	 */
	@Column(name = "active_id")
	private Long activeId;

	/**
	 * 站点ID
	 */
	@Column(name = "station_id")
	private Long stationId;

	/**
	 * 用户ID
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 用户账号
	 */
	@Column(name = "account", length = 20)
	private String account;

	/**
	 * 奖项类型
	 */
	@Column(name = "award_type")
	private Long awardType;

	/**
	 * 商品名称
	 */
	@Column(name = "product_name", length = 200)
	private String productName;

	/**
	 * 奖项面值
	 */
	@Column(name = "award_value")
	private BigDecimal awardValue;

	/**
	 * 中奖时间
	 */
	@Column(name = "create_datetime")
	private Date createDatetime;

	/**
	 * 商品ID
	 */
	@Column(name = "product_id")
	private Long productId;

	/**
	 * 处理状态
	 */
	@Column(name = "status")
	private Long status;
	
	/**
	 * 备注说明
	 */
	@Column(name = "remark")
	private String remark;

	/**
	 * 乐观锁版本号
	 */
	@InsertValue(value = "0")
	@Column(name = "data_version")
	private Long dataVersion;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActiveId() {
		return this.activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public Long getStationId() {
		return this.stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getAwardType() {
		return this.awardType;
	}

	public void setAwardType(Long awardType) {
		this.awardType = awardType;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getAwardValue() {
		return this.awardValue;
	}

	public void setAwardValue(BigDecimal awardValue) {
		this.awardValue = awardValue;
	}

	public Date getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Long getDataVersion() {
		return dataVersion;
	}

	public void setDataVersion(Long dataVersion) {
		this.dataVersion = dataVersion;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
