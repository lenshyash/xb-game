package com.game.model;

import org.jay.frame.jdbc.annotation.Column;
import org.jay.frame.jdbc.annotation.Table;

@Table(name = "sys_user_message")
public class SysUserMessage {

	public static long STATUS_UNREAD = 1;// 未读状态

	public static long STATUS_READ = 2;// 已读状态

	@Column(name = "id", primarykey = true)
	private Long id;

	/**
	 * 用户ID
	 */
	@Column(name = "account_id")
	private Long accountId;

	/**
	 * 用户账号
	 */
	@Column(name = "account")
	private String account;

	/**
	 * 站内信ID
	 */
	@Column(name = "message_id")
	private Long messageId;

	/**
	 * 状态（1、未读，2、已读）
	 */
	@Column(name = "status")
	private Long status;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getMessageId() {
		return this.messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getStatus() {
		return this.status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
}
