package com.game.secret.otp;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;



public class OTPTest {

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	public static void main(String[] args) throws  Exception {
		String iccid="515025515079930";
		String pwd= "123456";
		System.out.println(OneTimePasswordAlgorithm.getOneTimePassword(iccid, pwd, "b001",0));
		System.out.println(OneTimePasswordAlgorithm.getOneTimePassword(iccid, pwd, "b001",-1));
	}

}
