package com.game.constant;

/**
 * 
 *第三版本彩种
 */
public enum GameTypeVersion3 {

	/** 彩种类型，51=系统彩，52=时时彩，53=pk10，54=排列三，55=11选5，56=香港彩，7=PC蛋蛋 **/

		xtc("XTC", 51, "系统彩"),ssc("SSC", 52, "时时彩"),pk10("PK10", 53, "PK10"),pl3("PL3", 54, "排列三")
		,x5("11X5", 55, "11选5"),xgc("XGC", 6, "香港彩"),pcegg("PCEGG", 57, "PC蛋蛋"),k3("K3", 58, "快三");
		
	
	
	public String EN; // 彩种名称
	public int type; // 类型
	public String CN;// 汉字名字

	private GameTypeVersion3(String EN, int type, String CN) {
		this.EN = EN;
		this.type = type;
		this.CN = CN;
	}

	public static GameTypeVersion3 getGameEmun(String name) {
		try {
			return GameTypeVersion3.valueOf(GameTypeVersion3.class, name);
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println(xgc.CN);
//		System.out.println(GameType.values());
		for(GameTypeVersion3 aa : GameTypeVersion3.values()){
			System.out.println(aa.CN+"||"+aa.name()+"||"+aa.type);
		}
	}
}
