package com.game.constant;

public interface BusinessConstant {
	public static final String SWITCH_ON = "on";// 开关开启状态－on
	public static final String SWITCH_OFF = "off";// 开关关闭状态－off

	public static final Integer status_normal = 2;// 状态－开启
	public static final Integer status_unable = 1;// 状态－关闭

	/**
	 * 显示时分组，1=时时彩，2=低频彩，3=快开，4=快三，5=11选5，6=香港彩,8=第二版独立彩
	 */
	public static final Integer lottery_view_group_ssc = 1;
	public static final Integer lottery_view_group_dpc = 2;
	public static final Integer lottery_view_group_kk = 3;
	public static final Integer lottery_view_group_ks = 4;
	public static final Integer lottery_view_group_11x5 = 5;
	public static final Integer lottery_view_group_xgc = 6;
	public static final Integer lottery_view_group_independence = 8;
	/**
	 * 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋
	 * 8=北京赛车，幸运飞艇，9=重庆时时彩，天津时时彩，新疆时时彩，10=江苏骰宝，11=幸运28，12=湖南快乐十分,广东快乐十分，重庆幸运农场
	 * 13=第二版的六合彩 第三版彩种======>>>51=系统彩,52=时时彩，53=pk10，54=排列三，55=11选5，57=PC蛋蛋
	 */
	public static final Integer lottery_type_xtc = 1;
	public static final Integer lottery_type_ssc = 2;
	public static final Integer lottery_type_pk10 = 3;
	public static final Integer lottery_type_pl3 = 4;
	public static final Integer lottery_type_11x5 = 5;
	public static final Integer lottery_type_xgc = 6;
	public static final Integer lottery_type_pcdd = 7;
	/**
	 * 以下为第二版彩票type
	 */
	public static final Integer lottery_type_v2_bjsc = 8;
	public static final Integer lottery_type_v2_ssc = 9;
	public static final Integer lottery_type_v2_jssb3 = 10;
	public static final Integer lottery_type_v2_xy28 = 11;
	public static final Integer lottery_type_v2_klsf = 12;
	public static final Integer lottery_type_v2_xgc = 13;
	public static final Integer lottery_type_v2_11x5 = 14;
	public static final Integer lottery_type_v2_dpc = 15;
	/**
	 * 以下为第三版彩票type
	 */
	public static final Integer lottery_type_v3_xtc = 51;
	public static final Integer lottery_type_v3_ssc = 52;
	public static final Integer lottery_type_v3_pk10 = 53;
	public static final Integer lottery_type_v3_pl3 = 54;
	public static final Integer lottery_type_v3_11x5 = 55;
	public static final Integer lottery_type_v3_pcdd = 57;
	public static final Integer lottery_type_xtc_sflhc = 66;
	// 以下为第四版彩票type
	public static final Integer lottery_type_v4_pk10 = 158;
	public static final Integer lottery_type_v4_pcdd = 161;

	/**
	 * 彩票版本 1=第一版，2=第二版, 3=第三版,4=第四版,5=第五版
	 */
	public static final int lottery_identify_V1 = 1;
	public static final int lottery_identify_V2 = 2;
	public static final int lottery_identify_V3 = 3;
	public static final int lottery_identify_V4 = 4;
	public static final int lottery_identify_V5 = 5;

	// 彩票开奖数据状态
	public static final int LOTTERY_OPEN_STATUS_UNOPEN = 1;// 未开奖
	public static final int LOTTERY_OPEN_STATUS_UN_AWARD = 2;// 未派奖
	public static final int LOTTERY_OPEN_STATUS_AWARDED = 3;// 已派奖
	public static final int LOTTERY_OPEN_STATUS_AWARD_EXCEPTION = 4;// 未派奖完
	public static final int LOTTERY_OPEN_STATUS_CANCLE = 5;// 已经取消
	public static final int LOTTERY_OPEN_STATUS_ROLL = 6;// 已经回滚

	// 彩票订单状态
	public static final int LOTTERY_ORDER_STATUS_WAIT = 1;// 等待开奖
	public static final int LOTTERY_ORDER_STATUS_PRIZE = 2;// 已中奖
	public static final int LOTTERY_ORDER_STATUS_UNPRIZE = 3;// 未中奖
	public static final int LOTTERY_ORDER_STATUS_CANCEL = 4;// 撤单
	public static final int LOTTERY_ORDER_STATUS_ROLLBACK_SUCCESS = 5;// 派奖回滚成功
	public static final int LOTTERY_ORDER_STATUS_ROLLBACK_EXCEPTION = 6;// 回滚异常的
	public static final int LOTTERY_ORDER_STATUS_PRIZE_EXCEPTION = 7;// 开奖异常
	public static final int LOTTERY_ORDER_STATUS_TIE = 8;// 和
	public static final int LOTTERY_ORDER_STATUS_INVALID = 9; // 合买失效(仅支持合买订单有效)
	public static final int LOTTER_ORDER_STATUS_TIE_V4 = 10;	//和局，但有中奖
	// 彩票追号标识，不是追号＝1，中奖继续＝2，中奖中止＝追号第一期的order_id
	public static final String LOTTERY_ORDER_ZHUI_HAO_NO = "1";// 不是追号
	public static final String LOTTERY_ORDER_ZHUI_HAO_WIN_GO_ON = "2";// 中奖继续

	// 合买方案状态
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_1 = 1; // 可认购
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_2 = 2; // 满员
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_3 = 3; // 时间截止
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_4 = 4; // 完成（派奖成功后的状态）
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_5 = 5; // 方案失效
	public static final int LOTTERY_JOINT_PURCHASE_TYPE_6 = 6; // 撤单

	public static final int LOTTERY_JOINT_PURCHASE_PATE_STATUS_1 = 1;
	public static final int LOTTERY_JOINT_PURCHASE_PATE_STATUS_2 = 2;
	public static final int LOTTERY_JOINT_PURCHASE_PATE_STATUS_3 = 3;

	public static final int BET_TYPE_LOTTERY = 1;// 彩票
	public static final int BET_TYPE_REAL = 2;// 真人
	public static final int BET_TYPE_EGAME = 3;// 电子游艺
	public static final int BET_TYPE_SPORT = 4;// 体育
	public static final int BET_TYPE_MARK_SIX = 5;// 六合彩
	public static final int BET_TYPE_SPORT_THIRD = 99;// 三方体育
	public static final int BET_TYPE_CHESS = 10;// 棋牌
	public static final int BET_TYPE_LOTTERY_THIRD = 91;// 三方彩票
	public static final int BET_TYPE_ESPORT = 92;// 电竞

	/**
	 * 1,表示AG
	 */
	public static final int AG = 1;
	/**
	 * 表示BBin
	 */
	public static final int BBIN = 2;
	/**
	 * 表示MG',
	 */
	public static final int MG = 3;
	/**
	 * 表示MG 电子游艺',
	 */
	public static final int MG_EGAME = 4;
	/**
	 * 表示AG电子游戏',
	 */
	public static final int AEGAME = 5;
	/**
	 * 表示bbin电子游戏',
	 */
	public static final int BBINEGAME = 6;
	/**
	 * 表示Ag AG捕鱼王,
	 */
	public static final int AGHUNTER = 7;

	public static final String lottery_bet_token_key = "lottery_bet_token_key";
	public static final String mobile_lottery_bet_token_key = "mobile_lottery_bet_token_key";
	public static final String native_lottery_bet_token_key = "native_lottery_bet_token_key";

	/**
	 * 彩票开奖结果管理操作类型
	 */
	public static final int bc_lottery_cancel = 1;// 单期取消
	public static final int bc_lottery_redo = 3;// 重新结算
	public static final int bc_lottery_rollback = 4;// 派奖回滚
	public static final int bc_lottery_lock = 5;// 锁定
	public static final int bc_lottery_repair = 10;// 补派奖
	public static final int bc_lottery_delete = 11;// 删除期号
	/** 订单撤单 **/
	public static final int BC_LOTTERY_ORDER_ROLLCASH = 1;// 撤单并退钱
	public static final int BC_LOTTERY_ORDER_NOTROLLCASH = 2;// 撤单不退钱
	/**
	 * 手机跳转页面，跳转到pc 或者 mobile 的session key
	 */
	public static final String MOBILE_JUMP_SESSION_KEY = "mobileTo";

	/**
	 * 多级反水流程, 1,先计算除反水,实时,状态已经反水未到账 2,将已经反水未到账的到账 3,如果回滚的话,直接回滚到位
	 */
	public static final int STATUS_NOT_ROLL = 1;// 未反水
	public static final int STATUS_ALREADY_ROLL_BACK_NOT_MONEY = 2;// 已经反水,但还未到账
	public static final int STATUS_CANCLE_ROLL_BACK = 3;// 反水已经回滚
	public static final int STATUS_ALREADY_ROLL_BACK_AND_MONEY = 4;// 已经反水

	public static final String PREVENT_REGISTER_ATTACK = "PREVENT_REGISTER_ATTACK";// 防止恶意注册session--key
}
