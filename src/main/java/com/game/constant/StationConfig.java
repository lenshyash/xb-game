package com.game.constant;

import com.game.model.AgentBaseConfig;
import com.game.model.SysAccount;

public enum StationConfig {
	/**
	 * 帐变记录开关
	 */
	onoff_change_money("帐变记录开关"),
	/**
	 * 真人娱乐开关
	 */
	onoff_zhen_ren_yu_le("真人娱乐开关"),
	/**
	 * 电子游艺开关
	 */
	onoff_dian_zi_you_yi("电子游艺开关"),
    /**
	 * 第三方体育开关
	 */
    onoff_third_sports("第三方体育开关"),
    /**
     * 第三方彩票开关
     */
    onoff_third_lottery("第三方彩票开关"),
    /**
     * 棋牌开关
     */
    onoff_chess("棋牌开关"),
	/**
	 * 电子游艺开关
	 */
	onoff_dian_jing_yu_le("电竞开关"),
    /**
     * BB体育开关
     */
    onoff_bbty_sports("BB体育开关"),
	/**
	 * 独立彩票系统开关
	 */
	onoff_du_li_cai_piao("独立彩票系统开关"),

	basic_info_lottery_code("配置默认彩种"),

	lottery_page_logo_url("彩票玩法页面logo路径"),

	lottery_v2_1_bg_color("彩票V2(1)版本颜色配置"),

	lottery_page_version("彩票投注版本号，用于解决js，css升级后浏览器缓存的问题"),

	online_customer_service_url("在线客服的url地址"),

	onoff_lengre_yilou("彩票投注界面冷热遗漏开关。on－开，off－关"),

	onoff_chong_zhi_class("转账记录开关。on－开，off－关"),

	onoff_recharge_record("处理充值记录是否发送站内信。on－开，off－关"),
	
	onoff_withdraw_record("处理取款记录是否发送站内信。on－开，off－关"),
	
	onoff_online_pay_class("在线充值记录开关。on－开，off－关"),

	onoff_offers_apply("优惠申请开关。on－开，off－关"),

	onoff_letters("站内信息开关。on－开，off－关"),

	onoff_fan_shui("彩票中心返水开关。on－开，off－关"),

	onoff_agent_ip_change("租户后台ip变动开关。on－开，off－关"),

	onoff_mg_game("mg游戏开关。on－开，off－关"),
	
	onoff_mg_dz_game("mg游戏开关(电子小栏目)。on－开，off－关"),

	onoff_lottery_game("彩票游戏开关。on－开，off－关"),

	onoff_sports_game("皇冠体育开关。on－开，off－关"),
	
	onoff_wh_game("皇冠体育维护开关。on－开，off－关"),
	
	onoff_sports_basketBall_game("皇冠篮球开关。on－开，off－关"),
	
	onoff_sports_native_game("原生体育开关。on－开，off－关"),

	onoff_system("网站开关。on－开，off－关"),

	onoff_ag_game("ag游戏开关。on－开，off－关"),
	
	onoff_ag_dz_game("ag游戏开关(电子小栏目)on－开，off－关"),

	onoff_bbin_game("bbin游戏开关。on－开，off－关"),
	
	onoff_bbin_dz_game("bbin游戏开关(电子小栏目)。on－开，off－关"),

	onoff_pt_game("pt游戏开关。on－开，off－关"),

	onoff_ab_game("ab游戏开关。on－开，off－关"),

	onoff_qt_game("qt游戏开关。on－开，off－关"),
	
	onoff_og_game("og游戏开关。on－开，off－关"),
	
	onoff_cq9_game("cq9游戏开关。on－开，off－关"),
	
	onoff_jdb_game("jdb游戏开关。on－开，off－关"),
	
	onoff_isb_game("isb游戏开关。on－开，off－关"),
	
	onoff_ttg_game("ttg游戏开关。on－开，off－关"),
	onoff_mw_game("mw游戏开关。on－开，off－关"),
	
	onoff_bg_game("bg游戏开关。on－开，off－关"),
	
	onoff_bg_dz_game("bg游戏开关(电子小栏目)。on－开，off－关"),
	
	onoff_ds_game("ds游戏开关。on－开，off－关"),
	
	onoff_ebet_game("ebet游戏开关。on－开，off－关"),
	
    onoff_m8_game("m8体育开关。on－开，off－关"),
    onoff_m8h_game("m8h体育开关。on－开，off－关"),
    onoff_ibc_game("IBC体育开关。on－开，off－关"),
    
    onoff_vr_game("VR游戏开关。on－开，off－关"),
    onoff_ky_game("KY游戏开关。on－开，off－关"),
    onoff_th_game("TH游戏开关。on－开，off－关"),
    onoff_lh_game("LH游戏开关。on－开，off－关"),
    onoff_kx_game("凯旋电竞开关。on－开，off－关"),
    onoff_by_game("捕鱼游戏开关。on－开，off－关"),
    onoff_ibc_dj_game("沙巴电竞开关。on－开，off－关"),
    
    onoff_hg_sport_system("系统皇冠开关。on－开，off－关"),
    
    onoff_ky_auto_transfer("开元自动转入额度开关，on－开，off－关"),

	live_transfor_all_limit_onoff("真人共用余额开关"),
	live_transfor_all_platform("真人共用余额平台"),
	live_transfor_all_money("真人共用余额"),
	
	onoff_daili_login_member_platfrom("代理登录会员端开关"),

	onoff_mobile_ios_examine("手机页面ios审核是否关闭。on－是，off－否"),
	
	onoff_mobile_tool_util("手机底部工具栏开关。on－开，off－关"),
	
	onoff_mobile_lottery_sort("手机版彩票分类开关。on－开，off－关"),
	
	onoff_mobile_lunbo("手机轮播开关。on－开，off－关"),

	onoff_liu_he_cai("六合彩开关"),

	same_ip_register_num("同一IP当天注册会员数"),

	onoff_register("注册开关。on－开，off－关"),
	
	onoff_register_dafa_promocode("大发公共邀请码"),
	
	onoff_register_account_trun_agent("会员注册转代理开关"),
	
	member_genrt_link_desc("代理中心下级开户说明"),
	
	onoff_mobile_app_reg("手机单独app注册开关。on－开，off－关"),

	onoff_mobile_notlogged_to_loginview("手机未登录是否跳转到登录页面开关。on－开，off－关"),
	
	onoff_member_register("会员注册开关。on－开，off－关"),
	
	onoff_daili_register("代理注册开关。on－开，off－关"),
	
	onoff_promlink_wechat_anti_sealing("推广链接微信防封开关"),
	

	mobile_index("手机主页设置。v1－突出彩票，v2－突出真人,v3-突出体育"),

	onoff_mobile_recharge("手机页面中显示存款按钮。on－开，off－关"),

	onoff_mobile_drawing("手机页面中显示取款按钮。on－开，off－关"),

	mobile_payway_version("手机充值页面版本。v1－版本一（经典版本），v2－版本二（500vip版本）"),
	
	onoff_mobile_login_verify_code("手机开启验证码登录开关。on－开，off－关"),
	
	onoff_login_no_need_pwd("无痕登录开关(导入空密码时,直接使用登录者输入密码)。on－开，off－关"),
	
	onoff_login_sys_Auto_ban("系统自动封禁开关（会员密码错误次数过多自动禁用账号）。on－开，off－关"),
	
	onoff_bg_login_google_verify_code("租户后台谷歌验证码开关 on－开，off－关"),

	onoff_member_red_packet("抢红包功能开关。on－开，off－关"),
	onoff_red_packet_auto_award("红包自动派奖开关。on－开，off－关"),
	onoff_member_mobile_red_packet("手机抢红包开关。on－开，off－关"),
	member_red_packet_record_limit("红包记录设置。真，假。5,45"),
	member_active_record_limit("活动记录设置。真，假。20,0"),
	set_red_sort("红包奖励排序规则。rise-升序，drop-降序，disorderly-无序。drop"),
	set_active_sort("活动奖励排序规则。rise-升序，drop-降序，disorderly-无序。drop"),
	onoff_wheel_auto_award("大转盘自动派奖开关。on－开，off－关"),
	set_account_pc_level("电脑端会员等级开关。on-开，off-关"),
	set_account_mobie_level("手机端会员等级开关。on-开，off-关"),
	member_cardno_show("会员银行卡账号显示开关。on-开，off-关"),
	
	system_maintenance("网站维护原因"),
	proxy_daily_rebate("代理返点"),
	/**
	 * 提款、入款开始处理时间
	 */
	money_start_time("提款、入款开始处理时间"),
	/**
	 * 提款、入款结束处理时间
	 */
	money_end_time("提款、入款结束处理时间"),
	/**
	 * 提款最低金额
	 */
	withdraw_min_money("提款最低金额"),
	/**
	 * 提款最高金额
	 */
	withdraw_max_money("提款最高金额"),
	/**
	 * 每天提款次数限制
	 */
	withdraw_time_one_day("每天提款次数限制，0=不限制，其他为取款次数"),
	/**
	 * 提款说明
	 */
	withdraw_desc("提款说明"),
	/**
	 * 引导会员禁止提款开关
	 */
	onoff_embark_withdraw_guide("引导会员禁止提款开关on-开，off-关"),
	
	onoff_withdraw_report_type_guide("引导报表类型禁止提款开关on-开，off-关"),
	/**
	 * 提款次数是否只记录成功次数
	 */
	withdraw_failed_time_onoff("提款次数是否只记录成功次数,默认是"),

	live_transfor_ag_limit("真人AG转换限制"),

	live_transfor_bbin_limit("真人BBIN转换限制"),

	live_transfor_mg_limit("真人MG转换限制"),

	live_transfor_pt_limit("真人PT转换限制"),

	live_transfor_qt_limit("真人QT转换限制"),

	live_transfor_ab_limit("真人AB转换限制"),
	
	live_transfor_og_limit("真人OG转换限制"),
	
	live_transfor_ds_limit("真人DS转换限制"),
	live_transfor_cq9_limit("真人CQ9转换限制"),
	live_transfor_jdb_limit("真人JDB转换限制"),
	live_transfor_isb_limit("真人ISB转换限制"),
	live_transfor_ttg_limit("真人TTG转换限制"),
	live_transfor_mw_limit("真人MW转换限制"),
	live_transfor_bg_limit("真人BG转换限制"),
    live_transfor_m8_limit("M8体育转换限制"),
    live_transfor_ibc_limit("IBC体育转换限制"),
    live_transfor_m8h_limit("M8H体育转换限制"),
    live_transfor_vr_limit("VR游戏转换限制"),
    live_transfor_ky_limit("KY游戏转换限制"),
    live_transfor_th_limit("TH游戏转换限制"),
    live_transfor_lh_limit("LH游戏转换限制"),
    live_transfor_kx_limit("KX游戏转换限制"),
    live_transfor_ebet_limit("真人EBET转换限制"),
	/**
	 * 取款消费比例设置
	 */
	consume_rate("取款消费比例设置"),
	/**
	 * 低于该额度时，再次充值，当前打码量和提款所需打码量清零
	 */
	bet_num_draw_need_money("放款额度"),

	basic_info_website_copyright("最终解释权(copyright)"),

	basic_info_website_name("网站基础信息－网站名称"),

	basic_info_customer_phone("网站基础信息－客服电话"),

	basic_info_customer_email("网站基础信息－客服邮箱"),

	basic_info_customer_skype("网站基础信息－客服Skype"),
	
	basic_info_customer_chatroom("网站基础信息－聊天室token"),
	
	basic_info_new_im_chat_id("网站基础信息－新版聊天室ID"),

	basic_info_url_phone("网站手机端地址"),

	agent_web_head_keywords("网站页面头部keywords配置"),

	agent_web_head_description("网站页面头部description配置"),

	onoff_multi_agent("多级代理开关"),
	
	onoff_agent_merge_Rebate_odds_switch("多级代理返点赔率合并开关"),
	
	onoff_proxy_fixed_rebate("多级代理上级固定返点开关"),
	
	proxy_fixed_rebate_layers("多级代理上级固定返点层数"),
	
	proxy_fixed_rabate_per("直属上级固定返点百分比"),
	
	top_agent_rabate_per("上级代理固定返点百分比"),
	
	onoff_profit_share("代理占成开关"),
	
	onoff_agent_move_agent("代理批量移动到代理开关"),
	
	onoff_member_move_agent("会员批量移动到代理开关"),

	agent_rebate_max_num("代理设置－最大返点限制"),
	
	red_package_img("红包活动图片url"),
	turnlate_img("大转盘活动图片url"),
	sign_in_img("签到活动图片url"),
	
	agent_profit_share_max_num("代理设置－最大占成数"),
	
	member_dynamic_rate_max_num("会员设置－最大赔率值"),

	agent_rebate_step("代理设置－上下级返点差额"),

	onoff_agent_withdrawals("代理设置－代理后台给下级充值"),

	online_customer_start_time("在线客服上班时间"),

	online_customer_end_time("在线客服下班时间"),

	online_customer_showphone("在线客服手机登录页面开关"),

	first_register_to_give("第一次注册赠送"),

	onoff_show_pay_normal("一般存款是否显示，on=显示"),

	onoff_show_pay_quick("快速存款是否显示，on=显示"),

	onoff_show_pay_third("第三方存款是否显示，on=显示"),
	
	onoff_show_pay_virtual("虚拟货币存款是否显示，on=显示"),

	// [{"jiebao":"捷豹版"},{"v2":"第二版"},{"v2_1":"第二版(1)"},{"v3":"第三版"},{"v4":"第四版"}]
	lottery_template_name("彩票投注界面模版目录名称jiebao=捷豹版,v2=第二版,v2_1=第二版(1),v3=第三版,v4=第四版"),
	
	onoff_lot_official_credit_double_play("彩票官方信用双玩法模式开关"),
	
	onoff_mobile_version("手机版本[{v1},{v3}]"),

	lottery_bet_model("彩票投注模式，111-元角分模式、100-元模式"),
	
	mobile_lot_bet_model("手机（app）彩票下注模式，101 赔率单注模式，102:彩金注单模式"),
	
	mobile_lottery_code_icon_style("手机版彩种图标类型，101  爱彩版，102:综合版,103：完美版，104：信博版"),

	lottery_hot_stat_num("彩票冷热分析统计期数"),

	sai_che_swf_path_all("集群赛车视频路径", 2L),
	
	onoff_multi_rebate_record_count("多级代理返点记录条数", 2L),

	onoff_sai_che_swf("开启开奖视频"),

	sports_basketball_runball_4th_onoff("篮球滚球第四节投注开关"),

	sports_basketball_runball_checktime("篮球滚球待确认时间"),

	sports_football_runball_checktime("足球滚球待确认时间"),

	sports_min_bet_money("体育单注最低限额"),

	sports_max_bet_money("体育单注最高限额"),
	/**
	 * 根据gid判断
	 */
	sports_game_max_bet_money("体育单场最高限额"),

	jiebao_theme_content_color("捷豹彩票配色方案"),
	jiebao_menu_add_third("捷豹版本菜单添加真人电子"),

	jiebao_zst("彩票中心走势图"),

	pay_tips_deposit_fast("快速入款支付说明"),

	pay_tips_deposit_general("一般入款支付说明"),

	pay_tips_deposit_third("第三方支付说明"),
	
	pay_tips_deposit_virtual("虚拟货币入款支付说明"),

	lottery_send_rusult("系统彩开奖结果推送域名。"),

	jiebao_cash_name("账户金额名称"),

	model_ffc_generate_haoMa("分分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	model_2fc_generate_haoMa("2分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	model_5fc_generate_haoMa("5分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_hk5fc_generate_haoMa("香港5分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_am5fc_generate_haoMa("澳门5分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_10fc_generate_haoMa("10分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_20fc_generate_haoMa("20分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	sys_lottery_run_percentage("系统彩开奖中奖百分比，开奖模式为中奖百分比后才生效"),

	model_sflhc_generate_haoMa("十分六合彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	sf_mark_six_run_percentage("系统六合彩开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	model_wflhc_generate_haoMa("五分六合彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_hkmhlhc_generate_haoMa("香港马会六合彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_tmlhc_generate_haoMa("三分六合彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

//	wf_mark_six_run_percentage("五分六合彩开奖中奖百分比，开奖模式为中奖百分比后才生效"), //没用开关   应和系统六合彩一起

	model_ffk3_generate_haoMa("极速快三开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	model_wfk3_generate_haoMa("幸运快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_jpk3_generate_haoMa("日本快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_krk3_generate_haoMa("韩国快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_sfk3_generate_haoMa("十分快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_esk3_generate_haoMa("二十快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_tmk3_generate_haoMa("三分快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_hkk3_generate_haoMa("香港快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_amk3_generate_haoMa("澳门快三奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	sys_k3_run_percentage("系统快三开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_sf28_run_percentage("极速28开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_ff3d_run_percentage("极速3D开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_wf3d_run_percentage("五分3D开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_ff11x5_run_percentage("极速11选5开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_sf11x5_run_percentage("三分11选5开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_wf11x5_run_percentage("五分11选5开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_sfklsf_run_percentage("三分快乐十分开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_wfklsf_run_percentage("五分快乐十分开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	model_sfsc_generate_haoMa("极速赛车奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_ffsc_generate_haoMa("疯狂赛车奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_wfsc_generate_haoMa("五分赛车奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_wfft_generate_haoMa("五分飞艇奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_lbjsc_generate_haoMa("老北京赛车奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_sfft_generate_haoMa("极速飞艇奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),

	sys_sfsc_run_percentage("极速赛车开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_ffsc_run_percentage("疯狂赛车开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_wfsc_run_percentage("五分赛车开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_wfft_run_percentage("五分飞艇开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_lbjsc_run_percentage("老北京赛车开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	sys_sfft_run_percentage("极速飞艇开奖中奖百分比，开奖模式为中奖百分比后才生效"),
	
	model_txffc_generate_haoMa("腾讯分分彩开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_sf28_generate_haoMa("极速28开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_ff3d_generate_haoMa("极速3D开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_wf3d_generate_haoMa("五分3D开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_ff11x5_generate_haoMa("极速11选5开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_sf11x5_generate_haoMa("三分11选5开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_wf11x5_generate_haoMa("五分11选5开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_sfklsf_generate_haoMa("三分快乐十分开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	model_wfklsf_generate_haoMa("五分快乐十分开奖模式，rand=随机，pct=中奖百分比，push=从正式站推送开奖结果"),
	
	pay_third_url("第三版外部支付方式跳转地址"),

	/**
	 * 0跟空代表不限制。
	 */
	jie_bao_lottery_max_bet_money("捷豹彩票单注最大投注金额"),

	jie_bao_lottery_min_bet_money("捷豹彩票单注最小投注金额"),
	/**
	 * 1=时时彩,2=低频彩,3=PK10,5=11选5,6=香港彩,7=快三,8=快乐十分,9=PC蛋蛋(逗号隔开)
	 */
	lottery_group_sort("彩种组别排序"),
	/**
	 * 游戏投注记录开关
	 */
	onoff_lottery_record("彩票投注记录开关。on－开，off－关"),

	onoff_six_record("六合投注记录开关。on－开，off－关"),

	onoff_real_record("真人投注记录开关。on－开，off－关"),

	onoff_electronic_record("电子投注记录开关。on－开，off－关"),

	onoff_sport_record("体育投注记录开关。on－开，off－关"),

	/**
	 * 第三版奖金是否浮动开关
	 */
	bc_lottery_v3_dynam("第三版奖金浮动开关.on－开，off－关"),
	/**
	 * 第三版是否开启合买功能
	 */
	bc_lottery_v3_is_joint("是否开启合买功能（注：开启合买无法实现奖金浮动）"),

	agent_member_deposit_record("代理平台会员入款记录开关。on－开，off－关"),

	onoff_sign_in("签到开关。on－开，off－关"),
	
	onoff_turnlate("大转盘开关。on－开，off－关"),
	
	onoff_showhandmoney("人工扣款显示开关。on－开，off－关"),
	
	chat_notconfigured("聊天室未配置提示信息"),
	
	mobile_safari_add_to_home_screen("wap添加到主屏幕配置。[{off:不显示},{0:每次},{60:1小时一次},{1440:一天一次}]"),

	exchange_score("积分兑换"),
	
	payment_sort("网站入款方式排序。1-在线支付，2-快速入款，3-公司入款，4-虚拟货币入款"),
	
	guide_show_payment("引导账号要显示的入款方式。1-在线支付，2-快速入款，3-公司入款，4-虚拟货币入款"),

	mny_score_show("积分显示"),
	
	exchange_score_bet_num("积分兑换现金打码量开关"),

	test_station_off("会员注册赠送开关。on－开，off－关"),

	test_station_init_money("会员注册赠送金额"),
	
	agent_gift_onoff("代理注册赠送开关。on－开，off－关"),

	agent_gift_init_money("代理注册赠送金额"),

	register_send_money_bet_num("会员注册赠送金额打码量倍数"),

	on_off_register_test_guest_station("是否可以注册试玩账号。on－开，off－关"),
	
	onoff_agent_daili_search_type_default("租户后台代理查询默认最上级"),
	
	register_test_guest_station_count("站点试玩账户同一天同一IP注册次数"),

	register_test_guest_init_money("试玩账号初始会员金额"),

	access_statistics_code("访问统计代码"),
	
	onoff_lottery_change_order("彩票改单开关"),
	bc_lottery_change_order_count_limit("彩票改单次数限制"),
	
	onoff_daili_member_user_name_hidden("代理下线管理隐藏会员真实名字开关"),
	onoff_daili_member_user_contact_way_hidden("代理下线管理隐藏会员联系方式开关"),
	onoff_daili_member_user_show_all("代理下线管理全部信息显示开关(不在原开关上做改动)"),
	
	money_manager_member_list("资金管理会员集合"),
	money_manager_money_validation("金额格式验证表达式"),
	money_manager_validation_error_msg("验证不通过错误提示"),
	
	/**
	 * 单位小时 <= 0:不失效
	 * 
	 */
	deposit_record_timeout("存款未处理记录失效时间"),
	
	deposit_handler_cancle("存款取消已经成功记录开关"),
	withdraw_handler_cancle("提款取消已经成功记录开关"),

	/**
	 * 单位小时 <= 0:不失效
	 * 
	 */
	withdraw_record_timeout("提款未处理记录失效时间"),
	
	member_backwater_expired_time("会员反水自动过期(单位天数)"),
	
	onoff_platform_backwater("本平台数据反水开关"),
	
	mark_six_hexiao_49_change("六合彩合肖49选择(he:和局退反本金,normal:正常本命年)"),

	tenant_login_ip_check("租户后台登陆白名单。on－开，off－关"),
	/**
	 * 手机配置
	 */
	app_access_statistics_code("APP独立访问统计代码"),
	
	app_onoff_access_statistics_code("APP独立访问统计代码开关"),
	
	app_create_station_domain("APP打包域名"),
	
	app_download_link_ios("IOS版本app下载地址"),

	app_download_link_android("Android版本app下载地址"),

	app_qr_code_link_ios("IOS版本app二维码地址"),

	app_qr_code_link_android("Android版本app二维码地址"),
	
	app_qr_code_link_title("下载页公告提示"),

	mobile_topmenu_activities_action("手机顶部菜单活动按钮对应操作"),

	wap_first_jump_page("wap首页显示跳转页面。on－开，off－关"),

	wap_first_page_is_pc("wap显示PC首页"), wechat_appid("公众号Appid"),

	wechat_appsecret("公众号AppSecret"),

	wechat_login_domain("公众号授权域名"),

	mobile_open_outlink_type("手机打开外部链接的方式"),

	wap_qr_code_url("WAP二维码地址"),
	
	mobile_logo_url("手机版本LOGO地址"),
	
	mobile_head_color("手机头部背景色"),

	onoff_register_daili_checked("注册代理是否需要审核"),
	
	mobile_wechat_pop_tips("手机版微信弹窗提示"),

	mobile_pop_notice_style("手机弹窗样式  one-弹窗只显示一条  all-弹窗显示多条"),
	
	mobile_active_bg_color("手机版优惠活动背景颜色"),
	
	offon_station_first_nav_page("站点导航页开关"),
	
	mobile_desktop_webpage_icon_link("手机桌面网页版图标链接"),
	/**
	 * 真人中心配置(系统)
	 */
	sys_server_url("服务器地址", 2l),

	sys_real_center_url("接口地址", 2l),
	
	sys_real_center_url_tw("接口地址(台湾)", 2l),

	sys_real_center_token("授权口令", 2l),
	
	sys_chat_url("接口地址", 2l),
	
	sys_im_url("新版IM接口地址", 2l),
	
	agent_super_admin_white_ip("租户后台超级管理员白名单", 2l),

	sys_server_name("服务器名称", 2l),
	
	sys_lottery_change_order("彩票改单开关", 2l),
	
	admin_black_ip_config("全局黑名单配置", 2l),

	/**
	 * 真人配置
	 */
	sys_bbin_maintenance_onoff("BBIN维护开关", 2l),

	sys_ag_maintenance_onoff("AG维护开关", 2l),

	sys_mg_maintenance_onoff("MG维护开关", 2l),

	sys_pt_maintenance_onoff("PT维护开关", 2l),

	sys_qt_maintenance_onoff("QT维护开关", 2l),

	sys_ab_maintenance_onoff("AB维护开关", 2l),

	sys_og_maintenance_onoff("OG维护开关", 2l),
	
	sys_ds_maintenance_onoff("DS维护开关", 2l),
	
	sys_cq9_maintenance_onoff("CQ9维护开关", 2l),
	sys_jdb_maintenance_onoff("JDB维护开关", 2l),
	
	sys_isb_maintenance_onoff("ISB维护开关", 2l),
	sys_ttg_maintenance_onoff("TTG维护开关", 2l),
	sys_mw_maintenance_onoff("MW维护开关", 2l),
	
	sys_bg_maintenance_onoff("BG维护开关", 2l),
	
	sys_m8_maintenance_onoff("M8维护开关",2l),
	sys_ibc_maintenance_onoff("IBC维护开关",2l),
	sys_m8h_maintenance_onoff("M8H维护开关",2l),
	sys_vr_maintenance_onoff("VR维护开关",2l),
	sys_ky_maintenance_onoff("KY维护开关",2l),
	sys_ebet_maintenance_onoff("EBET维护开关",2l),
	sys_th_maintenance_onoff("TH维护开关",2l),

	/**
	 * 推广链接默认页面，没设置就是注册页面
	 */
	link_code_member_index_page("会员推广链接默认页面"),

	link_code_daili_index_page("代理推广链接默认页面"),

	link_code_mobile_index_page("手机推广链接默认页面"),
	
	agent_copy_info_botton("管理后台复制会员信息"),
	/**
	 * 在线支付中心(系统)
	 */
	sys_pay_center_url("在线支付中心地址"),

	onoff_log_onlinepay_notify("是否纪录在线充值的通知纪录。on－是，off－否"),

	lottery_order_cancle_switch("彩票撤单开关"),

	lottery_order_chase_switch("彩票追号开关"),

	register_verify_code_type("防刷注册验证码类型"),

	login_verify_code_type("登录验证码类型"),
	
	onoff_register_verify_code("注册验证码关闭开关"),
	
	sys_api_deposit_key("系统充值接口KEY"),
	
	sys_api_deposit_ips("系统充值接口IP白名单"),
	
	online_deposit_voice_remind("在线充值未到账语音提醒"),
	
	onoff_agent_login_otp_key("租户登录动态口令开关"),
	onoff_daili_multi_login("同代理允许多处登录开关"),
	
	deposit_interval_times("2次充值的时间间隔，单位秒"),
	withdraw_interval_times("2次提款的时间间隔，单位秒"),
	
	guide_account_deposit_auto_onoff("引导账号充值自动开关"),
	guide_account_withdraw_auto_onoff("引导账号提款自动开关"),
	guide_account_withdraw_not_need_betnum("引导账号提款不判断打码量"),
	
	onoff_agent_login_otp_key_btn("租户登录页面显示动态口令功能说明按钮"),
	
	onoff_score_to_zero("积分归零开关"),
	
	onoff_payment_show_info("支付页面显示二维码开关"),
	onoff_draw_show_copy_btn("出款审核页面显示复制按钮"),
    qz_group_switch("群组开关2 - on 开 -off关"),
	onoff_station_code_change("站点文件目录动态开关。on－开，off－关"),
	templete_station_code_list("展示模版代号数组"),
	
	guide_account_draw_hide_info("引导账号提款不显示信息"),
	
	bet_money_bigDecimal_scale_round("投注金额bigDecimalScaleRound"),
	
	/**
	 * 站点模板读取源
	 */
	station_folder_source("站点模板读取源。station-站点，domain-域名"),
	
	/**
	 * 站点会员动态赔率
	 */
	onoff_member_rate_random("动态赔率"),
	
	/**
	 * 站点会员动态赔率
	 */
	register_account_max_rate("无推广注册最大动态赔率"),
	
	/**
	 * 动态赔率反水放发开关
	 */
	onoff_rate_random_water_choice("动态赔率反水发放开关"),
	/**
	 * 动态赔率返点开关
	 */
	onoff_member_rate_random_rebate("动态赔率返点开关"),
	/**
	 * 分享注单限制金额
	 */
	share_order_money_limit("分享注单限制金额"),
	
	comprehensive_only_report_type_normal("统计概况只查询报表类型是普通的"),
	
	onoff_multi_proxy_daily_rebate_marksix("多级代理六合彩返点开关"),
	/**
	 * 第二版系统彩赔率租户配置开关
	 */
	onoff_system_loterryV2_peilv("第二版系统彩赔率租户配置开关"),
	
	/**
	 * 站点彩票返点模式(all,全部投注,win:中奖注单,unwin:未中奖注单)
	 */
	member_lottery_rebate_choice("站点彩票返点模式(all,全部投注,win:中奖注单,unwin:未中奖注单)"),
	
	
	/**
	 * 站点彩票租户多级代理返点开关
	 */
	agent_lottery_rebate_choice("站点彩票租户多级代理返点开关 off:关，on:开"),
	
	/**
	 * 北京赛车11号大小单双选择
	 */
	onoff_bjsc_11_select("北京赛车11号大小单双选择。draw:全和局,xiaodan:小单,dadan:大单,hedan:和单,dahe:大和,xiaohe:小和"),
	
	/**
	 * 开奖结果显示龙虎和开关选择
	 */
	onoff_dtSum_select("开奖结果显示龙虎和开关。on－开，off－关"),
	
	/**
	 * 快3豹子通杀选择
	 */
	onoff_k3_baozi_select("快3豹子通杀选择。off:不通杀(算点数),on:通杀"),
	
	/**
	 *  PC蛋蛋13、14选择
	 */
	onoff_pcegg_1314_select("PC蛋蛋13、14选择。off:和局,on:不和局(对应算法)"),
	/**
	 *  PC蛋蛋13、14单独赔率开关
	 */
	onoff_pcegg_1314_single("PC蛋蛋13、14单独赔率开关。off:关,on:开"),
	/**
	 *  打码量自动清0开关
	 */
	onoff_automatic_clear_0_betNum("打码量自动清0开关"),
	/**
	 *  快三官网开奖顺序开关
	 */
	onoff_official_lottery_order("快三官网开奖顺序开关"),
	/**
	 *  快三随机开奖对子限制开关
	 */
	onoff_k3_pair_limit_Period("快三随机开奖对子限制开关"),
	/**
	 *  pk10第五版猜前几玩法开关
	 */
	onoff_pk10_v5_Guess("pk10第五版猜前几玩法开关"),
	
	/**
	 *  代理参与在线人数统计开关
	 */
	onoff_agent_participation_online("代理参与在线人数统计开关"),
	
	/**
	 *  引导会员参与在线人数统计开关
	 */
	onoff_guide_participation_online("引导会员参与在线人数统计开关"),
	/**
	 *  报表类型试引导的参与在线人数统计开关
	 */
	report_guide_participation_online("引导报表参与在线人数统计开关"),
	/**
	 *  提款密码长度
	 */
	member_draw_password_length("取款密码位数长度"),
	/**
	 * 聊天室开关
	 */
	onoff_mobile_chat("手机聊天室开关"),
	
	onoff_im_chat("新版聊天室开关"),
	/**
	 * 聊天室一站多版开关
	 */
	onoff_chat_multiple_template("聊天室一站多版开关"),
	/**
	 * 1=彩票,2=体育,3=电子,4=真人,5=棋牌(逗号隔开)
	 */
	mobile_home_group_sort("手机版首页组別排序"),
	
	/**
	 * 前台访问白名单访问开关
	 */
	front_white_ip_permission("前台白名单访问开关，on-开，off-关"),
	/**
	 * 前台访问黑名单访问开关
	 */
	front_black_ip_permission("前台访问黑名单访问开关，on-开，off-关"),
	
	
	front_v4_index_version("v4版本首页切换,on-首页一，off-首页二"),
	
	/**
	 *团队报表管理只显示代理并且index时候显示最上一级代理
	 */
	onoff_totalStatistic_only_daili("团队报表管理只显示代理并且index时候显示最上一级代理。on－开，off－关"),
	
	deposit_artificial_2_com_record("手动加款记录添加到充值记录"),
	
	test_guest_alert("试玩账号提示由试玩账号...变成此账号..."),
	
	/**
	 *  余额宝站点开关
	 */
	onoff_balance_gem("站点余额宝开关，on-开启，off-关闭，ban-禁止转入"),
	
	chat_red_packet_permission("聊天室红包权限开关，off全部禁止，goff引导账号禁止"),
	
	on_off_payonlinerd_straight_failed("充值直接处理失败开关"),
	
	mobile_announcement_scroll_mode("手机最新公告滚动显示  tit-标题,con-内容"),
	
	onoff_k3_dianshu_1318_select("快三13,18点数玩法开关"),
	
	play_account_default_odds_percentage("试玩账号默认赔率百分比"),
	
	hot_game_list_on_off("热门游戏开关"),
	
	grab_many_red_packet("可以抢多个红包开关"),
	
	member_change_daili("会员转代理开关"),
	
	horizontal_app_qq_link("横板qq客服链接"),
	
	horizontal_app_wx_link("横板微信客服链接"),
	
	app_wx_qq_qrcode_link("QQ微信二维码链接"),
	
	onoff_app_get_address_book("app获取手机通讯录开关"),
	
	onoff_auto_trans_book("自动转换额度开关"),
	
	onoff_check_last_login_ip("检查最后登录ip"),
	
	onoff_Long_dragon("长龙助手开关"),
	
	guide_send_red_packet_password("引导账号发送红包密码"),
	
	system_replace_font("系统彩替换"),
	
	horizontal_mobile_onoff("app版本 ，101  爱彩版，102:综合版,103：完美版"),
	
	on_off_user_warning("预警功能关闭开关，on-开，off-关"),
	
	station_payment_password("代付密码"),
	station_artificial_interval("加款扣款相同时间间隔"),
	
	mobile_start_up_img("手机广告启动图，[0]图片url;[1]跳转url"),
	on_off_ag_test_guest("AG视讯试玩开关。on－开，off－关"),
	bet_time_interval("投注时间间隔 秒为单位"),
	not_handle_com_record("未处理的充值订单限制"),
	onoff_runball_bet("下注滚球电竞开关"),
	onoff_today_future_bet("下注今日赛事 早盘电竞开关，today今日赛事,future早盘"),
	onoff_agent_lottery_countdown("管理后台开奖结果倒计时"),
	register_regex_username("注册用户名正则表达式"),
	agent_page_query_special("层级特殊查询,全部代理只能查询代理数据  会员数据可以出现代理数据"),
	agent_modify_info("租户修改菜单信息,逗号分隔,[0]彩票改成竞猜,[1]皇冠体育改成体育竞技"),
	perfect_copy_url("完美版复制url"),
	onoff_hg_top_half_auto_balance("皇冠半场自动结算开关"),
	onoff_daili_review_close("代理后台审核关闭开关"),
	onoff_draw_rollback_close("后台提款回滚关闭开关"),
	onoff_agent_report_default_ordinary("后台默认普通报表类型开关"),
	onoff_Interface_current_limit("接口限流启用开关"),
	onoff_agent_lottery_quoto("平台彩票额度开关"),
	agent_lottery_quoto("平台彩票额度"),
	onoff_Force_priority_jump_login("强制优先跳转登陆开关");

	
	private String desc;
	private Long platform;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}

	private StationConfig(String _desc) {
		this.desc = _desc;
		this.platform = AgentBaseConfig.PLATFORM_AGENT;
	}

	private StationConfig(String _desc, long _platform) {
		this.desc = _desc;
		this.platform = _platform;
	}
	
	
}
