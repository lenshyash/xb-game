package com.game.constant;

/**
 * 
 * 第二版本彩种
 */
public enum GameTypeVersion2 {
	/** 彩种类型，8=pk10，9=时时彩，10=快三，11=PC蛋蛋，12=快乐十分，13=香港彩 **/

	pk10("PK10", 8, "PK10"), ssc("SSC", 9, "时时彩"), jssb3("JSSB3", 10, "快三"), pcegg("PCEGG", 11, "PC蛋蛋"), x5("klsf", 12,
			"快乐十分"), xgc("XGC", 6, "香港彩");

	public String EN; // 彩种名称
	public int type; // 类型
	public String CN;// 汉字名字

	private GameTypeVersion2(String EN, int type, String CN) {
		this.EN = EN;
		this.type = type;
		this.CN = CN;
	}

	public static GameTypeVersion2 getGameEmun(String name) {
		try {
			return GameTypeVersion2.valueOf(GameTypeVersion2.class, name);
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println(xgc.CN);
		// System.out.println(GameTypeVersion2.values());
		for (GameTypeVersion2 aa : GameTypeVersion2.values()) {
			System.out.println(aa.CN + "||" + aa.name() + "||" + aa.type);
		}
	}
}
