package com.game.constant;

/**
 * 
 *第一版本彩种
 */
public enum GameType {

	/** 彩种类型，1=系统彩，2=时时彩，3=pk10，4=排列三，5=11选5，6=香港彩，7=PC蛋蛋 **/

		xtc("XTC", 1, "系统彩"),ssc("SSC", 2, "时时彩"),pk10("PK10", 3, "PK10"),pl3("PL3", 4, "排列三")
		,x5("11X5", 5, "11选5"),xgc("XGC", 6, "香港彩"),pcegg("PCEGG", 7, "PC蛋蛋"),k3("K3", 100, "快三");
		
	
	
	public String EN; // 彩种名称
	public int type; // 类型
	public String CN;// 汉字名字

	private GameType(String EN, int type, String CN) {
		this.EN = EN;
		this.type = type;
		this.CN = CN;
	}

	public static GameType getGameEmun(String name) {
		try {
			return GameType.valueOf(GameType.class, name);
		} catch (Exception e) {
			return null;
		}
	}

	public static void main(String[] args) {
		System.out.println(xgc.CN);
//		System.out.println(GameType.values());
		for(GameType aa : GameType.values()){
			System.out.println(aa.CN+"||"+aa.name()+"||"+aa.type);
		}
	}
}
