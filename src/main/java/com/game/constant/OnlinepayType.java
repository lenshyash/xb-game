package com.game.constant;

public enum OnlinepayType {

	// 银宝支付、信盈支付、卡诚支付、仁信支付、信汇支付使用同一个第三方（程序类似）
	BAOFOO("baofoo", "baofoo", "宝付支付"), 
	YINGYUPAY("yingyupay","yingyupay","赢鱼支付"),
	KUAIDAPAY("kuaidapay", "kuaidapay", "快达支付"), 
	ALLINEPAY("allinepay", "allinepay", "万付宝"), 
	GOLDPAYMENT("goldpayment", "goldpayment", "黄金支付"), 
	MALLJLS("malljls", "malljls", "吉付宝"), 
	SAASPAY("saaspay", "saaspay", "SAAS支付平台"), 
	YAFUPAY("yafupay", "yafupay", "雅付支付"), 
	AIKULIPAY("aikulipay", "aikulipay", "皇冠支付"), 
	ONEEIGHTSEVEN("oneeightseven", "oneeightseven", "信付支付"), 
	THREETWOPAY("threetwopay", "threetwopay", "32PAY支付"), 
	PAY779("pay779", "pay779", "瑞付通支付"), 
	HLBPAY("hlbpay", "hlbpay", "好利宝支付"), 
	HAOLIANPAY("haolianpay", "haolianpay", "豪联支付"), 
	SHANFU("shanfu", "shanfu", "闪付支付"), 
	DINPAY40("dinpay40", "dinpay40", "智付支付4.0"), 
	DINPAY30("dinpay30", "dinpay30", "智付支付3.0"), 
	YEEPAY("yeepay", "yeepay", "易宝支付"), 
	IPSPAY70("ipspay70", "ipspay70", "环迅支付7.0"), 
	IPSPAY30("ipspay30", "ipspay30", "环迅支付3.0"), 
	REAPAL("reapal", "reapal", "融宝支付"), 
	HEEPAY("heepay", "heepay", "汇付宝"), 
	HEEPAY_WECHAT("heepay_wechat", "heepay_wechat", "汇付宝-微信支付"), 
	MOBAO("mobao", "mobao", "魔宝支付"), 
	YIFUBAO("yifubao", "yifubao", "易付宝"), // 与通汇卡［TONGHUIKA］相同
	YINBAO("yinbao", "yinbao", "银宝支付"), // 与信盈支付［XINYINGPAY］相同
	DANBAOPAY("danbaopay", "danbaopay", "钱海支付(微信、支付宝)"), 
	XBEIPAY("xbeipay", "xbeipay", "新贝支付"), 
	R1PAY("r1pay", "r1pay", "融e付"), // 与付乾支付［FUQPAY］相同
	YOMPAY("yompay", "yompay", "优付支付"), 
	YOMPAY20("yompay20", "yompay20", "优付支付2.0"), 
	KDPAY("kdpay", "kdpay", "口袋支付"), // 与新起点［XINQIDIAN］相同
	JINANFU("jinanfu", "jinanfu", "金安付"), 
	SHANGYINXIN("shangyinxin", "shangyinxin", "商银信"), 
	FUNPAY("funpay", "funpay", "乐盈支付"), 
	EKAPAY("ekapay", "ekapay", "千网支付"), // 与彩虹支付［CAIHONGPAY］相同
	MOBAO20("mobao20", "mobao20", "魔宝支付2.0"), 
	XUNHUIBAO("xunhuibao","xunhuibao","迅汇宝"),
	TONGHUIKA("tonghuika","tonghuika","通汇卡"), // 与易付宝［YIFUBAO］相同
	XUNFUTONG("xunfutong","xunfutong","迅付通"), // 与启付支付［QIFUPAY］相同
	EASYPAY("easypay","easypay","快付"),
	XINQIDIAN("xinqidian","xinqidian","新起点"), // 与口袋支付［KDPAY］相同
	CAIHONGPAY("caihongpay","caihongpay","彩虹支付"), // 与千网支付［EKAPAY］相同
	HUIXINPAY("huixinpay","huixinpay","汇鑫支付"),
	RENXINPAY("renxinpay","renxinpay","仁信支付"),
	IYIBANK("iyibank","iyibank","爱益支付"),
	AIZHUANPAY("aizhuanpay","aizhuanpay","爱赚支付"),
	WANGPAI("wangpai","wangpai","王牌支付"),
	FUQPAY("fuqpay","fuqpay","付乾支付"), // 与融e付［R1PAY］相同
	FULAPAY("fulapay","fulapay","付啦支付"),
	HUIBAOTONG("huibaotong","huibaotong","汇宝通"),
	MIAOFUPAY("miaofupay","miaofupay","秒付支付"), // 与易付宝［YIFUBAO］相同
	SHUFUPAY("shufupay","shufupay","舒付支付"),
	ZHIHPAY("zhihpay","zhihpay","智汇付"), // 与多得宝［DUODEBAO］、环迅支付3.0［IPSPAY30］相同
	ZHONGTIETONGFU("zhongtietongfu","zhongtietongfu","中铁通付"),
	NOWTOPAY("nowtopay","nowtopay","立刻付"), // 与银宝支付［YINBAO］相同
	HKKPAY("hkkpay","hkkpay","快付支付"),
	QIFUPAY("qifupay","qifupay","启付支付"), // 与迅付通［XUNFUTONG］相同
	HELIBAO("helibao","helibao","合利宝"),
	XINYINGPAY("xinyingpay","xinyingpay","信盈支付"), // 与银宝支付［YINBAO］相同
	JUBAOPAY("jubaopay","jubaopay","聚宝支付"),
	JUHEPAY("juhepay","juhepay","聚合支付"),
	DUODEBAO("duodebao","duodebao","多得宝"), // 与智汇付［ZHIHPAY］、环迅支付3.0［IPSPAY30］相同
	JINFUKA("jinfuka","jinfuka","金付卡"),
	ULINEPAY("ulinepay","ulinepay","优畅支付"),
	JINHAIZHE("jinhaizhe","jinhaizhe","金海哲"),
	QIXUNPAY("qixunpay","qixunpay","启迅支付"), // 与千网支付［EKAPAY］相同
	TONGBAOPAY("tongbaopay","tongbaopay","通宝支付"), // 与金安付［JINANFU］类似
	JUHEZAIXIAN("juhezaixian","juhezaixian","聚合在线"),
	QIFPAY("qifpay","qifpay","旗付支付"),
	KCPAY("kcpay","kcpay","和付支付"), // 与银宝支付［YINBAO］相似
	XINHUIPAY("xinhuipay","xinhuipay","信汇支付"), // 与仁信支付［RENXINPAY］相似
	NIXIAOKA("nixiaoka","nixiaoka","你销卡"), // 与银宝支付［YINBAO］相同
	TEAPAY("teapay","teapay","腾易宝"), // 与彩虹支付［CAIHONGPAY］相同
	YNNPAY("ynnpay","ynnpay","云盛支付"), 
	ZHISHUAPAY("zhishuapay","zhishuapay","智刷支付"), // 彩虹支付系
	DUOBAOPAY("duobaopay","duobaopay","多宝支付"), // 银宝系
	ANKUAIPAY("ankuaipay","ankuaipay","安快支付"), // 银宝系
	HIPAY("hipay","hipay","hi支付"), 
	BAIFUPAY("baifupay","baifupay","百付支付"), 
	SHUNFUPAY("shunfupay","shunfupay", "在线支付"), //瞬付支付 
	AIMISENPAY("aimisenpay","aimisenpay","艾米森"), 
	SHUNBAOPAY("shunbaopay","shunbaopay","顺宝支付"), 
	CHANGJIEPAY("changjiepay","changjiepay","畅捷支付"), 
	AOSIDUNPAY("aosidunpay","aosidunpay","奥斯顿"), 
	SANWUPAY("sanwupay","sanwupay","三五支付"), 
	NEWEPAY("newepay","newepay","新e付"), 
	XINMAPAY("xinmapay","xinmapay","新码支付"), 
	YINBANGPAY("yinbangpay","yinbangpay","银邦支付"), // 与金付卡［JINFUKA］相同
	SUPERSTARPAY("superstarpay","superstarpay","超级星支付"), // 与金付卡［JINFUKA］相同
	RUIMAFU("ruimafu","ruimafu","瑞码付"), // 与聚合在线［JUHEZAIXIAN］相同
	JUHEPUFA("juhepufa","juhepufa","聚合浦发"), 
	JUHERONGZHIFU("juherongzhifu","juherongzhifu","融智付聚合平台"), 
	XIANDAIJINKONG("xiandaijinkong","xiandaijinkong","现代金控"), 
	TIANCHUANGPAY("tianchuangpay","tianchuangpay","天创支付"), 
	WOOZF("woozf","woozf","沃支付"), 
	LUDEPAY("ludepay","ludepay","路德支付"), 
	HUITENGPAY("huitengpay","huitengpay","汇腾支付"), 
	HEBAOPAY("hebaopay","hebaopay","荷包支付"), 
	ZAIXIANBAO("zaixianbao","zaixianbao","在线宝"), 
	YIKAPAY("yikapay","yikapay","易卡支付"), 
	KUAIFUXIAODIAN("kuaifuxiaodian","kuaifuxiaodian","快付小店（星空宝）"), 
	ANTOPAY("antopay","antopay","云安付"), 
	HAIOPAY("haiopay","haiopay","海鸥闪付"), 
	DPAYHK("dpayhk","dpayhk","DPAY支付"), 
	SUHUIBAO("suhuibao","suhuibao","速汇宝"), 
	AK47PAY("ak47pay","ak47pay","AK47支付"), 
	ZESHENGPAY("zeshengpay","zeshengpay","泽圣支付"), 
	QINGYIFU("qingyifu","qingyifu","轻易付"), 
	CHANGCHENGZHIFU("changchengzhifu","changchengzhifu","长城支付"), 
	TIANJINCHUANGXINGOU("tianjinchuangxingou","tianjinchuangxingou","天津创新购"), 
	XUNBAOSHANGWU("xunbaoshangwu","xunbaoshangwu","讯宝商务"), 
	BAIFUTONG("baifutong","baifutong","百付通"), 
	GUOSHENGTONG("guoshengtong","guoshengtong","国盛通"), 
	YUNXUNZHIFU("yunxunzhifu","yunxunzhifu","云迅支付"), 
	YUANBAOZHIFU("yuanbaozhifu","yuanbaozhifu","元宝支付"), 
	UFUZHIFU("ufuzhifu","ufuzhifu","U付支付"), 
	WZHIFU("wzhifu","wzhifu","W支付"), 
	GAOTONGZHIFU("gaotongzhifu","gaotongzhifu","高通支付"), 
	OKPAY("okpay","okpay","OK支付"), 
	DINGFENGPAY("dingfengpay","dingfengpay","鼎峰支付"), 
	LUOBOPAY("luobopay","luobopay","萝卜支付"), 
	JUHEMINSHENG("juheminsheng","juheminsheng","聚合民生"), 
	YUNMAFUPAY("yunmafuPay","yunmafuPay","云码付"), 
	CHENGBAOPAY("chengbaopay","chengbaopay","诚宝支付"),
	QUANYINPAY("quanyinpay","quanyinpay","全银支付"),
	QUANYINPAYTWO("quanyinpaytwo","quanyinpaytwo","全银支付2"),
	HESHENGPAY("heshengpay","heshengpay","合生支付"),
	SHUANGCHENGPAY("shuangchengpay","shuangchengpay","双城支付"),
	ESHIDAI("eshidai","eshidai","E时代"),
	GMSTONE("gmstone","gmstone","讯通宝"),
	RUJIN8("rujin8","rujin8","金银宝"),
	ZTBAOPAY("ztbaopay","ztbaopay","智通宝"),
	XINGFUPAY("xingfupay","xingfupay","星付支付"),
	MMPAY("mmpay","mmpay","mmpay"),
	XUNJIETONG("xunjietong","xunjietong","迅捷通"),
	YUYPAY("yuypay","yuypay","钰盈支付"),
	SPDBWEB("spdbweb","spdbweb","深圳浦发"),
	BAOLEPAY("baolepay","baolepay","宝乐支付"),
	PUXUN101KA("puxun101ka","puxun101ka","普讯101卡"),
	CMBCPOS("cmbcpos","cmbcpos","民生银行"),
	SHKPAY("shkpay","shkpay","时刻宝"),
	YDPAY("ydpay","ydpay","易到"),
	FOUPANG("foupang","foupang","嘟嘟叫"),
	XJPAY("xjpay","xjpay","香蕉支付"),
	YIZHIBANK("yizhibank","yizhibank","易支付"),
	// YIFUPAY("yifupay","yifupay","易富支付"), // 不能对接， 订单号固定20位纯数字字符串，yyMMddHHmmssSSS+5位随机数。例如：17050210153352351211
	XINGHEYITONG("xingheyitong","xingheyitong","星和易通"),
	HENGFENGPAY("hengfengpay","hengfengpay","恒丰"),
	SHENGTONGKEJI("shengtongkeji","shengtongkeji","盛通科技"),
	BIFUPAY("bifupay","bifupay","必付支付"),
	XINGFUPAYWAP("xingfupaywap","xingfupaywap","星付支付(wap)"),
	ZESHENGPAYWAP("zeshengpaywap","zeshengpaywap","泽圣支付(wap)"),
	RUIYINFU("ruiyinfu","ruiyinfu","瑞银付"),
	JINSHUN("jinshun","jinshun","金顺支付"),
	JUEMAZHONGXING("juemazhongxing","juemazhongxing","爵玛中兴"),
	EBOOPAY("eboopay","eboopay","E宝支付"),
	FANGTEPAY("fangtepay","fangtepay","方特支付"),
	JIZHIPAY("jizhipay","jizhipay","极致支付"),
	HEYIFU("heyifu","heyifu","合易付"),
	YIAIPAY("yiaipay","yiaipay","易爱支付"),
	YIAIPAYH5("yiaipayh5","yiaipayh5","易爱H5支付"),
	SF532("sf532","sf532","神付通"),
	DUODEBAOH5("duodebaoh5","duodebaoh5","多得宝H5"), 
	JUHEAPI("juheapi","juheapi","聚合通用API"), 
	SIXBPAY("sixbpay","sixbpay","捷付支付"), 
	CAIMAO9("caimao9","caimao9","财猫快汇"), 
	XIIPAY("xiipay","xiipay","七月支付"), 
	NINEBAOPAY("ninebaopay","ninebaopay","云支付"), 
	ATRUSTPAY("atrustpay","atrustpay","信付宝"), 
	YUNXUNPAY("yunxunpay","yunxunpay","云讯支付"), 
	ZDBBILL("zdbbill","zdbbill","智得宝"), 
	RCPAY("rcpay","rcpay","　融财支付"), 
	YUEBAOPAY("yuebaopay","yuebaopay","月宝支付"), 
	MAGOPAY("magopay","magopay","芒果支付"), 
	SCQYDAPAY("scqydapay","scqydapay","汇达支付"), 
	JIAMANPAY("jiamanpay","jiamanpay","加满付"), 
	LUDEPAYH5("ludepayh5","ludepayh5","路德支付H5"), 
	LIANLIANSPC("lianlianspc","lianlianspc","连连付"), 
	ZIYOUPAY("ziyoupay","ziyoupay","自由付"), 
	BINGSHANPAY("bingshanpay","bingshanpay","冰山支付"), 
	DONGFANGPAY("dongfangpay","dongfangpay","东方支付"), 
	JUHEBOSSPAY("juhebosspay","juhebosspay","聚合BOSS支付"), 
	MIFUPAY("mifupay","mifupay","密付支付"), 
	HUIHEPAY("huihepay","huihepay","汇合支付"), 
	WUFUPAY("wufupay","wufupay","五福支付"), 
	JINYANGPAY("jinyangpay","jinyangpay","金阳支付"), 
	SHANGXINPAY("shangxinpay","shangxinpay","尚信支付"), 
	SHANGXINH5PAY("shangxinh5pay","shangxinh5pay","尚信支付H5"), 
	ZHONGXIN2DAI("zhongxin2dai","zhongxin2dai","中信2代"), 
	BOPAY("bopay","bopay","BOPAY"), 
	ZHIHUIBAO("zhihuibao","zhihuibao","智汇宝"), 
	ZHIFUHUI("zhifuhui","zhifuhui","支付汇"), 
	ZHIFUHUIH5("zhifuhuih5","zhifuhuih5","支付汇H5"), 
	BAIQIANFU("baiqianfu","baiqianfu","佰钱付"), 
	ZAIXIANBAOWAP("zaixianbaowap","zaixianbaowap","在线宝WAP"),
	XUNYINPAY("xunyinpay","xunyinpay","迅银支付"),
	RUYIPAY("ruyipay","ruyipay","如一付"),
	WUFUPAYH5("wufupayh5","wufupayh5","五福支付H5"),
	JUHEH5("juheh5","juheh5","聚合通H5"),
	LIYINGPAY("liyingpay","liyingpay","利盈支付"),
	DONGFANGKUAIFU("dongfangkuaifu","dongfangkuaifu","东方快付"),
	YIBAOTONGH5PAY("yibaotongh5pay","yibaotongh5pay","亿宝通H5"),
	MIBEIPAY("mibeipay","mibeipay","米贝支付"),
	BITEPAY("bitepay","bitepay","比特付"),
	CONGFU("congfu","congfu","聪付"),
	OUBAOPAY("oubaopay","oubaopay","欧宝支付"),
	JUFUYUN("jufuyun","jufuyun","在线支付"),
	SHUNXINFUPAY("shunxinfupay","shunxinfupay","顺心付"),
	EBOOPAYH5("eboopayh5","eboopayh5","E宝支付H5"),
	QUANNENGPAY("quannengpay","quannengpay","全能付"),
	ZHUOGEPAY("zhuogepay","zhuogepay","拙歌支付"),
	MCBPAY_ALIPAY("mcbpay_alipay","mcbpay_alipay","秒充宝-支付宝"),
	MCBPAY_WXPAY("mcbpay_wxpay","mcbpay_wxpay","秒充宝-微信"),

	RUYIPAYH5("ruyipayh5","ruyipayh5","如一付H5"),
	LINGDIANPAY("lingdianpay","lingdianpay","零点支付"),
	SHANGYINXINX("shangyinxinX","shangyinxinX","在线支付"),
	DEBAOZHIFU("debaozhifu","debaozhifu","得宝支付"),
	PAYPAL("paypal","paypal","paypal支付"),
	XIFUPAY("xifupay","xifupay","喜付支付"),
	ZHIFULE("zhifule","zhifule","支付乐"),
	XUNFEI("xunfei","xunfei","讯飞付"),
	SHANFUTONG("shanfutong","shanfutong","闪付通"),
	YINXUN("yinxun","yinxun","银讯"),
	XINBAO("xinbao","xinbao","新宝支付"),
	MIAOYIFU("miaoyifu","miaoyifu","秒易付"),
	YIZHIFU("yizhifu","yizhifu","益支付"),
	SULONG("sulong","sulong","速龙支付"),
	SHUIDI("shuidi","shuidi","水滴支付"),
	LIANYOU("lianyou","lianyou","链游旗下区块链支付"),
	OTHER("other", "other", "不匹配时返回");

	public String type; // 类型
	public String iconClass;// css的class样式名
	public String typeName;// 类型名称

	private OnlinepayType(String type, String iconClass, String typeName) {
		this.type = type;
		this.iconClass = iconClass;
		this.typeName = typeName;
	}

	public static OnlinepayType getOnlinepayType(String name) {
		try {
			return OnlinepayType.valueOf(OnlinepayType.class, name);
		} catch (Exception e) {
			return OTHER;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
