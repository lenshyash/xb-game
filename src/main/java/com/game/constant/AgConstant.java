package com.game.constant;


public class AgConstant{
	
	public enum AgStaticConstans {
		TRANS_TYPE_IN("IN","从网站账号转款到游戏账号"),
		TRANS_TYPE_OUT("OUT","從遊戲账號转款到網站賬號");
		
		public String key;
		public String name;
		
		private AgStaticConstans(String key, String name) {
			this.key = key;
			this.name = name;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	
	/**
	 * 游戏类型
	 *
	 */
	public enum  GameType{
		BAC("BAC","百家乐"),
		CBAC("CBAC","包桌百家乐"),
		LINK("LINK","连环百家乐"),
		DT("DT","龙虎"),
		SHB("SHB","骰宝"),
		ROU("ROU","轮盘"),
		FT("FT","番摊"),
		LBAC("LBAC","竞咪百家乐"),
		ULPK("ULPK","终极德州扑克"),
		
		
		GAME_TYPE_0("0","大厅"),
		GAME_TYPE_1("1","旗舰厅"),
		GAME_TYPE_2("2","国际厅"),
		GAME_TYPE_3("3","自选多台"),
		GAME_TYPE_4("4","包桌"),
		GAME_TYPE_5("5","竟眯"),
		GAME_TYPE_6("6","捕鱼王"),
		
		GAME_TYPE_11("11","HTML5 大厅 (AGIN 移动网页版游戏平台大厅)"),
		GAME_TYPE_12("12","旗舰厅 HTML5 百家乐"),
		GAME_TYPE_13("13","国际厅 HTML5 百家乐"),
		GAME_TYPE_14("14","旗舰厅 HTML5 龙虎"),
		GAME_TYPE_15("15","国际厅 HTML5 龙虎");
		
		public String gameTypeKey ;
		public String gameTypeName;
		
		private GameType(String gameTypeKey, String gameTypeName) {
			this.gameTypeKey = gameTypeKey;
			this.gameTypeName = gameTypeName;
		}
		
		public static String matchName(String gameTypeKey){
			GameType[] arrays = GameType.values();
			for (GameType gameType : arrays) {
				if(gameTypeKey.equals(gameType.gameTypeKey)){
					return gameType.gameTypeName;
				}
			}
			return null;
		}
	}
	
	/**
	 * 平台内的大厅类型 (round)
	 *
	 */
	public enum  Round{
		DSP("DSP","国际厅"),
		AGQ("AGQ","旗舰厅"),
		VIP("VIP","包桌厅"),
		SLOT("SLOT","电子游戏"),
		LED("LED","竞咪厅"),
		LOTTO("LOTTO","彩票");
		
		public String roundKey ;
		public String roundName;
		
		
		
		private Round(String roundKey, String roundName) {
			this.roundKey = roundKey;
			this.roundName = roundName;
		}
		public static String matchName(String roundKey){
			Round[] arrays = Round.values();
			for (Round round : arrays) {
				if(roundKey.equals(round.roundKey)){
					return round.roundName;
				}
			}
			return null;
		}
	}
	
	/**
	 * 结算状态
	 *
	 */
	public enum  Flag{
		FLAG1("1","已结算"),
		FLAG0("0","未结算"),
		FLAG_1("-1","重置试玩额度"),
		FLAG_2("-2","注单被篡改"),
		FLAG_8("-8","取消指定局注单"),
		FLAG_9("LOTTO","取消注单");
		
		public String flagKey ;
		public String flagName;
		

		private Flag(String flagKey, String flagName) {
			this.flagKey = flagKey;
			this.flagName = flagName;
		}

		public static String matchName(String flagKey){
			Flag[] arrays = Flag.values();
			for (Flag flag : arrays) {
				if(flagKey.equals(flag.flagKey)){
					return flag.flagName;
				}
			}
			return null;
		}
	}
	
	/**
	 * 
	 *平台名称
	 */
	public enum  PlatformType{
		AGIN("AGIN","AG 国际厅"),
		AG("AG","AG 旗舰厅极速版"),
		DSP("DSP","AG 实地厅"),
		IPM("IPM","IPM"),
		BBIN("BBIN","BBIN"),
		SABAH("SABAH","沙巴体育"),
		HG("HG","HG"),
		PT("PT","PT"),
		NMG("NMG","NMG"),
		OG("OG","东方游戏"),
		UGS("UGS","UGS"),
		HUNTER("HUNTER","捕鱼王"),
		AGTEX("AGTEX","棋牌大厅"),
		HBR("HBR","HBR"),
		XTD("XTD","新天地"),
		MG("MG","MG"),
		EBET("EBET","EBET");
		
		public String platformTypeKey ;
		public String platformTypeName;
		
		
		private PlatformType(String platformTypeKey, String platformTypeName) {
			this.platformTypeKey = platformTypeKey;
			this.platformTypeName = platformTypeName;
		}
		
		public static String matchName(String flagKey){
			PlatformType[] arrays = PlatformType.values();
			for (PlatformType platformType : arrays) {
				if(platformType.equals(platformType.platformTypeKey)){
					return platformType.platformTypeName;
				}
			}
			return null;
		}
	}
	
	
	/**
	 *玩法
	 */
	public enum  PlayType{
		PlayType1("1","百家乐 庄  0.95"),
		PlayType2("2","百家乐 闲 1.00"),
		PlayType3("3","百家乐 和 8.00"),
		PlayType4("4","百家乐 庄对 11.00"),
		PlayType5("5","百家乐 闲对 11.00"),
		PlayType6("6","百家乐 大 0.50"),
		PlayType7("7","百家乐 小 1.50"),
		
		PlayType11("11","百家乐 庄免佣 莊 6 點時 0.5,其他為 1.00"),
		PlayType12("12","百家乐 庄龙宝 投注时为准"),
		PlayType13("13","百家乐 闲龙宝 投注时为准"),
		
		PlayType21("21","龙虎玩法 龙 1.00"),
		PlayType22("22","龙虎玩法 虎 1.00"),
		PlayType23("23","龙虎玩法 和(龙虎)8.00"),
		
		PlayType41("41","骰宝 大 (big) 1"),
		PlayType42("42","骰宝 小 (small) 1"),
		PlayType43("43","骰宝 单 (single) 1"),
		PlayType44("44","骰宝 双 (double) 1"),
		PlayType45("45","骰宝 全围 (all wei) 24"),
		PlayType46("46","骰宝 围 1 (wei 1) 150"),
		PlayType47("47","骰宝 围 2 (wei 2) 150"),
		PlayType48("48","骰宝 围 3 (wei 3) 150"),
		PlayType49("49","骰宝 围 4 (wei 4) 150"),
		PlayType50("50","骰宝 围 5 (wei 5) 150"),
		PlayType51("51","骰宝 围 6 (wei 6) 150"),
		PlayType52("52","骰宝 单点 1 (single 1) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType53("53","骰宝 单点 2 (single 2) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType54("54","骰宝 单点 3 (single 3) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType55("55","骰宝 单点 4 (single 5) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType56("56","骰宝 单点 5 (single 6) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType57("57","骰宝 单点 6 (single 7) 区分同骰(1,2,3)赔率对应(1,2,3)"),
		PlayType58("58","骰宝 对子 1 (double 1) 8"),
		PlayType59("59","骰宝 对子 2 (double 2) 8"),
		PlayType60("60","骰宝 对子 3 (double 3) 8"),
		PlayType61("61","骰宝 对子 4 (double 4) 8"),
		PlayType62("62","骰宝 对子 5 (double 5) 8"),
		PlayType63("63","骰宝 对子 6 (double 6) 8"),
		PlayType64("64","骰宝 组合 12 (combine 12) 5"),
		PlayType65("65","骰宝 组合 13 (combine 13) 5"),
		PlayType66("66","骰宝 组合 14 (combine 14) 5"),
		PlayType67("67","骰宝 组合 15 (combine 15) 5"),
		PlayType68("68","骰宝 组合 16 (combine 16) 5"),
		PlayType69("69","骰宝 组合 23 (combine 23) 5"),
		PlayType70("70","骰宝 组合 24 (combine 24) 5"),
		PlayType71("71","骰宝 组合 25 (combine 25) 5"),
		PlayType72("72","骰宝 组合 26 (combine 26) 5"),
		PlayType73("73","骰宝 组合 34 (combine 34) 5"),
		PlayType74("74","骰宝 组合 35 (combine 35) 5"),
		PlayType75("75","骰宝 组合 36 (combine 36) 5"),
		PlayType76("76","骰宝 组合 45 (combine 45) 5"),
		PlayType77("77","骰宝 组合 46 (combine 46) 5"),
		PlayType78("78","骰宝 组合 56 (combine 56) 5"),
		PlayType79("79","骰宝 和值 4 (sum 4) 50"),
		PlayType80("80","骰宝 和值 5 (sum 5) 18"),
		PlayType81("81","骰宝 和值 6 (sum 6) 14"),
		PlayType82("82","骰宝 和值 7 (sum 7) 12"),
		PlayType83("83","骰宝 和值 8 (sum 8) 8"),
		PlayType84("84","骰宝 和值 9 (sum 9) 6"),
		PlayType85("85","骰宝 值 10 (sum 10) 6"),
		PlayType86("86","骰宝 值 11 (sum 11) 6"),
		PlayType87("87","骰宝 值 12 (sum 12) 6"),
		PlayType88("88","骰宝 值 13 (sum 13) 6"),
		PlayType89("89","骰宝 值 14 (sum 14) 6"),
		PlayType90("90","骰宝 值 15 (sum 15) 6"),
		PlayType91("91","骰宝 值 16 (sum 16) 6"),
		PlayType92("92","骰宝 值 17 (sum 17) 6"),

		
		PlayType101("101","轮盘 直接注 1:35"),
		PlayType102("102","轮盘 分注 1:17"),
		PlayType103("103","轮盘 街注 1:11"),
		PlayType104("104","轮盘 三數 1:11"),
		PlayType105("105","轮盘 4 個號碼 1:8"),
		PlayType106("106","轮盘 角注 1:8"),
		PlayType107("107","轮盘 列注(列 1) 1:2"),
		PlayType108("108","轮盘 列注(列 2) 1:2"),
		PlayType109("109","轮盘 列注(列 3) 1:2"),
		PlayType110("110","轮盘 線注 1:5"),
		PlayType111("111","轮盘 打一 1:2"),
		PlayType112("112","轮盘 打二 1:2"),
		PlayType113("113","轮盘 打三 1:2"),
		PlayType114("114","轮盘 紅 1:1"),
		PlayType115("115","轮盘 黑 1:1"),
		PlayType116("116","轮盘 大 1:1"),
		PlayType117("117","轮盘 小 1:1"),
		PlayType118("118","轮盘 單 1:1"),
		PlayType119("119","轮盘 雙 1:1"),
		
		
		
		
		
		PlayType130("130","番摊 1 番 2.8500"),
		PlayType131("131","番摊 2 番 2.8500"),
		PlayType132("132","番摊 3 番 2.8500"),
		PlayType133("133","番摊 4 番 2.8500"),
		PlayType134("134","番摊 1 念 2 1.9000"),
		PlayType135("135","番摊 1 念 3 1.9000"),
		PlayType136("136","番摊 1 念 4 1.9000"),
		PlayType137("137","番摊 2 念 1 1.9000"),
		PlayType138("138","番摊 2 念 3 1.9000"),
		PlayType139("139","番摊 2 念 4 1.9000"),
		PlayType140("140","番摊 3 念 1 1.9000"),
		PlayType141("141","番摊 3 念 2 1.9000"),
		PlayType142("142","番摊 3 念 4 1.9000"),
		PlayType143("143","番摊 4 念 1 1.9000"),
		PlayType144("144","番摊 4 念 2 1.9000"),
		PlayType145("145","番摊 4 念 3 1.9000"),
		PlayType146("146","番摊 角(1,2) 0.9500"),
		PlayType147("147","番摊 單 0.9500"),
		PlayType148("148","番摊 角(1,4) 0.9500"),
		PlayType149("149","番摊 角(2,3) 0.9500"),
		PlayType150("150","番摊 雙 0.9500"),
		PlayType151("151","番摊 角(3,4) 0.9500"),
		PlayType152("152","番摊 1,2 四 通 0.4750"),
		PlayType153("153","番摊 1,2 三 通 0.4750"),
		PlayType154("154","番摊 1,3 四 通 0.4750"),
		PlayType155("155","番摊 1,3 二 通 0.4750"),
		PlayType156("156","番摊 1,4 三 通 0.4750"),
		PlayType157("157","番摊 1,4 二 通 0.4750"),
		PlayType158("158","番摊 2,3 四 通 0.4750"),
		PlayType159("159","番摊 2,3 一 通 0.4750"),
		PlayType160("160","番摊 2,4 三 通 0.4750"),
		PlayType161("161","番摊 2,4 一 通 0.4750"),
		PlayType162("162","番摊 3,4 二 通 0.4750"),
		PlayType163("163","番摊 3,4 一 通 0.4750"),
		PlayType164("164","番摊 三門(3,2,1) 0.3200"),
		PlayType165("165","番摊 三門(2,1,4) 0.3200"),
		PlayType166("166","番摊 三門(1,4,3) 0.3200"),
		PlayType167("167","番摊 三門(4,3,2) 0.3200"),
		
		
		PlayType180("180","终极德州扑克 底注+盲注 投注时为准"),
		PlayType181("181","终极德州扑克 一倍加注 投注时为准"),
		PlayType182("182","终极德州扑克 二倍加注 投注时为准"),
		PlayType183("183","终极德州扑克 三倍加注 投注时为准"),
		PlayType184("184","终极德州扑克 四倍加注 投注时为准");
		
		public String playTypeKey ;
		public String playTypeName;
		

		private PlayType(String playTypeKey, String playTypeName) {
			this.playTypeKey = playTypeKey;
			this.playTypeName = playTypeName;
		}

		public static String matchName(String playTypeKey){
			PlayType[] arrays = PlayType.values();
			for (PlayType playType : arrays) {
				if(playTypeKey.equals(playType.playTypeKey)){
					return playType.playTypeName;
				}
			}
			return null;
		}
	}
}



