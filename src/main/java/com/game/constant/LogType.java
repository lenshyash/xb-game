package com.game.constant;

import com.game.model.SysLog;

public enum LogType {

	ADMIN_SYSTEM(1l, "系统管理", SysLog.PLATFORM_ADMIN),
	ADMIN_PLATFORM(2l, "平台管理", SysLog.PLATFORM_ADMIN),
	ADMIN_SPORT(3l,"体育管理", SysLog.PLATFORM_ADMIN),
	ADMIN_LOTTERY(4l, "彩票管理", SysLog.PLATFORM_ADMIN),
	ADMIN_REAL(5l, "真人管理",SysLog.PLATFORM_ADMIN),
	ADMIN_LOG(6l, "数据维护", SysLog.PLATFORM_ADMIN),
	ADMIN_JOINT_PURCHASE(7l,"合买管理",SysLog.PLATFORM_ADMIN),

	AGENT_SYSTEM(7l, "系统", SysLog.PLATFORM_PLATFORM),
	AGENT_FINANCE(8l, "财务", SysLog.PLATFORM_PLATFORM),
	AGENT_MEMBER(9l, "会员", SysLog.PLATFORM_PLATFORM),
	AGENT_AGENT(10l, "代理", SysLog.PLATFORM_PLATFORM),
	AGENT_SPORT(11l,"体育", SysLog.PLATFORM_PLATFORM),
	AGENT_LOTTER(12l, "彩票", SysLog.PLATFORM_PLATFORM),
	AGENT_REAL(13l,"真人", SysLog.PLATFORM_PLATFORM),
	AGENT_REPORT(14l, "报表", SysLog.PLATFORM_PLATFORM);

	private Long platform;
	private Long type;
	private String desc;

	public String getDesc() {
		return desc;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Long getPlatform() {
		return platform;
	}

	public void setPlatform(Long platform) {
		this.platform = platform;
	}

	private LogType(Long value, String desc, Long platform) {
		this.type = value;
		this.desc = desc;
		this.platform = platform;
	}
}
