package com.game.permission;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jay.frame.util.StringUtil;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.DataReader;
import com.game.model.AdminMenu;
import com.game.model.AdminUser;
import com.game.model.AgentMenu;
import com.game.model.SysAccount;
import com.game.service.AdminMenuService;
import com.game.service.AgentMenuService;
import com.game.util.SpringUtil;
import com.game.util.UserUtil;

public class PermissionManager {

	/**
	 * 总控后台权限Map 获取当前用户权限 key : url value ：{@link} AdminMenu status字段
	 * 
	 * @return
	 */
	public static Map<String, Long> getAdminPermissionMap() {
		AdminUser user = (AdminUser) UserUtil.getCurrentUser();
		final Long groupId = user.getGroupId();
		Map map = CacheUtil.getNull2Set(new DataReader<Map>() {
			public Map getData() {
				AdminMenuService as = (AdminMenuService) SpringUtil.getBean("adminMenuServiceImpl");
				List<AdminMenu> menu = as.getGroupPermissionMenu(groupId);
				return toAdminPermissionMap(menu);
			}
		}, Map.class, CacheType.ADMIN_GROUP_PERMISSION_MAP, groupId + "");
		return map;
	}

	/**
	 * 租户后台权限Set
	 * 
	 * @return
	 */
	public static Set<String> getAgentPermission() {
		SysAccount user = (SysAccount) UserUtil.getCurrentUser();
		Long groupId = user.getGroupId();
		if (groupId == null && user.getAccountType().equals(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
			groupId = 0L;
		}
		Set<String> set = null;
		if (groupId != null) {
			String key = "agent_perm_"+user.getStationId() + "_" + groupId;
			set = CacheUtil.getCache(CacheType.AGENT_GROUP_PERMISSION_MAP, key, Set.class);
			if (set != null) {
				return set;
			}
			set = toAgentPermissionSet(SpringUtil.getBean(AgentMenuService.class).getGroupPermissionMenu(groupId,user.getStationId()));
			if (set != null) {
				CacheUtil.addCache(CacheType.AGENT_GROUP_PERMISSION_MAP, key, set);
			}
		} else {
			set = new HashSet<>();
		}
		return set;
	}

	private static Map<String, Long> toAdminPermissionMap(List<AdminMenu> menu) {
		Map<String, Long> map = new HashMap<String, Long>();
		for (int i = 0; i < menu.size(); i++) {
			AdminMenu m = menu.get(i);
			String modulePath = StringUtil.trim2Null(m.getModulePath());
			if (StringUtil.isNotEmpty(modulePath)) {
				map.put(modulePath, m.getStatus());
			}
			String functionPath = StringUtil.trim2Null(m.getUrl());
			if (StringUtil.isNotEmpty(functionPath)) {
				map.put(functionPath, m.getStatus());
			}
		}
		return map;
	}

	private static Set<String> toAgentPermissionSet(List<AgentMenu> menu) {
		Set<String> set = new HashSet<>();
		if(menu==null){
			return set;
		}
		// 根目录权限都有
		set.add("/agent");
		for (int i = 0; i < menu.size(); i++) {
			AgentMenu m = menu.get(i);
			String modulePath = StringUtil.trim2Null(m.getModulePath());
			if (StringUtil.isNotEmpty(modulePath)) {
				set.add(modulePath);
			}
			String functionPath = StringUtil.trim2Null(m.getUrl());
			if (StringUtil.isNotEmpty(functionPath)) {
				set.add(functionPath);
			}
		}
		return set;
	}

}
