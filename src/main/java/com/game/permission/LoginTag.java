package com.game.permission;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.jay.frame.util.SysUtil;

public class LoginTag extends TagSupport {

	private String login;

	public String isLogin() {
		return login;
	}

	public void setLogin(String login) {

		if (login != null) {
			this.login = login.trim();
		} else {
			this.login = "";
		}
	}

	public int doStartTag() throws JspException {
		boolean isLogin = SysUtil.isLogin();
		if ("true".equalsIgnoreCase(login) && isLogin) {
			return EVAL_PAGE;
		} else if ("false".equalsIgnoreCase(login) && !isLogin) {
			return EVAL_PAGE;
		}
		return SKIP_BODY;
	}
}