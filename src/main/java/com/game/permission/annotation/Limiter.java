package com.game.permission.annotation;

import java.lang.annotation.*;

/**
 * 用于springmvc controller方法
 * 限流注解
 * @author admin
 *
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Limiter {
	double LimitNum() default  10;      //默认每秒产生10个令牌
}
