package com.game.permission.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)

/**
 * 用于检查当前请求ip和用户最后登录ip是否一致 用于管理后台
 * @author Administrator
 *
 */
public @interface CheckLastLoginIp {
	
}
