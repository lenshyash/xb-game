package com.game.permission;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.exception.RequestBusyException;
import com.game.permission.annotation.*;
import com.google.common.util.concurrent.RateLimiter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.NoLoginException;
import org.jay.frame.exception.PageNotFoundException;
import org.jay.frame.exception.PermissionCheckException;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.exception.ForbiddenException;
import com.game.exception.LoginErrorException;
import com.game.exception.PayDomainException;
import com.game.model.MemberLevelBase;
import com.game.model.MemberWhiteIp;
import com.game.model.MnyMoney;
import com.game.model.SysStationDomain;
import com.game.model.platform.AdminBlackIp;
import com.game.model.vo.AccountVo;
import com.game.service.AdminblackipService;
import com.game.service.AgentArticleService;
import com.game.service.MemberWhiteIpService;
import com.game.service.MnyBetNumberService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;
import com.game.service.SysMessageService;
import com.game.util.HttpUtils;
import com.game.util.IPSeeker;
import com.game.util.MessageUtils;
import com.game.util.SecurityCheckUtil;
import com.game.util.SpringUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

public class PermissionInterceptor extends HandlerInterceptorAdapter {
	private Logger logger = Logger.getLogger(PermissionInterceptor.class);
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private SysMessageService messageService;
	@Autowired
	private AgentArticleService agentArticleService;
	@Autowired
	private MnyBetNumberService mnyBetNumberService;
	
	private static final int ADMIN_BLACK_IP_REDIS_DB = 12;

	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {

	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view) throws Exception {
 		if (request.getRequestURI().indexOf("error.do") > 0) {
			return;
		}
		String hostUrl1 = HttpUtils.getDomain(request);
		request.setAttribute("hostUrl1", hostUrl1);
		request.setAttribute("base", request.getContextPath());
		request.setAttribute("pageUrl", request.getRequestURI());
		request.setAttribute("egameVersion", SystemConfig.PLATFROM_EGAME_VERSION);
		
		if (StationUtil.isAdminStation()) {
			request.setAttribute("station", request.getContextPath() + SystemConfig.ADMIN_CONTROL_PATH);
			request.setAttribute("_title", "游戏管理系统");
		} else if (StationUtil.isDailiStation()) {
			request.setAttribute("loginUser", UserUtil.getCurrentUser());
			request.setAttribute("_title", "代理平台");
			request.setAttribute("station", request.getContextPath() + SystemConfig.DAILI_CONTROL_PATH);

			if (UserUtil.isLogin()) {
				request.setAttribute("onlineCountNav", accountService.getOnlineCount());
				request.setAttribute("messageCountNav", MessageUtils.getMessageCount());
				// 是否是旧站导入代理，需要重新设置密码
				HttpSession session = UserUtil.getSession();

				Object obj = session.getAttribute(SystemConfig.SESSION_DAILI_KEY + "repwdFlag");
				if (obj != null && (Boolean) obj) {
					String url = request.getRequestURI();
					if (url.indexOf("/daili/repwdset") == -1) {
						response.sendRedirect(request.getContextPath() + "/daili/repwdset.do");
					}
				}
			}

			// 如果网站是维护状态 StationConfig.onoff_system == off
			if ("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_system)) && request.getRequestURL().toString().indexOf("/daili/systemmaintenance.do") == -1) {
				response.sendRedirect(request.getContextPath() + "/daili/systemmaintenance.do");
			}
		} else if (StationUtil.isAgentStation()) {
			request.setAttribute("station", request.getContextPath() + SystemConfig.AGENT_CONTROL_PATH);
			request.setAttribute("_title", StationUtil.getGloalStationName()+"管理后台");
		} else if (StationUtil.isMobileStation()) {
			request.setAttribute("station", request.getContextPath() + SystemConfig.MOBILE_CONTROL_PATH);
		}

		if (StationUtil.isMemberStation() || StationUtil.isMobileStation()) {
			Long stationId = StationUtil.getStationId();
			
			if (!ActionUtil.isAjaxRequest(request)) {
				request.setAttribute("chatPlatToken", StationUtil.getDomainPlatToken());
				request.setAttribute("_title", StationUtil.getGloalStationName());
				request.setAttribute("domainFolder", getDomainFolder());
				request.setAttribute("isLogin", UserUtil.isLogin());
				
				request.setAttribute("publicInfo", filterStr(agentArticleService.publicInfo(13)));
				request.setAttribute("isAppTerminal",checkAppStation());
				Long regSwitch = StationUtil.getIsRegSwitch();
				if (regSwitch == null) {
					regSwitch = LoginErrorException.LOGIN_ERROR_OTHER_LOGIN;
				}
				request.setAttribute("isRegSwitch", regSwitch);
			}
			if (UserUtil.isLogin()) {
				AccountVo loginMember = new AccountVo();
				MnyMoney m = moneyService.getMoneyById(UserUtil.getUserId());
				//如果会员余额为0判断是否打码量清0
				/*if(m.getMoney().compareTo(BigDecimal.ZERO)==0){
					//判断是否开启自动清理打码量开关
					if("on".equalsIgnoreCase(StationConfigUtil.get(StationUtil.getStationId(),StationConfig.onoff_automatic_clear_0_betNum))) {
						mnyBetNumberService.clearZeroBetNumber(UserUtil.getUserId());
					}
				}*/
				loginMember.setAccount(UserUtil.getUserAccount());
				loginMember.setMoney(m.getMoney().setScale(2, BigDecimal.ROUND_DOWN));
				loginMember.setAccountType(UserUtil.getType());
				MemberLevelBase level = UserUtil.getMemberLevel();
				if(level!=null){
					loginMember.setLevelName(level.getLevelName());
					if(StringUtil.isNotEmpty(level.getIcon())){
						loginMember.setLevelIcon(level.getIcon());
					}
				}
				request.setAttribute("loginMember", loginMember);
				request.setAttribute("indexMsgCount", messageService.getMessageCount());
				request.setAttribute("userInfo", UserUtil.getCurrentUser());
				request.setAttribute("accountLevelSwitch", StationConfigUtil.get(stationId, StationConfig.set_account_pc_level));
				
				// 是否是旧站导入会员，需要重新设置密码
				HttpSession session = UserUtil.getSession();
				Object obj = session.getAttribute(SystemConfig.SESSION_MEMBER_KEY + "repwdFlag");
				if (obj != null && (Boolean) obj) {
					String url = request.getRequestURI();
					if (url.indexOf("/center/member/meminfo/resetpwdpg") == -1) {
						response.sendRedirect(request.getContextPath() + "/center/member/meminfo/resetpwdpg.do");
					}
				}
			}

			// 如果网站是维护状态 StationConfig.onoff_system == off
			if (StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_system), "off") && request.getRequestURL().toString().indexOf("/index/systemmaintenance.do") == -1) {
				response.sendRedirect(request.getContextPath() + "/index/systemmaintenance.do");
			}

		}

	}
	private static String[] interArr = new String[]{"error.do","/register","/index.do","login","progress.do","getArticle.do"
			,"authnum.do","regpage.do","swregpage.do","registerMutil","verifycode","regVerifycode","down.do"
			,"getStationSwitch.do","getVerifyCodeSwitch.do","getStationInfo.do","/receiveResult/lottery.do","/reg_guest.do","regconf","checkPromo.do","getGuestSwitch.do"
			,"progress","getLunBo","getArticle","indexJoint","getPopNotices.do","/lottery/data.do"};
	private boolean checkStr(String str){
		if("/lotteryV3/index.do".equals(str)){
			return true;
		}
		if("/".equals(str)){
			return false;
		}
		for(String arr: Arrays.asList(interArr)){
			if(str.indexOf(arr)>-1){
				return false;
			}
		}
		return true;
	}

	public static boolean checkAppStation() {
		HttpServletRequest request = UserUtil.getRequest();
		String s1 = request.getHeader("user-agent");
		if(StringUtil.isEmpty(s1)) {
			return false;
		}
		s1 = s1.trim().toLowerCase();
		if (s1.contains("android") || s1.contains("iphone") || s1.contains("ipad") || s1.contains("ipod")) {
			return true;
		} else {
			return false;
		}
	}
	
	private static String getDomainFolder() {
		String doaminFolder = StationUtil.getDomainFolder();
		if(StringUtil.isEmpty(doaminFolder)) {
			return StationUtil.getFolder();
		}
		return doaminFolder;
	}

	private static String filterStr(String con) {
		con = StringUtils.replacePattern(con, "\\n", "");
		con = StringUtils.replacePattern(con, "'", "\"");
		return con;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// 处理Permission Annotation，实现方法级权限控制
		HandlerMethod method = (HandlerMethod) handler;
		
		//检查全局黑名单
		NotNeedLogin nnl = method.getMethodAnnotation(NotNeedLogin.class);
		//加入全局黑名单的先去掉
		//abc(request, nnl);
		
		// 检测路径是否可访问
		checkPathPermission(request);

		// 检查是否开启强制跳转登陆
		if(!checkMandatoryLogin(request,response)){
			return false;
		}


		// 检测是否存在登录异常情况
		checkLoginError();

		// 创建分页参数
		SortMapping sm = method.getMethodAnnotation(SortMapping.class);
		if (sm != null) {
			createPaggingSortParamter(sm, request);
		}
		//手机端添加跨域允许
		if (StationUtil.isMobileStation() || StationUtil.isNativeStation()) {// 手机端
			this.AllowCrossAomain(response,request);
		}

		//如果是前端接口判断限流
		if(StationUtil.isMemberPage() || StationUtil.isMobilePage() || StationUtil.isNativeStation()){
			//接口限流
			//获取限流接口信息
			Limiter lim = method.getMethodAnnotation(Limiter.class);
			if(lim!=null) {
				StringBuffer requestUrl = request.getRequestURL();
				String domain = WebUtil.getDomain(requestUrl.toString());
				this.interfaceCurrentLimit(lim, method.getMethod().getName(), domain);
			}
		}



		// 需登陆才能访问
		if (nnl != null) {
			return true;
		}else{
			//原生接口判断网站是否关闭
			if(StationUtil.isNativeStation()){
				this.checkSystemColse();
			}
			// 如果为空在表示该方法需要进行登陆验证
			if (!SysUtil.isLogin()) {
				if(StationUtil.isApiStation()) {
					throw new GenericException("登录状态失效，请重新登录");
				}
				throw new NoLoginException();
			}
		}
		if(StationUtil.isAgentPage()){
			//最后登录ip和当前ip进行对比
			CheckLastLoginIp clll = method.getMethodAnnotation(CheckLastLoginIp.class);
			if(clll != null) {
				SecurityCheckUtil.checkLastLoginIpRequestIp(UserUtil.getUserId(), UserUtil.getIpAddress());
			}
		}

		if (StationUtil.isAdminStation()) {// 总控后台
			this.checkAdminPermission(request, method);
		} else if (StationUtil.isAgentStation()) {// 租户后台
			this.checkAgentPermission(request, method);
		} else if (StationUtil.isDailiStation()) {// 代理后台
			this.checkDailiPermission(request);
		} else if (StationUtil.isMobileStation()) {// 手机端
			this.checkMobilePermission(request);
		} else {// 会员端
			this.checkMemberPermission(request);
		}
		
		return true;
	}

	private boolean checkMandatoryLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//判断是否开启强制跳转登陆页开关
		if(StationUtil.isMemberStation() || StationUtil.isMobileStation() || StationUtil.isNativeStation()){
			if(UserUtil.isLogin()){
				return true;
			}
			Long stationId = StationUtil.getStationId();
			if(!StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.onoff_Force_priority_jump_login), "on")){
				return true;
			}
			String url = request.getRequestURI();
			if (checkStr(url)) {
				String base = request.getContextPath();
				if (StationUtil.isMobilePage()) {
					String flag=  StationConfigUtil.get(stationId, StationConfig.onoff_mobile_version);
					if(StringUtil.isNotEmpty(flag)){
						if("on".equals(flag)){
							response.sendRedirect(base + "/mobile/v3/login.do");
							return false;
						} else if("off".equals(flag)){
							response.sendRedirect(base + "/mobile/login.do");
							return false;
						}
						if("new".equals(flag)||"v5".equals(flag)||"v3new".equals(flag)){
							response.sendRedirect(base + "/mobile/index.do");
							return false;
						}
					}
					response.sendRedirect(base + "/mobile/login.do");
					return false;
				}else if(StationUtil.isMemberPage()){
					//如果是pc版需要根据版本针对跳转
					String folder = StationUtil.getFirstFolder();
					String index = folder.substring(0,1);
					if("b".equals(index)||"t".equals(index) ||"a".equals(index)){
						response.sendRedirect(base + "/index.do");
						return false;
					}else if("d".equals(index)){
						response.sendRedirect(base + "/loginPage.do");
						return false;
					}else if("x".equals(index)){
						response.sendRedirect(base + "/index.do");
						return false;
					}
				} else if(StationUtil.isNativeStation()){
					JSONObject json = new JSONObject();
					json.put("isLogin",false);
					json.put("success",false);
					json.put("msg","请先登陆");
					ActionUtil.render("application/json", JsonUtil.toJson(json),null, response);
					return false;
				}else {
					response.sendRedirect(base + "/timeout.do");
					return false;
				}
			}
		}
		return true;
	}


	private void checkSystemColse() {
		// 如果网站是维护状态 StationConfig.onoff_system == off
		if ("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_system))) {
			String desc = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.system_maintenance);
			String msg = "网站正在维护中，禁止访问。";
			if(StringUtil.isNotEmpty(desc)){
				msg += "【维护原因】："+desc;
			}
			throw new GenericException(msg,false);
		}
	}

	/**
	 * 允许手机端跨域请求
	 * @param response
	 */
	private void AllowCrossAomain(HttpServletResponse response,HttpServletRequest request) {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin",request.getHeader("Origin"));//设置允许跨域请求
    	response.addHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    	response.addHeader("Access-Control-Allow-Credentials", "true");
	}

	private void createPaggingSortParamter(SortMapping sm, HttpServletRequest request) {
		String mapping[] = sm.mapping();
		String sortType = request.getParameter("sortOrder");
		String sortName = request.getParameter("sortName");
		String defColumn = sm.defColumn();
		String defSortType = sm.defSortType();
		if (StringUtil.isEmpty(sortName)) {// 前端未传参数过来，使用默认排序字段
			if (StringUtil.isNotEmpty(defColumn)) {
				UserUtil.setSortInfo(defColumn, defSortType);
			}
		} else {
			int size = mapping.length / 2;
			for (int i = 0; i < size; i++) {
				String sn = mapping[i * 2];
				if (StringUtils.equals(sn, sortName)) {
					UserUtil.setSortInfo(mapping[i * 2 + 1], sortType);
					break;
				}
			}
		}
	}

	private void checkLoginError() {
		HttpServletRequest request = UserUtil.getRequest();
		String url = request.getRequestURI();
		if (url.endsWith("/loginError.do")) {
			return;
		}
		HttpSession session = UserUtil.getSession();
		if (session == null) {
			return;
		}

	}

	/**
	 * 根据当前站点类型判断当前访问路径是否有权限 域名隔离访问路径
	 */
	private void checkPathPermission(HttpServletRequest request) {

		String url = request.getRequestURI();
		String basePath = request.getContextPath();

		/**
		 * 检查前台访问ip限制
		 */

		if (StationUtil.isMemberPage() || StationUtil.isMobilePage() || StationUtil.isNativePage()) {
//			long start = System.currentTimeMillis();
			if ("on".equals(StationConfigUtil.get(StationConfig.front_white_ip_permission))) {
				if (StringUtils.indexOf(url, "/error.do") == -1) {
					checkIpPermission();
				}
			}
			
			if ("on".equals(StationConfigUtil.get(StationConfig.front_black_ip_permission))) {
				if (StringUtils.indexOf(url, "/error.do") == -1) {
					checkBlackIpPermission();
				}
			}
			
			//logger.error("检查ip限制用时：" + (System.currentTimeMillis() - start));
		}

		// 总控后台只能访问 首页 、/admin/前缀开头的地址
		if (StationUtil.isAdminStation()) {
			// 不允许总控域名问会员前端跟代理后台
			if (!url.equals(basePath + "/") && !url.startsWith(basePath + SystemConfig.ADMIN_CONTROL_PATH + "/")) {
				throw new PageNotFoundException();
			}
		}
		// 接受数据地址
		if (StationUtil.isReceiveStation()) {
			if (StringUtils.indexOf(url, "/receive") == -1) {
				throw new PageNotFoundException();
			}
		}
		// 不允许盘口站点域名访问总控后台
		if (StationUtil.isAgentStation() || StationUtil.isMemberStation() || StationUtil.isDailiStation() || StationUtil.isMobileStation()) {
			if (url.startsWith(basePath + SystemConfig.ADMIN_CONTROL_PATH + "/")) {
				throw new PageNotFoundException();
			}
		}

		Long domainType = StationUtil.getDomainType();
		// 不允许后台类型域名访问非后台地址
		if (domainType != null && domainType == SysStationDomain.TYPE_AGENT) {
			if (!url.startsWith(basePath + SystemConfig.AGENT_CONTROL_PATH + "/") && !url.equals(basePath + "/")) {
				throw new PageNotFoundException();
			}
		}
		
		//支付域名 只能访问支付相关的链接
		if (domainType != null && domainType == SysStationDomain.TYPE_PAY){
		
			if(!url.startsWith(basePath +"/onlinepay/utils/") &&  !url.startsWith(basePath +"/onlinepay/") 
					&& !url.startsWith(basePath +"/shunpay/") && !url.startsWith(basePath +"/scanpay/") ) {
				throw new PayDomainException("支付域名不能访问其他页面");
			}
		}
		
		// 不允许前台类型域名反问后台地址
		if (domainType != null && domainType == SysStationDomain.TYPE_MEMBER) {
			if (url.startsWith(basePath + SystemConfig.AGENT_CONTROL_PATH + "/")) {
				throw new PageNotFoundException();
			}
		}
		
		// 第三方访问api
		if (StationUtil.isApiStation()) {
			if (StringUtils.indexOf(url, "/api") == -1 && StringUtils.indexOf(url, "/error.do") == -1) {
				throw new GenericException("没有访问权限");
			}
		}
	}

	private void checkIpPermission() {
		String ip = UserUtil.getIpAddress();
		boolean isChinaIp = IPSeeker.getInstance().isChinaIp(ip);
		if (!isChinaIp) {
			checkIpList(ip);
		}
	}

	private void checkIpList(String ip) {
		MemberWhiteIpService ipService = SpringUtil.getBean(MemberWhiteIpService.class);
		List<MemberWhiteIp> ipList = ipService.getStationWhiteIpList(MemberWhiteIp.TYPE_WHITE);
		if (ipList.isEmpty()) {
			throw new ForbiddenException();
		} else {
			boolean forbidden = true;
			for (MemberWhiteIp sIp : ipList) {
				if (ip.equals(sIp.getIp())) {
					forbidden = false;
					break;
				}
			}
			if (forbidden) {
				throw new ForbiddenException();
			}
		}
	}
	
	private void checkBlackIpPermission() {
		String ip = UserUtil.getIpAddress();
		MemberWhiteIpService ipService = SpringUtil.getBean(MemberWhiteIpService.class);
		List<MemberWhiteIp> ipList = ipService.getStationWhiteIpList(MemberWhiteIp.TYPE_BLACK);
		if (!ipList.isEmpty()){
			boolean forbidden = false;
			for (MemberWhiteIp sIp : ipList) {
				if (ip.equals(sIp.getIp())) {
					forbidden = true;
					break;
				}
			}
			if (forbidden) {
				throw new ForbiddenException();
			}
		}
		
	}
	/**
	 * 检测总控后台权限
	 * 
	 * @param request
	 */
	private void checkAdminPermission(HttpServletRequest request, HandlerMethod method) {
		// root用户，具备后台所有权限
		/**
		 * if(UserUtil.isSuperAdmin()){ return; }
		 **/

		String contentPath = request.getContextPath();
		String path = request.getRequestURI();
		if (!contentPath.equals("/")) { // 去除项目名
			path = path.substring(contentPath.length());
		}
		Permission p = method.getMethodAnnotation(Permission.class);
		String key = null;

		// 若没有Permission标签，则默认该方法只要有模块权限即可访问
		CheckType ct = (p == null) ? CheckType.MODULE : p.value();

		// 功能暂停使用
		if (ct == CheckType.CLOSE) {
			throw new PermissionCheckException("该功能已经暂停使用!");
		}

		// 开放权限，无需验证
		if (ct == CheckType.OPEN) {
			return;
		}

		if (ct == CheckType.MODULE) {
			key = WebUtil.toModuleURL(path);
		} else if (ct == CheckType.FUNCTION) {
			key = WebUtil.toContentURL(path);
		}

		Map<String, Long> pm = PermissionManager.getAdminPermissionMap();
		if (!pm.containsKey(key)) {
			throw new PermissionCheckException("你没有该项操作的权限，请与管理员联系!");
		}
	}

	/**
	 * 检测租户后台权限
	 * 
	 * @param request
	 */
	private void checkAgentPermission(HttpServletRequest request, HandlerMethod method) {
		String contentPath = request.getContextPath();
		String path = request.getRequestURI();
		if (!contentPath.equals("/")) { // 去除项目名
			path = path.substring(contentPath.length());
		}
		Permission p = method.getMethodAnnotation(Permission.class);
		String key = null;

		// 若没有Permission标签，则默认该方法只要有模块权限即可访问
		CheckType ct = (p == null) ? CheckType.MODULE : p.value();

		// 功能暂停使用
		if (ct == CheckType.CLOSE) {
			throw new PermissionCheckException("该功能已经暂停使用!");
		}

		// 开放权限，无需验证
		if (ct == CheckType.OPEN) {
			return;
		}

		if (ct == CheckType.MODULE) {
			key = WebUtil.toModuleURL(path);
		} else if (ct == CheckType.FUNCTION) {
			key = WebUtil.toContentURL(path);
		}

		Set<String> permSet = PermissionManager.getAgentPermission();
		if (!permSet.contains(key) && !permSet.contains(key + "/")) {
			throw new PermissionCheckException("你没有该项[" + key + "]操作的权限，请与管理员联系!");
		}
	}

	/**
	 * 检测代理后台权限
	 * 
	 * @param request
	 */
	private void checkDailiPermission(HttpServletRequest request) {

	}

	/**
	 * 检测会员权限
	 * 
	 * @param request
	 */
	private void checkMemberPermission(HttpServletRequest request) {

	}

	/**
	 * 检测手机端权限
	 * 
	 * @param request
	 */
	private void checkMobilePermission(HttpServletRequest request) {

	}
	
	/**
	 * 先检查全局白名单
	 */
	private void checkAdminBlackIp(String ip, AdminblackipService adminblackipService) {
		Boolean foreign = false;
		List<AdminBlackIp> list = adminblackipService.getList();
		
		for (AdminBlackIp adminBlackIp : list) {
			if(adminBlackIp.getIp().equals(ip)) {
				foreign = true;
				break;
			}
		}
		
		if(foreign) {
			throw new ForbiddenException();
		}
		
	}
	
	/**
	 * redis 存储val 第一个是时间戳 第二个请求次数  需要根据当前时间和redis的时间戳进行对比 最后判断次数
	 * @param ip
	 * @param adminblackipService
	 * @param nnl 
	 */
	private void addAdminBlackIP(HttpServletRequest request, String ip, AdminblackipService adminblackipService, NotNeedLogin nnl) {
		String cfg = StationConfigUtil.getSys(StationConfig.admin_black_ip_config);
		if(StringUtil.isNotEmpty(cfg) && nnl!=null) {
			int second = Integer.parseInt(cfg.split(";")[0]); //多少秒内
			int number = Integer.parseInt(cfg.split(";")[1]);//的请求次数
			
			String key = "request_"+request.getRequestURI()+"_"+ip;
			
			Long incrby = RedisAPI.incrby(key, 1L, ADMIN_BLACK_IP_REDIS_DB);
			if(incrby==1) {
				RedisAPI.expire(key, second, ADMIN_BLACK_IP_REDIS_DB);
			}
			
			if(incrby>number) {
				AdminBlackIp adminBlackIp = new AdminBlackIp();
				adminBlackIp.setIp(ip);
				adminBlackIp.setCreateDatetime(new Date());
				
				adminblackipService.add(adminBlackIp);
			}
		}
		
	}
	
	private void abc(HttpServletRequest request, NotNeedLogin nnl) {
		//不是管理后台和总控后台
		if(!StationUtil.isAdminStation() && !StationUtil.isAgentStation()) {
			String ip = UserUtil.getIpAddress();
			AdminblackipService adminblackipService = SpringUtil.getBean(AdminblackipService.class);
			
			checkAdminBlackIp(ip, adminblackipService);
			addAdminBlackIP(request, ip, adminblackipService, nnl);
		}
		
	}
	//创建一个ConcurrentHashMap来存放各个方法和它们自己对应的RateLimiter对象
	private static final ConcurrentMap<String,ConcurrentMap<String, RateLimiter>>RATE_LIMITER = new ConcurrentHashMap<>();
	private RateLimiter rateLimiter;
	private void interfaceCurrentLimit(Limiter annotation, String functionName, String domain) {
		if (!"on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_Interface_current_limit))) {
			return;
		}
		String key = "LIMIT:DOMAIN:";
		//判断当前域名是否开启访问限制
		String numStr = CacheUtil.getCache(CacheType.STATION_DOMAIN,key+domain);
		if(StringUtil.isEmpty(numStr)||"0".equals(numStr)){
			return;
		}
		double limitNum = Double.valueOf(numStr);
		//获取域名map，存在则直接获取令牌数据，不存在新建
		if(RATE_LIMITER.containsKey(domain)){
			ConcurrentMap<String, RateLimiter> ltMap = RATE_LIMITER.get(domain);
			if(ltMap.containsKey(functionName)){
				rateLimiter=ltMap.get(functionName);
				//判断限流量是否有变动，有变动则更新
				if(rateLimiter.getRate()!=limitNum){
					rateLimiter.setRate(limitNum);
				}
			}else {
				ltMap.put(functionName,RateLimiter.create(limitNum));
				rateLimiter=ltMap.get(functionName);
				RATE_LIMITER.put(domain,ltMap);
			}
		}else{
			ConcurrentMap<String, RateLimiter> newMap =  new ConcurrentHashMap<>();
			newMap.put(functionName,RateLimiter.create(limitNum));
			rateLimiter=newMap.get(functionName);
			RATE_LIMITER.put(domain,newMap);
		}
//		double limitNum = annotation.LimitNum(); //获取注解每秒加入桶中的token
		if(rateLimiter.tryAcquire()) {
			return;
		}
		else {
			//返回错误，不打印日志
			throw new RequestBusyException("数据请求繁忙请稍后再试");
		}
	}

}
