package com.game.readpacket;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RedPacketUtil {
	static public List<BigDecimal> splitRedPackets(BigDecimal allMoney, int count, BigDecimal min) {
		BigDecimal minNeed = min.multiply(new BigDecimal(count));
		List<BigDecimal> rlist = new ArrayList<>();
		if (minNeed.compareTo(allMoney) == 0) {
			for (int i = 0; i < count; i++) {
				rlist.add(min);
			}
			return rlist;
		}
		List<Integer> perList = new ArrayList<>();
		int all = 0;
		for (int i = 0; i < count; i++) {
			int c = getRandomInt();
			perList.add(c);
			all += c;
		}
		BigDecimal allPer = new BigDecimal(all);
		BigDecimal remain = allMoney.subtract(minNeed);
		BigDecimal send = BigDecimal.ZERO;
		BigDecimal cur = null;
		for (int i = 0; i < count - 1; i++) {
			cur = remain.multiply(new BigDecimal(perList.get(i))).divide(allPer, 2, RoundingMode.DOWN);
			rlist.add(min.add(cur));
			send = send.add(cur);
		}
		rlist.add(min.add(remain).subtract(send));
		return rlist;
	}

	static private int getRandomInt() {
		Random random = new Random();
//		return random.nextInt(100);
        int max=100;
        int min=50;
        return random.nextInt(max)%(max-min+1) + min;
	}

	public static void main(String[] args) {
		List<BigDecimal> l = splitRedPackets(new BigDecimal(500), 30, new BigDecimal(5));
		BigDecimal all = BigDecimal.ZERO;
		for (BigDecimal b : l) {
			System.out.println(b);
			all = all.add(b);
		}
		System.out.println(all);
	}
}
