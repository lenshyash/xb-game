package com.game.readpacket;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.controller.member.onlinepay.OnlinepayUtilsController;
import com.game.util.MD5Util;
import com.game.util.StationConfigUtil;

public class CommonRedpacketUtil {
	public static final Logger LOGGER = Logger.getLogger(CommonRedpacketUtil.class);
	
	private final static String MD5_KEY = "HKASD*!(@#&DAKJHYFG&A123hJHGU";

	static public void verifySign(String params, String sign) {

		if(StringUtil.isEmpty(params) || StringUtil.isEmpty(sign)) {
			throw new GenericException("参数不完整出错");
		}
		String argPreSign = getSign(params);
		LOGGER.error("params:"+params+"\nsign:"+sign+"\n"+"argPreSign:"+argPreSign);
		if (!argPreSign.equalsIgnoreCase(sign)) {
			throw new GenericException("验签出错");
		}
	}

	public static String getSign(String params) {
		SortedMap<String, Object> map = new TreeMap<String, Object>();
		map = JSONObject.parseObject(params, map.getClass());
		StringBuffer stringBuffer = new StringBuffer(MD5_KEY);
		for (Map.Entry<String, Object> m : map.entrySet()) {
			Object value = m.getValue();
			if (StringUtil.isNotEmpty(value)) {
				stringBuffer.append(m.getKey()).append("=").append(value).append("&");
			}
		}
		String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
//		String token = "930a1076-5e03-4509-baf7-52b56c2d15d3";
		String signString = stringBuffer.append(token).toString();
		return MD5Util.MD5(signString);
	}

	public static void main(String[] args) {
		String params = "{\"accountId\":502,\"totalNumber\":10,\"totalMoney\":100,\"title\":\"恭喜发财，大吉大利\\\"\",\"stationId\":23}";
		verifySign(params, "a372eeede89a541db69a707be791e321");
	}

}
