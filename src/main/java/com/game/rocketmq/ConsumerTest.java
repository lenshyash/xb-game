package com.game.rocketmq;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConsumerTest {

    private static ApplicationContext context;

    public static void main(String[] args) throws Exception{
        context = new ClassPathXmlApplicationContext("classpath:applicationContext-consumer.xml");
        Consumer consumer = context.getBean(Consumer.class);

        Thread.sleep(20*1000);

        System.out.println("end");
        consumer.destroy();
    }

}