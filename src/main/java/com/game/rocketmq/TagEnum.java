package com.game.rocketmq;

public enum TagEnum {

	LOTTERY_INS("彩票记录新增"),
	LOTTERY_UPD("彩票记录修改"),
	HG_SPORT_INS("皇冠体育记录新增"),
	HG_SPORT_UPD("皇冠体育记录修改"),
	THIRD_SPORT_INS("三方体育记录新增"),
	THIRD_SPORT_UPD("三方体育记录修改"),
	EGAME_INS("电子记录新增"),
	EGAME_UPD("电子记录修改"),
	REAL_INS("真人记录新增"),
	REAL_UPD("真人记录修改"),
	MNY_MONEY_INS("帐变记录插入");

	private String desc;


	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	TagEnum(String desc) {
		this.desc = desc;
	}
}
