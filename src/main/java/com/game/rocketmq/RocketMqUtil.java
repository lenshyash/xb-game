package com.game.rocketmq;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.model.lottery.BcLotteryOrder;
import com.game.util.StationConfigUtil;
import org.apache.log4j.Logger;
import org.apache.rocketmq.common.message.Message;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RocketMqUtil {
    protected static Logger log = Logger.getLogger(RocketMqUtil.class);
    public static String DEFAULT_TOPIC = "xb_etl";
    public static Long[] CHESS_STATION_ID = {97L, 100L};
    public static String[] CHESS_STATION_ID_STR = {"97", "100"};
    public static String[] CHESS_STATION_FOLDER = {"b008_back","b02688"};
    public static String DEFAULT_SERVER_NAME="Cluster-004";

    public static void sendOrder(TagEnum tag, JSONArray array,Long stationId){
        try {
            if(DEFAULT_SERVER_NAME.equals(StationConfigUtil.getSys(StationConfig.sys_server_name))&& isChessStationId(stationId)) {
                if (array == null || array.size() == 0) {
                    return;
                }
                List<Message> list = new ArrayList<>();
                for (Object json : array
                ) {
                    Message msg = new Message(DEFAULT_TOPIC, JSONObject.toJSONBytes(json));
                    msg.setTags(tag.name());
                    msg.setKeys(System.currentTimeMillis() + "");
                    Producer.send(msg,stationId);
                }
            }
        }catch (Exception e) {
//            SysLogUtil.log("RocketMq投递消息失败,原因:"+e.getMessage(),array.toJSONString(), null, "system", null,stationId, LogType.AGENT_SYSTEM.getType(), SysLog.PLATFORM_PLATFORM,
//                    null);
        }
        return;
    }
    public static void sendOneOrder(TagEnum tag, byte[] json,Long stationId){
        try {
            if (DEFAULT_SERVER_NAME.equals(StationConfigUtil.getSys(StationConfig.sys_server_name)) && isChessStationId(stationId)) {
                Message msg = new Message(DEFAULT_TOPIC, json);
                msg.setTags(tag.name());
                msg.setKeys(System.currentTimeMillis() + "");
                Producer.send(msg,stationId);
            }
        }catch (Exception e) {
        }
        return;
    }

    public static void sendOpenLotOrder(Long stationId,String orderId, int orderStatus, Date now, Integer zhuShu, BigDecimal winMoney, BigDecimal curOdds) {
        try {
            if(DEFAULT_SERVER_NAME.equals(StationConfigUtil.getSys(StationConfig.sys_server_name))&& isChessStationId(stationId)){
                BcLotteryOrder order = new BcLotteryOrder();
                order.setOrderId(orderId);
                order.setStatus(orderStatus);
                if(zhuShu!=null){
                    order.setWinZhuShu(zhuShu);
                }
                if(winMoney!=null) {
                    order.setWinMoney(winMoney);
                }
                if(curOdds!=null) {
                    order.setOdds(curOdds);
                }
                if(now!=null) {
                    order.setOpenTime(now);
                }
                Message message = new Message();
                message.setTags(TagEnum.LOTTERY_UPD.name());
                message.setBody(JSONObject.toJSONBytes(order));
                message.setKeys(System.currentTimeMillis()+"");
                message.setTopic(DEFAULT_TOPIC);
                Producer.send(message,stationId);
                return;
            }
        }catch (Exception e) {
        }
        return;
    }
    
    public static Boolean isChessStationId(Long stationId) {
    	Boolean flag = false;
    	
    	for (Long sid : CHESS_STATION_ID) {
			if (sid==stationId) {
				flag = true;
				break;
			}
		}
    	return flag;
    }
    
    public static Boolean isChessStationId(String stationId) {
    	Boolean flag = false;
    	
    	List<String> list = Arrays.asList(CHESS_STATION_ID_STR);
    	for (String sid : list) {
			if (sid.equals(stationId)) {
				flag = true;
				break;
			}
		}
    	return flag;
    }
}
