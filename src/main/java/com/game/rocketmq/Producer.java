package com.game.rocketmq;

import com.game.constant.LogType;
import com.game.model.SysLog;
import com.game.util.SysLogUtil;
import com.github.rholder.retry.*;
import org.apache.log4j.Logger;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
// * 生产
 */
public class Producer {

    protected static Logger log = Logger.getLogger(Producer.class);

    private static String producerGroup;

    private static String namesrvAddr;

    private static String instanceName;

    private static DefaultMQProducer producer;

    public Producer() {

    }

    public DefaultMQProducer getProducer() {
        return producer;
    }

    public  Producer(String producerGroup,String namesrvAddr,String instanceName){
        Producer.producerGroup = producerGroup;
        Producer.namesrvAddr = namesrvAddr;
        Producer.instanceName = instanceName;
    }

    public void init() throws MQClientException {
        log.info("start init DefaultMQProducer...");
        producer = new DefaultMQProducer(producerGroup);
        producer.setNamesrvAddr(namesrvAddr);
        producer.setInstanceName(instanceName);
        producer.start();
        log.info("DefaultMQProducer init success.");
    }

    public void destroy(){
        log.info("start destroy DefaultMQProducer...");
        producer.shutdown();
        log.info("DefaultMQProducer destroy success.");
    }

    public static void send(final Message message,final Long stationId) throws ExecutionException, RetryException {
        log.info("RocketMq投递消息开始");
        final Retryer<SendResult> retryer = RetryerBuilder.<SendResult>newBuilder()
                // 当抛出异常时，就会进行重试
                .retryIfException()
                .withRetryListener(new RetryListener() {
                    @Override
                    public <V> void onRetry(Attempt<V> attempt) {
                        long retryNum = attempt.getAttemptNumber(); //重试次数
                        log.info("重试次数："+retryNum);
                        if (retryNum == 3 && (attempt.hasException() || ((SendResult) attempt.getResult()).getSendStatus() != SendStatus.SEND_OK)) {
                            //如果重试三次失败，则记录日志
                            SysLogUtil.log("RocketMq投递消息失败,原因:"+attempt.getExceptionCause().toString(),new String(message.getBody()), null, "system", null,stationId, LogType.AGENT_SYSTEM.getType(), SysLog.PLATFORM_PLATFORM,
                                    null);
                        }
                    }
                })
                // 最多重试3次
                .withStopStrategy(StopStrategies.stopAfterAttempt(3))
                //第一次重试间隔10秒，后面每次间隔时间是在前一个间隔上加5秒（就是3秒）
                .withWaitStrategy(WaitStrategies.incrementingWait(0, TimeUnit.SECONDS,2,TimeUnit.SECONDS))
                .build();
        retryer.call(buildTask(message));
    }
    private static Callable<SendResult> buildTask(final Message message) {
        return new Callable<SendResult>() {
            @Override
            public SendResult call() throws Exception {
                return producer.send(message);
//                throw new RuntimeException();
            }

        };
    }
    public static boolean send(List<Message> message) {
        int count = 0;
        boolean status = false;
        long timeout = 30000;
        log.debug("count:" + count);
        while(count <= 3){
            log.debug("进入循环");
            try {
                SendResult sendResult = producer.send(message, timeout);
                log.debug("发送MQ状态：" + sendResult.getSendStatus());
                if(sendResult.getSendStatus() == SendStatus.SEND_OK){
                    status = true;
                    log.info("投递"+message.size()+"条注单记录成功");
                    break;
                }
            } catch (InterruptedException e) {
                log.error("MQ发送消息：" + count + "次失败，请查看一次原因" + e.getMessage(), e);
            } catch (RemotingException e) {
                log.error("MQ发送消息：" + count + "次失败，请查看一次原因" + e.getMessage(), e);
            } catch (MQClientException e) {
                log.error("MQ发送消息：" + count + "次失败，请查看一次原因" + e.getMessage(), e);
            } catch (MQBrokerException e) {
                log.error("MQ发送消息：" + count + "次失败，请查看一次原因" + e.getMessage(), e);
            }finally {
                count++;
            }
        }
        return status;
    }


    // ---------------setter -----------------

    public void setProducerGroup(String producerGroup) {
        this.producerGroup = producerGroup;
    }

    public void setNamesrvAddr(String namesrvAddr) {
        this.namesrvAddr = namesrvAddr;
    }
}