package com.game.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by thinkpad on 2015/3/28.
 */
public class HttpUtils {
	public static String getAddr(HttpServletRequest request) {

		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getCurrentUrl(HttpServletRequest request){
		String contextPath = request.getContextPath();
		String scheme = request.getScheme();
		int serverPort = request.getServerPort();
		String servletPath = request.getServletPath();

		StringBuilder currentUrl = new StringBuilder();
		currentUrl.append(scheme + "://" + request.getServerName());
		if((("http".equals(scheme) && serverPort == 80) || ("https".equals(scheme) && serverPort == 443)) == false){
			currentUrl.append(":" + serverPort);
		}
		currentUrl.append(contextPath + servletPath);

		return currentUrl.toString();
	}

	public static String getDomain(HttpServletRequest request){
		String contextPath = request.getContextPath();
		String scheme = request.getScheme();
		int serverPort = request.getServerPort();

		StringBuilder currentUrl = new StringBuilder();
		currentUrl.append(scheme + "://" + request.getServerName());
		if((("http".equals(scheme) && serverPort == 80) || ("https".equals(scheme) && serverPort == 443)) == false){
			currentUrl.append(":" + serverPort);
		}
		currentUrl.append(contextPath);

		return currentUrl.toString();
	}
}
