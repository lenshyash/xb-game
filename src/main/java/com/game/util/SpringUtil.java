package com.game.util;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

public class SpringUtil {
	/**
	 * 从当前Spring容器中获取bean
	 * 
	 * @param beanId
	 * @return
	 */
	public static Object getBean(String beanId) {
		WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
		return wac.getBean(beanId);
	}

	public static <T> T getBean(Class<T> clazz) {
		WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
		return wac.getBean(clazz);
	}

	public static void publishEvent(Object event) {
		WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
		wac.publishEvent(event);
	}
}
