package com.game.util;

import org.jay.frame.util.StringUtil;

import com.game.model.MemberPermission;
import com.game.service.MemberPermissionService;
import com.game.service.impl.MemberPermissionServiceImpl;

public class MemberPermissionUtil {
	
	private static MemberPermissionService permissionService;
	
	private static void init(){
		if(permissionService == null){
			permissionService = SpringUtil.getBean(MemberPermissionServiceImpl.class);
		}
	}
	/**
	 * 判断是否具备转账功能
	 * @param mp
	 * @return
	 */
	public static boolean hasRealGameTransfer(MemberPermission mp){
		if(mp == null){ 
			return true;
		}
		if(StringUtil.equals(mp.getRealGameTransfer(), MemberPermission.COMMON_NO)){
			return false;
		}
		return true;
	}
	
	public static boolean hasRealGameTransfer(long memberId){
		init();
		MemberPermission mp = permissionService.getPermission(memberId);
		return hasRealGameTransfer(mp);
	}
}
