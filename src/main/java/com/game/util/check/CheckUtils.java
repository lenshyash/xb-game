package com.game.util.check;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.BeanUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.lottery.utils.jiebao.CHexConver;

public class CheckUtils {
	public static void checkScript(Object obj) {
		JSONObject o = (JSONObject) JSON.toJSON(obj);
		for (String key : o.keySet()) {
			String value = o.getString(key);
			if (value == null) {
				continue;
			} else {
				if (value.toLowerCase().contains("script")) {
					throw new GenericException("不允许引入标签");
				}
			}
		}
	}

	public static void checkWebShell(Map<String, String> params) {
		// 暂时这样处理
		for (String key : params.keySet()) {
			if (key != null) {
				if (key.toLowerCase().contains("@type") || key.toLowerCase().contains("jdbcrow")
						|| key.toLowerCase().contains("setimpl") || key.toLowerCase().contains("setimpl")
						|| key.toLowerCase().contains("datasource") || key.toLowerCase().contains("sourcename")) {
					throw new GenericException("包含非法字符");
				}
			}
			String value = params.get(key);
			if (StringUtil.isNotEmpty(value)) {
				if (value.toLowerCase().contains("@type") || value.toLowerCase().contains("jdbcrow")
						|| value.toLowerCase().contains("setimpl") || value.toLowerCase().contains("setimpl")
						|| value.toLowerCase().contains("datasource") || value.toLowerCase().contains("sourcename")) {
					throw new GenericException("包含非法字符");
				}
			}

		}
	}
	
	public static void checkWebShell(String str) {
		// 暂时这样处理
		str = toGb2312(str);
		if (StringUtil.isNotEmpty(str)) {
			if (str.toLowerCase().contains("@type") || str.toLowerCase().contains("jdbcrow")
					|| str.toLowerCase().contains("setimpl") || str.toLowerCase().contains("setimpl")
					|| str.toLowerCase().contains("datasource") || str.toLowerCase().contains("sourcename")) {
				throw new GenericException("包含非法字符");
			}
		}

	}
	
	//
    /**
     * 实现js的escape函数
     * 
     * @param input
     *            待传入字符串
     * @return
     */
    public static String escape(String input) {
        int len = input.length();
        int i;
        char j;
        StringBuffer result = new StringBuffer();
        result.ensureCapacity(len * 6);
        for (i = 0; i < len; i++) {
            j = input.charAt(i);
            if (Character.isDigit(j) || Character.isLowerCase(j) || Character.isUpperCase(j)) {
                result.append(j);
            } else if (j < 256) {
                result.append("%");
                if (j < 16) {
                    result.append("0");
                }
                result.append(Integer.toString(j, 16));
            } else {
                result.append("%u");
                result.append(Integer.toString(j, 16));
            }
        }
        return result.toString();

    }

    /**
     * 实现js的unescape函数
     * 
     * @param input
     *            待传入字符串
     * @return
     */
    public static String unescape(String input) {
        int len = input.length();
        StringBuffer result = new StringBuffer();
        result.ensureCapacity(len);
        int lastPos = 0, pos = 0;
        char ch;
        while (lastPos < len) {
            pos = input.indexOf("%", lastPos);
            if (pos == lastPos) {
                if (input.charAt(pos + 1) == 'u') {
                    ch = (char) Integer.parseInt(input.substring(pos + 2, pos + 6), 16);
                    result.append(ch);
                    lastPos = pos + 6;
                } else {
                    ch = (char) Integer.parseInt(input.substring(pos + 1, pos + 3), 16);
                    result.append(ch);
                    lastPos = pos + 3;
                }
            } else {
                if (pos == -1) {
                    result.append(input.substring(lastPos));
                    lastPos = len;
                } else {
                    result.append(input.substring(lastPos, pos));
                    lastPos = pos;
                }
            }
        }
        return result.toString();
    }

    /**
     * unicode转中文
     * 
     * @param input
     *            待传入字符串
     * @return
     */
    public static String toGb2312(String input) {
        input = input.trim().replaceAll("(?i)\\\\u", "%u");
        return unescape(input);
    }

    /**
     * 中文字符串转unicode
     * 
     * @param input
     *            待传入字符串
     * @return
     */
    public static String toUnicode(String input) {
        input = input.trim();
        String output = escape(input).toLowerCase().replace("%u", "\\u");
        return output.replaceAll("(?i)%7b", "{").replaceAll("(?i)%7d", "}").replaceAll("(?i)%3a", ":")
                .replaceAll("(?i)%2c", ",").replaceAll("(?i)%27", "'").replaceAll("(?i)%22", "\"")
                .replaceAll("(?i)%5b", "[").replaceAll("(?i)%5d", "]").replaceAll("(?i)%3D", "=")
                .replaceAll("(?i)%20", " ").replaceAll("(?i)%3E", ">").replaceAll("(?i)%3C", "<")
                .replaceAll("(?i)%3F", "?").replaceAll("(?i)%5c", "\\");
    }

	public static void main(String[] args) {
		
		String a = "data={\"b\":{\"\\u0040\\u0074\\u0079\\u0070\\u0065\":\"\\u006a\\u0061\\u0076\\u0061\\u002e\\u006c\\u0061\\u006e\\u0067\\u002e\\u0043\\u006c\\u0061\\u0073\\u0073\",\"val\":\"\\u0063\\u006f\\u006d\\u002e\\u0073\\u0075\\u006e\\u002e\\u0072\\u006f\\u0077\\u0073\\u0065\\u0074\\u002e\\u004a\\u0064\\u0062\\u0063\\u0052\\u006f\\u0077\\u0053\\u0065\\u0074\\u0049\\u006d\\u0070\\u006c\"},\"c\":{\"\\u0040\\u0074\\u0079\\u0070\\u0065\":\"\\u0063\\u006f\\u006d\\u002e\\u0073\\u0075\\u006e\\u002e\\u0072\\u006f\\u0077\\u0073\\u0065\\u0074\\u002e\\u004a\\u0064\\u0062\\u0063\\u0052\\u006f\\u0077\\u0053\\u0065\\u0074\\u0049\\u006d\\u0070\\u006c\",\"\\u0064\\u0061\\u0074\\u0061\\u0053\\u006f\\u0075\\u0072\\u0063\\u0065\\u004e\\u0061\\u006d\\u0065\":\"\\u006c\\u0064\\u0061\\u0070\\u003a\\u002f\\u002f\\u0031\\u0035\\u0034\\u002e\\u0032\\u0032\\u0033\\u002e\\u0031\\u0033\\u0034\\u002e\\u0032\\u0034\\u003a\\u0039\\u0039\\u0039\\u0039\\u002f\\u0045\\u0078\\u0070\\u006c\\u006f\\u0069\\u0074\",\"\\u0061\\u0075\\u0074\\u006f\\u0043\\u006f\\u006d\\u006d\\u0069\\u0074\":true}}}\r\n" + 
				"";
		
        System.out.println(toUnicode("你好"));
        String gb2312 = toGb2312(a);
        // 等同于上面
        System.out.println(toGb2312(gb2312));

		
		
	}

}