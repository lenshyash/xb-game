package com.game.util;

public interface LotteryUtils {
	/**
	 * 第三版首页导航navClass
	 */
	public static final String LOTTERY_V3_NAV_INDEX = "INDEX";	//首页
	public static final String LOTTERY_V3_NAV_GCDT = "GCDT";	//够彩大厅
	public static final String LOTTERY_V3_NAV_KJGG = "KJGG";	//开奖公告
	public static final String LOTTERY_V3_NAV_ZST = "ZST";	//走势图
	public static final String LOTTERY_V3_NAV_HMZX = "HMZX";	//合买中心
	public static final String LOTTERY_V3_NAV_ZX = "ZX";	//资讯
	public static final String LOTTERY_V3_NAV_YHHD = "YHHD";	//优惠活动
	public static final String LOTTERY_V3_NAV_SJGC = "SJGC";	//手机够彩
	public static final String LOTTERY_V3_NAV_WIN_ORDER = "ZJJL";	//中奖纪录
	public static final String LOTTERY_V3_NAV_TOUZHU_ORDER = "TZJL";	//投注记录
	public static final String LOTTERY_V3_NAV_DAILI_JIAMENG = "DLJM";	//代理加盟
	public static final String LOTTERY_V3_NAV_WFGZ = "WFGZ";	//玩法规则
	public static final String LOTTERY_V3_NAV_NULL = "";	//空值
	public static final String LOTTERY_V3_NAV_SPORT = "SPORT";	//体育
	public static final String LOTTERY_V3_NAV_REAL = "REAL";	//真人
	public static final String LOTTERY_V3_NAV_EGAME = "EGAME";	//电子
	public static final String LOTTERY_V3_NAV_CHESS = "CHESS";	//棋牌
	public static final String LOTTERY_V3_NAV_XLJC = "XLJC";	//检测
	
	
	public static final String LOTTERY_V4_NAV_DATING = "DATING";
	public static final String LOTTERY_V4_NAV_QIANBAO = "QIANBAO";
	public static final String LOTTERY_V4_NAV_WODE = "WODE";
	public static final String LOTTERY_V4_NAV_DONGTAI = "DONGTAI";
	
	
	/**
	 * 机器人播报状态
	 */
	public static final int ROBOT_KAIPAN = 1001;	//开盘
	public static final int ROBOT_FENGPAN = 1002;	//封盘
	public static final int ROBOT_USERGOIN = 1003;	//会员进入或者游客进入
	public static final int ROBOT_ERROR_MSG = 1004;	//错误信息提示
	public static final int ROBOT_BET_ORDER = 1005;	//下注
	public static final int ROBOT_CHAT = 1006;	//聊天状态
	public static final int ROBOT_CHAT_SYSTEM = 1007;	//系统播报信息
	public static final int ROBOT_BET_ORDER_SYSTEM = 1008;	//机器人下注
	public static final int ROBOT_HISTORY_ORDER = 1009;		//获取当前期的所有缓存记录
	public static final int ROBOT_CLEAR_HISTORY_ORDER = 1010;	//清空当前的缓存历史
	public static final int ROBOT_FEI_FA_ERROR = 1011;	//非法异常连接，直接关闭
	public static final int ROBOT_TI_REN_STATUS = 1012;	//3分钟没有任何操作，退出到首页
}
