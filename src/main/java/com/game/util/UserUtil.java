package com.game.util;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;

import com.game.core.CustomThreadData;
import com.game.core.SortInfo;
import com.game.core.SystemConfig;
import com.game.model.AdminUser;
import com.game.model.MemberLevelBase;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountService;

public class UserUtil extends SysUtil {
	private static Logger logger = Logger.getLogger(UserUtil.class);
	
	private static MnyMoneyService mnyMoneyService;
	
	public static boolean isSuperAdmin() {
		if (!StationUtil.isAdminStation()) {
			return false;
		}
		User user = getCurrentUser();
		if (user == null) {
			return false;
		}
		return "root".equals(user.getLoginAccount());
	}

	public static boolean isSuperAgent() {
		if (!StationUtil.isAgentStation()) {
			return false;
		}
		User user = getCurrentUser();
		SysStation station = StationUtil.getStation();
		if (user == null) {
			return false;
		}
		if (station == null) {
			return false;
		}
		return StringUtil.equals(station.getAccountId(), user.getSysUserId());
	}

	public static Long getLevel() {
		if (StationUtil.isAdminStation()) {
			return null;
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return null;
		}

		return user.getLevel();
	}
	
	public static Long getLevelGroup(){
		if(StationUtil.isAdminStation()){
			return null;
		}
		SysAccount  user = (SysAccount) getCurrentUser();
		if(user == null){
			return null;
		}
		return user.getLevelGroup();
	}
	
	public static String getLevelGroupName(){
		if(StationUtil.isAdminStation()){
			return "";
		}
		SysAccount  user = (SysAccount) getCurrentUser();
		if(user == null){
			return "";
		}
		MemberLevelBase level = MemberLevelUtil.getLevel(user.getLevelGroup());
		if(level == null){
			return "";
		}
		return level.getLevelName();
	}
	public static MemberLevelBase getMemberLevel(){
		if(StationUtil.isAdminStation()){
			return null;
		}
		SysAccount  user = (SysAccount) getCurrentUser();
		if(user == null){
			return null;
		}
		MemberLevelBase level = MemberLevelUtil.getLevel(user.getLevelGroup());
		if(level == null){
			return null;
		}
		return level;
	}
	public static Long getGroupId() {
		if (StationUtil.isAdminStation()) {
			return null;
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return null;
		}

		return user.getGroupId();
	}

	public static BigDecimal getBetNum() {
		if (StationUtil.isAdminStation()) {
			return null;
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return null;
		}

		return user.getBetNum();
	}

	public static Long getType() {
		if (StationUtil.isAdminStation()) {
			return null;
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return null;
		}

		return user.getAccountType();
	}

	public static Long getAdminGroupId() {
		if (!StationUtil.isAdminStation()) {
			return null;
		}

		AdminUser user = (AdminUser) getCurrentUser();
		if (user == null) {
			return null;
		}

		return user.getGroupId();
	}

	public static String getUserAccount() {

		User user = (User) getCurrentUser();
		if (user == null) {
			return null;
		}
		return user.getLoginAccount();
	}

	/**
	 * 获取用户的上级
	 * 
	 * @return
	 */
	public static String getParents() {

		if (StationUtil.isAdminStation()) {
			return "";
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return "";
		}
		return user.getParents();
	}

	/**
	 * 获取自身所有的下级成员的上级ID
	 * 
	 * @return
	 */
	public static String getChildren() {

		if (StationUtil.isAdminStation()) {
			return "";
		}

		SysAccount user = (SysAccount) getCurrentUser();
		if (user == null) {
			return "";
		}
		String parents = getParents();
		if (StringUtil.isEmpty(parents)) {
			parents = ",";
		}
		return parents + user.getId() + ",";
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getChildren(SysAccount account) {

		if (account == null) {
			return "";
		}

		String parents = account.getParents();
		if (StringUtil.isEmpty(parents)) {
			parents = ",";
		}
		return parents + account.getId() + ",";
	}
	
	/**
	 * 获取自身所有的下级成员的上级ID
	 * 
	 * @return
	 */
	public static String getChildrenName(SysAccount account) {

		if (account == null) {
			return "";
		}

		String parentNames = account.getParentNames();
		if (StringUtil.isEmpty(parentNames)) {
			parentNames = ",";
		}
		return parentNames + account.getAccount() + ",";
	}

	/**
	 * 获取用户真实IP地址，不使用request.getRemoteAddr();的原因是有可能用户使用了代理软件方式避免真实IP地址,
	 * 
	 * 可是，如果通过了多级反向代理的话，X-Forwarded-For的值并不止一个，而是一串IP值，究竟哪个才是真正的用户端的真实IP呢？
	 * 答案是取X-Forwarded-For中第一个非unknown的有效IP字符串。
	 * 
	 * 如：X-Forwarded-For：192.168.1.110, 192.168.1.120, 192.168.1.130,
	 * 192.168.1.100
	 * 
	 * 用户真实IP为： 192.168.1.110
	 * 
	 * @return
	 */
	public static String getIpAddress() {
		//开发环境IP固定返回
		if(SystemConfig.SYS_DEVELOP_IP) {
			return "127.0.0.1";
		}
		HttpServletRequest request = getRequest();
		// StringBuilder sb=new StringBuilder();
		// sb.append("X-Real-IP=").append(request.getHeader("X-Real-IP"));
		// sb.append("
		// X-Forwarded-For=").append(request.getHeader("X-Forwarded-For"));
		// sb.append("
		// x-forwarded-for=").append(request.getHeader("x-forwarded-for"));
		// sb.append(" Cdn-Src-Ip=").append(request.getHeader("Cdn-Src-Ip"));
		// sb.append("
		// Proxy-Client-IP=").append(request.getHeader("Proxy-Client-IP"));
		// sb.append("
		// WL-Proxy-Client-IP=").append(request.getHeader("WL-Proxy-Client-IP"));
		// sb.append("
		// HTTP_CLIENT_IP=").append(request.getHeader("HTTP_CLIENT_IP"));
		// sb.append("
		// HTTP_X_FORWARDED_FOR=").append(request.getHeader("HTTP_X_FORWARDED_FOR"));
		// sb.append("
		// HTTP_X_FORWARDED_FOR=").append(request.getHeader("HTTP_X_FORWARDED_FOR"));
		// sb.append("").append(request.getHeader("X-Real-IP"));
		// org.apache.log4j.Logger.getLogger(UserUtil.class).error(sb.toString());

		String ipAddress = request.getHeader("X-Forwarded-For");
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("x-forwarded-for");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("X-Real-IP");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Cdn-Src-Ip");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		
		if (StringUtils.isEmpty(ipAddress) || "unknown".equalsIgnoreCase(ipAddress)) {
			ipAddress = request.getRemoteAddr();
			if (StringUtils.equals(ipAddress, "127.0.0.1") || StringUtils.equals(ipAddress, "[::1]") || StringUtils.equals(ipAddress, "0:0:0:0:0:0:0:1")) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
					if (inet != null) {
						ipAddress = inet.getHostAddress();
					}
				} catch (Exception e) {
				}
			}
		}
		// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
		if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
															// = 15
			if (ipAddress.indexOf(",") > 0) {
				String [] ips = ipAddress.split(",");
				for(String ip:ips){
					ip=StringUtils.trim(ip);
					if(StringUtils.isEmpty(ip)||ip.indexOf("<")>-1){
						continue;
					}
					ipAddress=ip;
					break;
				}
			}
		}
		if(ipAddress.indexOf("<")>-1){
			return "";
		}
		return ipAddress;
		// String ip = request.getHeader("X-Real-IP");
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getHeader("x-forwarded-for");
		// }
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getHeader("Proxy-Client-IP");
		// }
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getHeader("WL-Proxy-Client-IP");
		// }
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getHeader("HTTP_CLIENT_IP");
		// }
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		// }
		// if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
		// {
		// ip = request.getRemoteAddr();
		// }
		// return ip;
	}

	public static void logIpAddress() {
		HttpServletRequest request = getRequest();
		logger.info("X-Forwarded-For  ip=" + request.getHeader("X-Forwarded-For"));
		logger.info("x-forwarded-for  ip=" + request.getHeader("x-forwarded-for"));
		logger.info("X-Real-IP  ip=" + request.getHeader("X-Real-IP"));
		logger.info("Cdn-Src-Ip ip=" + request.getHeader("Cdn-Src-Ip"));
		logger.info("Proxy-Client-IP  ip=" + request.getHeader("Proxy-Client-IP"));
		logger.info("WL-Proxy-Client-IP  ip=" + request.getHeader("WL-Proxy-Client-IP"));
		logger.info("HTTP_CLIENT_IP  ip=" + request.getHeader("HTTP_CLIENT_IP"));
		logger.info("HTTP_X_FORWARDED_FOR  ip=" + request.getHeader("HTTP_X_FORWARDED_FOR"));
		logger.info("RemoteAddr  ip=" + request.getRemoteAddr());
	}

	public static SortInfo getSortInfo() {
		CustomThreadData data = (CustomThreadData) getCustomData();
		if (data == null) {
			return null;
		}
		return data.getSortInfo();
	}

	public static void setSortInfo(String column, String sortType) {
		SortInfo si = new SortInfo(column, sortType);
		CustomThreadData data = (CustomThreadData) getCustomData();
		if (data == null) {
			return;
		}
		data.setSortInfo(si);
	}
	
	public static Date getCreateDatetime() {
		if (StationUtil.isAdminStation()) {
			return null;
		}

		SysAccount user = (SysAccount) getCurrentUser();
		return user.getCreateDatetime();
	}
	
	public static SysAccountInfo getCurrentInfo() {
		CustomThreadData data = (CustomThreadData) getCustomData();
		if (data == null) {
			return null;
		}
		return data.getAccountInfo();
	}
	
	public static BigDecimal getRate() {
		Long accountId = getUserId();
		if(accountId == null) {
			return null;
		}
		MnyMoney money = getMoneyService().getMoneyById(accountId);
		if(money == null) {
			return null;
		}
		return money.getRate();
	}
	private static MnyMoneyService getMoneyService() {
		if (mnyMoneyService == null) {
			mnyMoneyService = (MnyMoneyService) SpringUtil.getBean("mnyMoneyServiceImpl");
		}
		return mnyMoneyService;
	}
	
	/**
	 * 判断一个用户是不是引导账号
	 * @return
	 */
	public static Boolean isGuestAccount() {
		Boolean flag = false;
		
		//没登录直接false
		Long userId = UserUtil.getUserId();
		if(StringUtil.isEmpty(userId)) {
			return flag;
		}
		
		SysAccountService accountService = SpringUtil.getBean(SysAccountService.class);
		Map aMap = accountService.getAccountById(userId, StationUtil.getStationId());
		Long accountType = Long.parseLong(aMap.get("accountType").toString());
		Long reportType = Long.parseLong(aMap.get("reportType").toString());
		
		if(accountType.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE) || reportType.equals(SysAccount.REPORT_TYPE_GUIDE)) {
			flag = true;
		}
		
		return flag;
	}
}
