package com.game.util;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.util.SysUtil;

import com.game.constant.StationConfig;

public class StationOnOffUtil {

	public static void initConfig() {
		HttpServletRequest request = SysUtil.getRequest();
		Long stationId = StationUtil.getStationId();
		request.setAttribute("testAccount",
				StationConfigUtil.get(stationId, StationConfig.on_off_register_test_guest_station));
		request.setAttribute("copyright", StationConfigUtil.get(stationId, StationConfig.basic_info_website_copyright));
		request.setAttribute("isReg", StationConfigUtil.get(stationId, StationConfig.onoff_register));
		request.setAttribute("isExChgOnOff", StationConfigUtil.get(stationId, StationConfig.exchange_score));
		request.setAttribute("isChangeMoney", StationConfigUtil.get(stationId, StationConfig.onoff_change_money));
		request.setAttribute("isQdOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_sign_in));
		request.setAttribute("isZpOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_turnlate));
		request.setAttribute("logo",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_page_logo_url));
		request.setAttribute("isRedPacket", StationConfigUtil.get(stationId, StationConfig.onoff_member_red_packet));
		request.setAttribute("chatroom", StationConfigUtil.get(stationId, StationConfig.basic_info_customer_chatroom));
		String kfUrl = StationConfigUtil.get(stationId, StationConfig.online_customer_service_url);
		String appQRCodeLinkIos = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_ios);
		String appQRCodeLinkAndroid = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_android);
	/*	if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}*/
		request.setAttribute("kfUrl", kfUrl);
		request.setAttribute("appQRCodeLinkIos", appQRCodeLinkIos);
		request.setAttribute("appQRCodeLinkAndroid", appQRCodeLinkAndroid);
		request.setAttribute("appDownloadLinkIos",StationConfigUtil.get(stationId, StationConfig.app_download_link_ios));
		request.setAttribute("appDownloadLinkAndroid",StationConfigUtil.get(stationId, StationConfig.app_download_link_android));
		request.setAttribute("isBgemOnOff",StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem));
	}
	
	
	public static void initBJSCSetting() {
		HttpServletRequest request = SysUtil.getRequest();
		String sel11 = StationConfigUtil.get(StationConfig.onoff_bjsc_11_select);
		String dx11Setting = "，";
		String ds11Setting = "，";
		if("draw".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为和";
			ds11Setting += "11为和";
		}else if("xiaodan".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为小";
			ds11Setting += "11为单";
		}else if("dadan".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为大";
			ds11Setting += "11为单";
		}else if("hedan".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为和";
			ds11Setting += "11为单";
		}else if("dahe".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为大";
			ds11Setting += "11为和";
		}else if("xiaohe".equalsIgnoreCase(sel11)) {
			dx11Setting += "11为小";
			ds11Setting += "11为和";
		}else {
			dx11Setting = "";
			ds11Setting = "";
		}
		boolean show = !"on".equals(StationConfigUtil.get(StationConfig.onoff_pcegg_1314_select));
		request.setAttribute("pceggDrawShow", show);
		boolean onoff_k3_baozi_select = !"off".equals(StationConfigUtil.get(StationUtil.getStationId(),StationConfig.onoff_k3_baozi_select));
		request.setAttribute("k3baoziDrawShow", onoff_k3_baozi_select);
		request.setAttribute("dx11Setting", dx11Setting);
		request.setAttribute("ds11Setting", ds11Setting);
	}
}
