package com.game.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.SysStation;
import com.game.service.SysStationDomainService;
import com.game.service.SysStationService;
import com.game.service.impl.AgentBaseConfigServiceImpl;
import com.game.util.SpringUtil;
import com.game.util.StationConfigUtil;

public class AuthTokenUtil {
	
	private static final Object LOCK = new Object();
	
//	private static final String CHAT_URL = "https://chat.xbxb555.com";
//	private static final String CHAT_URL = "http://192.168.31.53:8080/chat";
	
	public static String getToken(){
		String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		if(StringUtil.isNotEmpty(token)){
			return token;
		}
		
		final String serverName = StationConfigUtil.getSys(StationConfig.sys_server_name);
		if(StringUtil.isEmpty(serverName)){
			throw new GenericException("总控设置[sys_server_name]不能为空");
		}
		final String url = StationConfigUtil.getSys(StationConfig.sys_server_url);
		if(StringUtil.isEmpty(url)){
			throw new GenericException("总控设置[sys_server_url]不能为空");
		}
		
		String realcenterUrl = StationConfigUtil.getSys(StationConfig.sys_real_center_url);
		if(StringUtil.isEmpty(realcenterUrl)){
			throw new GenericException("总控设置[sys_real_center_url]不能为空");
		}
		
		synchronized (LOCK) {
			token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
			if(StringUtil.isNotEmpty(token)){
				return token;
			}
			
			final String new_token = UUID.randomUUID().toString();  
			try{
				String content = new RequestProxy() {
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> ps = new ArrayList<>();
						ps.add(new BasicNameValuePair("auth_token", new_token));
						ps.add(new BasicNameValuePair("serverName", serverName));
						ps.add(new BasicNameValuePair("url", url));
						return ps;
					};
				}.doRequest(realcenterUrl+"/api/system/createAuthToken.do", PostType.POST, true);
				JSONObject json = JSONObject.parseObject(content);
				if(json.getBooleanValue("success")){
					SpringUtil.getBean(AgentBaseConfigServiceImpl.class).updateSysConfigValue(StationConfig.sys_real_center_token, new_token);
					return new_token;
				}
				String msg = json.getString("msg");
				if(StringUtil.isNotEmpty(msg)){
					throw new GenericException("创建token失败，原因："+msg);
				}else{
					throw new GenericException("创建token失败");
				}
			}catch(GenericException e){
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				throw new GenericException("创建token失败");
			}
		
		}
	}
	
	public static Map<String,String> getChatToken(){
		final String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		final String serverName = StationConfigUtil.getSys(StationConfig.sys_server_name);
		if(StringUtil.isEmpty(serverName)){
			throw new GenericException("总控设置[sys_server_name]不能为空");
		}
		final String url = StationConfigUtil.getSys(StationConfig.sys_server_url);
		if(StringUtil.isEmpty(url)){
			throw new GenericException("总控设置[sys_server_url]不能为空");
		}
		String charUrl = StationConfigUtil.getSys(StationConfig.sys_chat_url);
		if(StringUtil.isEmpty(charUrl)){
			throw new GenericException("总控设置[sys_chat_url]不能为空");
		}
		String onflag =StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_chat_multiple_template);
		String folder = StationUtil.getFolder();
		if(onflag!=null && "on".equals(onflag)){
			folder = "domain".equals(StationConfigUtil.get(StationConfig.station_folder_source))?StationUtil.getDomainFolder():StationUtil.getFolder();
		}
		final String relfolder = folder;
		final String stationId = StationUtil.getStationId()+"";
		final String stationName = StationUtil.getStationName();
		synchronized (LOCK) {
			try{
				String content = new RequestProxy() {
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> ps = new ArrayList<>();
						ps.add(new BasicNameValuePair("auth_token", token));
						ps.add(new BasicNameValuePair("serverName", serverName));
						ps.add(new BasicNameValuePair("url", url));
						ps.add(new BasicNameValuePair("folder", relfolder));
						ps.add(new BasicNameValuePair("stationId", stationId));
						ps.add(new BasicNameValuePair("stationName", stationName));
						return ps;
					};
				}.doRequest(charUrl+"/api/system/createAuthToken?", PostType.POST, true);
				JSONObject json = JSONObject.parseObject(content);
				if(json.getInteger("code") ==1 && json!=null){
					 JSONObject data = json.getJSONObject("data");
					 String newToken = data.getString("platformId")+","+data.getString("token");
					 if(StringUtils.isNoneBlank(newToken)){
						 Map<String,String> map = new HashMap<String, String>();
						 map.put("newToken", newToken);
						 map.put("domain", data.getString("domain"));
						 return map;
//						 return newToken;
					 }
				}
				String msg = json.getString("msg");
				if(StringUtil.isNotEmpty(msg)){
					throw new GenericException("创建token失败，原因："+msg);
				}else{
					throw new GenericException("创建token失败");
				}
			}catch(GenericException e){
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				throw new GenericException("创建token失败");
			}
		}
	}
	
	public static Map<String,String> getChatToken(SysStation station,String platformNo,String domain){
		final String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		final String serverName = StationConfigUtil.getSys(StationConfig.sys_server_name);
		if(StringUtil.isEmpty(serverName)){
			throw new GenericException("总控设置[sys_server_name]不能为空");
		}
		final String url = StationConfigUtil.getSys(StationConfig.sys_server_url);
		if(StringUtil.isEmpty(url)){
			throw new GenericException("总控设置[sys_server_url]不能为空");
		}
		String charUrl = StationConfigUtil.getSys(StationConfig.sys_chat_url);
		if(StringUtil.isEmpty(charUrl)){
			throw new GenericException("总控设置[sys_chat_url]不能为空");
		}
	/*	if(StringUtil.isEmpty(folder)){
			folder =  station.getFloder();
		}*/
		final String relNo = platformNo;
		final String relStationId =station.getId()+"";
		final String stationName =station.getName();
		final String reldomain = domain;
		synchronized (LOCK) {
			try{
				String content = new RequestProxy() {
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> ps = new ArrayList<>();
						ps.add(new BasicNameValuePair("auth_token", token));
						ps.add(new BasicNameValuePair("serverName", serverName));
						ps.add(new BasicNameValuePair("url", url));
						ps.add(new BasicNameValuePair("folder", relNo));
						ps.add(new BasicNameValuePair("stationId", relStationId));
						ps.add(new BasicNameValuePair("stationName", stationName));
						ps.add(new BasicNameValuePair("domainUrl", reldomain));
						return ps;
					};
				}.doRequest(charUrl+"/api/system/createAuthToken?", PostType.POST, true);
				Map<String,String> map = new HashMap<String, String>();
				JSONObject json = JSONObject.parseObject(content);
				if(json.getInteger("code") ==1 && json!=null){
					 JSONObject data = json.getJSONObject("data");
					 map.put("platformId", data.getString("platformId"));
					 map.put("token", data.getString("token"));
					 map.put("platPass", data.getString("platPass"));
					 map.put("domain", data.getString("domain"));
					 return map;
				}
				String msg = json.getString("msg");
				if(StringUtil.isNotEmpty(msg)){
					throw new GenericException("创建聊天室失败，原因："+msg);
				}else{
					throw new GenericException("创建token失败");
				}
			}catch(GenericException e){
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				throw new GenericException("创建token失败");
			}
		}
	}
	/**
	 * 鉴定聊天室token，返回相关信息
	 * @param platToken
	 * @return
	 */
	public static Map<String,String> authChatToken(String platToken){
		String charUrl = StationConfigUtil.getSys(StationConfig.sys_chat_url);
		if(StringUtil.isEmpty(charUrl)){
			throw new GenericException("总控设置[sys_chat_url]不能为空");
		}
		if(StringUtil.isEmpty(platToken)){
			throw new GenericException("聊天室token不能为空");
		}
		final String relToken = platToken;
		synchronized (LOCK) {
			try{
				String content = new RequestProxy() {
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> ps = new ArrayList<>();
						ps.add(new BasicNameValuePair("platToken", relToken));
						return ps;
					};
				}.doRequest(charUrl+"/api/system/authToken?", PostType.POST, true,true);
				Map<String,String> map = new HashMap<String, String>();
				JSONObject json = JSONObject.parseObject(content);
				if(json.getInteger("code") ==1 && json!=null){
					 JSONObject data = json.getJSONObject("data");
					 map.put("platformId", data.getString("platformId"));
					 map.put("token", data.getString("token"));
					 map.put("domain", data.getString("domain"));
					 return map;
				}
				String msg = json.getString("msg");
				if(StringUtil.isNotEmpty(msg)){
					throw new GenericException("获取聊天室失败，原因："+msg);
				}else{
					throw new GenericException("获取聊天室失败");
				}
			}catch(GenericException e){
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				throw new GenericException("获取聊天室失败");
			}
		}
	}
	
	
	
	public static Map<String,String> createNewImChannel(SysStation station,String platformNo,String domain,Long chatNum){
		final String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		final String serverName = StationConfigUtil.getSys(StationConfig.sys_server_name);
		if(StringUtil.isEmpty(serverName)){
			throw new GenericException("总控设置[sys_server_name]不能为空");
		}
		final String url = StationConfigUtil.getSys(StationConfig.sys_server_url);
		if(StringUtil.isEmpty(url)){
			throw new GenericException("总控设置[sys_server_url]不能为空");
		}
		String charUrl = StationConfigUtil.getSys(StationConfig.sys_im_url);
		if(StringUtil.isEmpty(charUrl)){
			throw new GenericException("总控设置[sys_chat_url]不能为空");
		}
	/*	if(StringUtil.isEmpty(folder)){
			folder =  station.getFloder();
		}*/
		final String relNo = platformNo;
		final String relStationId =station.getId()+"";
		final String stationName =station.getName();
		final String reldomain = domain;
		final String realNum =chatNum+"";
		synchronized (LOCK) {
			try{
				String content = new RequestProxy() {
					public List<Header> getHeaders() {
						List<Header> headerList = new ArrayList<Header>();
						headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
						headerList.add(new BasicHeader("Connection", "close")); // 短链接
						return headerList;
					};
					public List<NameValuePair> getParameters() {
						List<NameValuePair> ps = new ArrayList<>();
						ps.add(new BasicNameValuePair("auth_token", token));
						ps.add(new BasicNameValuePair("serverName", serverName));
						ps.add(new BasicNameValuePair("url", url));
						ps.add(new BasicNameValuePair("folder", relNo));
						ps.add(new BasicNameValuePair("stationId", relStationId));
						ps.add(new BasicNameValuePair("stationName", stationName));
						ps.add(new BasicNameValuePair("domainUrl", reldomain));
						ps.add(new BasicNameValuePair("chatNum", realNum));
						return ps;
					};
				}.doRequest(charUrl+"/api/system/createAuthToken?", PostType.POST, true);
				Map<String,String> map = new HashMap<String, String>();
				JSONObject json = JSONObject.parseObject(content);
				if(json.getInteger("code") ==1 && json!=null){
					 JSONObject data = json.getJSONObject("data");
					 map.put("channelId", data.getString("channelId"));
					 map.put("account", data.getString("account"));
					 map.put("password", data.getString("password"));
					 map.put("domain", data.getString("domain"));
					 return map;
				}
				String msg = json.getString("msg");
				if(StringUtil.isNotEmpty(msg)){
					throw new GenericException("创建渠道失败，原因："+msg);
				}else{
					throw new GenericException("创建渠道失败");
				}
			}catch(GenericException e){
				throw e;
			}catch(Exception e){
				e.printStackTrace();
				throw new GenericException("创建渠道失败");
			}
		}
	}
}
