package com.game.util;

import javax.servlet.http.HttpServletRequest;

public class ErrorUtil {
	
	static public HttpServletRequest error(Integer code,HttpServletRequest request) {
		String msg = "";
		if(code == null){
			code = 404;
			msg = "页面不存在!";
		}
		switch (code) {
		case 404:
			code = 404;
			msg = "无法找到该页面!";
			break;
		case 500:
			code = 500;
			msg = "访问出错!";
			break;
		case 403:
			String ip = UserUtil.getIpAddress();
			String country = IPSeeker.getInstance().getCountry(ip);
			code = 403;
			msg = "您所在地ip : "+ip+"<br/>地址："+country+"已经访问受限<br/>请联系客服！";
			break;
		case 408:
			code = 408;
			msg = "当前页面请求繁忙，请稍后再试";
			break;
		default:
			code = 404;
			msg = "页面不存在!";
			break;
		}
		request.setAttribute("code", code);
		request.setAttribute("msg", msg);
		request.setAttribute("base", request.getContextPath());
		return request;
	}
}
