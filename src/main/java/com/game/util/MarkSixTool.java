package com.game.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 计算生肖以及岁数
 * 
 * @author A04
 *
 */
public class MarkSixTool {
	public static void main(String[] args) {
		Map<String, String> aa = MarkSixTool.getZodiacsToHaoMaMap();
		for (String i : aa.keySet()) {
			System.out.println("i="+i+"  = "+aa.get(i));
		}
		//System.out.println(aa);
		//System.out.println(MarkSixTool.contain(ZODIACS, "虎"));
		System.out.println(MarkSixTool.getYear());
	}

	/** 生肖 */
	public static final String[] ZODIACS = new String[] { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" };
	// 红波
	public static final int[] COLOR_RED = new int[] { 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 };
	// 蓝波
	public static final int[] COLOR_BLUE = new int[] { 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 };
	// 绿波
	public static final int[] COLOR_GREEN = new int[] { 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 };

	/**
	 * 获取今年的生肖所对应的号码
	 * 
	 * @return
	 */
	public static Map<String, String> getZodiacsToHaoMaMap() {
		return getZodiacsToHaoMaMap(true);
	}
	
	/**
	 * 获取今年的生肖所对应的号码
	 * 
	 * @return
	 */
	public static Map<String, String> getZodiacsToHaoMaMap(boolean normal) {
		Map<String, String> animalMap = new HashMap<String, String>();
		Calendar c = Calendar.getInstance();
		int year = LunarCalendarUtil.getLunarYear(c.getTime());
//		int year = LunarCalendarUtil.getLunarYear(c.getTime());
		for (int i = 0; i < 12; i++) {
			animalMap.put(ZODIACS[subtractYear(year + i) % 12], getHaoMaStrByAge(i + 1,normal));
		}
		return animalMap;
	}
	
	/**
	 * 获取生肖本命年 与之BcMarkSixChineseZodiac对应
	 * @return
	 */
	public static String getYear(){
		int start = 1900;
		Calendar c = Calendar.getInstance();
		int year = LunarCalendarUtil.getLunarYear(new Date());
//		int year=2017;
		String[] years = new String[]{"shu","niu","hu","tu","lonG","she","ma","yang","hou","ji","gou","zhu"};
		return years[(year-start)%years.length];
		
	}

	public static Map<Integer, String> getHaoMaToZodiacsMap() {
		Map<Integer, String> map = new HashMap<>();
		Calendar c = Calendar.getInstance();
		int year = LunarCalendarUtil.getLunarYear(c.getTime());
		String sx = null;
		int[] hms = null;
		for (int i = 0; i < 12; i++) {
			hms = getHaoMaByAge(i + 1);
			if (hms != null) {
				sx = ZODIACS[subtractYear(year + i) % 12];
				for (int j : hms) {
					map.put(j, sx);
				}
			}
		}
		return map;
	}

	public static boolean contain(String[] array, String str) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(str)) {
				return true;
			}
		}
		return false;
	}

	public static boolean contain(int[] array, int ii) {
		for (int i : array) {
			if (i == ii) {
				return true;
			}
		}
		return false;
	}

	// 蓝波
	public static boolean isBlue(int haoMa) {
		return contain(COLOR_BLUE, haoMa);
	}

	// 红波
	public static boolean isRed(int haoMa) {
		return contain(COLOR_RED, haoMa);
	}

	// 绿波
	public static boolean isGreen(int haoMa) {
		return contain(COLOR_GREEN, haoMa);
	}

	/** 获取当前年份与起始年之间的差值 **/
	public static int subtractYear(int year) {
		int jiaziYear = 1804;
		if (year < jiaziYear) {// 如果年份小于起始的甲子年(startYear = 1804),则起始甲子年往前偏移
			jiaziYear = jiaziYear - (60 + 60 * ((jiaziYear - year) / 60));// 60年一个周期
		}
		return year - jiaziYear;
	}

	public static int[] getHaoMaByAge(int age) {
		if (age == 12)
			return new int[] { 2, 14, 26, 38 };
		if (age == 11)
			return new int[] { 3, 15, 27, 39 };
		if (age == 10)
			return new int[] { 4, 16, 28, 40 };
		if (age == 9)
			return new int[] { 5, 17, 29, 41 };
		if (age == 8)
			return new int[] { 6, 18, 30, 42 };
		if (age == 7)
			return new int[] { 7, 19, 31, 43 };
		if (age == 6)
			return new int[] { 8, 20, 32, 44 };
		if (age == 5)
			return new int[] { 9, 21, 33, 45 };
		if (age == 4)
			return new int[] { 10, 22, 34, 46 };
		if (age == 3)
			return new int[] { 11, 23, 35, 47 };
		if (age == 2)
			return new int[] { 12, 24, 36, 48 };
		if (age == 1)
			return new int[] { 1, 13, 25, 37, 49 };
		return null;
	}

	public static String getHaoMaStrByAge(int age) {
		return getHaoMaStrByAge(age,true);
	}
	
	public static String getHaoMaStrByAge(int age,boolean normal) {
		if(!normal && age == 1) 
			return "01,13,25,37";
		if (age == 12)
			return "02,14,26,38";
		if (age == 11)
			return "03,15,27,39";
		if (age == 10)
			return "04,16,28,40";
		if (age == 9)
			return "05,17,29,41";
		if (age == 8)
			return "06,18,30,42";
		if (age == 7)
			return "07,19,31,43";
		if (age == 6)
			return "08,20,32,44";
		if (age == 5)
			return "09,21,33,45";
		if (age == 4)
			return "10,22,34,46";
		if (age == 3)
			return "11,23,35,47";
		if (age == 2)
			return "12,24,36,48";
		if (age == 1)
			return "01,13,25,37,49";
		return null;
	}

	public static boolean isBenMingNian(int age) {
		return (age == 1 || age == 13 || age == 25 || age == 37 || age == 49);
	}
}
