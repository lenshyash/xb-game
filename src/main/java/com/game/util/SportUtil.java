package com.game.util;

import org.jay.frame.util.StringUtil;

import com.game.constant.StationConfig;

public class SportUtil {
    /**
     * 替换全角、半角 空格 tab
     * @param str
     * @return
     */
    public static String trim(String str){
        int len = str.length();
        int st = 0;
        char[] val = str.toCharArray();    /* avoid getfield opcode */

        while ((st < len) && ((val[st] <= ' ') ||  isWhitespace(val[st])) ) {
            st++;
        }
        while ((st < len) && ((val[len - 1] <= ' ') ||  isWhitespace(val[len - 1])) ) {
            len--;
        }
        return ((st > 0) || (len < val.length)) ? str.substring(st, len) : str;
    }
    
    public static boolean isWhitespace(char ch){  
        if(ch == '　' || ch == ' '){  
            return true;  
        }  
        if(Character.isWhitespace(ch)){  
            return true;  
        }  
        return false;  
    }  
    
    /**
     * 获取当前站点最低投注金额
     * @return
     */
	public static int getMinBettingMoney(){
		int min = StringUtil.toInt(StationConfigUtil.get(StationConfig.sports_min_bet_money));
		if(min < 1){
			return 10; 
		}
		return min;
	}
	
	/**
	 * 获取当前站点最高投注金额
	 * @return
	 */
	public static int getMaxBettingMoney(){
		int min = getMinBettingMoney();
		int max = StringUtil.toInt(StationConfigUtil.get(StationConfig.sports_max_bet_money));
		//如果最大限额小于 最小限额。 则最大限额不生效
		if(max <= min){
			return 500000;
		}
		return max;
	}
	
	/**
	 * 单场(单租)最大限额
	 * @return
	 */
	public static int getGameMaxBettingMoney(){
		int min = getMinBettingMoney();
		int max = StringUtil.toInt(StationConfigUtil.get(StationConfig.sports_game_max_bet_money));
		if(max <= min){
			return 500000;
		}
		return max;
	}
}
