package com.game.util;

import java.util.ArrayList;
import java.util.List;

public class ListPageUtil {
	public static List<List<Long>> split(List<Long> list, int pageSize) {
		int totalSize = list.size();
		int totalPage = (totalSize + pageSize - 1) / pageSize;
		List<List<Long>> result = new ArrayList<List<Long>>(totalPage);
		for (int i = 0, next = i * pageSize; i < totalPage; i++) {
			result.add(new ArrayList<Long>(list.subList(next, (i < totalPage - 1) ? (next = next + pageSize) : totalSize)));
		}
		return result;
	}
}
