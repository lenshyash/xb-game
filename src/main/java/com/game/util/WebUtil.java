package com.game.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jay.frame.util.SysUtil;

import com.game.constant.StationConfig;
import com.game.model.SysStation;
import com.game.service.FrameService;
import com.game.web.filter.XssHttpServletRequestWrapper;

public class WebUtil {

	/**
	 * 去除URL参数部分
	 * 	例如 url = admin/getUser.do?NativeSysConfigController=true
	 *         返回"a.com/admin/getUser.do"
	 *         
	 * @param url
	 * 			
	 */
	public static String toContentURL(String url) {
		int index = url.indexOf("?");
		if (index == -1) {
			return url;
		}
		return url.substring(0, index);
	}

	/**
	 * 转换成模块链接地址
	 * 例如 url = admin/permission/getUser.do?NativeSysConfigController=true
	 * 		   返回 admin/permission
	 * @param url
	 */
	public static String toModuleURL(String url) {
		url = toContentURL(url);
		int index = url.lastIndexOf("/");
		return url.substring(0, index);
	}

	/**
	 * 通过请求路径 获取主域名
	 * 
	 * @param requestUrl
	 * @return
	 */
	public static String getDomain(String requestUrl) {
		int index = requestUrl.indexOf("://");
		// 去掉http头
		if (index > -1) {
			requestUrl = requestUrl.substring(index + 3);
		}

		// 去掉www.
		if (requestUrl.indexOf("www.") == 0) {
			requestUrl = requestUrl.substring(4);
		}

		if ((index = requestUrl.indexOf("/")) > -1) {
			requestUrl = requestUrl.substring(0, index);
		}

		// 截掉端口号
		index = requestUrl.indexOf(":");
		if (index > -1) {
			requestUrl = requestUrl.substring(0, index);
		}
		return requestUrl;
	}

	private static FrameService frameService;

	private static FrameService getFrameService() {
		if (frameService == null) {
			frameService = (FrameService) SpringUtil.getBean("frameServiceImpl");
		}
		return frameService;
	}

	public static SysStation getStation(HttpServletRequest request) {
		StringBuffer requestUrl = request.getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		String[] codeAndDomain = domain.split("\\.");
		String stationCode = codeAndDomain[0];
		FrameService frame = getFrameService();
		SysStation station = frame.getStation(domain);
		if (station == null) {
			String secDomain = "";
			if (codeAndDomain != null && codeAndDomain.length > 2) {
				for (int i = 1; i < codeAndDomain.length; i++) {
					secDomain += codeAndDomain[i] + ".";
				}
				station = frame.getStation(secDomain.substring(0, secDomain.length() - 1));
			}
		}
		if (station != null && codeAndDomain.length > 2
				&& "on".equals(StationConfigUtil.get(station.getId(), StationConfig.onoff_station_code_change))) {
			station.setFloder(stationCode);
		}
		return station;
	}

	public static HttpServletRequest getOrgRequest() {
		return XssHttpServletRequestWrapper.getOrgRequest(SysUtil.getRequest());
	}
	
	public static void AllowCrossAomain(HttpServletResponse response,HttpServletRequest request) {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin",request.getHeader("Origin"));//设置允许跨域请求
    	response.addHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
    	response.addHeader("Access-Control-Allow-Credentials", "true");
	}
}