package com.game.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class ShellUtil {
	private static Logger logger = Logger.getLogger(ShellUtil.class);
	private static final String GOD_CODE_SHELL = "/usr/local/mysh/addgodcode.sh ";
	private static final String CAT_NGINX_LOG_SHELL = "/usr/local/mysh/catgrepnginxlog.sh ";

	private static final String UPDATE_SSL_FILE_SHELL = "/usr/local/mysh/updatesslfile.sh ";
	private static final String UPDATE_NGINX_CONF_SHELL = "/usr/local/mysh/updatenginxconf.sh";
	private static final String CLEAN_NGINX_CACHE_SHELL="/usr/local/mysh/cleannginxcache.sh";

	public static String addGodCode(String code) {
//		try {
//			Process p0 = Runtime.getRuntime().exec(GOD_CODE_SHELL + code);
//			// 读取标准输出流
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p0.getInputStream()));
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			while ((line = bufferedReader.readLine()) != null) {
//				sb.append(line);
//			}
//			// 读取标准错误流
//			BufferedReader brError = new BufferedReader(new InputStreamReader(p0.getErrorStream(), "utf-8"));
//			while ((line = brError.readLine()) != null) {
//				sb.append(line);
//			}
//			// waitFor()判断Process进程是否终止，通过返回值判断是否正常终止。0代表正常终止
//			int c = p0.waitFor();
//			if (c != 0) {
//				return "非正常终止";
//			}
//			return sb.toString();
//		} catch (Exception e1) {
//			logger.error("运行shell  addGodCode 发生错误code=" + code, e1);
//		}
//		return null;
		return "";
	}

	public static String catGrepNginxLog(String key) {
//		try {
//			Process p0 = Runtime.getRuntime().exec(CAT_NGINX_LOG_SHELL + "\"" + key + "\"");
//			// 读取标准输出流
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p0.getInputStream()));
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			while ((line = bufferedReader.readLine()) != null) {
//				sb.append(line);
//			}
//			// 读取标准错误流
//			BufferedReader brError = new BufferedReader(new InputStreamReader(p0.getErrorStream(), "utf-8"));
//			while ((line = brError.readLine()) != null) {
//				sb.append(line);
//			}
//			// waitFor()判断Process进程是否终止，通过返回值判断是否正常终止。0代表正常终止
//			int c = p0.waitFor();
//			if (c != 0) {
//				return "非正常终止";
//			}
//			return sb.toString();
//		} catch (Exception e1) {
//			logger.error("运行shell  catGrepNginxLog 发生错误code=" + key, e1);
//		}
//		return null;
		return "";
	}

	public static String updateSslFile(Integer certId) {
//		try {
//			Process p0 = Runtime.getRuntime().exec(UPDATE_SSL_FILE_SHELL + certId);
//			// 读取标准输出流
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p0.getInputStream()));
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			while ((line = bufferedReader.readLine()) != null) {
//				sb.append(line);
//			}
//			// 读取标准错误流
//			BufferedReader brError = new BufferedReader(new InputStreamReader(p0.getErrorStream(), "utf-8"));
//			while ((line = brError.readLine()) != null) {
//				sb.append(line);
//			}
//			// waitFor()判断Process进程是否终止，通过返回值判断是否正常终止。0代表正常终止
//			int c = p0.waitFor();
//			if (c != 0) {
//				return "非正常终止";
//			}
//			return sb.toString();
//		} catch (Exception e1) {
//			logger.error("运行shell  UPDATE_SSL_FILE_SHELL 发生错误", e1);
//		}
//		return null;
		return "";
	}

	public static String updateNginxConf() {
//		try {
//			Process p0 = Runtime.getRuntime().exec(UPDATE_NGINX_CONF_SHELL);
//			// 读取标准输出流
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p0.getInputStream()));
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			while ((line = bufferedReader.readLine()) != null) {
//				sb.append(line);
//			}
//			// 读取标准错误流
//			BufferedReader brError = new BufferedReader(new InputStreamReader(p0.getErrorStream(), "utf-8"));
//			while ((line = brError.readLine()) != null) {
//				sb.append(line);
//			}
//			// waitFor()判断Process进程是否终止，通过返回值判断是否正常终止。0代表正常终止
//			int c = p0.waitFor();
//			if (c != 0) {
//				return "非正常终止";
//			}
//			return sb.toString();
//		} catch (Exception e1) {
//			logger.error("运行shell  UPDATE_NGINX_CONF_SHELL 发生错误", e1);
//		}
//		return null;
		
		return "";
	}
	
	

	public static String cleanNginxCache() {
//		try {
//			Process p0 = Runtime.getRuntime().exec(CLEAN_NGINX_CACHE_SHELL);
//			// 读取标准输出流
//			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p0.getInputStream()));
//			String line = null;
//			StringBuilder sb = new StringBuilder();
//			while ((line = bufferedReader.readLine()) != null) {
//				sb.append(line);
//			}
//			// 读取标准错误流
//			BufferedReader brError = new BufferedReader(new InputStreamReader(p0.getErrorStream(), "utf-8"));
//			while ((line = brError.readLine()) != null) {
//				sb.append(line);
//			}
//			// waitFor()判断Process进程是否终止，通过返回值判断是否正常终止。0代表正常终止
//			int c = p0.waitFor();
//			if (c != 0) {
//				return "非正常终止";
//			}
//			return sb.toString();
//		} catch (Exception e1) {
//			logger.error("运行shell  CLEAN_NGINX_CACHE_SHELL 发生错误", e1);
//		}
		return "";
	}
}
