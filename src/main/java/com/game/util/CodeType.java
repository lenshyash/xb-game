package com.game.util;

public enum CodeType {

	Sports("S"), // 体育
	Lottery("L"), // 彩票
	Marksix("M"),//六合彩
	Real("Z"), // 视讯
	Egame("E"), // 电子
	Chess("CH"), // 棋牌
	ThirdLottery("TL"), // 三方彩票
	ThirdSport("TS"), // 三方体育
	Hunter("H"), // 捕鱼王
	CASH("C"), // 提现
	RECHARGE("R"),// 充值
	JointPurchase("J");//合买
	
	private int lastSaveIndex = 0;
	
	private int createCount = 0;
	
	private String codeHeader;

	public String getCodeHeader() {
		return codeHeader;
	}

	public void setCodeHeader(String codeHeader) {
		this.codeHeader = codeHeader;
	}

	private CodeType(String codeHeader) {
		this.codeHeader = codeHeader;
	}
	
	public void addCreateCount(){
		++createCount;
	}
	
	public boolean check(int value){
		if(createCount - lastSaveIndex > value){
			return true;
		}
		return false;
	}
	
	public void refreshLastSaveIndex(){
		lastSaveIndex = createCount;
	}
	
	public static CodeType getCodeType(String code) {
		if(code == null){
			return null;
		}
		code = code.toUpperCase();
		CodeType[] types = values();
		for (int i = 0; i < types.length; i++) {
			if (types[i].getCodeHeader().equals(code)) {
				return types[i];
			}
		}
		return null;
	}
}