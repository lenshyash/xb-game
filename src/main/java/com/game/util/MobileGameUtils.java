package com.game.util;

import com.game.model.lottery.LotteryEnum;

public class MobileGameUtils {

	public static String lotTypeTran(String lotType, String lotCode){
		// 第一版彩种类型: 1=系统彩, 2=时时彩, 3=pk10, 4=排列三, 5=11选5, 6=香港彩, 7=PC蛋蛋, 100=快三
		// 第三版彩种类型: 51=系统彩, 52=时时彩, 53=pk10, 54=排列三, 55=11选5, 57=PC蛋蛋, 100=快三
		LotteryEnum lotteryEnum = LotteryEnum.getEnum(lotCode);
		if(lotteryEnum == null){
			return "-1";
		}

		switch (lotteryEnum) {
		case JSSB3:
		case AHK3:
		case HBK3:
		case SHHK3:
		case HEBK3:
		case GXK3:
		case BJK3:
		case JXK3:
		case GSK3:
		case JLK3:
		case GZK3:
		case FFK3:
		case WFK3:
		case JPK3:
		case KRK3:
		case SFK3:
		case ESK3:
		case TMK3:
		case HKK3:
		case AMK3:
			return "0";
		default:
			if(lotType.length() > 1){
				lotType = lotType.substring(1);
			}
			return lotType;
		}
	}

	public static JiebaoGameType getJiebaoGameType(String lotCode, String playCode) {
		LotteryEnum lotteryEnum = LotteryEnum.getEnum(lotCode);
		if(lotteryEnum == null){
			return new JiebaoGameType("", "");
		}

		switch (lotteryEnum) {
		case BJSC:
		case XYFT:
		case LXYFT:
		case AZXY10:
		case SFSC:
		case SFFT:
		case FFSC:
		case WFSC:
		case WFFT:
		case LBJSC:
		case XSFSC:
		case XWFSC:
		case JSSC168:
			return getJiebaoGameTypeBJSC(playCode);
		case SH11X5:
		case GX11X5:
		case JX11X5:
		case SD11X5:
		case GD11X5:
		case FF11X5:
		case SF11X5:
		case WF11X5:
			return getJiebaoGameType11X5(playCode);
		case PL3:
		case FC3D:
		case FF3D:
		case WF3D:
			return getJiebaoGameTypeKK(playCode);
		case CQSSC:
		case XJSSC:
		case TJSSC:
		case FFC:
		case EFC:
		case WFC:
		case HKWFC:
		case AMWFC:
		case SFC:
		case ESFC:
		case TXFFC:
		case HNFFC:
		case HNWFC:
		case AZXY5:
		case JSSSC168:
			return getJiebaoGameTypeSSC(playCode);
		case PCEGG:
		case JND28:
		case SF28:
			return getJiebaoGameTypePCEGG(playCode);
		case JSSB3:
		case AHK3:
		case HBK3:
		case SHHK3:
		case HEBK3:
		case GXK3:
		case BJK3:
		case GSK3:
		case JLK3:
		case GZK3:
		case JXK3:
		case FFK3:
		case WFK3:
		case JPK3:
		case KRK3:
		case SFK3:
		case ESK3:
		case TMK3:
		case HKK3:
		case AMK3:
			return getJiebaoGameTypeK3(playCode);
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameTypeSSC(String playCode) {
		switch (playCode) {
		case "5xzx_fs":
			return new JiebaoGameType("1", "12345");
		case "bdw_h31m":
			return new JiebaoGameType("5", "1345");
		case "bdw_q31m":
			return new JiebaoGameType("5", "1123");
		case "bdw_z31m":
			return new JiebaoGameType("5", "1234");
		case "dwd":
			return new JiebaoGameType("1", "112345");
		case "h2zx_fs":
			return new JiebaoGameType("1", "45");
		case "h2zx_hz":
			return new JiebaoGameType("3", "45");
		case "h3zux_zu3":
			return new JiebaoGameType("7", "345");
		case "h3zux_zu6":
			return new JiebaoGameType("8", "345");
		case "h3zx_fs":
			return new JiebaoGameType("1", "345");
		case "h4zx_fs":
			return new JiebaoGameType("1", "2345");
		case "q2zx_fs":
			return new JiebaoGameType("1", "12");
		case "q2zx_hz":
			return new JiebaoGameType("3", "12");
		case "q3zux_zu3":
			return new JiebaoGameType("7", "123");
		case "q3zux_zu6":
			return new JiebaoGameType("8", "123");
		case "q3zx_fs":
			return new JiebaoGameType("1", "123");
		case "q4zx_fs":
			return new JiebaoGameType("1", "1234");
		case "rxwf_r2zx_fs":
			return new JiebaoGameType("1", "20");
		case "rxwf_r3zux_zu3":
			return new JiebaoGameType("7", "30");
		case "rxwf_r3zux_zu6":
			return new JiebaoGameType("8", "30");
		case "rxwf_r3zx_fs":
			return new JiebaoGameType("1", "30");
		case "rxwf_r4zx_fs":
			return new JiebaoGameType("1", "40");
		case "z3zux_zu3":
			return new JiebaoGameType("7", "234");
		case "z3zux_zu6":
			return new JiebaoGameType("8", "234");
		case "z3zx_fs":
			return new JiebaoGameType("1", "234");
		case "dxds_q3":
			return new JiebaoGameType("1", "6123");
		case "dxds_h3":
			return new JiebaoGameType("1", "6345");
		case "dxds_h2":
			return new JiebaoGameType("1", "612");
		case "dxds_q2":
			return new JiebaoGameType("1", "645");
		case "dxds_zh":
			return new JiebaoGameType("99", "612345");
		case "longhudou":
			return new JiebaoGameType("99", "6120");
		case "longhuhe":
			return new JiebaoGameType("99", "6120");
		case "baozi":
			return new JiebaoGameType("99", "6999");
		case "shunzi":
			return new JiebaoGameType("99", "6123");
		case "duizi":
			return new JiebaoGameType("99", "6112");
		case "banshun":
			return new JiebaoGameType("99", "6126");
		case "zaliu":
			return new JiebaoGameType("99", "6169");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameType11X5(String playCode) {
		switch (playCode) {
		case "bdw_h3":
			return new JiebaoGameType("64", "35");
		case "bdw_q3":
			return new JiebaoGameType("64", "13");
		case "bdw_z3":
			return new JiebaoGameType("64", "24");
		case "dwd":
			return new JiebaoGameType("63", "0");
		case "h2zx":
			return new JiebaoGameType("66", "2045");
		case "h2zx_fs":
			return new JiebaoGameType("65", "2045");
		case "h3zx":
			return new JiebaoGameType("68", "3035");
		case "h3zx_fs":
			return new JiebaoGameType("67", "3035");
		case "q2zx":
			return new JiebaoGameType("66", "2012");
		case "q2zx_fs":
			return new JiebaoGameType("65", "2012");
		case "q3zx":
			return new JiebaoGameType("68", "3013");
		case "q3zx_fs":
			return new JiebaoGameType("67", "3013");
		case "rxfs_rx1z1":
			return new JiebaoGameType("55", "0");
		case "rxfs_rx2z2":
			return new JiebaoGameType("56", "0");
		case "rxfs_rx3z3":
			return new JiebaoGameType("57", "0");
		case "rxfs_rx4z4":
			return new JiebaoGameType("58", "0");
		case "rxfs_rx5z5":
			return new JiebaoGameType("59", "0");
		case "rxfs_rx6z5":
			return new JiebaoGameType("60", "0");
		case "rxfs_rx7z5":
			return new JiebaoGameType("61", "0");
		case "rxfs_rx8z5":
			return new JiebaoGameType("62", "0");
		case "z3zx":
			return new JiebaoGameType("68", "3024");
		case "z3zx_fs":
			return new JiebaoGameType("67", "3024");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameTypeBJSC(String playCode) {
		switch (playCode) {
		case "dwd":
			return new JiebaoGameType("1", "10");
		case "longhu_gunjun":
			return new JiebaoGameType("2", "1");
		case "longhu_jijun":
			return new JiebaoGameType("2", "2");
		case "longhu_yajun":
			return new JiebaoGameType("2", "3");
		case "q1zx_fs":
			return new JiebaoGameType("1", "1");
		case "q2zx_fs":
			return new JiebaoGameType("1", "12");
		case "q3zx_fs":
			return new JiebaoGameType("1", "123");
		case "dxds":
			return new JiebaoGameType("3", "100");
		case "gyhz":
			return new JiebaoGameType("3", "200");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameTypeKK(String playCode) {
		switch (playCode) {
		case "bdw_1m":
			return new JiebaoGameType("4", "1123");
		case "bdw_2m":
			return new JiebaoGameType("4", "2123");
		case "dwd":
			return new JiebaoGameType("1", "1123");
		case "dxds_h2":
			return new JiebaoGameType("2", "12");
		case "dxds_q2":
			return new JiebaoGameType("2", "23");
		case "em_h2zux":
			return new JiebaoGameType("4", "212");
		case "em_q2zux":
			return new JiebaoGameType("4", "223");
		case "h2zx_fs":
			return new JiebaoGameType("1", "23");
		case "q2zx_fs":
			return new JiebaoGameType("1", "12");
		case "zhx_fs":
			return new JiebaoGameType("9", "123");
		case "zux_z3":
			return new JiebaoGameType("5", "123");
		case "zux_z6":
			return new JiebaoGameType("6", "123");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameTypePCEGG(String playCode) {
		switch (playCode) {
		case "sxfs": // 三星复式
			return new JiebaoGameType("1", "1");
		case "q2zx_fs": // 前二复式
			return new JiebaoGameType("1", "2");
		case "h2zx_fs": // 后二复式
			return new JiebaoGameType("1", "3");
		case "dwd": // 定位胆
			return new JiebaoGameType("2", "4");
		case "dxds": // 和值大小单双
			return new JiebaoGameType("2", "5");
		case "bdw": // 不定位胆
			return new JiebaoGameType("3", "6");
		case "q2zx": // 前二组选
			return new JiebaoGameType("4", "7");
		case "h2zx": // 后二组选
			return new JiebaoGameType("4", "8");
		case "sxzx": // 三星组六
			return new JiebaoGameType("5", "9");
		case "hz": // 和值
			return new JiebaoGameType("6", "10");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static JiebaoGameType getJiebaoGameTypeK3(String playCode) {
		switch (playCode) {
		case "hz": // 和值
			return new JiebaoGameType("1", "1");
		case "dxds": // 大小单双
			return new JiebaoGameType("2", "2");
		case "sthtx": // 三同号通选
			return new JiebaoGameType("2", "3");
		case "sthdx": // 三同号单选
			return new JiebaoGameType("2", "4");
		case "slhtx": // 三连号通选
			return new JiebaoGameType("2", "5");
		case "ethfx": // 二同号复式
			return new JiebaoGameType("2", "6");
		case "sbtx": // 三不同号
			return new JiebaoGameType("3", "7");
		case "ebth": // 二不同号
			return new JiebaoGameType("4", "8");
		case "ethdx": // 二同号单式
			return new JiebaoGameType("", "");
		default:
			return new JiebaoGameType("", "");
		}
	}

	public static class JiebaoGameType {
		private String bettype;
		private String subid;

		public JiebaoGameType(){
			
		}

		public JiebaoGameType(String bettype, String subid){
			this.bettype = bettype;
			this.subid = subid;
		}

		public String getBettype() {
			return bettype;
		}

		public void setBettype(String bettype) {
			this.bettype = bettype;
		}

		public String getSubid() {
			return subid;
		}

		public void setSubid(String subid) {
			this.subid = subid;
		}

	}
}
