package com.game.util;

import java.net.URI;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;

public class ShortUrlHelper {
	private static Logger logger = LoggerFactory.getLogger(ShortUrlHelper.class);

	public static String getShortUrlSina(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接必须以http://或https://开头");
			return null;
		}
		String appkey = "3a16d912ed469bc5ebc8adf3f35eb6e2";
		String sign = MD5.encryption(appkey+MD5.encryption(url));
		
		try {
			url = "http://www.mynb8.com/api/sina?appkey="+appkey+"&sign="+sign+"&long_url=" +URLEncoder.encode(url, "UTF-8");
			String r = httpGet(url, "UTF-8");
			if (r != null) {
				JSONObject data = JSONObject.parseObject(r);
				return data.getString("short_url");
			}
		} catch (Exception e) {
			logger.error("生成短链接发生错误", e);
		}
		return null;
	}
	
	public static String getShortUrlSina2(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接必须以http://或https://开头");
			return null;
		}
		String appkey = "AbF19a5657589c424cf5ab8a0cf0aac4";
		
		try {
			url = "http://dwz7.cc/dwz.php/api/short_dwz.html?key="+appkey+"&url="+URLEncoder.encode(url, "UTF-8");
			String r = httpGet(url, "UTF-8");
			if (r != null) {
				return r;
			}
		} catch (Exception e) {
			logger.error("生成短链接发生错误", e);
		}
		return null;
	}
	
	public static String getShortUrlTx(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接必须以http://或https://开头");
			return null;
		}
		String appkey = "3a16d912ed469bc5ebc8adf3f35eb6e2";
		try {
			url = "http://www.mynb8.com/api2/urlcn?appkey="+appkey+"&long_url=" +URLEncoder.encode(url, "UTF-8");
			String r = httpGet(url, "UTF-8");
			if (r != null) {
				JSONObject data = JSONObject.parseObject(r);
				return data.getString("short_url");
			}
		} catch (Exception e) {
			logger.error("生成短链接发生错误", e);
		}
		return null;
	}
	
	public static String getShortUrl5(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接必须以http://或https://开头");
			return null;
		}
		try {
			url = "https://api.weibo.com/2/short_url/shorten.json?source=1681459862&url_long=" + URLEncoder.encode(url, "UTF-8");
			String r = httpGet(url, "UTF-8");
			if (r != null) {
				JSONObject json = JSON.parseObject(r);
				if("23216".equals(json.getString("error_code"))){//如果授权失败则重复请求三次
					int i = 0;
					while(i<3){
						r = httpGet(url, "UTF-8");
						json = JSON.parseObject(r);
						if(json.getJSONArray("urls")!=null){
							break;
						}
						i++;
					}
				}
				JSONArray arr = json.getJSONArray("urls");
				JSONObject obj = arr.getJSONObject(0);
				return obj.getString("url_short");
			}
		} catch (Exception e) {
			logger.error("生成短链接发生错误", e);
		}
		return null;
	}

	public static String getShortUrl2(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接2必须以http://开头");
			return null;
		}
		try {
			url = "http://50r.cn/short_url.json?url=" + URLEncoder.encode(url, "UTF-8");
			String r = httpGet(url, "UTF-8");
			if (r != null) {
				JSONObject obj = JSON.parseObject(r);
				return obj.getString("url");
			}
		} catch (Exception e) {
			logger.error("生成短链接2发生错误", e);
		}
		return null;
	}

	private static String httpGet(String url, String encode) {
		RequestConfig defaultRequestConfig = RequestConfig.custom().setConnectTimeout(2500).setConnectionRequestTimeout(2500).setSocketTimeout(2500).build();
		CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
		CloseableHttpResponse response1 = null;
		try {
			HttpGet get = new HttpGet(url);
			response1 = httpclient.execute(get);
			if (response1 == null) {
				return null;
			}
			return EntityUtils.toString(response1.getEntity(), encode);

		} catch (Exception e) {
			logger.error("生成短链接发生错误url=" + url, e);
		} finally {
			try {
				if (response1 != null) {
					response1.close();
				}
				httpclient.close();
			} catch (Exception e) {
				logger.error("生成短链接finally发生错误", e);
			}
		}
		return null;
	}

	public static void main(String[] args){
		String shortUrlSina = getShortUrlSina2("https://www.baidu.com");
		System.out.println(shortUrlSina);
	}

	private static Pattern copyLinkPattern = Pattern.compile("<input.+(?=id)id=\"copylink\".+(?=value)value=\"([^\"]+)\"[^>]*>");

	/**
	 * 国外网站无法使用该接口
	 * 
	 * @param url
	 * @return
	 */
	public static String getShortUrl3(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接3必须以http://开头");
			return null;
		}
		try {
			RequestConfig defaultRequestConfig = RequestConfig.custom().setConnectTimeout(1500).build();
			CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
			CloseableHttpResponse response1 = null;
			try {
				HttpUriRequest post = RequestBuilder.post().setUri(new URI("http://suo.im/")).addParameter("url", url).build();
				response1 = httpclient.execute(post);
				if (response1 == null) {
					return null;
				}
				String html = EntityUtils.toString(response1.getEntity(), "UTF-8");
				Matcher m = copyLinkPattern.matcher(html);
				if (m.find()) {
					return m.group(1);
				}
			} catch (Exception e) {
				logger.error("生成短链接3发生错误url=" + url, e);
			} finally {
				try {
					if (response1 != null) {
						response1.close();
					}
					httpclient.close();
				} catch (Exception e) {
					logger.error("生成短链接3finally发生错误", e);
				}
			}
			return null;
		} catch (Exception e) {
			logger.error("生成短链接3发生错误", e);
		}
		return null;
	}

	/**
	 * 国外网站使用该接口
	 * 
	 * @param url
	 * @return
	 */
	public static String getShortUrl4(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接4必须以http://开头");
			return null;
		}
		try {
			RequestConfig defaultRequestConfig = RequestConfig.custom().setConnectTimeout(2500).setConnectionRequestTimeout(2500).setSocketTimeout(2500).build();
			CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
			CloseableHttpResponse response1 = null;
			try {
				HttpUriRequest post = RequestBuilder.post().setUri(new URI("http://url.ms/cn.php")).addParameter("url", url).build();
				response1 = httpclient.execute(post);
				if (response1 == null) {
					return null;
				}
				String html = EntityUtils.toString(response1.getEntity(), "UTF-8");
				Matcher m = copyLinkPattern.matcher(html);
				if (m.find()) {
					return m.group(1);
				} else {
					logger.error("生成短链接4数据为空url=" + url + "   html=" + html);
				}
			} catch (Exception e) {
				logger.error("生成短链接4发生错误url=" + url, e);
			} finally {
				try {
					if (response1 != null) {
						response1.close();
					}
					httpclient.close();
				} catch (Exception e) {
					logger.error("生成短链接4finally发生错误", e);
				}
			}
			return null;
		} catch (Exception e) {
			logger.error("生成短链接4发生错误", e);
		}
		return null;
	}
	/**
	 * 防红链接生成
	 * @param linkUrl
	 * @return
	 * http://www.whirljump.cn/add_api.php?name=tp1&pass=123456&url=https://xbzhanshi.com/registerMutil/link_82738.do
	 * http://www.whirljump.cn/add_api.php?name=tp1&pass=123456&url=
	 */
	public static String ACCOUNT ="t123";
	public static String PASSWORD ="xb123789";
	
//	public static String ACCOUNT ="go14";
//	public static String PASSWORD ="123456";
	public static String getAntiSealink(String url) {
		if (url == null || (!url.startsWith("http://") && !url.startsWith("https://"))) {
			logger.info("生成短链接必须以http://或https://开头");
			return null;
		}
		try {
//			url = "http://www.adwj6d.cn/add_api.php?name="+ACCOUNT+"&pass="+PASSWORD+"&url="+ URLEncoder.encode(url, "UTF-8")+"&title="+StationUtil.getFolder();
			url = "http://www.adwj6d.cn/add_api.php?name="+ACCOUNT+"&pass="+PASSWORD+"&url="+ URLEncoder.encode(url, "UTF-8")+"&title="+StationUtil.getStationName();
//			url = "http://www.p4cdn.cn/add_api.php?name="+ACCOUNT+"&pass="+PASSWORD+"&url="+ URLEncoder.encode(url, "UTF-8")+"&title="+StationUtil.getStationName();
//			String r = httpGet(url, "UTF-8");
			String r = HttpClientUtils.getInstance().sendHttpGet(url);
			if (r != null) {
				JSONObject result = JSONObject.parseObject(r);
				if("200".equals(result.getString("code"))){
					return result.getString("msg");
				}
			}
		} catch (Exception e) {
			logger.error("生成短链接发生错误", e);
		}
		return null;
	}
}
