package com.game.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;

import com.game.constant.LogType;
import com.game.model.SysLog;
import com.game.model.SysLoginLog;
import com.game.service.SysLogService;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

/**
 * 日志辅助类
 * 
 * @author admin
 *
 */
public class SysLogUtil {

	private static SysLogService logService;

	/**
	 * 增加当前登录用户的登录日志，参数为日志内容
	 * 
	 * @param content
	 */
	public static void loginLog(String content, Long userId, String account,Long stationId) {
		if(stationId==null) {
			stationId = StationUtil.getStationId();
		}
		SysLoginLog log = new SysLoginLog();
		log.setAccount(account);
		log.setAccountId(userId);
		log.setCreateDatetime(new Date());
		log.setContent(content);
		log.setAccountIp(UserUtil.getIpAddress());
		if (StationUtil.isAdminStation()) {
			log.setPlatform(SysLoginLog.PLATFORM_ADMIN);
			log.setStationId(0l);
		} else {
			log.setPlatform(SysLoginLog.PLATFORM_PLATFORM);
			log.setStationId(stationId);
		}
		HttpServletRequest req=SysUtil.getRequest();
		if(req!=null) {
			String uaStr = req.getHeader("User-Agent");
			//看一下控制台
			System.out.println("请求设备是"+uaStr);
			String deviceId = req.getParameter("deviceId");
			if (StringUtils.isNotEmpty(uaStr)) {
				UserAgent userAgent = UserAgent.parseUserAgentString(uaStr);
//				Browser browser = userAgent.getBrowser();
//				String name = browser.getName();
//				Version browserVersion = userAgent.getBrowserVersion();
//				String version = browserVersion.getVersion();
//				System.out.println("请求浏览器信息是"+name+" "+version);
				
				OperatingSystem os = userAgent.getOperatingSystem();
				if (os != null) {
					log.setLoginOs(os.name());
					log.setDeviceType(os.getDeviceType().toString());
				}
				if(deviceId!=null) {
					log.setDeviceId(deviceId);
				}
			}
		}
		log.setDomain(req.getHeader("host"));
		getService().addLoginLog(log);
	}

	/**
	 * 增加当前登录用户的管理日志，参数为日志内容，和枚举类型
	 * 
	 * @param content
	 * @param logType
	 */
	public static SysLog log(String content, LogType logType) {
		return log(content, "", logType);
	}

	/**
	 * 增加当前登录用户的日志，参数为日志内容、日志备注
	 * 
	 * @param content
	 * @param remark
	 */
	public static SysLog log(String content, String remark, LogType logType) {
		long platform = 0l;
		long stationId = 0l;
		String params = JsonUtil.toJson(UserUtil.getRequest().getParameterMap());
		if (StationUtil.isAdminStation()) {
			platform = SysLog.PLATFORM_ADMIN;
		} else {
			platform = SysLog.PLATFORM_PLATFORM;
			stationId = StationUtil.getStationId();
		}
		String account = UserUtil.getUserAccount();
		Long accountId = UserUtil.getUserId();
		if (StringUtil.isEmpty(account) || Validator.isNull(accountId)) {
			return null;
		}
		return log(content, remark, accountId, account, UserUtil.getIpAddress(), stationId, logType.getType(), platform,
				params);
	}

	public static SysLog log(String content, String remark, Long accountId, String account, String accountIp,
			Long stationId, Long type, Long platform, String params) {
		SysLog log = new SysLog();
		log.setAccount(account);
		log.setAccountId(accountId);
		log.setCreateDatetime(new Date());
		log.setLogContent(content);
		log.setRemark(remark);
		log.setAccountIp(accountIp);
		log.setLogType(type);
		log.setPlatform(platform);
		log.setStationId(stationId);
		log.setParams(params);
		return getService().addLog(log);
	}
	
	public static SysLog getLogById(Long id) {
		return getLogById(id,StationUtil.getStationId());
	}
	
	public static SysLog getLogById(Long id,Long stationId) {
		return getService().getOne(id,stationId);
	}
	
	public static SysLog saveLog(SysLog log) {
		return getService().saveLog(log);
	}

	private static SysLogService getService() {
		if (logService == null) {
			logService = (SysLogService) SpringUtil.getBean("sysLogServiceImpl");
		}
		return logService;
	}

	public static void loginLog(String msg, Long id, String account) {
		loginLog(msg,id,account,null);
	}
}
