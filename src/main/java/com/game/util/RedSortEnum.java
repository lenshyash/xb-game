package com.game.util;
/**
 * 
 * 红包排序规则
 * 
 * @author lj
 */
public enum RedSortEnum {

	RISE("升","rise"),
	DROP("降","drop"),
	DISORDERLY("无序","disorderly");
	
	private String name;
	private String value;
	
	private RedSortEnum(String name,String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
