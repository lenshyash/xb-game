package com.game.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;

import com.alibaba.fastjson.JSONObject;
import com.game.model.MemberLevelBase;
import com.game.service.MemberLevelService;

public class MemberLevelUtil {

	private static MemberLevelService memberLevelService;

	/**
	 * 创建未分层等级
	 */
	public static void createDefaultLevel(Long stationId) {
		MemberLevelBase level = new MemberLevelBase();
		level.setCreateDatetime(new Date());
		level.setLevelName(MemberLevelBase.LEVEL_DEFALUT_NAME);
		level.setMemberCount(0);
		level.setDepositMoney(BigDecimal.ZERO);
		level.setStatus(MemberLevelBase.STATUS_ENALBED);
		level.setLevelDefault(MemberLevelBase.LEVEL_DEFALUT);
		level.setStationId(stationId);
		getService().saveDefaultLevel(level);
	}

	/**
	 * 未分层数量+1
	 */
	public static void addDefaultLevelCount() {
		MemberLevelBase defaultLevel = getDefaultLevel();
		if (defaultLevel != null) {
			getService().addLevelCount(defaultLevel.getId());
		}
	}

	public static MemberLevelBase getLevel(Long levelId) {
		List<MemberLevelBase> levels = getStationLevels();
		if (levels == null || levels.isEmpty()) {
			return null;
		}
		for (MemberLevelBase memberLevelBase : levels) {
			if (StringUtil.equals(memberLevelBase.getId(), levelId)) {
				return memberLevelBase;
			}
		}
		return null;
	}

	public static JSONObject getLevelInfoJson() {
		return getService().getCurrentLevelJson();
	}

	public static MemberLevelBase getNextLevel() {
		return getNextLevel(UserUtil.getLevelGroup());
	}

	public static MemberLevelBase getNextLevel(Long levelId) {
		List<MemberLevelBase> levels = getStationLevels();
		if (levels == null || levels.isEmpty()) {
			return null;
		}
		boolean current = false;
		for (MemberLevelBase memberLevelBase : levels) {
			if (current) {
				return memberLevelBase;
			}
			if (StringUtil.equals(memberLevelBase.getId(), levelId)) {
				current = true;
			}
		}
		return null;
	}

	/**
	 * 得到未分层ID
	 * 
	 * @return
	 */
	public static Long getDefaultLevelId() {
		MemberLevelBase defaultLevel = getDefaultLevel();
		if (defaultLevel != null) {
			return defaultLevel.getId();
		}
		return null;
	}

	/**
	 * 检查是否升级
	 * 
	 * @param accountId
	 */
	public static void checkDepositForLevel(long accountId) {
		getService().checkDepositForLevel(accountId);
	}

	/**
	 * 得到未分层等级
	 * 
	 * @return
	 */
	public static MemberLevelBase getDefaultLevel(Long stationId) {
		List<MemberLevelBase> levels = getService().getStationLevels(stationId);
		if (levels == null || levels.isEmpty()) {
			return null;
		}
		for (MemberLevelBase memberLevelBase : levels) {
			if (StringUtil.equals(memberLevelBase.getLevelDefault(), MemberLevelBase.LEVEL_DEFALUT)) {
				return memberLevelBase;
			}
		}
		return null;
	}

	public static List<MemberLevelBase> getStationLevels() {
		return getStationLevels(StationUtil.getStationId());
	}

	public static Map<Long, String> getStationLevelCombo() {
		List<MemberLevelBase> levels = getStationLevels(StationUtil.getStationId());
		Map<Long, String> typeMap = new HashMap<Long, String>();
		for (MemberLevelBase memberLevelBase : levels) {
			typeMap.put(memberLevelBase.getId(), memberLevelBase.getLevelName());
		}
		return typeMap;
	}

	public static void addLevelCount(Long levelId) {
		getService().addLevelCount(levelId);
	}

	public static void subLevelCount(Long levelId) {
		getService().subLevelCount(levelId);
	}

	public static List<MemberLevelBase> getStationLevels(Long stationId) {
		List<MemberLevelBase> levels = getService().getStationLevels(stationId);
		if (levels == null || levels.isEmpty()) {
			return null;
		}
		return levels;
	}

	public static boolean hasRolePays(Long curLevelId, Long levelId) {

		if (Validator.isNull(levelId)) {
			return true;
		}

		if (Validator.isNull(curLevelId)) {
			return false;
		}

		if (StringUtil.equals(levelId, curLevelId)) {
			return true;
		}

		MemberLevelBase curLevel = getLevel(curLevelId);
		MemberLevelBase limitLevel = getLevel(levelId);

		if (limitLevel == null) {
			return true;
		}
		if (curLevel == null) {
			return false;
		}

		return curLevel.getDepositMoney().compareTo(limitLevel.getDepositMoney()) != -1;
	}

	/**
	 * 得到未分层等级
	 * 
	 * @return
	 */
	public static MemberLevelBase getDefaultLevel() {
		return getDefaultLevel(StationUtil.getStationId());
	}

	private static MemberLevelService getService() {
		if (memberLevelService == null) {
			memberLevelService = (MemberLevelService) SpringUtil.getBean("memberLevelServiceImpl");
		}
		return memberLevelService;
	}
}