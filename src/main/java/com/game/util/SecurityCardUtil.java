package com.game.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.MixUtil;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.model.SecCard;
import com.game.model.SysAccount;

/**
 * 密保卡
 * @author admin
 *
 */
public class SecurityCardUtil {
	
	public static String CHECKED_PASSWORD = "CHECKED_PASSWORD";
	
	public static String SECURITY_CARD_POINTS_KEY = "SECURITY_CARD_POINTS";
	
	public static String SECURITY_CARD_LOGIN_USER_ID = "SECURITY_CARD_USER";
	
	public static int CACHE_TIMEOUT = 5 * 60;
	
	private static final String[] xindex = {"1","2","3","4","5","6","7","8"};
	private static final String[] yindex = {"A","B","C","D","E","F","G","H"};
	private static final String[] xyindex = {"—","|"};
	
	private static final int width = 400;    
	private static final int height = 300;   
	private static final String title = "密保卡"; 
	
	/**
	 * 生成三个坐标，并生成答案写入redis 
	 * @param cards
	 */
	public static void createPoints(List<SecCard> cards){
		int size = 3;
		int ccount = cards.size();
		String [] points = new String[3];
		String [][][] pwds = new String[ccount][][];
		for (int j = 0; j < pwds.length; j++) {
			String [][] table = JSONObject.parseObject(cards.get(j).getCardPassword(),String [][].class);
			pwds[j] = table;
		}
		
		StringBuffer result[] = new StringBuffer[ccount];
		for (int i = 0; i < size; i++) {
			int y = (int)(Math.random() * yindex.length);
			int x = (int)(Math.random() * xindex.length);
			points[i] = yindex[y] + xindex[x];
			
			for (int j = 0; j < result.length; j++) {
				StringBuffer sb = result[j];
				if(sb == null){
					sb = result[j] = new StringBuffer(6);
				}
				sb.append(pwds[j][y][x]);
			}
		}
		HttpServletRequest request = UserUtil.getRequest();
		request.setAttribute(SECURITY_CARD_POINTS_KEY, points);
		RedisAPI.addCache(getCacheKey(SECURITY_CARD_POINTS_KEY), JSONObject.toJSONString(result),CACHE_TIMEOUT);
	}
	
	public static void createPoints4Login(List<SecCard> cards,SysAccount account){
		createPoints(cards);
		RedisAPI.addCache(getCacheKey(SECURITY_CARD_LOGIN_USER_ID), account.getId()+"",CACHE_TIMEOUT);
		HttpServletRequest request = UserUtil.getRequest();
		request.setAttribute(CHECKED_PASSWORD, true);
	}
		
	/**
	 * 响应校验密保卡
	 * @return
	 */
	public static boolean reponseCardInfo(){
		HttpServletRequest request = UserUtil.getRequest();
		if(request.getAttribute(CHECKED_PASSWORD) == null){
			return false;
		}
		Object points = request.getAttribute(SECURITY_CARD_POINTS_KEY);
		ActionUtil.renderJson(MixUtil.newHashMap("success",true,"points",points,"hasCard",true));
		return true;
	}
	
	public static String getCacheKey(String pointKey) {
		HttpSession session = UserUtil.getSession();
		String sessionId = session.getId();
		String key = pointKey;
		if (StationUtil.isAdminStation()) {
			key += "_admin_";
		} else if (StationUtil.isMemberStation()) {
			key += "_member_";
		} else if (StationUtil.isAgentStation()) {
			key += "_agent_";
		} else if (StationUtil.isDailiStation()) {
			key += "_daili_";
		} else if (StationUtil.isMobileStation()) {
			key += "_mobile_";
		}
		return key + sessionId;
	}
	
	public static void check(String code){
		String json = RedisAPI.getCache(getCacheKey(SECURITY_CARD_POINTS_KEY));
		if(json == null){
			throw new GenericException("密保卡已过期");
		}
		String [] pwds = JSONObject.parseObject(json, String[].class);
		for (int i = 0; i < pwds.length; i++) {
			if(pwds[i].equalsIgnoreCase(code)){
				return;
			}
		}
		throw new GenericException("密保卡错误");
	}
	
	public static void responseCardImage(SecCard card) throws IOException{
		StringBuffer index = new StringBuffer("序列号 : ");
		String cardNo = card.getCode();
		
		for (int i = 0; i < cardNo.length(); i++) {
			if(i != 0 && i % 4 == 0){
				index.append(" ");
			}
			index.append(cardNo.charAt(i));
		}
		Font font = new Font("Serif", Font.BOLD, 14);    
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);    
		Graphics2D g2 = (Graphics2D)bi.getGraphics();    
		g2.setBackground(Color.WHITE);    
		g2.clearRect(0, 0, width, height);    
		g2.setPaint(Color.RED);    
/////////////////////////////////////////////////////////////////
		FontRenderContext context0 = g2.getFontRenderContext();    
		Rectangle2D bounds0 = font.getStringBounds(index.toString(), context0); 
		double x0 = 2;    
		double y0 = 2;    
		double ascent0 = -bounds0.getY();    
		double baseY0 = y0 + ascent0;   
		g2.drawString(title, (int)x0, (int)baseY0);    
/////////////////////////////////////////////////////////////////
		FontRenderContext context = g2.getFontRenderContext();    
		Rectangle2D bounds = font.getStringBounds(index.toString(), context); 
		double x = (width - 350);    
		double y = (height - 280);    
		double ascent = -bounds.getY();    
		double baseY = y + ascent;   
		
		g2.drawString(index.toString(), (int)x, (int)baseY);    
/////////////////////////////////////////////////////////////////
		for (int i = 0; i < xindex.length; i++) {
			double xx = (width - 350) + 40*(i+1);    
			double yx = (height - 280) + 30;    
			g2.drawString(xindex[i], (int)xx -4, (int)yx);
		}
/////////////////////////////////////////////////////////////////
		for (int i = 0; i < yindex.length; i++) {
			double xy = (width - 350) - 10;    
			double yy = (height - 280) + 30 + 30*(i+1);    
			g2.drawString(yindex[i], (int)xy, (int)yy - 3);
		}
/////////////////////////////////////////////////////////////////	
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 32; i++) {
				double xx = (width - 350) + 9+ 10*(i+1);    
				double yx = (height - 280) + 40 + 30*j;     
				g2.drawString(xyindex[0], (int)xx, (int)yx);
			}
		}
/////////////////////////////////////////////////////////////////	
		for (int j = 0; j < 9; j++) {
			for (int i = 0; i < 24; i++) {
				double xx = (width - 350) + 18 + 40*j;    
				double yx = (height - 280) + 34 + 10*(i+1);     
				g2.drawString(xyindex[1], (int)xx, (int)yx);
			}
		}
		
/////////////////////////////////////////////////////////////////	
		
		String [][] pwdTable = JSONObject.parseObject(card.getCardPassword(),String [][].class);
		for (int j = 0; j < pwdTable.length; j++) {
			for (int i = 0; i < pwdTable[j].length; i++) {
				String p = pwdTable[j][i];
				double xx = (width - 350) - 10 + 40*(i+1);    
				double yx = (height - 280) + 50 + 30*j;     
				g2.drawString(p, (int)xx + 5, (int)yx + 8);
			}
		}
		HttpServletResponse resp = UserUtil.getResponse();
		// 禁止图像缓存。
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", 0);
		resp.setContentType("image/jpeg");
		// 将图像输出到Servlet输出流中。
		ServletOutputStream sos = resp.getOutputStream();
		ImageIO.write(bi, "bmp", sos);
		sos.close();
	}
}
