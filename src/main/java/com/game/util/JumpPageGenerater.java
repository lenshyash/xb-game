package com.game.util;

import java.util.Map;
import java.util.TreeMap;

public class JumpPageGenerater {
	public static void main(String[] args) {
		System.out.println(initFunction());
		System.out.println(funcStr());
	}

	private static String funcStr() {
		String fun = "function dwrite(){function a(b){document.write(b);}a('<fra');a('meset');a(' fra');a('mespacing=\"0\" bor');a('der=\"0\" ro');a('ws=\"100%,*\" fr');a('amebor');a('der=\"0\"><fr');a('am');a('e sr');a('c=\"');a('方');a('\" scr');a('lling=\"auto\"');a('></fr');a('ame></fra');a('meset>');}$(function () {$(\"body\").empty();dwrite();})";
		StringBuilder sb = new StringBuilder("eval(");
		for (int i = 0; i < fun.length(); i++) {
			if ((fun.charAt(i) + "").equals("方")) {
				sb.append("-----");
			} else {
				if (i > 0) {
					sb.append("+");
				}
				sb.append("l(").append(encodeChar(fun.charAt(i))).append(")");
			}
		}
		sb.append(")");
		return sb.toString();
	}

	private static int encodeChar(char c) {
		return c * 78 * 20000 + (int) (Math.random() * 20000);
	}

	/**
	 * 创建js的方法，并加密
	 * 
	 * @return
	 */
	private static String initFunction() {
		String str = "function l(i){return String.fromCharCode(Math.floor(i/20000)/78)}";
		Map<String, String> map = new TreeMap<>();
		for (int i = 0; i < str.length(); i++) {
			map.put(str.charAt(i) + "", "");
		}
		StringBuilder sb = new StringBuilder();
		int i = map.size();
		String key = null;
		for (String c : map.keySet()) {
			key = getVarName(i);
			map.put(c, key);
			sb.append(key).append("='").append(c).append("';");
			i--;
		}
		sb.append("eval(");
		for (i = 0; i < str.length(); i++) {
			if (i > 0) {
				sb.append("+");
			}
			sb.append(map.get(str.charAt(i) + ""));
		}
		sb.append(")");
		return sb.toString();
	}

	private static String getVarName(int i) {
		String var = Integer.toBinaryString(i);
		var = ("000000" + var);
		var = var.substring(var.length() - 5);
		return "O" + var.replaceAll("0", "O").replaceAll("1", "l");
	}
}
