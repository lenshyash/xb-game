package com.game.util;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.filter.SysThreadData;
import org.jay.frame.filter.ThreadVariable;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.core.CustomThreadData;
import com.game.core.StationType;
import com.game.core.SystemConfig;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;

public class StationUtil {
	/**
	 * 获取当前用户所登陆的站点类别
	 * 
	 * @return
	 */
	public static StationType getStationType() {
		CustomThreadData data = (CustomThreadData) SysUtil.getCustomData();
		if (data == null) {
			return null;
		}
		return data.getStationType();
	}
	
	public static String getGloalStationName() {
		if(StringUtil.isNotEmpty(StationUtil.getDomainStationName())) {
			return StationUtil.getDomainStationName();
		}
		String stationName = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.basic_info_website_name);
		if (StringUtil.isEmpty(stationName)) {
			stationName = StationUtil.getStationName();
		}
		return stationName;
	}

	/**
	 * 获取当前站点默认代理ID
	 * 
	 * @return
	 */
	public static Long getStationAgentId() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getAgentId();
	}

	/**
	 * 获取当前用户所登陆的站点的ID
	 * 
	 * @return
	 */
	public static Long getStationId() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getId();
	}

	/**
	 * 获取当前站点的注册开关
	 * 
	 */
	public static Long getIsRegSwitch(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getRegSwitch();
	}

	/**
	 * 获取当前站点的域名类型
	 * 
	 */
	public static Long getDomainType(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getType();
	}

	/**
	 * 获取当前站点的默认主页
	 * 
	 */
	public static String getDefaultHome(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDefaultHome();
	}
	
	/**
	 * 获取当前域名绑定的站点名称
	 * 
	 */
	public static String getDomainStationName(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDomainStationName();
	}
	
	/**
	 * 获取当前域名绑定的聊天室token
	 * 
	 */
	public static String getDomainPlatToken(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDomainPlatToken();
	}
	
	/**
	 * 获取当前域名绑定的模板
	 * 
	 */
	public static String getDomainFolder(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDomainFolder();
	}

	/**
	 * 获取模板号 优先域名绑定模板
	 *
	 */
	public static String getFirstFolder(){
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDomainFolder()==null?station.getFloder():station.getDomainFolder();
	}


	/**
	 * 是否读取域名模板数据
	 * 
	 */
	public static boolean isDomainFolderSource(){
		String folder = getDomainFolder();
		return StringUtil.isNotEmpty(folder);
	}

	/**
	 * 获取当前域名绑定的默认代理
	 * 
	 * @return
	 */
	public static Long getDomainAgentId() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getDomainAgentId();
	}

	/**
	 * 判断当前访问站点是不是总控后台
	 * 
	 * @return
	 */
	public static boolean isAdminStation() {
		return getStationType() == StationType.ADMIN;
	}

	public static boolean isReceiveStation() {
		return getStationType() == StationType.RECEIVE_DATA;
	}
	/**
	 * 判断当前访问站点是不是客户端
	 * 
	 * @return
	 */
	public static boolean isMemberStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_MEMBER) && getStationType() == StationType.MEMBER;
	}

	/**
	 * 判断当前访问站点是不是租户平台
	 * 
	 * @return
	 */
	public static boolean isAgentStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_AGENT) && getStationType() == StationType.AGENT;
	}

	/**
	 * 判断当前访问站点是不是代理平台
	 * 
	 * @return
	 */
	public static boolean isDailiStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_MEMBER || domainType == SysStationDomain.TYPE_APP) && getStationType() == StationType.DAILI;
	}

	/**
	 * 判断当前访问站点是不是手机端
	 * 
	 * @return
	 */
	public static boolean isMobileStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_MEMBER|| domainType == SysStationDomain.TYPE_APP) && getStationType() == StationType.MOBILE;
	}

	/**
	 * 判断当前访问站点是不是原生手机端
	 * @return
	 */
	public static boolean isNativeStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_MEMBER|| domainType == SysStationDomain.TYPE_APP) && getStationType() == StationType.NATIVE;
	}

	/**
	 * 判断当前访问是不是第三方访问平台API
	 * @return
	 */
	public static boolean isApiStation() {
		Long domainType = getDomainType();
		return (domainType == null || domainType == SysStationDomain.TYPE_MEMBER) && getStationType() == StationType.API;
	}

	
	/**
	 * 获取当前线程访问的站点
	 * 
	 * @return
	 */
	public static SysStation getStation() {
		SysThreadData data = ThreadVariable.get();
		if (data == null) {
			return null;
		}
		CustomThreadData threadData = (CustomThreadData) data.getSysData();
		if (threadData == null) {
			return null;
		}
		return threadData.getStation();
	}
	
	/**
	 * 获取当前线程的模板
	 * 
	 * @return
	 */
	public static String getFolder() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getFloder();
	}

	/**
	 * 获取站点全名
	 * 
	 * @return
	 */
	public static String getStationFullName() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return "(" + station.getFloder() + ")" + station.getName();
	}

	/**
	 * 获取站点别名
	 * 
	 * @return
	 */
	public static String getStationName() {
		SysStation station = getStation();
		if (station == null) {
			return null;
		}
		return station.getName();
	}

	public static boolean isMemberPage() {
		boolean isMemberStation = isMemberStation();
		// 其他操作、判断
		return isMemberStation;
	}

	public static boolean isAgentPage() {
		boolean isAgentStation = isAgentStation();
		// 其他操作、判断
		return isAgentStation;
	}

	public static boolean isDailiPage() {
		boolean isDailiStation = isDailiStation();
		// 其他操作、判断
		return isDailiStation;
	}

	public static boolean isAdminPage() {
		boolean isAdminStation = isAdminStation();
		// 其他操作、判断
		return isAdminStation;
	}

	public static boolean isMobilePage() {
		boolean isMobileStation = isMobileStation();
		// 其他操作、判断
		return isMobileStation;
	}
	
	public static boolean isNativePage() {
		boolean isNativeStation = isNativeStation();
		// 其他操作、判断
		return isNativeStation;
	}
	/**
	 * 获取会员源目录
	 * @return
	 */
	public static String getMemberFolder(){
		String memberFolder = RedisAPI.getCache(SystemConfig.SOURCE_FOLDER_MEMBER_KEY);
		if(StringUtils.isEmpty(memberFolder)){
			memberFolder = SystemConfig.SOURCE_FOLDER_MEMBER;
		}else{
			memberFolder = SystemConfig.SOURCE_FOLDER_MEMBER_2;
		}
		return memberFolder;
	}
	
	public static boolean isMultiAgent() {
		return isMultiAgent(StationUtil.getStationId());
	}
	
	public static boolean isMultiAgent(Long stationId) {
		return "on".equals(StationConfigUtil.get(stationId,StationConfig.onoff_multi_agent));
	}
	 
}
