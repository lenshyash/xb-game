package com.game.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.cglib.beans.BeanCopier;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

public class BeanDtoVoUtils {

    /**
     * TODO  dot ,Do ,entity 相互转换
     * 同：BeanUtils.copyProperties(dtoEntity, newInstance);
     *
     * @param oldClass 原数据--Dto，Vo，entity
     * @param newClass 转换为--Dto，Vo，entity
     * @return
     */
    public static <E> E convert(Object oldClass, Class<E> newClass) {
        // 判断oldClass 是否为空!
        if (oldClass == null) {
            return null;
        }
        // 判断newClass 是否为空
        if (newClass == null) {
            return null;
        }
        try {
            // 创建新的对象实例
            E newInstance = newClass.newInstance();
            // 把原对象数据拷贝到新的对象
            BeanUtils.copyProperties(oldClass, newInstance);
            // 返回新对象
            return newInstance;
        } catch (Exception e) {
            return null;
        }
    }

    //TODO  list<Entity> 集合对象转list<Vo> ( list 循环)
    public static <T, V> List<V> listVo(List<T> oldList, Class<V> v) {
        try {
            List<V> voList = new ArrayList<>();
            for (T t : oldList) {
                V newv = (V) BeanDtoVoUtils.convert(t, v.newInstance().getClass());
                voList.add(newv);
            }
            return voList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //对象转对象
    public static void beanToBean(Object bean1, Object bean2){
        BeanCopier beanCopier = BeanCopier.create(bean1.getClass(), bean2.getClass(), false);
        beanCopier.copy(bean1, bean2, null);
    }

    public static <T, V> List<V> listMap(List<T> oldList, Class<V> v) {
        try {
            List<V> voList = new ArrayList<>();
            for (T t : oldList) {
                Object vn = JSON.parseObject(JSON.toJSONString(t), v.newInstance().getClass());
                voList.add((V) vn);
            }
            return voList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * List<Bean> 转  List<Map>
     * @param list
     * @return
     */
    public static <T> List<Map<String,Object>> listConvert(List<T> list){
        List<Map<String,Object>> list_map=new ArrayList<Map<String,Object>>();
        try {
            for (T t : list) {
                Field[] fields=getAllFields(t);
                Map<String, Object> m = new HashMap<String, Object>();
                for(Field field:fields){
                    try {
                        String keyName=field.getName();
                        if(keyName.equals("serialVersionUID")) {
                            continue;
                        }
                        PropertyDescriptor pd = new PropertyDescriptor(keyName,t.getClass());
                        Method getMethod = pd.getReadMethod();// 获得getter方法
                        Object o = getMethod.invoke(t);// 执行get方法返回一个Object
                        m.put(keyName, o);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                list_map.add(m);
            }
            return list_map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取所有属性 包括父类
     * @param object
     * @return
     */
    public static Field[] getAllFields(Object object){
        Class clazz = object.getClass();
        List<Field> fieldList = new ArrayList<>();
        while (clazz != null){
            fieldList.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        Field[] fields = new Field[fieldList.size()];
        fieldList.toArray(fields);
        return fields;
    }
}
