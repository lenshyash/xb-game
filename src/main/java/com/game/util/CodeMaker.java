package com.game.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;

import com.game.cache.redis.RedisAPI;
import com.game.service.CodeMakerService;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class CodeMaker {
	
	
	private static Logger logger = Logger.getLogger(CodeMaker.class);
	/**
	 * 默认根据
	 */
	public static boolean GENERATE_BY_DB = false;   //generate
	
	public final static int CODE_DB_INDEX = 9;
	
	/**
	 * seq创建n条后，自动保存到数据库
	 */
	public static final int SAVE_SEQ_LIMIT = 10; 
	
	private static CodeMakerService codeMakerService;
	
	public static CodeMakerService getCodeMakerService(){
		if(codeMakerService == null){
			codeMakerService = (CodeMakerService)SpringUtil.getBean("codeMakerServiceImpl");
		}
		return codeMakerService;
	}
 	
	public static String getCode(CodeType ct){
		return  getCode(ct,5);
	}
	
	public static String getCode(CodeType ct, int seqLength) {
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		String cacheKey = ct.getCodeHeader() + sdf.format(now);

		long seq = 0;

		if (GENERATE_BY_DB) {
			seq = getSeqFromDB(ct,cacheKey);
		} else {
			seq = getSeqFromRedis(ct, cacheKey);
//    改成由定时器更新
//			if (ct.check(10)) {
//				getCodeMakerService().updateCode(cacheKey, seq);
//				ct.refreshLastSaveIndex();
//			}
		}
		if (("" + seq).length() >= seqLength) {
			return cacheKey + seq;
		}
		return cacheKey + String.format("%0" + seqLength + "d", seq);
	}
	
	public static void saveSeqCache(CodeType ct,Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		String cacheKey = ct.getCodeHeader() + sdf.format(date);
		
		Jedis jedis = null;
		JedisPool pool = null;
		try {
			pool = RedisAPI.getPool();
			jedis = pool.getResource();
			jedis.select(CODE_DB_INDEX);
			if(!jedis.exists(cacheKey)){
//				logger.info("单号不存在缓存，无需更新");
				return;
			}
			long val = StringUtil.toLong(jedis.get(cacheKey));
			getCodeMakerService().updateCode(cacheKey, val);
			logger.info(ct.name()+"单号更新成功");
		} catch (Exception e) {
			logger.error("更新数据单号缓存失败", e);
		} finally {
			RedisAPI.returnResource(pool, jedis);
		}
	} 
	
	private static long getSeqFromRedis(CodeType ct,String cacheKey){
	    Jedis jedis = null;  
        JedisPool pool = RedisAPI.getPool();
        try {  
            jedis = pool.getResource();  
            jedis.select(CODE_DB_INDEX);
            boolean flag = jedis.exists(cacheKey);
            if(flag == false){//不存在 设置过期时间
            	synchronized (ct) {
            		if(!jedis.exists(cacheKey)){
            			long seq = getSeqFromDB(ct,cacheKey);
            			seq = seq + (SAVE_SEQ_LIMIT * 5);
    	            	jedis.set(cacheKey, seq+"");
    	            	jedis.expire(cacheKey, 24 * 60 * 60);
    	            	ct.addCreateCount();
    	            	return seq;
            		}
            	}
            }
            //自增
            long seq = jedis.incr(cacheKey);
            ct.addCreateCount();
            return seq;
        } finally {  
        	RedisAPI.returnResource(pool, jedis);
        }
	}
	
	private static long getSeqFromDB(CodeType ct,String cacheKey){
		return getCodeMakerService().createNextCode(cacheKey);
	}
	
}
