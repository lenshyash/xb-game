package com.game.util;

import org.apache.commons.lang3.StringUtils;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;

public class LotteryVersionUtils {

	public static Integer resultVersion(Long stationId){
		String template = StationConfigUtil.get(stationId, StationConfig.lottery_template_name);
		if(StringUtils.isEmpty(template)){
			//为空的话默认第一版本
			return BusinessConstant.lottery_identify_V1;
		}
		switch(template){
			case "jiebao":
				return BusinessConstant.lottery_identify_V1;
			case "v2":
			case "v2_1":
				return BusinessConstant.lottery_identify_V2;
			case "v3":
				return BusinessConstant.lottery_identify_V3;
			case "v4":
				return BusinessConstant.lottery_identify_V4;
			case "v5":
				return BusinessConstant.lottery_identify_V5;
			default:
				return BusinessConstant.lottery_identify_V1;
		}
	}
	
	public static boolean isV3OrV5(Long stationId) {
		String template = StationConfigUtil.get(stationId, StationConfig.lottery_template_name);
		if(StringUtils.isEmpty(template)) {
			return false;
		}
		if(StringUtils.equals(template, "v3") || StringUtils.equals(template, "v5")) {
			return true;
		}
		return false;
	}
}
