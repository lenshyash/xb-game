package com.game.util;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;

import com.game.constant.StationConfig;
import com.game.model.SysAccount;
import com.game.service.SysAccountService;

public class SecurityCheckUtil {

	public static void check(Object... objects) {
		String checkString = "";
		for (Object obj : objects) {
			checkString = StringUtil.trim2Empty(obj);
			if (checkString.indexOf("<") != -1) {
				throw new GenericException("输入非法字符!");
			}
		}
	}
	
	public static void checkLastLoginIpRequestIp(Long userId, String requestIp) {
		Long stationId = StationUtil.getStationId();
		boolean onoff = !"off".equals(StationConfigUtil.get(stationId, StationConfig.onoff_check_last_login_ip));
		
		//登录了才判断
		if(userId!=null && StationUtil.isAgentPage() && onoff) {
			SysAccountService accountService = SpringUtil.getBean(SysAccountService.class);
			SysAccount account = accountService.getOne(userId, StationUtil.getStationId());
			String lastLoginIp = account.getLastLoginIp();
			
			if(StringUtil.isNotEmpty(lastLoginIp) && !lastLoginIp.equals(requestIp)) {
				SysUtil.getRequest().getSession().invalidate();
				throw new GenericException("请求IP和最后登录IP不一致，请重新登录系统");
			}
		}
		
		
	}
}
