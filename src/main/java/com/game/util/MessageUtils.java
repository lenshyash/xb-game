package com.game.util;

import com.game.service.SysMessageService;

public class MessageUtils {

	private static SysMessageService messageService;

	public static int getMessageCount() {
		return getService().getMessageCount();
	}

	private static SysMessageService getService() {
		if (messageService == null) {
			messageService = (SysMessageService) SpringUtil.getBean("sysMessageServiceImpl");
		}
		return messageService;
	}
}