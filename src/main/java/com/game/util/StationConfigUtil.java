package com.game.util;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;

import com.game.constant.StationConfig;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigValue;
import com.game.service.AgentBaseConfigService;

public class StationConfigUtil {

	private static AgentBaseConfigService abcService;

	/**
	 * 租户域名访问有效，通过KEY得到当前站点配置项
	 * 
	 * @param key
	 */
	public static String get(StationConfig key) {
		if(StringUtil.equals(key.getPlatform(), AgentBaseConfig.PLATFORM_SYSTEM)){
			return getSys(key);
		}
		long stationId = getStationId();
		return get(stationId, key);
	}
	
	/**
	 * 得到系统配置值
	 * 
	 * @param key
	 */
	public static String getSys(StationConfig key) {
		String k = key.name();
		AgentBaseConfig val = getService().getSysConfigByKey(k);
		if (val == null) {
			return null;
		}
		return val.getInitValue();
	}
	
	private static long getStationId() {
		Long stationId = StationUtil.getStationId();
		if (stationId == null) {
			throw new GenericException("非法请求！");
		}
		return stationId.longValue();
	}

	/**
	 * 总控接口，通过KEY得到当前站点配置项
	 * 
	 * @param stationId
	 * @param key
	 * @return
	 */
	public static String get(long stationId, StationConfig key) {
		return getService().getSettingValueByKey(key.name(), stationId);
	}

	private static AgentBaseConfigService getService() {
		if (abcService == null) {
			abcService = (AgentBaseConfigService) SpringUtil.getBean("agentBaseConfigServiceImpl");
		}
		return abcService;
	}

	/**
	 * 判断取款时间
	 * 
	 * @param startTime
	 * @param endTime
	 * @param data
	 */
	public static String validateWithdrawalTime(String startTime, String endTime) {
		Calendar now = Calendar.getInstance();
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		int startHour = 0;
		if (!StringUtils.isEmpty(startTime)) {
			String[] hm = startTime.split(":");
			if (hm.length == 2) {
				startHour = NumberUtils.toInt(hm[0]);
				start.set(Calendar.HOUR_OF_DAY, startHour);
				start.set(Calendar.MINUTE, NumberUtils.toInt(hm[1]));
			}
		}
		int endHour = 0;
		if (!StringUtils.isEmpty(endTime)) {
			String[] hm = endTime.split(":");
			if (hm.length == 2) {
				endHour = NumberUtils.toInt(hm[0]);
				end.set(Calendar.HOUR_OF_DAY, endHour);
				end.set(Calendar.MINUTE, NumberUtils.toInt(hm[1]));
			}
		}
		if (!((endHour < startHour && (now.compareTo(start) >= 0 || now.compareTo(end) <= 0)) || (endHour > startHour && now.compareTo(start) >= 0 && now.compareTo(end) <= 0))) {
			if (endHour < startHour) {
				return "否(超出取款时间，取款时间：今天" + startTime + " 至 明天" + endTime + ")";
			} else {
				return "否(超出取款时间，取款时间：早上" + startTime + " 至 晚上" + endTime + ")";
			}
		} else {
			return "是";
		}
	}
}
