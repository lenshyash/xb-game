
package com.game.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author LJ-silver
 */
public class IPSeekerUtils
{

        private static Logger logger = LoggerFactory.getLogger(IPSeekerUtils.class);

        /**
         * 从ip的字符串形式得到字节数组形式
         * 
         * @param ip
         *                字符串形式的ip
         * @return 字节数组形式的ip
         */
        public static byte[] getIpByteArrayFromString(String ip)
        {
                byte[] ret = new byte[4];
                java.util.StringTokenizer st = new java.util.StringTokenizer(ip, ".");
                try
                {
                        ret[0] = (byte)(Integer.parseInt(st.nextToken()) & 0xFF);
                        ret[1] = (byte)(Integer.parseInt(st.nextToken()) & 0xFF);
                        ret[2] = (byte)(Integer.parseInt(st.nextToken()) & 0xFF);
                        ret[3] = (byte)(Integer.parseInt(st.nextToken()) & 0xFF);
                }
                catch (Exception e)
                {
                        logger.error("ip=" + ip + "没有对应的地址");
                }
                return ret;
        }

        public static void main(String args[])
        {
                byte[] a = getIpByteArrayFromString(args[0]);
                for(int i = 0; i < a.length; i++)
                        System.out.println(a[i]);
                System.out.println(getIpStringFromBytes(a));
        }

        /**
         * 对原始字符串进行编码转换，如果失败，返回原始的字符串
         * 
         * @param s
         *                原始字符串
         * @param srcEncoding
         *                源编码方式
         * @param destEncoding
         *                目标编码方式
         * @return 转换编码后的字符串，失败返回原始字符串
         */
        public static String getString(String s, String srcEncoding, String destEncoding)
        {
                try
                {
                        return new String(s.getBytes(srcEncoding), destEncoding);
                }
                catch (UnsupportedEncodingException e)
                {
                        return s;
                }
        }

        /**
         * 根据某种编码方式将字节数组转换成字符串
         * 
         * @param b
         *                字节数组
         * @param encoding
         *                编码方式
         * @return 如果encoding不支持，返回一个缺省编码的字符串
         */
        public static String getString(byte[] b, String encoding)
        {
                try
                {
                        return new String(b, encoding);
                }
                catch (UnsupportedEncodingException e)
                {
                        return new String(b);
                }
        }

        /**
         * 根据某种编码方式将字节数组转换成字符串
         * 
         * @param b
         *                字节数组
         * @param offset
         *                要转换的起始位置
         * @param len
         *                要转换的长度
         * @param encoding
         *                编码方式
         * @return 如果encoding不支持，返回一个缺省编码的字符串
         */
        public static String getString(byte[] b, int offset, int len, String encoding)
        {
                try
                {
                        return new String(b, offset, len, encoding);
                }
                catch (UnsupportedEncodingException e)
                {
                        return new String(b, offset, len);
                }
        }

        /**
         * @param ip
         *                ip的字节数组形式
         * @return 字符串形式的ip
         */
        public static String getIpStringFromBytes(byte[] ip)
        {
                StringBuffer sb = new StringBuffer();
                sb.append(ip[0] & 0xFF);
                sb.append('.');
                sb.append(ip[1] & 0xFF);
                sb.append('.');
                sb.append(ip[2] & 0xFF);
                sb.append('.');
                sb.append(ip[3] & 0xFF);
                return sb.toString();
        }

        /**
         * 私有IP：
         * A类  10.0.0.0-10.255.255.255
         * B类  172.16.0.0-172.31.255.255
         * C类  192.168.0.0-192.168.255.255
         *
         * 127这个网段是环回地址
         * localhost
         */
        public static boolean isInnerIP(String ipAddress){
                boolean isInnerIp = false;
                long ipNum = getIpNum(ipAddress);
                long aBegin = getIpNum("10.0.0.0");
                long aEnd = getIpNum("10.255.255.255");
                long bBegin = getIpNum("172.16.0.0");
                long bEnd = getIpNum("172.31.255.255");
                long cBegin = getIpNum("192.168.0.0");
                long cEnd = getIpNum("192.168.255.255");
                isInnerIp = isInner(ipNum,aBegin,aEnd) || isInner(ipNum,bBegin,bEnd) || isInner(ipNum,cBegin,cEnd) || ipAddress.equals("127.0.0.1");
                return isInnerIp;
        }
        private static long getIpNum(String ipAddress) {
                String [] ip = ipAddress.split("\\.");
                long a = Integer.parseInt(ip[0]);
                long b = Integer.parseInt(ip[1]);
                long c = Integer.parseInt(ip[2]);
                long d = Integer.parseInt(ip[3]);

                long ipNum = a * 256 * 256 * 256 + b * 256 * 256 + c * 256 + d;
                return ipNum;
        }
        private static boolean isInner(long userIp,long begin,long end){
                return (userIp>=begin) && (userIp<=end);
        }
}