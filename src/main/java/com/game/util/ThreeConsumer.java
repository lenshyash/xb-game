package com.game.util;

@FunctionalInterface
public interface ThreeConsumer<One, Two, Three> {

    /**
     *
     * @param one
     * @param two
     * @param three
     */
    void accept(One one, Two two, Three three);

}
