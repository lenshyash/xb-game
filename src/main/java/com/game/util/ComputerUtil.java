package com.game.util;

import java.net.InetAddress;

public class ComputerUtil {
	
	/**
	 * 获取内网ip
	 * @return
	 */
	public static String getIP(){
		try{ 
			 InetAddress addr = InetAddress.getLocalHost();  
	         String ip= addr.getHostAddress().toString();//获得本机IP　　
	         return ip;
		}catch(Exception e){
			return null;
		}
	}
   
	/**
	 * 获取计算机名
	 * @return
	 */
	public static String getName(){
		try{ 
			 InetAddress addr = InetAddress.getLocalHost();  
			 String address = addr.getHostName().toString();//获得本机名称  
	         return address;
		}catch(Exception e){
			return null;
		}
	}
}
