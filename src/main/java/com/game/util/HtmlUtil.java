package com.game.util;

import org.jay.frame.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HtmlUtil {
	
	public static String getText(String html){
		Document document = Jsoup.parse(html);
		return StringUtil.trim2Null(document.text());
	}
}
