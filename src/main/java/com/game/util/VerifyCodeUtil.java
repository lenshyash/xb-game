package com.game.util;

import java.awt.Font;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.apache.log4j.Logger;

import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.web.vcode.DefaultVerifyCode;
import com.game.web.vcode.GifCaptcha;
import com.game.web.vcode.HanZiGifCaptcha;
import com.game.web.vcode.kaptcha.KaptchaVerifyCode;

import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

public class VerifyCodeUtil {
	private static final Logger LOGGER = Logger.getLogger(VerifyCodeUtil.class);



	// 验证码KEY
	public static String VERIFY_CODE_KEY = "verify_code_";
	// 验证码过期时间
	public static int VERIFY_CODE_KEY_TIME_OUT = 180;

	// 验证码图片的宽度。
	private static int width = 80;
	// 验证码图片的高度。
	private static int height = 30;
	// 验证码字符个数
	private static int codeCount = 4;

	public static String createVerifyCode(boolean bianKuang, StationConfig sc)
			throws ServletException, java.io.IOException {
		Long stationId = StationUtil.getStationId();
		if (stationId == null) {
			String c = DefaultVerifyCode.createVerifyJPGCode(width, height, codeCount, bianKuang, false, false);
			setVerifyCache(c);
			return c;
		}
		if (sc == null) {
			sc = StationConfig.login_verify_code_type;
		}
		boolean ganRao = true;
		if (sc == StationConfig.register_verify_code_type) {
			HttpServletRequest request = SysUtil.getRequest();
			String uaStr = request.getHeader("User-Agent");
			if (Validator.isNotNull(uaStr)) {
				UserAgent userAgent = UserAgent.parseUserAgentString(uaStr);
				OperatingSystem os = userAgent.getOperatingSystem();
				if (os == null || StringUtils.equals(os.name(), "UNKNOWN")) {
					return null;
				}
			} else {
				return null;
			}
		}

		String type = StationConfigUtil.get(stationId, sc);
		if (StringUtils.isEmpty(type)) {
			type = "a";
		}
		String c = null;
		LOGGER.error("会员登录获取验证码001redisKey获取验证码："+type);
		switch (type) {
		case "a1":// 5字符
			c = DefaultVerifyCode.createVerifyJPGCode(width, height, 5, bianKuang, ganRao, false);
			break;
		case "b":// 纯数字
			c = DefaultVerifyCode.createVerifyJPGCode(width, height, codeCount, bianKuang, ganRao, true);
			break;
		case "b1":// 5纯数字
			c = DefaultVerifyCode.createVerifyJPGCode(width, height, 5, bianKuang, ganRao, true);
			break;
		case "c":// 纯数字扭曲
			c = KaptchaVerifyCode.createNumberVerifyCode();
			break;
		case "c1":// 5纯数字扭曲
			c = KaptchaVerifyCode.createNumber5VerifyCode();
			break;
		case "d":// 扭曲字符
			c = KaptchaVerifyCode.createCharVerifyCode();
			break;
		case "d1":// 扭曲5英文
			c = KaptchaVerifyCode.createChar5VerifyCode();
			break;
		case "zz":// 普通中文
			c = KaptchaVerifyCode.createNormalChineseVerifyCode();
			break;
		case "e":// 扭曲中文
			c = KaptchaVerifyCode.createChineseVerifyCode();
			break;
		case "e1":// 扭曲中文1
			c = KaptchaVerifyCode.createChinese1VerifyCode();
			break;
		case "f":// gif英文
			GifCaptcha g = new GifCaptcha(width, height, 4);
			g.out(UserUtil.getResponse().getOutputStream());
			c = g.text();
			break;
		case "f1":// gif5英文
			g = new GifCaptcha(width, height, 5);
			g.out(UserUtil.getResponse().getOutputStream());
			c = g.text();
			break;
		case "g":// gif中文
			HanZiGifCaptcha hzg = new HanZiGifCaptcha(width, height, codeCount, normalFonts);
			hzg.out(UserUtil.getResponse().getOutputStream());
			c = hzg.text();
			break;
		case "g1":// gif中文1
			hzg = new HanZiGifCaptcha(width, height, codeCount, hardFonts);
			hzg.out(UserUtil.getResponse().getOutputStream());
			c = hzg.text();
			break;
		case "add1":// 加法
			c = DefaultVerifyCode.createVerifyJPGCodeByAdd(width, height, 0, bianKuang, true, true);
			break;
		default:
			c = DefaultVerifyCode.createVerifyJPGCode(width, height, codeCount, bianKuang, ganRao, false);
		}
		LOGGER.error("会员登录获取验证码000redisKey获取验证码："+c);
		setVerifyCache(c);
		return c;
	}

	private static Font[] normalFonts = new Font[] { new Font("SimHei", Font.ITALIC | Font.BOLD, 20),
			new Font("SimSun", Font.ITALIC | Font.BOLD, 20) };

	private static Font[] hardFonts = new Font[] { new Font("AaStrawberrySister", Font.ITALIC | Font.BOLD, 20),
			new Font("AaMissLemon", Font.ITALIC | Font.BOLD, 20),
			new Font("woziku-mmxysm-CN6967", Font.ITALIC | Font.BOLD, 20), new Font("JST", Font.ITALIC | Font.BOLD, 20),
			new Font("STHupo", Font.ITALIC | Font.BOLD, 20),
			new Font("ZoomlaYingXing-A024", Font.ITALIC | Font.BOLD, 20),
			new Font("STXingkai", Font.ITALIC | Font.BOLD, 20) };

	private static void setVerifyCache(String c) {
		String key = getCacheKey();
		RedisAPI.addCache(key, c, VERIFY_CODE_KEY_TIME_OUT);
		RedisAPI.addCache(key + "time", new Date().getTime() + "", VERIFY_CODE_KEY_TIME_OUT);
	}

	private static String getCacheKey() {
		HttpSession session = UserUtil.getSession();
		String sessionId = session.getId();
		System.out.print("the session id when get vcode key = "+sessionId);
		String key = VERIFY_CODE_KEY;
		if (StationUtil.isAdminStation()) {
			key += "admin_";
		} else if (StationUtil.isMemberStation() || StationUtil.isMobileStation()
				||StationUtil.isNativeStation()) {
			key += "member_";
		} else if (StationUtil.isAgentStation()) {
			key += "agent_";
		} else if (StationUtil.isDailiStation()) {
			key += "daili_";
//		} else if (StationUtil.isMobileStation()) {
//			key += "mobile_";
		}
		return key + sessionId;
	}

	public static String getVerifyCode() {
		String key = getCacheKey();
		return RedisAPI.getCache(key);
	}

	public static void isCheckSuccess(String code) throws GenericException {
		String key = getCacheKey();
		String t = RedisAPI.getCache(key + "time");
		LOGGER.error("会员登录获取验证码redisKey--2:"+key+"code:"+code);
		if (t != null) {
			long l = NumberUtils.toLong(t, 0);
			long currentTimeMillis = System.currentTimeMillis();
			if (l != 0 && currentTimeMillis - l < 2500) {
				RedisAPI.delCache(key);
				RedisAPI.delCache(key+"time");
				LOGGER.error("会员登录获取验证码redisKey--3:"+key+"code:"+code+"l"+l);
				throw new GenericException("验证码错误！", false);
			}
		}
		if (StringUtil.isEmpty(code) || !code.equalsIgnoreCase(getVerifyCode())) {
			LOGGER.error("会员登录获取验证码redisKey--4:"+key+"code:"+code+"redisCode:"+getVerifyCode());
			throw new GenericException("验证码错误！", false);
		}
		// 验证完成删除随机码
		LOGGER.error("会员登录获取验证码redisKey--5:"+key+"code:"+code);
		RedisAPI.delCache(key);
		RedisAPI.delCache(key+"time");
	}
}
