package com.game.util.keygen;

/**
 * Key generator interface.
 *
 * @author zhangliang
 */
public interface KeyGenerator {

    /**
     * Generate key.
     *
     * @return generated key
     */
    Number generateKey();
}
