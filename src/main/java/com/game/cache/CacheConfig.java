package com.game.cache;

public class CacheConfig{
	
	public CacheConfig(){}
	
	public CacheConfig(int timeout,int db){
		this.timeout = timeout;
		this.selectDb = db;
	}
	
	public static CacheConfig getDefaultConfig(){
		CacheConfig cfg = new CacheConfig();
		cfg.selectDb = 0;
		cfg.timeout = 0;
		return cfg;
	}
	
	private int selectDb;


	/**
	 * 超时时间 0:不超时  单位秒
	 */
	private int timeout;
	

	public int getSelectDb() {
		return selectDb;
	}
	public void setSelectDb(int selectDb) {
		this.selectDb = selectDb;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}