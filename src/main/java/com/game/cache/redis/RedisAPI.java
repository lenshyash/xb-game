package com.game.cache.redis;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.util.StringUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

public class RedisAPI {

    private static JedisPool pool = null;

    public static int DEFAULT_DB_INDEX = 0;

    public static int LONGEST_TIMEOUT = 3600 * 24 * 7;//最长保存时候为7天

    /**
     * 获取连接池
     *
     * @return
     */
    public static JedisPool getPool() {
        return pool;
    }

    /**
     * 初始化连接池
     *
     * @throws IOException
     */
    public synchronized static void initPool() throws IOException {
        Properties prop = new Properties();
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("redis.properties");
        if (in == null) {
            throw new FileNotFoundException("文件 /WEB-INF/classes/redis.properties 不存在");
        }
        prop.load(in);
        if (pool == null) {
            initPoolPro(1, prop, "redis");
            testConnection(pool, "pool");
        }
    }

    public static void initPoolPro(int index, Properties prop, String prefix) {
        RedisConfig cfg = new RedisConfig();
        String param = prop.getProperty(prefix + ".host");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setHost(param);
        }

        param = prop.getProperty(prefix + ".port");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setPort(Integer.parseInt(param));
        }

        param = prop.getProperty(prefix + ".password");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setPassword(param);
        }

        param = prop.getProperty(prefix + ".default.db");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setDefaultDb(Integer.parseInt(param));
            // defaultDb = Integer.parseInt(param);
        }

        param = prop.getProperty(prefix + ".timeout");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setTimeout(Integer.parseInt(param));
            // timeout = Integer.parseInt(param);
        }

        param = prop.getProperty(prefix + ".maxActive");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setMaxActive(Integer.parseInt(param));
        }

        param = prop.getProperty(prefix + ".maxWait");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setMaxWait(Integer.parseInt(param));
        }

        param = prop.getProperty(prefix + ".maxIdle");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setMaxIdle(Integer.parseInt(param));
        }

        param = prop.getProperty(prefix + ".testOnBorrow");
        if (StringUtil.isNotEmpty(param)) {
            cfg.setTestOnBorrow(Boolean.parseBoolean(param));
        }

        JedisPoolConfig config = new JedisPoolConfig();

        config.setMaxTotal(cfg.getMaxActive());

        config.setMaxIdle(cfg.getMaxIdle());

        config.setMaxWaitMillis(cfg.getMaxWait());

        config.setTestOnBorrow(cfg.isTestOnBorrow());
        switch (index) {
            case 1:
                pool = new JedisPool(config, cfg.getHost(), cfg.getPort(), cfg.getTimeout(), cfg.getPassword(),
                        cfg.getDefaultDb(), null);
                break;
            default:
                break;
        }

    }

    /**
     * 测试连接是否成功
     */
    private static void testConnection(JedisPool pool, String poolName) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            System.out.println("【" + poolName + "连接池创建成功】");
        } finally {
            returnResource(pool, jedis);
        }
    }

    /**
     * 返还到连接池
     *
     * @param pool
     * @param redis
     */
    public static void returnResource(JedisPool pool, Jedis redis) {
        if (redis != null) {
            pool.returnResource(redis);
        }
    }

    /**
     * 获取数据
     *
     * @param key
     * @return
     */
    public static String getCache(String key, int db) {
        return getCache(key, db, pool);
    }

    /**
     * 获取数据
     *
     * @param key
     * @return
     */
    public static String getCache(String key, int db, JedisPool pool) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            value = jedis.get(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return value;
    }

    /**
     * 是否存在KEY
     *
     * @param key
     * @return
     */
    public static boolean exists(String key) {
        return exists(key, DEFAULT_DB_INDEX);
    }

    /**
     * 是否存在KEY
     *
     * @param key
     * @return
     */
    public static boolean exists(String key, int db) {
        boolean value = false;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            value = jedis.exists(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return value;
    }

    public static String getCache(String key) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(DEFAULT_DB_INDEX);
            value = jedis.get(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return value;
    }

    public static String getAndDel(String key) {
        return getAndDel(key, DEFAULT_DB_INDEX);
    }

    public static String getAndDel(String key, int db) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            value = jedis.get(key);
            jedis.del(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return value;
    }

    /**
     * 写入缓存
     *
     * @param key     键
     * @param val     值
     * @param timeout 超时时间单位（秒），0：永不超时
     * @param db      数据
     */
    public static void addCache(String key, String val, int timeout, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            if (timeout <= 0) {
                jedis.setex(key, LONGEST_TIMEOUT, val);
            } else {
                jedis.setex(key, timeout, val);
            }
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static void addCache(String key, String val, Integer timeout) {
        addCache(key, val, timeout, DEFAULT_DB_INDEX);
    }

    public static void operateCache(RedisCallback callback) {
        operateCache(callback, pool);
    }

    public static void operateCache(RedisCallback callback, JedisPool pool) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            callback.execute(jedis);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static Set<String> getCacheList(String keyHeader, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            Set<String> keys = jedis.keys(keyHeader + "*");
            return keys;
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static void delCacheByPrefix(String keyPrefix, int db) {
        List<String> keys = RedisAPI.lrange(keyPrefix, 0, -1);
        if (keys != null && !keys.isEmpty()) {
            String[] arr = new String[keys.size()];
            keys.toArray(arr);
            delCache(db, arr);
        }
        RedisAPI.delCache(db, keyPrefix);
    }

    /**
     * 删除缓存
     *
     * @param db
     * @param keys
     */
    public static void delCache(String... keys) {
        delCache(DEFAULT_DB_INDEX, keys);
    }

    /**
     * 删除缓存
     *
     * @param db
     * @param keys
     */
    public static void delCache(List<String> list) {
        delCache(DEFAULT_DB_INDEX, list);
    }

    /**
     * 删除缓存
     *
     * @param db
     * @param keys
     */
    public static void delCache(int db, String... keys) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.del(keys);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 删除缓存
     *
     * @param db
     * @param list
     */
    public static void delCache(int db, List<String> list) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            for (String keys : list) {
                jedis.del(keys);
            }
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 往LIST增加元素
     *
     * @param key
     * @param value
     * @return
     */
    public static void rpush(String key, String... value) {
        rpush(key, DEFAULT_DB_INDEX, value);
    }

    /**
     * 往LIST增加元素
     *
     * @param key
     * @param value
     * @return
     */
    public static void rpush(String key, int db, String... value) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.rpush(key, value);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 往set增加元素
     *
     * @param key
     * @param value
     * @return
     */
    public static Long sadd(String key, String... value) {
        return sadd(key, DEFAULT_DB_INDEX, value);
    }

    /**
     * 往set增加元素
     *
     * @param key
     * @param value
     * @return
     */
    public static Long sadd(String key, int db, String... value) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            return jedis.sadd(key, value);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 通过KEY取出LIST
     *
     * @param key
     * @param start
     * @param stop
     */
    public static List<String> lrange(String key, int start, int stop) {
        return lrange(key, start, stop, DEFAULT_DB_INDEX);
    }

    /**
     * 通过KEY取出LIST
     *
     * @param key
     * @param start
     * @param stop
     */
    public static List<String> lrange(String key, int start, int stop, int db) {
        List<String> list = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            list = jedis.lrange(key, start, stop);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return list;
    }

    /**
     * 移除LIST的VALUE
     *
     * @param key
     * @param value
     */
    public static void lrem(String key, String value) {
        lrem(key, value, DEFAULT_DB_INDEX);
    }

    /**
     * 移除LIST的VALUE
     *
     * @param key
     * @param value
     */
    public static void lrem(String key, String value, int db) {
        lrem(key, -1, value, db);
    }

    /**
     * 移除LIST的VALUE
     *
     * @param key
     * @param count -1 表示从头到尾的第一个 其他 COUNT为几就移除几个
     * @param value
     * @param db
     */
    public static void lrem(String key, int count, String value, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.lrem(key, count, value);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 取LIST的长度
     *
     * @param key
     */
    public static Long llen(String key) {
        return llen(key, DEFAULT_DB_INDEX);
    }

    /**
     * 移除LIST的VALUE
     *
     * @param key
     */
    public static Long llen(String key, int db) {
        Long len = 0l;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            len = jedis.llen(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return len;
    }

    /**
     * 取出MAP
     *
     * @param key
     */
    public static Map hgetAll(String key) {
        return hgetAll(key, DEFAULT_DB_INDEX);
    }

    /**
     * 取出MAP
     *
     * @param key
     */
    public static Map hgetAll(String key, int db) {
        Map map = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            map = jedis.hgetAll(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return map;
    }

    /**
     * 删除MAP中的元素
     *
     * @param key
     */
    public static void hdel(String key, String... fields) {
        hdel(key, DEFAULT_DB_INDEX, fields);
    }

    /**
     * 删除MAP中的元素
     *
     * @param key
     */
    public static void hdel(String key, int db, String... fields) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.hdel(key, fields);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 累加
     *
     * @param key
     * @param increment
     * @param db
     */
    public static Long incrby(String key, long increment) {
        return incrby(key, increment, DEFAULT_DB_INDEX);
    }

    /**
     * 累加
     *
     * @param key
     * @param increment
     * @param db
     */
    public static Long incrby(String key, long increment, int db) {
        Long after = 0l;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            after = jedis.incrBy(key, increment);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return after;
    }

    /**
     * 累加
     *
     * @param key
     * @param increment
     * @param db
     */
    public static Double incrByFloat(String key, double increment) {
        return incrByFloat(key, increment, DEFAULT_DB_INDEX, 0);
    }

    /**
     * 累加
     *
     * @param key
     * @param increment
     * @param db
     */
    public static Double incrByFloat(String key, double increment, int timeout) {
        return incrByFloat(key, increment, DEFAULT_DB_INDEX, timeout);
    }

    /**
     * 累加
     *
     * @param key
     * @param increment
     * @param db
     */
    public static Double incrByFloat(String key, double increment, int db, int timeout) {
        Double after = 0d;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            after = jedis.incrByFloat(key, increment);
            if (timeout > 0) {
                jedis.expire(key, timeout);
            }
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return after;
    }

    public static void zadd(String key, String value, Double source) {
        zadd(key, value, source, DEFAULT_DB_INDEX);
    }

    public static void zadd(String key, String value, Double source, int db) {
        Map<String, Double> map = new HashMap<String, Double>();
        map.put(value, source);
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.zadd(key, map);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static void expire(String key, int timeout) {
        expire(key, timeout, DEFAULT_DB_INDEX);
    }

    public static void expire(String key, int timeout, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.expire(key, timeout);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static void zrem(String key, String value) {
        zrem(key, value, DEFAULT_DB_INDEX);
    }

    public static void zrem(String key, String value, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.zrem(key, value);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static void srem(String key, int db, String... value) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            jedis.srem(key, value);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    public static Set<String> smembers(String key, int db) {
        Jedis jedis = null;
        Set<String> set = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            set = jedis.smembers(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return set;
    }

    public static Long zcard(String key) {
        return zcard(key, DEFAULT_DB_INDEX);
    }

    public static Long zcard(String key, int db) {
        Long count = 0l;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            count = jedis.zcard(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return count;
    }

    public static Set<String> zrange(String key, long start, long stop) {
        return zrange(key, start, stop, DEFAULT_DB_INDEX);
    }

    public static Set<String> zrange(String key, long start, long stop, int db) {
        Set<String> set = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            set = jedis.zrange(key, start, stop);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return set;
    }

    public static Double zscore(String key, String member) {
        return zscore(key, member, DEFAULT_DB_INDEX);
    }

    public static Double zscore(String key, String member, int db) {
        Double set = null;
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            set = jedis.zscore(key, member);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
        return set;
    }

    public static String lpop(String key) {
        return lpop(key, DEFAULT_DB_INDEX);
    }

    public static String lpop(String key, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            return jedis.lpop(key);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }

    /**
     * 使用管道，批量获取List 里面的值
     *
     * @param key
     * @return
     */
    public static List<String> lpopByPipe(String key) {
        return lpopByPipe(key, DEFAULT_DB_INDEX, 20);
    }

    /**
     * 使用管道，批量获取List 里面的值
     *
     * @param key
     * @param db
     * @param recordNum 记录条数
     * @return
     */
    public static List<String> lpopByPipe(String key, int db, int recordNum) {
        List<String> list = new ArrayList<>();
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            Pipeline pl = jedis.pipelined();
            List<Response<String>> rlist = new ArrayList<>();
            for (int i = 0; i < recordNum; i++) {
                rlist.add(pl.lpop(key));
            }
            pl.sync();
            String value = null;
            for (Response<String> d : rlist) {
                value = d.get();
                if (StringUtils.isNotEmpty(value)) {
                    list.add(value);
                }
            }
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }

        return list;
    }

    public static void executeWithPipeline(Consumer<Pipeline> callback, int db) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            Pipeline pl = jedis.pipelined();
            callback.accept(pl);
            pl.sync();
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }

    }

    public static <T> T eval(String lua, int db, int keyCount, String... params) {
        Jedis jedis = null;
        try {
            jedis = pool.getResource();
            jedis.select(db);
            return (T) jedis.eval(lua, keyCount, params);
        } finally {
            // 返还到连接池
            returnResource(pool, jedis);
        }
    }
}
