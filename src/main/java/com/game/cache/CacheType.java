package com.game.cache;

public enum CacheType {

	/**
	 * 站点域名列表
	 */
	STATION_DOMAIN,

	/**
	 * 菜单栏
	 */
	ADMIN_GROUP_MENU,

	/**
	 * 总控后台用户组别权限缓存
	 */
	ADMIN_GROUP_PERMISSION_MAP,
	/**
	 * 租户后台用户组别权限缓存
	 */
	AGENT_GROUP_PERMISSION_MAP,

	/**
	 * 代理菜单栏
	 */
	AGENT_MENU,
	/**
	 * 代理系统参数设置
	 */
	AGENT_SETTING,

	/**
	 * 真人中心配置缓存
	 */
	REAL_CENTER_CONFIG,

	/**
	 * 真人账户缓存
	 */
	REAL_GAME_USER,
	/**
	 * 二级代理反水策略缓存
	 */
	MEMBER_ROLL_BACK_STRATEGY,
	/**
	 * 二级代理代理返点策略缓存
	 */
	PROXY_REBATE_TACTICS,
	/**
	 * 代理平台未处理充值记录条数
	 */
	AGENT_STATION_COM_COUNT,
	/**
	 * 代理平台未处理提款记录条数
	 */
	AGENT_STATION_DRAW_COUNT,

	/**
	 * 彩种缓存
	 */
	BC_LOTTERY,
	/**
	 * 彩票玩法大类
	 */
	BC_LOTTERY_PLAY_GROUP,
	/**
	 * 彩票玩法小类
	 */
	BC_LOTTERY_PLAY,
	/**
	 * 彩票期号和开奖结果数据
	 */
	BC_LOTTERY_DATA,
	/**
	 * 团队统计结果
	 */
	TEAM_TOTAL_STATISTIC,
	/**
	 * 站点所有会员等级
	 */
	STTAION_MEMBER_LEVEL,
	
	/**
	 * 站点ip白名单
	 */
	STATION_WHITE_IP_LIST,
	/**
	 * 会员等级权限
	 */
	MEMBER_LEVEL_ROLE,
	/**
	 * 会员红包
	 */
	MEMBER_RED_PACKET,
	/**
	 * 会员信息自动处理
	 */
	AUTO_HANDLER_ACCOUNT,
	/**
	 * 会员聊天信息
	 */
	LOTTERY_UIM_LIST_DATA,
	/**
	 * 前端访问ip白名单
	 */
	MEMBER_WHITE_IP_LIST,
	/**
	 * 站点预警用户数
	 */
	AGENT_STATION_ACCOUNT_WARN_COUNT,

	/**
	 * 用户首页数据缓存
	 */
	MEMBER_INDEX_DATA;
	
}
