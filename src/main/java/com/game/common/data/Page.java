package com.game.common.data;

import java.util.List;

public interface Page<T> {

    long getTotal();

    void setTotal(long total);

    List<T> getRecords();

    void setRecords(List<T> records);

    static <T> Page<T> empty() {
        return new PageImpl<>();
    }

    static <T> Page<T> newPage(long total, List<T> records) {
        return new PageImpl<>(total, records);
    }
}
