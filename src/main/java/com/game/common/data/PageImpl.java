package com.game.common.data;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 通用分页结果
 * Created by Administrator on 2018/1/22 0022.
 */
public class PageImpl<T> implements Page<T>, Serializable {

    private static int DEFAULT_PAGE_SIZE = 20;

    private int pageSize = DEFAULT_PAGE_SIZE;

    @Getter
    @Setter
    private long total;

    @Getter
    @Setter
    private List<T> records;

    /**
     * 构造方法，只构造空页.
     */
    public PageImpl() {
        this(0, Collections.emptyList());
    }

    public PageImpl(long total, List<T> records) {
        this.total = total;
        this.records = records;
    }

}
