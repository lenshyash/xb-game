package com.game.common.data.annotation;


import com.game.util.keygen.KeyGenerator;
import com.game.util.keygen.impl.IPKeyGenerator;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

/**
 * Created by Administrator on 2018/1/28 0028.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {FIELD, METHOD, ANNOTATION_TYPE})
public @interface KeyGen {

    Class<? extends KeyGenerator> value() default IPKeyGenerator.class;

}
