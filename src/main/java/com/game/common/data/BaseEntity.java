package com.game.common.data;

import com.alibaba.fastjson.annotation.JSONField;
import com.game.common.data.annotation.CreateAt;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by terry on 2018/1/22.
 */
@Data
public abstract class BaseEntity implements Serializable {


    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @CreateAt
    private Date createAt;
}
