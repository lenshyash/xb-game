package com.game.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyComRecord;
import com.game.model.MnyDrawRecord;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.model.vo.MnyMoneyVo;

public interface MnyDrawRecordService {
	public Page getPage(MnyDrawRecordVo mdrvo);
	public Page getPage2(MnyDrawRecordVo mdrvo);
	/**
	 * 
	 * @param moneyVo
	 */
	public void withdraw(MnyMoneyVo moneyVo);

	/**
	 * 锁定和解锁操作
	 * 
	 * @param drawId
	 * @param lockFlag
	 */
	public void lock(long drawId, long lockFlag);

	/**
	 * 确认取消提款
	 * 
	 * @param drawId
	 */
	public void drawHandler(long drawId, long status, BigDecimal fee, String remark, String paymentPassword);
	/**
	 * 确认取消提款
	 * 
	 * @param drawId
	 */
	public void drawHandler(long drawId, long status, BigDecimal fee, String remark,Long payId,String paymentPassword);

	/**
	 * 获取未处理的提款数量
	 * 
	 * @return
	 */
	public Integer getCountOfUntreated(final long stationId);

	/**
	 * 增加未处理的提款数量
	 * 
	 * @return
	 */
	public void addCountOfUntreated(final long stationId);

	/**
	 * 减去未处理的提款数量
	 * 
	 * @return
	 */
	public void subCountOfUntreated(final long stationId);

	/**
	 * 获取用户今日的提款数次
	 * 
	 * @param accountId
	 * @param stationId
	 * @return
	 */
	public int getCurrentDateMemDrawNum(Long accountId, Long stationId);

	public Page realtslog(HashMap<String, Object> params);

	/**
	 * 会员提款界面信息
	 * 
	 * @param memberId
	 * @return
	 */
	public Map getMemberDrawCheckMap(Long memberId, Long stationId);

	/**
	 * 会员提款记录导出功能
	 */
	public void export(MnyDrawRecordVo mdrvo);

	/**
	 * 会员提款总额
	 * 
	 * @param accountId
	 * @param stationId
	 * @return
	 */
	public Long getDrawTotalMoney(Long accountId, Long stationId);

	public MnyDrawRecord getOne(Long drawId, Long stationId);
	
	/**
	 * 得到所有需要过期处理的提款记录
	 * @return
	 */
	public List<MnyDrawRecord> getUntreatedReocrd(Long stationId);
	
	/**
	 * 提款记录过期处理
	 * @param reocrd
	 */
	public void autoExpiredStatus(MnyDrawRecord reocrd);
	
	
	/**
	 * 撒消成功提款
	 */
	public void cancelHandler(MnyDrawRecordVo mcrVo);
	
	
	
	public MnyDrawRecord getOrderNo(String orderNo);
	
	public void onlinePaymentHandler(MnyDrawRecordVo cvrVo);
	
	//回滚
	public void rollBack(Long comId);
	void drawHandlerNotNeedLock(long drawId, long status, BigDecimal fee, String remark, Long payId);
	void failedRollBack(long drawId);
}
