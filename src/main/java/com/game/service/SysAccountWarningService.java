package com.game.service;

import java.util.Date;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysAccountWarning;
import com.game.model.vo.AccountWarningVo;

public interface SysAccountWarningService {
	/**
	 * 执行异常用户预警统计
	 * @param start
	 * @param stationId
	 */
	void executeUserWarning(Date start, Long stationId);
	
	/**
	 * 获取站点未处理用户数
	 * @param stationId
	 * @return
	 */
	Integer getCountOfUntreated(long stationId);
	/**
	 * 分页
	 * @param accountVo
	 * @return
	 */
	Page<Map> getPage(AccountWarningVo accountVo);
	/**
	 * 修改状态
	 * @param account
	 */
	void updStatus(SysAccountWarning account);
}
