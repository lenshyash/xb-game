package com.game.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.game.cert.CertDomainVo;

public interface SysCertificateService {

	String uploadCertificate(MultipartFile crt1, MultipartFile crt2, MultipartFile keyFile, MultipartFile csrFile,
			Integer certId);

	List<CertDomainVo> getDomains();

	String saveConfig(String data);

}
