package com.game.service;

import java.math.BigDecimal;

public interface BatchOptMoneyService {
	/**
	 * 手动批量添加金额
	 * 
	 * @param accounts
	 * @param money
	 * @param betNumMultiple
	 * @param giftMoney
	 * @param giftBetNumMultiple
	 * @param remark
	 */
	public void batchAddMoney(String accounts, BigDecimal betNumMultiple, BigDecimal giftBetNumMultiple, String remark);

	public void batchSubMoney(String accounts, BigDecimal money, String remark);
}
