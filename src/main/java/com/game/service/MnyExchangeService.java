package com.game.service;

import java.math.BigDecimal;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyExchangeConfig;
import com.game.model.vo.ExchangeConfigVo;

public interface MnyExchangeService {

	public Page getConfigPage(ExchangeConfigVo ecvo);

	public void updStatus(Long id, Long status);

	public void delete(Long id);

	public void saveConfig(MnyExchangeConfig config);

	public List<MnyExchangeConfig> getProgressConfig(ExchangeConfigVo ecvo);
	
	public void exchange(Long typeId,BigDecimal amount);

	public MnyExchangeConfig getOne(Long id, Long stationId);
}
