package com.game.service;

public interface CodeMakerService {
	
	//public int getCode(String code);
	
	public void addCode(String code,long value);
	
	public void updateCode(String code,long value);
	
	public long createNextCode(String code);
	//public int getCode(String code);
}
