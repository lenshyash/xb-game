package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.lottery.BcLottery;
import com.game.model.platform.AgentActivity;
import com.game.model.platform.AgentArticle;
import com.game.model.platform.AgentLunBo;
import com.game.model.platform.AgentWinData;

public interface GetConfigService {

	public List<AgentArticle> getArticle(Integer code, Long stationId);

	public List<AgentActivity> getPreferential(Long stationId);
	public int getUnreadPreferential(Long stationId);
	public void readActivity(Long id);

	public List<Map<String, Object>> getActivityImg(Long stationId);
	
	public AgentActivity getActivityById(Long id);

	public List<AgentLunBo> getLunBo(Long stationId,Integer... code);
	
	public List<AgentWinData> getWinData(Long stationId);

	public List<BcLottery> getLotList(Long stationId);
	
	public Page getArticleList(Integer code,Long stationId);
	
	public AgentArticle getArticleById(Long id);
	
	public List<AgentArticle> getArticle(Integer[] code, Long stationId); 

}
