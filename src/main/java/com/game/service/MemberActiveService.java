package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberActive;
import com.game.model.MemberActiveAward;
import com.game.model.MemberActiveRecord;
import com.game.model.MemberActiveVirtualRecord;
import com.game.model.vo.ActiveRecordVo;
import com.game.model.vo.ActiveVo;

public interface MemberActiveService {

	public Page getPage(ActiveVo activeVo);

	public void saveActive(MemberActive ma);

	public void delActive(ActiveVo activeVo);

	public void updStatus(Long maId, Long status);

	public void saveAwards(Long activeId, List<MemberActiveAward> awards);

	public List<MemberActiveAward> getAwardsByActiveId(Long activeId);

	public MemberActiveAward play(Long activeId, Long memberId);

	public boolean balanceAndRecord(MemberActiveRecord mar);
	
	public boolean scoreAndRecord(MemberActiveRecord mar);

	public List<MemberActiveRecord> getUntreatedRecords();

	public MemberActive getProgressActive(ActiveVo activeVo);

	public Page getAwardRecordPage(ActiveRecordVo recordVo);

	public List<MemberActiveRecord> getAwardRecordList(ActiveRecordVo recordVo);

	public void handlerRecord(Long id, Long status, String remark);

	public MemberActive getOne(Long id, Long stationId);

	public MemberActiveRecord getActiveRecord(Long id, Long stationId);
	
	public Page getAwardVirtualRecordPage(ActiveRecordVo virtualRecordVo);
	
	public void saveVirtualRecord(Integer records,Long station_id,Long activeType ,Long award_type,String productName);
	
	public void delVirtualRecord(Long id);
	
	public void updateVirtualRecord(MemberActiveVirtualRecord memberActiveVirtualRecord);
	
	public MemberActiveVirtualRecord getActiveVirtualRecord(Long id, Long stationId);

}
