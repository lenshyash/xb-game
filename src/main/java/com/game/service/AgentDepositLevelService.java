package com.game.service;

import java.util.List;
import java.util.Set;

import com.game.model.AgentDepositLevel;

public interface AgentDepositLevelService {

	List<AgentDepositLevel> findByDepositId(Long depositId, Long stationId, int type);

	Set<Long> getLevelIdsByDepositId(Long depositId, Long stationId, int type);

	void batchInsert(List<AgentDepositLevel> dllist);

	void batchDelete(Long depositId, Long stationId, int type, List<Long> levelIdList);

	void deleteByDeposiId(Long depositId, Long stationId, int type);

}
