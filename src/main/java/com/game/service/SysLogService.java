package com.game.service;

import java.util.Date;

import org.jay.frame.jdbc.Page;

import com.game.model.SysLog;
import com.game.model.SysLoginLog;
import com.game.model.vo.SysLogVo;
import com.game.model.vo.SysLoginLogVo;

public interface SysLogService {

	public Page getLogsPage(SysLogVo slvo);

	public Page getLoginLogPage(SysLoginLogVo sllvo);

	public SysLog addLog(SysLog log);

	public SysLoginLog addLoginLog(SysLoginLog log);

	public SysLog getOne(Long id, Long stationId);
	
	public SysLog saveLog(SysLog log);
	
	public void exportLoginLog(SysLoginLogVo slvo);
	
	public void exportOperationLog(SysLogVo slvo);
}
