package com.game.service;

import java.util.Set;

public interface MemberBalanceGemStrategyLevelService {

	Set<Long> getLevelSet(Long strategyId, Long stationId);

}
