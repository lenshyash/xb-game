package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentDepositBank;

public interface AgentDepositBankService {
	public Page getBankPage();

	public void save(AgentDepositBank bank,Long[] groupLevelIds);
	
	public List<Map> getStationBanks();
	
	public void delBank(long id);

	public AgentDepositBank getOne(Long id,Long stationId);

	public void updateStatus(Integer status, Long id, Long stationId);
}
