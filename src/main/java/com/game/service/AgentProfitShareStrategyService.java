package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentProfitShareStrategy;

public interface AgentProfitShareStrategyService {

	Page page(Integer type, Long stationId);

	void closeOrOpen(Integer status, Long stationId, Long id);

	AgentProfitShareStrategy getOne(Long id, Long stationId);

	void insert(AgentProfitShareStrategy mrbs);

	void update(AgentProfitShareStrategy mrbs);

	void delete(AgentProfitShareStrategy old);

}
