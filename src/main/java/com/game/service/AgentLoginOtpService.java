package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentLoginOtp;
import com.game.model.AgentLoginOtpEscape;

public interface AgentLoginOtpService {

	Page<AgentLoginOtp> getPage(Long stationId, String iccid);

	List<AgentLoginOtp> find(Long stationId);

	AgentLoginOtpEscape findOneEscape(Long stationId, String account);

	void delete(Long id);

	void save(AgentLoginOtp otp);

	Page<AgentLoginOtpEscape> getEscapePage(Long stationId, String account);

	List<Map> getAgentUsers(Long stationId);

	boolean existOtp(Long stationId);

	void changeEscapeStatus(Long stationId, String account, Boolean esc);
	
	 AgentLoginOtp findGoogleOtp(Long stationId,Long type);

}
