package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.MemberDepositStrategy;

public interface MemberDepositStrategyService {

	Page page(Long stationId);

	void openCloseH(Integer modelStatus, Long id, Long stationId);

	void addSave(MemberDepositStrategy alb);

	void eidtSave(MemberDepositStrategy alb);

	void delete(Long id, Long stationId);

	MemberDepositStrategy getOne(Long id, Long stationId);

}
