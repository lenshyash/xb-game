package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentActivity;

public interface AgentActivityService {

	Page page(Long stationId);

	void openCloseH(Integer modelStatus, Long id, Long stationId);

	void openClosek(Integer status, Long id, Long stationId);

	void addSave(AgentActivity aaty);

	void eidtSave(AgentActivity aaty);

	void delete(Long id, Long stationId);

	AgentActivity getOne(Long id, Long stationId);
	
}
