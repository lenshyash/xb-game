package com.game.service;

import com.game.model.vo.AccountVo;

public interface ExportService {
	public void export(AccountVo avo,Boolean exportAll);
}
