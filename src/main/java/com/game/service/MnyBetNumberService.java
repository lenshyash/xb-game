package com.game.service;

import java.math.BigDecimal;
import java.util.Date;

public interface MnyBetNumberService {

	/**
	 * 增加打码量
	 */
	public void addBetNumber(Long memberId,BigDecimal betNumber, Integer type, String remark, String orderId, Date bizDatetime);
	/**
	 * 系统清0会员打码量
	 * @param accountId
	 */
	public void clearZeroBetNumber(Long accountId);
}
