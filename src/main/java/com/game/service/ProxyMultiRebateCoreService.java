package com.game.service;

import java.math.BigDecimal;
import java.util.Date;

import com.game.model.lottery.BcLotteryOrder;
import com.game.model.platform.ProxyMultiRebateRecord;

public interface ProxyMultiRebateCoreService {
	public Integer updateDateAndAddMoney4MultiRebate(ProxyMultiRebateRecord proxyMultiRebateRecord)throws Exception;
	public Integer updateDateAndSubMoney4MultiRebate(Long betId,String account,Long stationId)throws Exception;
	public BigDecimal getGameShare(Long stationId);
	public void winFoRebateToAgent(BigDecimal baseWinMoney,BigDecimal winMoney,BigDecimal curOdds,BcLotteryOrder order);
	public void flushGameShare(Long agentId);
	public Integer directSuperordinateFixedRebate(ProxyMultiRebateRecord proxyMultiRebateRecord);
}
