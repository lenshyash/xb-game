package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentOnlinepayLog;
import com.game.model.vo.OnlinepayLogVo;

public interface AgentOnlinepayLogService {
	public Page getPage(OnlinepayLogVo onlinepayLogVo);

	public void save(AgentOnlinepayLog model);

	public AgentOnlinepayLog getOnline(long payId);
}
