package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.CommonRedpacket;
import com.game.model.CommonRedpacketRecord;
import com.game.model.vo.CommonRedpacketVo;

public interface CommonRedpacketService {

	public Page<CommonRedpacket> getPage(Long stationId);

	public Page<CommonRedpacketRecord> getRecordPage(String account, Long stationId, Date begin, Date end);

	public CommonRedpacket createRedpacket(CommonRedpacket mrp);

	public Map grabRedpacket(Long redpacketId, Long userId);

	public List<CommonRedpacket> getRedpacketList(Long stationId);

	public List<CommonRedpacketRecord> getMarqueeRecord(Long stationId, Long RedpacketId);

	public CommonRedpacketRecord getRedpacketRecord(Long id, Long stationId);

	public boolean balanceAndRecord(CommonRedpacketRecord record);

	public void delRedpacket(Long id);

	public void initRedpacketRedis(CommonRedpacket rp);

	public List<CommonRedpacketRecord> getUntreatedRecords(Date end, Integer limit);

	public CommonRedpacketVo redpacketInfo(Long redpacketId, Long userId);
	
	public void redpacketExpireReturnMoney(Long stationId) ;

}
