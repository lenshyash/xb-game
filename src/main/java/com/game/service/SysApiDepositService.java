package com.game.service;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.jdbc.Page;

import com.game.model.SysApiDeposit;

public interface SysApiDepositService {

	void deposit(Long stationId, String account, Long accountId, String ip, BigDecimal money, BigDecimal betMultiple,
			String orderId, String remark);

	Page<SysApiDeposit> getPage(String account, Date beginDate, Date endDate, String orderId, Long stationId);

}
