package com.game.service;

import java.util.Map;

public interface DataRecordDeleteService {

	/** 清除各种游戏投注记录 **/
	public Map<Object, Object> bettingLog(Long stationId, Integer timeLen, String type);

	/** 删除帐变记录 **/
	public Map<Object, Object> financeLog(Long stationId, Integer timeLen);

	/**
	 * 删除充值记录
	 */
	public Map<Object, Object> payonlineLog(Long stationId, Integer timeLen);

	/**
	 * 删除提款记录
	 */
	public Map<Object, Object> withdrawLog(Long stationId, Integer timeLen);

	/**
	 * 删除真人转账记录
	 */
	public Map<Object, Object> realchangerdLog(Long stationId, Integer timeLen);

	/**
	 * 删除管理日志记录
	 */
	public Map<Object, Object> operationLog(Long stationId, Integer timeLen);

	/**
	 * 删除登录日志记录
	 */
	public Map<Object, Object> loginLog(Long stationId, Integer timeLen);
	
	/**
	 * 删除报表记录
	 */
	public Map<Object, Object> reportLog(Long stationId, Integer timeLen);
	
	public Map<Object, Object> userMessage(Long stationId, Integer timeLen);
}
