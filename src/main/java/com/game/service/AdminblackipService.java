package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AdminBlackIp;

public interface AdminblackipService {
	
	public Page page(String ip);
	
	public void add(AdminBlackIp adminBlackIp);
	
	public void delete(Long id);
	
	public AdminBlackIp getOne(String ip);
	
	public List<AdminBlackIp> getList();
}
