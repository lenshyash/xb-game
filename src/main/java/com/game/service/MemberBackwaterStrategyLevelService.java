package com.game.service;

import java.util.List;
import java.util.Set;

import com.game.model.MemberBackwaterStrategyLevel;

public interface MemberBackwaterStrategyLevelService {

	List<MemberBackwaterStrategyLevel> findByStrategyId(Long strategyId, Long stationId);

	Set<Long> getLevelIdsByStrategyId(Long strategyId, Long stationId);

	void batchInsert(List<MemberBackwaterStrategyLevel> dllist);

	void batchDelete(Long strategyId, Long stationId, List<Long> levelIdList);

	void deleteByStrategyId(Long strategyId, Long stationId);

}
