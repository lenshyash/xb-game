package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysStation;
import com.game.model.SysStationFolderUrl;
import com.game.model.platform.AgentArticle;
import com.game.model.vo.StationVo;

public interface SysStationFolderUrlService {

	/**
	 * 获取分页列表
	 * @param stationId
	 * @param folder
	 * @return
	 */
	public Page page(Long stationId, String folder);
	/**
	 * 修改记录状态
	 * @param modelStatus
	 * @param id
	 * @param stationId
	 */
	public void openCloseH(Integer modelStatus, Long id, Long stationId);
	/**
	 * 新增记录
	 * @param aacle
	 */
	public void addSave(SysStationFolderUrl aacle);
	/**
	 * 修改记录
	 * @param aacle
	 */
	public void eidtSave(SysStationFolderUrl aacle);
	/**
	 * 删除计记录
	 * @param id
	 * @param stationId
	 */
	public void delete(Long id, Long stationId);
	/**
	 * 获取指定记录
	 * @param id
	 * @return
	 */
	public SysStationFolderUrl getOne(Long id);
	
	/**
	 * 根据模板名称获取记录
	 * @param id
	 * @return
	 */
	public SysStationFolderUrl findByFolder(String folder,Long stationId,int status);
	/**
	 * 检查返回当前可用app下载链接
	 * @return
	 */
	public SysStationFolderUrl checkAppLink();

}
