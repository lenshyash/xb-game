package com.game.service;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.game.model.SysAccount;

public interface ChatService {
	
	public String login();
	
	public void shareOrder(SysAccount user,final JSONObject obj);
	
	public void shareReport(SysAccount user, final JSONObject obj);

	public Map generateChatRoom(Long id, String folder, String domain);
	
	public String getApplicableToken();
}
