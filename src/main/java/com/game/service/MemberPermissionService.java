package com.game.service;

import com.game.model.MemberPermission;

public interface MemberPermissionService {
	
	public MemberPermission getPermission(long accountId);
	
	public void savePermission(MemberPermission permission);
	
	public MemberPermission getPermission(String account);
}
