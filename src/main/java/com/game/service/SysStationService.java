package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysStation;
import com.game.model.vo.StationVo;

public interface SysStationService {

	public List<Map> getStationCombo(Long status);

	public Page<Map> getPage(StationVo svo);

	public void saveStation(SysStation station, String pwd, String rpwd);

	public void updStatus(Long stationId, Long status);

	public List<SysStation> getAllActive();
	
	public SysStation findOneById(Long stationId);
	
	public void deleteStationById(Long stationId);


    SysStation findOneByFolder(String folder);
}
