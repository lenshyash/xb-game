package com.game.service;

import com.game.model.lottery.BcLotteryOrder;
import com.game.model.sports.SportBettingOrder;

public interface OrderDetailService {
	public BcLotteryOrder getLotteryOrderDetail(String orderId, String account, Long stationId, String lotCode);

	public BcLotteryOrder getLotteryOrderDetailForList(String orderId, String account, Long stationId, String lotCode);

	public SportBettingOrder getSportOrderDetail(String bettingCode, Long stationId);
}
