package com.game.service.impl;

import org.apache.commons.lang3.RandomUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.controller.phone.enumerate.UserHeadInfo;
import com.game.dao.platform.AgentWinDataDao;
import com.game.model.platform.AgentWinData;
import com.game.service.AgentWinDataService;

@Repository
public class AgentWinDataServiceImpl implements AgentWinDataService{
	
	@Autowired
	AgentWinDataDao agentWinDataDao;
	
	@Override
	public Page page(Long stationId){
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentWinDataDao.page(stationId);
	}

	@Override
	public void addSave(AgentWinData awd) {
		if (awd.getStationId() == null) {
			throw new GenericException("参数错误");
		}
		int n = RandomUtils.nextInt(1, 100);
		awd.setHeadUrl(UserHeadInfo.getEmojiInfo("h"+n).getUrl());
		agentWinDataDao.save(awd);
	}

	@Override
	public void delete(Long id, Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		agentWinDataDao.delete(id, stationId);
	}
}
