package com.game.service.impl;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MemberPermissionDao;
import com.game.dao.SysAccountDao;
import com.game.model.MemberPermission;
import com.game.model.SysAccount;
import com.game.service.MemberPermissionService;
import com.game.util.StationUtil;

@Repository
public class MemberPermissionServiceImpl implements MemberPermissionService{

	@Autowired
	private MemberPermissionDao permissionDao;
	
	@Autowired
	private SysAccountDao accountDao;
	
	@Override
	public MemberPermission getPermission(long accountId) {
		return permissionDao.get(accountId);
	}
	
	public MemberPermission getPermission(String account) {
		SysAccount member = accountDao.getByAccountAndTypeAndStationId(account, StationUtil.getStationId(),null);
		if(member == null || member.getAccountType()==SysAccount.ACCOUNT_PLATFORM_GUIDE){
			throw new GenericException("会员["+account+"]不存在");
		}
		MemberPermission mp	= permissionDao.get(member.getId());
		if(mp == null){
			mp = new MemberPermission();
			mp.setAccountId(member.getId());
			//设置默认值
			mp.setRealGameTransfer(MemberPermission.COMMON_YES);
		}else{
			if(mp.getRealGameTransfer() == null){
				mp.setRealGameTransfer(MemberPermission.COMMON_YES);
			}
		}
		return mp;
	}

	@Override
	public void savePermission(MemberPermission permission) {
		long accountId = permission.getAccountId();
		SysAccount account = accountDao.getFromCache(accountId);
		if(account == null){
			throw new GenericException("会员不存在");
		}
		if(!StringUtil.equals(account.getStationId(),StationUtil.getStationId())){
			throw new GenericException("会员不存在");
		}
		if(permissionDao.get(accountId) != null){
			permissionDao.updateNotNull(permission);
		}else{
			permissionDao.insert(permission);
		}
	}
}
