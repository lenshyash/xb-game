package com.game.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.AgentDepositOnlineDao;
import com.game.dao.AgentDepositVirtaulDao;
import com.game.dao.SysAccountDao;
import com.game.model.AgentDepositLevel;
import com.game.model.AgentDepositOnline;
import com.game.model.AgentDepositVirtual;
import com.game.model.SysAccount;
import com.game.model.SysPayPlatform;
import com.game.service.AgentDepositLevelService;
import com.game.service.AgentDepositOnlineService;
import com.game.service.AgentDepositvirtualService;
import com.game.service.SysPayPlatformService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentDepositVirtualServiceImpl implements AgentDepositvirtualService {

	@Autowired
	private AgentDepositVirtaulDao adoDao;

	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private AgentDepositLevelService depositLevelService;
	@Autowired
	private SysPayPlatformService payPlatformService;
	
	@Override
	public Page getOnlinePage(String name, Long status) {
		return adoDao.getPage(name,status);
	}

	@Override
	public void save(AgentDepositVirtual virtual, Long[] groupLevelIds) {
		if (virtual == null) {
			throw new GenericException("数据异常");
		}
		virtual.setMerchantKey(clearEmptyStr(virtual.getMerchantKey()));
		virtual.setMerchantCode(clearEmptyStr(virtual.getMerchantCode()));
		virtual.setUrl(clearEmptyStr(virtual.getUrl()));
		virtual.setIcon(clearEmptyStr(virtual.getIcon()));
		virtual.setPayGetway(clearEmptyStr(virtual.getPayGetway()));
		virtual.setAccount(clearEmptyStr(virtual.getAccount()));
		virtual.setAppid(clearEmptyStr(virtual.getAppid()));
		virtual.setPayStarttime(virtual.getPayStarttime());
		virtual.setPayEndtime(virtual.getPayEndtime());
		virtual.setPayChannel(clearEmptyStr(virtual.getPayChannel()));
		Set<Long> levelIdSet = null;
		Long stationId = StationUtil.getStationId();
		if (virtual.getId() != null && virtual.getId() > 0) {
			AgentDepositVirtual saveVirtaul = adoDao.get(virtual.getId());
			saveVirtaul.setAccount(virtual.getAccount());
			saveVirtaul.setMerchantCode(virtual.getMerchantCode());
			saveVirtaul.setMerchantKey(virtual.getMerchantKey());
			saveVirtaul.setUrl(virtual.getUrl());
			saveVirtaul.setMin(virtual.getMin());
			saveVirtaul.setMax(virtual.getMax());
			saveVirtaul.setStatus(virtual.getStatus());
			saveVirtaul.setPayPlatformId(virtual.getPayPlatformId());
			saveVirtaul.setIcon(virtual.getIcon());
			saveVirtaul.setPayGetway(virtual.getPayGetway());
			saveVirtaul.setAppid(virtual.getAppid());
			saveVirtaul.setSortNo(virtual.getSortNo());
			saveVirtaul.setShowType(virtual.getShowType());
			saveVirtaul.setPayDesc(virtual.getPayDesc());
			saveVirtaul.setPayStarttime(virtual.getPayStarttime());
			saveVirtaul.setPayEndtime(virtual.getPayEndtime());
			saveVirtaul.setPayChannel(virtual.getPayChannel());
			saveVirtaul.setRate(virtual.getRate());
			SysLogUtil.log("修改虚拟充值：" + saveVirtaul.getMerchantCode() + "  支付端账号：" + saveVirtaul.getAccount(),
					LogType.AGENT_FINANCE);
			adoDao.update(saveVirtaul);
			levelIdSet = depositLevelService.getLevelIdsByDepositId(saveVirtaul.getId(), stationId,
					AgentDepositLevel.TYPE_VIRTAUL);
		} else {
			virtual.setStationId(StationUtil.getStationId());
			SysLogUtil.log("添加虚拟充值：" + virtual.getMerchantCode() + "  支付端账号：" + virtual.getAccount(),
					LogType.AGENT_FINANCE);
			adoDao.insert(virtual);
		}
		if (groupLevelIds != null) {
			List<AgentDepositLevel> dllist = new ArrayList<>();
			AgentDepositLevel dl = null;
			for (Long lid : groupLevelIds) {
				if (levelIdSet != null && levelIdSet.contains(lid)) {
					levelIdSet.remove(lid);
					continue;
				}
				dl = new AgentDepositLevel();
				dl.setDepositId(virtual.getId());
				dl.setStationId(stationId);
				dl.setMemberLevelId(lid);
				dl.setType(AgentDepositLevel.TYPE_VIRTAUL);
				dllist.add(dl);
			}
			depositLevelService.batchInsert(dllist);
			if (levelIdSet != null && !levelIdSet.isEmpty()) {
				depositLevelService.batchDelete(virtual.getId(), stationId, AgentDepositLevel.TYPE_VIRTAUL,
						new ArrayList<>(levelIdSet));
			}
		}
	}

	private String clearEmptyStr(String str) {
		if (str == null) {
			return "";
		}
		str = str.trim();
		str = str.replaceAll(" ", "");
		str = str.replaceAll("\n", "");
		str = str.replaceAll("\r", "");
		return str;
	}

	/**
	 * 是否显示引导账号充值的判断
	 * @param payEnum
	 * @param string
	 * @return
	 */
	private boolean isShowGuestPayment(Enum payEnum) {
		Boolean flag = true;

		String string = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.guide_show_payment);

		Boolean guest = UserUtil.isGuestAccount();
		// 是引导账号
		if (guest) {
			// 有配置东西
			if (StringUtil.isNotEmpty(string)) {
				if (!string.contains("1") && payEnum.equals(StationConfig.onoff_show_pay_third)) {
					flag = false;
				} else if (!string.contains("2") && payEnum.equals(StationConfig.onoff_show_pay_quick)) {
					flag = false;
				} else if (!string.contains("3") && payEnum.equals(StationConfig.onoff_show_pay_normal)) {
					flag = false;
				}
			} else {
				//没选的情况
				flag = false;
			}
		}
		return flag;
	}
	
	private Long getLong(Object val) {
		if (val instanceof Integer) {
			return new Long((Integer) val);
		}
		if (val instanceof Long) {
			return (Long) val;
		}
		return null;
	}

	@Override
	public void delOnline(long id) {
		AgentDepositVirtual virtual = adoDao.get(id);
		SysPayPlatform payPlatform = payPlatformService.getPayPlatform(virtual.getPayPlatformId());
		adoDao.delete(id);
		depositLevelService.deleteByDeposiId(id,StationUtil.getStationId(),AgentDepositLevel.TYPE_VIRTAUL);
		SysLogUtil.log("删除虚拟支付：" + payPlatform.getName(),
				LogType.AGENT_FINANCE);
	}

	@Override
	public AgentDepositVirtual getOnline(long payId) {
		return adoDao.get(payId);
	}

	@Override
	public void updateStatus(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		adoDao.updateStatus(status, id, stationId);
	}
	
	@Override
	public List<AgentDepositVirtual> getVirtuals(Long status){
		return adoDao.getVirtuals(status);
	}

	@Override
	public List<Map> getStationOnlines(String showType) {
		return getStationOnlines(showType,null);
	}

	@Override
	public List<Map> getStationOnlines(String showType,String code) {
		List<Map> onlinePays = adoDao.getOnlines(showType,code);
		List<Map> newOP = new ArrayList();
		Object payStartTime = "";
		Object payEndTime = "";
		Date startTime = null;
		Date endTime = null;
		Date now = new Date();
		String curDateStr = DateUtil.getCurrentDate();
		for (Map map : onlinePays) {
			payStartTime = map.get("payStarttime");
			payEndTime = map.get("payEndtime");
			if(StringUtil.isEmpty(payStartTime) && StringUtil.isEmpty(payEndTime)) {
				newOP.add(map);
				continue;
			}
			if(StringUtil.isNotEmpty(payStartTime) ) {
				startTime = DateUtil.parseDate(curDateStr +" "+payStartTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(StringUtil.isNotEmpty(payEndTime) ) {
				endTime = DateUtil.parseDate(curDateStr +" "+payEndTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(startTime == null && endTime == null) {
				newOP.add(map);
				continue;
			}
			if(startTime == null && now.before(endTime)) {
				newOP.add(map);
				continue;
			}

			if(endTime == null && now.after(startTime)) {
				newOP.add(map);
				continue;
			}
			if(now.after(startTime) && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
		}

		boolean show = isShowGuestPayment(StationConfig.onoff_show_pay_virtual);

		List<Map> resultPays = new ArrayList<Map>();
		Set<Long> levelIdSet = null;
		SysAccount user = accountDao.get(UserUtil.getUserId());
		for (Map pay : newOP) {
			levelIdSet = depositLevelService.getLevelIdsByDepositId(getLong(pay.get("id")), user.getStationId(),
					AgentDepositLevel.TYPE_ONLINE);
			if ((levelIdSet == null || levelIdSet.isEmpty() || levelIdSet.contains(user.getLevelGroup())) && show) {
				resultPays.add(pay);
			}
		}

		return resultPays;
	}
}
