package com.game.service.impl;

import java.util.Calendar;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.MnyComRecordDao;
import com.game.dao.MnyDrawRecordDao;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysLogDao;
import com.game.dao.SysLoginLogDao;
import com.game.dao.SysStationDao;
import com.game.dao.SysUserMessageDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.dao.sports.SportBettingOrderDao;
import com.game.dao.third.RealGameTransLogDao;
import com.game.service.DataRecordDeleteService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class DataRecordDeleteServiceImpl implements DataRecordDeleteService {

	@Autowired
	private BcLotteryOrderDao bcOrderDao;// 彩票投注记录
	@Autowired
	private SportBettingOrderDao sbOrderDao;// 体育投注记录
	@Autowired
	private MnyMoneyRecordDao mmrDao;// 帐变记录
	@Autowired
	private MnyComRecordDao mcrDao;// 充值记录
	@Autowired
	private MnyDrawRecordDao mdrDao;// 提款记录
	@Autowired
	private SysLogDao logDao; // 管理日志
	@Autowired
	private SysLoginLogDao loginLogDao;// 登录日志
	@Autowired
	private SysAccountDailyMoneyDao dailyDao;// 报表日志
	@Autowired
	private RealGameTransLogDao realGameTransLogDao;
	@Autowired
	private SysStationDao stationDao;
	@Autowired
	private SysMessageServiceImpl messageServiceImpl;
	
	public Map<Object, Object> bettingLog(Long stationId, Integer timeLen, String type) {
		if (stationId == null || timeLen == null || StringUtils.isEmpty(type) || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = 0;// 被删除条数
		String logType = "";
		switch (type) {
		case "lottery":// 彩票
			count = bcOrderDao.delByCreateTimeAndStationId(c.getTime(), stationId);
			logType = "彩票";
			break;
		case "sport":// 体育
			count = sbOrderDao.delByCreateTimeAndStationId(c.getTime(), stationId);
			logType = "体育";
			break;
		}
		if (count <= 0) {
			throw new ParameterException("没有可删除的数据!");
		}
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的"+logType+"记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	/**
	 * 删除帐变记录
	 * 
	 * @return
	 */
	public Map<Object, Object> financeLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		int count = mmrDao.delByCreateTimeAndStationId(c.getTime(), stationId);
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的帐变记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	/**
	 * 删除充值记录
	 */
	public Map<Object, Object> payonlineLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = mcrDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的充值记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	/**
	 * 删除提款记录
	 */
	public Map<Object, Object> withdrawLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = mdrDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的提款记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	/**
	 * 删除真人转账记录
	 */
	public Map<Object, Object> realchangerdLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = realGameTransLogDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		if (count <= 0) {
			throw new ParameterException("没有可删除的数据!");
		}
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的真人转账记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	@Override
	public Map<Object, Object> operationLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = logDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		if (count <= 0) {
			throw new ParameterException("没有可删除的数据!");
		}
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的管理日志记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	@Override
	public Map<Object, Object> loginLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = loginLogDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		if (count <= 0) {
			throw new ParameterException("没有可删除的数据!");
		}
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的登录记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}

	@Override
	public Map<Object, Object> reportLog(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		Integer count = dailyDao.delByCreateTimeAndStationId(c.getTime(), stationId);// 被删除条数
		if (count <= 0) {
			throw new ParameterException("没有可删除的数据!");
		}
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的报表记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}
	
	@Override
	public Map<Object, Object> userMessage(Long stationId, Integer timeLen) {
		if (stationId == null || timeLen == null || timeLen < 0 || timeLen > 6) {
			throw new ParameterException("参数异常");
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -timeLen);
		messageServiceImpl.deleteSysUserMessageByDate(c.getTime());
		int count = 0;
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationDao.get(stationId).getFloder()+"的站内信记录", LogType.ADMIN_PLATFORM);
		return MixUtil.newHashMap("success", true, "msg", count + "条投注记录已被删除!");
	}
}
