package com.game.service.impl;

import java.util.List;
import java.util.Objects;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.game.constant.LogType;
import com.game.dao.AgentMenuGroupDao;
import com.game.dao.AgentUserGroupDao;
import com.game.model.AgentUserGroup;
import com.game.model.SysAccount;
import com.game.service.AgentUserGroupService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentUserGroupServiceImpl implements AgentUserGroupService {

	@Autowired
	private AgentUserGroupDao userGroupDao;

	@Autowired
	private AgentMenuGroupDao menuGroupDao;

	@Override
	public Page<AgentUserGroup> getGroups() {
		if (UserUtil.isSuperAgent()) {
			return userGroupDao.getGroups(StationUtil.getStationId(), false);
		}
		return userGroupDao.getGroups(StationUtil.getStationId(), true);
	}

	@Override
	public void saveGroup(AgentUserGroup group) {

		if (StringUtil.isEmpty(group.getName())) {
			throw new GenericException("组别名称不能为空！");
		}

		if (userGroupDao.isNotUnique(group, "name,stationId")) {
			throw new GenericException("已经存在名为：\"" + group.getName() + "\"的用户组别！");
		}
		if (Validator.isNotNull(group.getId())) {
			AgentUserGroup old = userGroupDao.get(group.getId());
			old.setName(group.getName());
			if (UserUtil.isSuperAgent()) {
				old.setType(group.getType());
			}
			userGroupDao.update(old);
			SysLogUtil.log("修改用户组别:" + group.getName(), LogType.AGENT_SYSTEM);
		} else {
			group.setStationId(StationUtil.getStationId());
			if (!UserUtil.isSuperAgent()) {
				group.setType(AgentUserGroup.TYPE_EDITABLE);
			}
			userGroupDao.insert(group);
			SysLogUtil.log("新增用户组别:" + group.getName(), LogType.AGENT_SYSTEM);
		}
	}

	@Override
	public AgentUserGroup getGroupById(Long groupId) {
		return userGroupDao.getGroupById(groupId);
	}

	@Override
	public List<AgentUserGroup> getGroupCombo(boolean setPerm) {
		if (UserUtil.isSuperAgent()) {
			return userGroupDao.getGroupAll(StationUtil.getStationId(), null);
		}
		if (setPerm) {
			return userGroupDao.getGroupAll(StationUtil.getStationId(), AgentUserGroup.TYPE_EDITABLE);
		} else {
			return userGroupDao.getGroupAll(StationUtil.getStationId(), AgentUserGroup.TYPE_EDITABLE,
					AgentUserGroup.TYPE_SHOWED);
		}
	}

	@Override
	public void del(Long id) {
		if (!StringUtils.isEmpty(id) && id > 0) {
			AgentUserGroup g = userGroupDao.getGroupById(id);
			if (g == null || !Objects.equals(g.getStationId(), StationUtil.getStationId())) {
				throw new GenericException("记录不存在!");
			}
			SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
			if (Objects.equals(g.getId(), acc.getGroupId())) {
				throw new GenericException("当前用户的分组不能删除!");
			}
			if (userGroupDao.delete(id) == 1) {
				menuGroupDao.deleteByGroupId(g.getStationId(), id);
				SysLogUtil.log("删除用户组别:" + g.getName(), LogType.AGENT_SYSTEM);
			}
		} else {
			throw new GenericException("参数不正确!");
		}

	}

}
