package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.MnyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.dao.SysStationDao;
import com.game.dao.SysStationFolderUrlDao;
import com.game.event.SysStationSaveEvent;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.model.SysStationFolderUrl;
import com.game.model.platform.AgentArticle;
import com.game.model.vo.StationVo;
import com.game.service.SysStationFolderUrlService;
import com.game.service.SysStationService;
import com.game.util.MD5Util;
import com.game.util.MemberLevelUtil;
import com.game.util.SpringUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;
import com.game.util.ValidateUtil;

@Repository
public class SysStationFolderUrlServiceImpl implements SysStationFolderUrlService {

	@Autowired
	private SysStationFolderUrlDao folderUrlDao;

	@Override
	public Page page(Long stationId, String folder) {
		// TODO Auto-generated method stub
		return folderUrlDao.page(stationId,folder);
	}
	@Override
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		if (id == null || id <= 0 || modelStatus == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		folderUrlDao.openCloseH(modelStatus, id, stationId);
		
	}
	@Override
	public void addSave(SysStationFolderUrl folderUrl) {
		if (folderUrl.getFolder() == null || folderUrl.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		SysStationFolderUrl url = new SysStationFolderUrl();
		url.setFolder(folderUrl.getFolder());
		url.setStationId(folderUrl.getStationId());
		if(folderUrlDao.isNotUnique(url, "stationId,folder")){
			throw new GenericException("该模板已经绑定过链接");
		}
		folderUrlDao.save(folderUrl);
		SysLogUtil.log("配置模板为"+folderUrl.getFolder()+"的app链接", LogType.AGENT_SYSTEM);
	}
	@Override
	public void eidtSave(SysStationFolderUrl folderUrl) {
		if (folderUrl.getId() == null || folderUrl.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		
		SysStationFolderUrl fu = folderUrlDao.get(folderUrl.getId());
		fu.setAndroidDlUrl(folderUrl.getAndroidDlUrl());
		fu.setAndroidQrUrl(folderUrl.getAndroidQrUrl());
		fu.setIosDlUrl(folderUrl.getIosDlUrl());
		fu.setIosQrUrl(folderUrl.getIosQrUrl());
		fu.setStatus(folderUrl.getStatus());
		fu.setKfUrl(folderUrl.getKfUrl());
		folderUrlDao.update(fu);
		SysLogUtil.log("修改模板为"+folderUrl.getFolder()+"的app链接", LogType.AGENT_SYSTEM);
	}
	@Override
	public void delete(Long id, Long stationId) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		// TODO Auto-generated method stub
		folderUrlDao.delete(id,stationId);
	}
	@Override
	public SysStationFolderUrl getOne(Long id) {
		// TODO Auto-generated method stub
		return folderUrlDao.get(id);
	}
	@Override
	public SysStationFolderUrl findByFolder(String folder,Long stationId,int status) {
		// TODO Auto-generated method stub
		return folderUrlDao.findByFolder(folder,stationId,status);
	}
	@Override
	public SysStationFolderUrl checkAppLink() {
		//获取当前域名的模板名称及站点信息
		String folder = StationUtil.getDomainFolder();
		Long stationId = StationUtil.getStationId();
		//定义链接
		String appDownloadLinkIos = "";
		String appDownloadLinkAndroid = "";
		String appQRCodeLinkIos = "";
		String appQRCodeLinkAndroid = "";
		//赋值公用模板下载链接
		appDownloadLinkIos = StationConfigUtil.get(StationConfig.app_download_link_ios);
		appDownloadLinkAndroid = StationConfigUtil.get(StationConfig.app_download_link_android);
		appQRCodeLinkIos = StationConfigUtil.get(StationConfig.app_qr_code_link_ios);
		appQRCodeLinkAndroid = StationConfigUtil.get(StationConfig.app_qr_code_link_android);
		//检查是否配置模板指定下载链接
		SysStationFolderUrl link = folderUrlDao.findByFolder(folder, stationId,2); 
		if(link!=null){
			if(StringUtil.isNotEmpty(link.getAndroidDlUrl())){
				appDownloadLinkAndroid = link.getAndroidDlUrl();
			}
			if(StringUtil.isNotEmpty(link.getAndroidQrUrl())){
				appQRCodeLinkAndroid = link.getAndroidQrUrl();
			}
			if(StringUtil.isNotEmpty(link.getIosDlUrl())){
				appDownloadLinkIos = link.getIosDlUrl();
			}
			if(StringUtil.isNotEmpty(link.getIosQrUrl())){
				appQRCodeLinkIos = link.getIosQrUrl();
			}
		}
		SysStationFolderUrl result = new SysStationFolderUrl();
		result.setAndroidDlUrl(appDownloadLinkAndroid);
		result.setAndroidQrUrl(appQRCodeLinkAndroid);
		result.setIosDlUrl(appDownloadLinkIos);
		result.setIosQrUrl(appQRCodeLinkIos);
		return result;
	}
}