package com.game.service.impl;

import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.SysAccountDao;
import com.game.dao.SysLogDao;
import com.game.dao.StationHandMoneyDao;
import com.game.model.SysAccount;
import com.game.model.SysLog;
import com.game.model.SysLoginLog;
import com.game.model.vo.AccountVo;
import com.game.model.vo.SysLogVo;
import com.game.model.vo.SysLoginLogVo;
import com.game.service.StationHandMoneyService;
import com.game.service.SysLogService;
import com.game.util.IPSeeker;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class StationHandMoneyServiceImpl implements StationHandMoneyService {

	@Autowired
	private StationHandMoneyDao stationHandMoneyDao;
	
	@Autowired
	private SysAccountDao sysAccountDao;



	/**
	 * 
	 * 查询站点手动加款数据
	 * 
	 * SysLoginLogVo
	 */
	@Override
	public Page getStaionHandMoneyData(SysLoginLogVo slvo) {
		return stationHandMoneyDao.getStaionHandMoneyData(slvo);
	}




}
