package com.game.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.poi.hssf.record.chart.BeginRecord;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.dao.QzGroupReportDao;
import com.game.service.QzGroupReportService;
import com.game.util.DateUtil;
import com.game.util.UserUtil;

@Service
public class QzGroupReportServiceImpl implements QzGroupReportService{
	@Autowired
	private QzGroupReportDao qzGroupReportDao;

	@Override
	public Page<Map> getGroupPage(Long stationId,String lotCode,String groupName,String qihao,Date beign,Date end) {
		
		String key = getCacheKey(stationId,lotCode,groupName,qihao,beign,end);

		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);

		if (jsons != null) {
			return JsonUtil.toBean(jsons, Page.class);
		}
		
		Page page = qzGroupReportDao.page(stationId, lotCode, groupName,qihao,beign,end);
		
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		
		return page;
	}
	/*
	 * 拼接rediskey
	 */
	public String getCacheKey(Long stationId,String lotCode,String groupName,String qihao,Date beign,Date end){
		StringBuilder sb = new StringBuilder();
		sb.append(stationId);
		sb.append("_group_report_");
		sb.append("_"+lotCode+"_");
		sb.append("_"+groupName+"_");
		sb.append("_"+qihao+"_");
		sb.append("_"+DateUtil.toDatetimeStr(beign)+"_");
		sb.append("_"+DateUtil.toDatetimeStr(end)+"_");
		
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(UserUtil.getRequest());
		int pageNo = webParam.getPageNo();
		int pageSize = webParam.getPageSize();
		if (pageNo > 0) {
			sb.append("_pn_").append(pageNo);
		}
		if (pageSize > 0) {
			sb.append("_ps_").append(pageSize);
		}
		
		return sb.toString();
	}
}
