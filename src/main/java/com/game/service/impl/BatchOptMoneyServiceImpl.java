package com.game.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.game.model.SysAccount;
import com.game.service.BatchOptMoneyService;
import com.game.service.MnyComRecordService;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.StationUtil;

@Service
public class BatchOptMoneyServiceImpl implements BatchOptMoneyService {
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private MnyComRecordService mnyComRecordService;

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public void batchAddMoney(String accounts, BigDecimal betNumMultiple, BigDecimal giftBetNumMultiple,
			String remark) {
		if (StringUtils.isEmpty(accounts)) {
			throw new GenericException("请输入会员账号");
		}
		String[] accs = accounts.split("\n");
		SysAccount account = null;
		Long stationId = StationUtil.getStationId();
		StringBuilder accError = new StringBuilder();
		String [] ais=null;
		BigDecimal money=null;
		BigDecimal giftMoney=null;
		for (String accInfo : accs) {
			accInfo = StringUtils.trim(accInfo);
			if (StringUtils.isEmpty(accInfo)) {
				continue;
			}
			accInfo=accInfo.replaceAll("( +)|(,+)", " ");
			ais=accInfo.split(" ");
			if(ais.length<2) {
				accError.append(accInfo).append(" 数据格式错误,");
				continue;
			}
			try {
				account = accountService.findOneByAccountAndStationId(ais[0], stationId);
			} catch (Exception e) {
				accError.append(accInfo).append(",");
				continue;
			}
			money=BigDecimalUtil.toBigDecimal(ais[1]);
			if(money==null || money.compareTo(BigDecimal.ZERO) < 0) {
				accError.append(accInfo).append("金额格式有误,");
				continue;
			}
			if(ais.length>2) {
				giftMoney=BigDecimalUtil.toBigDecimal(ais[2]);
			}else {
				giftMoney=null;
			}
			mnyComRecordService.artificialAddOne(account, money, betNumMultiple, giftMoney, giftBetNumMultiple, remark);
		}
		if (accError.length() > 0) {
			accError.deleteCharAt(accError.length() - 1);
			throw new GenericException("错误信息：" + accError.toString());
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public void batchSubMoney(String accounts, BigDecimal money, String remark) {
		if (StringUtils.isEmpty(accounts)) {
			throw new GenericException("请输入会员账号");
		}
		if (money == null || money.compareTo(BigDecimal.ZERO) <= 0) {
			throw new GenericException("金额格式有误！");
		}
		String[] accs = accounts.split(",| |\n");
		SysAccount account = null;
		Long stationId = StationUtil.getStationId();
		StringBuilder accError = new StringBuilder();
		for (String acc : accs) {
			acc = StringUtils.trim(acc);
			if (StringUtils.isEmpty(acc)) {
				continue;
			}
			try {
				account = accountService.findOneByAccountAndStationId(acc, stationId);
			} catch (Exception e) {
				accError.append(acc).append(",");
				continue;
			}
			mnyComRecordService.artificialSubOne(account, money, remark);
		}
		if (accError.length() > 0) {
			accError.deleteCharAt(accError.length() - 1);
			throw new GenericException("以下会员账号不存在：" + accError.toString());
		}
	}
}
