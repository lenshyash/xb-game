package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.game.model.BetNumRecord;
import com.game.model.vo.BetNumRecordVo;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.BetNumRecordDao;
import com.game.service.BetNumRecordService;

@Service
public class BetNumRecordServiceImpl implements BetNumRecordService {
	@Autowired
	private BetNumRecordDao betNumRecordDao;

	public Page getBetNumRecordByApi(BetNumRecordVo recordVo) {
		return betNumRecordDao.getPageByApi(recordVo);
	}

	@Override
	public BigDecimal getBetNumByDate(BetNumRecordVo recordVo) {
		return betNumRecordDao.getBetNumByDate(recordVo);
	}

	@Override
	public BetNumRecord getLastOneRecord(BetNumRecordVo recordVo) {
		return betNumRecordDao.getLastOneRecord(recordVo);
	}

	@Override
	public Page getPage(String account, Date beginDate, Date endDate, Integer type, Long stationId) {
		return betNumRecordDao.getPage(account, null, beginDate, endDate, type, stationId);
	}
}
