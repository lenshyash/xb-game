package com.game.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.ProxyMultiRebateSettingDao;
import com.game.dao.SysAccountDao;
import com.game.dao.platform.AgentProfitShareHandlerRecordDao;
import com.game.dao.platform.AgentProfitShareRecordDao;
import com.game.model.ProxyMultiRebateSetting;
import com.game.model.SysAccount;
import com.game.model.platform.AgentProfitShareHandlerRecord;
import com.game.model.platform.AgentProfitShareRecord;
import com.game.service.AgentProfitShareRecordService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;

@Service
public class AgentProfitShareRecordServiceImpl implements AgentProfitShareRecordService {
	private Logger logger = Logger.getLogger(AgentProfitShareRecordService.class);
	@Autowired
	private AgentProfitShareRecordDao agentProfitShareRecordDao;
	@Autowired
	private AgentProfitShareHandlerRecordDao agentProfitShareHandlerRecordDao;
	@Autowired
	private SysAccountDao accountDao;

	@Autowired
	private ProxyMultiRebateSettingDao proxyMultiSettingDao;

	@Override
	public Page page(String account, Date startDate, Date endDate, Integer status, Long stationId) {
		Page page = agentProfitShareRecordDao.page(account, startDate, endDate, status, stationId);
		return page;
	}

	@Override
	public Map findOneMap(Long recordId, Long stationId) {
		return agentProfitShareRecordDao.findOneMap(recordId, stationId);
	}

	private void handler(AgentProfitShareRecord record) {
		SysAccount acc = accountDao.get(record.getAccountId());
		if (acc == null || !StringUtil.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			logger.error("占成处理失败,账号异常");
			return;
		}
		
		if(agentProfitShareRecordDao.updateToHandler(record.getId()) != 1){
			logger.error("占成处理失败,记录状态异常");
			return;
		}
		
		balanceHandlerRecord(record,acc, BigDecimal.ZERO);
	}

	private void balanceHandlerRecord(AgentProfitShareRecord record,SysAccount acc,BigDecimal childrenRate) {
		
		
		BigDecimal betMoney = BigDecimalUtil.addAll(record.getChessBetMoney(),record.getLiveBetMoney(),record.getEgameBetMoney(),record.getThirdLotteryBetMoney(),
				record.getThirdSportBetMoney(),record.getSportBetMoney(),record.getLotteryBetMoney());
		BigDecimal winMoney = BigDecimalUtil.addAll(record.getChessWinMoney(),record.getLiveWinMoney(),record.getEgameWinMoney(),record.getThirdLotteryWinMoney(),
				record.getThirdSportWinMoney(),record.getSportWinMoney(),record.getLotteryWinMoney());
		BigDecimal rebateMoney = BigDecimalUtil.addAll(record.getChessRebateMoney(),record.getLiveRebateMoney(),record.getEgameRebateMoney(),record.getThirdLotteryRebateMoney(),
				record.getThirdSportRebateMoney(),record.getSportRebateMoney(),record.getLotteryRebateMoney());
		BigDecimal platformFee = BigDecimalUtil.addAll(record.getChessPlatformFee(),record.getLivePlatformFee(),record.getEgamePlatformFee(),record.getThirdLotteryPlatformFee(),
				record.getThirdSportPlatformFee(),record.getSportPlatformFee(),record.getLotteryPlatformFee());
		BigDecimal bunko = betMoney.subtract(winMoney);
		BigDecimal feeTotal = BigDecimal.ZERO;
		BigDecimal giveMoney = record.getGiveMoney();
		BigDecimal money = bunko.subtract(rebateMoney).subtract(platformFee).subtract(feeTotal).subtract(giveMoney);
		if (money.compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		Long agentId = acc.getAgentId();
		ProxyMultiRebateSetting pmrs = proxyMultiSettingDao.get(acc.getId());
		if (pmrs == null) {
			logger.error("代理" + acc.getAccount() + "缺少占成设置");
			return;
		}
		BigDecimal selfRate = pmrs.getProfitShare();
		BigDecimal diffRate = selfRate.subtract(childrenRate);
		BigDecimal resultMoney = BigDecimal.ZERO;
		// 占成少于等于0不用处理占成
		
		resultMoney = money.multiply(diffRate).divide(new BigDecimal(100), 2, RoundingMode.DOWN);
		String remark = DateUtil.formatDate(record.getBetDate(), "yyyy-MM-dd")+"记录计算占成,总投注："+betMoney
				+",总派奖："+winMoney+",总输赢："+bunko+",总反水："+rebateMoney+",总费率："+platformFee.setScale(4,RoundingMode.DOWN)+",总赠送:"+giveMoney
				+",结算总金额："+money+",发放金额："+resultMoney
				+",自身占成：["+selfRate+"],下级占成：["+childrenRate+"],结算占成:["+diffRate+"]";
		if(resultMoney.compareTo(BigDecimal.ZERO) != 0) {
			AgentProfitShareHandlerRecord handlerRecord =  agentProfitShareHandlerRecordDao.queryNoHandlerRecord(acc.getId());
			Date now = new Date();
			if(handlerRecord == null) {
				handlerRecord = new AgentProfitShareHandlerRecord();
				handlerRecord.setAccount(acc.getAccount());
				handlerRecord.setAccountId(acc.getId());
				handlerRecord.setAgentId(acc.getAgentId());
				handlerRecord.setAgentName(acc.getAgentName());
				handlerRecord.setParents(acc.getParents());
				handlerRecord.setCreateDatetime(now);
				handlerRecord.setModifyDatetime(handlerRecord.getCreateDatetime());
				handlerRecord.setStatus(AgentProfitShareHandlerRecord.STATUS_NO);
				handlerRecord.setMoney(resultMoney);
				handlerRecord.setStationId(acc.getStationId());
				handlerRecord.setParentNames(acc.getParentNames());
				handlerRecord.setRemark(remark);
				agentProfitShareHandlerRecordDao.save(handlerRecord);
			}else {
				if(StringUtil.isNotEmpty(handlerRecord.getRemark())) {
					remark = handlerRecord.getRemark()+"<br>"+remark;
				}
				
				if(agentProfitShareHandlerRecordDao.updateMoney(handlerRecord.getId(),resultMoney,handlerRecord.getMoney(),remark) != 1) {
					logger.error("占成累加金额出错！["+handlerRecord.getAccount()+" 金额:"+resultMoney+" 原金额:"+handlerRecord.getMoney()+"]");
				}
			}
		}
		
		if (Validator.isNotNull(agentId)) {
			SysAccount agent = accountDao.get(agentId);
			if (agent == null) {
				return;
			}
			balanceHandlerRecord(record,agent, selfRate);
		}
	}

	@Override
	public void saveAll(List<AgentProfitShareRecord> all) {
		if (all != null && !all.isEmpty()) {
			AgentProfitShareRecord old = null;
			Map<Long, SysAccount> agentIdMap = new HashMap<>();
			SysAccount acc = null;
			for (AgentProfitShareRecord r : all) {
				//空数据不保存
				if(checkNoSave(r)) {
					continue;
				}
				old = agentProfitShareRecordDao.findOne(r.getBetDate(), r.getAccountId(), r.getStationId());
				if (old == null) {
					if (agentIdMap.containsKey(r.getAccountId())) {
						acc = agentIdMap.get(r.getAccountId());
					} else {
						acc = accountDao.getFromCache(r.getAccountId());
					}
					if (acc != null) {
						r.setAgentId(acc.getAgentId());
						r.setAgentName(acc.getAgentName());
						agentIdMap.put(r.getAccountId(), acc);
					}
					r.setProfitShareStatus(AgentProfitShareRecord.STATUS_NOT_PROFIT);
					agentProfitShareRecordDao.save(r);
				} else {
					// 投注金额不相等，且 状态!=已经反水 才需要修改记录
					agentProfitShareRecordDao.updateBetMoney(r);
					r.setId(old.getId());
				}
				if(StringUtil.equals(r.getProfitShareStatus(), AgentProfitShareRecord.STATUS_OK)) {
					continue;
				}
				handler(r);
			}
		}
	}
	
	private boolean checkNoSave(AgentProfitShareRecord r) {
		return isNull(r.getLotteryBetMoney(),r.getLotteryWinMoney(),r.getLotteryPlatformFee(),r.getLotteryRebateMoney()
				,r.getSportBetMoney(),r.getSportWinMoney(),r.getSportPlatformFee(),r.getSportRebateMoney()
				,r.getLiveBetMoney(),r.getLiveWinMoney(),r.getLivePlatformFee(),r.getLiveRebateMoney()
				,r.getChessBetMoney(),r.getChessWinMoney(),r.getChessPlatformFee(),r.getLiveRebateMoney()
				,r.getThirdSportBetMoney(),r.getThirdSportWinMoney(),r.getThirdSportPlatformFee(),r.getThirdSportRebateMoney()
				,r.getThirdLotteryBetMoney(),r.getThirdLotteryWinMoney(),r.getThirdLotteryPlatformFee(),r.getThirdLotteryRebateMoney()
				,r.getEgameBetMoney(),r.getEgameWinMoney(),r.getEgamePlatformFee(),r.getEgameRebateMoney(),
				r.getThirdDeposit(),r.getGiveMoney());
	}
	
	private boolean isNull(BigDecimal ... values) {
		for (int i = 0; i < values.length; i++) {
			if(StringUtil.toBigDecimal(values[i]).compareTo(BigDecimal.ZERO) != 0) {
				return false;
			}
		}
		return true;
	}
}
