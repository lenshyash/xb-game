package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.game.constant.StationConfig;
import com.game.dao.SysAccountDao;
import com.game.dao.lottery.BcLotteryRoomDao;
import com.game.dao.platform.MemberBackwaterRecordDao;
import com.game.dao.platform.MemberBackwaterStrategyDao;
import com.game.model.SysAccount;
import com.game.model.lottery.BcLotteryRoom;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.platform.MemberBackwaterStrategy;
import com.game.model.vo.BackwaterParam;
import com.game.service.MemberBackwaterCoreService;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Service
public class MemberBackwaterRecordServiceImpl implements MemberBackwaterRecordService {
	private Logger logger = Logger.getLogger(MemberBackwaterRecordService.class);
	@Autowired
	private MemberBackwaterRecordDao backwarterRecordDao;
	@Autowired
	private MemberBackwaterCoreService memberBackwaterCoreService;
	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private ProxyDailyBettingStatisticsService pbssService;
	@Autowired
	private MemberBackwaterStrategyDao backwaterStrategyDao;
	@Autowired
	private BcLotteryRoomDao lotteryRoomDao;

	@Override
	public Page page(String account, Date startDate, Date endDate, Integer betType, Integer status, Long stationId,Long roomId,Long reportType) {
		Page<MemberBackwaterRecord> page = backwarterRecordDao.page(account, startDate, endDate, betType, status, stationId,roomId,reportType);
		boolean isV4 = "v4".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		if(page.getList() != null && !page.getList().isEmpty()){
			Integer backwaterStatus = 0;
			for(MemberBackwaterRecord record : page.getList()){
				if(isV4){
					record.setRoomName(lotteryRoomDao.get(StringUtil.toLong(record.getBackwaterRoomId())).getName());
				}
				backwaterStatus = record.getBackwaterStatus();
				if(StringUtil.equals(backwaterStatus, MemberBackwaterRecord.STATUS_NOT_ROLL )
						|| StringUtil.equals(backwaterStatus, MemberBackwaterRecord.STATUS_CANCLE_ROLL_BACK)) {
					record.setBackwaterRate(getBackWaterRate(record));
				}
			}
		}
		
		return page;
	}
	
	/**
	 * 获取记录的符合的反水作记录，预备反水预览
	 * @param record
	 * @return
	 */
	private BigDecimal getBackWaterRate(MemberBackwaterRecord record) {
		MemberBackwaterStrategy strategy= memberBackwaterCoreService.filterBackwaterStrategy(record);
		if(strategy == null) {
			return BigDecimal.ZERO;
		}
		return strategy.getRate();
	}

	@Override
	public int cancel(Integer betType, String account, String betDate, Long stationId,Long roomId) {
		Date bd = DateUtil.toDate(betDate);
		if (bd == null || betType == null || StringUtils.isEmpty(account)) {
			throw new GenericException("参数不对");
		}
		MemberBackwaterRecord r = backwarterRecordDao.findOne(bd, account, betType, stationId,roomId);
		if (r == null || !r.getStationId().equals(stationId)) {
			throw new GenericException("反水记录不存在");
		}
		return memberBackwaterCoreService.cancel(r);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
	public void manualRollback(Integer betType, String account, String betDate, Long stationId,Long roomId, BigDecimal money) {
		Date bd = DateUtil.toDate(betDate);
		if (bd == null || betType == null || StringUtils.isEmpty(account)) {
			throw new GenericException("参数不对");
		}
		MemberBackwaterRecord r = backwarterRecordDao.findOne(bd, account, betType, stationId,roomId);
		memberBackwaterCoreService.manualRollbackOne(r, stationId, money);
		List<Long> sIdList = new ArrayList<>();
		sIdList.add(stationId);
		pbssService.collectDailyAmount2(bd, bd, sIdList);
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
	public void doBackwaterMoneyByBatch(String[] moneys, Long stationId, Date startDate, Date endDate) {
		List<Long> ids=new ArrayList<>();
		Map<Long,BigDecimal> moneyMap=new HashMap<>();
		String [] idMoney=null;
		long id=0L;
		for(String m:moneys){
			idMoney=m.split(":");
			if(idMoney.length!=2){
				throw new GenericException("数据格式错误");
			}
			id=NumberUtils.toLong(idMoney[0]);
			ids.add(id);
			moneyMap.put(id, BigDecimalUtil.toBigDecimalDefaultZero(idMoney[1]));
		}
		List<MemberBackwaterRecord> list = backwarterRecordDao.findByIds(ids, startDate, endDate, stationId);
		if (list != null && !list.isEmpty()) {
			for (MemberBackwaterRecord r : list) {
				try {
					memberBackwaterCoreService.manualRollbackOne(r, stationId,moneyMap.get(r.getId()));
				} catch (Exception e) {
					logger.error("反水发生错误", e);
				}
			}
			List<Long> sIdList = new ArrayList<>();
			sIdList.add(stationId);
			pbssService.collectDailyAmount2(startDate, endDate, sIdList);
		}
	}

	@Override
	public MemberBackwaterRecord findOne(Date betDate, String account, Integer betType, Long stationId,Long roomId) {
		return backwarterRecordDao.findOne(betDate, account, betType, stationId,roomId);
	}

	@Override
	public void saveAll(List<MemberBackwaterRecord> all) {
		if (all != null && !all.isEmpty()) {
			MemberBackwaterRecord old = null;
			Map<Long, SysAccount> agentIdMap = new HashMap<>();
			SysAccount acc = null;
			for (MemberBackwaterRecord r : all) {
				old = findOne(r.getBetDate(), r.getAccount(), r.getBetType(), r.getStationId(),r.getBackwaterRoomId());
				if (old == null) {
					if (agentIdMap.containsKey(r.getAccountId())) {
						acc = agentIdMap.get(r.getAccountId());
					} else {
						acc = accountDao.getFromCache(r.getAccountId());
					}
					if (acc != null) {
						r.setAgentId(acc.getAgentId());
						r.setAgentName(acc.getAgentName());
						agentIdMap.put(r.getAccountId(), acc);
					}
					r.setBackwaterStatus(MemberBackwaterRecord.STATUS_NOT_ROLL);
					backwarterRecordDao.insert(r);
				} else {
					// 投注金额不相等，且 状态!=已经反水 才需要修改记录
					backwarterRecordDao.updateBetMoney(r);
				}
			}
		}
	}

	@Override
	public List<MemberBackwaterRecord> collectMarkSixDayBetMoney(Date start, Date end, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			return null;
		}
		List<Long> ids = new ArrayList<>();
		List<MemberBackwaterStrategy> strategyList = null;
		for (Long id : sIdList) {
			strategyList = backwaterStrategyDao.findByTypeFromCache(5, id);// 六合特码B
			if (strategyList != null && !strategyList.isEmpty()) {
				ids.add(id);
			} else {
				strategyList = backwaterStrategyDao.findByTypeFromCache(6, id);// 六合正码B
				if (strategyList != null && !strategyList.isEmpty()) {
					ids.add(id);
				}
			}
		}
		if (ids.isEmpty()) {
			return null;
		}
		return backwarterRecordDao.collectMarkSixDayBetMoney(start, end, ids);
	}

	public List<MemberBackwaterRecord> collectLotteryDayBetMoney(Date start, Date end, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			return null;
		}
		List<Long> ids = new ArrayList<>();
		List<MemberBackwaterStrategy> strategyList = null;
		for (Long id : sIdList) {
			if("v4".equals(StationConfigUtil.get(id, StationConfig.lottery_template_name))){	//如果是v4版本的过滤，使用新的反水机制
				continue;
			}
			strategyList = backwaterStrategyDao.findByTypeFromCache(1, id);// 彩票
			if (strategyList != null && !strategyList.isEmpty()) {
				ids.add(id);
			}
		}
		if (ids.isEmpty()) {
			return null;
		}
		return backwarterRecordDao.collectLotteryDayBetMoney(start, end, ids);
	}
	
	
	public List<MemberBackwaterRecord> collectSysLotteryDayBetMoney(Date start, Date end, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			return null;
		}
		List<Long> ids = new ArrayList<>();
		List<MemberBackwaterStrategy> strategyList = null;
		for (Long id : sIdList) {
			if("v4".equals(StationConfigUtil.get(id, StationConfig.lottery_template_name))){	//如果是v4版本的过滤，使用新的反水机制
				continue;
			}
			strategyList = backwaterStrategyDao.findByTypeFromCache(11, id);// 系统彩
			if (strategyList != null && !strategyList.isEmpty()) {
				ids.add(id);
			}
		}
		if (ids.isEmpty()) {
			return null;
		}
		return backwarterRecordDao.collectSysLotteryDayBetMoney(start, end, ids);
	}
	/**
	 * 汇总v4版本投注金额
	 */
	@Override
	public List<MemberBackwaterRecord> collectLotteryV4DayBetMoney(Date start, Date end, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			return null;
		}
		Set<Long> ids = new HashSet<>();
		Set<Long> rIds = new HashSet<>();
		List<MemberBackwaterStrategy> strategyList = null;
		List<BcLotteryRoom> roomList = null;
		for (Long id : sIdList) {
			roomList = lotteryRoomDao.getRoomStationId(id);
			//必须是配置v4，且房间号不能为空
			if("v4".equals(StationConfigUtil.get(id, StationConfig.lottery_template_name)) && roomList != null && !roomList.isEmpty()){	
				ids.add(id);
				for(BcLotteryRoom room:roomList){
					strategyList = backwaterStrategyDao.findByTypeFromCache(1, id,room.getId());// v4彩票
					if (strategyList != null && !strategyList.isEmpty()) {
						rIds.add(room.getId());
					}
				}
			}
		}
		if (ids.isEmpty()) {
			return null;
		}
		return backwarterRecordDao.collectLotteryV4DayBetMoney(start, end, ids,rIds);
	}

	@Override
	public List<MemberBackwaterRecord> collectSportDayBetMoney(Date start, Date end, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			return null;
		}
		List<Long> ids = new ArrayList<>();
		List<MemberBackwaterStrategy> strategyList = null;
		for (Long id : sIdList) {
			strategyList = backwaterStrategyDao.findByTypeFromCache(4, id);// 体育
			if (strategyList != null && !strategyList.isEmpty()) {
				ids.add(id);
			}
		}
		if (ids.isEmpty()) {
			return null;
		}
		return backwarterRecordDao.collectSportDayBetMoney(start, end, ids);
	}

	@Override
	public Page page(BackwaterParam param) {
		return backwarterRecordDao.page(param);
	}

	@Override
	public void cashingBackwater(Long id) {
		MemberBackwaterRecord mbr = backwarterRecordDao.getReocrdById(id);
		if(mbr == null || !StringUtil.equals(mbr.getAccountId(), UserUtil.getUserId())) {
			throw new GenericException("非法操作");
		}
		mbr.setBackwaterStatus(MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
		memberBackwaterCoreService.backWaterAddMoney(mbr,MemberBackwaterRecord.STATUS_MONEY_UNCLAIMED);
	}
	
	@Override
	public void autoExpireStatus(Long stationId) {
		String timeoutStr = StationConfigUtil.get(stationId, StationConfig.member_backwater_expired_time);
		if (StringUtils.isEmpty(timeoutStr)) {
			return;
		}
		BigDecimal timeout = BigDecimalUtil.toBigDecimal(timeoutStr);
		if (timeout == null || timeout.compareTo(BigDecimal.ZERO) <= 0) {
			return;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(DateUtil.parseDate(DateUtil.getCurrentDate()));
		c.add(Calendar.DATE, -timeout.abs().intValue());
		backwarterRecordDao.updateStatusToExpired(c.getTime(), stationId);
	}
}
