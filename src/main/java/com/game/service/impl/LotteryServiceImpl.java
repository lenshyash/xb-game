package com.game.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryDao;
import com.game.model.SysAccount;
import com.game.model.SysStation;
import com.game.model.SysStationFolderUrl;
import com.game.model.lottery.BcLottery;
import com.game.model.platform.AgentLunBo;
import com.game.model.vo.LotteryGroupVo;
import com.game.service.GetConfigService;
import com.game.service.LotteryService;
import com.game.service.SysStationFolderUrlService;
import com.game.util.LotteryVersionUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class LotteryServiceImpl implements LotteryService{

	@Autowired
	private BcLotteryDao lotteryDao;
	@Autowired
	private GetConfigService getConfigService;
	@Autowired
	private SysStationFolderUrlService folderUrlService;
	/**
	 * 以下第三版彩票使用
	 */
	@Override
	public List<BcLottery> getLottery(Model model) {
		SysStation station = StationUtil.getStation();
		Integer version = LotteryVersionUtils.resultVersion(station.getId());
		List<BcLottery> blList = lotteryDao.find(station.getId(), BusinessConstant.status_normal, BusinessConstant.status_normal, null, null, version);
		if(blList == null || blList.isEmpty()){
			throw new GenericException("还没有彩种可玩");
		} 
		model.addAttribute("lotList", blList);
		model.addAttribute("stationFolder", station.getFloder());
		model.addAttribute("logo",StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_page_logo_url));
		List<AgentLunBo> lunBoList = getConfigService.getLunBo(station.getId(), new Integer[]{2,3});
		model.addAttribute("indexLunBo",lunBoList);
		model.addAttribute("isReg",StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_register));
		SysStationFolderUrl link = folderUrlService.checkAppLink();
		model.addAttribute("appIOS",link.getIosQrUrl());
		model.addAttribute("appAndroid",link.getAndroidQrUrl());
		model.addAttribute("saiCheSwfStatus", StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_sai_che_swf));
		model.addAttribute("saiCheSwfPath", StationConfigUtil.getSys(StationConfig.sai_che_swf_path_all));
		String cashName = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.jiebao_cash_name);
		model.addAttribute("cashName", StringUtils.isEmpty(cashName)?"元":cashName);// 账户金额名称
		model.addAttribute("lotVersion", version);
		return blList;
	}

	@Override
	public void headerLot(Model model,String navClass) {
		List<BcLottery> lotList = getLottery(model);
		Set<Integer> set = new HashSet<>();
		for(BcLottery bl:lotList){
			if(bl.getType() == 52 || bl.getType() == 57 || bl.getType() == 53 || bl.getType() == 6){
				continue;
			}
			set.add(bl.getType());
		}
		model.addAttribute("headerView", set);
		long stationId = StationUtil.getStationId();
		//公用部分
		model.addAttribute("caipiao_version", StationConfigUtil.get(stationId, StationConfig.lottery_page_version));
		model.addAttribute("caipiao", StationConfigUtil.get(stationId, StationConfig.onoff_du_li_cai_piao));
		model.addAttribute("lotteryBetModel", StationConfigUtil.get(stationId, StationConfig.lottery_bet_model));
		model.addAttribute("betZst", StationConfigUtil.get(stationId, StationConfig.jiebao_zst));// 走势图
		model.addAttribute("fanShui", StationConfigUtil.get(stationId, StationConfig.onoff_fan_shui));
		model.addAttribute("registerTestGuest",StringUtils.equals("on", StationConfigUtil.get(stationId, StationConfig.on_off_register_test_guest_station)));
		if(UserUtil.isLogin()){
			SysAccount acc = (SysAccount) SysUtil.getCurrentUser();
			if( Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
				model.addAttribute("isGuest",true);
			}else{
				model.addAttribute("isGuest",false);
			}
		}
		//奖金是否浮动
		model.addAttribute("isDynam", "on".equals(StationConfigUtil.get(stationId, StationConfig.bc_lottery_v3_dynam)));
//		boolean dynam = "on".equals(StationConfigUtil.get(stationId, StationConfig.bc_lottery_v3_dynam));
//		if(dynam){
//			model.addAttribute("isJoint",true);
//		}else{
//			model.addAttribute("isJoint", false);
//		}
		//合买开关
		model.addAttribute("isJoint","off".equals(StationConfigUtil.get(stationId, StationConfig.bc_lottery_v3_is_joint)));
		if(StringUtils.isNotEmpty(navClass)){
			model.addAttribute("navClass", navClass);
		}
		//积分兑换开关
		model.addAttribute("exchangeScore", StationConfigUtil.get(stationId, StationConfig.exchange_score));
		//积分显示开关
		model.addAttribute("mnyScore", StationConfigUtil.get(stationId, StationConfig.mny_score_show));
		//站内信开关
		model.addAttribute("letters", StationConfigUtil.get(stationId, StationConfig.onoff_letters));
		//账户金额名称
		model.addAttribute("cashName", StationConfigUtil.get(stationId, StationConfig.jiebao_cash_name));
		//独立彩票系统开关 
		model.addAttribute("duLiCaiPiao", StationConfigUtil.get(stationId, StationConfig.onoff_du_li_cai_piao));
		//充值记录开关
		model.addAttribute("onlinePay", StationConfigUtil.get(stationId, StationConfig.onoff_online_pay_class));
		//帐变记录开关
		model.addAttribute("changeMoney", StationConfigUtil.get(stationId, StationConfig.onoff_change_money));
		//访问统计代码
//		model.addAttribute("access", StationConfigUtil.get(stationId, StationConfig.access_statistics_code));
		//帐变记录开关
		model.addAttribute("changeMoney", StationConfigUtil.get(stationId, StationConfig.onoff_change_money));
		
		model.addAttribute("webKeywords", StationConfigUtil.get(stationId, StationConfig.agent_web_head_keywords));
		
		model.addAttribute("webDescription", StationConfigUtil.get(stationId, StationConfig.agent_web_head_description));
		
		model.addAttribute("isTyOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_sports_game));
		model.addAttribute("isBgemOnOff", StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem));
		//客服url
		String kfUrl = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.online_customer_service_url);
		if (kfUrl != null && kfUrl.indexOf("http://") == -1 && kfUrl.indexOf("https://") == -1) {
			kfUrl = "http://" + kfUrl;
		}
		model.addAttribute("kfUrl", kfUrl);
		String appQRCodeLinkIos = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_ios);
		String appQRCodeLinkAndroid = StationConfigUtil.get(stationId, StationConfig.app_qr_code_link_android);
		model.addAttribute("appQRCodeLinkIos", appQRCodeLinkIos);
		model.addAttribute("appQRCodeLinkAndroid", appQRCodeLinkAndroid);
		
	}

	@Override
	public List<BcLottery> getLotteryList() {
		SysStation station = StationUtil.getStation();
		Integer version = LotteryVersionUtils.resultVersion(station.getId());
		List<BcLottery> blList = lotteryDao.find(station.getId(), BusinessConstant.status_normal, BusinessConstant.status_normal, null, null, version);
		return blList;
	}
	
	public Map<String,String> getLotteryMap(){
		List<BcLottery> lotterys= getLotteryList();
		Map<String,String> lotteryMaps = new HashMap<>();
		for (BcLottery bcLottery : lotterys) {
			lotteryMaps.put(bcLottery.getCode(), bcLottery.getName());
		}
		return lotteryMaps;
	}
	public List<LotteryGroupVo> getBigGroup(Integer indentify){
		 List<Integer> bigGroup = lotteryDao.getBigGroup(StationUtil.getStationId(),indentify);
		 List<LotteryGroupVo> groupList = new ArrayList<>();
		 if(bigGroup!=null && bigGroup.size()>0){
			 for(Integer type:bigGroup){
				if(type==6 || type == 66){
					continue;
				}
				LotteryGroupVo vo = new LotteryGroupVo(); 
				vo.setType(type);
				vo.setName(getGroupName(type));
				groupList.add(vo);
			 }
		 }
		 return groupList;
	}
	private String getGroupName(Integer type) {
		switch (type) {
		case 2:
		case 9:
		case 52:
			return "时时彩";
		case 3:
		case 8:
		case 53:
		case 158:
			return "北京赛车(快开)";
		case 4:
		case 54:
		case 15:
			return "排列3/福彩3D";
		case 5:
		case 55:
		case 14:
			return "11选5";
		case 6:
			return "香港彩";
		case 66:
			return "十分香港彩";
		case 7:
		case 11:
		case 57:
		case 161:
			return "PC蛋蛋";
		case 10:
		case 100:
		case 58:
			return "快三";
		case 12:
			return "快乐十分";
		case 1:
		case 51:
			return "系统彩";
		case 929:
			return "(系统)时时彩";
		case 828:
			return "(系统)赛车";
		case 102:
			return "(系统)快三";
		}
		return "";
	}
	
	
}
