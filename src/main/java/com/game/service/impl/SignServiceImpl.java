package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.core.SystemConfig;
import com.game.dao.AgentSignRecordDao;
import com.game.dao.AgentSignRuleDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.model.AgentSignRecord;
import com.game.model.AgentSignRule;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.dictionary.ScoreRecordType;
import com.game.model.vo.MnyScoreVo;
import com.game.service.MnyScoreService;
import com.game.service.SignService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Service
public class SignServiceImpl implements SignService {

	@Autowired
	private AgentSignRecordDao recordDao;

	@Autowired
	private AgentSignRuleDao ruleDao;

	@Autowired
	private MnyScoreService scoreService;

	@Autowired
	private SysAccountDao sysAccountDao;
	@Autowired
	private SysAccountInfoDao sysAccountInfoDao;

	@Override
	public Page getRulePage() {
		return ruleDao.queryStationRulePage();
	}

	@Override
	public Page getSignRecords() {
		return recordDao.queryPage();
	}

	@Override
	public void delRule(long id) {
		AgentSignRule rule = ruleDao.get(id);
		if (rule == null) {
			return;
		}
		// 非法操作，只能删除本站点的记录
		if (!StringUtil.equals(rule.getStationId(), StationUtil.getStationId())) {
			return;
		}
		ruleDao.delete(id);
	}

	@Override
	public void saveRule(AgentSignRule rule) {
		Long id = rule.getId();
		Long stationId = StationUtil.getStationId();
		rule.setStationId(stationId);
		if (ruleDao.isNotUnique(rule, "stationId,days")) {
			throw new GenericException("连续签到" + rule.getDays() + "天的配置已存在");
		}
		if (Validator.isNull(id)) {// 新增
			ruleDao.save(rule);
		} else {
			AgentSignRule dbRule = ruleDao.get(id);
			// 非法操作，只能删除本站点的记录
			if (!StringUtil.equals(dbRule.getStationId(), stationId)) {
				throw new GenericException("非法操作");
			}
			dbRule.setScore(rule.getScore());
			dbRule.setDays(rule.getDays());
			dbRule.setSignType(rule.getSignType());
			dbRule.setSignClear(rule.getSignClear());
			ruleDao.save(dbRule);
		}
	}

	@Override
	public AgentSignRecord sign() {
		int signCount = sysAccountDao.sign(UserUtil.getUserId(), new Date());
		if (signCount == -1) {
			throw new GenericException("今天已经签到过");
		}
		long stationId = StationUtil.getStationId();
		AgentSignRule rule = ruleDao.getMatchRule(stationId, signCount);
		
		long winScore = 0;
		if (rule != null) {
			winScore = rule.getScore();
		}
		//判断规则是否需要清零
		if(rule!=null && rule.getSignClear()!=null && rule.getSignClear()==AgentSignRule.SIGN_CLEAR_YES){
			//判断会员签到数是否达到签到最大值
			if(rule.getDays()==signCount){
				sysAccountDao.clearSignCount(UserUtil.getUserId());
			}
		}
		if (winScore > 0) { // 写入积分
			MnyScoreVo scoreVo = new MnyScoreVo();
			scoreVo.setAccountId(UserUtil.getUserId());
			scoreVo.setScore(new BigDecimal(winScore));
			scoreVo.setRemark("连续" + signCount + "天签到");
			scoreVo.setScoreRecordType(ScoreRecordType.SIGN_IN);
			scoreService.updScoreAndRecord(scoreVo);
		}
		Date now = new Date();
		AgentSignRecord record = new AgentSignRecord();
		record.setStationId(stationId);
		record.setMemberId(UserUtil.getUserId());
		record.setMemberName(UserUtil.getUserAccount());
		record.setScore(winScore);
		record.setSignDays(new Long(signCount));
		record.setSignDate(now);
		recordDao.save(record);

		SysAccount member = sysAccountDao.get(UserUtil.getUserId());
		SysAccountInfo memberInfo = sysAccountInfoDao.get(UserUtil.getUserId());
		UserUtil.getSession().setAttribute(SystemConfig.SESSION_MEMBER_KEY, member);
		UserUtil.getSession().setAttribute(SystemConfig.SESSION_MEMBER_KEY+SystemConfig.SESSION_ACCOUNT_INFO_SUFFIX, memberInfo);
		return record;
	}

	@Override
	public List<AgentSignRecord> getMemberSignRecord() {
		Date now = new Date();
		Date endDate = DateUtil.dayFirstTime(now, 1);
		Date startDate = DateUtil.monthOfDays(DateUtils.addMonths(now, -1), 1); // 上个月第一天
		List<AgentSignRecord> records = recordDao.getMemberRecords(UserUtil.getUserId(), startDate, endDate);
		return records;
	}
	
	@Override
	public List<Map<String, String>> signList(String signYear,String signMonth,String signDay,Long userId) {
		
		if(Integer.valueOf(signMonth) < 10){
			signMonth = "0" + signMonth;
		}
		
		Date startDate = DateUtil.toDatetime(signYear+"-"+signMonth+"-01 00:00:00");
		Date endDate = DateUtil.toDatetime(signYear+"-"+signMonth+"-"+signDay+" 23:59:59");
		List<AgentSignRecord> records = recordDao.getMemberRecords(userId, startDate, endDate);
		List<Map<String, String>> list = new ArrayList<>();
		if(records != null && records.size() > 0){
			for(AgentSignRecord r : records){
				Map<String, String> map = new HashMap<>();
				map.put("signDay", DateUtil.toDateStr(r.getSignDate()).split("-")[2]);
				list.add(map);
			}
		}
		return list;
	}

	@Override
	public List<AgentSignRecord> zxqd(Long stationId) {
		List<AgentSignRecord> asr = recordDao.getMemZxqd(stationId);
		return asr;
	}
	@Override
	public AgentSignRule getOne(Long id, Long stationId) {
		if (id != null) {
			AgentSignRule it = ruleDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

	@Override
	public  Page<Map> zxqdRecord(Long stationId) {
		// TODO Auto-generated method stub
		Page asr = recordDao.getMemZxqdPage(stationId);
		return asr;
	}
}
