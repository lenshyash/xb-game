package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.LogType;
import com.game.dao.MemberBalanceGemStrategyDao;
import com.game.dao.MemberBalanceGemStrategyLevelDao;
import com.game.dao.MemberLevelBaseDao;
import com.game.model.MemberBalanceGemStrategy;
import com.game.model.MemberLevelBase;
import com.game.model.MemberBalanceGemStrategyLevel;
import com.game.model.SysAccount;
import com.game.service.MemberBalanceGemStrategyService;
import com.game.service.SysAccountService;
import com.game.util.SysLogUtil;

@Service
public class MemberBalanceGemStrategyServiceImpl implements MemberBalanceGemStrategyService {
	@Autowired
	private MemberBalanceGemStrategyDao MemberBalanceGemStrategyDao;
	@Autowired
	private MemberBalanceGemStrategyLevelDao memberBalanceGemStrategyLevelDao;
	@Autowired
	private MemberLevelBaseDao memberLevelBaseDao;
	@Autowired
	private SysAccountService accountService;

	@Override
	public Page<MemberBalanceGemStrategy> getPage( Date begin, Date end,
			Long stationId) {
		return MemberBalanceGemStrategyDao.getPage( begin, end, stationId);
	}

	@Override
	public MemberBalanceGemStrategy getOne(Long id, Long stationId) {
		return MemberBalanceGemStrategyDao.getOne(id, stationId);
	}

	@Override
	public void delete(Long id, Long stationId) {
		MemberBalanceGemStrategyDao.delete(id, stationId);
		memberBalanceGemStrategyLevelDao.deleteByStrategyId(id, stationId);
	}

	@Override
	public void addSave(MemberBalanceGemStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId) {
		if (com.getMinMoney() == null) {
			com.setMinMoney(BigDecimal.ZERO);
		}
		if (com.getMaxMoney() == null) {
			com.setMaxMoney(new BigDecimal("10000000"));
		}
		if (com.getMinMoney().compareTo(com.getMaxMoney()) >= 0) {
			throw new GenericException("充值金额最小值必须小于最大值");
		}
		List<Long> levelIds = new ArrayList<>();
		if (groupLevelIds != null && groupLevelIds.length > 0) {
			for (Long lid : groupLevelIds) {
				levelIds.add(lid);
			}
		}
		com.setCreateDatetime(new Date());
		com.setStatus(MemberBalanceGemStrategy.STATUS_NORMAL);
		com.setStationId(stationId);
		validStrategy(com, levelIds);
		MemberBalanceGemStrategyDao.insert(com);
		SysLogUtil.log("添加余额宝策略", LogType.AGENT_SYSTEM);
		if (groupLevelIds != null && groupLevelIds.length != 0) {
			Set<Long> groupLevelSet = new HashSet<>();
			MemberBalanceGemStrategyLevel mcsl = null;
			List<MemberBalanceGemStrategyLevel> llist = new ArrayList<>();
			for (Long glId : groupLevelIds) {
				if (!groupLevelSet.contains(glId)) {
					mcsl = new MemberBalanceGemStrategyLevel();
					mcsl.setMemberLevelId(glId);
					mcsl.setStationId(stationId);
					mcsl.setStrategyId(com.getId());
					llist.add(mcsl);
					groupLevelSet.add(glId);
				}
			}
			memberBalanceGemStrategyLevelDao.batchInsert(llist);
		}
	}

	private void validStrategy(MemberBalanceGemStrategy com, List<Long> groupLevelIds) {
		List<MemberBalanceGemStrategy> list = MemberBalanceGemStrategyDao.findByDepositType(null, com.getStationId(),
				MemberBalanceGemStrategy.STATUS_NORMAL, null, null);
		if (list == null || list.isEmpty()) {
			return;
		}
		BigDecimal min = com.getMinMoney();
		BigDecimal max = com.getMaxMoney();
		List<Long> levelIds = null;
		MemberLevelBase level = null;
		// 同一个时间内，同个金额范围内，同一个等级范围内，不能同时存在2条活动策略
		for (MemberBalanceGemStrategy mcs : list) {
			if (com.getId() != null && com.getId().equals(mcs.getId())) {
				continue;
			}
			if (mcs.getMaxMoney().compareTo(min) < 0 || mcs.getMinMoney().compareTo(max) > 0) {
				continue;
			}
			if (groupLevelIds == null || groupLevelIds.isEmpty()) {
				throw new GenericException("策略冲突：" + mcs.getRemark());
			}
			levelIds = memberBalanceGemStrategyLevelDao.findLevels(mcs.getId(), mcs.getStationId());
			if (levelIds == null || levelIds.isEmpty()) {
				throw new GenericException("策略冲突：" + mcs.getRemark());
			}
			for (Long lid : groupLevelIds) {
				if (levelIds.contains(lid)) {
					level = memberLevelBaseDao.get(lid);
					if (level != null) {
						throw new GenericException("活动冲突：" + mcs.getRemark());
					}
				}
			}
		}
	}

	@Override
	public void updStatus(Integer status, Long id, Long stationId) {
		MemberBalanceGemStrategy mcs = MemberBalanceGemStrategyDao.getOne(id, stationId);
		if (mcs == null) {
			throw new GenericException("余额宝策略不存在");
		}
		String statusStr = "禁用";
		if (status == MemberBalanceGemStrategy.STATUS_NORMAL) {
			validStrategy(mcs, memberBalanceGemStrategyLevelDao.findLevels(id, stationId));
			statusStr = "启用";
		} else {
			status = MemberBalanceGemStrategy.STATUS_DISABLED;
		}

		if (mcs.getStatus() != status) {
			MemberBalanceGemStrategyDao.updStatus(id, status);
			SysLogUtil.log("修改余额宝策略：" + id + "状态为：" + statusStr, LogType.AGENT_FINANCE);
		}
	}

	@Override
	public void update(MemberBalanceGemStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId) {
		if (com.getMinMoney() == null) {
			com.setMinMoney(BigDecimal.ZERO);
		}
		if (com.getMaxMoney() == null) {
			com.setMaxMoney(new BigDecimal("10000000"));
		}
		if (com.getMinMoney().compareTo(com.getMaxMoney()) >= 0) {
			throw new GenericException("充值金额最小值必须小于最大值");
		}
		MemberBalanceGemStrategy old = MemberBalanceGemStrategyDao.getOne(com.getId(), stationId);
		if (old == null) {
			throw new GenericException("请选择赠送策略");
		}

		old.setMinMoney(com.getMinMoney());
		old.setMaxMoney(com.getMaxMoney());
		old.setBetMultiple(com.getBetMultiple());
		old.setRemark(com.getRemark());
		old.setBenchmarkAnnualYield(com.getBenchmarkAnnualYield());
		old.setHongyunAnnualYield(com.getHongyunAnnualYield());
		if (old.getStatus() == MemberBalanceGemStrategy.STATUS_NORMAL) {
			List<Long> levelIds = new ArrayList<>();
			if (groupLevelIds != null && groupLevelIds.length > 0) {
				for (Long lid : groupLevelIds) {
					levelIds.add(lid);
				}
			}
			validStrategy(old, levelIds);
		}
		MemberBalanceGemStrategyDao.update(old);
		SysLogUtil.log("修改余额宝策略", LogType.AGENT_FINANCE);
		if (groupLevelIds != null && groupLevelIds.length != 0) {
			List<Long> oldList = memberBalanceGemStrategyLevelDao.findLevels(old.getId(), stationId);
			Set<Long> groupLevelSet = new HashSet<>();
			MemberBalanceGemStrategyLevel mcsl = null;
			List<MemberBalanceGemStrategyLevel> llist = new ArrayList<>();
			for (Long glId : groupLevelIds) {
				if (!groupLevelSet.contains(glId) && !oldList.contains(glId)) {
					mcsl = new MemberBalanceGemStrategyLevel();
					mcsl.setMemberLevelId(glId);
					mcsl.setStationId(stationId);
					mcsl.setStrategyId(com.getId());
					llist.add(mcsl);
					groupLevelSet.add(glId);
				}
				if (oldList != null) {
					oldList.remove(glId);
				}
			}
			if (!llist.isEmpty()) {
				memberBalanceGemStrategyLevelDao.batchInsert(llist);
			}
			if (oldList != null && !oldList.isEmpty()) {
				memberBalanceGemStrategyLevelDao.deletes(oldList, old.getId(), stationId);
			}
		}
	}

	@Override
	public MemberBalanceGemStrategy filter(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId) {
		int type = getDepositType(moneyRecordType);
		if (type == 0) {
			return null;
		}
		List<MemberBalanceGemStrategy> list = MemberBalanceGemStrategyDao.findByDepositType(type, stationId, MemberBalanceGemStrategy.STATUS_NORMAL,
				depositDate, money);
		list = filterByDepositCount(depositCount, dayDepoitCount, list);
		list = filterByMemberLevel(accountId, stationId, list);
		if (list == null || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	@Override
	public List<MemberBalanceGemStrategy> filterList(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId) {
		int type = getDepositType(moneyRecordType);
		if (type == 0) {
			return null;
		}
		List<MemberBalanceGemStrategy> list = MemberBalanceGemStrategyDao.findByDepositType(type, stationId, MemberBalanceGemStrategy.STATUS_NORMAL,
				depositDate, money);
		list = filterByDepositCount(depositCount, dayDepoitCount, list);
		list = filterByMemberLevel(accountId, stationId, list);
		if (list == null || list.isEmpty()) {
			return null;
		}
		List<MemberBalanceGemStrategy> resultList = new ArrayList<>();
		for (MemberBalanceGemStrategy MemberBalanceGemStrategy : list) {
			if(resultList.size() == 2) {
				return resultList;
			}
			if(resultList.size() == 0 || (resultList.size() != 2 )) {
				resultList.add(MemberBalanceGemStrategy);
			}
		}
		return resultList;
	}

	/**
	 * 根据账变类型获取存款类型
	 * 
	 * @param moneyRecordType
	 * @return
	 */
	private int getDepositType(long moneyRecordType) {
		int type = 0;
		return type;
	}

	/**
	 * 根据存款次数过滤
	 * 
	 * @param depositCount
	 * @param list
	 * @return
	 */
	private List<MemberBalanceGemStrategy> filterByDepositCount(long depositCount, long dayDepoitCount,
			List<MemberBalanceGemStrategy> list) {
		return list;
	}

	/**
	 * 根据会员层级过滤策略
	 * 
	 * @param accountId
	 * @param stationId
	 * @param list
	 * @return
	 */
	private List<MemberBalanceGemStrategy> filterByMemberLevel(Long accountId, Long stationId, List<MemberBalanceGemStrategy> list) {
		if (list == null || list.isEmpty())
			return null;
		SysAccount acc = accountService.getOne(accountId, stationId);
		if (acc == null || acc.getLevelGroup() == null || acc.getLevelGroup() <= 0) {
			return null;
		}
		List<MemberBalanceGemStrategy> rlist = new ArrayList<>();
		List<Long> levelList = null;
		for (MemberBalanceGemStrategy m : list) {
			levelList = memberBalanceGemStrategyLevelDao.findLevels(m.getId(), stationId);
			if (levelList == null || levelList.isEmpty() || levelList.contains(acc.getLevelGroup())) {
				rlist.add(m);
			}
		}
		return rlist;
	}

	@Override
	public List<MemberBalanceGemStrategy> getByType(int type, Long stationId) {
		return MemberBalanceGemStrategyDao.findByDepositType(type, stationId, MemberBalanceGemStrategy.STATUS_NORMAL, new Date(), null);
	}

	@Override
	public List<MemberBalanceGemStrategy> findStationStrategy(Long stationId) {
		// TODO Auto-generated method stub
		return MemberBalanceGemStrategyDao.findStationStrategy(stationId);
	}
	
	
}
