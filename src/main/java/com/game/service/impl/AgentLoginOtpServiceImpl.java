package com.game.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.game.constant.LogType;
import com.game.dao.AgentLoginOtpDao;
import com.game.dao.AgentLoginOtpEscapeDao;
import com.game.model.AgentLoginOtp;
import com.game.model.AgentLoginOtpEscape;
import com.game.service.AgentLoginOtpService;
import com.game.util.SysLogUtil;
import com.game.util.google.GoogleAuthenticator;

@Service
public class AgentLoginOtpServiceImpl implements AgentLoginOtpService {
	@Autowired
	private AgentLoginOtpDao agentLoginOtpDao;
	@Autowired
	private AgentLoginOtpEscapeDao agentLoginOtpEscapeDao;

	@Override
	public Page<AgentLoginOtp> getPage(Long stationId, String iccid) {
		return agentLoginOtpDao.getPage(stationId, iccid);
	}

	@Override
	public List<AgentLoginOtp> find(Long stationId) {
		return agentLoginOtpDao.find(stationId);
	}

	@Override
	public void save(AgentLoginOtp otp) {
		if (otp.getId() == null) {
			otp.setCreateDatetime(new Date());
			if(otp.getStationId()==null || otp.getStationId()==0){
				throw new GenericException("请选择站点！");
			}
			if(agentLoginOtpDao.isNotUnique(otp, "type,stationId")){
				throw new GenericException("该站点已綁定过谷歌验证码");
			}
			
			String desc = "动态密码";
			if(otp.getType()==AgentLoginOtp.type_two){
				//生成秘钥
				String key = GoogleAuthenticator.generateSecretKey();
				if(StringUtil.isNotEmpty(key)){
					otp.setPassword(key);
					//生成谷歌验证二维码链接
					String url = GoogleAuthenticator.getQRBarcodeURL("", otp.getEmail(), key);
					otp.setUrl(url);
				}
				desc = "谷歌验证码";
			}
			agentLoginOtpDao.insert(otp);
			SysLogUtil.log("绑定站点" + otp.getStationId() +"的"+desc, JSON.toJSONString(otp), LogType.ADMIN_LOG);
		} else {
			AgentLoginOtp old = agentLoginOtpDao.get(otp.getId());
			if (old == null)
				return;
			old.setPassword(otp.getPassword());
			agentLoginOtpDao.update(old);
			SysLogUtil.log("修改站点" + otp.getStationId() + "绑定的动态密码", JSON.toJSONString(old), LogType.ADMIN_LOG);
		}
	}

	@Override
	public void delete(Long id) {
		if (id == null || id < 1) {
			return;
		}
		AgentLoginOtp otp = agentLoginOtpDao.get(id);
		if (otp == null)
			return;
		agentLoginOtpDao.delete(id);
		SysLogUtil.log("删除站点" + otp.getStationId() + "绑定的动态密码", JSON.toJSONString(otp), LogType.ADMIN_LOG);
	}

	@Override
	public AgentLoginOtpEscape findOneEscape(Long stationId, String account) {
		if (stationId == null || StringUtils.isEmpty(account)) {
			return null;
		}
		return agentLoginOtpEscapeDao.findOne(stationId, account);
	}

	@Override
	public Page<AgentLoginOtpEscape> getEscapePage(Long stationId, String account) {
		return agentLoginOtpEscapeDao.getPage(stationId, account);
	}

	@Override
	public boolean existOtp(Long stationId) {
		if (stationId == null || stationId <= 0) {
			return false;
		}
		return agentLoginOtpDao.existOtp(stationId);
	}

	@Override
	public List<Map> getAgentUsers(Long stationId) {
		return agentLoginOtpEscapeDao.getAgentUsers(stationId);
	}

	@Override
	public void changeEscapeStatus(Long stationId, String account, Boolean esc) {
		if (esc != null && esc) {
			AgentLoginOtpEscape e = new AgentLoginOtpEscape();
			e.setAccount(account);
			e.setStationId(stationId);
			agentLoginOtpEscapeDao.insert(e);
			SysLogUtil.log("新增站点" + stationId + "管理员" + account + "关闭动态密码", LogType.ADMIN_LOG);
		} else {
			agentLoginOtpEscapeDao.delete(stationId, account);
			SysLogUtil.log("删除站点" + stationId + "管理员" + account + "关闭动态密码", LogType.ADMIN_LOG);
		}

	}
	
	@Override
	public AgentLoginOtp findGoogleOtp(Long stationId,Long type) {
		return agentLoginOtpDao.findGoogleOtp(stationId,type);
	}
}
