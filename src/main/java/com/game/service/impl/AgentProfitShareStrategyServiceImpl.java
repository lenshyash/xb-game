package com.game.service.impl;

import java.util.Date;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.AgentProfitShareStrategyDao;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.service.AgentProfitShareStrategyService;

@Service
public class AgentProfitShareStrategyServiceImpl implements AgentProfitShareStrategyService {
	@Autowired
	private AgentProfitShareStrategyDao memberBackwaterStrategyDao;

	@Override
	public Page page(Integer type, Long stationId) {
		Page<AgentProfitShareStrategy> page = memberBackwaterStrategyDao.page(type, stationId);
		return page;
	}

	@Override
	public void closeOrOpen(Integer status, Long stationId, Long id) {
		if (id == null || stationId == null)
			return;
		memberBackwaterStrategyDao.closeOrOpen(status, stationId, id);
	}

	@Override
	public AgentProfitShareStrategy getOne(Long id, Long stationId) {
		if (id == null || stationId == null)
			return null;
		return memberBackwaterStrategyDao.getOne(id, stationId);
	}

	@Override
	public void insert(AgentProfitShareStrategy mrbs) {
		if (mrbs.getCreateDatetime() == null) {
			mrbs.setCreateDatetime(new Date());
		}
		try {
			memberBackwaterStrategyDao.insert(mrbs);
		} catch (Exception e) {
			throw new GenericException("已经存在此类型的策略");
		}
	}

	@Override
	public void update(AgentProfitShareStrategy mrbs) {
		AgentProfitShareStrategy old = getOne(mrbs.getId(), mrbs.getStationId());
		if(old == null) {
			throw new GenericException("非法请求");
		}
		old.setStatus(mrbs.getStatus());
		old.setMemo(mrbs.getMemo());
		old.setType(mrbs.getType());
		old.setPlatformFeeRate(mrbs.getPlatformFeeRate());
		old.setPlatformFeeType(mrbs.getPlatformFeeType());
		try {
			memberBackwaterStrategyDao.update(old);
		} catch (Exception e) {
			throw new GenericException("已经存在此类型的策略");
		}
	}

	@Override
	public void delete(AgentProfitShareStrategy old) {
		memberBackwaterStrategyDao.delete(old);
	}
}
