package com.game.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.lottery.BcLotteryDao;
import com.game.dao.lottery.BcLotteryDataDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.dao.lottery.BcLotteryPlayDao;
import com.game.dao.sports.SportBettingOrderDao;
import com.game.model.lottery.BcLottery;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.lottery.LotteryEnum;
import com.game.model.vo.LotteryParamVo;
import com.game.model.vo.SearchRecordParam;
import com.game.service.MemberBettingRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Repository
public class MemberBettingRecordServiceImpl implements MemberBettingRecordService {

	@Autowired
	private BcLotteryOrderDao lotteryBetDao;
	@Autowired
	private BcLotteryDao lotteryDao;
	@Autowired
	private SportBettingOrderDao sportBetDao;
	@Autowired
	private BcLotteryPlayDao bcLotteryPlayDao;
	@Autowired
	private BcLotteryDataDao bcLotteryDataDao;

	@Override
	public Page getSportBettingRecord(SearchRecordParam params) {
		return sportBetDao.getPage(params, StationUtil.getStationId());
	}

	@Override
	public Page getLotteryBettingRecord(LotteryParamVo params) {

		String code = params.getCode();
		Date startTime = params.getStartTime();
		Date endTime = params.getEndTime();
		Integer status = params.getStatus();
		String zhuiHao = params.getZhuiHao();
		Integer model = params.getModel();
		String orderId = params.getOrderId();
		String account = params.getAccount();
		Long stationId = params.getStationId();
		if (startTime == null) {
			startTime = DateUtil.dayFirstTime(new Date(), 0);
		}
		if (endTime == null) {
			endTime = DateUtil.dayFirstTime(new Date(), 1);
		}
		Page boPage = this.lotteryBetDao.getBcLotteryOrder(code, startTime, endTime, status, zhuiHao, model, orderId, account, stationId, false);
		List<BcLotteryOrder> list = boPage.getList();
		long sid=0L;
		BcLottery lot = null;
		Map<String, BcLottery> lotMap = new HashMap<>();
		for (BcLotteryOrder order : list) {
			order.setPlayName(bcLotteryPlayDao.getPlayName(order.getPlayCode(), stationId, order.getLotType()));
			lot = lotMap.get(order.getLotCode());
			if (lot == null) {
				lot = lotteryDao.getOne(stationId, order.getLotCode(), null);
				lotMap.put(order.getLotCode(), lot);
			}
			if (lot != null) {
				order.setLotName(lot.getName());
			} else {
				order.setLotName(LotteryEnum.getEnum(order.getLotCode()).getLotName());
			}
//			order.setLotName(LotteryEnum.getEnum(order.getLotCode()).getLotName());
			if (LotteryEnum.isSysLot(order.getLotCode())) {
				sid = stationId;
			}else{
				sid=0L;
			}
			order.setLotteryHaoMa(bcLotteryDataDao.getLotteryHaoMa(order.getQiHao(), order.getLotCode(), sid));
		}
		return boPage;
	}

}
