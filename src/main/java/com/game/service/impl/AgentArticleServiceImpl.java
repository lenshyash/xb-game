package com.game.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.platform.AgentArticleDao;
import com.game.model.platform.AgentArticle;
import com.game.model.vo.ArticleVo;
import com.game.service.AgentArticleService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Repository
public class AgentArticleServiceImpl implements AgentArticleService {

	@Autowired
	private AgentArticleDao agentArticleDao;

	@Override
	public Page page(Long stationId, Integer code) {
		return agentArticleDao.page(stationId, code);
	}
	
	@Override
	public String publicInfo(Integer code) {
		
		Date overTime = new Date();
		List<AgentArticle> list = agentArticleDao.getArticle(code, StationUtil.getStationId(), overTime);
		String info = "";
		for(AgentArticle a : list){
			info += a.getContent();
		}
		return info;
	}

	@Override
	public List<Map> getStationAtcs(Long stationId) {
		ArticleVo avo = new ArticleVo();
		avo.setStationId(stationId);
		List<Long> codes = new ArrayList<Long>();
		codes.add(1l);
		codes.add(2l);
		codes.add(3l);
		codes.add(4l);
		codes.add(5l);
		codes.add(6l);
		codes.add(7l);
		codes.add(8l);
		codes.add(9l);
		codes.add(10l);
		codes.add(18l);
		codes.add(20l);
		codes.add(21l);
		codes.add(23l);
		codes.add(24l);
		avo.setCodes(codes);
		return agentArticleDao.getAtcsLst(avo);
	}
	
	@Override
	public List<Map<String,Object>> getArticleForMobile(Integer code, Long stationId, Date overTime){
		return agentArticleDao.getArticleForMobile(code, stationId, overTime);
	}

	@Override
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		if (id == null || id <= 0 || modelStatus == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentArticleDao.getStationId(id);
		if (!stationId.equals(staId)) {
			throw new GenericException("站点错误！");
		}
		agentArticleDao.openCloseH(modelStatus, id, stationId);
	}

	@Override
	public void openClosek(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentArticleDao.openCloseK(status, id, stationId);
	}

	@Override
	public void addSave(AgentArticle aacle) {
		if (aacle.getCode() == null || aacle.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		agentArticleDao.save(aacle);
		SysLogUtil.log("添加公告"+aacle.getTitle(), LogType.AGENT_SYSTEM);
	}

	@Override
	public void eidtSave(AgentArticle aacle) {
		if (aacle.getId() == null || aacle.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentArticleDao.getStationId(aacle.getId());
		if (!aacle.getStationId().equals(staId)) {
			throw new GenericException("站点错误！");
		}
		AgentArticle acle = agentArticleDao.get(aacle.getId());
		acle.setTitle(aacle.getTitle());
		acle.setContent(aacle.getContent());
		acle.setUpdateTime(aacle.getUpdateTime());
		acle.setOverTime(aacle.getOverTime());
		acle.setIndex(aacle.isIndex());
		acle.setReg(aacle.isReg());
		acle.setMutil(aacle.isMutil());
		acle.setCp(aacle.isCp());
		acle.setFrameHeight(aacle.getFrameHeight());
		acle.setFrameWidth(aacle.getFrameWidth());
		acle.setModelStatus(aacle.getModelStatus());
		acle.setFolderCode(aacle.getFolderCode());
		acle.setSort(aacle.getSort());
		agentArticleDao.update(acle);
		SysLogUtil.log("修改公告"+aacle.getTitle(), LogType.AGENT_SYSTEM);
	}

	@Override
	public void delete(Long id, Long stationId, Integer code) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentArticleDao.delete(id, stationId);
	}

	@Override
	public AgentArticle getOne(Long id) {
		if (id != null) {
			return agentArticleDao.get(id);
		}
		throw new GenericException("参数不正确!");
	}

	@Override
	public List<AgentArticle> listV3(Long id, int code) {
		return agentArticleDao.list(StationUtil.getStationId(), code);
	}
}
