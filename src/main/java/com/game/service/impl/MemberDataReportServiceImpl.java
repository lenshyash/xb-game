package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.dao.MnyComRecordDao;
import com.game.dao.MnyDrawRecordDao;
import com.game.dao.MnyMoneyDao;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.model.MnyComRecord;
import com.game.model.MnyDrawRecord;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MemberDataVo;
import com.game.model.vo.ReportParamVo;
import com.game.service.MemberDataReportService;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

/**
 * 单个会员数据统计
 * 
 * @author macair
 *
 */
@Service
public class MemberDataReportServiceImpl implements MemberDataReportService {
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private MnyMoneyDao mnyMoneyDao;
	@Autowired
	private MnyComRecordDao comRecordDao;
	@Autowired
	private MnyDrawRecordDao drawRecordDao;
	@Autowired
	private MnyMoneyRecordDao moneyRecordDao;
	@Autowired
	private SysAccountDailyMoneyDao accountDailyMoneyDao;
	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	@Autowired
	SysAccountDao accountDao;
	@Autowired
	SysAccountDailyMoneyDao dailyMoneyDao;

	@Override
	public MemberDataVo getMemberData(String account, Long stationId, String begin, String end) {
		String key = getCacheKey(account, stationId, begin, end);
		MemberDataVo rvo = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, MemberDataVo.class);
		if (rvo != null)
			return rvo;
		SysAccount member = accountService.findOneByAccountAndStationId(account, stationId);
		rvo = new MemberDataVo();
		rvo.setAccount(account);
		rvo.setAgentName(member.getAgentName());
		rvo.setBetNum(member.getBetNum());
		rvo.setDrawNeed(member.getDrawNeed());
		rvo.setRegisterDatetime(member.getCreateDatetime());
		rvo.setAccountType(member.getAccountType().intValue());
		rvo.setBalance(mnyMoneyDao.getBalance(member.getId()));

		Date beginDate = DateUtil.parseDate(begin);
		if (beginDate == null) {
			beginDate = DateUtil.dayFirstTime(new Date(), 0);
		}
		Date endDate = DateUtil.parseDate(end);
		if (endDate == null) {
			endDate = DateUtil.dayFirstTime(new Date(), 1);
		}
		readDepositInfo(member.getId(), stationId, beginDate, endDate, rvo);
		readDrawInfo(member.getId(), stationId, beginDate, endDate, rvo);
		readMoneyRecord(member.getId(), stationId, beginDate, endDate, rvo);
		readLotteryInfo(member.getId(), stationId, beginDate, endDate, rvo);
		readRealInfo(member.getId(), stationId, beginDate, endDate, rvo);
		
		//代理返点总计
		ReportParamVo reportParamVo = new ReportParamVo();
		reportParamVo.setBegin(beginDate);
		reportParamVo.setEnd(endDate);
		
		if(member.getAccountType()==4) {
			reportParamVo.setAgentName(account);
			reportParamVo.setStationId(stationId);
			setChildren(reportParamVo);
		}else {
			reportParamVo.setAccount(account);
			reportParamVo.setStationId(stationId);
			setChildren(reportParamVo);
		}
		getRebateAgentTotal(member.getId(), stationId, beginDate, endDate, rvo, reportParamVo);
		
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, rvo);
		return rvo;
	}

	// 统计充值
	private void readDepositInfo(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo) {
		List<MnyComRecord> list = comRecordDao.getSuccessList(accountId, stationId, beginDate, endDate);
		if (list != null && !list.isEmpty()) {
			MnyComRecord last = list.get(0);
			rvo.setLastDepositMoney(last.getMoney());
			rvo.setLastDepositTime(last.getCreateDatetime());
			String desc = "使用" + last.getPayName() + "支付";
			if (last.getHandlerType() == MnyComRecord.HANDLER_TYPE_HAND) {
				if (last.getModifyUserId() != null) {
					SysAccount acc = accountService.getFromCache(last.getModifyUserId(), stationId);
					if (acc != null && acc.getAccountType() != SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
						desc = desc + "," + acc.getAccount() + "手动确认";
					}
				}
			}
			if (StringUtils.isNotEmpty(last.getOpDesc())) {
				desc = desc + "," + last.getOpDesc();
			}
			rvo.setLastDepositDesc(desc);
			for (MnyComRecord c : list) {
				rvo.setDepositTotal(BigDecimalUtil.addAll(c.getMoney(), rvo.getDepositTotal()));
				if (c.getHandlerType() == MnyComRecord.HANDLER_TYPE_HAND) {
					rvo.setDepositHandlerArtificial(
							BigDecimalUtil.addAll(c.getMoney(), rvo.getDepositHandlerArtificial()));
				}
			}
		}
	}

	// 统计取款
	private void readDrawInfo(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo) {
		List<MnyDrawRecord> list = drawRecordDao.getSuccessList(accountId, stationId, beginDate, endDate);
		if (list != null && !list.isEmpty()) {
			MnyDrawRecord last = list.get(0);
			rvo.setLastDrawMoney(last.getTrueMoney());
			rvo.setLastDrawTime(last.getCreateDatetime());
			String desc = "";
			if (last.getModifyUserId() != null) {
				SysAccount acc = accountService.getFromCache(last.getModifyUserId(), stationId);
				if (acc != null && acc.getAccountType() != SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
					desc = acc.getAccount() + "手动确认  ";
				}
			}
			if (StringUtils.isNotEmpty(last.getRemark())) {
				desc = last.getRemark();
			}
			rvo.setLastDrawDesc(desc);
			for (MnyDrawRecord r : list) {
				rvo.setWithdrawTotal(BigDecimalUtil.addAll(rvo.getWithdrawTotal(), r.getTrueMoney()));
			}
		}
	}

	private void readMoneyRecord(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo) {
		Map map = moneyRecordDao.getMemberDataVo(accountId, stationId, beginDate, endDate);
		if (map == null || map.isEmpty()) {
			return;
		}
		rvo.setManualDepositTotal((BigDecimal) map.get("manualDepositTotal"));
		rvo.setManualWithdrawTotal((BigDecimal) map.get("manualWithdrawTotal"));
		rvo.setGiveTotal((BigDecimal) map.get("giveTotal"));
		rvo.setWithdrawGiveTotal((BigDecimal) map.get("withdrawGiveTotal"));
		rvo.setGiveRegisterTotal((BigDecimal) map.get("giveRegisterTotal"));
		rvo.setRebateTotal((BigDecimal) map.get("rebateTotal"));
		rvo.setActiveAwardTotal((BigDecimal) map.get("activeAwardTotal"));
		rvo.setSportTotal((BigDecimal) map.get("sportTotal"));
		rvo.setSportAward((BigDecimal) map.get("sportAward"));
	}

	private void readLotteryInfo(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo) {
		Map map = lotteryOrderDao.getForMemberData(accountId, stationId, beginDate, endDate);
		if (map == null || map.isEmpty()) {
			return;
		}
		rvo.setLotteryAward((BigDecimal) map.get("lotteryAward"));
		rvo.setLotteryTotal((BigDecimal) map.get("lotteryTotal"));
		rvo.setMarkSixAward((BigDecimal) map.get("markSixAward"));
		rvo.setMarkSixTotal((BigDecimal) map.get("markSixTotal"));
	}

	private void readRealInfo(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo) {
		Map map = accountDailyMoneyDao.getRealAndEgame(accountId, stationId, beginDate, endDate);
		if (map == null || map.isEmpty()) {
			return;
		}
		rvo.setRealTotal((BigDecimal) map.get("realTotal"));
		rvo.setRealAward((BigDecimal) map.get("realAward"));
		rvo.setEgameAward((BigDecimal) map.get("egameAward"));
		rvo.setEgameTotal((BigDecimal) map.get("egameTotal"));
		rvo.setChessAward((BigDecimal) map.get("chessAward"));
		rvo.setChessTotal((BigDecimal) map.get("chessTotal"));
	}

	private String getCacheKey(String account, Long stationId, String begin, String end) {
		StringBuilder sb = new StringBuilder("member_data_");
		sb.append(stationId);
		sb.append("_a_").append(account);
		sb.append("_s_").append(begin);
		sb.append("_e_").append(end);
		return sb.toString();
	}
	
	private void setChildren(ReportParamVo paramVo) {
		SysAccount agent = null;
		if (StringUtil.isNotEmpty(paramVo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(paramVo.getStationId());
			avo.setAccount(paramVo.getAgentName());
//			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			agent = accountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			paramVo.setChildren(parents + agent.getId() + ",");
			paramVo.setSearchSelf(true);
			paramVo.setSelfId(agent.getId());
		}
		if (StringUtil.isNotEmpty(paramVo.getAccount())) {
			SysAccount acc = accountDao.findOneByAccountAndStationId(paramVo.getAccount(), paramVo.getStationId());
			if (acc != null) {
				paramVo.setAccountId(acc.getId());
			}
		}
	}
	
	public void getRebateAgentTotal(Long accountId, Long stationId, Date beginDate, Date endDate, MemberDataVo rvo,ReportParamVo reportParamVo){
		Map map = dailyMoneyDao.getRebateAgentTotal(reportParamVo);
		Object rebateAgentTotal = map.get("rebateAgentTotal");
		BigDecimal a= (BigDecimal)rebateAgentTotal;
		rvo.setRebateAgentTotal(a);
	}
}
