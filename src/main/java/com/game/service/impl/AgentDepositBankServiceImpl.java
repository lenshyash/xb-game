package com.game.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.AgentDepositBankDao;
import com.game.dao.SysAccountDao;
import com.game.model.AgentDepositBank;
import com.game.model.AgentDepositLevel;
import com.game.model.SysAccount;
import com.game.model.SysPayPlatform;
import com.game.service.AgentDepositBankService;
import com.game.service.AgentDepositLevelService;
import com.game.service.SysPayPlatformService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentDepositBankServiceImpl implements AgentDepositBankService {

	@Autowired
	private AgentDepositBankDao adbDao;

	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private AgentDepositLevelService depositLevelService;
	@Autowired
	private SysPayPlatformService payPlatformService;
	
	@Override
	public Page getBankPage() {
		return adbDao.getPage();
	}

	@Override
	public void save(AgentDepositBank bank, Long[] groupLevelIds) {
		if (bank == null) {
			throw new GenericException("数据异常");
		}

		//收款卡号支持9位账号
		/*if (bank.getBankCard() == null || bank.getBankCard().length() > 21 || (bank.getBankCard().length() < 15 && bank.getBankCard().length() != 9)) {
			throw new GenericException("银行卡号格式错误！");
		}*/
//		if(!ValidateUtil.isNumber(bank.getBankCard())){
//			throw new GenericException("银行卡号格式错误！");
//		}
		Set<Long> levelIdSet = null;
		Long stationId = StationUtil.getStationId();
		if (bank.getId() != null && bank.getId() > 0) {
			AgentDepositBank old = adbDao.get(bank.getId());
			if (!old.getStationId().equals(stationId)) {
				throw new GenericException("数据异常");
			}
			old.setBankAddress(bank.getBankAddress());
			old.setBankCard(bank.getBankCard());
			old.setCreatorName(bank.getCreatorName());
			old.setMin(bank.getMin());
			old.setMax(bank.getMax());
			old.setSortNo(bank.getSortNo());
			old.setStatus(bank.getStatus());
			old.setPayPlatformId(bank.getPayPlatformId());
			old.setIcon(bank.getIcon());
			old.setBankDesc(bank.getBankDesc());
			old.setPayStarttime(bank.getPayStarttime());
			old.setPayEndtime(bank.getPayEndtime());
			SysLogUtil.log("修改银行入款卡号：" + old.getBankCard() + "  持卡人：" + old.getCreatorName(), LogType.AGENT_FINANCE);
			adbDao.update(old);
			levelIdSet = depositLevelService.getLevelIdsByDepositId(old.getId(), stationId,
					AgentDepositLevel.TYPE_BANK);
		} else {
			bank.setStationId(StationUtil.getStationId());
			SysLogUtil.log("新增银行入款卡号：" + bank.getBankCard() + "  持卡人：" + bank.getCreatorName(), LogType.AGENT_FINANCE);
			adbDao.insert(bank);
		}
		if (groupLevelIds != null) {
			List<AgentDepositLevel> dllist = new ArrayList<>();
			AgentDepositLevel dl = null;
			for (Long lid : groupLevelIds) {
				if (levelIdSet != null && levelIdSet.contains(lid)) {
					levelIdSet.remove(lid);
					continue;
				}
				dl = new AgentDepositLevel();
				dl.setDepositId(bank.getId());
				dl.setStationId(stationId);
				dl.setMemberLevelId(lid);
				dl.setType(AgentDepositLevel.TYPE_BANK);
				dllist.add(dl);
			}
			depositLevelService.batchInsert(dllist);
			if(levelIdSet!=null && !levelIdSet.isEmpty()){
				depositLevelService.batchDelete(bank.getId(), stationId, AgentDepositLevel.TYPE_BANK, new ArrayList<>(levelIdSet));
			}
		}
	}

	@Override
	public List<Map> getStationBanks() {
		List<Map> bankPays = adbDao.getBanks();
		List<Map> newOP = new ArrayList();
		Object payStartTime = "";
		Object payEndTime = "";
		Date startTime = null;
		Date endTime = null;
		Date now = new Date();
		String curDateStr = DateUtil.getCurrentDate();
		for (Map map : bankPays) {
			payStartTime = map.get("payStarttime");
			payEndTime = map.get("payEndtime");
			if(StringUtil.isEmpty(payStartTime) && StringUtil.isEmpty(payEndTime)) {
				newOP.add(map);
				continue;
			}
			if(StringUtil.isNotEmpty(payStartTime) ) {
				startTime = DateUtil.parseDate(curDateStr +" "+payStartTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(StringUtil.isNotEmpty(payEndTime) ) {
				endTime = DateUtil.parseDate(curDateStr +" "+payEndTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(startTime == null && endTime == null) {
				newOP.add(map);
				continue;
			}
			if(startTime == null && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
			
			if(endTime == null && now.after(startTime)) {
				newOP.add(map);
				continue;
			}
			if(now.after(startTime) && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
		}
		
		List<Map> resultPays = new ArrayList<Map>();
		SysAccount user = accountDao.get(UserUtil.getUserId());
		Set<Long> levelIdSet = null;
		boolean show = isShowGuestPayment(StationConfig.onoff_show_pay_normal);
		
		for (Map pay : newOP) {
			levelIdSet=depositLevelService.getLevelIdsByDepositId(getLong(pay.get("id")), user.getStationId(), AgentDepositLevel.TYPE_BANK);
			if ((levelIdSet == null || levelIdSet.isEmpty() || levelIdSet.contains(user.getLevelGroup())) && show) {
				resultPays.add(pay);
			}
		}
		return resultPays;
	}
	
	/**
	 * 是否显示引导账号充值的判断
	 * @param payEnum
	 * @param string
	 * @return
	 */
	private boolean isShowGuestPayment(Enum payEnum) {
		Boolean flag = true;

		String string = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.guide_show_payment);

		Boolean guest = UserUtil.isGuestAccount();
		// 是引导账号
		if (guest) {
			// 有配置东西
			if (StringUtil.isNotEmpty(string)) {
				if (!string.contains("1") && payEnum.equals(StationConfig.onoff_show_pay_third)) {
					flag = false;
				} else if (!string.contains("2") && payEnum.equals(StationConfig.onoff_show_pay_quick)) {
					flag = false;
				} else if (!string.contains("3") && payEnum.equals(StationConfig.onoff_show_pay_normal)) {
					flag = false;
				}
			} else {
				//没选的情况
				flag = false;
			}
		}
		return flag;
	}
	
	private Long getLong(Object val) {
		if(val instanceof Integer){
			return new Long ((Integer)val);
		}
		if(val instanceof Long){
			return (Long)val;
		}
		return null;
	}

	@Override
	public void delBank(long id) {
		AgentDepositBank bank = adbDao.get(id);
		SysPayPlatform payPlatform = payPlatformService.getPayPlatform(bank.getPayPlatformId());
		adbDao.delete(id);
		depositLevelService.deleteByDeposiId(id,StationUtil.getStationId(),AgentDepositLevel.TYPE_BANK);
		SysLogUtil.log("删除银行入款：" + payPlatform.getName(),
				LogType.AGENT_FINANCE);
	}

	@Override
	public AgentDepositBank getOne(Long id, Long stationId) {
		if (id != null) {
			AgentDepositBank ma = adbDao.get(id);
			if (ma != null && (stationId == null || ma.getStationId().equals(stationId))) {
				return ma;
			}
		}
		return null;
	}

	@Override
	public void updateStatus(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		adbDao.updateStatus(status, id, stationId);
	}
}
