package com.game.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.AgentDepositBankDao;
import com.game.dao.AgentDepositFastDao;
import com.game.dao.AgentDepositOnlineDao;
import com.game.dao.MemberDepositInfoDao;
import com.game.dao.MnyBetNumberDao;
import com.game.dao.MemberBalanceGemRecordDao;
import com.game.dao.MemberBalanceGemStrategyDao;
import com.game.dao.MemberBalanceGemStrategyLevelDao;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.dao.SysLogDao;
import com.game.dao.SysPayPlatformDao;
import com.game.dao.platform.MemberDepositStrategyDao;
import com.game.model.BetNumRecord;
import com.game.model.MemberBalanceGemRecord;
import com.game.model.MemberBalanceGemStrategy;
import com.game.model.MnyComStrategy;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.SysLog;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.AgentProfitShareRecord;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.MnyComStrategyService;
import com.game.service.MnyMoneyService;
import com.game.service.MnyScoreService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysMessageService;
import com.game.util.DateUtil;
import com.game.util.Snowflake;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class MemberBalanceGemRecordServiceImpl implements MemberBalanceGemRecordService {
	private Logger logger = Logger.getLogger(MemberBalanceGemRecordService.class);

	@Autowired
	MemberBalanceGemRecordDao balanceGemRecordDao;

	@Autowired
	SysAccountDao accountDao;
	
	@Autowired
	SysAccountInfoDao accountInfoDao;

	@Autowired
	AgentDepositBankDao bankDao;

	@Autowired
	AgentDepositFastDao fastDao;

	@Autowired
	AgentDepositOnlineDao onlineDao;

	@Autowired
	MnyMoneyService moneyService;

	@Autowired
	SysPayPlatformDao payPlatformDao;

	@Autowired
	MemberBalanceGemStrategyDao memberBalanceGemStrategyDao;
	
	@Autowired
	MemberBalanceGemStrategyLevelDao memberBalanceGemStrategyLevelDao;

	@Autowired
	MnyMoneyRecordDao moneyRecordDao;

	@Autowired
	MemberDepositInfoDao depositInfoDao;

	@Autowired
	MnyScoreService scoreService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;

	@Autowired
	SysAccountDailyMoneyService sysAccountDailyMoneyService;


	@Override
	public Page<MemberBalanceGemRecord> getPage(MemberBalanceGemRecord mcrVo,String startTime,String endTime) {
		Page<MemberBalanceGemRecord> page = balanceGemRecordDao.getPage(mcrVo,DateUtil.toDatetime(startTime),DateUtil.toDatetime(endTime));
		return page;
	}

	@Override
	public void collectBalanceGemRecord(Date start, Date end, Long stationId) {
		logger.info("站点"+stationId+"开始进行余额宝返利计算");
 		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(start);
		List<MemberBalanceGemStrategy> strategys = null;
		strategys = memberBalanceGemStrategyDao.findStationStrategy(stationId);
		// 未开余额宝策略不统计
		if (strategys == null || strategys.isEmpty()) {
			return;
		}
		//获取会员次日零点到八点转出的非有效份额     转入金额-转出金额
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(start);
		cal1.add(Calendar.DAY_OF_MONTH,-1);
		Date sta = cal1.getTime();
		//获取会员当天23:59:59前的余额宝余额,为会员基准有效份额
		List<MemberBalanceGemRecord> list = balanceGemRecordDao.selMemberDayBalanceGemRecord(sta,end,stationId);
		if(list==null || list.size()<=0){
			return;
		}
		for(MemberBalanceGemRecord record:list){
			logger.info("会员"+record.getAccount()+"开始进行余额宝返利计算");
			Long accountId= record.getAccountId();
			//判断会员当日是否已经有余额宝返利记录 避免重复返利
			int i = balanceGemRecordDao.checkMemberCurDateRebate(DateUtil.toDate(date),accountId);
			if(i>0){
				continue;
			}
			String remark= "";
			
			//当天余额宝基准有效份额
			BigDecimal bmLastAmount = record.getBackMoney();
			//当天余额宝基准有效份额
			BigDecimal hyLastAmount = null;
			//当天余额宝基准收益
			BigDecimal  bmProfitAmount = BigDecimal.ZERO; 
			//当天余额宝红运收益
			BigDecimal  hyProfitAmount = BigDecimal.ZERO; 
			//判断基准份额小于0忽略
			if(bmLastAmount.compareTo(BigDecimal.ZERO)==-1){
				continue;
			}
			//获取会员当日红运有效份额
			hyLastAmount = balanceGemRecordDao.selMemberDayInvalidAmount(start,end,accountId);
			if(hyLastAmount == null){
				hyLastAmount = BigDecimal.ZERO;
			}
			//获取会员次日零点到八点转出的非有效份额     转入金额-转出金额
			Calendar cal = Calendar.getInstance();
			cal.setTime(start);
			cal.add(Calendar.DAY_OF_MONTH,1);
			Date selstart = cal.getTime();
			cal.add(Calendar.HOUR_OF_DAY, 8);
			Date selend  = cal.getTime();
			BigDecimal invalidAmount = balanceGemRecordDao.selMemberDayInvalidAmount(selstart,selend,accountId);
			//无效金额小于0证明八点前存在转出金额  
			if(invalidAmount!=null && invalidAmount.compareTo(BigDecimal.ZERO)==-1){
				bmLastAmount = bmLastAmount.add(invalidAmount);
				hyLastAmount = hyLastAmount.add(invalidAmount);
			}
			//获取相匹配策略  以基准收益匹配策略为准
			MemberBalanceGemStrategy memberStrategys = filterByMemberLevelandMoney(accountId,stationId,strategys,bmLastAmount);
			boolean flag = true;
			//无法找到会员对应策略
			if(memberStrategys ==null){
				remark = "会员"+record.getAccount()+date+"基准份额为"+bmLastAmount.floatValue()+",未找到对应策略。余额宝返利失败";
//				SysLogUtil.log(remark,"",record.getAccountId(),record.getAccount(),null,stationId,LogType.AGENT_SYSTEM.getType(),LogType.AGENT_SYSTEM.getPlatform(),null);
				flag = false;
			}else{
				//计算当日余额宝基准收益
				bmProfitAmount = countProfitAmountDay(memberStrategys,bmLastAmount,1);
				//计算红运收益
				if(hyLastAmount.compareTo(BigDecimal.ZERO)==1){
					hyProfitAmount = countProfitAmountDay(memberStrategys,hyLastAmount,2);
				}
				remark = "会员"+record.getAccount()+date+"基准有效份额为："+bmLastAmount.floatValue()+",红运有效份额为："+hyLastAmount.floatValue()
				+",基准收益为："+bmProfitAmount+",红运收益为："+hyProfitAmount;
				flag = true;
			}
			
			//返利少于1分钱就不返了 数据卡死
			BigDecimal fanliSum = bmProfitAmount.add(hyProfitAmount);
			if(fanliSum.compareTo(new BigDecimal("0.01"))<0) {
				continue;
			}
			
			//保存记录
			MemberBalanceGemRecord newRecord = new MemberBalanceGemRecord();
			Calendar temp = Calendar.getInstance();
			temp.setTime(end);
			temp.add(Calendar.HOUR_OF_DAY,1);
			temp.set(Calendar.HOUR_OF_DAY, 8);
			temp.set(Calendar.MINUTE, 0);
			temp.set(Calendar.SECOND, 0);
			temp.set(Calendar.MILLISECOND, 0);
			end = temp.getTime();
			newRecord.setCreateDatetime(end);
			newRecord.setType(MemberBalanceGemRecord.TYPE_REBATE);
			newRecord.setCreateUserId(null);
			newRecord.setOrderNo(Snowflake.getOrderNo() + "");
			newRecord.setAccount(record.getAccount());
			newRecord.setAccountId(accountId);
			newRecord.setStationId(stationId);
			newRecord.setRemark(remark);
			//返利日期
			newRecord.setRebateDate(DateUtil.toDate(date));
			if(flag){
				//当日收益=基准收益+红运收益
				BigDecimal sumAmount = bmProfitAmount.add(hyProfitAmount);
				BigDecimal cur = null;
				newRecord.setMoney(sumAmount);
				newRecord.setStauts(MemberBalanceGemRecord.STATUS_SUCCESS);
				//当日收益大于0.01时更改会员余额宝余额及累积收益
				if(sumAmount.compareTo(new BigDecimal(0.01))==1){
					MnyMoneyVo moneyVo = new MnyMoneyVo();
					moneyVo.setStationId(stationId);
					moneyVo.setAccountId(accountId);
					moneyVo.setMoneyRecordType(MoneyRecordType.BALANCE_GEM_REBATE);
					moneyVo.setBgIncome(sumAmount);
					moneyVo.setBgMoney(sumAmount);
					cur = moneyService.updateBalanceGem(moneyVo);
					//保存会员每日报表记录
					SysAccountDailyMoney dm = new SysAccountDailyMoney();
					dm.setAccountId(accountId);
					dm.setBalanceGemAmount(sumAmount);
					dm.setStatDate(new Date());
					SysAccount acc = accountDao.getFromCache(accountId);
					sysAccountDailyMoneyService.editBalanceGemRepost(dm, acc);
					//返利成功计算流水打码量
					//打码量 = 转出金额可达到的基准收益*转出流水打码量倍数
//					BigDecimal tempAmount = countProfitAmountDay(memberStrategys,money,1);
					//获取到的流水打码量是null
					BigDecimal bet_multiple = memberStrategys.getBetMultiple();
					if(bet_multiple==null || StringUtil.isEmpty(bet_multiple.toString())) {
						bet_multiple=BigDecimal.ZERO;
					}
					BigDecimal betNumMultiple = sumAmount.multiply(bet_multiple).setScale(0, BigDecimal.ROUND_CEILING);
					mnyBetNumberDao.updateDrawCheck(accountId,stationId, betNumMultiple,
							BetNumRecord.TYPE_BALANCE_GEM_REBATE, "余额宝返利打码量",false);
					
				}else{
					Map map = moneyService.getMoneyByAccount(record.getAccount(), stationId);
					cur = StringUtil.toBigDecimal(map.get("balanceGemMoney"));
					sumAmount = BigDecimal.ZERO;
				}
				newRecord.setBackMoney(cur);
				newRecord.setBeforeMoney(cur.subtract(sumAmount));
				balanceGemRecordDao.insert(newRecord);
				
				
			}else{
				newRecord.setStauts(MemberBalanceGemRecord.STATUS_FAIL);
				balanceGemRecordDao.insert(newRecord);
			}
			logger.info("会员"+record.getAccount()+"结束余额宝返利计算");
		}
	}
	
	/**
	 * 当日余额宝盈利金额
	 * @param memberStrategys
	 * @param lastAmount
	 * @return
	 */
	private BigDecimal countProfitAmountDay(MemberBalanceGemStrategy strategy, BigDecimal amount,Integer type) {
		BigDecimal profitAmount = BigDecimal.ZERO;
		BigDecimal annualizedRate = BigDecimal.ZERO;
		BigDecimal theThousand = new BigDecimal(10000);
		//type为1计算基准收益 为2计算红运收益
		if(type==1){
			annualizedRate = strategy.getBenchmarkAnnualYield().divide(new BigDecimal(100),2,BigDecimal.ROUND_HALF_UP);
		}else if(type==2){
			annualizedRate = strategy.getHongyunAnnualYield().divide(new BigDecimal(100),2,BigDecimal.ROUND_HALF_UP);
		}
		
		//计算万份收益(元)：1万元存1天可得的收益=10000*年化收益率/360；
		BigDecimal tenThousandGains = theThousand.multiply(annualizedRate).divide(new BigDecimal(360),6,BigDecimal.ROUND_HALF_UP);
		//计算基准或红运收益     基准收益=基准有效份额*基准万份收益/10000；   红运收益=红运有效份额*红运万份收益/10000；
		profitAmount = amount.multiply(tenThousandGains).divide(theThousand,6,BigDecimal.ROUND_HALF_UP);
		return profitAmount;
	}
	/**
	 * 计算万份收益率
	 * @param memberStrategys
	 * @param lastAmount
	 * @return
	 */
	@Override
	public MemberBalanceGemStrategy countTheThousand(MemberBalanceGemStrategy strategy) {
		BigDecimal profitAmount = BigDecimal.ZERO;
		BigDecimal bmannualizedRate = BigDecimal.ZERO;
		
		BigDecimal hyannualizedRate = BigDecimal.ZERO;
		BigDecimal theThousand = new BigDecimal(10000);
		//type为1计算基准收益 为2计算红运收益
		bmannualizedRate = strategy.getBenchmarkAnnualYield().divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
		hyannualizedRate = strategy.getHongyunAnnualYield().divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP);
		
		//计算万份收益(元)：1万元存1天可得的收益=10000*年化收益率/360；
		BigDecimal bmTenThousandGains = theThousand.multiply(bmannualizedRate).divide(new BigDecimal(360),6,BigDecimal.ROUND_HALF_UP);
		BigDecimal hyTenThousandGains = theThousand.multiply(hyannualizedRate).divide(new BigDecimal(360),6,BigDecimal.ROUND_HALF_UP);
		strategy.setBenchmarkThousandGains(bmTenThousandGains.setScale(2, BigDecimal.ROUND_DOWN));
		strategy.setHongyunThousandGains(hyTenThousandGains.setScale(2, BigDecimal.ROUND_DOWN));
		return strategy;
		
	}
	/**
	 * 根据会员层级过滤策略
	 * 
	 * @param accountId
	 * @param stationId
	 * @param list
	 * @return
	 */
	@Override
	public MemberBalanceGemStrategy filterByMemberLevelandMoney(Long accountId, Long stationId, List<MemberBalanceGemStrategy> list,BigDecimal lastAmount) {
		if (list == null || list.isEmpty())
			return null;
		SysAccount acc = accountDao.findOneByAccountIdAndStationId(accountId, stationId);
		if (acc == null || acc.getLevelGroup() == null || acc.getLevelGroup() <= 0) {
			return null;
		}
		List<Long> levelList = null;
		MemberBalanceGemStrategy strategy = null;
		for (MemberBalanceGemStrategy m : list) {
			//获取策略对应等级
			levelList = memberBalanceGemStrategyLevelDao.findLevels(m.getId(), stationId);
			//判断会员等级是否匹配策略
			if (levelList == null || levelList.isEmpty() || levelList.contains(acc.getLevelGroup())) {
				//判断金额是否匹配
				if(lastAmount.compareTo(m.getMinMoney())>-1 && lastAmount.compareTo(m.getMaxMoney())<1){
					strategy = m;
					break;
				}
			}
		}
		return strategy;
	}

	public static void main(String[] args) {
		BigDecimal result = new BigDecimal(10000).multiply(new BigDecimal(0.12)).divide(new BigDecimal(360),6,BigDecimal.ROUND_HALF_UP);
		System.out.println(result);
	}

	@Override
	public void transferBalanceGem(BigDecimal money, boolean isAdd) {
		Long accountId = UserUtil.getUserId();
		String account = UserUtil.getUserAccount();
		Long stationId = StationUtil.getStationId();
		//备注
		String remark = "";
		//策略集合
		List<MemberBalanceGemStrategy> strategys = null;
		//判断站点余额宝开关状态
		String bgFlag = StationConfigUtil.get(stationId, StationConfig.onoff_balance_gem);
		//余额宝处于开启或者禁止转入状态才可操作金额
		if(!"on".equals(bgFlag) && !"ban".equals(bgFlag)){
			throw new GenericException("余额宝未开启，无法进行操作");
		}
		//余额宝禁止转入状态下无法进行转入操作
		if("ban".equals(bgFlag)&&isAdd){
			throw new GenericException("当期余额宝已禁止转入，请联系客服");
		}
		SysAccount acc = (SysAccount) SysUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
	        throw new GenericException("此账号不能进行余额宝操作");
	    }
		
		//操作后余额
		BigDecimal cur = null;
		//新增余额宝账变记录
		MemberBalanceGemRecord newRecord = new MemberBalanceGemRecord();
		newRecord.setCreateUserId(null);
		newRecord.setOrderNo(Snowflake.getOrderNo() + "");
		newRecord.setAccount(account);
		newRecord.setAccountId(accountId);
		newRecord.setStationId(stationId);
		//新增会员账变记录
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setAccountId(accountId);
		moneyVo.setBgIncome(BigDecimal.ZERO);
		moneyVo.setBgMoney(money);
		//转入
		if(isAdd){
			newRecord.setType(MemberBalanceGemRecord.TYPE_TRANSFER);
			moneyVo.setMoneyRecordType(MoneyRecordType.BALANCE_GEM_TRAN);
			remark = "余额宝转入金额";
		}else{
			newRecord.setType(MemberBalanceGemRecord.TYPE_TRANSFER_OUT);
			moneyVo.setMoneyRecordType(MoneyRecordType.BALANCE_GEM_TRAN_OUT);
			remark = "余额宝转出金额";
			//获取站点余额宝策略
			strategys = memberBalanceGemStrategyDao.findStationStrategy(stationId);
			if(strategys==null || strategys.isEmpty()){
				throw new GenericException("未开启有效策略，转出金额失败");
			}
		}
		//更新余额宝余额
		cur = moneyService.updateBalanceGem(moneyVo);
		
		//转出金额需要添加打码量
//		if(!isAdd){
//			BigDecimal betNumMultiple = null;
//			//获取当前余额匹配策略  读取转出流水打码量
//			MemberBalanceGemStrategy memberStrategys = filterByMemberLevelandMoney(accountId,stationId,strategys,cur);
//			if(memberStrategys==null){
//				throw new GenericException("未开启有效策略，转出金额失败");
//			}
//			//打码量 = 转出金额可达到的基准收益*转出流水打码量倍数
//			BigDecimal tempAmount = countProfitAmountDay(memberStrategys,money,1);
//			betNumMultiple = tempAmount.multiply(memberStrategys.getBetMultiple()).setScale(0, BigDecimal.ROUND_CEILING);
//			mnyBetNumberDao.updateDrawCheck(accountId,stationId, betNumMultiple,
//					BetNumRecord.TYPE_BALANCE_GEM_TRANSFER_OUT, "余额宝转出金额", true);
//		}
		//更新会员账变会员余额
		moneyVo.setMoney(money);
		moneyVo.setRemark(remark);
		moneyService.updMnyAndRecord(moneyVo);
		//保存余额宝变更记录
		newRecord.setBackMoney(cur);
		if(!isAdd){
			money = money.negate();
		}
		newRecord.setMoney(money);
		newRecord.setBeforeMoney(cur.subtract(money));
		newRecord.setCreateDatetime(new Date());
		newRecord.setStauts(MemberBalanceGemRecord.STATUS_SUCCESS);
		newRecord.setRemark(remark);
		balanceGemRecordDao.insert(newRecord);
	}

	@Override
	public BigDecimal getYesterdayIncome(Date start, Long accountId) {
		return balanceGemRecordDao.getYesterdayIncome(start, accountId);
	}
}