package com.game.service.impl;

import java.util.Date;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.dao.SysAccountDao;
import com.game.dao.SysMessageDao;
import com.game.dao.SysUserMessageDao;
import com.game.model.SysAccount;
import com.game.model.SysMessage;
import com.game.model.SysUserMessage;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MessageVo;
import com.game.service.SysMessageService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class SysMessageServiceImpl implements SysMessageService {

	@Autowired
	private SysMessageDao messageDao;

	@Autowired
	private SysUserMessageDao userMessageDao;
	@Autowired
	private SysAccountDao accountDao;

	@Override
	public Page getMessagePage(MessageVo mvo) {
		return messageDao.getPage(mvo);
	}

	@Override
	public void updMessage(SysMessage message) {

		SysMessage saveMessage = null;
		if (Validator.isNull(message.getId())) {
			throw new GenericException("该站内信不存在！");
		}
		saveMessage = messageDao.get(message.getId());

		if (saveMessage == null || !StringUtil.equals(StationUtil.getStationId(), saveMessage.getStationId())) {
			throw new GenericException("非法请求");
		}

		saveMessage.setMessage(message.getMessage());
		saveMessage.setTitle(message.getTitle());

		messageDao.save(saveMessage);
	}

	@Override
	public void sendMessage(MessageVo mvo) {
		SysMessage message = new SysMessage();
		SysUserMessage userMessage = new SysUserMessage();
		SysAccount sysAccount = null;
		Long stationId = StationUtil.getStationId();
		Long accountId = mvo.getAccountId();
		String account = mvo.getAccount();
		if (StringUtil.equals(SysMessage.TYPE_DEFAULT, mvo.getType())) {
			if (Validator.isNotNull(accountId)) {
				sysAccount = accountDao.getFromCache(accountId);
				if (sysAccount == null || !StringUtil.equals(stationId, sysAccount.getStationId()) || StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, sysAccount.getAccountType()) || StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER, sysAccount.getAccountType())) {
					throw new GenericException("非法请求！");
				}
				account = sysAccount.getAccount();
			} else if (StringUtil.isNotEmpty(account)) {
				AccountVo avo = new AccountVo();
				avo.setAccount(account);
				avo.setStationId(stationId);
				sysAccount = accountDao.queryAccount(avo);
				if (sysAccount == null || !StringUtil.equals(stationId, sysAccount.getStationId()) || StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, sysAccount.getAccountType()) || StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER, sysAccount.getAccountType())) {
					throw new GenericException("接收账号不存在！");
				}
				accountId = sysAccount.getId();
			}
			sendUserMessage(mvo.getTitle(),mvo.getMessage(),stationId,accountId,account);
		} else {
			message.setMessage(mvo.getMessage());
			message.setTitle(mvo.getTitle());
			message.setStationId(stationId);
			message.setCreateDatetime(new Date());
			message.setType(mvo.getType());
			messageDao.save(message);
		}
	}
	
	@Override
	public void sendUserMessage(String title,String content,Long stationId,Long accountId,String account){
		SysMessage message = new SysMessage();
		SysUserMessage userMessage = new SysUserMessage();
		message.setMessage(content);
		message.setTitle(title);
		message.setStationId(stationId);
		message.setCreateDatetime(new Date());
		message.setType(SysMessage.TYPE_DEFAULT);
		messageDao.save(message);

		userMessage.setAccountId(accountId);
		userMessage.setAccount(account);
		userMessage.setMessageId(message.getId());
		userMessage.setStatus(SysUserMessage.STATUS_UNREAD);
		userMessageDao.save(userMessage);
		RedisAPI.delCache(Contants.AGENT_MESSAGE_COUNT + accountId);
	}

	@Override
	public void delMessage(Long messageId) {
		SysMessage delMessage = messageDao.get(messageId);
		if (delMessage == null || !StringUtil.equals(delMessage.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		userMessageDao.delByMessageId(messageId);
		messageDao.delete(messageId);
		String title = delMessage.getTitle();
		SysLogUtil.log("删除了标题是"+title+"的站内信" , LogType.AGENT_SYSTEM);
	}

	@Override
	public Integer getMessageCount() {
		int count = 0;
		if (!UserUtil.isLogin()) {
			return count;
		}

		String redisKey = Contants.AGENT_MESSAGE_COUNT + UserUtil.getUserId();
		if (RedisAPI.exists(redisKey)) {
			count = StringUtil.toInteger(RedisAPI.getCache(redisKey), 0);
		} else {
			count = userMessageDao.getMessageCount();
			RedisAPI.addCache(redisKey, count + "", Contants.AGENT_MESSAGE_COUNT_TIMEOUT);
		}
		return count;
	}

	@Override
	public void delUserMessage(Long userMessageId) {
		if (userMessageId == null || userMessageId == 0L) {
			throw new GenericException("不能删除未读信息，请先查看信息。");
		}
		SysUserMessage delMessage = userMessageDao.get(userMessageId);
		if (delMessage == null || !StringUtil.equals(delMessage.getAccountId(), UserUtil.getUserId())) {
			throw new GenericException("非法请求！");
		}
		userMessageDao.delMessage(userMessageId);
		RedisAPI.delCache(Contants.AGENT_MESSAGE_COUNT + delMessage.getAccountId());
	}

	public void readMessage(Long userMessageId,Long status) {
		SysUserMessage sum = new SysUserMessage();
		SysMessage sm = messageDao.get(userMessageId);
		if (sm == null) {
			throw new GenericException("非法请求！");
		} else {
			if (StringUtil.equals(SysMessage.TYPE_ALL, sm.getType())) {
				Integer delMessage = userMessageDao.getCount(userMessageId);
				if (delMessage == 0) {
					sum.setAccount(UserUtil.getUserAccount());
					sum.setAccountId(UserUtil.getUserId());
					sum.setMessageId(userMessageId);
					if(null != status) {
						sum.setStatus(3l);
					}else {
						sum.setStatus(SysUserMessage.STATUS_READ);
					}
					userMessageDao.save(sum);
				} else {
						if(null != status) {
							userMessageDao.deleteMessage(userMessageId);
						}else {
							userMessageDao.readMessage(userMessageId);		
						}
				}
				RedisAPI.delCache(Contants.AGENT_MESSAGE_COUNT + UserUtil.getUserId());
			} else {
				Long userMsgId = userMessageDao.getUserMsgId(userMessageId);
				if (Validator.isNotNull(userMsgId)) {
					if(null != status) {
						userMessageDao.deleteMessage(userMessageId);
					}else {
						userMessageDao.readMessage(userMsgId);	
					}
				}
			}
		}
	}
	@Override
	public void readMessage(Long userMessageId) {
		readMessage(userMessageId,null);
	}
	@Override
	public SysMessage getOne(Long id, Long stationId) {
		if (id != null) {
			SysMessage it = messageDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

	@Override
	public void delUserNewMessage(Long messageId) {
		// TODO Auto-generated method stub
		readMessage(messageId,3l);
	}

	@Override
	public void deleteSysUserMessageByDate(Date createDatetime) {
		userMessageDao.deleteSysUserMessageByDate(createDatetime);
		messageDao.deleteSysMessageByDate(createDatetime);
	}
	
}
