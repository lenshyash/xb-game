package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.SysAccountDao;
import com.game.dao.platform.AgentProfitShareHandlerRecordDao;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.AgentProfitShareHandlerRecord;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.AgentProfitShareHandlerRecordService;
import com.game.service.MnyMoneyService;

@Service
public class AgentProfitShareHandlerRecordServiceImpl implements AgentProfitShareHandlerRecordService {
	private Logger logger = Logger.getLogger(AgentProfitShareHandlerRecordService.class);

	@Autowired
	private AgentProfitShareHandlerRecordDao agentProfitShareHandlerRecordDao;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private SysAccountDao sysaccountDao;

	@Override
	public Page page(String account,String agentName,String parentNames, Date startDate, Date endDate, Integer status, Long stationId) {
		String children = "";
		if(StringUtil.isNotEmpty(parentNames)) {
			Map agent = sysaccountDao.queryAccount(parentNames, stationId);
			if(agent == null) {
				throw new GenericException("代理线不存在");
			}
			children = agent.get("parents").toString();
			if(StringUtil.isEmpty(children)) {
				children = ",";
			}
			children = children +agent.get("id").toString()+",";
		}
		Page page = agentProfitShareHandlerRecordDao.page(account,agentName,children, startDate, endDate, status, stationId);
		return page;
	}

	@Override
	public void doProfitShare(Long stationId, String[] ids, String[] moneys) {
		if (ids == null || ids.length == 0 || moneys == null || moneys.length == 0) {
			throw new GenericException("参数错误");
		}
		AgentProfitShareHandlerRecord record = null;
		BigDecimal money = null;
		BigDecimal oldMoney = null;
		String remark = "";
		for (int i = 0; i < ids.length; i++) {
			money = StringUtil.toBigDecimal(moneys[i]);
			if(money.compareTo(BigDecimal.ZERO) < 0) {
				logger.error("金额小于0的不处理");
				continue;
			}
			record = agentProfitShareHandlerRecordDao.get(StringUtil.toLong(ids[i]));
			if (record == null || !StringUtil.equals(record.getStationId(), stationId)) {
				throw new GenericException("非法请求");
			}
			oldMoney = record.getMoney();
			remark = record.getRemark();
			if(StringUtil.isNotEmpty(remark)) {
				remark +="<br>";
			}
			remark +="原金额："+oldMoney+"，放发金额："+money;
			if(agentProfitShareHandlerRecordDao.updateToHandler(record.getId(),stationId,money,remark) != 1) {
				continue;
			}
			//大于零时才发钱
			if(money.compareTo(BigDecimal.ZERO) == 1) {
				balanceMoney(record.getStationId(), record.getAccount(), record.getAccountId(),
					money);
			}
		}
	}

	private void balanceMoney(Long stationId, String account, Long accountId, BigDecimal money) {
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setAccount(account);
		moneyVo.setAccountId(accountId);
		moneyVo.setMoney(money);
		moneyVo.setMoneyRecordType(MoneyRecordType.AGENT_PROFIT_SHARE_ADD);
		moneyVo.setRemark("");
		mnyMoneyService.updMnyAndRecord(moneyVo);
	}
}
