package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.dao.MnyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.dao.SysStationDao;
import com.game.event.SysStationSaveEvent;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.SysStation;
import com.game.model.vo.StationVo;
import com.game.service.AgentBaseConfigService;
import com.game.service.SysStationDomainService;
import com.game.service.SysStationService;
import com.game.util.MD5Util;
import com.game.util.MemberLevelUtil;
import com.game.util.SpringUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;
import com.game.util.ValidateUtil;

@Repository
public class SysStationServiceImpl implements SysStationService {

	@Autowired
	private SysStationDao sysStationDao;

	@Autowired
	private SysAccountDao sysAccountDao;

	@Autowired
	private SysAccountInfoDao sysAccountInfoDao;

	@Autowired
	private MnyMoneyDao moneyDao;
	
	@Autowired
	private SysStationDomainService sysStationDomainService;
	
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	
	@Override
	public Page<Map> getPage(StationVo svo) {
		return sysStationDao.getPage(svo);
	}

	@Override
	public void saveStation(SysStation station, String pwd, String rpwd) {
		Long stationId = station.getId();

		if (sysStationDao.isNotUnique(station, "floder")) {
			throw new GenericException("名别:\"" + station.getFloder() + "\"已经存在");
		}
		SysAccount account = new SysAccount();
		SysAccount agent = new SysAccount();
		// stationId为空则为新增站点
		SysStation saveStation = null;
		boolean addFlag = false;
		SysAccountInfo aif = new SysAccountInfo();
		SysAccountInfo agentGif = new SysAccountInfo();
		if (stationId == null) {
			addFlag = true;
			account.setAccount(station.getAccount());
			account.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			if (sysAccountDao.isNotUnique(account, "account,accountType")) {
				throw new GenericException("该超级管理员已存在！");
			}

			if (!ValidateUtil.isUsername(account.getAccount())) {
				throw new GenericException("用户账号格式不正确！");
			}

			agent.setAccount(station.getAgentName());
			agent.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			if (StringUtil.isEmpty(pwd)) {
				throw new GenericException("密码不能为空!");
			}
			if (StringUtil.isEmpty(rpwd)) {
				throw new GenericException("确认密码不能为空!");
			}

			if (!pwd.equals(rpwd)) {
				throw new GenericException("两次密码不一致!");
			}

			if (!ValidateUtil.isPassword(pwd)) {
				throw new GenericException("密码格式不正确！");
			}

			String ipAddress = UserUtil.getIpAddress();

			// 租户不存在，创建租户超级管理员
			account.setAgentId(0L);
			account.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER);
			account.setStationId(0l);
			account.setAccountStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
			account.setLevel(1l);
			account.setPassword(MD5Util.getMD5String(account.getAccount(), pwd));
			account.setRegisterIp(ipAddress);

			// 增加默认代理
			agent.setAgentId(0L);
			agent.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			agent.setStationId(0l);
			agent.setAccountStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
			agent.setLevel(1l);
			agent.setReportType(SysAccount.REPORT_TYPE_NORMAL);
			agent.setPassword(MD5Util.getMD5String(account.getAccount(), Contants.INIT_AGENT_PASSWORD));
			account.setRegisterIp(ipAddress);
			account.setParents("");
			agent.setParents("");
			account = sysAccountDao.save(account);
			agent = sysAccountDao.save(agent);

			aif.setAccountId(account.getId());
			aif.setAccount(account.getAccount());
			aif.setStationId(stationId);
			agentGif.setAccountId(agent.getId());
			agentGif.setAccount(agent.getAccount());
			sysAccountInfoDao.insert(aif);
			sysAccountInfoDao.insert(agentGif);
			MnyMoney money = new MnyMoney();
			money.setAccountId(account.getId());
			money.setMoneyTypeId(MnyMoney.MONEY_RMB);
			money.setMoney(new BigDecimal(0));
			moneyDao.insert(money);
			MnyMoney agentMoney = new MnyMoney();
			agentMoney.setAccountId(agent.getId());
			agentMoney.setMoneyTypeId(MnyMoney.MONEY_RMB);
			agentMoney.setMoney(new BigDecimal(0));
			moneyDao.insert(agentMoney);
			station.setCreateDatetime(new Date());
			saveStation = station;
			saveStation.setAccountId(account.getId());
			saveStation.setAgentId(agent.getId());
		} else {
			// 修改站点
			saveStation = sysStationDao.get(stationId);
			saveStation.setName(station.getName());
			saveStation.setStatus(station.getStatus());
			saveStation.setFloder(station.getFloder());
			SysLogUtil.log("更新平台站点:" + station.getName(), LogType.ADMIN_PLATFORM);
		}
		sysStationDao.save(saveStation);
		if (addFlag) {
			stationId = saveStation.getId();
			account.setStationId(stationId);
			aif.setStationId(stationId);
			agentGif.setStationId(stationId);
			agent.setStationId(stationId);
			String parents = "";
			String parentNames = "";
			agent.setParents(parents);
			agent.setParentNames(parentNames);
			sysAccountDao.save(account);
			sysAccountDao.save(agent);
			sysAccountInfoDao.save(aif);
			sysAccountInfoDao.save(agentGif);
			SysLogUtil.log(
					"新增平台站点:" + station.getName() + ",超级管理员:" + account.getAccount() + ",默认代理:" + agent.getAccount(),
					LogType.ADMIN_PLATFORM);
//			SpringUtil.publishEvent(new SysStationSaveEvent(saveStation));
			//创建时间站点的默认等级
			MemberLevelUtil.createDefaultLevel(stationId);
		}
		CacheUtil.flashDB(CacheType.STATION_DOMAIN);
	}

	@Override
	public void updStatus(Long stationId, Long status) {
		SysStation station = sysStationDao.get(stationId);
		if (station.getStatus() != status) {
			if (StringUtil.equals(status, SysStation.STATUS_DISABLE)) {
				SysLogUtil.log("禁用平台站点:" + station.getName(), LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("启用平台站点:" + station.getName(), LogType.ADMIN_PLATFORM);
			}
			station.setStatus(status);
			sysStationDao.save(station);
			CacheUtil.flashDB(CacheType.STATION_DOMAIN);
		}
	}

	@Override
	public List<Map> getStationCombo(Long status) {
		return sysStationDao.getStationCombo(status);
	}

	@Override
	public List<SysStation> getAllActive() {
		return sysStationDao.findByStatus(SysStation.STATUS_ENABLE);
	}

	@Override
	public SysStation findOneById(Long stationId) {
		SysStation station = sysStationDao.findOneById(stationId);
		return station;
	}
	
	/**
	 * 物理删除站点  站点domain 站点配置
	 */
	public void deleteStationById(Long stationId) {
		sysStationDao.deleteStationById(stationId);
		sysStationDomainService.deleteDomainByStationId(stationId);
		agentBaseConfigService.deleteAgentConfigValByStationId(stationId);
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：删除站点编号为"+stationId+"的站点信息", LogType.ADMIN_SYSTEM);
	}

	@Override
	public SysStation findOneByFolder(String folder) {
		return sysStationDao.findOneByFolder(folder);
	}

}