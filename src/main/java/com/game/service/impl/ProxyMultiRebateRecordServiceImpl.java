package com.game.service.impl;

import java.util.Date;

import org.jay.frame.jdbc.Page;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.ProxyMultiRebateRecordDao;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.service.ProxyMultiRebateRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Service
public class ProxyMultiRebateRecordServiceImpl implements ProxyMultiRebateRecordService {
	private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ProxyMultiRebateRecordServiceImpl.class);
	
	@Autowired
	private ProxyMultiRebateRecordDao proxyMultiRebateRecordDao;
	@Autowired
	private ProxyMultiRebateCoreService proxyMultiRebateCoreService;
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<ProxyMultiRebateRecord> getPageConfig(ProxyMultiRebateRecord proxyMultiRebateRecord,String startTime,String endTime) {
		Date start=DateUtil.validStartDateTime(startTime );
		Date end=DateUtil.validEndDateTime(endTime);
		return proxyMultiRebateRecordDao.page(proxyMultiRebateRecord,start,end,StationUtil.isAdminPage());
	}
	
	@Override
	public int cancleProxyMultiRebate(Long betId,Long stationId) {
		try {
			return proxyMultiRebateCoreService.updateDateAndSubMoney4MultiRebate(betId, null, stationId);
		} catch (Exception e) {
			logger.error("彩票 投注id["+betId+"] , 站点id["+stationId+"] 反点取消异常",e);
		}
		return 0;
	}

}
