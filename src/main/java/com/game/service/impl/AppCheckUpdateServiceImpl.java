package com.game.service.impl;

import com.game.dao.AppCheckUpdateDao;
import com.game.model.AppUpdate;
import com.game.service.AppCheckUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AppCheckUpdateServiceImpl implements AppCheckUpdateService {

    @Autowired
    private AppCheckUpdateDao appCheckUpdateDao;

    @Override
    public List<AppUpdate> getAppUpdates() {
        return appCheckUpdateDao.getAppUpdates();
    }

    @Override
    public List<AppUpdate> getLastUpdateInfo(String version) {
        return appCheckUpdateDao.getLastUpdateInfo(version);
    }

    @Override
    public void deleteAppUpdate(String version) {
        appCheckUpdateDao.deleteAppUpdate(version);
    }

	@Override
	public AppUpdate getLastAppVerByFlag(String flag,Long stationId,String domain,String type) {
		// TODO Auto-generated method stub
		return appCheckUpdateDao.getLastAppVerByFlag(flag,stationId,domain,type);
	}

	@Override
	public AppUpdate saveAppUpdate(AppUpdate app) {
		// TODO Auto-generated method stub
		return appCheckUpdateDao.save(app);
	}
    
}
