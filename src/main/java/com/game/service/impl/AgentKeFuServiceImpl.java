package com.game.service.impl;

import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.AgentKeFuDao;
import com.game.model.platform.AgentKeFu;
import com.game.service.AgentKeFuService;

@Service
public class AgentKeFuServiceImpl implements AgentKeFuService {
	@Autowired
	private AgentKeFuDao agentKeFuDao;

	@Override
	public Page page(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentKeFuDao.page(stationId);
	}

	@Override
	public void addSave(AgentKeFu kefu) {
		agentKeFuDao.save(kefu);
	}

	@Override
	public AgentKeFu getOne(Long id) {
		AgentKeFu keFu = agentKeFuDao.get(id);
		return keFu;
	}

	@Override
	public void delete(Long id) {
		agentKeFuDao.delete(id);
	}

	@Override
	public void openCloseH(Long id, Integer status) {
		agentKeFuDao.openCloseH(id, status);
	}

	@Override
	public List<AgentKeFu> getActiveList(Long stationId,Integer type) {
		List<AgentKeFu> list = agentKeFuDao.getActiveList(stationId,type);
		return list;
	}

}
