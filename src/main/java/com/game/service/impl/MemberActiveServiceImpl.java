package com.game.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONArray;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.dao.MemberActiveAwardDao;
import com.game.dao.MemberActiveDao;
import com.game.dao.MemberActivePlayNumDao;
import com.game.dao.MemberActiveRecordDao;
import com.game.dao.MemberActiveVirtualRecordDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysStationDao;
import com.game.model.MemberActive;
import com.game.model.MemberActiveAward;
import com.game.model.MemberActivePlayNum;
import com.game.model.MemberActiveRecord;
import com.game.model.MemberActiveVirtualRecord;
import com.game.model.MemberRedPacket;
import com.game.model.MemberRedPacketRecord;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.dictionary.ScoreRecordType;
import com.game.model.vo.ActiveRecordVo;
import com.game.model.vo.ActiveVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.model.vo.MnyScoreVo;
import com.game.service.MemberActiveService;
import com.game.service.MnyMoneyService;
import com.game.service.MnyScoreService;
import com.game.util.DateUtil;
import com.game.util.RedSortEnum;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Repository
public class MemberActiveServiceImpl implements MemberActiveService {

	@Autowired
	SysAccountDao atDao;

	@Autowired
	MemberActiveDao maDao;

	@Autowired
	MemberActiveAwardDao awardDao;

	@Autowired
	MemberActiveRecordDao activeRecordDao;

	@Autowired
	MemberActivePlayNumDao playNumDao;

	@Autowired
	MnyScoreService scoreService;

	@Autowired
	SysStationDao stationDao;

	@Autowired
	MnyMoneyService moneyService;
	
	@Autowired
	MemberActiveVirtualRecordDao activeVirtualRecordDao;
	
	@Override
	public Page getPage(ActiveVo activeVo) {
		return maDao.getActivePage(activeVo);
	}

	@Override
	public void saveActive(MemberActive ma) {
		MemberActive saveMa = null;
		String imgPath = ma.getImgPath();
		Long activeType = ma.getActiveType();
		String activeName = ma.getActiveName();
		Long playCount = ma.getPlayCount();
		BigDecimal score = ma.getScore();
		Date begin = ma.getBeginDatetime();
		Date end = ma.getEndDatetime();
		if (StringUtil.isEmpty(activeName)) {
			throw new GenericException("奖项名称不能为空！");
		}
		if (StringUtil.isEmpty(imgPath)) {
			throw new GenericException("图片路径不能为空！");
		}
		if (StringUtil.isEmpty(begin)) {
			throw new GenericException("开始时间不能为空！");
		}
		if (StringUtil.isEmpty(end)) {
			throw new GenericException("结束时间不能为空！");
		}

		if (Validator.isNull(activeType)) {
			throw new GenericException("活动类型不能为空！");
		}
		if (Validator.isNull(playCount)) {
			throw new GenericException("当日可玩次数不能为空！");
		}
		if (Validator.isNull(score)) {
			throw new GenericException("消耗积分不能为空！");
		}

		if (ma.getId() != null) {
			saveMa = maDao.get(ma.getId());
			saveMa.setActiveName(activeName);
			saveMa.setActiveType(activeType);
			saveMa.setBeginDatetime(begin);
			saveMa.setEndDatetime(end);
			saveMa.setPlayCount(playCount);
			saveMa.setScore(score);
			saveMa.setActiveHelp(ma.getActiveHelp());
			saveMa.setImgPath(imgPath);
			saveMa.setActiveRole(ma.getActiveRole());
			saveMa.setActiveRemark(ma.getActiveRemark());

		} else {

			saveMa = ma;
			saveMa.setStationId(StationUtil.getStationId());
			saveMa.setCreateDatetime(new Date());
		}
		saveMa.setStatus(MemberActive.STATUS_DISABLED);
		maDao.save(saveMa);
	}

	@Override
	public void delActive(ActiveVo activeVo) {
		Long activeId = activeVo.getActiveId();
		if (Validator.isNull(activeId)) {
			throw new GenericException("非法请求！");
		}
		MemberActive ma = maDao.get(activeId);
		if (!StringUtil.equals(ma.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		// 删除活动记录
		maDao.delete(activeId);
		// 级联删除活动的奖项记录
		awardDao.delAllByActiveId(activeId);
		//删除虚拟记录缓存
		RedisAPI.delCache( 8,Contants.ACTIVE_RECORD_SHOW + activeId);
		// 删除虚拟记录
		activeVirtualRecordDao.delByActiveId(activeId);
		
	}

	@Override
	public void updStatus(Long maId, Long status) {
		MemberActive saveMa = maDao.get(maId);
		if (saveMa == null) {
			throw new GenericException("活动不存在");
		}

		if (StringUtil.equals(status, MemberActive.STATUS_ENABLED)) {
			MemberActive checkMa = new MemberActive();
			checkMa.setStatus(status);
			checkMa.setActiveType(saveMa.getActiveType());
			checkMa.setStationId(saveMa.getStationId());

			if (saveMa.getAwardCount()==null || saveMa.getAwardCount() < 1) {
				throw new GenericException("该活动未设置奖项！");
			}

			if (maDao.isNotUnique(checkMa, "status,activeType,stationId")) {
				throw new GenericException("同类活动只能启用一个！");
			}
		}
		if (!StringUtil.equals(saveMa.getStatus(), status)) {
			saveMa.setStatus(status);
			maDao.save(saveMa);
		}
	}

	@Override
	public void saveAwards(Long activeId, List<MemberActiveAward> awards) {

		if (awards == null || awards.size() == 0) {
			throw new GenericException("奖项数量设置不能为空！");
		}

		MemberActive ma = maDao.get(activeId);
		if (ma == null || !StringUtil.equals(ma.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		for (MemberActiveAward memberActiveAward : awards) {
			if (StringUtil.equals(memberActiveAward.getAwardType(), MemberActiveAward.AWARD_TYPE_MONEY)) {
				memberActiveAward.setProductId(0l);
			} else if (StringUtil.equals(memberActiveAward.getAwardType(), MemberActiveAward.AWARD_TYPE_PRODUCT)) {
				memberActiveAward.setAwardValue(BigDecimal.ZERO);
			} else if (StringUtil.equals(memberActiveAward.getAwardType(), MemberActiveAward.AWARD_TYPE_SCORE)) {
				memberActiveAward.setAwardValue(memberActiveAward.getAwardValue());
			}else {
				memberActiveAward.setProductId(0l);
				memberActiveAward.setAwardValue(BigDecimal.ZERO);
			}
			memberActiveAward.setActiveId(activeId);
		}
		ma.setAwardCount(awards.size());
		maDao.save(ma);

		awardDao.delAllByActiveId(activeId);
		awardDao.batchInsert(awards);
		RedisAPI.delCache(getAwardRedisKey(ma));

	}

	@Override
	public List<MemberActiveAward> getAwardsByActiveId(Long activeId) {
		List<MemberActiveAward> awards = null;
		MemberActive ma = maDao.get(activeId);
		if (ma == null || !StringUtil.equals(ma.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}
		String key = getAwardRedisKey(ma);
		if (RedisAPI.exists(key)) {
			return JSONArray.parseArray(RedisAPI.getCache(key), MemberActiveAward.class);
		} else {
			awards = awardDao.getAwardByActiveId(activeId);
			if (awards != null && awards.size() > 0) {
				RedisAPI.addCache(key, JsonUtil.toJson(awards), Contants.ACTIVE_TYPE_STATION_ACTIVEID_TIMEOUT, RedisAPI.DEFAULT_DB_INDEX);
			}
		}
		return awards;
	}

	private String getAwardRedisKey(MemberActive ma) {
		return Contants.ACTIVE_TYPE_STATION_ACTIVEID + ma.getActiveType() + ":" + ma.getStationId() + ":" + ma.getId();
	}

	@Override
	public MemberActiveAward play(Long activeId, Long memberId) {
		SysAccount member = atDao.get(memberId);
		if (member == null || !StringUtil.equals(member.getStationId(), StationUtil.getStationId()) || (!StringUtil.equals(member.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER) && !StringUtil.equals(member.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT))) {
			throw new GenericException("非法请求！");
		}
		MemberActive ma = maDao.get(activeId);

		if (ma == null || !StringUtil.equals(ma.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}
		Date curDatetime = new Date();

		if (!StringUtil.equals(ma.getStatus(), MemberActive.STATUS_ENABLED)) {
			throw new GenericException("活动已关闭！");
		}

		if (curDatetime.before(ma.getBeginDatetime()) || curDatetime.after(ma.getEndDatetime())) {
			throw new GenericException("请在活动期间参与！");
		}

		if (ma.getScore() == null) {
			throw new GenericException("活动积分设置异常，请联系管理员！");
		}
		if (member.getScore() == null || member.getScore().compareTo(ma.getScore()) == -1) {
			throw new GenericException("积分不足，无法参与抽奖");
		}

		MemberActivePlayNum playNum = playNumDao.get(member.getId());

		if (playNum != null && playNum.getActiveNum() >= ma.getPlayCount() && DateUtil.isToday(playNum.getCurrentDate())) {
			throw new GenericException("您今日参与活动次数已达当日上限！");
		}

		if (playNum == null) {
			playNum = new MemberActivePlayNum();
			playNum.setAccountId(member.getId());
			playNum.setActiveNum(1l);
			playNum.setCurrentDate( new Date());
			playNumDao.insert(playNum);
		} else {
			if(DateUtil.isToday(playNum.getCurrentDate())){
				playNum.setActiveNum(playNum.getActiveNum() + 1l);
			}else{
				playNum.setActiveNum(1l);
				playNum.setCurrentDate( new Date());
			}
			playNumDao.update(playNum);
		}

		MemberActiveAward result = activeHandler(getAwardsByActiveId(activeId));
		// 奖项结果不是中奖类型，不插入中奖记录
		if (!StringUtil.equals(result.getAwardType(), MemberActiveAward.AWARD_TYPE_NONE)) {

			BigDecimal awardValue = BigDecimal.ZERO;
			Long productId = 0l;

			// 奖项结果是再多把面值带入中奖记录，是商品把商品ID带入中奖记录
			if (StringUtil.equals(result.getAwardType(), MemberActiveAward.AWARD_TYPE_MONEY)) {
				awardValue = result.getAwardValue();
			} else if (StringUtil.equals(result.getAwardType(), MemberActiveAward.AWARD_TYPE_PRODUCT)) {
				productId = result.getProductId();
			}else if(StringUtil.equals(result.getAwardType(), MemberActiveAward.AWARD_TYPE_SCORE)){//积分
				if(result.getAwardValue() != null){
					awardValue = result.getAwardValue();
				}
			}

			MemberActiveRecord mar = new MemberActiveRecord();
			mar.setAccountId(member.getId());
			mar.setAccount(member.getAccount());
			mar.setActiveId(activeId);
			mar.setAwardType(result.getAwardType());
			mar.setCreateDatetime(curDatetime);
			mar.setProductName(result.getAwardName());
			mar.setStationId(member.getStationId());
			mar.setAwardValue(awardValue);
			mar.setProductId(productId);
			mar.setStatus(MemberActiveRecord.STATUS_UNTREATED);
			activeRecordDao.save(mar);
		}

		MnyScoreVo scoreVo = new MnyScoreVo();
		scoreVo.setAccountId(member.getId());
		scoreVo.setScore(ma.getScore());
		scoreVo.setScoreRecordType(ScoreRecordType.ACTIVE_SUB);
		scoreVo.setRemark("参与幸运大转盘");
		scoreService.updScoreAndRecord(scoreVo);

		return result;
	}

	/**
	 * 开奖处理器
	 * 
	 * @param awards
	 * @return
	 */
	private MemberActiveAward activeHandler(List<MemberActiveAward> awards) {
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal maxChance = BigDecimal.ZERO;
		BigDecimal curAwardChance = BigDecimal.ZERO;
		MemberActiveAward maxChanceAward = null;

		// 结算概率基数总和，得到最高概率奖项和概率
		for (MemberActiveAward maa : awards) {
			curAwardChance = maa.getChance();
			if (curAwardChance == null) {
				continue;
			}
			curAwardChance = curAwardChance.abs();
			if (maxChance.compareTo(BigDecimal.ZERO) == 0) {
				maxChance = curAwardChance;
			}
			if (maxChance.compareTo(curAwardChance) == -1) {
				maxChance = curAwardChance;
				maxChanceAward = maa;
			}
			total = total.add(curAwardChance);
		}
		if (total.compareTo(BigDecimal.ZERO) == 0) {
			throw new GenericException("抽奖设置异常，请联系客服！");
		}
		int num = (int) (Math.random() * 100000000) + 1;// 100% 六位小数 100
														// *1000000
		int start = 0;
		int end = 0;
		MemberActiveAward single = null;

		for (int i = 0; i < awards.size(); i++) {
			single = awards.get(i);
			curAwardChance = single.getChance();
			if (curAwardChance == null) {
				continue;
			}
			curAwardChance = curAwardChance.abs();
			curAwardChance = curAwardChance.divide(total, 8, BigDecimal.ROUND_FLOOR);

			if (i == 0) {
				end = (int) (curAwardChance.doubleValue() * 100000000); // 将小数转成正数
			} else {
				start = end;
				end = start + (int) (curAwardChance.doubleValue() * 100000000);
			}
			if (num >= start && num < end) {
				maxChanceAward = single;
				break;
			}
		}

		return maxChanceAward;
	}

	/**
	 * 活动中奖记录处理器
	 * 
	 * @param active
	 */
	@Override
	public List<MemberActiveRecord> getUntreatedRecords() {

		ActiveRecordVo arvo = new ActiveRecordVo();
		List<Long> types = new ArrayList<>();
		types.add(MemberActiveAward.AWARD_TYPE_MONEY);
		types.add(MemberActiveAward.AWARD_TYPE_SCORE);
		arvo.setStatus(MemberActiveRecord.STATUS_UNTREATED);// 未处理状态
		arvo.setLimit(1000);// 单次处理最多1000条
		arvo.setRecordTypes(types);
		return activeRecordDao.getRecordList(arvo);
	}

	/**
	 * 发放奖金并写账变记录
	 * 
	 * @param mar
	 */
	@Override
	public boolean balanceAndRecord(MemberActiveRecord mar) {
		boolean success = activeRecordDao.hanlderUntreated(mar) > 0;
		if (success) {
			String remark = "幸运大转盘活动中，获得" + mar.getProductName();
			MnyMoneyVo mvo = new MnyMoneyVo();
			mvo.setAccountId(mar.getAccountId());
			mvo.setMoney(mar.getAwardValue());
			mvo.setMoneyRecordType(MoneyRecordType.ACTIVE_AWARD);
			mvo.setRemark(remark);
			mvo.setOrderId(mar.getId() + "");
			mvo.setBizDatetime(mar.getCreateDatetime());
			moneyService.updMnyAndRecord(mvo);
		}
		return success;
	}
	
	/**
	 * 发放积分并写账变记录
	 * 
	 * @param mar
	 */
	@Override
	public boolean scoreAndRecord(MemberActiveRecord mar) {
		boolean success = activeRecordDao.hanlderUntreated(mar) > 0;
		if(success) {
			MnyScoreVo scoreVo = new MnyScoreVo();
			scoreVo.setAccountId(mar.getAccountId());
			scoreVo.setScore(mar.getAwardValue());
			scoreVo.setScoreRecordType(ScoreRecordType.ACTIVE_BIG_WHEEL);
			scoreVo.setRemark(mar.getRemark());
			scoreService.updScoreAndRecord(scoreVo);
		}
		//发放积分
//		SysAccount sa = atDao.get(mar.getAccountId());
//		if(sa != null && mar.getAwardValue() != null){
//			sa.setScore(sa.getScore().add(mar.getAwardValue()));
//		}
//		atDao.update(sa);
		return success;
	}

	@Override
	public MemberActive getProgressActive(ActiveVo activeVo) {
		return maDao.getProgressActive(activeVo);
	}

	@Override
	public Page getAwardRecordPage(ActiveRecordVo recordVo) {
		return activeRecordDao.getRecordPage(recordVo);
	}

	@Override
	public void handlerRecord(Long id, Long status, String remark) {
		MemberActiveRecord mar = activeRecordDao.get(id);
		if (mar == null || !StringUtil.equals(mar.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求");
		}
		mar.setStatus(status);
		mar.setRemark(remark);
		if (StringUtil.equals(status, MemberActiveRecord.STATUS_SUCCESS) && StringUtil.equals(mar.getAwardType(), MemberActiveAward.AWARD_TYPE_MONEY)) {
			mar.setRemark("手动结算：" + mar.getRemark());
			if (!balanceAndRecord(mar)) {
				throw new GenericException("处理失败！");
			}
		}else if (StringUtil.equals(status, MemberActiveRecord.STATUS_SUCCESS) && StringUtil.equals(mar.getAwardType(), MemberActiveAward.AWARD_TYPE_SCORE)) {
			mar.setRemark("手动结算：" + mar.getRemark());
			if (!scoreAndRecord(mar)) {
				throw new GenericException("处理失败！");
			}
		} else {
			activeRecordDao.save(mar);
		}
		RedisAPI.delCache( 8,Contants.ACTIVE_RECORD_SHOW + mar.getActiveId());
	}
	
	@Override
	public List<MemberActiveRecord> getAwardRecordList(ActiveRecordVo recordVo) {

		List<MemberActiveRecord> ars = null;
		if (recordVo.getActiveId() == null || recordVo.getActiveId() == 0) {
			return null;
		}
		
		if (StationUtil.isMemberStation()) {
			String key = Contants.ACTIVE_RECORD_SHOW + recordVo.getActiveId();
			if (RedisAPI.exists(key)) {
				return JSONArray.parseArray(RedisAPI.getCache(key), MemberActiveRecord.class);
			} else {
				
				//活动(大转盘)红包记录数
				String redpacketLimint = StationConfigUtil.get(StationConfig.member_active_record_limit);
				List<MemberActiveVirtualRecord> flist = null;
				List<MemberActiveRecord> vrflist =  new ArrayList<>();
				Integer fictitiousLimit = 0;
				Integer realLimit = recordVo.getLimit();
				String[] limit = null;
				if(StringUtil.isNotEmpty(redpacketLimint)) {
					limit = redpacketLimint.split(",");
					if(limit != null && limit.length > 1) {
						realLimit = StringUtil.toInteger(limit[0], realLimit);
						fictitiousLimit = StringUtil.toInteger(limit[1], 0);
						recordVo.setLimit(fictitiousLimit);
						recordVo.setStationId(StationUtil.getStationId());
						flist = activeVirtualRecordDao.getVirtualRecordList(recordVo);
					}
				}
				
				ars = activeRecordDao.getRecordList(recordVo);
				
				//合并假记录
				if(flist != null && !flist.isEmpty()) {
					MemberActiveRecord base = null;
					for(MemberActiveVirtualRecord mavr : flist) {
						base = new MemberActiveRecord();
						BeanUtils.copyProperties(mavr,base);
						vrflist.add(base);
					}
					if(ars == null || ars.isEmpty()) {
						ars = vrflist;
					}else {
						ars.addAll(vrflist);
					}
				}
//				if (ars != null && ars.size() > 0) {
//					Collections.shuffle(ars);
//					RedisAPI.addCache(key, JsonUtil.toJson(ars), Contants.ACTIVE_RECORD_SHOW_TIMEOUT, 8);
//				}
				if(ars.isEmpty()) {
					return ars;
				}
				String sort = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.set_active_sort);
				//降序
				if(StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.DROP.getValue())) {
					Collections.sort(ars,new Comparator<MemberActiveRecord>() {
						@Override
						public int compare(MemberActiveRecord o1, MemberActiveRecord o2) {
							return o2.getCreateDatetime().compareTo(o1.getCreateDatetime());
						}
					});
				}
				//升序
				if(StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.RISE.getValue())) {
					Collections.sort(ars,new Comparator<MemberActiveRecord>() {
						@Override
						public int compare(MemberActiveRecord o1, MemberActiveRecord o2) {
							return o1.getCreateDatetime().compareTo(o2.getCreateDatetime());
						}
					});
				}
				//无序\默认
				if((StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.DISORDERLY.getValue())) || StringUtils.isEmpty(sort)) {
					Collections.shuffle(ars);
				}
				RedisAPI.addCache(key, JsonUtil.toJson(ars), Contants.ACTIVE_RECORD_SHOW_TIMEOUT, 8);
			}
		} else {
			ars = activeRecordDao.getRecordList(recordVo);
		}
		return ars;
	}

	@Override
	public MemberActive getOne(Long id, Long stationId) {
		if (id != null) {
			MemberActive ma = maDao.get(id);
			if (ma != null && (stationId == null || ma.getStationId().equals(stationId))) {
				return ma;
			}
		}
		return null;
	}
	@Override
	public MemberActiveRecord getActiveRecord(Long id, Long stationId) {
		if (id != null) {
			MemberActiveRecord ma = activeRecordDao.get(id);
			if (ma != null && (stationId == null || ma.getStationId().equals(stationId))) {
				return ma;
			}
		}
		return null;
	}

	@Override
	public Page getAwardVirtualRecordPage(ActiveRecordVo arv) {
		return activeVirtualRecordDao.getVirtualRecordPage(arv);
	}
	
	@Override
	public void saveVirtualRecord(Integer records,Long station_id,Long activeType ,Long award_type,String productName) {
		
		if(Validator.isNull(station_id) || StringUtil.equals(1L, station_id)) {
			throw new GenericException("站点id不能为空！");
		};
		if(Validator.isNull(activeType)) {
			throw new GenericException("活动类型不能为空！");
		};
		if(Validator.isNull(records) || records.intValue() < 1) {
			throw new GenericException("活动虚拟记录至少要一条！");
		};
		if(Validator.isNull(award_type)) {
			throw new GenericException("奖项类型不能空！");
		};
		if(Validator.isNull(productName)) {
			throw new GenericException("商品名称不能为空！");
		};
		if( records >= 50) {
			throw new GenericException("虚拟条数不能大于50条");
		}
		List acrList = new ArrayList<MemberActiveVirtualRecord>();
		Long stationId = StationUtil.getStationId();
		MemberActiveVirtualRecord memberActiveVirtualRecord = null;
		for(int i =0;i<records ;i++){
			memberActiveVirtualRecord = new MemberActiveVirtualRecord();
			memberActiveVirtualRecord.setAccount(getRandomAccount());
			memberActiveVirtualRecord.setActiveId(award_type);
			memberActiveVirtualRecord.setAwardType(award_type);
			memberActiveVirtualRecord.setCreateDatetime(DateUtil.randomDate(DateUtil.getRollDay(new Date(),-1),DateUtil.getCurrentDate()));
			memberActiveVirtualRecord.setProductName(productName);
			memberActiveVirtualRecord.setStationId(stationId);
			memberActiveVirtualRecord.setStatus(1L);
			acrList.add(memberActiveVirtualRecord);
		}
		
		activeVirtualRecordDao.batchInsert(acrList);
	}
	private String getRandomAccount() {
		//随便生成姓名
		Random rd = new Random();
		int length = Contants.PYSTR.length;
		int pyindex = 0;
		String account ="";
		pyindex = (int)(rd.nextFloat()*(length))+1;
		if(pyindex != 1) {
			pyindex = length%pyindex;
		}
		account += Contants.PYSTR[pyindex];
		return account;
	}

	@Override
	public void delVirtualRecord(Long id) {
		activeVirtualRecordDao.delete(id);
	}

	@Override
	public void updateVirtualRecord(MemberActiveVirtualRecord memberActiveVirtualRecord) {
		if(StringUtil.isEmpty(memberActiveVirtualRecord.getAccount()) ||
		   StringUtil.isNotEmpty(memberActiveVirtualRecord.getProductName()) ||
		   (null != memberActiveVirtualRecord.getAwardType()) ||
		   (null  != memberActiveVirtualRecord.getActiveId())) {
			activeVirtualRecordDao.updateVirtualRecord(memberActiveVirtualRecord);
		}
	
	}
	@Override
	public MemberActiveVirtualRecord getActiveVirtualRecord(Long id, Long stationId) {
		if (id != null) {
			MemberActiveVirtualRecord ma = activeVirtualRecordDao.get(id);
			if (ma != null && (stationId != null && ma.getStationId().equals(stationId))) {
				return ma;
			}
		}
		return null;
	}
	
}
