package com.game.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.model.SysStation;
import com.game.service.GlobalStatService;
import com.game.service.SysStationService;
import com.game.user.online.OnlineManager;

@Service
public class GlobalStatServiceImpl implements GlobalStatService {
	@Autowired
	private SysStationService stationService;

	@Override
	public List<Map<String, Object>> getOnLineCountMap() {
		List<SysStation> list = stationService.getAllActive();
		if (list == null || list.isEmpty()) {
			return null;
		}

		List<Map<String, Object>> r = new ArrayList<>();
		Map<String, Object> map = null;
		for (SysStation s : list) {
			map = new HashMap<>();
			map.put("id", s.getId());
			map.put("folder", s.getFloder());
			map.put("name", s.getName());
			map.put("count", OnlineManager.getOnlineCount(s.getId(), null));
			r.add(map);
		}
		Collections.sort(r, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				return (int) (((Integer) o2.get("count")) - ((Integer) o1.get("count")));
			}
		});
		return r;
	}
}
