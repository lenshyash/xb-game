package com.game.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.DataReader;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountWarningDao;
import com.game.model.MnyDrawRecord;
import com.game.model.SysAccount;
import com.game.model.SysAccountWarning;
import com.game.model.vo.AccountWarningVo;
import com.game.model.vo.MnyComRecordVo;
import com.game.service.MnyMoneyRecordService;
import com.game.service.SysAccountWarningService;
import com.game.util.StationUtil;


@Repository
public class SysAccountWarningServiceImpl implements SysAccountWarningService {

	@Autowired
	private SysAccountDao accountDao;
	
	@Autowired
	private SysAccountWarningDao accountWarningDao;
	
	@Autowired
	private MnyMoneyRecordDao moneyRecordDao;

	@Override
	public void executeUserWarning(Date start, Long stationId) {
		List<Map> warnsMap = null;
		//执行站点预警用户统计
		try{
			warnsMap  = accountWarningDao.executeStationUserWarning(start,stationId);
		}catch(Exception e){
			warnsMap = new ArrayList<>();
		}
		List<SysAccountWarning> list = new ArrayList<SysAccountWarning>();
		List<Long> ulist = new ArrayList<Long>();
		
		
		if(!warnsMap.isEmpty()){
			for (Map map : warnsMap) {
				SysAccountWarning saw = new SysAccountWarning();
				saw.setAccount((String)map.get("account"));
				saw.setAccountId(StringUtil.toLong(map.get("id")));
				saw.setStationId(stationId);
				saw.setStatus(SysAccountWarning.STAUS_ENABLE);
				saw.setCreateTime(new Date());
				saw.setRemark("系统检测该账号资金异常，请警惕出款等资金操作");
				saw.setLevel(SysAccountWarning.LEVEL_GENERAL);
				Calendar now = Calendar.getInstance();
				Date end = now.getTime();
				now.add(Calendar.MINUTE, -10);
				start = now.getTime();
				//查询十分钟之内是否有提款等危险操作
				int num = moneyRecordDao.checkAccountWarningRecord(start,end,saw.getAccountId());
				if(num>0){
					saw.setLevel(SysAccountWarning.LEVEL_HIGH);
				}
				Long warnId = StringUtil.toLong(map.get("warnId"));
				//判断是否已经存在，存在则判断是否要调整高危等级和状态
				if(Validator.isNotNull(warnId)){
					if(num>0){
						ulist.add(warnId);
					}
					continue;
				}
				list.add(saw);
				//修改账号异常状态
				SysAccount acc = new SysAccount();
				acc.setId(saw.getAccountId());
				acc.setAbnormalFlag(SysAccount.ACCOUNT_ABNORMAL);
				accountDao.updateAbnormalFlag(acc);
			}
		}
		if(!list.isEmpty()){
			accountWarningDao.batchInsert(list);
		}
		if(!ulist.isEmpty()){
			accountWarningDao.updateHighLevel(ulist);
		}
		if(!warnsMap.isEmpty()){
			CacheUtil.delCache(CacheType.AGENT_STATION_ACCOUNT_WARN_COUNT, StationUtil.getStationId() + "");
		}
	}
	
	@Override
	public Integer getCountOfUntreated(long stationId) {
		return CacheUtil.getNull2Set(new DataReader<Integer>() {
			public Integer getData() {
				return getCountOfUntreatedByDb();
			}
		}, Integer.class, CacheType.AGENT_STATION_ACCOUNT_WARN_COUNT, stationId + "");
		// TODO Auto-generated method stub
	}
	
	private Integer getCountOfUntreatedByDb() {
		return accountWarningDao.getCountOfUntreated(StationUtil.getStationId());
	}

	@Override
	public Page<Map> getPage(AccountWarningVo accountVo) {
		// TODO Auto-generated method stub
		return accountWarningDao.getPage(accountVo);
	}

	@Override
	public void updStatus(SysAccountWarning account) {
		// TODO Auto-generated method stub
		accountWarningDao.updStatus(account);
		//修改账号异常状态
		SysAccount acc = new SysAccount();
		acc.setId(account.getAccountId());
		acc.setAbnormalFlag(account.getStatus());
		accountDao.updateAbnormalFlag(acc);
		CacheUtil.delCache(CacheType.AGENT_STATION_ACCOUNT_WARN_COUNT, StationUtil.getStationId() + "");
	}


}
