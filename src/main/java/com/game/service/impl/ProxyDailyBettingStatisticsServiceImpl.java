package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.platform.MemberBackwaterRecordDao;
import com.game.dao.platform.ProxyDailyBettingStatisticsDao;
import com.game.dao.platform.ProxyManualRebateRecordDao;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.platform.ProxyDailyBettingStatistics;
import com.game.model.platform.ProxyManualRebateRecord;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyMoneyService;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Service
public class ProxyDailyBettingStatisticsServiceImpl implements ProxyDailyBettingStatisticsService {
	private Logger logger = Logger.getLogger(ProxyDailyBettingStatisticsServiceImpl.class);
	@Autowired
	private ProxyDailyBettingStatisticsDao pdbsDao;
	@Autowired
	private SysAccountDailyMoneyDao accountDailyMoneyDao;
	@Autowired
	private ProxyManualRebateRecordDao mrrDao;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MemberBackwaterRecordDao backwaterRecordDao;
	@Autowired
	private SysAccountDao accountDao;

	@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
	@Override
	public void collectDailyAmount2(Date startDate, Date endDate, List<Long> sIdList) {
		if (sIdList == null || sIdList.isEmpty()) {
			logger.info("站点列表为空，不需要计算反水");
			return;
		}
		Map<String, ProxyDailyBettingStatistics> dailyBettingMap = new HashMap<>();

		Map<Long, SysAccount> agentIdMap = new HashMap<>();
		for (Long sid : sIdList) {
			collectFromMemberBackwater(startDate, endDate, sid, dailyBettingMap);
			collectLhc(startDate, endDate, sid, agentIdMap, dailyBettingMap);
			collectLottery(startDate, endDate, sid, agentIdMap, dailyBettingMap);
			collectSports(startDate, endDate, sid, agentIdMap, dailyBettingMap);
		}
		for (ProxyDailyBettingStatistics dbs : dailyBettingMap.values()) {
			dbs.setMarkSixProfitAmount(
					BigDecimalUtil.subtract(dbs.getMarkSixProfitAmount(), dbs.getMarkSixRebateAmount()));
			dbs.setLotteryProfitAmount(
					BigDecimalUtil.subtract(dbs.getLotteryProfitAmount(), dbs.getLotteryRebateAmount()));
			dbs.setSportsProfitAmount(
					BigDecimalUtil.subtract(dbs.getSportsProfitAmount(), dbs.getSportsRebateAmount()));
			saveOrUpdate(dbs);
		}
	}

	private void collectFromMemberBackwater(Date startDate, Date endDate, Long stationId,
			Map<String, ProxyDailyBettingStatistics> dailyBettingMap) {
		List<MemberBackwaterRecord> brList = backwaterRecordDao.findForProxyDailyStat(stationId, startDate, endDate);
		if (brList == null || brList.isEmpty()) {
			return;
		}
		Map<String, SysAccountDailyMoney> map = getBetAmountForAgent(stationId, startDate, endDate);
		SysAccountDailyMoney adm = null;
		String key = null;
		ProxyDailyBettingStatistics dailyBetting = null;
		for (MemberBackwaterRecord r : brList) {
			key = getKeyForMap(r.getAgentId(), r.getBetDate());
			dailyBetting = dailyBettingMap.get(key);
			if (dailyBetting == null) {
				dailyBetting = new ProxyDailyBettingStatistics();
				dailyBetting.setAccountId(r.getAgentId());
				dailyBetting.setStatDate(r.getBetDate());
				dailyBetting.setStationId(stationId);
				dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
				dailyBettingMap.put(key, dailyBetting);
			}
			switch (r.getBetType()) {
			case 1:// 彩票
			case 11:// 系统彩
					// dailyBetting.setLotteryBetAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryBetAmount(),
					// r.getBetMoney()));
					// dailyBetting.setLotteryProfitAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryProfitAmount(),
					// BigDecimalUtil.subtract(r.getBetMoney(),
					// r.getWinMoney(),
					// r.getBackwaterMoney())));
				dailyBetting.setLotteryRebateAmount(
						BigDecimalUtil.addAll(dailyBetting.getLotteryRebateAmount(), r.getBackwaterMoney()));
				break;
			case 2:// 真人
				adm = map.get(key);
				if (adm != null) {
					dailyBetting.setRealBetAmount(
							BigDecimalUtil.addAll(dailyBetting.getRealBetAmount(), adm.getRealBetAmount()));
					dailyBetting.setRealProfitAmount(BigDecimalUtil.addAll(dailyBetting.getRealProfitAmount(),
							BigDecimalUtil.subtract(adm.getRealBetAmount(), r.getWinMoney(), r.getBackwaterMoney())));
					dailyBetting.setRealRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getRealRebateAmount(), r.getBackwaterMoney()));
				}
				break;
			case 3:// 电子
				adm = map.get(key);
				if (adm != null) {
					dailyBetting.setEgameBetAmount(
							BigDecimalUtil.addAll(dailyBetting.getEgameBetAmount(), adm.getEgameBetAmount()));
					dailyBetting.setEgameProfitAmount(BigDecimalUtil.addAll(dailyBetting.getEgameProfitAmount(),
							BigDecimalUtil.subtract(adm.getEgameBetAmount(), r.getWinMoney(), r.getBackwaterMoney())));
					dailyBetting.setEgameRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getEgameRebateAmount(), r.getBackwaterMoney()));
				}
				break;
			case 4:// 体育
					// dailyBetting.setSportsBetAmount(BigDecimalUtil.addAll(dailyBetting.getSportsBetAmount(),
					// r.getBetMoney()));
					// dailyBetting.setSportsProfitAmount(BigDecimalUtil.addAll(dailyBetting.getSportsProfitAmount(),
					// BigDecimalUtil.subtract(r.getBetMoney(),
					// r.getWinMoney(),
					// r.getBackwaterMoney())));
				dailyBetting.setSportsRebateAmount(
						BigDecimalUtil.addAll(dailyBetting.getSportsRebateAmount(), r.getBackwaterMoney()));
				break;
			case 5:// 六合特码b
			case 6:// 六合正码b
			case 15:// 十分六合特码b
			case 16:// 十分六合正码b
					// 因为此出只有特码b和正码b的投注金额和中奖金额，所以需要从订单表 统计投注金额和派奖金额
				dailyBetting.setMarkSixRebateAmount(
						BigDecimalUtil.addAll(dailyBetting.getMarkSixRebateAmount(), r.getBackwaterMoney()));
				break;
			case 90://棋牌
				adm = map.get(key);
				if (adm != null) {
					dailyBetting.setChessRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getChessBetAmount(), adm.getChessBetAmount()));
					dailyBetting.setChessProfitAmount(BigDecimalUtil.addAll(dailyBetting.getChessProfitAmount(),
							BigDecimalUtil.subtract(adm.getChessBetAmount(), r.getWinMoney(), r.getBackwaterMoney())));
					dailyBetting.setChessRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getChessRebateAmount(), r.getBackwaterMoney()));
				}
				break;
			case 92://电竞
				adm = map.get(key);
				if (adm != null) {
					dailyBetting.setEsportsRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getEsportsBetAmount(), adm.getEsportsBetAmount()));
					dailyBetting.setEsportsProfitAmount(BigDecimalUtil.addAll(dailyBetting.getEsportsProfitAmount(),
							BigDecimalUtil.subtract(adm.getEsportsBetAmount(), r.getWinMoney(), r.getBackwaterMoney())));
					dailyBetting.setEsportsRebateAmount(
							BigDecimalUtil.addAll(dailyBetting.getEsportsRebateAmount(), r.getBackwaterMoney()));
				}
				break;
			}
		}
	}

	private Map<String, SysAccountDailyMoney> getBetAmountForAgent(Long stationId, Date startDate, Date endDate) {
		List<SysAccountDailyMoney> admList = accountDailyMoneyDao.getRealAndEgameBetAmountForAgent(stationId, startDate,
				endDate);
		Map<String, SysAccountDailyMoney> map = new HashMap<>();
		String key = null;
		if (admList != null && !admList.isEmpty()) {
			for (SysAccountDailyMoney adm : admList) {
				key = getKeyForMap(adm.getAgentId(), adm.getStatDate());
				map.put(key, adm);
			}
		}
		return map;
	}

	private void collectLhc(Date startDate, Date endDate, Long stationId, Map<Long, SysAccount> agentIdMap,
			Map<String, ProxyDailyBettingStatistics> dailyBettingMap) {
		try {
			List<MemberBackwaterRecord> brList = backwaterRecordDao.findLHCBetAndWin(stationId, startDate, endDate);
			if (brList != null && !brList.isEmpty()) {
				SysAccount acc = null;
				String key = null;
				ProxyDailyBettingStatistics dailyBetting = null;
				for (MemberBackwaterRecord r : brList) {
					if (agentIdMap.containsKey(r.getAccountId())) {
						acc = agentIdMap.get(r.getAccountId());
					} else {
						acc = accountDao.getFromCache(r.getAccountId());
					}
					if (acc != null) {
						r.setAgentId(acc.getAgentId());
						r.setAgentName(acc.getAgentName());
						agentIdMap.put(r.getAccountId(), acc);
					}
					if (r.getAgentId() == null || r.getAgentId() == 0) {
						continue;
					}
					key = getKeyForMap(r.getAgentId(), r.getBetDate());
					dailyBetting = dailyBettingMap.get(key);
					if (dailyBetting == null) {
						dailyBetting = new ProxyDailyBettingStatistics();
						dailyBetting.setAccountId(r.getAgentId());
						dailyBetting.setStatDate(r.getBetDate());
						dailyBetting.setStationId(stationId);
						dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
						dailyBettingMap.put(key, dailyBetting);
					}
					dailyBetting.setMarkSixBetAmount(
							BigDecimalUtil.addAll(dailyBetting.getMarkSixBetAmount(), r.getBetMoney()));
					dailyBetting.setMarkSixProfitAmount(BigDecimalUtil.addAll(dailyBetting.getMarkSixProfitAmount(),
							BigDecimalUtil.subtract(r.getBetMoney(), r.getWinMoney())));
				}
			}
		} catch (Exception e) {
			logger.error("汇总代理六合彩返点信息发生错误", e);
		}
	}

	private void collectLottery(Date startDate, Date endDate, Long stationId, Map<Long, SysAccount> agentIdMap,
			Map<String, ProxyDailyBettingStatistics> dailyBettingMap) {
		try {
			List<MemberBackwaterRecord> brList = backwaterRecordDao.findLotteryBetAndWin(stationId, startDate, endDate);
			if (brList != null && !brList.isEmpty()) {
				SysAccount acc = null;
				String key = null;
				ProxyDailyBettingStatistics dailyBetting = null;
				for (MemberBackwaterRecord r : brList) {
					if (agentIdMap.containsKey(r.getAccountId())) {
						acc = agentIdMap.get(r.getAccountId());
					} else {
						acc = accountDao.getFromCache(r.getAccountId());
					}
					if (acc != null) {
						r.setAgentId(acc.getAgentId());
						r.setAgentName(acc.getAgentName());
						agentIdMap.put(r.getAccountId(), acc);
					}
					if (r.getAgentId() == null || r.getAgentId() == 0) {
						continue;
					}
					key = getKeyForMap(r.getAgentId(), r.getBetDate());
					dailyBetting = dailyBettingMap.get(key);
					if (dailyBetting == null) {
						dailyBetting = new ProxyDailyBettingStatistics();
						dailyBetting.setAccountId(r.getAgentId());
						dailyBetting.setStatDate(r.getBetDate());
						dailyBetting.setStationId(stationId);
						dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
						dailyBettingMap.put(key, dailyBetting);
					}
					dailyBetting.setLotteryBetAmount(
							BigDecimalUtil.addAll(dailyBetting.getLotteryBetAmount(), r.getBetMoney()));
					dailyBetting.setLotteryProfitAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryProfitAmount(),
							BigDecimalUtil.subtract(r.getBetMoney(), r.getWinMoney())));
				}
			}
		} catch (Exception e) {
			logger.error("汇总代理彩票返点信息发生错误", e);
		}
	}

	private void collectSports(Date startDate, Date endDate, Long stationId, Map<Long, SysAccount> agentIdMap,
			Map<String, ProxyDailyBettingStatistics> dailyBettingMap) {
		try {
			List<MemberBackwaterRecord> brList = backwaterRecordDao.findSportBetAndWin(stationId, startDate, endDate);
			if (brList != null && !brList.isEmpty()) {
				SysAccount acc = null;
				String key = null;
				ProxyDailyBettingStatistics dailyBetting = null;
				for (MemberBackwaterRecord r : brList) {
					if (agentIdMap.containsKey(r.getAccountId())) {
						acc = agentIdMap.get(r.getAccountId());
					} else {
						acc = accountDao.getFromCache(r.getAccountId());
					}
					if (acc != null) {
						r.setAgentId(acc.getAgentId());
						r.setAgentName(acc.getAgentName());
						agentIdMap.put(r.getAccountId(), acc);
					}
					if (r.getAgentId() == null || r.getAgentId() == 0) {
						continue;
					}
					key = getKeyForMap(r.getAgentId(), r.getBetDate());
					dailyBetting = dailyBettingMap.get(key);
					if (dailyBetting == null) {
						dailyBetting = new ProxyDailyBettingStatistics();
						dailyBetting.setAccountId(r.getAgentId());
						dailyBetting.setStatDate(r.getBetDate());
						dailyBetting.setStationId(stationId);
						dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
						dailyBettingMap.put(key, dailyBetting);
					}
					dailyBetting.setSportsBetAmount(
							BigDecimalUtil.addAll(dailyBetting.getSportsBetAmount(), r.getBetMoney()));
					dailyBetting.setSportsProfitAmount(BigDecimalUtil.addAll(dailyBetting.getSportsProfitAmount(),
							BigDecimalUtil.subtract(r.getBetMoney(), r.getWinMoney())));
				}
			}
		} catch (Exception e) {
			logger.error("汇总代理体育返点信息发生错误", e);
		}
	}

	private String getKeyForMap(Long agentId, Date betDate) {
		return "a_" + agentId + "_d_" + DateUtil.toDateStr(betDate);
	}

	// @Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
	// @Override
	// public void collectDailyAmount(Date startDate, Date endDate,
	// List<SysStation> stationList) {
	// if (stationList == null) {
	// stationList = stationService.getAllActive();
	// }
	// if (stationList == null || stationList.isEmpty()) {
	// logger.info("站点列表为空，不需要计算反水");
	// return;
	// }
	// Map<Long, ProxyDailyBettingStatistics> dailyBettingMap = new HashMap<>();
	// for (SysStation station : stationList) {
	// if (StringUtils.equals(StationConfigUtil.get(station.getId(),
	// StationConfig.onoff_multi_agent), "on")) {
	// logger.info("站点使用多级代理机制，不需要计算一级代理返点，站点＝" + station.getFloder());
	// continue;
	// }
	// logger.info("开始计算一级代理返点，站点＝" + station.getFloder());
	// try {
	// calcLotteryRebate(station, startDate, endDate, dailyBettingMap);
	// } catch (Exception e) {
	// logger.info("计算彩票一级代理返点发生错误，站点＝" + station.getFloder(), e);
	// }
	// try {
	// calcSportRebate(station, startDate, endDate, dailyBettingMap);
	// } catch (Exception e) {
	// logger.info("计算体育一级代理返点发生错误，站点＝" + station.getFloder(), e);
	// }
	// logger.info("一级代理返点计算结束，站点＝" + station.getFloder());
	// }
	// for (ProxyDailyBettingStatistics dbs : dailyBettingMap.values()) {
	// saveOrUpdate(dbs);
	// }
	// }

	/**
	 * 彩票投注、中奖、返点金额统计
	 * 
	 * @param station
	 * @param startDate
	 * @param endDate
	 * @param dailyBettingMap
	 */
	// private void calcLotteryRebate(SysStation station, Date startDate, Date
	// endDate, Map<Long, ProxyDailyBettingStatistics> dailyBettingMap) {
	// List<BcLotteryOrder> bets =
	// lotteryOrderDao.getOrderByDate(station.getId(), startDate, endDate);
	// if (bets != null && !bets.isEmpty()) {
	// List<Long> ids = new ArrayList<>();
	// for (BcLotteryOrder bo : bets) {
	// ids.add(bo.getId());
	// }
	// Map<Long, BigDecimal> rebateMap =
	// memberRollBackRecordDao.getRollbackMoneyMap(startDate, endDate, ids, new
	// Integer[] { BusinessConstant.BET_TYPE_LOTTERY,
	// BusinessConstant.BET_TYPE_MARK_SIX }, station.getId());
	// Map<Long, SysAccount> accMap = new HashMap<>();
	// SysAccount acc = null;
	// ProxyDailyBettingStatistics dailyBetting = null;
	// for (BcLotteryOrder bo : bets) {
	// acc = accMap.get(bo.getAccountId());
	// if (acc == null) {
	// acc = accountService.findOneByAccountAndStationId(bo.getAccount(),
	// station.getId());
	// if (acc != null) {
	// accMap.put(acc.getId(), acc);
	// }
	// }
	// if (acc != null) {
	// dailyBetting = dailyBettingMap.get(acc.getAgentId());
	// if (dailyBetting == null) {
	// dailyBetting = new ProxyDailyBettingStatistics();
	// dailyBetting.setAccountId(acc.getAgentId());
	// dailyBetting.setStatDate(startDate);
	// dailyBetting.setStationId(station.getId());
	// dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
	// dailyBettingMap.put(acc.getAgentId(), dailyBetting);
	// }
	// if (bo.getLotCode().equals(LotteryEnum.LHC.name())) {
	// dailyBetting.setMarkSixBetAmount(BigDecimalUtil.addAll(dailyBetting.getMarkSixBetAmount(),
	// bo.getBuyMoney()));
	// dailyBetting.setMarkSixProfitAmount(BigDecimalUtil.addAll(dailyBetting.getMarkSixProfitAmount(),
	// BigDecimalUtil.subtract(bo.getBuyMoney(), bo.getWinMoney(),
	// rebateMap.get(bo.getId()))));
	// dailyBetting.setMarkSixRebateAmount(BigDecimalUtil.addAll(dailyBetting.getMarkSixRebateAmount(),
	// rebateMap.get(bo.getId())));
	// } else {
	// dailyBetting.setLotteryBetAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryBetAmount(),
	// bo.getBuyMoney()));
	// dailyBetting.setLotteryProfitAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryProfitAmount(),
	// BigDecimalUtil.subtract(bo.getBuyMoney(), bo.getWinMoney(),
	// rebateMap.get(bo.getId()))));
	// dailyBetting.setLotteryRebateAmount(BigDecimalUtil.addAll(dailyBetting.getLotteryRebateAmount(),
	// rebateMap.get(bo.getId())));
	// }
	// }
	// }
	// }
	// }

	/**
	 * 投注、中奖、返点金额统计
	 * 
	 * @param station
	 * @param startDate
	 * @param endDate
	 * @param dailyBettingMap
	 */
	// private void calcSportRebate(SysStation station, Date startDate, Date
	// endDate, Map<Long, ProxyDailyBettingStatistics> dailyBettingMap) {
	// List<SportBettingOrder> bets =
	// sportBettingOrderDao.getOrderByDate(station.getId(), startDate, endDate);
	// if (bets != null && !bets.isEmpty()) {
	// List<Long> ids = new ArrayList<>();
	// for (SportBettingOrder bo : bets) {
	// ids.add(bo.getId());
	// }
	// Map<Long, BigDecimal> rebateMap =
	// memberRollBackRecordDao.getRollbackMoneyMap(startDate, endDate, ids, new
	// Integer[] { BusinessConstant.BET_TYPE_SPORT }, station.getId());
	// Map<Long, SysAccount> accMap = new HashMap<>();
	// SysAccount acc = null;
	// ProxyDailyBettingStatistics dailyBetting = null;
	// for (SportBettingOrder bo : bets) {
	// acc = accMap.get(bo.getMemberId());
	// if (acc == null) {
	// acc = accountService.findOneByAccountAndStationId(bo.getMemberName(),
	// station.getId());
	// if (acc != null) {
	// accMap.put(acc.getId(), acc);
	// }
	// }
	// if (acc != null) {
	// dailyBetting = dailyBettingMap.get(acc.getAgentId());
	// if (dailyBetting == null) {
	// dailyBetting = new ProxyDailyBettingStatistics();
	// dailyBetting.setAccountId(acc.getAgentId());
	// dailyBetting.setStatDate(startDate);
	// dailyBetting.setStationId(station.getId());
	// dailyBetting.setStatus(ProxyDailyBettingStatistics.status_unroll);
	// dailyBettingMap.put(acc.getAgentId(), dailyBetting);
	// }
	// dailyBetting.setSportsBetAmount(BigDecimalUtil.addAll(dailyBetting.getSportsBetAmount(),
	// bo.getBettingMoney()));
	// dailyBetting.setSportsProfitAmount(BigDecimalUtil.addAll(dailyBetting.getSportsProfitAmount(),
	// BigDecimalUtil.subtract(bo.getBettingResult(), bo.getBettingMoney(),
	// rebateMap.get(bo.getId()))));
	// dailyBetting.setSportsRebateAmount(BigDecimalUtil.addAll(dailyBetting.getSportsRebateAmount(),
	// rebateMap.get(bo.getId())));
	// }
	// }
	// }
	// }

	public void saveOrUpdate(ProxyDailyBettingStatistics dbs) {
		boolean exist = pdbsDao.exist(dbs.getStatDate(), dbs.getAccountId());
		if (exist) {
			pdbsDao.updateAmount(dbs);
		} else {
			try {
				pdbsDao.insert(dbs);
			} catch (Exception e) {
				try {
					pdbsDao.updateAmount(dbs);
				} catch (Exception e1) {
					logger.error("持久化代理日投注总额发生错误", e1);
				}
			}
		}
	}

	@Override
	public Page page(Date startDate, Date endDate, String account, Long stationId,Integer status,Integer searchType) {
		if (startDate == null) {
			startDate = new Date();
		}
		if (endDate == null) {
			endDate = new Date();
		}
		return pdbsDao.page(startDate, endDate, account, stationId,status,searchType);
	}

	/**
	 * 计算二级代理返点
	 * 
	 * @param pointIds
	 * @param accountId
	 * @param stationId
	 * @param lotteryPoint
	 * @param realPoint
	 * @param egamePoint
	 * @param sportsPoint
	 * @return
	 */
	@Override
	public String toProfit(String pointIds, Long accountId, Long stationId, BigDecimal lotteryPoint,
			BigDecimal markSixPoint, BigDecimal realPoint, BigDecimal egamePoint, BigDecimal sportsPoint, BigDecimal chessPoint, BigDecimal esportsPoint) {
		String[] ids = pointIds.split(",");
		List<Long> list = new ArrayList<>();
		Long idl = null;
		for (String id : ids) {
			if (StringUtils.isEmpty(id)) {
				continue;
			}
			idl = NumberUtils.toLong(id);
			if (idl > 0L) {
				list.add(idl);
			}
		}
		if (list.isEmpty()) {
			throw new GenericException("请选择需要返点的记录");
		}
		List<ProxyDailyBettingStatistics> dbsList = pdbsDao.findByIdsAndStatusAndAccountId(list,
				ProxyDailyBettingStatistics.status_unroll, accountId);
		if (dbsList == null || dbsList.isEmpty()) {
			throw new GenericException("没有找到需要返点的记录");
		}
		if (list.size() != dbsList.size()) {
			throw new GenericException("有些记录已经返点，不能再重复返点");
		}

		ProxyManualRebateRecord record = markProxyManualRebateRecord(accountId, stationId, lotteryPoint, markSixPoint,
				realPoint, egamePoint, sportsPoint,chessPoint,esportsPoint, dbsList);
		Date now = new Date();
		for (Long id : list) {
			int updated = pdbsDao.updateRebateInfo(id, accountId, ProxyDailyBettingStatistics.status_unroll,
					ProxyDailyBettingStatistics.status_rollback, now);
			if (updated != 1) {
				throw new GenericException("有些记录已经返点，不能再重复返点");
			}
		}
		mrrDao.save(record);
		int updated = pdbsDao.updateRecordId(list, accountId, record.getId());
		if (updated != list.size()) {
			throw new GenericException("有些记录已经返点，不能再重复返点");
		}
		BigDecimal money = BigDecimalUtil.addAll(record.getLotteryRebateAmount(), record.getSportRebateAmount(),
				record.getRealRebateAmount(), record.getEgameRebateAmount(), record.getMarkSixRebateAmount(),record.getChessRebateAmount(),record.getEsportsRebateAmount());
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setAccountId(accountId);
		moneyVo.setMoney(money);
		moneyVo.setMoneyRecordType(MoneyRecordType.PROXY_REBATE_ADD);
		moneyVo.setRemark("代理返点加钱 到账金额[" + money.setScale(5) + "]");
		moneyService.updMnyAndRecord(moneyVo);
		JSONObject jobj = new JSONObject();
		jobj.put("success", "1");
		jobj.put("msg", "返点成功");
		return jobj.toJSONString();
	}

	private ProxyManualRebateRecord markProxyManualRebateRecord(Long accountId, Long stationId, BigDecimal lotteryPoint,
			BigDecimal markSixPoint, BigDecimal realPoint, BigDecimal egamePoint, BigDecimal sportsPoint, BigDecimal chessPoint, BigDecimal esportsPoint,
			List<ProxyDailyBettingStatistics> dbsList) {
		ProxyManualRebateRecord record = new ProxyManualRebateRecord();
		record.setAccountId(accountId);
		record.setStationId(stationId);
		record.setLotteryRebate(lotteryPoint);
		record.setSportRebate(sportsPoint);
		record.setRealRebate(realPoint);
		record.setEgameRebate(egamePoint);
		record.setMarkSixRebate(markSixPoint);
		record.setChessRebate(chessPoint);
		record.setEsportsRebate(esportsPoint);
		Date start = null;
		Date end = null;
		BigDecimal lotteryProfit = BigDecimal.ZERO;
		BigDecimal markSixProfit = BigDecimal.ZERO;
		BigDecimal lotteryHandler = null;
		BigDecimal markSixHandler = null;
		
		for (ProxyDailyBettingStatistics dbs : dbsList) {
			if (start == null || start.compareTo(dbs.getStatDate()) > 0) {
				start = dbs.getStatDate();
			}
			if (end == null || end.compareTo(dbs.getStatDate()) < 0) {
				end = dbs.getStatDate();
			}
			record.setLotteryBettingAmount(
					BigDecimalUtil.addAll(record.getLotteryBettingAmount(), dbs.getLotteryBetAmount()));
			record.setSportProfitAmount(
					BigDecimalUtil.addAll(record.getSportProfitAmount(), dbs.getSportsProfitAmount()));
			record.setEgameProfitAmount(
					BigDecimalUtil.addAll(record.getEgameProfitAmount(), dbs.getEgameProfitAmount()));
			record.setRealProfitAmount(BigDecimalUtil.addAll(record.getRealProfitAmount(), dbs.getRealProfitAmount()));
			record.setMarkSixBettingAmount(
					BigDecimalUtil.addAll(record.getMarkSixBettingAmount(), dbs.getMarkSixBetAmount()));
			record.setChessProfitAmount(
					BigDecimalUtil.addAll(record.getChessProfitAmount(), dbs.getChessProfitAmount()));
			record.setEsportsProfitAmount(
					BigDecimalUtil.addAll(record.getEsportsProfitAmount(), dbs.getEsportsProfitAmount()));
			lotteryProfit = BigDecimalUtil.addAll(lotteryProfit, dbs.getLotteryProfitAmount());
			markSixProfit = BigDecimalUtil.addAll(markSixProfit, dbs.getMarkSixProfitAmount());
		}
		List<MemberBackwaterRecord> l = backwaterRecordDao.getUnBackwater(start, end, accountId, stationId);
		if (l != null && !l.isEmpty()) {
			throw new GenericException(
					"该代理下会员" + l.get(0).getAccount() + "在" + DateUtil.toDateStr(l.get(0).getBetDate()) + "投注没有反水");
		}
		record.setStartDate(start);
		record.setEndDate(end);
		if("profit".equals(StationConfigUtil.get(StationConfig.proxy_daily_rebate))) {
			lotteryHandler = lotteryProfit;
			markSixHandler = markSixProfit;
		}else {
			lotteryHandler = record.getLotteryBettingAmount();
			markSixHandler = record.getMarkSixBettingAmount();
		}
		record.setLotteryRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(lotteryHandler, record.getLotteryRebate()), 100, 5));
		record.setMarkSixRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(markSixHandler, record.getMarkSixRebate()), 100, 5));
		record.setSportRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(record.getSportRebate(), record.getSportProfitAmount()), 100, 5));
		record.setEgameRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(record.getEgameProfitAmount(), record.getEgameRebate()), 100, 5));
		record.setRealRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(record.getRealProfitAmount(), record.getRealRebate()), 100, 5));
		record.setChessRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(record.getChessProfitAmount(), record.getChessRebate()), 100, 5));
		record.setEsportsRebateAmount(BigDecimalUtil
				.divide(BigDecimalUtil.multiply(record.getEsportsProfitAmount(), record.getEsportsRebate()), 100, 5));
		return record;
	}
}
