package com.game.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.redis.RedisAPI;
import com.game.dao.SysAnnouncementDao;
import com.game.dao.SysAnnouncementStationDao;
import com.game.model.SysAnnouncement;
import com.game.model.SysAnnouncementStation;
import com.game.model.SysStation;
import com.game.model.vo.AnnouncementVo;
import com.game.service.SysAnnouncementService;
import com.game.service.SysStationService;

@Repository
public class SysAnnouncementServiceImpl implements SysAnnouncementService {

	@Autowired
	private SysAnnouncementDao sysAnnouncementDao;
	@Autowired
	private SysAnnouncementStationDao sysAnnouncementStationDao;
	@Autowired
	private SysStationService stationService;

	private static final String CACHE_KEY_PREFIX = "agent:notice:";

	@Override
	public Page<Map> getPage(AnnouncementVo ancvo) {
		return sysAnnouncementDao.getPage(ancvo);
	}

	@Override
	public void saveAnnouncement(SysAnnouncement announcement, Long[] stationIds) {
		if (announcement.getId() == null) {
			announcement.setCreateDatetime(new Date());
			sysAnnouncementDao.insert(announcement);
			if (announcement.getType() == SysAnnouncement.TYPE_SOME_STATION) {
				SysAnnouncementStation sas = null;
				for (Long sid : stationIds) {
					sas = new SysAnnouncementStation();
					sas.setStationId(sid);
					sas.setAnnouncementId(announcement.getId());
					sysAnnouncementStationDao.save(sas);
				}
			}
		} else {
			SysAnnouncement old = sysAnnouncementDao.get(announcement.getId());
			old.setContent(announcement.getContent());
			old.setEndDatetime(announcement.getEndDatetime());
			old.setType(announcement.getType());
			sysAnnouncementDao.update(old);
			if (announcement.getType() == SysAnnouncement.TYPE_SOME_STATION) {
				SysAnnouncementStation sas = null;
				List<Long> stationIdList = sysAnnouncementStationDao.findStationIds(old.getId());
				for (Long sid : stationIds) {
					if (!stationIdList.remove(sid)) {
						sas = new SysAnnouncementStation();
						sas.setStationId(sid);
						sas.setAnnouncementId(old.getId());
						sysAnnouncementStationDao.save(sas);
					}
				}
				sysAnnouncementStationDao.deleteSomes(old.getId(), stationIdList);
			}
		}
		delCache();
	}

	private void delCache() {
		List<SysStation> slist = stationService.getAllActive();
		if (slist != null) {
			for (SysStation ss : slist) {
				RedisAPI.delCache(CACHE_KEY_PREFIX + ss.getId());
			}
		}
	}

	@Override
	public void deleteAnc(Long ancId) {
		sysAnnouncementDao.delete(ancId);
		sysAnnouncementStationDao.deleteByAnnouncementId(ancId);
		delCache();
	}

	@Override
	public String getAgentNotices(Long stationId) {
//		String notices = RedisAPI.getCache(CACHE_KEY_PREFIX + stationId);
//		if (notices != null) {
//			return notices;
//		}
		String notices =null;
		List<String> list = sysAnnouncementStationDao.findNotices(stationId, new Date());
		if (list != null && !list.isEmpty()) {
			notices = JSON.toJSONString(list);
		} else {
			notices = "";
		}
//		RedisAPI.addCache(CACHE_KEY_PREFIX + stationId, notices, 0);
		return notices;
	}

	@Override
	public List<Long> getStationIds(Long announcementId) {
		return sysAnnouncementStationDao.findStationIds(announcementId);
	}

	@Override
	public List<Map> getLastAnnouncement(Integer count) {
		
		return sysAnnouncementStationDao.getLastAnnouncement(count);
	}
}