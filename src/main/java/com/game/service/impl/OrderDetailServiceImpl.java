package com.game.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryDao;
import com.game.dao.lottery.BcLotteryDataDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.dao.lottery.BcLotteryPlayDao;
import com.game.dao.lottery.BcLotteryPlayGroupDao;
import com.game.dao.lottery.BcMarkSixDao;
import com.game.dao.platform.MemberBackwaterRecordDao;
import com.game.dao.sports.SportBettingOrderDao;
import com.game.lottery.utils.jiebao.PlayCodeConstants;
import com.game.model.MnyMoney;
import com.game.model.lottery.BcLottery;
import com.game.model.lottery.BcLotteryData;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.lottery.BcLotteryPlay;
import com.game.model.lottery.BcLotteryPlayGroup;
import com.game.model.lottery.LotteryEnum;
import com.game.model.lottery.six.BcMarkSix;
import com.game.model.lottery.six.BcMarkSixEnum;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.sports.SportBettingOrder;
import com.game.service.AgentBaseConfigService;
import com.game.service.MnyMoneyService;
import com.game.service.OrderDetailService;
import com.game.service.lottery.impl.BcMarkSixServiceImpl;
import com.game.util.BigDecimalUtil;
import com.game.util.StationConfigUtil;
import com.game.util.UserUtil;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {
	@Autowired
	private BcLotteryDao lotteryDao;
	@Autowired
	private BcLotteryPlayDao bcLotteryPlayDao;
	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	@Autowired
	private BcMarkSixDao bcMarkSixDao;
	@Autowired
	private BcLotteryDataDao bcLotteryDataDao;
	@Autowired
	private BcLotteryPlayGroupDao lotPlayGroupDao;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private MemberBackwaterRecordDao backwaterRecordDao;
	@Autowired
	private SportBettingOrderDao sportBetDao;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private BcMarkSixServiceImpl markSixServiceImpl;

	public BcLotteryOrder getLotteryOrderDetailForList(String orderId, String account, Long stationId, String lotCode) {
		BcLotteryOrder bcOrder = getLotteryOrderDetail(orderId, account, stationId, lotCode);
		if (!stationId.equals(bcOrder.getStationId())) {
			throw new GenericException("订单不存在!");
		}
		if (bcOrder.getLotType() == 6 || bcOrder.getLotType() == 66) {
			List<BcMarkSix> msList = null;
			List<BcMarkSix> msList2 = null;
			if (StringUtils.equals(bcOrder.getPlayCode(), PlayCodeConstants.ezt)) {
				msList = bcMarkSixDao.findByPlayCode(PlayCodeConstants.eztzzt, null, bcOrder.getStationId(),
							bcOrder.getLotType());
				msList2 = bcMarkSixDao.findByPlayCode(PlayCodeConstants.eztzze, null, bcOrder.getStationId(),
						bcOrder.getLotType());
				bcOrder.setPeilv(msList.get(0).getOdds().stripTrailingZeros().toPlainString()+"/"+msList2.get(0).getOdds().stripTrailingZeros().toPlainString());
			}else if (StringUtils.equals(bcOrder.getPlayCode(), PlayCodeConstants.szes)) {
				msList = bcMarkSixDao.findByPlayCode(PlayCodeConstants.szezze, null, bcOrder.getStationId(),
						bcOrder.getLotType());
				msList2 = bcMarkSixDao.findByPlayCode(PlayCodeConstants.szezzs, null, bcOrder.getStationId(),
						bcOrder.getLotType());
				bcOrder.setPeilv(msList.get(0).getOdds().stripTrailingZeros().toPlainString()+"/"+msList2.get(0).getOdds().stripTrailingZeros().toPlainString());
			}else {
				if (StringUtils.equals(bcOrder.getPlayCode(), "ztxztw")) {
					String markType = "";
					if (bcOrder.getHaoMa().endsWith("尾")) {
						markType = BcMarkSixEnum.weishu.name();
					} else {
						markType = BcMarkSixEnum.shengxiao.name();
					}
					msList = bcMarkSixDao.findByPlayCode(bcOrder.getPlayCode(), markType, bcOrder.getStationId(),
							bcOrder.getLotType());
				} else {
					msList = bcMarkSixDao.findByPlayCode(bcOrder.getPlayCode(), null, bcOrder.getStationId(),
							bcOrder.getLotType());
				}
				BigDecimal byIdOdds = null;
				BigDecimal byBZOdds = null; // 中奖的号码都是本命年的生肖或者0尾号码
				if (msList != null) {
					for (BcMarkSix ms : msList) {
						if (Objects.equals(ms.getId(), bcOrder.getMarkSixId())) {
							byIdOdds = ms.getOdds();
						}
						if (ms.getIsNowYear() != null && ms.getIsNowYear() == 1) {
							byBZOdds = ms.getOdds();
						}
					}
					String ss = "";
					if (byIdOdds != null) {
						ss = byIdOdds.setScale(2, RoundingMode.DOWN).toString();
					}
					if (byBZOdds != null) {
						if (ss.equals("")) {
							ss = byBZOdds.setScale(2, RoundingMode.DOWN).toString();
						} else {
							ss = ss + "/" + byBZOdds.setScale(2, RoundingMode.DOWN).toString();
						}
					}
					bcOrder.setPeilv(ss);
				}
			}
		}
		bcOrder.setYingKui(BigDecimalUtil.subtract(
				BigDecimalUtil.addAll(bcOrder.getWinMoney(), bcOrder.getRollBackMoney()), bcOrder.getBuyMoney()));
		return bcOrder;
	}

	/**
	 * 订单详情
	 */
	@Override
	public BcLotteryOrder getLotteryOrderDetail(String orderId, String account, Long stationId, String lotCode) {
		if (orderId == null || stationId == null) {
			throw new GenericException("参数错误");
		}
		BcLotteryOrder blo = lotteryOrderDao.getBcLotteryOrderDetail(orderId, account, stationId, lotCode);
		if (blo == null)
			return null;
		lotCode = blo.getLotCode();
		long sid = 0L;
		if (LotteryEnum.isSysLot(lotCode)) {
			sid = stationId;
		}
		BcLottery lot = lotteryDao.getOne(stationId, lotCode, null);
		if (lot != null) {
			blo.setLotName(lot.getName());
		} else {
			blo.setLotName(LotteryEnum.getEnum(lotCode).getLotName());
		}
		BcLotteryPlay play = bcLotteryPlayDao.getByStationIdAndCodeAndType(stationId, blo.getPlayCode(),
				blo.getLotType());
		if (play == null) {
			throw new GenericException("小类玩法不存在");
		}
		BcLotteryPlayGroup group = lotPlayGroupDao.getById(play.getGroupId());
		if (group == null) {
			throw new GenericException("大类玩法不存在");
		}

		if (group.getName().equals(play.getName())) {
			blo.setPlayName(group.getName() + "-" + play.getName());
		} else {
			blo.setPlayName(group.getName() + "-" + play.getName());
		}
		BcMarkSix markSix = markSixServiceImpl.findByMarkSixIdAndStationId(blo.getMarkSixId(), stationId, blo.getLotType());
		blo.setPlayName(blo.getPlayName()+"-"+markSix.getName());
		
		blo.setGroupName(group.getName());
		if(blo.getStatus() == BusinessConstant.LOTTER_ORDER_STATUS_TIE_V4 && blo.getLotType() == 161){
			//v4版本修改一下和局的赔率
			List<BcMarkSix> tieList = bcMarkSixDao.findByRoomId(blo.getRoomId(), blo.getLotType(), blo.getStationId(), true);
			if(tieList != null && !tieList.isEmpty()){
				for(BcMarkSix six:tieList){
					if(StringUtils.equals(blo.getHaoMa(), "小单")) {
						if(six.getIsNowYear() == 10) {
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) > 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}else if(six.getIsNowYear() == 8){
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) <= 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}
					}else if(StringUtils.equals(blo.getHaoMa(), "大双")) {
						if(six.getIsNowYear() == 11) {
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) > 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}else if(six.getIsNowYear() == 9){
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) <= 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}
					}else {
						if(six.getIsNowYear() == 6){
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) > 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}else if(six.getIsNowYear() == 7){
							if(blo.getBuyMoney().compareTo(six.getMaxBetAmmount()) <= 0){
								blo.setMinBonusOdds(six.getOdds());
								break;
							}
						}
					}
				}
			}
		}else{
			if (blo.getOdds() == null || blo.getOdds().compareTo(BigDecimal.ZERO) <= 0) {
				blo.setMinBonusOdds(
						bcLotteryPlayDao.getPlayMinMoney(blo.getPlayCode(), blo.getStationId(), blo.getLotType()));
			} else {
				blo.setMinBonusOdds(blo.getOdds());
			}
		}
		if (StringUtils.equals(lotCode, LotteryEnum.LHC.name())
				|| StringUtils.equals(lotCode, LotteryEnum.SFLHC.name())
				|| StringUtils.equals(lotCode, LotteryEnum.WFLHC.name())
				|| StringUtils.equals(lotCode, LotteryEnum.TMLHC.name())
				|| StringUtils.equals(lotCode, LotteryEnum.HKMHLHC.name())
				|| StringUtils.equals(lotCode, LotteryEnum.AMLHC.name())
				) {
			if (group.getCode().equals("wsl") || group.getCode().equals("hx") || group.getCode().equals("lx")) {
				// 计算0尾或者本命年赔率
				BcMarkSix sixOdds = bcMarkSixDao.getMarkSixIsNowYearOdds(blo.getPlayCode(), stationId,
						BusinessConstant.status_normal, BusinessConstant.status_normal, blo.getLotType());
				blo.setMinBonusOdds(sixOdds == null ? BigDecimal.ZERO : sixOdds.getOdds());
			} else {
				blo.setMinBonusOdds(BigDecimal.ZERO);
			}
		}
		if (blo.getLotType() == 6 || blo.getLotType() == 66) {
			List<BcMarkSix> msList = null;
			List<BcMarkSix> msList2 = null;
			if (StringUtils.equals(blo.getPlayCode(), PlayCodeConstants.ezt)) {
				msList = bcMarkSixDao.findByPlayCode(PlayCodeConstants.eztzzt, null, blo.getStationId(),
						blo.getLotType());
				msList2 = bcMarkSixDao.findByPlayCode(PlayCodeConstants.eztzze, null, blo.getStationId(),
						blo.getLotType());
				blo.setPeilv(msList.get(0).getOdds().stripTrailingZeros().toPlainString()+"/"+msList2.get(0).getOdds().stripTrailingZeros().toPlainString());
			}else if (StringUtils.equals(blo.getPlayCode(), PlayCodeConstants.szes)) {
				msList = bcMarkSixDao.findByPlayCode(PlayCodeConstants.szezze, null, blo.getStationId(),
						blo.getLotType());
				msList2 = bcMarkSixDao.findByPlayCode(PlayCodeConstants.szezzs, null, blo.getStationId(),
						blo.getLotType());
				blo.setPeilv(msList.get(0).getOdds().stripTrailingZeros().toPlainString()+"/"+msList2.get(0).getOdds().stripTrailingZeros().toPlainString());
			}else {
				
				if (StringUtils.equals(blo.getPlayCode(), "ztxztw")) {
					String markType = "";
					if (blo.getHaoMa().endsWith("尾")) {
						markType = BcMarkSixEnum.weishu.name();
					} else {
						markType = BcMarkSixEnum.shengxiao.name();
					}
					msList = bcMarkSixDao.findByPlayCode(blo.getPlayCode(), markType, blo.getStationId(),
							blo.getLotType());
				} else {
					msList = bcMarkSixDao.findByPlayCode(blo.getPlayCode(), null, blo.getStationId(),
							blo.getLotType());
				}
				BigDecimal byIdOdds = null;
				BigDecimal byBZOdds = null; // 中奖的号码都是本命年的生肖或者0尾号码
				if (msList != null) {
					for (BcMarkSix ms : msList) {
						if (Objects.equals(ms.getId(), blo.getMarkSixId())) {
							byIdOdds = ms.getOdds();
						}
						if (ms.getIsNowYear() != null && ms.getIsNowYear() == 1) {
							byBZOdds = ms.getOdds();
						}
					}
					String ss = "";
					if (byIdOdds != null) {
						ss = byIdOdds.setScale(2, RoundingMode.DOWN).toString();
					}
					if (byBZOdds != null) {
						if (ss.equals("")) {
							ss = byBZOdds.setScale(2, RoundingMode.DOWN).toString();
						} else {
							ss = ss + "/" + byBZOdds.setScale(2, RoundingMode.DOWN).toString();
						}
					}
					blo.setPeilv(ss);
				}
			}
		}
		BcLotteryData data = bcLotteryDataDao.findOne(lotCode, blo.getQiHao(), sid);

		if (data != null) {
			blo.setLotteryHaoMa(data.getHaoMa());
			Calendar c = Calendar.getInstance();
			c.setTime(data.getEndTime());
			if (blo.getAgo() == null) {
				c.add(Calendar.SECOND, -lot.getAgo());
			} else {
				c.add(Calendar.SECOND, -blo.getAgo());
			}
			blo.setSealTime(c.getTime());
			blo.setSellingTime(getSellingTime(lot, data, blo.getQiHao(), blo.getAgo()));
		}

		if (agentBaseConfigService.isOn(StationConfig.onoff_fan_shui.name(), stationId)) {// 返水开关
			int type = BusinessConstant.BET_TYPE_LOTTERY;
			if (lotCode.equals("LHC") || lotCode.equals("SFLHC") || lotCode.equals("WFLHC") || lotCode.equals("TMLHC") || lotCode.equals("HKMHLHC")) {
				if (StringUtils.equals(blo.getPlayCode(), "tm_b")) {
					type = BusinessConstant.BET_TYPE_MARK_SIX;
				} else if (StringUtils.equals(blo.getPlayCode(), "zm_b")) {
					type = 6;
				} else {
					type = 0;
				}
			}
			if (type > 0) {
				MemberBackwaterRecord br = backwaterRecordDao.findOne(blo.getCreateTime(), blo.getAccount(), type,
						blo.getStationId(), blo.getRoomId());
				if (br != null && br.getBackwaterRate() != null) {
					blo.setRollBackRate(br.getBackwaterRate().toString());
					blo.setRollBackMoney(
							BigDecimalUtil.divide(BigDecimalUtil.multiply(blo.getBuyMoney(), br.getBackwaterRate()),
									BigDecimalUtil.HUNDRED, 3));
					blo.setRollBackStatus(br.getBackwaterStatus());
				}
			}
		}
		
		//动态赔率处理
		 String onoffRateRandom = StationConfigUtil.get(StationConfig.onoff_member_rate_random);
		 BigDecimal rate = null;
			//添加赔率百分比
			if("on".equalsIgnoreCase(onoffRateRandom)&& !StringUtils.equals("LHC", lotCode)&& !StringUtils.equals("SFLHC", lotCode) && !StringUtils.equals("WFLHC", lotCode) && !StringUtils.equals("TMLHC", lotCode) && !StringUtils.equals("HKMHLHC", lotCode) && (blo.getStatus()==1 || blo.getStatus()==4)) {
				Long userId  = UserUtil.getUserId();
				MnyMoney money = mnyMoneyService.getMoneyById(userId);
				//动态赔率
				if(money!=null){
					rate =  money.getRate().divide(new BigDecimal(100));
					if(rate.compareTo(BigDecimal.ZERO)>0){
						blo.setOdds(blo.getOdds().multiply(rate).setScale(3, BigDecimal.ROUND_HALF_UP));
					}
				}
			}
		blo.setGroupCode(group.getCode());
		return blo;
	}

	/**
	 * 计算当前期的开始投注时间
	 * 
	 * @param lot
	 * @param curData
	 * @param qiHao
	 * @param sid
	 * @return
	 */
	private Date getSellingTime(BcLottery lot, BcLotteryData curData, String qiHao, Integer ago) {
		Calendar c = Calendar.getInstance();
		if(curData.getStartTime()!=null){
			c.setTime(curData.getStartTime());
		}
		if (lot.getCode().equals(LotteryEnum.LHC.name()) || lot.getCode().equals(LotteryEnum.AMLHC.name())) {
			long qiHaoL = NumberUtils.toLong(qiHao);// 六合彩使用上一期的结束时间
			BcLotteryData data = bcLotteryDataDao.findOne(lot.getCode(), (qiHaoL - 1) + "", 0L);
			if (data != null) {
				c.setTime(data.getEndTime());
			} else {
				c.setTime(curData.getEndTime());
				c.add(Calendar.MINUTE, -2880);
			}
		}
		if (ago == null) {
			c.add(Calendar.SECOND, -lot.getAgo());
		} else {
			c.add(Calendar.SECOND, -ago);
		}
		return c.getTime();
	}

	public SportBettingOrder getSportOrderDetail(String bettingCode, Long stationId) {
		return sportBetDao.getOrderByBettingCode(bettingCode, stationId);
	}

}
