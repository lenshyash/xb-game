package com.game.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.StationConfig;
import com.game.dao.platform.AgentProfitShareRecordDao;
import com.game.dao.platform.AgentProfitShareStrategyDao;
import com.game.model.SysStation;
import com.game.model.platform.AgentProfitShareRecord;
import com.game.model.platform.AgentProfitShareStrategy;
import com.game.service.AgentProfitShareCoreService;
import com.game.service.AgentProfitShareRecordService;
import com.game.util.StationConfigUtil;

@Service
public class AgentProfitShareCoreServiceImpl implements AgentProfitShareCoreService {
	@Autowired
	private AgentProfitShareRecordDao profitShareRecordDao;
	@Autowired
	private AgentProfitShareStrategyDao profitShareStrategyDao;
	@Autowired
	private AgentProfitShareRecordService profitShareRecordService;

	@Override
	public void collectAgentProfitShareRecord(Date start, Date end, Long stationId) {
		List<AgentProfitShareStrategy> strategys = null;
		// 未开启占成开关不进行统计
		if (!"on".equalsIgnoreCase(StationConfigUtil.get(stationId, StationConfig.onoff_profit_share))) {
			return;
		}
		strategys = profitShareStrategyDao.findStationStrategy(stationId);
		// 未开启占成策略不统计
		if (strategys == null || strategys.isEmpty()) {
			return;
		}
		List<AgentProfitShareRecord> list = profitShareRecordDao.collectAgentProfitShareRecord(stationId, start,
				end, strategys);
		if (list == null || list.isEmpty()) {
			return;
		}
		profitShareRecordService.saveAll(list);
	}
}
