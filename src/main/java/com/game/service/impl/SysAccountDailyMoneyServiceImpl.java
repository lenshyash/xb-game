package com.game.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.game.core.SystemConfig;
import com.game.dao.*;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.IndexOperation;
import com.game.elasticsearch.operations.UpdateOperation;
import com.game.model.lottery.BcLotteryData;
import com.game.util.*;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.support.QueryWebParameter;
import org.jay.frame.jdbc.support.QueryWebUtils;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.BusinessConstant;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.model.AgentComprehensiveCount;
import com.game.model.DailyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.SysStation;
import com.game.model.lottery.BcLottery;
import com.game.model.lottery.LotteryEnum;
import com.game.model.vo.AccountVo;
import com.game.model.vo.ReportParamVo;
import com.game.model.vo.ReportVo;
import com.game.model.vo.TotalStatisticVo;
import com.game.service.FrameService;
import com.game.service.MnyMoneyService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.service.SysAccountService;

@Repository
public class SysAccountDailyMoneyServiceImpl implements SysAccountDailyMoneyService {

	@Autowired
	SysAccountDailyMoneyDao dailyMoneyDao;
	
	@Autowired
	SysAccountGuideMoneyDao guideMoneyDao;

	@Autowired
	SysAccountDao accountDao;

	@Autowired
	MnyMoneyDao moneyDao;

	@Autowired
	MnyDrawRecordDao mdrDao;
	
	@Autowired
	MnyMoneyService moneyService;

	@Autowired
	private ComprehensiveCountDao ccDao;

	@Autowired
	private FrameService frameService;
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private BcLotteryDao lotteryDao;
	@Autowired
	private SysStationDao stationDao;
	
	@Autowired
	private SysAccountDailyMoneyDao sysAccountDailyMoneyDao;
	@Autowired
	private MnyMoneyRecordDao moneyRecordDao;
	
	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	@Autowired
	private SysAccountDao sysAccountDao;
	@Autowired
	private MnyMoneyDao mnyMoneyDao;
	@Autowired
	private MnyComRecordDao comRecordDao;
	@Autowired
	private ProxyMultiRebateSettingDao rebateSettingDao;

	@Override
	public Page getFinanceReport(ReportParamVo paramVo) {
		if (StringUtil.isNotEmpty(paramVo.getAgentName())) {
			Long stationId = paramVo.getStationId();
			SysAccount agent = accountDao.getByAccountAndTypeAndStationId(paramVo.getAgentName(), stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);

			if (agent != null) {
				String children = agent.getParents();
				if (StringUtil.isEmpty(children)) {
					children = ",";
				}
				children += agent.getId() + ",";
				paramVo.setSearchSelf(true);
				paramVo.setChildren(children);
				paramVo.setSelfId(agent.getId());
			}
		}
		return dailyMoneyDao.getFinanceReport(paramVo);
	}
	
	@Override
	public Page getRiskReport(ReportParamVo paramVo) {

		if (StringUtil.isNotEmpty(paramVo.getAgentName())) {
			Long stationId = paramVo.getStationId();
			SysAccount agent = accountDao.getByAccountAndTypeAndStationId(paramVo.getAgentName(), stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);

			if (agent != null) {
				String children = agent.getParents();
				if (StringUtil.isEmpty(children)) {
					children = ",";
				}
				children += agent.getId() + ",";
				paramVo.setSearchSelf(true);
				paramVo.setChildren(children);
				paramVo.setSelfId(agent.getId());
			}
		}
		if (paramVo.getType() != null) {
			
			String keyForRisk = getCacheKeyForRisk(paramVo);
			String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, keyForRisk);
			if(jsons!=null) {
				JSONObject obj = JSON.parseObject(jsons);
				HashMap<String,Object> map = new HashMap<String,Object>();
				JSONObject abc = new JSONObject();
				List<? extends JSONObject> list = JSON.parseArray(obj.getString("list"),abc.getClass());
				Page<? extends JSONObject> page = new Page<>(obj.getIntValue("start"), obj.getIntValue("totalCount"), obj.getIntValue("pageSize"), list);
				return page;
			}
			switch (paramVo.getType()) {
			case "cunqu":
				//做一些调整
				Page page = dailyMoneyDao.getRiskCunQuReport(paramVo);
//				List<Map> list = page.getList();
//				for (Map map : list) {
//					String account = (String)map.get("account");
//					Map profitMap = dailyMoneyDao.getProfit(StationUtil.getStationId(), account, paramVo.getBegin(), paramVo.getEnd());
//					map.put("profit", profitMap.get("profit"));
//					map.put("newTotalDeposit", profitMap.get("totalDeposit"));
//					map.put("newTotalWithdraw", profitMap.get("totalWithdraw"));
//				}
//				CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, keyForRisk, page);
				return page;
			case "lottery":
				return dailyMoneyDao.getRiskLotteryReport(paramVo);
			case "sports":
				return dailyMoneyDao.getRiskSportReport(paramVo);
			case "real":
				return dailyMoneyDao.getRiskRealReport(paramVo);
			case "egame":
				return dailyMoneyDao.getRiskEgameReport(paramVo);
			case "hunter":
				return dailyMoneyDao.getRiskHunterReport(paramVo);
			case "markSix":
				return dailyMoneyDao.getRiskMarkSixReport(paramVo);
			case "sysLottery":
				return dailyMoneyDao.getRiskSysLotteryReport(paramVo);
			case "sfMarkSix":
				return dailyMoneyDao.getRiskSfMarkSixReport(paramVo);
			case "thirdSports":
				return dailyMoneyDao.getRiskThirdSportsReport(paramVo);
			case "thirdLottery":
				return dailyMoneyDao.getRiskThirdLotteryReport(paramVo);
			case "chess":
				return dailyMoneyDao.getRiskChessReport(paramVo);
			case "esports":
				return dailyMoneyDao.getRiskEsportsReport(paramVo);
			default:
				return new Page();
			}
		}
		return dailyMoneyDao.getRiskReport(paramVo);
	}

	@Override
	public ReportVo getGlobalReport(ReportParamVo paramVo) {
		String key = getCacheKey(paramVo);
		ReportVo rvo = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, ReportVo.class);
		if (rvo != null && !StringUtil.equals(ReportParamVo.REFRESH_FLAG, paramVo.getRefresh()))
			return rvo;
		
		setChildren(paramVo);
		Map datas = dailyMoneyDao.getReport(paramVo);
		rvo = JsonUtil.toBean(JsonUtil.toJson(datas), ReportVo.class);
		
		Integer yesCount = 0;
		Integer tdyCount = 0;
		if(StationUtil.isDailiStation()) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.MILLISECOND, 0);
			Date toDay = c.getTime();
			c.add(Calendar.DAY_OF_MONTH, -1);
			Date yesDay = c.getTime();
			Date now = new Date();
			tdyCount = accountDao.queryRegisterCount(paramVo.getStationId(),paramVo.getChildren(),toDay,now);
			yesCount = accountDao.queryRegisterCount(paramVo.getStationId(),paramVo.getChildren(),yesDay,toDay);
		}
		rvo.setRegisterAccountYes(yesCount.longValue());
		rvo.setRegisterAccount(tdyCount.longValue());
		// 取相反数
		rvo.setWithdrawTotal(getNegate(rvo.getWithdrawTotal()));
		rvo.setManualWithdrawTotal(getNegate(rvo.getManualWithdrawTotal()));
		rvo.setLotteryTotal(getNegate(rvo.getLotteryTotal()));
		rvo.setSysLotteryTotal(getNegate(rvo.getSysLotteryTotal()));
		rvo.setSportTotal(getNegate(rvo.getSportTotal()));
		rvo.setMarkSixTotal(getNegate(rvo.getMarkSixTotal()));
		rvo.setSfMarkSixTotal(getNegate(rvo.getSfMarkSixTotal()));

		rvo.setSportBunko(sum(getNegate(rvo.getSportAward()), rvo.getSportTotal()));
		rvo.setLotteryBunko(sum(getNegate(rvo.getLotteryAward()), rvo.getLotteryTotal()));
		rvo.setSfMarkSixBunko(sum(getNegate(rvo.getSfMarkSixAward()), rvo.getSfMarkSixTotal()));
		rvo.setSysLotteryBunko(sum(getNegate(rvo.getSysLotteryAward()), rvo.getSysLotteryTotal()));
		rvo.setMarkSixBunko(sum(getNegate(rvo.getMarkSixAward()), rvo.getMarkSixTotal()));
		rvo.setDianZiBunko(sum(getNegate(rvo.getDianZiAward()), rvo.getDianZiTotal()));
		rvo.setRealBunko(sum(getNegate(rvo.getRealAward()), rvo.getRealTotal()));
		rvo.setThirdSportsBunko(sum(getNegate(rvo.getThirdSportsAward()), rvo.getThirdSportsTotal()));
		rvo.setThirdLotteryBunko(sum(getNegate(rvo.getThirdLotteryAward()), rvo.getThirdLotteryTotal()));
		rvo.setChessBunko(sum(getNegate(rvo.getChessAward()), rvo.getChessTotal()));
		rvo.setEsportsBunko(sum(getNegate(rvo.getEsportsAward()), rvo.getEsportsTotal()));
		rvo.setAllBunko(sum(rvo.getSportBunko(), rvo.getLotteryBunko(), rvo.getSysLotteryBunko(), rvo.getRealBunko(),
				rvo.getDianZiBunko(), rvo.getMarkSixBunko(), rvo.getSfMarkSixBunko(),
				rvo.getChessBunko(), rvo.getThirdSportsBunko(),  rvo.getThirdLotteryBunko(), 
				getNegate(rvo.getRebateAgentTotal()), getNegate(rvo.getRebateTotal())));
		if(StationUtil.isDailiStation()) {
			BigDecimal sumMoney = moneyDao.getsumMoney(paramVo);
			rvo.setSumMoney(sumMoney);
		}
		//管理后台
		if(StationUtil.isAgentStation()) {
			BigDecimal sumMoney = BigDecimal.ZERO;  ;
			//先找出
			SysAccount account = sysAccountDao.findOneByAccountAndStationId(paramVo.getAgentName(), paramVo.getStationId());
			if(account!=null && account.getAccountType()==SysAccount.ACCOUNT_PLATFORM_AGENT) {
				paramVo.setSearchType(ReportParamVo.SEARCHTYPE_NEXT_ALL);
				paramVo.setSelfId(account.getId());
				paramVo.setChildren(UserUtil.getChildren(account));
				sumMoney = moneyDao.getsumMoney(paramVo);
				
			}
			rvo.setSumMoney(sumMoney);
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, rvo);
		return rvo;
	}



	@Override
	public ReportVo getGlobalReportAdmin(ReportParamVo paramVo) {
		String key = getCacheKey(paramVo);
		ReportVo rvo = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, ReportVo.class);
		if (rvo != null && !StringUtil.equals(ReportParamVo.REFRESH_FLAG, paramVo.getRefresh()))
				return rvo;

			setChildren(paramVo);
		Map datas = dailyMoneyDao.getReport(paramVo);
		rvo = JsonUtil.toBean(JsonUtil.toJson(datas), ReportVo.class);

		Integer yesCount = 0;
		Integer tdyCount = 0;
		if(StationUtil.isDailiStation()) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.MILLISECOND, 0);
			Date toDay = c.getTime();
			c.add(Calendar.DAY_OF_MONTH, -1);
			Date yesDay = c.getTime();
			Date now = new Date();
			tdyCount = accountDao.queryRegisterCount(paramVo.getStationId(),paramVo.getChildren(),toDay,now);
			yesCount = accountDao.queryRegisterCount(paramVo.getStationId(),paramVo.getChildren(),yesDay,toDay);
		}
		rvo.setRegisterAccountYes(yesCount.longValue());
		rvo.setRegisterAccount(tdyCount.longValue());
		// 取相反数
		rvo.setWithdrawTotal(getNegate(rvo.getWithdrawTotal()));
		rvo.setManualWithdrawTotal(getNegate(rvo.getManualWithdrawTotal()));
		rvo.setLotteryTotal(getNegate(rvo.getLotteryTotal()));
		rvo.setSysLotteryTotal(getNegate(rvo.getSysLotteryTotal()));
		rvo.setSportTotal(getNegate(rvo.getSportTotal()));
		rvo.setMarkSixTotal(getNegate(rvo.getMarkSixTotal()));
		rvo.setSfMarkSixTotal(getNegate(rvo.getSfMarkSixTotal()));
		rvo.setSportBunko(sum(getNegate(rvo.getSportAward()), rvo.getSportTotal()));
		rvo.setLotteryBunko(sum(getNegate(rvo.getLotteryAward()), rvo.getLotteryTotal()));
		rvo.setSfMarkSixBunko(sum(getNegate(rvo.getSfMarkSixAward()), rvo.getSfMarkSixTotal()));
		rvo.setSysLotteryBunko(sum(getNegate(rvo.getSysLotteryAward()), rvo.getSysLotteryTotal()));
		rvo.setMarkSixBunko(sum(getNegate(rvo.getMarkSixAward()), rvo.getMarkSixTotal()));
		rvo.setDianZiBunko(sum(getNegate(rvo.getDianZiAward()), rvo.getDianZiTotal()));
		rvo.setRealBunko(sum(getNegate(rvo.getRealAward()), rvo.getRealTotal()));
		rvo.setThirdSportsBunko(sum(getNegate(rvo.getThirdSportsAward()), rvo.getThirdSportsTotal()));
		rvo.setThirdLotteryBunko(sum(getNegate(rvo.getThirdLotteryAward()), rvo.getThirdLotteryTotal()));
		rvo.setChessBunko(sum(getNegate(rvo.getChessAward()), rvo.getChessTotal()));
		rvo.setEsportsBunko(sum(getNegate(rvo.getEsportsAward()), rvo.getEsportsTotal()));
		rvo.setAllBunko(sum(rvo.getSportBunko(), rvo.getLotteryBunko(), rvo.getSysLotteryBunko(), rvo.getRealBunko(),
				rvo.getDianZiBunko(), rvo.getMarkSixBunko(), rvo.getSfMarkSixBunko(),
				rvo.getChessBunko(), rvo.getThirdSportsBunko(),  rvo.getThirdLotteryBunko(),
				getNegate(rvo.getRebateAgentTotal()), getNegate(rvo.getRebateTotal())));
		if(StationUtil.isDailiStation()) {
			BigDecimal sumMoney = moneyDao.getsumMoney(paramVo);
			rvo.setSumMoney(sumMoney);
		}
		//管理后台
//		if(StationUtil.isAgentStation()) {
			BigDecimal sumMoney = BigDecimal.ZERO;  ;
			//先找出
//			SysAccount account = sysAccountDao.findOneByAccountAndStationId(paramVo.getAgentName(), paramVo.getStationId());
//			if(account!=null ) {
//				paramVo.setSearchType(ReportParamVo.SEARCHTYPE_NEXT_ALL);
//				paramVo.setSelfId(account.getId());
//				paramVo.setChildren(UserUtil.getChildren(account));
				sumMoney = moneyDao.getsumMoneyAdmin(paramVo);

//			}
			rvo.setSumMoney(sumMoney);
//		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, rvo);
		return rvo;
	}


	@Override
	public ReportVo getGlobalReportByNative(ReportParamVo paramVo) {
		String key = getCacheKey(paramVo);
		ReportVo rvo = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key, ReportVo.class);
		if (rvo != null && !StringUtil.equals(ReportParamVo.REFRESH_FLAG, paramVo.getRefresh()))
			return rvo;
		setChildren(paramVo);
		Map datas = null;
		if(StringUtil.equals(UserUtil.getType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
			datas = guideMoneyDao.getReport(paramVo);
		}else {
			datas = dailyMoneyDao.getReport(paramVo);
		}
		
		rvo = JsonUtil.toBean(JsonUtil.toJson(datas), ReportVo.class);
		// 取相反数
		rvo.setWithdrawTotal(getNegate(rvo.getWithdrawTotal()));
		rvo.setManualWithdrawTotal(getNegate(rvo.getManualWithdrawTotal()));
		rvo.setLotteryTotal(getNegate(rvo.getLotteryTotal()));
		rvo.setSysLotteryTotal(getNegate(rvo.getSysLotteryTotal()));
		rvo.setSportTotal(getNegate(rvo.getSportTotal()));
		rvo.setMarkSixTotal(getNegate(rvo.getMarkSixTotal()));
		rvo.setSfMarkSixTotal(getNegate(rvo.getSfMarkSixTotal()));

		rvo.setSportBunko(sum(getNegate(rvo.getSportAward()), rvo.getSportTotal()));
		rvo.setLotteryBunko(sum(getNegate(rvo.getLotteryAward()), rvo.getLotteryTotal()));
		rvo.setSfMarkSixBunko(sum(getNegate(rvo.getSfMarkSixAward()), rvo.getSfMarkSixTotal()));
		rvo.setSysLotteryBunko(sum(getNegate(rvo.getSysLotteryAward()), rvo.getSysLotteryTotal()));
		rvo.setMarkSixBunko(sum(getNegate(rvo.getMarkSixAward()), rvo.getMarkSixTotal()));
		rvo.setDianZiBunko(sum(getNegate(rvo.getDianZiAward()), rvo.getDianZiTotal()));
		rvo.setRealBunko(sum(getNegate(rvo.getRealAward()), rvo.getRealTotal()));
		rvo.setThirdSportsBunko(sum(getNegate(rvo.getThirdSportsAward()), rvo.getThirdSportsTotal()));
		rvo.setThirdLotteryBunko(sum(getNegate(rvo.getThirdLotteryAward()), rvo.getThirdLotteryTotal()));
		rvo.setChessBunko(sum(getNegate(rvo.getChessAward()), rvo.getChessTotal()));

		rvo.setAllBunko(sum(rvo.getSportBunko(), rvo.getLotteryBunko(), rvo.getSysLotteryBunko(), rvo.getRealBunko(),
				rvo.getDianZiBunko(), rvo.getMarkSixBunko(), rvo.getSfMarkSixBunko(),
				rvo.getChessBunko(), rvo.getThirdSportsBunko(),  rvo.getThirdLotteryBunko(), 
				getNegate(rvo.getRebateAgentTotal()), getNegate(rvo.getRebateTotal())));
		BigDecimal sumMoney = moneyDao.getsumMoney(paramVo);
		rvo.setSumMoney(sumMoney);
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, rvo);
		return rvo;
	}
	@Override
	public Page getDayReport(ReportParamVo paramVo) {
		String key = getCacheKey(paramVo);
		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);
		if (jsons != null) {
			return JsonUtil.toBean(jsons, Page.class);
		}
		setChildren(paramVo);
		if(paramVo.getBegin()==null||paramVo.getEnd()==null) {
			String today=DateUtil.getCurrentDate();
			paramVo.setBegin(DateUtil.toDate(today));
			paramVo.setEnd(DateUtil.toDate(today));
		}
		List<AgentComprehensiveCount> accs= ccDao.getComprehensives(paramVo.getStationId(),paramVo.getBegin(),paramVo.getEnd());
		Page page=dailyMoneyDao.getDayReport(paramVo);
		
		if(page != null && page.getList() != null && !page.getList().isEmpty()) {
			for (Map map : (List<Map>)page.getList()) {
				paramVo.setSeachDate(map.get("statDate").toString());
				map.put("registerCount", accountDao.getRegisterCount(paramVo));
				Map temp = comRecordDao.countRechargeNumDay(paramVo);
				map.putAll(temp);
				
				Map todayFirstCom = comRecordDao.todayFirstCom(paramVo);
				map.putAll(todayFirstCom);
				Map previousFirstCom = comRecordDao.previousFirstCom(paramVo);
				map.putAll(previousFirstCom);
			}
			paramVo.setEnd(DateUtil.getTomorrow(paramVo.getEnd()));
			//获取首充二充总额
			Map comTotal = comRecordDao.countRechargeTotal(paramVo);
			if(comTotal!=null && page.getAggsData()!=null){
				page.getAggsData().putAll(comTotal);
			}
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		return page;
	}
	
	public Long getRedisCount(List<AgentComprehensiveCount> accs,String countDate) {
		if(accs == null || accs.isEmpty()) {
			return 0l;
		}
		for (AgentComprehensiveCount agentComprehensiveCount : accs) {
			if(DateUtil.formatDate(agentComprehensiveCount.getCountDate(), DateUtil.DATE_FORMAT_STR).equals(countDate)) {
				return agentComprehensiveCount.getRegisterAccount();
			}
		}
		
		return 0l;
	}

	@Override
	public Page getTotalStatistic(Long stationId, String account, String agentName, Date startTime, Date endTime, Long searchType,Integer pageNo, Integer pageSize, String sortName, String sortOrder, Long reportType) {
		if (stationId == null) {
			return null;
		}
		String parentIds = null;
		if(StationUtil.isDailiStation() && StringUtil.isEmpty(agentName)){
			agentName = UserUtil.getUserAccount();
		}
		String key = getCacheKey(stationId, account, agentName, startTime, endTime,searchType, pageNo, pageSize, sortName, sortOrder, reportType);
		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);
		Page page = null;
		if (jsons != null) {
			return JsonUtil.toBean(jsons, Page.class);
		}
		Long agentId = null;
		if(StationUtil.isDailiStation() && UserUtil.getUserAccount().equals(agentName)) {
			parentIds = UserUtil.getChildren();
		}else if (StringUtils.isNotEmpty(agentName)) {
			AccountVo avo = new AccountVo();
			avo.setStationId(stationId);
			avo.setAccount(agentName);
			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			if(StationUtil.isDailiStation()) {
				avo.setParents(UserUtil.getChildren());
			}
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null) {
				if(StationUtil.isDailiStation() ) {
					return new Page();
				}
				throw new GenericException("代理不存在！");
			}
			parentIds = agent.getParents();
			if (StringUtil.isEmpty(parentIds)) {
				parentIds = ",";
			}
			parentIds = parentIds + agent.getId() + ",";
			agentId = agent.getId();
		}
		
		page = dailyMoneyDao.findTeamStatistic(stationId, account, parentIds, agentId, startTime, endTime,searchType, sortName, sortOrder, reportType);
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		return page;
	}
	
	@Override
	public Page getTotalStatisticTwo(Long stationId, String account, String agentName, Date startTime, Date endTime, Long searchType,Long reportType,Integer pageNo, Integer pageSize) {
		if (stationId == null) {
			return null;
		}
		String parentIds = null;
		if(StationUtil.isDailiStation() && StringUtil.isEmpty(agentName)){
			agentName = UserUtil.getUserAccount();
		}
		String key = getCacheKeyTwo(stationId, account, agentName, startTime, endTime,searchType,reportType, pageNo, pageSize);
		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);
		Page page = null;
		if (jsons != null) {
			return JsonUtil.toBean(jsons, Page.class);
		}
		Long agentId = null;
		if(StationUtil.isDailiStation() && UserUtil.getUserAccount().equals(agentName)) {
			parentIds = UserUtil.getChildren();
		}else if (StringUtils.isNotEmpty(agentName)) {
			AccountVo avo = new AccountVo();
			avo.setStationId(stationId);
			avo.setAccount(agentName);
			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			if(StationUtil.isDailiStation()) {
				avo.setParents(UserUtil.getChildren());
			}
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null) {
				if(StationUtil.isDailiStation() ) {
					return new Page();
				}
				throw new GenericException("代理不存在！");
			}
			parentIds = agent.getParents();
			if (StringUtil.isEmpty(parentIds)) {
				parentIds = ",";
			}
			parentIds = parentIds + agent.getId() + ",";
			agentId = agent.getId();
		}
		

			String parents = null;
			Long agentIdL = null;
			String account2 = null;
			Long id = null;
			if(StringUtil.isNotEmpty(agentName)) {
				AccountVo avo = new AccountVo();
				avo.setStationId(stationId);
				avo.setAccount(agentName);
				avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
				SysAccount agent = accountDao.queryAccount(avo);
				parents = agent.getParents();
				agentIdL = agent.getAgentId();
				account2 = agent.getAccount(); 
				id = agent.getId();
				
			}
			page = dailyMoneyDao.findTeamStatistic2(stationId, account2, parents, agentIdL,id, startTime, endTime,searchType,reportType);
			 List<Map> list = page.getList();

			 for (Map map : list) {
				 String accountId = map.get("agentId").toString();
				 //获取返点数

				 Map rebateMap = rebateSettingDao.getAccountRebate(Long.parseLong(accountId));
				 if(rebateMap!=null && rebateMap.containsKey("gameShare")){
					 map.put("gameShare",rebateMap.get("gameShare"));
				 }
					Map accountById = accountDao.getAccountById(Long.parseLong(accountId), stationId);
					String agentName2 = accountById.get("account").toString();

					ReportParamVo paramVo = new ReportParamVo();
					paramVo.setAccount("");
					paramVo.setAgentName(agentName2);
					paramVo.setStationId(stationId);
					setChildren(paramVo);
					String children = paramVo.getChildren();
				   Map totalMap = sysAccountDailyMoneyDao.findBetCountAndFirstDepositTotal(accountId,children,stationId.toString(),startTime,endTime,reportType);
				   Object betCountTotal = totalMap.get("betCountTotal");
				   Object firstDepositTotal = totalMap.get("firstDepositTotal");
				   map.put("betCountTotal", betCountTotal);
				   map.put("firstDepositTotal", firstDepositTotal);
				   // 

					Map upUpAgentName = accountDao.getUpUpAgentName(stationId, agentName2);

					String string = "";
					
					if(upUpAgentName==null) {
						string = "";
					}else if(upUpAgentName.get("agentName")==null){
						string = "";
					}else {
						Object agentName3 = upUpAgentName.get("agentName");
						string = agentName3.toString();
					}
					
					
					map.put("agentName", agentName2);
					map.put("agentName2", string);
			}
		
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		return page;
	}

	@Override
	public Map getComprehensiveCountMap(Long stationId,Long reportType, String beginS, String endS) {
		String cacheKey =  "ComprehensiveCount_" + stationId+"reportType:"+reportType+"beginS:"+beginS+"endS:"+endS;
		
		String json = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, cacheKey);
		if (json != null) {
			return JSON.parseObject(json, Map.class);
		}
		//统计概况只看报表类型普通开关
//		Long reportType= 0L;
//		String reportSwitch = StationConfigUtil.get(stationId, StationConfig.comprehensive_only_report_type_normal);
//		if("on".equals(reportSwitch)) {
//			reportType = 1L;
//		}
		
		Calendar now = Calendar.getInstance();
		// 昨日统计记录
		now.add(Calendar.DAY_OF_MONTH, -1);
		Map map_yes = ccDao.getComprehensiveCount(stationId, now.getTime());
		Map new_map_yes = dailyMoneyDao.getComprehensiveCount(stationId, now.getTime(),reportType);
		if (map_yes == null) {
			map_yes = new HashMap();
		}
		if (new_map_yes != null) {
			for (Object key : new_map_yes.keySet()) {
				map_yes.put(key, new_map_yes.get(key));
			}
		}
		// 得到当日统计记录
		now.add(Calendar.DAY_OF_MONTH, 1);
		Map map_tdy = ccDao.getComprehensiveCount(stationId, now.getTime());
		Map new_map_tdy = dailyMoneyDao.getComprehensiveCount(stationId, now.getTime(),reportType);
		if (map_tdy == null) {
			map_tdy = new HashMap();
		}
		if (new_map_tdy != null) {
			for (Object key : new_map_tdy.keySet()) {
				map_tdy.put(key, new_map_tdy.get(key));
			}
		}

		dataHandler(map_tdy, map_yes);
		Date end = null;
		Date begin = null;
		
		//默认查询近两个月
		if(StringUtil.isEmpty(beginS) || StringUtil.isEmpty(endS)){
			Calendar temp = Calendar.getInstance();
			temp.set(Calendar.DAY_OF_MONTH, 1);
			temp.set(Calendar.HOUR_OF_DAY, 0);
			temp.set(Calendar.MINUTE, 0);
			temp.set(Calendar.SECOND, 0);
			temp.set(Calendar.MILLISECOND, 0);
			end = temp.getTime();
			temp.add(Calendar.MONTH, -1);
			begin = temp.getTime();
		}else{
			begin = DateUtil.parseDate(beginS, "yyyy-MM");
			end = DateUtil.parseDate(endS, "yyyy-MM");
		}
		initYearReport(map_tdy, stationId, now,begin,end,reportType);
		map_tdy.put("allBalance", moneyDao.getStationMoney(stationId,reportType));
		//获取充值次数统计
		Map crn = comRecordDao.countRechargeNum(stationId,null);
		map_tdy.putAll(crn);
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, cacheKey, map_tdy);
		return map_tdy;
	}

	/**
	 * 数据组装
	 * 
	 * @param map_tdy
	 * @param map_yes
	 */
	private void dataHandler(Map map_tdy, Map map_yes) {
		BigDecimal betNumYes = BigDecimal.ZERO;// 昨日投注
		BigDecimal winNumYes = BigDecimal.ZERO;// 昨日中奖
		BigDecimal agentRebateYes = BigDecimal.ZERO;// 昨日返点
		BigDecimal memberRebateYes = BigDecimal.ZERO;// 昨日反水
		BigDecimal betNumTdy = BigDecimal.ZERO;// 今日投注
		BigDecimal winNumTdy = BigDecimal.ZERO;// 今日中奖
		BigDecimal agentRebateTdy = BigDecimal.ZERO;// 今日返点
		BigDecimal memberRebateTdy = BigDecimal.ZERO;// 今日反水
		BigDecimal bunkoTdy = BigDecimal.ZERO;// 今日盈亏
		BigDecimal bunkoYes = BigDecimal.ZERO;// 昨天盈亏
		Long registerAccountYes = 0l;
		if (map_tdy != null) {
			betNumTdy = StringUtil.toBigDecimal(map_tdy.get("betAmount"));
			winNumTdy = StringUtil.toBigDecimal(map_tdy.get("awardAmount"));
			agentRebateTdy = StringUtil.toBigDecimal(map_tdy.get("agentRebate"));
			memberRebateTdy = StringUtil.toBigDecimal(map_tdy.get("memberRebate"));
			bunkoTdy = sum(betNumTdy, getNegate(winNumTdy), getNegate(agentRebateTdy), getNegate(memberRebateTdy));
			if (map_yes != null) {
				betNumYes = StringUtil.toBigDecimal(map_yes.get("betAmount"));
				winNumYes = StringUtil.toBigDecimal(map_yes.get("awardAmount"));
				agentRebateYes = StringUtil.toBigDecimal(map_yes.get("agentRebate"));
				memberRebateYes = StringUtil.toBigDecimal(map_yes.get("memberRebate"));

				bunkoYes = sum(betNumYes, getNegate(winNumYes), getNegate(agentRebateYes), getNegate(memberRebateYes));
				registerAccountYes = StringUtil.toLong(map_yes.get("registerAccount"));
				map_tdy.put("depositCountYes", map_yes.get("depositCount"));
				map_tdy.put("firstDepositYes", map_yes.get("firstDeposit"));
			}
			map_tdy.put("bunkoTdy", bunkoTdy.floatValue());
			map_tdy.put("betAmountYes", betNumYes.floatValue());
			map_tdy.put("winAmountYes", winNumYes.floatValue());
			map_tdy.put("bunkoYes", bunkoYes.floatValue());
			map_tdy.put("registerAccountYes", registerAccountYes);
			map_tdy.put("online", accountService.getOnlineCount());// 在线人数
			map_tdy.put("accountCount",
					StringUtil.toLong(map_tdy.get("agentCount")) + StringUtil.toLong(map_tdy.get("memberCount")));
		}
	}

	/**
	 * 
	 * @param map_tdy
	 */
	@SuppressWarnings("unchecked")
	private void initYearReport(Map map_tdy, Long stationId, Calendar now,Date begin,Date end,Long reportType) {
		List<Map> allyea = null;
		String curMonth = DateUtil.formatDate(now.getTime(), "yyyy-MM");
		if(end!=null){
			now.add(Calendar.MONTH, 1);
			now.add(Calendar.DAY_OF_MONTH, -1);
		}
		String key = Contants.COMPREHENSIVE_ALL_YEAR_MONTH + curMonth + "_" + stationId+"_"+reportType;
		String jsons = RedisAPI.getCache(key, Contants.COMPREHENSIVE_ALL_YEAR_MONTH_DB);

//		Long reportType= 0L;
//		String reportSwitch = StationConfigUtil.get(stationId, StationConfig.comprehensive_only_report_type_normal);
//		if("on".equals(reportSwitch)) {
//			reportType = 1L;
//		}
		// 截至三个月前9个月统计集合
		now.add(Calendar.DAY_OF_MONTH, 1);
		Date lastEndTime = now.getTime();
		now.set(Calendar.DAY_OF_MONTH, 1);
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		now.add(Calendar.MONTH, -3);
		Date endTime = now.getTime();
		now.add(Calendar.MONTH, -8);
		Date startTime = now.getTime();
		if (jsons != null) {
			allyea = JSONArray.parseArray(jsons, Map.class);
		} else {
			allyea = dailyMoneyDao.getMonthReports(stationId, startTime, endTime,reportType);
			RedisAPI.addCache(key, JsonUtil.toJson(allyea), Contants.COMPREHENSIVE_ALL_YEAR_MONTH_TIMEOUT,
					Contants.COMPREHENSIVE_ALL_YEAR_MONTH_DB);
		}
		List<Map> lastMap = dailyMoneyDao.getMonthReports(stationId, endTime, lastEndTime,reportType);

		if (allyea == null || allyea.isEmpty()) {
			allyea = lastMap;
		} else if (lastMap != null && !lastMap.isEmpty()) {
			allyea.addAll(lastMap);
		}
		
		if (allyea != null && !allyea.isEmpty()) {
			if(begin!=null && end!=null){
				List<Map> newyear = new ArrayList<Map>();
				for(int i =0;i<allyea.size();i++){
					Object ym1 = allyea.get(i).get("ym");
					//获取首充次数
//					Map map = comRecordDao.countRechargeNum(stationId,ym1.toString());
//					allyea.get(i).putAll(map);
					Date d1 = DateUtil.parseDate(ym1.toString(), "yyyy-MM");
					if(d1.compareTo(begin)>-1 && d1.compareTo(end)<1){
						newyear.add(allyea.get(i));
					}

				}
				if(newyear!=null){
					allyea = newyear;
				}
			}
			Collections.sort(allyea, new Comparator() {
				public int compare(Object obj1, Object obj2) {
					Map a = (Map) obj1;
					Map b = (Map) obj2;
					Object ym1 = a.get("ym");
					Object ym2 = b.get("ym");

					if (ym2 == null) {
						return -1;
					}

					if (ym1 == null) {
						return 1;
					}

					Date d1 = DateUtil.parseDate(ym1.toString(), "yyyy-MM");
					Date d2 = DateUtil.parseDate(ym2.toString(), "yyyy-MM");
					return d1.compareTo(d2);
				}
			});
		}

		map_tdy.put("allyear", allyea);
	}

	private String getCacheKey(ReportParamVo paramVo) {
		StringBuilder sb = new StringBuilder();
		sb.append(paramVo.getStationId());
		if(StringUtil.isNotEmpty(paramVo.getCacheName())) {
			sb.append("_cn_").append(paramVo.getCacheName());
		}
		if(StationUtil.isDailiStation()) {
			sb.append("_pt_").append("d");
		}else {
			sb.append("_pt_").append("a");
		}
		sb.append("_a_").append(paramVo.getAccount());
		sb.append("_b_").append(paramVo.getAgentName());
		if (paramVo.getBegin() != null) {
			sb.append("_s_").append(DateUtil.toDateStr(paramVo.getBegin()));
		}
		if (paramVo.getEnd() != null) {
			sb.append("_e_").append(DateUtil.toDateStr(paramVo.getEnd()));
		}
		sb.append("_c_").append(paramVo.getChildren());
		sb.append("_ss_").append(paramVo.getSearchSelf());
		sb.append("_si_").append(paramVo.getSelfId());
		sb.append("_st_").append(paramVo.getSearchType());
		
		if (Validator.isNotNull(paramVo.getReportType())) {
			sb.append("_rt_").append(paramVo.getReportType());
		}
		
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(UserUtil.getRequest());
		int pageNo = webParam.getPageNo();
		int pageSize = webParam.getPageSize();
		if (pageNo > 0) {
			sb.append("_pn_").append(pageNo);
		}
		if (pageSize > 0) {
			sb.append("_ps_").append(pageSize);
		}
		return sb.toString();
	}
	
	private String getCacheKeyForRisk(ReportParamVo paramVo) {
		//额外增加这个 标识风险类型rediskey
		StringBuilder sb = new StringBuilder("risk:report:");
		
		sb.append(paramVo.getStationId());
		if(StringUtil.isNotEmpty(paramVo.getCacheName())) {
			sb.append("_cn_").append(paramVo.getCacheName());
		}
		if(StationUtil.isDailiStation()) {
			sb.append("_pt_").append("d");
		}else {
			sb.append("_pt_").append("a");
		}
		sb.append("_a_").append(paramVo.getAccount());
		sb.append("_b_").append(paramVo.getAgentName());
		if (paramVo.getBegin() != null) {
			sb.append("_s_").append(DateUtil.toDateStr(paramVo.getBegin()));
		}
		if (paramVo.getEnd() != null) {
			sb.append("_e_").append(DateUtil.toDateStr(paramVo.getEnd()));
		}
		sb.append("_c_").append(paramVo.getChildren());
		sb.append("_ss_").append(paramVo.getSearchSelf());
		sb.append("_si_").append(paramVo.getSelfId());
		sb.append("_st_").append(paramVo.getSearchType());
		
		if (Validator.isNotNull(paramVo.getReportType())) {
			sb.append("_rt_").append(paramVo.getReportType());
		}
		
		//额外增加一个type
		if (paramVo!=null) {
			sb.append("_type_").append(paramVo.getType());
		}
		
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(UserUtil.getRequest());
		int pageNo = webParam.getPageNo();
		int pageSize = webParam.getPageSize();
		if (pageNo > 0) {
			sb.append("_pn_").append(pageNo);
		}
		if (pageSize > 0) {
			sb.append("_ps_").append(pageSize);
		}
		return sb.toString();
	}
	
	private String getCacheKey(Long stationId, String account, String agentName, Date startTime, Date endTime,Long searchType, Integer pageNo, Integer pageSize,String sortName,String sortOrder, Long reportType) {
		StringBuilder sb = new StringBuilder();
		sb.append(stationId);
		sb.append("_a_").append(account);
		sb.append("_b_").append(agentName);
		if (startTime != null) {
			sb.append("_s_").append(DateUtil.toDateStr(startTime));
		}
		if (endTime != null) {
			sb.append("_e_").append(DateUtil.toDateStr(endTime));
		}
		sb.append("_st_").append(searchType);
		sb.append("_p_").append(pageNo);
		sb.append("_ps_").append(pageSize);
		sb.append("_sn_").append(sortName);
		sb.append("_so_").append(sortOrder);
		sb.append("_rt_").append(reportType);
		return sb.toString();
	}

	private void setChildren(ReportParamVo paramVo) {
		SysAccount agent = null;
		if (StringUtil.isNotEmpty(paramVo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(paramVo.getStationId());
			avo.setAccount(paramVo.getAgentName());
//			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			agent = accountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			paramVo.setChildren(parents + agent.getId() + ",");
			paramVo.setSearchSelf(true);
			paramVo.setSelfId(agent.getId());
		}
		if (StringUtil.isNotEmpty(paramVo.getAccount())) {
			SysAccount acc = accountDao.findOneByAccountAndStationId(paramVo.getAccount(), paramVo.getStationId());
			if (acc != null) {
				paramVo.setAccountId(acc.getId());
			}
		}
	}

	private BigDecimal getNegate(Object val) {
		return getNegate(StringUtil.toBigDecimal(val));
	}

	private BigDecimal getNegate(BigDecimal val) {
		if (val == null) {
			return BigDecimal.ZERO;
		}
		return val.negate();
	}

	/**
	 * 统计
	 * 
	 * @return
	 */
	private BigDecimal sum(Object... secg) {
		BigDecimal total = BigDecimal.ZERO;

		if (secg != null && secg.length > 0) {
			for (int i = 0; i < secg.length; i++) {
				total = total.add(StringUtil.toBigDecimal(secg[i]));
			}
		}
		return total;
	}

	@Override
	public int saveThirdAmount(SysAccountDailyMoney dailyMoney, SysAccount acc){
		SysAccountDailyMoney old = dailyMoneyDao.findOneByAccountIdAndStatDate(dailyMoney.getAccountId(),
				dailyMoney.getStatDate());
		if (old == null) {
			old = new SysAccountDailyMoney();
			old.setAccountId(dailyMoney.getAccountId());
			old.setStatDate(dailyMoney.getStatDate());
			old.setAccount(acc.getAccount());
			old.setAccountType(acc.getAccountType());
			old.setParents(acc.getParents());
			old.setStationId(acc.getStationId());
			old.setAgentId(acc.getAgentId());
			old.setAgentName(acc.getAgentName());
			old.setBalance(moneyDao.getBalance(dailyMoney.getAccountId()));
			old.setRealBetAmount(dailyMoney.getRealBetAmount());
			old.setRealWinAmount(dailyMoney.getRealWinAmount());
			old.setRealBetTimes(dailyMoney.getRealBetTimes());
			old.setRealWinTimes(dailyMoney.getRealWinTimes());
			old.setEgameBetAmount(dailyMoney.getEgameBetAmount());
			old.setEgameWinAmount(dailyMoney.getEgameWinAmount());
			old.setEgameBetTimes(dailyMoney.getEgameBetTimes());
			old.setEgameWinTimes(dailyMoney.getEgameWinTimes());
			old.setHunterBetAmount(dailyMoney.getHunterBetAmount());
			old.setHunterWinAmount(dailyMoney.getHunterWinAmount());
			old.setHunterBetTimes(dailyMoney.getHunterBetTimes());
			old.setHunterWinTimes(dailyMoney.getHunterWinTimes());
			old.setThirdSportsBetAmount(dailyMoney.getThirdSportsBetAmount());
			old.setThirdSportsWinAmount(dailyMoney.getThirdSportsWinAmount());
			old.setThirdSportsBetTimes(dailyMoney.getThirdSportsBetTimes());
			old.setThirdSportsWinTimes(dailyMoney.getThirdSportsWinTimes());
			
			old.setThirdLotteryBetAmount(dailyMoney.getThirdLotteryBetAmount());
			old.setThirdLotteryWinAmount(dailyMoney.getThirdLotteryWinAmount());
			old.setThirdLotteryBetTimes(dailyMoney.getThirdLotteryBetTimes());
			old.setThirdLotteryWinTimes(dailyMoney.getThirdLotteryWinTimes());
			
			old.setChessBetAmount(dailyMoney.getChessBetAmount());
			old.setChessWinAmount(dailyMoney.getChessWinAmount());
			old.setChessBetTimes(dailyMoney.getChessBetTimes());
			old.setChessWinTimes(dailyMoney.getChessWinTimes());
			
			old.setEsportsBetAmount(dailyMoney.getEsportsBetAmount());
			old.setEsportsWinAmount(dailyMoney.getEsportsWinAmount());
			old.setEsportsBetTimes(dailyMoney.getEsportsBetTimes());
			old.setEsportsWinTimes(dailyMoney.getEsportsWinTimes());
			if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
				//同步数据到es
				dailyMoneyDao.insertOne(old);
				SysAccountDailyMoney newData = new SysAccountDailyMoney();
				BeanDtoVoUtils.beanToBean(old,newData);
				ElasticApi.addQueue(new IndexOperation<>(EsEnum.SYS_ACCOUNT_DAILY_MONEY.getIndexTag(),EsEnum.SYS_ACCOUNT_DAILY_MONEY.getType(),newData));
			}else{
				dailyMoneyDao.insert(old);
			}
		} else {
			dailyMoneyDao.updateThirdAmount(dailyMoney);
			dailyMoney.setId(old.getId());
			if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN) {
				//同步数据到es
				ElasticApi.updateOne(new UpdateOperation<SysAccountDailyMoney>(EsEnum.SYS_ACCOUNT_DAILY_MONEY.getIndexTag(), EsEnum.SYS_ACCOUNT_DAILY_MONEY.getType(), dailyMoney.getId().toString()).setSource(dailyMoney));
			}
		}
		return 0;
	}

	@Override
	public DailyMoney findOneByAccountIdAndStatDate(Long accountId, Date date) {
		return moneyService.findByAccountAndDate(accountId, date);
	}

	@Override
	public Map getLastSucDepositHandlerData(Long userId) {
		Date startTime = mdrDao.getLastSuccessTime(userId);
		Map dailyMap = dailyMoneyDao.getLastSucDepositHandlerData(userId, startTime);
		if (dailyMap != null && startTime != null) {
			dailyMap.put("startTime", DateUtil.toDateStr(startTime));
		}
		if(dailyMap==null){
			dailyMap=new HashMap<>();
		}
		startTime=mdrDao.getLastSuccessCreateTime(userId);
		if(startTime!=null){
			dailyMap.put("startDateTime", DateUtil.toDatetimeStr(startTime));
		}
		return dailyMap;
	}

	@Override
	public List<Map> findByLotteryAndMarkWinTotal(Long stationId, Date statDate, Date endDate) {
		if (stationId == null) {
			return null;
		}
		return dailyMoneyDao.findByLotteryAndMarkWinTotal(stationId, statDate, endDate);
	}

	@Override
	public List<Map> getGlobalReportForAdmin(Date start, Date end) {
		List<Map> list = dailyMoneyDao.getGlobalReportForAdmin(start, end);
		if (list != null && !list.isEmpty()) {
			SysStation station = null;
			Long id = null;
			List<BcLottery> lotList = null;
			String lotNames = null;
			for (Map m : list) {
				if (m.get("stationId") == null) {
					continue;
				}
				id = ((Integer) m.get("stationId")).longValue();
				station = frameService.findOneById(id);
				if (station == null) {
					continue;
				}
				m.put("stationName", station.getName());
				m.put("folder", station.getFloder());
				lotList = lotteryDao.find(id, BusinessConstant.status_normal, BusinessConstant.status_normal, null, null, null);
				if (lotList != null && !lotList.isEmpty()) {
					lotNames="";
					for (BcLottery lot : lotList) {
						if(lot.getCode().equals(LotteryEnum.FFK3.name())){
							lotNames=lotNames+"极速快三，";
						}
						if(lot.getCode().equals(LotteryEnum.WFK3.name())){
							lotNames=lotNames+"幸运快三，";
						}
						if(lot.getCode().equals(LotteryEnum.JPK3.name())){
							lotNames=lotNames+"日本快三，";
						}
						if(lot.getCode().equals(LotteryEnum.KRK3.name())){
							lotNames=lotNames+"韩国快三，";
						}
						if(lot.getCode().equals(LotteryEnum.SFK3.name())){
							lotNames=lotNames+"十分快三，";
						}
						if(lot.getCode().equals(LotteryEnum.ESK3.name())){
							lotNames=lotNames+"二十分快三，";
						}
						if(lot.getCode().equals(LotteryEnum.HKK3.name())){
							lotNames=lotNames+"香港快三，";
						}
						if(lot.getCode().equals(LotteryEnum.AMK3.name())){
							lotNames=lotNames+"澳门快三，";
						}
						if(lot.getCode().equals(LotteryEnum.SFSC.name())){
							lotNames=lotNames+"极速赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.SFFT.name())){
							lotNames=lotNames+"极速飞艇，";
						}
						if(lot.getCode().equals(LotteryEnum.FFSC.name())){
							lotNames=lotNames+"疯狂赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.WFSC.name())){
							lotNames=lotNames+"五分赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.WFFT.name())){
							lotNames=lotNames+"五分飞艇，";
						}
						if(lot.getCode().equals(LotteryEnum.LBJSC.name())){
							lotNames=lotNames+"老北京赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.XSFSC.name())){
							lotNames=lotNames+"新极速赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.XWFSC.name())){
							lotNames=lotNames+"幸运赛车，";
						}
						if(lot.getCode().equals(LotteryEnum.SFLHC.name())){
							lotNames=lotNames+"十分六合彩，";
						}
						if(lot.getCode().equals(LotteryEnum.TMLHC.name())){
							lotNames=lotNames+"三分六合彩，";
						}
						if(lot.getCode().equals(LotteryEnum.WFLHC.name())){
							lotNames=lotNames+"五分六合彩，";
						}
						if(lot.getCode().equals(LotteryEnum.HKMHLHC.name())){
							lotNames=lotNames+"香港马会六合彩，";
						}
						if(lot.getCode().equals(LotteryEnum.SF28.name())){
							lotNames=lotNames+"极速28，";
						}
						if(lot.getCode().equals(LotteryEnum.TMK3.name())){
							lotNames=lotNames+"三分快三，";
						}
					}
					m.put("sysLotName", lotNames);
				}
			}
		}
		Collections.sort(list, new Comparator<Map>() {
			@Override
			public int compare(Map o1, Map o2) {
				String f1 = (String) o1.get("folder");
				String f2 = (String) o2.get("folder");
				if (f1 == null) {
					return -1;
				}
				if (f2 == null) {
					return 1;
				}
				return f1.compareTo(f2);
			}
		});
		return list;
	}

	@Override
	public Map getLotteryAmountCount(Long stationId, String account, Date start,Date end) {
		// TODO Auto-generated method stub
		return dailyMoneyDao.getLotteryAmountCount(account, stationId, start,end);
	}

	@Override
	public void synDailiData(Long stationId, String end, String begin) {
		// TODO Auto-generated method stub
		 dailyMoneyDao.synDailiData(stationId,DateUtil.toDatetime(end+" 23:59:59"),DateUtil.toDatetime(begin+" 00:00:00"));
		 //同步账变记录
		 int i =  moneyRecordDao.snyMoneyRecord(stationId,DateUtil.toDatetime(end+" 23:59:59"),DateUtil.toDatetime(begin+" 00:00:00"));
		 //同步投注记录
		 int y = lotteryOrderDao.snyLotteryOrder(stationId,DateUtil.toDatetime(end+" 23:59:59"),DateUtil.toDatetime(begin+" 00:00:00"));
		 SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：同步站点编号为"+stationDao.get(stationId).getFloder()+"的报表、账变、投注数据", LogType.ADMIN_PLATFORM);
	}

	@Override
	public void fixUpLowRelationship(Long stationId) {
		dailyMoneyDao.fixUpLowRelationship(stationId);
		 SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：修复站点编号为"+stationDao.get(stationId).getFloder()+"的上下级关系", LogType.ADMIN_PLATFORM);

	}

	@Override
	public void editBalanceGemRepost(SysAccountDailyMoney dailyMoney, SysAccount acc) {
		SysAccountDailyMoney old = dailyMoneyDao.findOneByAccountIdAndStatDate(dailyMoney.getAccountId(),
				dailyMoney.getStatDate());
		if (old == null) {
			old = new SysAccountDailyMoney();
			old.setAccountId(dailyMoney.getAccountId());
			old.setStatDate(dailyMoney.getStatDate());
			old.setAccount(acc.getAccount());
			old.setAccountType(acc.getAccountType());
			old.setParents(acc.getParents());
			old.setStationId(acc.getStationId());
			old.setAgentId(acc.getAgentId());
			old.setAgentName(acc.getAgentName());
			old.setReportType(acc.getReportType());
			old.setBalance(moneyDao.getBalance(dailyMoney.getAccountId()));
			old.setBalanceGemAmount(dailyMoney.getBalanceGemAmount());
			dailyMoneyDao.insert(old);
		} else {
			dailyMoneyDao.updateBalaceGemAmount(dailyMoney);
		}
	}

	@Override
	public Map getComprehensiveYearDataMap(Long stationId, Date begin, Date end,Long reportType) {
		Map map = new HashMap<>();
		Calendar now = Calendar.getInstance();
		now.setTime(end);
		initYearReport(map, stationId, now,begin,end,reportType);
		return map;
	}
	
	@Override
	public Page<Map> getAccountPageWithTeamBalance(AccountVo accountVo) {
		if (StringUtil.equals(accountVo.getAgentId(), UserUtil.getUserId())) {
			accountVo.setAgentId(0l);
		}
		if (StationUtil.isAgentStation() && StringUtil.isNotEmpty(accountVo.getParents())) {
			accountVo.setAccountType(null);
		}

		if (StationUtil.isDailiStation() && Validator.isNull(accountVo.getAgentParentId())) {
			accountVo.setSearchSelf(true);
			accountVo.setSelfId(UserUtil.getUserId());
		}
		Long stationId = StationUtil.getStationId();
		String key = getCacheKey(stationId,accountVo.getAccount(),accountVo.getLevel(),accountVo.getReportType(),accountVo.getAccountStatus(),accountVo.getParents());
		
		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);
		if (jsons != null) {
			return JsonUtil.toBean(jsons, Page.class);
		}
		
		Page<Map> page = sysAccountDao.getPage(accountVo);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<Long> ids = new ArrayList<>();
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : page.getList()) {
				if (((Integer) m.get("accountType")) == SysAccount.ACCOUNT_PLATFORM_AGENT) {
					if (m.get("id") != null) {
						if (m.get("id") instanceof Integer) {
							ids.add(new Long((Integer) m.get("id")));
						} else if (m.get("id") instanceof Long) {
							ids.add((Long) m.get("id"));
						}
					}
					
					//查询代理列表的同时把代理线下的团队余额查出
					//拼接parents
					Object parentsObject = m.get("parents");
					String parents = "";
					if(parentsObject==null) {
						parents = ",";
					} else{
						parents = parentsObject.toString();
						if (StringUtil.isEmpty(parents)) {
							parents = ",";
						}
					}
					parents = parents+m.get("id").toString() + ",";
					String account = m.get("account").toString();
					//拼装查询条件
					AccountVo accountVo2 = new AccountVo();
					accountVo2.setParents(parents);
					accountVo2.setAccount(account);
					accountVo2.setStationId(stationId);
					accountVo2.setSearchType(AccountVo.SEARCHTYPE_NEXT_ALL);
					BigDecimal teamSumMoney = mnyMoneyDao.getTeamSumMoney(accountVo2);
					m.put("teamSumMoney", teamSumMoney);
					
				}
				if (m.get("lastLoginIp") != null) {
					String ipArea = ips.getCountry((String) m.get("lastLoginIp"));
					if (ipArea.equals("IANA")) {
						ipArea = "";
					}
					m.put("lastLoginIpAddress", ipArea);
				} else {
					m.put("lastLoginIpAddress", "");
				}
			}
		}
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		return page;
	}
	
	/*
	 * 拼接rediskey
	 */
	public String getCacheKey(Long stationId,String account,Long level,Long reportType,Long accountStatus,String parents){
		StringBuilder sb = new StringBuilder();
		sb.append(stationId);
		sb.append("_team_balance_");
		sb.append("_"+account+"_");
		sb.append("_"+level+"_");
		sb.append("_"+reportType+"_");
		sb.append("_"+accountStatus+"_");
		sb.append("_"+parents+"_");
		
		QueryWebParameter webParam = QueryWebUtils.generateQueryWebParameter(UserUtil.getRequest());
		int pageNo = webParam.getPageNo();
		int pageSize = webParam.getPageSize();
		if (pageNo > 0) {
			sb.append("_pn_").append(pageNo);
		}
		if (pageSize > 0) {
			sb.append("_ps_").append(pageSize);
		}
		
		return sb.toString();
	}

	private String getCacheKeyTwo(Long stationId, String account, String agentName, Date startTime, Date endTime,Long searchType, Long reportType,Integer pageNo, Integer pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(stationId);
		sb.append("_a_").append(account);
		sb.append("_b_").append(agentName);
		if (startTime != null) {
			sb.append("_s_").append(DateUtil.toDateStr(startTime));
		}
		if (endTime != null) {
			sb.append("_e_").append(DateUtil.toDateStr(endTime));
		}
		sb.append("_st_").append(searchType);
		sb.append("rp").append(reportType);
		sb.append("_p_").append(pageNo);
		sb.append("_ps_").append(pageSize);
		return sb.toString();
	}
}
