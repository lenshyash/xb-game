package com.game.service.impl;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.game.core.SystemConfig;
import com.game.elasticsearch.repository.MoneyRecordEsRepository;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MnyMoneyRecordDao;
import com.game.model.MnyMoneyRecord;
import com.game.model.vo.MnyMoneyRecordVo;
import com.game.service.MnyMoneyRecordService;
import com.game.util.StationUtil;

@Repository
public class MnyMoneyRecordServiceImpl implements MnyMoneyRecordService {

	@Autowired
	MnyMoneyRecordDao moneyRecordDao;

	@Autowired
	MoneyRecordEsRepository moneyRecordEsRepository;
	
	@Override
	public Page<Map> getMoneyRecord(MnyMoneyRecordVo moneyRecordVo) {
		moneyRecordVo.setStationId(StationUtil.getStationId());
		Page<Map> page;
//		if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
//			page = moneyRecordEsRepository.getPage(moneyRecordVo);
//			if(page==null || page.getResults()==null){
//				page= moneyRecordDao.getMemMnyRdPage(moneyRecordVo);
//			}else{
//				for(Map map:page.getList()){
//					map.put("type",map.get("moneyType"));
//				}
//			}
//			return page;
//		}
		return moneyRecordDao.getMemMnyRdPage(moneyRecordVo);
	}

	@Override
	public MnyMoneyRecord getById(Long id) {
		if(id == null){
			throw new GenericException("参数不正确!");
		}
		return moneyRecordDao.get(id);
	}
	
	@Override
	public MnyMoneyRecord getByOrderId(String orderId,Long stationId) {
		if(StringUtil.isEmpty(orderId)){
			throw new GenericException("参数不正确!");
		}
		return moneyRecordDao.getByOrderId(orderId,stationId);
	}

	@Override
	public Page<Map> getAccountxiMaPage(MnyMoneyRecordVo moneyVo) {
		return moneyRecordDao.getAccountxiMaPage(moneyVo);
	}

}