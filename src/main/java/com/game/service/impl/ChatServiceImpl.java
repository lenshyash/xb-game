package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.common.onlinepay.yifubao.third.URLUtils;
import com.game.constant.StationConfig;
import com.game.dao.SysAccountDao;
import com.game.dao.SysStationDao;
import com.game.dao.SysStationDomainDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.service.ChatService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.AESUtil;
import com.game.util.AuthTokenUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.ValidateUtil;
import com.game.util.WebUtil;

@Service
public class ChatServiceImpl implements ChatService {

	public static String securitySalt = "lks234#$(gfdg>r4w";

	@Autowired
	private SysStationDomainDao stationDomainDao;

	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	
	@Autowired
	private SysStationDao stationDao;

	@Autowired
	private SysAccountDao sysAccountDao;
	@Autowired
	private SysAccountDailyMoneyService dailyMoneyService;

	private String getRequestToken() {
		String chatOn = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		//获取当前用户适用聊天室token
		String token = getApplicableToken();
		Map<String,String> chatMap = new HashMap<>();
		if (StringUtil.isNotEmpty(token)) {
			chatMap = AuthTokenUtil.authChatToken(token);
		}
		/*
		Long stationId = StationUtil.getStationId();
		String platToken = StationUtil.getDomainPlatToken();
		Map<String,String> chatMap = new HashMap<>();
		if (StringUtil.isEmpty(platToken)) {
			chatMap = AuthTokenUtil.getChatToken();
			String newToken = chatMap.get("newToken");
			if (StringUtil.isEmpty(newToken)) {
				throw new GenericException("获取聊天室token失败");
			}
			stationDomainDao.updateDomainChatToken(stationId, platToken);
			platToken = newToken;
		}
		String[] temp = platToken.split(",");
		if (temp != null && temp.length < 2) {
			throw new GenericException("聊天室token格式错误");
		}*/
		// 组装用户token
		MemberLevelBase level = UserUtil.getMemberLevel();
		MemberLevelBase defaultLevel = MemberLevelUtil.getDefaultLevel();
		Map<String, Object> map = new HashMap<>();
		map.put("token",token);
		map.put("platformId",chatMap.get("platformId"));
		map.put("name", UserUtil.getUserAccount());
		map.put("level", 0);
		 //没有用户等级采用站点默认等级
	     if(level !=null && StringUtil.isNotEmpty(level.getLevelName())){
	    	 map.put("grade", level.getLevelName());
	     }else{
	    	 map.put("grade", defaultLevel.getLevelName());
	     }
		map.put("accountId", UserUtil.getUserId());
		map.put("accountType", UserUtil.getType());
		String json = JSONObject.toJSONString(map);
		return AESUtil.encrypt(json, securitySalt);
	}
	private Map<String,String> getChatInfo() {
		String chatOn = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat);
		if (chatOn == null || !"on".equals(chatOn) || !checkChatOpen()) {
			throw new GenericException("聊天室功能未开启");
		}
		//获取当前用户适用聊天室token
		String token = getApplicableToken();
		//判断token缓存是否过期
		String redisKey = Contants.CHAT_MEMBER_TOKEN +":::"+token;
		String chatMapStr  = RedisAPI.getCache(redisKey);
		Map<String,String> chatMap = new HashMap<>();
		if(StringUtil.isEmpty(chatMapStr)){
			if (StringUtil.isNotEmpty(token)) {
				chatMap = AuthTokenUtil.authChatToken(token);
				RedisAPI.addCache(redisKey, JSONObject.toJSONString(chatMap), Contants.CHAT_MEMBER_TOKEN_TIMEOUT);
			}
		}else{
			chatMap = JSONObject.parseObject(chatMapStr, chatMap.getClass());
		}
		
		// 组装用户token
//		MemberLevelBase level = UserUtil.getMemberLevel()
		SysAccount acc = sysAccountDao.get(UserUtil.getUserId());
		MemberLevelBase level = MemberLevelUtil.getLevel(acc.getLevelGroup());
		MemberLevelBase defaultLevel = MemberLevelUtil.getDefaultLevel();
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		map.put("platformId",chatMap.get("platformId"));
		map.put("name", UserUtil.getUserAccount());
		map.put("level", 0);
		 //没有用户等级采用站点默认等级
	     if(level !=null && StringUtil.isNotEmpty(level.getLevelName())){
	    	 map.put("grade", level.getLevelName());
	     }else{
	    	 map.put("grade", defaultLevel.getLevelName());
	     }
		map.put("accountId", UserUtil.getUserId());
		map.put("accountType", UserUtil.getType());
		String json = JSONObject.toJSONString(map);
		Map<String,String> resultMap = new HashMap<>();
		resultMap.put("token",AESUtil.encrypt(json, securitySalt));
		resultMap.put("domain", chatMap.get("domain"));
		return resultMap;
	}
	@Override
	public String login() {
		Map<String,String> map = getChatInfo();
		String domain = map.get("domain");
		String token = map.get("token");
		if(StringUtil.isEmpty(domain)){
			domain = StationConfigUtil.getSys(StationConfig.sys_chat_url);
		}else{
			domain = "https://"+domain;
		}
		
		List<SysStationDomain> list =  stationDomainDao.getListByStationType(StationUtil.getStationId(), SysStationDomain.TYPE_CHAT);
		if(list!=null && list.size()>0){
			domain = list.get(0).getDomain();
	    }
		String loginUrl =domain+ "?token=" + token;
		if (StationUtil.isMobileStation()) {
			loginUrl = domain+ "/iphoneChat/index.html?token=" + token;
		}
		return loginUrl;
	}

	public static void main(String[] args) {
		String securitySalt = "lks234#$(gfdg>r4w";
		String veriy = "H4+tlJOduF4Io6U3Sr9yJbMAc9yQ44vaOms6lgOIRwpSElZALYM6DGy3AmgqfXBn5Mq9iVyX6drA3+7bbJGJVHPhgi4bcQvA+zLnMRYjo9rP0NFPW/cIbNg4u+fbgOlzzqUQah5ZxqdUj3LiM1zvbmACKfPB8txUluHyirA71YeuVHMmMOFXcT35kdbsanih";
		System.out.println(AESUtil.decrypt(veriy, securitySalt));
	}

	@Override
	public void shareOrder(SysAccount user, final JSONObject obj) {
		if (user == null || obj == null) {
			throw new GenericException("非常请求！");
		}
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
			throw new GenericException("此账号不能使用此功能");
		}
		JSONArray arr = obj.getJSONArray("data");
		if(arr == null) {
			throw new GenericException("非常请求！");
		}
		BigDecimal shareMoney = StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.share_order_money_limit));
		for (int i = 0; i < arr.size(); i++) {
			if(arr.getJSONObject(i).getBigDecimal("money").compareTo(shareMoney) !=1) {
				throw new GenericException("超过分享注单限制金额");
			}
		}
		
		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("params", obj.toJSONString()));
				ps.add(new BasicNameValuePair("token", getRequestToken()));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/user/operation/shareOrder?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
	}

	@Override
	public void shareReport(SysAccount user, final JSONObject obj) {
		if (user == null || obj == null) {
			throw new GenericException("非常请求！");
		}
		
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
			throw new GenericException("此账号不能使用此功能");
		}

		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("params", obj.toJSONString()));
				ps.add(new BasicNameValuePair("token", getRequestToken()));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_chat_url) + "/user/operation/shareReport?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
	}

	@Override
	public Map generateChatRoom(Long id, String platformNo, String domain) {
		Map<String,String> map = new HashMap<>();
		String chatOn = StationConfigUtil.get(id, StationConfig.onoff_mobile_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		SysStation station = stationDao.get(id);
		String url = WebUtil.getDomain(domain);
		map = AuthTokenUtil.getChatToken(station,platformNo,url);
		map.put("superAdmin", map.get("platformId")+"admin");
		String newToken = map.get("platformId")+","+map.get("token");
//		stationDomainDao.updateDomainChatToken(id,newToken);
		map.put("domain", domain+"/admin/agent/index");
		return map;
	}
	/**
	 * 获取用户聊天室 （优先规则 1用户指定聊天室 2域名绑定聊天室 3 站点默认聊天室）
	 * @return
	 */
	@Override
	public String getApplicableToken() {
		Long stationId = StationUtil.getStationId();
		String token = "";
		Map<String,Object> account = sysAccountDao.getAccountById(UserUtil.getUserId(),stationId);
		if(StringUtil.isNotEmpty((String)account.get("chatToken"))){
			token =  account.get("chatToken").toString();
		}
		if(StringUtil.isEmpty((String)account.get("chatToken")) && StringUtil.isNotEmpty(StationUtil.getDomainPlatToken())){
			token =  StationUtil.getDomainPlatToken();
		}
		String defaultToken = StationConfigUtil.get(stationId, StationConfig.basic_info_customer_chatroom);
		if(StringUtil.isEmpty((String)account.get("chatToken"))
				&& StringUtil.isEmpty(StationUtil.getDomainPlatToken())
				&& StringUtil.isNotEmpty(defaultToken)){
			token = defaultToken;
		}	
		if(StringUtil.isEmpty(token)){
			String msg = StationConfigUtil.get(StationConfig.chat_notconfigured);
			if (StringUtil.isNotEmpty(msg)) {
				throw new GenericException(msg);
			}
			throw new GenericException("暂未配置聊天室，请联系客服人员");
		}
		//获取域名对象
		/*StringBuffer requestUrl = UserUtil.getRequest().getRequestURL();
		String domainUrl = WebUtil.getDomain(requestUrl.toString());
		SysStationDomain domain = stationDomainDao.getDomainByFolder(domainUrl, StationUtil.getDomainFolder(), stationId);*/
		return token;
		
	}

	public boolean checkChatOpen(){
		boolean flag=true;
		StringBuffer requestUrl = UserUtil.getRequest().getRequestURL();
		String domainUrl = WebUtil.getDomain(requestUrl.toString());
		SysStationDomain domain = stationDomainDao.getDomainByFolder(domainUrl, StationUtil.getDomainFolder(), StationUtil.getStationId());
		if(domain!=null 
				&& domain.getIsChatSwitch()!=null 
				&& domain.getIsChatSwitch().intValue()==SysStationDomain.STATUS_DISABLE){
			flag = false;
		}
		return flag;
	}
	
}
