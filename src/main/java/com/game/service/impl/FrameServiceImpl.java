package com.game.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.dao.SysStationDao;
import com.game.model.SysStation;
import com.game.service.FrameService;
import com.game.util.StationConfigUtil;

@Repository
public class FrameServiceImpl implements FrameService {

	@Autowired
	private SysStationDao stationDao;

	public SysStation getStation(String domain) {
		SysStation station = CacheUtil.getCache(CacheType.STATION_DOMAIN, domain, SysStation.class);
		if (station != null) {
			return station;
		}
		station = stationDao.getStation(domain);

		// System.out.println("站点缓存过期，读取缓存【"+domain+"】");
		if (station == null) {// 站点不存在 或者已经停用
			return null;
		}
		// 此缓存只有系统重启才能刷新。
		if ("domain".equalsIgnoreCase(StationConfigUtil.get(station.getId(), StationConfig.station_folder_source))) {
			station.setFloder(station.getDomainFolder());
		}
		CacheUtil.addCache(CacheType.STATION_DOMAIN, domain, station);
		return station;
	}

	@Override
	public SysStation findOneById(Long stationId) {
		if (stationId == null) {
			return null;
		}
		SysStation station = CacheUtil.getCache(CacheType.STATION_DOMAIN, "id_" + stationId, SysStation.class);
		if (station != null) {
			return station;
		}
		station = stationDao.findOneById(stationId);
		if (station == null) {// 站点不存在 或者已经停用
			return null;
		}
		CacheUtil.addCache(CacheType.STATION_DOMAIN, "id_" + stationId, station);
		return station;
	}

	@Override
	public List<String> getStationFolderByMulti(Long stationId) {

		List<String> folders = null;
		String key = SysStation.MULTI_FOLDER_KEY+ stationId;
		String json = CacheUtil.getCache(CacheType.STATION_DOMAIN, key);
		try {
			folders = JSON.parseArray(json, String.class);
		} catch (Exception e) {
		}
		if (folders != null && !folders.isEmpty()) {
			return folders;
		}
		List<Map> folderMaps = stationDao.getStationFolderByMulti(stationId);

		if (folderMaps == null || folderMaps.isEmpty()) {
			return null;
		}

		folders = new ArrayList<>();
		for (Map map : folderMaps) {
			folders.add(map.get("folder").toString());
		}

		if (folders.isEmpty()) {// 站点不存在 或者已经停用
			return null;
		}
		CacheUtil.addCache(CacheType.STATION_DOMAIN, key, folders);
		return folders;
	}
}
