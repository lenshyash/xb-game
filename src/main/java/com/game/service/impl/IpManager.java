
package com.game.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.game.util.IPSeeker;

public class IpManager
{

        private static Map<String, String> provinceMap = new HashMap<String, String>();

        static
        {
                provinceMap.put("北京", "北京市");
                provinceMap.put("天津", "天津市");
                provinceMap.put("重庆", "重庆市");
                provinceMap.put("上海", "上海市");
                provinceMap.put("河北", "河北省");
                provinceMap.put("山西", "山西省");
                provinceMap.put("辽宁", "辽宁省");
                provinceMap.put("吉林", "吉林省");
                provinceMap.put("黑龙", "黑龙江省");
                provinceMap.put("江苏", "江苏省");
                provinceMap.put("浙江", "浙江省");
                provinceMap.put("安徽", "安徽省");
                provinceMap.put("福建", "福建省");
                provinceMap.put("江西", "江西省");
                provinceMap.put("山东", "山东省");
                provinceMap.put("河南", "河南省");
                provinceMap.put("湖北", "湖北省");
                provinceMap.put("湖南", "湖南省");
                provinceMap.put("广东", "广东省");
                provinceMap.put("海南", "海南省");
                provinceMap.put("四川", "四川省");
                provinceMap.put("贵州", "贵州省");
                provinceMap.put("云南", "云南省");
                provinceMap.put("陕西", "陕西省");
                provinceMap.put("甘肃", "甘肃省");
                provinceMap.put("青海", "青海省");
                provinceMap.put("台湾", "台湾省");
                provinceMap.put("内蒙", "内蒙古自治区");
                provinceMap.put("广西", "广西壮族自治区");
                provinceMap.put("宁夏", "宁夏回族自治区");
                provinceMap.put("新疆", "新疆维吾尔自治区");
                provinceMap.put("西藏", "西藏自治区");
                provinceMap.put("香港", "香港特别行政区");
                provinceMap.put("澳门", "澳门特别行政区");
        }

        public static String getProvinceByIp(String ip)
        {
                String province = IPSeeker.getInstance().getCountry(ip);
                String pre = province.trim().substring(0, 2);
                if(provinceMap.containsKey(pre))
                {
                        return provinceMap.get(pre);
                }
                return province;
        }

        public static String getCityByIp(String ip)
        {
                return getCityByAreaStr(IPSeeker.getInstance().getCountry(ip));
        }

        public static String getCityByAreaStr(String areaStr)
        {
                String pre = areaStr.trim().substring(0, 2);
                if(provinceMap.containsKey(pre))
                {
                        pre = provinceMap.get(pre);
                }
                areaStr = areaStr.substring(pre.length());
                String city = areaStr;
                for(String suffix : new String[] { "市", "县", "区" })
                {
                        int index = StringUtils.indexOf(areaStr, suffix);
                        if(index > -1)
                        {
                                city = areaStr.substring(0, index);
                                if(!city.equals(""))
                                        break;
                        }
                }
                return city;
        }

        public Map<String, List<String>> analyseIpByProvince(List<String> ips)
        {
                Map<String, List<String>> map = new HashMap<String, List<String>>();
                if(ips == null || ips.size() == 0)
                        return map;
                String province = "";
                List<String> ipList = null;
                for(String ip : ips)
                {
                        province = getProvinceByIp(ip);
                        if(map.containsKey(province))
                        {
                                map.get(province).add(ip);
                        }
                        else
                        {
                                ipList = new ArrayList<String>();
                                ipList.add(ip);
                                map.put(province, ipList);
                        }
                }
                return map;
        }

        /**
         * 判断ip是否属于同一个地方,至少要有2级相同,比如:省+市相同 ,或者市+区相同
         * 
         * @param ip1
         * @param ip2
         * @return
         */
        public static boolean isSamePlace(String ip1, String ip2)
        {
                if(StringUtils.equals(ip1, ip2))
                {
                        return true;
                }
                String c1 = IPSeeker.getInstance().getCountry(ip1).trim();
                String c2 = IPSeeker.getInstance().getCountry(ip2).trim();
                if(StringUtils.equals(c1, c2))
                {
                        return true;
                }
                String province1 = c1.substring(0, 2);
                String province2 = c2.substring(0, 2);
                if(StringUtils.equals(province1, province2))
                {
                        String city1 = getCityByAreaStr(c1);
                        String city2 = getCityByAreaStr(c2);
                        if(StringUtils.equals(city1, city2))
                        {
                                return true;
                        }
                }
                return false;
        }

        public static void main(String[] args)
        {
                System.out.println(getCityByIp("218.6.74.94"));
                // System.out.println(getProvinceByIp("218.78.196.218"));
                // System.out.println(getProvinceByIp("60.30.83.220") + "1080@SOCKS4$6&78,140,687#天津市 网通ADSL");
                // System.out.println(getProvinceByIp("60.191.25.138") + "1080@SOCKS4$6&94,391,938#浙江省 电信");
                // System.out.println(getProvinceByIp("61.150.92.158") + "1080@SOCKS4$6&329,625,1141#陕西省汉中市 电信");
                // System.out.println(getProvinceByIp("202.104.35.116") + "1080@SOCKS4$6&62,172,578#广东省广州市芳村区 ADSL");
                // System.out.println(getProvinceByIp("210.52.15.210") + "1080@SOCKS4$6&47,3047,3266#河北省唐山市 网通");
                // System.out.println(getProvinceByIp("218.5.18.252") + "1080@SOCKS4$6&63,188,500#福建省福州市 Unknown");
                // System.out.println(getProvinceByIp("218.24.146.37") + "1080@SOCKS4$6&15,78,250#辽宁省大连市 ADSL");
                // System.out.println(getProvinceByIp("218.57.11.112") + "80@SOCKS4$6&0,203,547#山东省济南市 Unknown");
                // System.out.println(getProvinceByIp("218.90.161.189") + "1080@SOCKS4$6&63,141,406#江苏省无锡市 ADSL");
                // System.out.println(getProvinceByIp("218.194.80.230") + "1080@SOCKS4$6&94,360,3313#[C]西南民族大学 女生楼");
                // System.out.println(getProvinceByIp("221.2.196.4") + "1080@SOCKS4$6&63,203,500#山东省聊城市 网通");
                // System.out.println(getProvinceByIp("221.12.147.80") + "1080@SOCKS4$6&78,703,1047#浙江省 网通");
                // System.out.println(getProvinceByIp("221.214.221.49") + "1080@SOCKS4$6&109,375,750#山东省烟台市 网通ADSL");
                // System.out.println(getProvinceByIp("222.135.93.61") + "1080@SOCKS4$6&578,671,937#山东省威海市 网通");
                // System.out.println(getProvinceByIp("222.135.178.62") + "1080@SOCKS4$6&46,140,437#山东省 网通");
                // System.out.println(getProvinceByIp("59.56.174.199") + "808@HTTP$6&62,328,625#江西省 电信");
                // System.out.println(getProvinceByIp("59.61.88.193") + "80@HTTP$6&16,16063,16235#江西省 电信");
                // System.out.println(getProvinceByIp("60.30.83.220") + "8080@HTTP$6&78,2890,3640#天津市 网通ADSL");
                // System.out.println(getProvinceByIp("60.190.151.77") + "8088@HTTP$6&94,3500,3891#浙江省 电信");
                // System.out.println(getProvinceByIp("60.190.152.5") + "8080@HTTP$6&2938,3375,3610#浙江省 电信");
                // System.out.println(getProvinceByIp("60.216.45.10") + "8088@HTTP$6&62,3250,3453#山东省 网通");
                // System.out.println(getProvinceByIp("61.53.137.50") + "8080@HTTP$6&171,515,703#河南省开封市 网通");
                // System.out.println(getProvinceByIp("61.139.73.6") + "8080@HTTP$6&313,719,938#四川省成都市 电信");
                // System.out.println(isSamePlace("1.92.255.255", "1.202.255.61"));
                // System.out.println(getProvinceByIp("61.166.68.69") + "80@HTTP$6&0,3484,3672#云南省开远市 电信");
                // System.out.println(getProvinceByIp("61.166.68.71") + "80@HTTP$6&0,3453,3609#云南省开远市 电信");
                // System.out.println(getProvinceByIp("61.166.68.72") + "80@HTTP$6&0,3406,3703#云南省开远市 电信");
                // System.out.println(getProvinceByIp("61.166.155.83") + "808@HTTP$6&313,5453,5578#云南省昆明市 Unknown");
                // System.out.println(getProvinceByIp("140.109.17.180") + "3127@HTTP$6&578,2219,2688#台湾省 Unknown");
                // System.out.println(getProvinceByIp("140.109.17.180") + "3128@HTTP$6&578,1656,2172#台湾省 Unknown");
                // System.out.println(getProvinceByIp("140.113.152.201") + "8080@HTTP$6&297,907,1469#[C]台湾省 新竹市交通大学");
                // System.out.println(getProvinceByIp("163.15.64.8") + "3128@HTTP$6&641,3453,3875#台湾省 Unknown");
                // System.out.println(getProvinceByIp("190.196.15.157") + "80@HTTP$6&0,18500,22093#末知物理地址 Unknown");
                // System.out.println(getProvinceByIp("202.75.62.230") + "3128@HTTP$6&281,765,1094#香港 特别行政区");
                // System.out.println(getProvinceByIp("202.78.227.32") + "8080@HTTP$6&141,9625,9844#香港 特别行政区");
                // System.out.println(getProvinceByIp("202.99.29.27") + "80@HTTP$6&0,29234,29656#北京市 Unknown");
                // System.out.println(getProvinceByIp("202.106.121.134") + "80@HTTP$6&0,3438,3516#北京市 Unknown");
                // System.out.println(getProvinceByIp("202.107.231.157") + "8080@HTTP$6&78,391,1563#浙江省义乌市 教育网");
                // System.out.println(getProvinceByIp("202.206.100.39") + "3128@HTTP$6&46,250,328#[C]河北师范大学 网络中心");
                // System.out.println(getProvinceByIp("203.70.237.16") + "3128@HTTP$6&3218,12578,12750#台湾省 Unknown");
                // System.out.println(getProvinceByIp("203.73.180.16") + "3128@HTTP$6&3453,13031,13484#台湾省 Unknown");
                // System.out.println(getProvinceByIp("203.78.11.73") + "80@HTTP$6&0,8016,8156#台湾省 Unknown");
                // System.out.println(getProvinceByIp("210.37.0.57") + "808@HTTP$6&93,640,3718#[C]海南省 海南师范大学");
                // System.out.println(getProvinceByIp("210.51.14.197") + "80@HTTP$6&0,2047,2125#北京市 网通");
                // System.out.println(getProvinceByIp("210.192.17.116") + "3128@HTTP$6&3390,6296,6906#台湾省 Unknown");
                // System.out.println(getProvinceByIp("211.72.125.19") + "3128@HTTP$6&2391,13282,19860#台湾省 Unknown");
                // System.out.println(getProvinceByIp("211.95.176.6") + "808@HTTP$6&109,453,609#四川省成都市 网通ADSL");
                // System.out.println(getProvinceByIp("211.100.4.71") + "80@HTTP$6&0,6141,6234#北京市 Unknown");
                // System.out.println(getProvinceByIp("211.138.198.6") + "10917@HTTP$6&63,1984,2000#中国 移通");
                // System.out.println(getProvinceByIp("218.3.242.106") + "8080@HTTP$6&63,266,375#江苏省徐州市 Unknown");
                // System.out.println(getProvinceByIp("218.4.65.118") + "8080@HTTP$6&282,1422,1750#江苏省苏州市 (张家港)");
                // System.out.println(getProvinceByIp("218.4.189.194") + "3128@HTTP$6&47,2891,2985#[C]苏州大学 Unknown");
                // System.out.println(getProvinceByIp("218.6.135.137") + "8088@HTTP$6&94,3688,3829#四川省雅安市 电信光纤");
                // System.out.println(getProvinceByIp("218.64.88.30") + "8080@HTTP$6&78,22594,22719#江西省吉安市 Unknown");
                // System.out.println(getProvinceByIp("218.97.194.94") + "80@HTTP$6&0,469,656#黑龙江省大庆市 油田窄带拨号");
                // System.out.println(getProvinceByIp("218.204.106.150") + "8080@HTTP$6&203,437,562#江西省赣州市 移通");
                // System.out.println(getProvinceByIp("218.249.35.4") + "8080@HTTP$6&9062,43140,43375#北京市 Unknown");
                // System.out.println(getProvinceByIp("218.249.83.87") + "8080@HTTP$6&156,375,453#北京市 Unknown");
                // System.out.println(getProvinceByIp("219.132.142.10") + "8080@HTTP$6&62,10578,10687#广东省梅州市丰顺县 美达网吧");
                // System.out.println(getProvinceByIp("220.175.13.252") + "80@HTTP$6&0,797,922#江西省南昌市 电信");
                // System.out.println(getProvinceByIp("220.189.234.194") + "8080@HTTP$6&94,391,516#浙江省 电信");
                // System.out.println(getProvinceByIp("221.2.216.38") + "8080@HTTP$6&47,906,1000#山东省聊城市 网通");
                // System.out.println(getProvinceByIp("221.11.27.110") + "8080@HTTP$6&47,219,437#陕西省西安市 网通");
                // System.out.println(getProvinceByIp("221.203.154.26") + "8080@HTTP$6&9000,11407,11516#辽宁省铁岭市 网通ADSL");
                // System.out.println(getProvinceByIp("221.224.53.84") + "3128@HTTP$6&62,6625,6734#江苏省苏州市 电信");
                // System.out.println(getProvinceByIp("222.29.154.31") + "80@HTTP$6&0,10609,11687#[C]北京大学 Unknown");
                // System.out.println(getProvinceByIp("222.75.165.130") + "8080@HTTP$6&109,422,578#宁夏银川市 电信");
                // System.out.println(getProvinceByIp("222.128.55.205") + "80@HTTP$6&16,31891,32063#北京市 网通");
                // System.out.println(getProvinceByIp("222.135.178.62") + "8080@HTTP$6&46,890,984#山东省 网通");
                // System.out.println(getProvinceByIp("222.168.102.181") + "80@HTTP$6&0,375,531#吉林省 电信");
                // System.out.println(getProvinceByIp("222.184.56.3") + "80@HTTP$6&0,5750,5860#江苏省 电信");
        }
}
