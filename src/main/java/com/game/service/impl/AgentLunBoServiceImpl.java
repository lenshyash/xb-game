package com.game.service.impl;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.platform.AgentLunBoDao;
import com.game.model.platform.AgentLunBo;
import com.game.service.AgentLunBoService;

@Repository
public class AgentLunBoServiceImpl implements AgentLunBoService {
	@Autowired
	private AgentLunBoDao agentLunBoDao;

	@Override
	public Page page(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentLunBoDao.page(stationId);
	}

	@Override
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		if (id == null || id <= 0 || modelStatus == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentLunBoDao.getStationId(id);
		if (!stationId.equals(staId)) {
			throw new GenericException("站点错误！");
		}
		agentLunBoDao.openCloseH(modelStatus, id, stationId);
	}

	@Override
	public void openClosek(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentLunBoDao.getStationId(id);
		if (!stationId.equals(staId)) {
			throw new GenericException("站点错误！");
		}
		agentLunBoDao.openCloseK(status, id, stationId);
	}

	@Override
	public void addSave(AgentLunBo alb) {
		if (alb.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		agentLunBoDao.save(alb);
	}

	@Override
	public void eidtSave(AgentLunBo alb) {
		if (alb.getId() == null || alb.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
//		Long staId = agentLunBoDao.getStationId(alb.getId());
		AgentLunBo albs = agentLunBoDao.get(alb.getId());
		if(albs==null){
			throw new GenericException("参数不正确！");
		}
		if (!alb.getStationId().equals(albs.getStationId())) {
			throw new GenericException("站点错误！");
		}
		albs.setCode(alb.getCode());
		albs.setTitle(alb.getTitle());
		albs.setTitleImg(alb.getTitleImg());
		albs.setTitleUrl(alb.getTitleUrl());
		albs.setUpdateTime(alb.getUpdateTime());
		albs.setOverTime(alb.getOverTime());
		albs.setModelStatus(alb.getModelStatus());
		albs.setPaiXu(alb.getPaiXu());
		albs.setDomainId(alb.getDomainId());
		albs.setFolderCode(alb.getFolderCode());
		agentLunBoDao.update(albs);
	}

	@Override
	public void delete(Long id, Long stationId) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentLunBoDao.delete(id, stationId);
	}

	@Override
	public AgentLunBo getOne(Long id, Long stationId) {
		if (id != null) {
			AgentLunBo a = agentLunBoDao.get(id);
			if (a != null && (stationId == null || a.getStationId().equals(stationId))) {
				return a;
			}
		}
		return null;
	}

}
