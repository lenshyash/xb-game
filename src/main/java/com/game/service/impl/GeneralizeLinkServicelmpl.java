package com.game.service.impl;

import com.alibaba.fastjson.JSONObject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.dao.SysStationDomainDao;
import com.game.dao.platform.GeneralizeLinkDao;
import com.game.model.SysStationDomain;
import com.game.model.platform.MemberGenrtLink;
import com.game.model.vo.AccountVo;
import com.game.service.AgentBaseConfigService;
import com.game.service.GeneralizeLinkService;
import com.game.service.SysAccountService;
import com.game.util.BigDecimalUtil;
import com.game.util.ShortUrlHelper;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

@Repository
public class GeneralizeLinkServicelmpl implements GeneralizeLinkService {
	private Logger logger = Logger.getLogger(GeneralizeLinkServicelmpl.class);

	@Autowired
	private GeneralizeLinkDao generalizeLinkDao;
	@Autowired
	private AgentBaseConfigService agentBaseConfigService;
	@Autowired
	private SysStationDomainDao domainDao;
	@Autowired
	private SysAccountService sysAccountService;

	@Override
	public Page page(Long stationId, HttpServletRequest request, String userAccount, Integer type , String queryLinkKey) {
		if (stationId == null) {
			throw new GenericException("站点ID不能为空!");
		}
		List<SysStationDomain> domainList = domainDao.list(stationId);
		if(domainList == null || domainList.size()<=0){
			return new Page();
		}
		String adminDomain = domainList.get(0).getDomain();
		StringBuffer requestUrl = request.getRequestURL();
		String domain = WebUtil.getDomain(requestUrl.toString());
		boolean isMulti = agentBaseConfigService.onoff_multi_agent(stationId);// 匹配多级代理开关
		Page page = generalizeLinkDao.page(stationId, userAccount, type,queryLinkKey);
		List<MemberGenrtLink> list = page.getList();
		String linkKey = "";
		for (int i = 0; i < list.size(); i++) {
			Map map = (Map) list.get(i);
			linkKey = (String) map.get("linkKey");
			if (isMulti) {
				String url = domain + "/registerMutil/link_" + linkKey + ".do";
				String adminUrl = adminDomain + "/registerMutil/link_" + linkKey + ".do";
				map.put("linkUrl", url);
				map.put("adminUrl", adminUrl);
			} else {
				String url = domain + "/registersAlone.do?init=" + linkKey;
				String adminUrl = adminDomain + "/registersAlone.do?init=" + linkKey;
				map.put("linkUrl", url);
				map.put("adminUrl", adminUrl);
			}
			//判断是否开启合并开关
			if("on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_merge_Rebate_odds_switch))){
				map.put("cpRolling", map.get("dynamicRate"));
			}
		}
		return page;

	}

	@Override
	public void updateStatusById(Long id, Integer status, Long stationId) {
		if (status == null) {
			throw new GenericException("状态不能为空!");
		}

		if (id == null) {
			throw new GenericException("参数不正确!");
		}

		MemberGenrtLink mgl = generalizeLinkDao.get(id);
		if (mgl == null) {
			throw new GenericException("状态更改失败!");
		}
		logger.error("推广链接管理-更新状态"+ JSONObject.toJSONString(mgl)+"stationId:"+stationId);
		if (stationId.intValue() != mgl.getStationId().intValue()) {
			throw new GenericException("站点不匹配!");
		}

		mgl.setStatus(status);
		generalizeLinkDao.save(mgl);

	}

	@Override
	public String getRandCode(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数不正确!");
		}
		String randCode = getRandomString(5);
		MemberGenrtLink mgl = generalizeLinkDao.getByLinkKeyAndStationId(stationId, randCode);
		while (mgl != null) {
			randCode = getRandomString(5);
			mgl = generalizeLinkDao.getByLinkKeyAndStationId(stationId, randCode);
		}
		return randCode;
	}

	/**
	 * length表示生成字符串的长度
	 * 
	 * @param length
	 * @return
	 */
	public static String getRandomString(int length) {
//		String base = "abcdefghijklmnopqrstuvwxyz";
		String base = "0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}

	/**
	 * 保存或更新
	 */
	@Override
	public void updateOrSave(MemberGenrtLink mgl) {
		if (mgl == null) {
			throw new GenericException("参数不正确!");
		}
		if(mgl.getType().intValue() == 2 && "on".equals(StationConfigUtil.get(StationConfig.onoff_register_account_trun_agent))) {
			throw new GenericException("此版本不支持会员推广连接");
		}
		boolean isMulti = "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_multi_agent));
		//打开多级开关且是代理
		if(mgl.getType()==1 && isMulti){
			//判断返点数合法性
			AccountVo avo = new AccountVo();
			avo.setId(UserUtil.getUserId());
			avo.setGeneralize(true);
			Map map = sysAccountService.getAgentMultiData(avo);
			if(map != null){
				BigDecimal max = BigDecimalUtil.toBigDecimal(map.get("rebateMaxNum").toString());
				BigDecimal min = BigDecimalUtil.toBigDecimal(map.get("rebateMinNum").toString());
				BigDecimal uRoll = mgl.getCpRolling();
				if(uRoll == null){
					uRoll = BigDecimal.ZERO;
					//当开启返点赔率合并开关，设置返点数为最小差额值
					if("on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_agent_merge_Rebate_odds_switch))){
						BigDecimal rebateStep = StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.agent_rebate_step));
						mgl.setCpRolling(rebateStep);
					}
					
				}else{
					if(uRoll.compareTo(min) < 0){
						throw new GenericException("设置的返点数不能小于最小返点数!");
					}
					if(uRoll.compareTo(max) > 0){
						throw new GenericException("设置的返点数不能大于最大返点数!");
					}
				}
				
				
				if("on".equals(StationConfigUtil.get(StationConfig.onoff_profit_share))) {
					BigDecimal maxPS = BigDecimalUtil.toBigDecimal(map.get("profitShareMax").toString());
					BigDecimal minPS = BigDecimalUtil.toBigDecimal(map.get("profitShareMin").toString());
					BigDecimal profitShare = mgl.getProfitShare();
					if(profitShare == null){
						profitShare = BigDecimal.ZERO;
					}
					if(profitShare.compareTo(minPS) < 0){
						throw new GenericException("设置的占成数不能小于最小占成数!");
					}
					if(profitShare.compareTo(maxPS) > 0){
						throw new GenericException("设置的占成数不能大于最大占成数!");
					}
				}
				if("on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random))) {
					BigDecimal maxDR = BigDecimalUtil.toBigDecimal(map.get("dynamicRateMax").toString());
					BigDecimal minDR = BigDecimalUtil.toBigDecimal(map.get("dynamicRateMin").toString());
					BigDecimal dynamicRate = mgl.getDynamicRate();
					if(dynamicRate == null){
						dynamicRate = BigDecimal.ZERO;
					}
					if(dynamicRate.compareTo(minDR) < 0){
						throw new GenericException("设置的赔率点不能小于最小赔率点!");
					}
					if(dynamicRate.compareTo(maxDR) > 0){
						throw new GenericException("设置的赔率点不能大于最大赔率点!");
					}
				}
			}
		}
		
		String linkCode = StringUtil.trim2Empty(mgl.getLinkKey());
		Long stationId = StationUtil.getStationId();
		Long generalizeType = mgl.getGeneralizeType();
		if(generalizeType != null && StringUtil.equals(generalizeType, MemberGenrtLink.GENERALIZE_TYPE_RANDOM)) {
			linkCode = getRandCode(stationId);
			mgl.setLinkKey(linkCode);
		}else if(generalizeType != null && StringUtil.equals(generalizeType, MemberGenrtLink.GENERALIZE_TYPE_INPUT)) {
			if(StringUtil.isEmpty(linkCode) || linkCode.length() !=5) {
				throw new GenericException("推广码:"+linkCode+",格式错误，只能使用5位数字");
			}
			
			if(generalizeLinkDao.getByLinkKeyAndStationId(stationId, StringUtil.trim2Empty(linkCode)) != null) {
				throw new GenericException("推广码:"+linkCode+",已经被其他代理使用");
			}
		}
		String domain = WebUtil.getDomain(SysUtil.getRequest().getRequestURL().toString());
		String url = "";
		if (isMulti) {
			url = "http://" + domain + "/registerMutil/link_" + linkCode + ".do";
		} else {
			url = "http://" + domain + "/registersAlone.do?init=" + linkCode;
		}
		mgl.setLinkUrl(url);
		mgl.setStationId(stationId);
		String account = UserUtil.getUserAccount();
		Long userId = UserUtil.getUserId();
		mgl.setUserAccount(account);
		mgl.setUserId(userId);
		if (StringUtils.isNotEmpty(mgl.getLinkUrl())) {
			if("on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_promlink_wechat_anti_sealing))){
				mgl.setShortUrl1(ShortUrlHelper.getAntiSealink(mgl.getLinkUrl()));
			}else{
				mgl.setShortUrl1(ShortUrlHelper.getShortUrlSina2(mgl.getLinkUrl()));
				//判断是否做微信防封处理
//				mgl.setShortUrl2(ShortUrlHelper.getShortUrl4(mgl.getLinkUrl()));
			}
			
		}
		//用户默认链接只有一条
		if(StringUtil.equals(mgl.getDefaultLink(), MemberGenrtLink.DEFAULT_LINK)) {
			generalizeLinkDao.updateNullDefaultLink(stationId, userId,mgl.getType());
		}
		generalizeLinkDao.save(mgl);
		
	}

	@Override
	public void del(Long id, Long stationId) {
		if (id == null) {
			throw new GenericException("参数不正确!");
		}
		if (stationId == null) {
			throw new GenericException("站点ID不能为空!");
		}

		MemberGenrtLink mgl = generalizeLinkDao.get(id);
		if (mgl == null) {
			throw new GenericException("无此记录!");
		}
		if (!mgl.getStationId().equals(stationId)) {
			throw new GenericException("站点ID不匹配!");
		}
		generalizeLinkDao.delete(id);
	}

	@Override
	public MemberGenrtLink getByStationIdAndLinkCode(Long stationId, String linkCode) {

		if (stationId == null) {
			return null;
		}

		if (StringUtils.isEmpty(linkCode)) {
			return null;
		}

		return generalizeLinkDao.getByLinkKeyAndStationId(stationId, linkCode);
	}

	@Override
	public List<MemberGenrtLink> getDefaultLink(Long stationId, Long accountId, Long defaultLink) {
		
		ArrayList<MemberGenrtLink> arrayList = new ArrayList<MemberGenrtLink>();
		//获取默认链接 
		List<MemberGenrtLink> list = generalizeLinkDao.getListLink(stationId, accountId, MemberGenrtLink.DEFAULT_LINK);
		if(!list.isEmpty()) {
			arrayList.addAll(list);
			return arrayList;
		}
		// 没有默认链接 那么取用户最新链接返回
		List<MemberGenrtLink> noDefaultlist = generalizeLinkDao.getListLink(stationId, accountId, null);
		if(!noDefaultlist.isEmpty()) {
			MemberGenrtLink link = noDefaultlist.get(0);
			arrayList.add(link);
			return arrayList;
		}
		//什么都没有返回空
		return arrayList;
	}
	
	@Override
	public void updateDefaultById(Long id, Long DefaultLink, Long stationId) {
		if (DefaultLink == null) {
			throw new GenericException("状态不能为空!");
		}

		if (id == null) {
			throw new GenericException("参数不正确!");
		}

		MemberGenrtLink mgl = generalizeLinkDao.get(id);
		if (mgl == null) {
			throw new GenericException("状态更改失败!");
		}

		if (stationId != mgl.getStationId()) {
			throw new GenericException("站点不匹配!");
		}

		Long userId = UserUtil.getUserId();
		generalizeLinkDao.updateNullDefaultLink(stationId, userId,mgl.getType());
		mgl.setDefaultLink(DefaultLink);
		generalizeLinkDao.save(mgl);

	}

}
