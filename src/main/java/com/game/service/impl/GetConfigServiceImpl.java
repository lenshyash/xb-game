package com.game.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.game.model.SysMessage;
import com.game.model.SysUserMessage;
import com.game.util.*;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.exception.ParameterException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryDao;
import com.game.dao.platform.AgentActivityDao;
import com.game.dao.platform.AgentArticleDao;
import com.game.dao.platform.AgentLunBoDao;
import com.game.dao.platform.AgentWinDataDao;
import com.game.model.lottery.BcLottery;
import com.game.model.platform.AgentActivity;
import com.game.model.platform.AgentArticle;
import com.game.model.platform.AgentLunBo;
import com.game.model.platform.AgentWinData;
import com.game.service.GetConfigService;

@Repository
public class GetConfigServiceImpl implements GetConfigService {

	@Autowired
	private AgentArticleDao agentArticleDao;
	@Autowired
	private AgentActivityDao agentActivityDao;
	@Autowired
	private AgentLunBoDao agentLunBoDao;
	@Autowired
	private AgentWinDataDao agentWinDataDao;
	@Autowired
	private BcLotteryDao lotteryDao;

	/**
	 * 公告
	 * 
	 */
	@Override
	public List<AgentArticle> getArticle(Integer code, Long stationId) {
		if (Validator.isNull(code) || Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Date overTime = new Date();
		return agentArticleDao.getArticle(code, stationId, overTime);
	}

	/**
	 * 活动
	 */
	@Override
	public List<AgentActivity> getPreferential(Long stationId) {
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Date overTime = new Date();
		return agentActivityDao.getActivity(stationId, overTime);
	}

	@Override
	public int getUnreadPreferential(Long stationId) {
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Date overTime = new Date();
		List<AgentActivity> unreadItems = agentActivityDao.getUnreadActivity(stationId, overTime);
		if (unreadItems != null && !unreadItems.isEmpty()) {
			return unreadItems.size();
		}
		return 0;
	}

	@Override
	public void readActivity(Long id) {
		agentActivityDao.readActive(id);
	}
	
	/**
	 * 优惠活动列表页
	 */
	public List<Map<String, Object>> getActivityImg(Long stationId){
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Date overTime = new Date();
		return agentActivityDao.getActivityImg(stationId, overTime);
	}
	
	public AgentActivity getActivityById(Long id){
		if (Validator.isNull(id)) {
			throw new ParameterException("参数不正确！");
		}
		return agentActivityDao.get(id);
	}

	/**
	 * 轮播图
	 */
	@Override
	public List<AgentLunBo> getLunBo(Long stationId,Integer... code) {
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		if(code == null){
			code = new Integer[]{1};
		}
		Date overTime = new Date();
		return agentLunBoDao.getLunBo(stationId, overTime,code);
	}
	
	/**
	 * 中奖数据
	 */
	public List<AgentWinData> getWinData(Long stationId){
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		return agentWinDataDao.getWinData(stationId);
	}

	/**
	 * 彩票种类
	 */
	@Override
	public List<BcLottery> getLotList(Long stationId) {
		if (Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Integer version = LotteryVersionUtils.resultVersion(stationId);
		return lotteryDao.find(stationId, BcLottery.STATUS_ENABLE, BcLottery.STATUS_ENABLE, null, null, version);
	}
	/**
	 * 获取公告列表
	 */
	@Override
	public Page getArticleList(Integer code,Long stationId) {
		// TODO Auto-generated method stub
		return agentArticleDao.listByPage(stationId,code);
	}
	/**
	 * 获取公告
	 */
	public AgentArticle getArticleById(Long id){
		if (Validator.isNull(id)) {
			throw new ParameterException("参数不正确！");
		}
		return agentArticleDao.get(id);
	}

	@Override
	public List<AgentArticle> getArticle(Integer[] code, Long stationId) {
		if (Validator.isNull(code) || Validator.isNull(stationId)) {
			throw new GenericException("数据异常！");
		}
		Date overTime = new Date();
		return agentArticleDao.getArticle(code, stationId, overTime);
	}
}
