package com.game.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.game.cache.redis.RedisAPI;
import com.game.dao.MemberRedPacketLevelDao;
import com.game.model.MemberRedPacketLevel;
import com.game.service.MemberRedPacketLevelService;

@Service
public class MemberRedPacketLevelServiceImpl implements MemberRedPacketLevelService {
	@Autowired
	private MemberRedPacketLevelDao redPacketLevelDao;
	private static final int MEMBER_RED_PACKET_REDIS_DB = 8;

	@Override
	public void batchInsert(List<MemberRedPacketLevel> llist, Set<Long> groupLevelSet) {
		if (llist == null || llist.isEmpty()) {
			return;
		}
		redPacketLevelDao.batchInsert(llist);
		RedisAPI.addCache("red_packet_member_level_" + llist.get(0).getPacketId(), JSON.toJSONString(groupLevelSet), 0,
				MEMBER_RED_PACKET_REDIS_DB);
	}

	@Override
	public void deleteByRedPacketId(Long redPacketId) {
		redPacketLevelDao.deleteByRedPacketId(redPacketId);
		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "red_packet_member_level_" + redPacketId);
	}

	@Override
	public Set<Long> findLevelsByRedPacketId(Long redPacketId) {
		if (redPacketId == null)
			return null;
		String json = RedisAPI.getCache("red_packet_member_level_" + redPacketId, MEMBER_RED_PACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return new HashSet<>(JSON.parseArray(json, Long.class));
		}
		List<Long> list = redPacketLevelDao.findByRedPacketId(redPacketId);
		Set<Long> set = null;
		if (list != null) {
			RedisAPI.addCache("red_packet_member_level_" + redPacketId, JSON.toJSONString(list), 0,
					MEMBER_RED_PACKET_REDIS_DB);
			set = new HashSet<>(list);
		}
		return set;
	}
}
