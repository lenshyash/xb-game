package com.game.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.CreatePartitionDao;
import com.game.service.CreatePartitionService;

@Service
public class CreatePartitionServiceImpl implements CreatePartitionService {
	@Autowired
	private CreatePartitionDao createPartitionDao;

	@Override
	public void createLotteryPartition(Date date, String type) {
		switch (type) {
			case "data":
				createPartitionDao.createLotteryDataPartition(date);
				break;
			case "order":
				createPartitionDao.createLotteryOrderPartition(date);
				break;
		}
	}

//	@Override
//	public void createMemberRollBackRecordPartition(Date date) {
//		createPartitionDao.createMemberRollBackRecordPartition(date);
//	}

	@Override
	public void createProxyMultiRebateRecordPartition(Date date) {
		createPartitionDao.createProxyMultiRebateRecordPartition(date);
	}
	
	@Override
	public void createMemberBackwaterRecordPartition(Date date) {
		createPartitionDao.createMemberBackwaterRecordPartition(date);
	}
	
	@Override
	public void createBetNumRecordPartition(Date date) {
		createPartitionDao.createBetNumRecordPartition(date);	
	}
}
