package com.game.service.impl;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.dao.AgentWhiteIpDao;
import com.game.model.AgentWhiteIp;
import com.game.service.AgentWhiteIpService;
import com.game.util.StationUtil;

@Repository
public class AgentWhiteIpServiceImpl implements AgentWhiteIpService{
	
	@Autowired
	private AgentWhiteIpDao ipDao;
	
	@Override
	public void save(AgentWhiteIp agentWhiteIp) {
		if(ipDao.isNotUnique(agentWhiteIp, "ip,stationId")){
			throw new GenericException("IP [" + agentWhiteIp.getIp() + "]已经存在");
		}
		ipDao.save(agentWhiteIp);
		CacheUtil.delCache(CacheType.STATION_WHITE_IP_LIST, StationUtil.getStationId()+"");
	}

	@Override
	public void deleteIp(long ipId) {
		checkStationPermission(ipId);
		ipDao.delete(ipId);
		CacheUtil.delCache(CacheType.STATION_WHITE_IP_LIST, StationUtil.getStationId()+"");
	}

	@Override
	public void updateStatus(long ipId, long status) {
		checkStationPermission(ipId);
		AgentWhiteIp ip = new AgentWhiteIp();
		ip.setId(ipId);
		ip.setStatus(status);
		ipDao.updateNotNull(ip);
		CacheUtil.delCache(CacheType.STATION_WHITE_IP_LIST, StationUtil.getStationId()+"");
	}

	@Override
	public Page queryPage() {
		return ipDao.queryPage(StationUtil.getStationId());
	}
	
	private AgentWhiteIp checkStationPermission(long ipId){
		AgentWhiteIp ip = ipDao.get(ipId);
		if(!StationUtil.isAgentStation()){
			return ip;
		}
		if(ip == null){
			throw new GenericException("记录不存在");
		}
		if(!StringUtil.equals(ip.getStationId(), StationUtil.getStationId())){
			throw new GenericException("非法操作");
		}
		return ip;
	}
}
