package com.game.service.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MnyScoreRecordDao;
import com.game.dao.SysAccountDao;
import com.game.model.MnyScoreRecord;
import com.game.model.SysAccount;
import com.game.model.dictionary.ScoreRecordType;
import com.game.model.vo.MnyScoreReocrdVo;
import com.game.model.vo.MnyScoreVo;
import com.game.service.MnyScoreService;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class MnyScoreServiceImpl implements MnyScoreService {

	@Autowired
	SysAccountDao sysAccountDao;

	@Autowired
	MnyScoreRecordDao scoreRecordDao;

	private BigDecimal updateScore(MnyScoreVo scoreVo) {
		Long accountId = scoreVo.getAccountId();
		SysAccount checkAccount = sysAccountDao.getFromCache(accountId);

		if (checkAccount == null) {
			throw new GenericException("用户不存在！");
		}
		scoreVo.setStationId(checkAccount.getStationId());
		scoreVo.setAccount(checkAccount.getAccount());

		BigDecimal score = scoreVo.getScore();
		BigDecimal lastScore = null;
		if (score == null || score.compareTo(BigDecimal.ZERO) < 1) {
			return score;//积分设置为0也要可以抽奖
		}

		if (!scoreVo.getScoreRecordType().isAdd()) {
			score = score.negate();
			scoreVo.setScore(score);
		}
		lastScore = score;

		BigDecimal[] results = null;
		try {
			results = scoreRecordDao.updateScore(accountId, lastScore);
		} catch (SQLException sqle) {
			throw new GenericException("操作失败！", sqle);
		}

		if (results == null || results.length != 2) {
			throw new GenericException("未知异常");
		}
		if (results[0] != null && results[0].intValue() == 0) {
			throw new GenericException("积分不足");
		}
		BigDecimal beforeScore = results[1];
		if (beforeScore == null) {
			throw new GenericException("积分记录不存在");
		}

		return beforeScore;
	}

	@Override
	public MnyScoreRecord updScoreAndRecord(MnyScoreVo moneyVo) {
		BigDecimal beforeScore = updateScore(moneyVo);
		BigDecimal afterScore = beforeScore.add(moneyVo.getScore());
		MnyScoreRecord record = new MnyScoreRecord();
		record.setAccountId(moneyVo.getAccountId());
		record.setAccount(moneyVo.getAccount());
		record.setScore(moneyVo.getScore());
		record.setType(moneyVo.getScoreRecordType().getType());
		record.setBeforeScore(beforeScore);
		record.setAfterScore(afterScore);
		record.setStationId(moneyVo.getStationId());
		record.setCreateDatetime(new Date());
		record.setRemark(moneyVo.getRemark());
		if (UserUtil.getCurrentUser() != null && UserUtil.getCurrentUser() instanceof SysAccount) {
			SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
			acc.setScore(afterScore);
		}
		return scoreRecordDao.save(record);
	}

	@Override
	public Page getScoreReocrd(MnyScoreReocrdVo msvo) {
		return scoreRecordDao.getScoreRecordPage(msvo);
	}

	@Override
	public void operatScore(Integer type, String accountName, Integer score, String remark) {
	SysAccount account = sysAccountDao.findOneByAccountAndStationId(accountName, StationUtil.getStationId());
	if(account==null){
		throw new GenericException("账号不存在");
	}
		MnyScoreVo scoreVo = new MnyScoreVo();
		scoreVo.setAccountId(account.getId());
		scoreVo.setScore(new BigDecimal(score));
		if(type==2){
			scoreVo.setScoreRecordType(ScoreRecordType.ADD_ARTIFICIAL);
		}else{
			scoreVo.setScoreRecordType(ScoreRecordType.SUB_ARTIFICIAL);
		}
		scoreVo.setRemark( remark);
		updScoreAndRecord(scoreVo);
	}
}