package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLEngineResult.Status;

import com.game.model.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.DataReader;
import com.game.cache.redis.RedisAPI;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.MemberDrawInfoDao;
import com.game.dao.MemberLevelBaseDao;
import com.game.dao.MnyComRecordDao;
import com.game.dao.MnyDrawRecordDao;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.dao.third.RealGameTransLogDao;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.third.RealGameTransLog;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MnyDrawRecordVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.permission.PermissionManager;
import com.game.service.MnyDrawRecordService;
import com.game.service.MnyMoneyService;
import com.game.service.SysMessageService;
import com.game.service.pay.AgentPaymentService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.MD5Util;
import com.game.util.PoiUtil;
import com.game.util.SecurityCheckUtil;
import com.game.util.Snowflake;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Repository
public class MnyDrawRecordServiceImpl implements MnyDrawRecordService {

	@Autowired
	SysAccountDao sysAccountDao;

	@Autowired
	SysAccountInfoDao accountInfoDao;

	@Autowired
	MnyDrawRecordDao drawRecordDao;

	@Autowired
	MnyMoneyService moneyService;

	@Autowired
	MnyMoneyRecordDao moneyRecordDao;
	
	@Autowired
	private AgentPaymentService paymentService;

	@Autowired
	MnyComRecordDao comRecordDao;
	@Autowired
	private MemberLevelBaseDao memberLevelBaseDao;
	@Autowired
	private RealGameTransLogDao realGameTransLogDao;
	
	@Autowired
	private SysMessageService sysMessageService;

	@Autowired
	private MemberDrawInfoDao memberDrawInfoDao;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private com.game.dao.MnyMoneyDao MnyMoneyDao;
	
	@Override
	public Page getPage(MnyDrawRecordVo mdrvo) {
		
		if (StringUtil.isNotEmpty(mdrvo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(mdrvo.getStationId());
			avo.setAccount(mdrvo.getAgentName());
			SysAccount agent = sysAccountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			mdrvo.setParents(parents + agent.getId() + ",");
			mdrvo.setSearchSelf(true);
			mdrvo.setSelfId(agent.getId());
		}

		Long status = mdrvo.getStatus();
		if (Validator.isNotNull(status)) {
			if (StringUtil.equals(status, MnyDrawRecord.STATUS_UNTREATED)) {
				mdrvo.setLockFlag(MnyDrawRecord.LOCK_FLAG_LOCKED);
			} else if (StringUtil.equals(status, 4l)) {
				mdrvo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
				mdrvo.setLockFlag(MnyDrawRecord.LOCK_FLAG_UNLOCKED);
			}
		}
		Page<Map> page = drawRecordDao.getPage(mdrvo);
		if (page != null && page.getList() != null) {
			Integer levelGroup = null;
			Map<Integer, MemberLevelBase> map = new HashMap<>();
			MemberLevelBase mlb = null;
			for (Map m : page.getList()) {
				if (m.containsKey("levelGroup")) {
					levelGroup = (Integer) m.get("levelGroup");
					mlb = map.get(levelGroup);
					if (mlb == null) {
						mlb = memberLevelBaseDao.get(levelGroup);
						map.put(levelGroup, mlb);
					}
					if (mlb != null) {
						m.put("levelName", mlb.getLevelName());
					}
				}
			}
		}
		return page;
	}
	@Override
	public Page getPage2(MnyDrawRecordVo mdrvo) {
		
		if (StringUtil.isNotEmpty(mdrvo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(mdrvo.getStationId());
			avo.setAccount(mdrvo.getAgentName());
			SysAccount agent = sysAccountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			mdrvo.setParents(parents + agent.getId() + ",");
			mdrvo.setSearchSelf(true);
			mdrvo.setSelfId(agent.getId());
		}

		Long status = mdrvo.getStatus();
		if (Validator.isNotNull(status)) {
			if (StringUtil.equals(status, MnyDrawRecord.STATUS_UNTREATED)) {
				mdrvo.setLockFlag(MnyDrawRecord.LOCK_FLAG_LOCKED);
			} else if (StringUtil.equals(status, 4l)) {
				mdrvo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
				mdrvo.setLockFlag(MnyDrawRecord.LOCK_FLAG_UNLOCKED);
			}
		}
		Page<Map> page = drawRecordDao.getPage(mdrvo);
		if (page != null && page.getList() != null) {
			Integer levelGroup = null;
			Map<Integer, MemberLevelBase> map = new HashMap<>();
			MemberLevelBase mlb = null;
			for (Map m : page.getList()) {
				if (m.containsKey("levelGroup")) {
					levelGroup = (Integer) m.get("levelGroup");
					mlb = map.get(levelGroup);
					if (mlb == null) {
						mlb = memberLevelBaseDao.get(levelGroup);
						map.put(levelGroup, mlb);
					}
					if (mlb != null) {
						m.put("levelName", mlb.getLevelName());
					}
				}
			}
		}
		return page;
	}
	@Override
	public void withdraw(MnyMoneyVo moneyVo) {
		BigDecimal money = moneyVo.getMoney();
		if (money == null || money.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("提款金额格式错误！");
		}
		String max = StationConfigUtil.get(StationConfig.withdraw_max_money);
		if (StringUtil.toBigDecimal(max).compareTo(money) < 0) {
			throw new GenericException("提款金额不能大于最高金额" + max);
		}
		String min = StationConfigUtil.get(StationConfig.withdraw_min_money);
		if (StringUtil.toBigDecimal(min).compareTo(money) > 0) {
			throw new GenericException("提款金额不能小于最低金额" + min);
		}
		SysAccount member = sysAccountDao.getFromCache(moneyVo.getAccountId());
		//引导账号判断是否开启禁止提款开关
		String isGuideEWOnOff = StationConfigUtil.get(moneyVo.getStationId(),StationConfig.onoff_embark_withdraw_guide);
		//报表类型账号是否开启禁止提款开关
		String isReportTypeDraw = StationConfigUtil.get(moneyVo.getStationId(),StationConfig.onoff_withdraw_report_type_guide);
		if (member == null 
				|| member.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_TEST_GUEST
				|| (!"off".equals(isGuideEWOnOff)&&member.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_GUIDE )
				|| ("on".equals(isReportTypeDraw) && member.getReportType().longValue() == SysAccount.REPORT_TYPE_GUIDE )) {
			throw new GenericException("暂无提款权限！");
		}
		
		Long stationId = StationUtil.getStationId();
		
		validWithdrawInterval(moneyVo.getAccountId(), member.getStationId());

		MnyDrawRecord record = new MnyDrawRecord();
		long status = MnyDrawRecord.STATUS_UNTREATED;

		String cacheKey = null;
		// 判断入口
		if (StationUtil.isAgentStation()) {
			status = MnyDrawRecord.STATUS_SUCCESS;
		} else {
			// 会员端判断提款密码
			String repPwd = moneyVo.getReceiptPwd();
			if (StringUtil.isEmpty(repPwd)) {
				throw new GenericException("提款密码为空！");
			}
			// 判断余额
			moneyService.checkMoney(moneyVo.getAccountId(), money);

			SysAccountInfo memberInfo = accountInfoDao.get(moneyVo.getAccountId());
			String receiptPwd = memberInfo.getReceiptPwd();
			if (StringUtil.isEmpty(receiptPwd)) {
				throw new GenericException("用户未设置取款密码！");
			}
			repPwd = MD5Util.getMD5String(moneyVo.getAccount(), repPwd);
			if (!repPwd.equals(receiptPwd)) {
				throw new GenericException("提款密码错误！");
			}

			// 判断当天提款次数

			// 站点一天可提款次数
			int wnum = NumberUtils.toInt(StationConfigUtil.get(StationConfig.withdraw_time_one_day));
			if (wnum > 0) {
				int curWnum = getCurrentDateMemDrawNum(moneyVo.getAccountId(), StationUtil.getStationId());
				if (curWnum >= wnum) {
					throw new GenericException("今日已提款" + curWnum + "次，不能再提款");
				}
			}

			String consumeRate = StationConfigUtil.get(StationConfig.consume_rate);

			String startTime = StationConfigUtil.get(StationConfig.money_start_time);
			String endTime = StationConfigUtil.get(StationConfig.money_end_time);
			String sj = StationConfigUtil.validateWithdrawalTime(startTime, endTime);
			if (!StringUtils.equals("是", sj)) {
				throw new GenericException(sj);
			}

			// 代理不用判断打码量 没设置消费比例或者消费比例为0则可以提款
			if (StringUtil.isNotEmpty(consumeRate) && !"0".equals(consumeRate)) {
				SysAccount drawer = sysAccountDao.get(moneyVo.getAccountId());
				if (drawer == null) {
					throw new GenericException("非法请求！");
				}
				BigDecimal betNum = StringUtil.toBigDecimal(drawer.getBetNum());
				BigDecimal checkBetNum = StringUtil.toBigDecimal(drawer.getDrawNeed());
				
				//引导账号提款不判断打码量
				Boolean onoff = "on".equals(StationConfigUtil.get(stationId, StationConfig.guide_account_withdraw_not_need_betnum));
				Boolean guestAccount = UserUtil.isGuestAccount();
				
				if(onoff && guestAccount) {
					//必须是引导且开关开着
				}else {
					if (betNum.compareTo(checkBetNum) == -1) {
						throw new GenericException("提款申请失败，投注量未达标！");
					}
				}
			}
			cacheKey = "aid_" + moneyVo.getAccountId() + "_sid_" + stationId + "_d_" + DateUtil.toDateStr(new Date());
			int drawNum = RedisAPI.incrby(cacheKey, 1).intValue();
			if (wnum > 0 && drawNum > wnum) {
				RedisAPI.incrby(cacheKey, -1);
				throw new GenericException("今日已提款" + (drawNum - 1) + "次，不能再提款");
			}
			record.setBankName(memberInfo.getBankName());
			record.setUserName(memberInfo.getUserName());
			record.setCardNo(memberInfo.getCardNo());
		}

		record.setTrueMoney(moneyVo.getMoney());
		record.setFee(new BigDecimal(0));
		record.setType(moneyVo.getMoneyRecordType().getType());
		record.setMemberId(moneyVo.getAccountId());
		record.setDrawMoney(moneyVo.getMoney());
		long orderNo = Snowflake.getOrderNo();
		record.setOrderNo(orderNo + "");
		record.setStauts(status);
		record.setStationId(stationId);
		record.setLockFlag(MnyDrawRecord.LOCK_FLAG_UNLOCKED);
		record.setAccount(moneyVo.getAccount());
		moneyVo.setOrderId(record.getOrderNo());

		try {
			MnyMoneyRecord rd = moneyService.updMnyAndRecord(moneyVo);
			record.setMoneyRecordId(rd.getId());
			drawRecordDao.save(record);
			addCountOfUntreated(stationId);
		} catch (Exception e) {
			// 申请时，余额不足的时候。 当天提款总数应该 -1
			if (cacheKey != null) {
				RedisAPI.incrby(cacheKey, -1);
			}
			throw e;
		}
		
		//引导会员直接成功
		boolean autoonoff = "on".equals(StationConfigUtil.get(stationId, StationConfig.guide_account_withdraw_auto_onoff));
		
		if(UserUtil.isGuestAccount() && autoonoff) {
			
			MnyDrawRecord order = getOrderNo(orderNo+"");
			drawHandlerNotNeedLock(order.getId(), MnyDrawRecord.STATUS_SUCCESS, BigDecimal.ZERO, "提款成功", null);
		}
	}

	@Override
	public void lock(long drawId, long lockFlag) {
		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord draw = drawRecordDao.get(drawId);
		if (draw == null) {
			throw new GenericException("存款记录不存在！");
		}

		// 判断该记录是否被其他用户锁定
		if (draw.getModifyUserId() != null && StringUtil.equals(draw.getLockFlag(), MnyDrawRecord.LOCK_FLAG_LOCKED)
				&& !StringUtil.equals(curUser.getSysUserId(), draw.getModifyUserId())) {
			SysAccount locker = sysAccountDao.get(draw.getModifyUserId());
			throw new GenericException("该记录已被" + locker.getAccount() + "锁定,您无权修改！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(draw.getStauts(), MnyDrawRecord.STATUS_UNTREATED)
				|| StringUtil.equals(draw.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}
		long oldLock = MnyDrawRecord.LOCK_FLAG_LOCKED;
		if (MnyDrawRecord.LOCK_FLAG_LOCKED == lockFlag) {
			oldLock = MnyDrawRecord.LOCK_FLAG_UNLOCKED;
		} else {
			lockFlag = MnyDrawRecord.LOCK_FLAG_UNLOCKED;
		}

		int flag = drawRecordDao.updateLock(drawId, oldLock, lockFlag,curUser.getSysUserId());
		if (flag != 1) {
			throw new GenericException("此记录已经被处理过！");
		}
		if (MnyDrawRecord.LOCK_FLAG_LOCKED == lockFlag) {
			subCountOfUntreated(draw.getStationId());
			SysLogUtil.log("锁定单号为:" + draw.getOrderNo() + "的取款记录", LogType.AGENT_FINANCE);
		} else {
			addCountOfUntreated(draw.getStationId());
			SysLogUtil.log("取消锁定单号为:" + draw.getOrderNo() + "的取款记录", LogType.AGENT_FINANCE);
		}
	}

	@Override
	public void failedRollBack(long drawId) {
		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord mdr = drawRecordDao.get(drawId);
		
		//判断
		if (mdr == null) {
			throw new GenericException("取款记录不存在！");
		}
		//代付无法回滚
		if(mdr.getPayId()!=null) {
			throw new GenericException("代付下发订单无法回滚");
		}
		if (!StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("该记录未完成！");
		}
		// 判断该记录是否被其他用户锁定
		if (!StringUtil.equals(curUser.getSysUserId(), mdr.getModifyUserId())) {
			SysAccount locker = sysAccountDao.get(mdr.getModifyUserId());
			throw new GenericException("该记录已是" + locker.getAccount() + "处理,您无权修改！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mdr.getStauts(), MnyDrawRecord.STATUS_FAILED)
				|| !StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录未被处理过！");
		}
		
		//先扣钱
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		
		moneyVo.setAccount(mdr.getAccount());
		moneyVo.setAccountId(mdr.getMemberId());
		moneyVo.setMoney(mdr.getTrueMoney());

		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_FAILED_ROLLBACK);
		moneyVo.setOrderId(mdr.getOrderNo());
		moneyVo.setBizDatetime(mdr.getCreateDatetime());
		
		moneyService.updMnyAndRecord(moneyVo);
		
		//改变订单状态
		int flag = drawRecordDao.failReback(drawId, new Date(), MnyDrawRecord.LOCK_FLAG_COMPLETED, MnyDrawRecord.STATUS_FAILED, MnyDrawRecord.LOCK_FLAG_LOCKED, MnyDrawRecord.STATUS_UNTREATED);
		if (flag != 1) {
			throw new GenericException("此记录已经被处理过！");
		}
		
		SysLogUtil.log("回滚编号为:" + drawId + "的提款记录", LogType.AGENT_FINANCE);
		
	}
		
	@Override
	public void drawHandler(long drawId, long status, BigDecimal fee, String remark,Long payId, String paymentPassword) {

		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord mdr = drawRecordDao.get(drawId);
		if (mdr == null) {
			throw new GenericException("取款记录不存在！");
		}

		if (!StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_LOCKED)) {
			throw new GenericException("该记录未锁定！");
		}
		// 判断该记录是否被其他用户锁定
		if (!StringUtil.equals(curUser.getSysUserId(), mdr.getModifyUserId())) {
			SysAccount locker = sysAccountDao.get(mdr.getModifyUserId());
			throw new GenericException("该记录已是" + locker.getAccount() + "锁定,您无权修改！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mdr.getStauts(), MnyDrawRecord.STATUS_UNTREATED)
				|| StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}
		//判断代付密码
		String sign = MD5Util.getMD5String(StationUtil.getFolder(), paymentPassword);
		String station_payment_password = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.station_payment_password);
		
		if(Validator.isNotNull(payId) && StringUtil.isNotEmpty(station_payment_password) && !station_payment_password.equalsIgnoreCase(sign)) {
			throw new GenericException("代付密码错误！");
		}
		
		Long stationId = mdr.getStationId();
		// MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		// mdrvo.setStationId(stationId);
		// mdrvo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
		// mdrvo.setAccountId(mdr.getMemberId());
		// mdrvo.setEnd(mdr.getCreateDatetime());
		// Integer untreatedCount = drawRecordDao.getCountOfUntreated(mdrvo);
		// if (untreatedCount != null && untreatedCount > 0) {
		// throw new GenericException("请优先处理该会员更早的提款记录！");
		// }
		SecurityCheckUtil.check(remark);
		AgentPayment payment = null;
		boolean flag = true;
		//判断当前代付是否存在异步通知
		if(Validator.isNotNull(payId)&& PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/handler.do")){
			 payment = paymentService.getHidePayment(payId);
			if(payment!=null && payment.getIsNotity()==1){
				status = MnyDrawRecord.STATUS_WAIT;
				remark = "提款申请成功,等待第三方通知";
				int success = drawRecordDao.drawPayId(payId,status,mdr.getId(),remark,new Date());
				if (success <= 0) {
					throw new GenericException("并发操作！");
				}
				flag = false;
			}
		}
		if(flag){
			long firstDraw = MnyDrawRecord.FIRST_DRAW_NONE;
			//去除首提判断
			/*if(StringUtil.equals(status, MnyDrawRecord.STATUS_SUCCESS)){
				//判断是否是首提
				int count = drawRecordDao.selSuccessCount(mdr.getMemberId(), stationId);
				if(count==0){
					firstDraw = MnyDrawRecord.FIRST_DRAW;
				}
			}*/
			//成功状态
			Long handlerDraw = null;
			if(StringUtil.equals(status, MnyDrawRecord.STATUS_SUCCESS)) {
				//往提款总计表增加数据  返回值是1就是首次提
				handlerDraw = memberDrawInfoDao.handlerDraw(mdr.getMemberId(), 1L, mdr.getDrawMoney());
				if(StringUtil.equals(handlerDraw, 1L)) {
					//首提 往每日表插入数据  只写首提次数和首提金额
					MnyMoneyVo moneyVo = new MnyMoneyVo();
					moneyVo.setMoneyRecordType(MoneyRecordType.FIRST_WITHDRAW);
					moneyVo.setMoney(mdr.getDrawMoney().negate());
					moneyVo.setFirstWithdraw(true);
					SysAccount sysAccount = sysAccountDao.findOneByAccountAndStationId(mdr.getAccount(), stationId);
					if(StringUtil.isEmpty(sysAccount)) {
						throw new GenericException("系统错误，请联系管理员");
					}
					BigDecimal balance = mnyMoneyService.exactDecimal(MnyMoneyDao.getBalance(sysAccount.getId()));
					mnyMoneyService.dailyHandler(moneyVo, new Date(), sysAccount, balance, null);
				}
			}
			int success = drawRecordDao.drawSuccess(mdr.getId(), status, remark,firstDraw,handlerDraw);
			if (success <= 0) {
				throw new GenericException("并发操作！");
			}
		}
		String rk = "";
		// 失败给会员增加金额
		if (StringUtil.equals(status, MnyDrawRecord.STATUS_FAILED)) {

			rk = "提款申请失败：" + remark;
			drawFailedHandler(mdr, rk, fee);
			String cacheKey = "aid_" + mdr.getMemberId() + "_sid_" + mdr.getStationId() + "_d_"
					+ DateUtil.toDateStr(mdr.getCreateDatetime());
			if ("on".equals(StationConfigUtil.get(StationConfig.withdraw_failed_time_onoff))) {
				// 失败次数减1
				RedisAPI.incrby(cacheKey, -1);
			} else {
				RedisAPI.delCache(cacheKey);
			}
			SysLogUtil.log("未批准编号为:" + drawId + "会员:" + mdr.getAccount() + "的取款记录," + rk, LogType.AGENT_FINANCE);
			
			String withdrawRecord = StationConfigUtil.get(stationId, StationConfig.onoff_withdraw_record);
			if(StringUtils.isEmpty(withdrawRecord) || "on".equals(withdrawRecord)){
				sysMessageService.sendUserMessage("提款申请失败", rk, stationId, mdr.getMemberId(), mdr.getAccount());
			}
		} else if (StringUtil.equals(status, MnyDrawRecord.STATUS_SUCCESS)||StringUtil.equals(status, MnyDrawRecord.STATUS_WAIT)) {
			rk = "提款申请成功";
			//下发。
			if(Validator.isNotNull(payId)&& PermissionManager.getAgentPermission().contains("/agent/finance/memdrawrd/handler.do")) {
				paymentService.pay(payId, mdr.getMemberId(),mdr.getId());
				if(payment!=null && payment.getIsNotity()==1){
					//有回调
					rk = "提款申请成功,等待第三方通知";
					SysLogUtil.log("等待通知编号为:" + drawId + "会员:" + mdr.getAccount() + "的取款记录", LogType.AGENT_FINANCE);
				}
			}else{
				// 团队统计增加提款金额
				SysLogUtil.log("批准编号为:" + drawId + "会员:" + mdr.getAccount() + "的取款记录", LogType.AGENT_FINANCE);
			}
		}
		// 回写账变记录的备注
		if (Validator.isNotNull(mdr.getMoneyRecordId())) {
			MnyMoneyRecord rd = moneyRecordDao.get(mdr.getMoneyRecordId());
			rd.setRemark(rk);
			moneyRecordDao.save(rd);
		}

		
	}
	
	@Override
	public void drawHandlerNotNeedLock(long drawId, long status, BigDecimal fee, String remark,Long payId) {

		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord mdr = drawRecordDao.get(drawId);
		if (mdr == null) {
			throw new GenericException("取款记录不存在！");
		}

		if (!StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_UNLOCKED)) {
			throw new GenericException("该记录不是未锁定状态！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mdr.getStauts(), MnyDrawRecord.STATUS_UNTREATED)
				|| StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}
		Long stationId = mdr.getStationId();
		
		SecurityCheckUtil.check(remark);
		AgentPayment payment = null;
		boolean flag = true;
		//判断当前代付是否存在异步通知
		
		if(flag){
			long firstDraw = MnyDrawRecord.FIRST_DRAW_NONE;

			//成功状态
			Long handlerDraw = null;
			if(StringUtil.equals(status, MnyDrawRecord.STATUS_SUCCESS)) {
				//往提款总计表增加数据  返回值是1就是首次提
				handlerDraw = memberDrawInfoDao.handlerDraw(mdr.getMemberId(), 1L, mdr.getDrawMoney());
				if(StringUtil.equals(handlerDraw, 1L)) {
					//首提 往每日表插入数据  只写首提次数和首提金额
					MnyMoneyVo moneyVo = new MnyMoneyVo();
					moneyVo.setMoneyRecordType(MoneyRecordType.FIRST_WITHDRAW);
					moneyVo.setMoney(mdr.getDrawMoney().negate());
					moneyVo.setFirstWithdraw(true);
					SysAccount sysAccount = sysAccountDao.findOneByAccountAndStationId(mdr.getAccount(), stationId);
					if(StringUtil.isEmpty(sysAccount)) {
						throw new GenericException("系统错误，请联系管理员");
					}
					BigDecimal balance = mnyMoneyService.exactDecimal(MnyMoneyDao.getBalance(sysAccount.getId()));
					mnyMoneyService.dailyHandler(moneyVo, new Date(), sysAccount, balance, null);
				}
			}
			int success = drawRecordDao.drawSuccessNotLock(mdr.getId(), status, remark,new Date(), firstDraw,handlerDraw);
			if (success <= 0) {
				throw new GenericException("并发操作！");
			}
		}
		String rk = "";
		// 失败给会员增加金额
		if (StringUtil.equals(status, MnyDrawRecord.STATUS_FAILED)) {

			rk = "提款申请失败：" + remark;
			drawFailedHandler(mdr, rk, fee);
			String cacheKey = "aid_" + mdr.getMemberId() + "_sid_" + mdr.getStationId() + "_d_"
					+ DateUtil.toDateStr(mdr.getCreateDatetime());
			if ("on".equals(StationConfigUtil.get(StationConfig.withdraw_failed_time_onoff))) {
				// 失败次数减1
				RedisAPI.incrby(cacheKey, -1);
			} else {
				RedisAPI.delCache(cacheKey);  
			}
			SysLogUtil.log("未批准编号为:" + drawId + "会员:" + mdr.getAccount() + "的取款记录," + rk, LogType.AGENT_FINANCE);
			
			String withdrawRecord = StationConfigUtil.get(stationId, StationConfig.onoff_withdraw_record);
			if(StringUtils.isEmpty(withdrawRecord) || "on".equals(withdrawRecord)){
				sysMessageService.sendUserMessage("提款申请失败", rk, stationId, mdr.getMemberId(), mdr.getAccount());
			}
		} else if (StringUtil.equals(status, MnyDrawRecord.STATUS_SUCCESS)) {
			rk = "提款申请成功";
			//下发。

			// 团队统计增加提款金额
			SysLogUtil.log("会员自动提款:" + drawId + "会员:" + mdr.getAccount() + "的取款记录", LogType.AGENT_FINANCE);
			
		}
		// 回写账变记录的备注
		if (Validator.isNotNull(mdr.getMoneyRecordId())) {
			MnyMoneyRecord rd = moneyRecordDao.get(mdr.getMoneyRecordId());
			rd.setRemark(rk);
			moneyRecordDao.save(rd);
		}
	}
	
	@Override
	public void drawHandler(long drawId, long status, BigDecimal fee, String remark,String paymentPassword) {
		drawHandler(drawId,status,fee,remark,null,paymentPassword);
	}

	@Override
	public void cancelHandler(MnyDrawRecordVo mcrVo) {
		
		if(!"on".equals(StationConfigUtil.get(StationConfig.withdraw_handler_cancle))) {
			throw new GenericException("非法请求！");
		}
		
		Long comId = mcrVo.getId();
		Long stationId = StationUtil.getStationId();
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		MnyDrawRecord mcr = drawRecordDao.get(comId);
		if (mcr == null) {
			throw new GenericException("提款记录不存在！");
		}

		if (stationId != null && !StringUtil.equals(stationId, mcr.getStationId())) {
			throw new GenericException("非法请求！");
		}

		SecurityCheckUtil.check(mcrVo.getOpDesc());

		Long oldStatus = mcr.getStauts();
		
		if(!StringUtil.equals(oldStatus, MnyDrawRecord.STATUS_SUCCESS)) {
			throw new GenericException("只能撒消提款成功记录！");
		}
		int flag = drawRecordDao.cancleResult(comId, mcrVo.getOpDesc());
		if (flag == 1) {
			moneyVo.setAccount(mcr.getAccount());
			moneyVo.setAccountId(mcr.getMemberId());
			moneyVo.setMoney(mcr.getDrawMoney());
			moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_CANCLE);
			moneyVo.setFee(BigDecimal.ZERO);
			moneyVo.setRemark(mcrVo.getOpDesc());
			SysLogUtil.log("撒消编号为:" + comId + "会员" + mcr.getAccount() + "的提款记录", LogType.AGENT_FINANCE);
			moneyVo.setOrderId(mcr.getOrderNo());
			moneyVo.setBizDatetime(mcr.getCreateDatetime());
			moneyService.updMnyAndRecord(moneyVo);
		}
		
	}

	private void drawFailedHandler(MnyDrawRecord mdr, String rk, BigDecimal fee) {
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(mdr.getStationId());
		moneyVo.setAccount(mdr.getAccount());
		moneyVo.setAccountId(mdr.getMemberId());
		moneyVo.setMoney(mdr.getDrawMoney());
		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ONLINE_FAILED);
		moneyVo.setFee(fee);
		moneyVo.setRemark(rk);
		moneyVo.setOrderId(mdr.getOrderNo());
		moneyVo.setBizDatetime(mdr.getCreateDatetime());
		moneyService.updMnyAndRecord(moneyVo);
	}

	@Override
	public Integer getCountOfUntreated(final long stationId) {

		return CacheUtil.getNull2Set(new DataReader<Integer>() {
			public Integer getData() {
				return getCountOfUntreatedByDb();
			}
		}, Integer.class, CacheType.AGENT_STATION_DRAW_COUNT, stationId + "");
	}

	private Integer getCountOfUntreatedByDb() {
		MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		mdrvo.setStationId(StationUtil.getStationId());
		mdrvo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
		mdrvo.setLockFlag(MnyDrawRecord.LOCK_FLAG_UNLOCKED);
		return drawRecordDao.getCountOfUntreated(mdrvo);
	}

	@Override
	public void addCountOfUntreated(long stationId) {
		CacheUtil.delCache(CacheType.AGENT_STATION_DRAW_COUNT, stationId + "");
		// JedisPool pool = RedisAPI.getPool();
		// Jedis jedis = null;
		// try {
		// CacheConfig cfg =
		// CacheManager.getCacheConifg(CacheType.AGENT_STATION_DRAW_COUNT);
		// String key = CacheUtil.toKey(CacheType.AGENT_STATION_DRAW_COUNT,
		// stationId + "");
		// Integer reVal = 0;
		// Integer dbVal = 0;
		// jedis = pool.getResource();
		// jedis.select(cfg.getSelectDb());
		// if (jedis.exists(key)) {
		// reVal = StringUtil.toInt(jedis.get(key));
		// if (reVal < 0) {
		// dbVal = getCountOfUntreatedByDb();
		// jedis.set(key, dbVal + "");
		// } else {
		// jedis.incrBy(key, 1);
		// }
		// } else {
		// dbVal = getCountOfUntreatedByDb();
		// jedis.set(key, dbVal + "");
		// jedis.expire(key, cfg.getTimeout());
		// }
		//
		// } finally {
		// RedisAPI.returnResource(pool, jedis);
		// }
	}

	@Override
	public void subCountOfUntreated(long stationId) {
		addCountOfUntreated(stationId);
		// JedisPool pool = RedisAPI.getPool();
		// Jedis jedis = null;
		// try {
		// CacheConfig cfg =
		// CacheManager.getCacheConifg(CacheType.AGENT_STATION_DRAW_COUNT);
		// String key = CacheUtil.toKey(CacheType.AGENT_STATION_DRAW_COUNT,
		// stationId + "");
		// Integer reVal = 0;
		// Integer dbVal = 0;
		// jedis = pool.getResource();
		// jedis.select(cfg.getSelectDb());
		// if (jedis.exists(key)) {
		// reVal = StringUtil.toInt(jedis.get(key));
		// if (reVal <= 0) {
		// dbVal = getCountOfUntreatedByDb();
		// jedis.set(key, dbVal + "");
		// } else {
		// jedis.incrBy(key, -1);
		// }
		// } else {
		// dbVal = getCountOfUntreatedByDb();
		// jedis.set(key, dbVal + "");
		// jedis.expire(key, cfg.getTimeout());
		// }
		//
		// } finally {
		// RedisAPI.returnResource(pool, jedis);
		// }
	}

	private Object curDayDrawNumlock = new Object();

	private Object getId;

	@Override
	public int getCurrentDateMemDrawNum(Long accountId, Long stationId) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date start = c.getTime();
		Jedis jedis = null;
		JedisPool pool = RedisAPI.getPool();
		try {
			String cacheKey = "aid_" + accountId + "_sid_" + stationId + "_d_" + DateUtil.toDateStr(start);
			jedis = pool.getResource();
			jedis.select(RedisAPI.DEFAULT_DB_INDEX);
			boolean flag = jedis.exists(cacheKey);
			if (flag == false) {// 不存在 设置过期时间
				synchronized (curDayDrawNumlock) {// 此处使用锁机制，避免并发问题
					if (!jedis.exists(cacheKey)) {
						c.add(Calendar.DAY_OF_MONTH, 1);
						Date end = c.getTime();
						int num = drawRecordDao.countMemDrawNum(accountId, stationId, start, end);
						jedis.set(cacheKey, num + "");
						jedis.expire(cacheKey, 24 * 60 * 60);
						return num;
					}
				}
			}
			// 自增
			int seq = NumberUtils.toInt(jedis.get(cacheKey));
			return seq;
		} finally {
			RedisAPI.returnResource(pool, jedis);
		}
	}

	@Override
	public Page realtslog(HashMap<String, Object> params) {
		RealGameTransLog realGameTransLog = new RealGameTransLog();
		realGameTransLog.setAccountId((Long) params.get("accountId"));
		realGameTransLog.setStationId((Long) params.get("stationId"));
		if (params.get("status") != null && !((String) params.get("status")).equals("")) {
			realGameTransLog.setTransStatus(Integer.parseInt((String) params.get("status")));
		}
		if (params.get("type") != null && !((String) params.get("type")).equals("")) {
			realGameTransLog.setGameType(Integer.parseInt((String) params.get("type")));
		}
		return realGameTransLogDao.pageRecord(realGameTransLog, (Date) params.get("startTime"),
				(Date) params.get("endTime"));
	}

	@Override
	public Map getMemberDrawCheckMap(Long memberId, Long stationId) {
		Map data = new HashMap();
		Map member = sysAccountDao.getAccountById(memberId, stationId);
		
		Long accountType = Long.parseLong(member.get("accountType").toString());
		Long reportType = Long.parseLong(member.get("reportType").toString());
		//引导账号提款隐藏信息开关 //现隐藏姓名
		if("on".equals(StationConfigUtil.get(StationConfig.guide_account_draw_hide_info)) && (accountType.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE) || reportType.equals(SysAccount.REPORT_TYPE_GUIDE))) {
			member = hideInfo(member);
		}
		
		String max = StationConfigUtil.get(StationConfig.withdraw_max_money);
		String min = StationConfigUtil.get(StationConfig.withdraw_min_money);
		String startTime = StationConfigUtil.get(StationConfig.money_start_time);
		String endTime = StationConfigUtil.get(StationConfig.money_end_time);
		String desc = StationConfigUtil.get(StationConfig.withdraw_desc);
		String consumeRate = StationConfigUtil.get(StationConfig.consume_rate);
		int curWnum = getCurrentDateMemDrawNum(memberId, StationUtil.getStationId());
		int wnumInt = NumberUtils.toInt(StationConfigUtil.get(StationConfig.withdraw_time_one_day));
		if (wnumInt > 0 && wnumInt <= curWnum) {
			data.put("drawFlag", "否(今日已提款" + curWnum + "次，不能再提款)");
		}
		// 没设置消费比例或者消费比例为0则可以提款
		if (StringUtil.isEmpty(consumeRate) || "0".equals(consumeRate)) {
			data.put("checkBetNum", "不限制");
			data.put("drawFlag", StationConfigUtil.validateWithdrawalTime(startTime, endTime));
		} else {
			// 消费比例*距上一次提款后的充值金额=提款需要达到的打码量
			data.put("checkBetNum", StringUtil.toBigDecimal(member.get("drawNeed")));
			if (StringUtil.toBigDecimal(member.get("betNum"))
					.compareTo(StringUtil.toBigDecimal(member.get("drawNeed"))) != -1) {
				data.put("drawFlag", "是");
			} else {
				data.put("drawFlag", "否(投注量未达标，不能取款)");
			}
		}
		data.put("max", max);
		data.put("min", min);
		if (wnumInt == 0) {
			data.put("wnum", "不限制");
		} else {
			data.put("wnum", wnumInt);
		}
		data.put("curWnum", curWnum);// 已经提款次数
		data.put("star", startTime);
		data.put("end", endTime);
		data.put("desc", desc);
		data.put("member", member);
		return data;
	}

	public Map hideInfo(Map member) {
		Object userName = member.get("userName");
		if(StringUtil.isNotEmpty(userName) && userName.toString().length()>1) {
			
			String string = userName.toString();
			String suff = string.substring(string.length()-1, string.length());
			
			String newUserName = "**"+suff;
			member.put("userName", newUserName);
		}
		
		return member;
	}
	
	@Override
	public void export(MnyDrawRecordVo mdrvo) {
		String title = "会员提款记录";
		String[] rowsName = new String[] { "序号", "会员", "所属代理", "单号", "收款户名 ","银行卡号", "提款金额(/元)", "申请时间", "状态", "操作","操作员","备注"};
		List<Map> list = drawRecordDao.export(mdrvo);
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < list.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(list.get(i).get("account"));
			objs[2] = getStrNull(list.get(i).get("agentName"));
			objs[3] = getStrNull(list.get(i).get("orderNo"));
			objs[4] = getStrNull(list.get(i).get("userName"));
			objs[5] = getStrNull(list.get(i).get("cardNo"));
			objs[6] = getStrNull(list.get(i).get("drawMoney"));
			objs[7] = getStrNull(list.get(i).get("createDatetime"));
			objs[8] = getStatus(list.get(i).get("status"));
			objs[9] = getLockFlag(list.get(i).get("lockFlag"));
			objs[10] = getStrNull(list.get(i).get("modifyUser"));
			objs[11] = getStrNull(list.get(i).get("remark"));
			dataList.add(objs);
		}
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出" + title, LogType.AGENT_FINANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStrNull(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		return obj.toString();
	}

	/**
	 * 1 : "处理中", 2 : "提款成功", 3 : "提款失败", 4 : "已取消"
	 * 
	 * @param obj
	 * @return
	 */
	private String getStatus(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		if ("1".equals(obj.toString())) {
			return "处理中";
		} else if ("2".equals(obj.toString())) {
			return "提款成功";
		} else if ("3".equals(obj.toString())) {
			return "提款失败";
		} else if ("4".equals(obj.toString())) {
			return "已取消";
		} else {
			return "未知状态";
		}
	}

	/**
	 * 是否操作
	 * 
	 * @param obj
	 * @return
	 */
	private String getLockFlag(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		if ("1".equals(obj.toString()) || "2".equals(obj.toString())) {
			return "未操作";
		} else {
			return "已操作";
		}
	}

	@Override
	public Long getDrawTotalMoney(Long accountId, Long stationId) {
		return drawRecordDao.getDrawTotalMoney(accountId, stationId);
	}

	@Override
	public MnyDrawRecord getOne(Long id, Long stationId) {
		if (id != null) {
			MnyDrawRecord it = drawRecordDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

	@Override
	public List<MnyDrawRecord> getUntreatedReocrd(Long stationId) {
		String timeoutStr = StationConfigUtil.get(stationId, StationConfig.withdraw_record_timeout);
		if (StringUtils.isEmpty(timeoutStr)) {
			return null;
		}
		if (!timeoutStr.matches("^[\\d]+(\\.[\\d]{1,2})?$")) {
			return null;
		}
		BigDecimal timeout = BigDecimalUtil.toBigDecimal(timeoutStr);
		if (timeout == null || timeout.compareTo(BigDecimal.ZERO) <= 0) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, -timeout.multiply(new BigDecimal(3600)).intValue());
		return drawRecordDao.getUntreatedReocrd(stationId, c.getTime());
	}

	@Override
	public void autoExpiredStatus(MnyDrawRecord reocrd) {
		int success = drawRecordDao.updateStatusToExpired(reocrd.getId());
		// 执行受影响行数为0，则不再继续处理，防止并发
		if (success <= 0) {
			return;
		}
		String rk = "超过过期时间系统自动过期处理！";
		drawFailedHandler(reocrd, rk, BigDecimal.ZERO);
		subCountOfUntreated(reocrd.getStationId());
	}
	
	/**
	 * 验证2次充值时间间隔
	 * 
	 * @param accountId
	 * @param stationId
	 */
	private void validWithdrawInterval(Long accountId, Long stationId) {
		String withdrawIntervalTimes = StationConfigUtil.get(stationId, StationConfig.withdraw_interval_times);
		if (StringUtils.isNoneEmpty(withdrawIntervalTimes)) {
			int seconds = NumberUtils.toInt(withdrawIntervalTimes, 0);
			if (seconds > 0) {
				Calendar c = Calendar.getInstance();
				c.add(Calendar.SECOND, -seconds);
				long count = drawRecordDao.countWithdrawCount(accountId, c.getTime(), stationId);
				if (count > 0) {
					throw new GenericException(withdrawIntervalTimes + "秒内只能提现一次");
				}
			}
		}
	}
	@Override
	public MnyDrawRecord getOrderNo(String orderNo) {
		// TODO Auto-generated method stub
		return drawRecordDao.getOrderNo(orderNo);
	}
	@Override
	public void onlinePaymentHandler(MnyDrawRecordVo mdrVo) {

		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord mdr = drawRecordDao.get(mdrVo.getId());
		if (mdr == null) {
			throw new GenericException("取款记录不存在！");
		}

		if (!StringUtil.equals(mdr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_LOCKED_WAIT)) {
			throw new GenericException("该记录状态错误！");
		}
		// 判断该记录是否被其他用户锁定
		/*if (!StringUtil.equals(curUser.getSysUserId(), mdr.getModifyUserId())) {
			SysAccount locker = sysAccountDao.get(mdr.getModifyUserId());
			throw new GenericException("该记录已是" + locker.getAccount() + "锁定,您无权修改！");
		}*/

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mdr.getStauts(), MnyDrawRecord.STATUS_WAIT)
				) {
			throw new GenericException("此记录已经被处理过！");
		}
		Long stationId = mdr.getStationId();
		// MnyDrawRecordVo mdrvo = new MnyDrawRecordVo();
		// mdrvo.setStationId(stationId);
		// mdrvo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
		// mdrvo.setAccountId(mdr.getMemberId());
		// mdrvo.setEnd(mdr.getCreateDatetime());
		// Integer untreatedCount = drawRecordDao.getCountOfUntreated(mdrvo);
		// if (untreatedCount != null && untreatedCount > 0) {
		// throw new GenericException("请优先处理该会员更早的提款记录！");
		// }
		SecurityCheckUtil.check(mdrVo.getOpDesc());
		long firstDraw = MnyDrawRecord.FIRST_DRAW_NONE;
		//成功判断是否是首提
		if(StringUtil.equals(mdrVo.getStatus(), MnyDrawRecord.STATUS_SUCCESS)){
			int count = drawRecordDao.selSuccessCount(mdr.getMemberId(), stationId);
			if(count==0){
				firstDraw = MnyDrawRecord.FIRST_DRAW;
			}
		}
		Long handlerDraw = null;
		if(StringUtil.equals(mdrVo.getStatus(), MnyDrawRecord.STATUS_SUCCESS)) {
			//往提款总计表增加数据  返回值是1就是首次提
			handlerDraw = memberDrawInfoDao.handlerDraw(mdr.getMemberId(), 1L, mdr.getDrawMoney());
			if(StringUtil.equals(handlerDraw, 1L)) {
				//首提 往每日表插入数据  只写首提次数和首提金额
				MnyMoneyVo moneyVo = new MnyMoneyVo();
				moneyVo.setMoneyRecordType(MoneyRecordType.FIRST_WITHDRAW);
				moneyVo.setMoney(mdr.getDrawMoney().negate());
				moneyVo.setFirstWithdraw(true);
				SysAccount sysAccount = sysAccountDao.findOneByAccountAndStationId(mdr.getAccount(), stationId);
				if(StringUtil.isEmpty(sysAccount)) {
					throw new GenericException("系统错误，请联系管理员");
				}
				BigDecimal balance = mnyMoneyService.exactDecimal(MnyMoneyDao.getBalance(sysAccount.getId()));
				mnyMoneyService.dailyHandler(moneyVo, new Date(), sysAccount, balance, null);
			}
		}
		int success = drawRecordDao.drawNotitySuccess(mdr.getId(), mdrVo.getStatus(), mdrVo.getOpDesc(),firstDraw,handlerDraw);
		if (success <= 0) {
			throw new GenericException("并发操作！");
		}
		String rk = "";
		// 失败给会员增加金额
		if (StringUtil.equals( mdrVo.getStatus(), MnyDrawRecord.STATUS_FAILED)) {
			rk = "提款申请失败：" +  mdrVo.getOpDesc();
			drawFailedHandler(mdr, rk, mdrVo.getFee());
			String cacheKey = "aid_" + mdr.getMemberId() + "_sid_" + mdr.getStationId() + "_d_"
					+ DateUtil.toDateStr(mdr.getCreateDatetime());
			if ("on".equals(StationConfigUtil.get(StationConfig.withdraw_failed_time_onoff))) {
				// 失败次数减1
				RedisAPI.incrby(cacheKey, -1);
			} else {
				RedisAPI.delCache(cacheKey);
			}
			SysLogUtil.log("代付下发失败编号为:" + mdrVo.getId() + "会员:" + mdr.getAccount() + "的取款记录," + rk, "",null, mdr.getAccount(), UserUtil.getIpAddress(), mdr.getStationId(), LogType.ADMIN_LOG.getType(), SysLog.PLATFORM_ADMIN,
					JsonUtil.toJson(UserUtil.getRequest().getParameterMap()));
			String withdrawRecord = StationConfigUtil.get(stationId, StationConfig.onoff_withdraw_record);
			if(StringUtils.isEmpty(withdrawRecord) || "on".equals(withdrawRecord)){
				sysMessageService.sendUserMessage("提款申请失败", rk, stationId, mdr.getMemberId(), mdr.getAccount());
			}
		} else if (StringUtil.equals(mdrVo.getStatus(), MnyDrawRecord.STATUS_SUCCESS)) {
			rk = "提款申请成功："+ mdrVo.getOpDesc();
			if (Validator.isNotNull(mdr.getMoneyRecordId())) {
				MnyMoneyRecord rd = moneyRecordDao.get(mdr.getMoneyRecordId());
				rd.setRemark(rk);
				moneyRecordDao.save(rd);
				SysLogUtil.log("代付下发成功编号为:" + mdrVo.getId() + "会员:" + mdr.getAccount() + "的取款记录," + rk, "",null, mdr.getAccount(), UserUtil.getIpAddress(), mdr.getStationId(), LogType.ADMIN_LOG.getType(), SysLog.PLATFORM_ADMIN,
						JsonUtil.toJson(UserUtil.getRequest().getParameterMap()));
			}
		}
		
	}
	@Override
	public void rollBack(Long drawId) {
		User curUser = UserUtil.getCurrentUser();
		MnyDrawRecord draw = drawRecordDao.get(drawId);
		if (draw == null) {
			throw new GenericException("存款记录不存在！");
		}
		if(draw.getModifyUserId() != null && (!StringUtil.equals(draw.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)
				&& !StringUtil.equals(draw.getStauts(), MnyDrawRecord.STATUS_FAILED))) {
			throw new GenericException("当前订单状态无法回滚");
		}
		if(draw.getPayId()!=null) {
			throw new GenericException("代付下发订单无法回滚");
		}
		// 判断该记录是否被其他用户锁定
		if (draw.getModifyUserId() != null && StringUtil.equals(draw.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)
				&& StringUtil.equals(draw.getStauts(), MnyDrawRecord.STATUS_SUCCESS)
				&& !StringUtil.equals(curUser.getSysUserId(), draw.getModifyUserId())) {
			SysAccount locker = sysAccountDao.get(draw.getModifyUserId());
			throw new GenericException("该订单已由" + locker.getAccount() + "批准失败,您无权修改！");
		}
		
		long lockFlag = MnyDrawRecord.LOCK_FLAG_LOCKED;
		int flag = drawRecordDao.reback(drawId,lockFlag);
		if (flag != 1) {
			throw new GenericException("此记录已经被处理过！");
		}
		//回滚减1
		MemberWithdrawInfo memberWithdrawInfo = memberDrawInfoDao.get(draw.getMemberId());
		memberWithdrawInfo.setWithdrawCount(memberWithdrawInfo.getWithdrawCount()-1);
		memberWithdrawInfo.setWithdrawTotal(memberWithdrawInfo.getWithdrawTotal().subtract(draw.getDrawMoney()));
		
		memberDrawInfoDao.update(memberWithdrawInfo);
		SysLogUtil.log("回滚编号为:" + drawId + "的提款记录", LogType.AGENT_FINANCE);
		
	}
}