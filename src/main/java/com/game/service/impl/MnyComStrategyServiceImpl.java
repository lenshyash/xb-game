package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.LogType;
import com.game.dao.MemberLevelBaseDao;
import com.game.dao.MnyComStrategyDao;
import com.game.dao.MnyComStrategyLevelDao;
import com.game.model.MemberLevelBase;
import com.game.model.MnyComStrategy;
import com.game.model.MnyComStrategyLevel;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.MemberDepositStrategy;
import com.game.service.MnyComStrategyService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;
import com.game.util.SysLogUtil;

@Service
public class MnyComStrategyServiceImpl implements MnyComStrategyService {
	@Autowired
	private MnyComStrategyDao mnyComStrategyDao;
	@Autowired
	private MnyComStrategyLevelDao mnyComStrategyLevelDao;
	@Autowired
	private MemberLevelBaseDao memberLevelBaseDao;
	@Autowired
	private SysAccountService accountService;

	@Override
	public Page<MnyComStrategy> getPage(Integer depositType, Integer giftType, Integer valueType, Date begin, Date end,
			Long stationId) {
		return mnyComStrategyDao.getPage(depositType, giftType, valueType, begin, end, stationId);
	}

	@Override
	public MnyComStrategy getOne(Long id, Long stationId) {
		return mnyComStrategyDao.getOne(id, stationId);
	}

	@Override
	public void delete(Long id, Long stationId) {
		mnyComStrategyDao.delete(id, stationId);
		mnyComStrategyLevelDao.deleteByStrategyId(id, stationId);
	}

	@Override
	public void addSave(MnyComStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId) {
		com.setStartDatetime(DateUtil.toDate(startDate));
		com.setEndDatetime(DateUtil.toDate(endDate));
		if (com.getEndDatetime() == null || com.getStartDatetime() == null) {
			throw new GenericException("请选择活动时间");
		}
		if (com.getEndDatetime().getTime() <= com.getStartDatetime().getTime()) {
			throw new GenericException("活动结束时间必须大于活动开始时间");
		}
		com.setCreateDatetime(new Date());
		com.setStatus(MnyComStrategy.STATUS_NORMAL);
		com.setStationId(stationId);
		if (com.getDepositCount() == null) {
			com.setDepositCount(0);
		}
		if (com.getMinMoney() == null) {
			com.setMinMoney(BigDecimal.ZERO);
		}
		if (com.getMaxMoney() == null) {
			com.setMaxMoney(new BigDecimal("10000000"));
		}
		if (com.getMinMoney().compareTo(com.getMaxMoney()) >= 0) {
			throw new GenericException("充值金额最小值必须小于最大值");
		}
		List<Long> levelIds = new ArrayList<>();
		if (groupLevelIds != null && groupLevelIds.length > 0) {
			for (Long lid : groupLevelIds) {
				levelIds.add(lid);
			}
		}
		validStrategy(com, levelIds);
		mnyComStrategyDao.insert(com);
		SysLogUtil.log("添加存款赠送策略", LogType.AGENT_FINANCE);
		if (groupLevelIds != null && groupLevelIds.length != 0) {
			Set<Long> groupLevelSet = new HashSet<>();
			MnyComStrategyLevel mcsl = null;
			List<MnyComStrategyLevel> llist = new ArrayList<>();
			for (Long glId : groupLevelIds) {
				if (!groupLevelSet.contains(glId)) {
					mcsl = new MnyComStrategyLevel();
					mcsl.setMemberLevelId(glId);
					mcsl.setStationId(stationId);
					mcsl.setStrategyId(com.getId());
					llist.add(mcsl);
					groupLevelSet.add(glId);
				}
			}
			mnyComStrategyLevelDao.batchInsert(llist);
		}
	}

	private void validStrategy(MnyComStrategy com, List<Long> groupLevelIds) {
		List<MnyComStrategy> list = mnyComStrategyDao.findByDepositType(com.getDepositType(), com.getStationId(),
				MnyComStrategy.STATUS_NORMAL, null, null);
		if (list == null || list.isEmpty()) {
			return;
		}
		long start = com.getStartDatetime().getTime();
		long end = com.getEndDatetime().getTime();
		BigDecimal min = com.getMinMoney();
		BigDecimal max = com.getMaxMoney();
		List<Long> levelIds = null;
		MemberLevelBase level = null;
		// 同一个时间内，同个金额范围内，同一个等级范围内，不能同时存在2条活动策略
		for (MnyComStrategy mcs : list) {
			if (com.getId() != null && com.getId().equals(mcs.getId())) {
				continue;
			}
			if (mcs.getEndDatetime().getTime() < start || mcs.getStartDatetime().getTime() > end) {
				continue;
			}
			if (mcs.getMaxMoney().compareTo(min) < 0 || mcs.getMinMoney().compareTo(max) > 0) {
				continue;
			}
			if (!mcs.getDepositCount().equals(com.getDepositCount())) {
				continue;
			}
			if (!mcs.getValueType().equals(com.getValueType())) {
				continue;
			}
			if (groupLevelIds == null || groupLevelIds.isEmpty()) {
				throw new GenericException("活动冲突：" + mcs.getDesc());
			}
			levelIds = mnyComStrategyLevelDao.findLevels(mcs.getId(), mcs.getStationId());
			if (levelIds == null || levelIds.isEmpty()) {
				throw new GenericException("活动冲突：" + mcs.getDesc());
			}
			for (Long lid : groupLevelIds) {
				if (levelIds.contains(lid)) {
					level = memberLevelBaseDao.get(lid);
					if (level != null) {
						throw new GenericException("活动冲突：" + mcs.getDesc());
					}
				}
			}
		}
	}

	@Override
	public void updStatus(Integer status, Long id, Long stationId) {
		MnyComStrategy mcs = mnyComStrategyDao.getOne(id, stationId);
		if (mcs == null) {
			throw new GenericException("存款赠送策略不存在");
		}
		String statusStr = "禁用";
		if (status == MnyComStrategy.STATUS_NORMAL) {
			if (mcs.getEndDatetime().before(new Date())) {
				throw new GenericException("启用失败,结束时间小于当前时间！");
			}
			validStrategy(mcs, mnyComStrategyLevelDao.findLevels(id, stationId));
			statusStr = "启用";
		} else {
			status = MnyComStrategy.STATUS_DISABLED;
		}

		if (mcs.getStatus() != status) {
			mnyComStrategyDao.updStatus(id, status);
			SysLogUtil.log("修改存款赠送策略：" + id + "状态为：" + statusStr, LogType.AGENT_FINANCE);
		}
	}

	@Override
	public void update(MnyComStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId) {
		com.setStartDatetime(DateUtil.toDate(startDate));
		com.setEndDatetime(DateUtil.toDate(endDate));
		if (com.getEndDatetime() == null || com.getStartDatetime() == null) {
			throw new GenericException("请选择活动时间");
		}
		if (com.getEndDatetime().getTime() <= com.getStartDatetime().getTime()) {
			throw new GenericException("活动结束时间必须大于活动开始时间");
		}
		if (com.getMinMoney() == null) {
			com.setMinMoney(BigDecimal.ZERO);
		}
		if (com.getMaxMoney() == null) {
			com.setMaxMoney(new BigDecimal("10000000"));
		}
		if (com.getMinMoney().compareTo(com.getMaxMoney()) >= 0) {
			throw new GenericException("充值金额最小值必须小于最大值");
		}
		MnyComStrategy old = mnyComStrategyDao.getOne(com.getId(), stationId);
		if (old == null) {
			throw new GenericException("请选择赠送策略");
		}
		old.setDepositType(com.getDepositType());
		old.setGiftType(com.getGiftType());
		old.setValueType(com.getValueType());

		old.setDepositCount(com.getDepositCount());
		if (old.getDepositCount() == null) {
			old.setDepositCount(0);
		}

		old.setGiftValue(com.getGiftValue());
		old.setUpperLimit(com.getUpperLimit());
		old.setMinMoney(com.getMinMoney());
		old.setMaxMoney(com.getMaxMoney());
		old.setBetMultiple(com.getBetMultiple());
		old.setStartDatetime(com.getStartDatetime());
		old.setEndDatetime(com.getEndDatetime());
		old.setMemo(com.getMemo());
		if (old.getStatus() == MnyComStrategy.STATUS_NORMAL) {
			List<Long> levelIds = new ArrayList<>();
			if (groupLevelIds != null && groupLevelIds.length > 0) {
				for (Long lid : groupLevelIds) {
					levelIds.add(lid);
				}
			}
			validStrategy(old, levelIds);
		}
		mnyComStrategyDao.update(old);
		SysLogUtil.log("修改存款赠送策略", LogType.AGENT_FINANCE);
		if (groupLevelIds != null && groupLevelIds.length != 0) {
			List<Long> oldList = mnyComStrategyLevelDao.findLevels(old.getId(), stationId);
			Set<Long> groupLevelSet = new HashSet<>();
			MnyComStrategyLevel mcsl = null;
			List<MnyComStrategyLevel> llist = new ArrayList<>();
			for (Long glId : groupLevelIds) {
				if (!groupLevelSet.contains(glId) && !oldList.contains(glId)) {
					mcsl = new MnyComStrategyLevel();
					mcsl.setMemberLevelId(glId);
					mcsl.setStationId(stationId);
					mcsl.setStrategyId(com.getId());
					llist.add(mcsl);
					groupLevelSet.add(glId);
				}
				if (oldList != null) {
					oldList.remove(glId);
				}
			}
			if (!llist.isEmpty()) {
				mnyComStrategyLevelDao.batchInsert(llist);
			}
			if (oldList != null && !oldList.isEmpty()) {
				mnyComStrategyLevelDao.deletes(oldList, old.getId(), stationId);
			}
		}
	}

	@Override
	public MnyComStrategy filter(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId) {
		int type = getDepositType(moneyRecordType);
		if (type == 0) {
			return null;
		}
		List<MnyComStrategy> list = mnyComStrategyDao.findByDepositType(type, stationId, MnyComStrategy.STATUS_NORMAL,
				depositDate, money);
		list = filterByDepositCount(depositCount, dayDepoitCount, list);
		list = filterByMemberLevel(accountId, stationId, list);
		if (list == null || list.isEmpty()) {
			return null;
		}

		Collections.sort(list, new Comparator<MnyComStrategy>() {
			@Override
			public int compare(MnyComStrategy o1, MnyComStrategy o2) {
				return o2.getDepositCount().compareTo(o1.getDepositCount());
			}
		});

		return list.get(0);
	}
	
	@Override
	public List<MnyComStrategy> filterList(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId) {
		int type = getDepositType(moneyRecordType);
		if (type == 0) {
			return null;
		}
		List<MnyComStrategy> list = mnyComStrategyDao.findByDepositType(type, stationId, MnyComStrategy.STATUS_NORMAL,
				depositDate, money);
		list = filterByDepositCount(depositCount, dayDepoitCount, list);
		list = filterByMemberLevel(accountId, stationId, list);
		if (list == null || list.isEmpty()) {
			return null;
		}

		Collections.sort(list, new Comparator<MnyComStrategy>() {
			@Override
			public int compare(MnyComStrategy o1, MnyComStrategy o2) {
				return o2.getDepositCount().compareTo(o1.getDepositCount());
			}
		});

		List<MnyComStrategy> resultList = new ArrayList<>();
		for (MnyComStrategy mnyComStrategy : list) {
			if(resultList.size() == 2) {
				return resultList;
			}
			if(resultList.size() == 0 || (resultList.size() != 2 && !resultList.get(0).getValueType().equals(mnyComStrategy.getValueType()))) {
				resultList.add(mnyComStrategy);
			}
		}
		return resultList;
	}

	/**
	 * 根据账变类型获取存款类型
	 * 
	 * @param moneyRecordType
	 * @return
	 */
	private int getDepositType(long moneyRecordType) {
		int type = 0;
		if (moneyRecordType == MoneyRecordType.DEPOSIT_BY_SYS_API.getType()) {
			type = MnyComStrategy.TYPE_SYS_API;
		} else if (moneyRecordType == MoneyRecordType.DEPOSIT_ARTIFICIAL.getType()) {
			type = MnyComStrategy.TYPE_ARTIFICIAL;
		} else if (moneyRecordType == MoneyRecordType.DEPOSIT_ONLINE_THIRD.getType()) {
			type = MnyComStrategy.TYPE_ONLINE;
		} else if (moneyRecordType == MoneyRecordType.DEPOSIT_ONLINE_BANK.getType()) {
			type = MnyComStrategy.TYPE_BANK;
		} else if (moneyRecordType == MoneyRecordType.DEPOSIT_ONLINE_FAST.getType()) {
			type = MnyComStrategy.TYPE_FAST;
		}
		return type;
	}

	/**
	 * 根据存款次数过滤
	 * 
	 * @param depositCount
	 * @param list
	 * @return
	 */
	private List<MnyComStrategy> filterByDepositCount(long depositCount, long dayDepoitCount,
			List<MnyComStrategy> list) {
		if (list == null || list.isEmpty()) {
			return null;
		}
		List<MnyComStrategy> rlist = new ArrayList<>();
		for (MnyComStrategy m : list) {
			switch (m.getDepositCount()) {
			case MnyComStrategy.deposit_count_0:// 每次
				rlist.add(m);
				break;
			case MnyComStrategy.deposit_count_1:// 首充
				if (depositCount == 1) {
					rlist.add(m);
				}
				break;
			case MnyComStrategy.deposit_count_2:// 第二次充值
				if (depositCount == 2) {
					rlist.add(m);
				}
				break;
			case MnyComStrategy.deposit_count_3:// 第三次充值
				if (depositCount == 3) {
					rlist.add(m);
				}
				break;
			case MnyComStrategy.deposit_count_222:// 前两次充值
				if (depositCount == 1 || depositCount == 2) {
					rlist.add(m);
				}
				break;
			case MnyComStrategy.deposit_count_333:// 前三次充值
				if (depositCount == 1 || depositCount == 2 || depositCount == 3) {
					rlist.add(m);
				}
				break;
			case MnyComStrategy.deposit_count_444:// 每日首充
				if(dayDepoitCount==1){
					rlist.add(m);
				}
				break;
			}
		}
		return rlist;
	}

	/**
	 * 根据会员层级过滤策略
	 * 
	 * @param accountId
	 * @param stationId
	 * @param list
	 * @return
	 */
	private List<MnyComStrategy> filterByMemberLevel(Long accountId, Long stationId, List<MnyComStrategy> list) {
		if (list == null || list.isEmpty())
			return null;
		SysAccount acc = accountService.getOne(accountId, stationId);
		if (acc == null || acc.getLevelGroup() == null || acc.getLevelGroup() <= 0) {
			return null;
		}
		List<MnyComStrategy> rlist = new ArrayList<>();
		List<Long> levelList = null;
		for (MnyComStrategy m : list) {
			levelList = mnyComStrategyLevelDao.findLevels(m.getId(), stationId);
			if (levelList == null || levelList.isEmpty() || levelList.contains(acc.getLevelGroup())) {
				rlist.add(m);
			}
		}
		return rlist;
	}

	@Override
	public List<MnyComStrategy> getByType(int type, Long stationId) {
		return mnyComStrategyDao.findByDepositType(type, stationId, MnyComStrategy.STATUS_NORMAL, new Date(), null);
	}
}
