package com.game.service.impl;

import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.dao.MemberWhiteIpDao;
import com.game.model.MemberWhiteIp;
import com.game.service.MemberWhiteIpService;
import com.game.util.StationUtil;

@Repository
public class MemberWhiteIpServiceImpl implements MemberWhiteIpService {

	@Autowired
	private MemberWhiteIpDao whiteIpDao;

	@Override
	public void save(MemberWhiteIp whiteIp) {
		if (whiteIpDao.isNotUnique(whiteIp, "ip,stationId,type")) {
			throw new GenericException("IP [" + whiteIp.getIp() + "]已经存在");
		}
		whiteIpDao.save(whiteIp);
		CacheUtil.delCache(CacheType.MEMBER_WHITE_IP_LIST, StationUtil.getStationId() + ":"+whiteIp.getType());
	}

	@Override
	public void deleteIp(long ipId) {
		MemberWhiteIp ip = checkStationPermission(ipId);
		whiteIpDao.delete(ipId);
		CacheUtil.delCache(CacheType.MEMBER_WHITE_IP_LIST, StationUtil.getStationId() + ":"+ip.getType());
	}

	@Override
	public void updateStatus(long ipId, long status) {
		MemberWhiteIp oldType = checkStationPermission(ipId);
		MemberWhiteIp ip = new MemberWhiteIp();
		ip.setId(ipId);
		ip.setStatus(status);
		whiteIpDao.updateNotNull(ip);
		CacheUtil.delCache(CacheType.MEMBER_WHITE_IP_LIST, StationUtil.getStationId() + ":" + oldType.getType());
	}

	@Override
	public Page queryPage(Long type) {
		return whiteIpDao.queryPage(StationUtil.getStationId(),type);
	}

	private MemberWhiteIp checkStationPermission(long ipId) {
		MemberWhiteIp ip = whiteIpDao.get(ipId);
		if (!StationUtil.isAgentStation()) {
			return ip;
		}
		if (ip == null) {
			throw new GenericException("记录不存在");
		}
		if (!StringUtil.equals(ip.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法操作");
		}
		return ip;
	}

	@Override
	public List<MemberWhiteIp> getStationWhiteIpList(Long type) {
		long stationId = StationUtil.getStationId();
		return whiteIpDao.getStationWhiteIpList(stationId,type);
	}
}
