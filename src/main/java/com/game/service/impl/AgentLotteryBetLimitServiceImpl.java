package com.game.service.impl;

import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.AgentLotteryBetLimitDao;
import com.game.model.AgentLotteryBetLimit;
import com.game.service.AgentLotteryBetLimitService;

@Service
public class AgentLotteryBetLimitServiceImpl implements AgentLotteryBetLimitService {
	
	@Autowired
	private AgentLotteryBetLimitDao AgentLotteryBetLimitDao;
	
	public Page getPage(Long stationId) {
		if(Validator.isNull(stationId)) {
			throw new GenericException("stationId为空");
		}
		return AgentLotteryBetLimitDao.getPage(stationId);
	}
	
	public void updateStatus(Long id,Long status,Long stationId) {
		AgentLotteryBetLimitDao.updateStatus(id, status, stationId);
	}
	
	public void save(AgentLotteryBetLimit betLimit) {
		AgentLotteryBetLimitDao.save(betLimit);
	}
	
	public List<AgentLotteryBetLimit> getActiveList(Long stationId){
		Long status = 2L;
		return AgentLotteryBetLimitDao.getList(stationId, status);
	}
	
	public void delete(Long id) {
		AgentLotteryBetLimitDao.delete(id);
	}

	public AgentLotteryBetLimit getOne(Long stationId, String groupName, String code,
			Long limitType, Long lotType) {
		return AgentLotteryBetLimitDao.getOne(stationId, groupName, code, limitType, lotType);
	}
	
	
}