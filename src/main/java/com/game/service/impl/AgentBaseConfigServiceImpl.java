package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.BusinessConstant;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.AgentBaseConfigDao;
import com.game.dao.AgentBaseConfigGroupDao;
import com.game.dao.AgentBaseConfigValueDao;
import com.game.dao.StationPlatformTransferDao;
import com.game.dao.SysStationDao;
import com.game.dao.lottery.BcLotteryDao;
import com.game.event.LotteryPlayModifyEvent;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigGroup;
import com.game.model.AgentBaseConfigValue;
import com.game.model.StationPlatformTransfer;
import com.game.model.lottery.BcLottery;
import com.game.model.third.TranLimitCache;
import com.game.service.AgentBaseConfigService;
import com.game.third.constant.Platform;
import com.game.util.SpringUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Repository
public class AgentBaseConfigServiceImpl implements AgentBaseConfigService {

    @Autowired
    private AgentBaseConfigValueDao configValueDao;
    @Autowired
    private StationPlatformTransferDao platformTransferDao;

    @Autowired
    private AgentBaseConfigGroupDao configGroupDao;

    @Autowired
    private AgentBaseConfigDao configDao;
    @Autowired
    private BcLotteryDao lotteryDao;
    @Autowired
    private SysStationDao stationDao;

    @Override
    public List<AgentBaseConfig> getConfigAll() {
        return configDao.getConfigAll(null, AgentBaseConfig.PLATFORM_AGENT);
    }

    @Override
    public List<AgentBaseConfigGroup> getConfigGroupLst(Long platform) {
        if (Validator.isNull(platform)) {
            platform = AgentBaseConfig.PLATFORM_AGENT;
        }
        return configGroupDao.getGroups(platform);
    }

    @Override
    public List<AgentBaseConfig> getSysConfigs() {
        return configDao.getSysConfs();
    }

    @Override
    public List<Map> getStationConfigs(long stationId) {
        boolean allStatus = false;
        if (StationUtil.isAdminStation()) {
            allStatus = true;
        }
        return getStationConfigs(stationId, allStatus, false);
    }

    @Override
    public List<Map> getStationConfigs(long stationId, boolean allStatus, boolean cache) {
        String key = "s_" + stationId + "_all_setting_configs_all_" + allStatus;
        List<Map> stationConfigs = null;
        if (cache) {
            String json = CacheUtil.getCache(CacheType.AGENT_SETTING, key);
            if (StringUtil.isNotEmpty(json)) {
                return (List<Map>) JsonUtil.toBean(json, List.class);
            }
        }
        stationConfigs = configValueDao.getStationConfs(stationId, AgentBaseConfigValue.AGENT_BASE_VALUE_ENABLE, allStatus);
        CacheUtil.addCache(CacheType.AGENT_SETTING, key, JsonUtil.toJson(stationConfigs));
        return stationConfigs;
    }

    @Override
    public void saveConfigValue(Long id, String value, Long stationId) {
        if (Validator.isNull(id)) {
            throw new GenericException("数据异常！");
        }
        AgentBaseConfigValue saveAbcv = configValueDao.get(id);
        AgentBaseConfig conf = configDao.get(saveAbcv.getConfigId());
        if (conf == null) {
            throw new GenericException("数据异常！");
        }
        if (StringUtils.equals(conf.getKey(), StationConfig.sys_lottery_run_percentage.name())
                || StringUtils.equals(conf.getKey(), StationConfig.sf_mark_six_run_percentage.name())) {
            // 系统彩中奖百分比控制在30～100之间
            Integer p = NumberUtils.toInt(value);
            if (p < 30 || p > 100) {
                throw new GenericException("中奖百分比必须大等于30且小等于100！");
            }
        }
        if (Objects.equals(conf.getStatus(), AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE)) {
            throw new GenericException("此配置处于禁用状态！");
        }

        if (StationUtil.isAgentPage()) {
            if (Objects.equals(conf.getStatus(), AgentBaseConfig.AGENT_BASE_CONFIG_HIDDEN)) {
                throw new GenericException("无修改权限！");
            }
            if (!Objects.equals(StationUtil.getStationId(), saveAbcv.getStationId())) {
                throw new GenericException("非法操作！");
            }
        } else if (!StationUtil.isAdminPage()) {
            throw new GenericException("非法操作！");
        }
        String oldValue = saveAbcv.getValue();
        saveAbcv.setValue(value);
        boolean showFlag = false;

        BigDecimal oldBig = StringUtil.toBigDecimal(oldValue);
        BigDecimal big = StringUtil.toBigDecimal(value);

        if (big.compareTo(BigDecimal.ZERO) > 0 || oldBig.compareTo(BigDecimal.ZERO) > 0) {
            showFlag = true;
        }

        configValueDao.updateValue(saveAbcv, conf.getKey());
        if (StationConfig.onoff_lengre_yilou.name().equals(conf.getKey())) {
            SpringUtil.publishEvent(new LotteryPlayModifyEvent(saveAbcv.getStationId(), null));
        }
        String desc = "更新站点编号为:" + stationDao.get(saveAbcv.getStationId()).getFloder() + "的" + conf.getName() + "配置项,原值为:"
                + oldValue + "改为:" + value;
        if (showFlag) {
            desc += ",差异值：" + big.subtract(oldBig);
        }
        if (StationUtil.isAdminStation()) {

            //总控给租户真人上分后要删除缓存
            StationConfig limitKey = null;
            String conKey = conf.getKey();
            if (StringUtils.equals(conKey, StationConfig.live_transfor_ag_limit.name())) {
                limitKey = StationConfig.live_transfor_ag_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_bbin_limit.name())) {
                limitKey = StationConfig.live_transfor_bbin_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_mg_limit.name())) {
                limitKey = StationConfig.live_transfor_mg_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_pt_limit.name())) {
                limitKey = StationConfig.live_transfor_pt_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_qt_limit.name())) {
                limitKey = StationConfig.live_transfor_qt_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ab_limit.name())) {
                limitKey = StationConfig.live_transfor_ab_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_og_limit.name())) {
                limitKey = StationConfig.live_transfor_og_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ds_limit.name())) {
                limitKey = StationConfig.live_transfor_ds_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_cq9_limit.name())) {
                limitKey = StationConfig.live_transfor_cq9_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_jdb_limit.name())) {
                limitKey = StationConfig.live_transfor_jdb_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ttg_limit.name())) {
                limitKey = StationConfig.live_transfor_ttg_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_mw_limit.name())) {
                limitKey = StationConfig.live_transfor_mw_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_isb_limit.name())) {
                limitKey = StationConfig.live_transfor_isb_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_all_money.name())) {
                limitKey = StationConfig.live_transfor_all_money;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_m8_limit.name())) {
                limitKey = StationConfig.live_transfor_m8_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ibc_limit.name())) {
                limitKey = StationConfig.live_transfor_ibc_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_m8h_limit.name())) {
                limitKey = StationConfig.live_transfor_m8h_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_bg_limit.name())) {
                limitKey = StationConfig.live_transfor_bg_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_vr_limit.name())) {
                limitKey = StationConfig.live_transfor_vr_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ky_limit.name())) {
                limitKey = StationConfig.live_transfor_ky_limit;
            } else if (StringUtils.equals(conKey, StationConfig.live_transfor_ebet_limit.name())) {
                limitKey = StationConfig.live_transfor_ebet_limit;
            }

            if (limitKey != null) {
                Long staId = saveAbcv.getStationId();
                String key = "s_" + staId + "_k_" + limitKey;
                if (limitKey == StationConfig.live_transfor_all_money) {
                    //是否要取公用余额标志
                    String idsKey = "";
                    if ("on".equals(StationConfigUtil.get(staId, StationConfig.live_transfor_all_limit_onoff))) {
                        List<String> groups = Arrays.asList(StationConfigUtil.get(staId, StationConfig.live_transfor_all_platform).split(";"));
                        for (int i = 0; i < groups.size(); i++) {
                            idsKey = groups.get(i);
                            CacheUtil.delCache(CacheType.REAL_GAME_USER, key + idsKey);
                        }
                    }
                } else {
                    CacheUtil.delCache(CacheType.REAL_GAME_USER, key);
                }
            }

            SysLogUtil.log(desc, LogType.ADMIN_PLATFORM);
        } else {
            SysLogUtil.log("更新" + conf.getName() + "配置项,值为:" + value, LogType.AGENT_SYSTEM);
        }
    }

    @Override
    public void updateConfigValue(String key, String value, Long stationId) {
        if (Validator.isNull(key) || stationId == null) {
            return;
        }
        AgentBaseConfigValue saveAbcv = configValueDao.getConfVal(key, stationId);
        if (saveAbcv == null) {
            return;
        }
        AgentBaseConfig conf = configDao.get(saveAbcv.getConfigId());
        if (conf == null) {
            return;
        }
        Long cStatus = conf.getStatus();
        if (StringUtil.equals(cStatus, AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE)) {
            return;
        }

        saveAbcv.setValue(value);
        configValueDao.updateValue(saveAbcv, conf.getKey());
    }

    @Override
    public Page<AgentBaseConfig> getPageConfig(String name, Long platform, String groupName, Long groupId) {
        return configDao.getPageConfig(name, platform, groupName, groupId);
    }

    @Override
    public AgentBaseConfigValue getConfValByKey(long stationId, String key) {
        if (StringUtil.isEmpty(key)) {
            throw new GenericException("配置键不能为空！");
        }

        AgentBaseConfigValue abcv = configValueDao.getConfVal(key, stationId);
        return abcv;
    }

    @Override
    public AgentBaseConfig getSysConfigByKey(String key) {
        if (StringUtil.isEmpty(key)) {
            throw new GenericException("配置键不能为空！");
        }
        return configDao.getConfig(key);
    }

    @Override
    public Page<AgentBaseConfigGroup> getPageConfigGroup(Long platform, String groupName) {
        return configGroupDao.getPageGroups(platform, groupName);
    }

    @Override
    public void updStatus(long confId, long status) {
        AgentBaseConfig conf = configDao.get(confId);
        if (conf.getStatus() != status) {
            conf.setStatus(status);
            if (StringUtil.equals(status, AgentBaseConfig.AGENT_BASE_CONFIG_DISABLE)) {
                SysLogUtil.log("禁用编号为:" + confId + "配置项", LogType.ADMIN_PLATFORM);
            } else if (StringUtil.equals(status, AgentBaseConfig.AGENT_BASE_CONFIG_ENABLE)) {
                SysLogUtil.log("启用编号为:" + confId + "配置项", LogType.ADMIN_PLATFORM);
            } else if (StringUtil.equals(status, AgentBaseConfig.AGENT_BASE_CONFIG_HIDDEN)) {
                SysLogUtil.log("隐藏编号为:" + confId + "配置项", LogType.ADMIN_PLATFORM);
            }
            configDao.save(conf);
        }
    }

    @Override
    public void delete(long acId) {
        configDao.delete(acId);
        SysLogUtil.log("删除编号为:" + acId + "配置项", LogType.ADMIN_PLATFORM);
    }

    @Override
    public void delGroup(long acId) {
        AgentBaseConfig abcv = new AgentBaseConfig();
        abcv.setGroupId(acId);
        if (configDao.isNotUnique(abcv, "groupId")) {
            throw new GenericException("该组别被其他配置内容使用，不能删除！");
        }
        SysLogUtil.log("删除编号为:" + acId + "配置组别", LogType.ADMIN_PLATFORM);
        configGroupDao.delete(acId);
    }

    @Override
    public void saveConfig(AgentBaseConfig abc) {
        Long id = abc.getId();
        if (StringUtil.isEmpty(abc.getName())) {
            throw new GenericException("配置名称不能为空！");
        }
        if (StringUtil.isEmpty(abc.getType())) {
            throw new GenericException("配置类型不能为空！");
        }
        AgentBaseConfig saveAbc = null;
        if (Validator.isNull(id)) {
            if (configDao.isNotUnique(abc, "name")) {
                throw new GenericException("该配置已存在！");
            }
            saveAbc = abc;
        } else {
            saveAbc = configDao.get(id);
            saveAbc.setName(abc.getName());
            saveAbc.setExpand(abc.getExpand());
            saveAbc.setInitValue(abc.getInitValue());
            saveAbc.setRemark(abc.getRemark());
            saveAbc.setSource(abc.getSource());
            saveAbc.setTitle(abc.getTitle());
            saveAbc.setType(abc.getType());
            saveAbc.setKey(abc.getKey());
            saveAbc.setStatus(abc.getStatus());
            saveAbc.setGroupId(abc.getGroupId());
            saveAbc.setOrderNo(abc.getOrderNo());
        }
        String key = saveAbc.getKey();
        if (configDao.isNotUnique(saveAbc, "key")) {
            throw new GenericException("已存在KEY为：" + key + "的配置项！");
        }
        try {
            StationConfig.valueOf(key);
        } catch (Exception e) {
            throw new GenericException("StationConfig枚举类不存在KEY：" + key);
        }
        configDao.save(saveAbc);
    }

    @Override
    public void saveSysConfigVal(Long id, String value) {
        configDao.updateSysConfigValue(id, value);
    }

    @Override
    public void saveConfigGroup(AgentBaseConfigGroup cg) {
        Long id = cg.getId();
        AgentBaseConfigGroup saveCg = null;

        if (Validator.isNull(id)) {
            if (configGroupDao.isNotUnique(cg, "name")) {
                throw new GenericException("该配置组别已存在！");
            }
            if (Validator.isNull(cg.getPlatform())) {
                throw new GenericException("参数异常！");
            }
            saveCg = cg;
            SysLogUtil.log("新增配置组别:" + cg.getName(), LogType.ADMIN_PLATFORM);
        } else {
            saveCg = configGroupDao.get(id);
            saveCg.setName(cg.getName());
            saveCg.setOrderNo(cg.getOrderNo());
            SysLogUtil.log("更新配置组别:" + cg.getName(), LogType.ADMIN_PLATFORM);
        }
        configGroupDao.save(saveCg);

    }

    @Override
    public List<AgentBaseConfigValue> getConfValsByStationId(long stationId) {
        return configValueDao.getConfVals(stationId, AgentBaseConfigValue.AGENT_BASE_VALUE_ENABLE);
    }

    @Override
    public void saveAgentConfVals(Long stationId, String ids) {
        if (stationId == null || stationId <= 0) {
            throw new GenericException("请选择站点！");
        }
        if (StringUtils.isEmpty(ids)) {
            throw new GenericException("没有提交修改数据！");
        }
        String[] idStrs = ids.split(",");
        Set<Long> idSet = new HashSet<>();
        for (String id : idStrs) {
            idSet.add(NumberUtils.toLong(id, 0));
        }
        // 取出所有租户基本配置项，组装成一个MAP
        List<AgentBaseConfig> abcs = configDao.getConfigAll(null, AgentBaseConfig.PLATFORM_AGENT);
        List<AgentBaseConfigValue> curLst = configValueDao.getConfVals(stationId, null);
        // 新增配置值集合
        List<AgentBaseConfigValue> inLst = new ArrayList<>();
        // 修改配置值集合
        List<AgentBaseConfigValue> updLst = new ArrayList<>();

        Map<Long, AgentBaseConfigValue> abcvMap = new HashMap<>();
        if (curLst != null) {
            for (AgentBaseConfigValue abcv : curLst) {
                abcvMap.put(abcv.getConfigId(), abcv);
            }
        }
        AgentBaseConfigValue abcv = null;
        for (AgentBaseConfig abc : abcs) {
            abcv = abcvMap.get(abc.getId());
            if (idSet.contains(abc.getId())) {
                if (abcv == null) {
                    abcv = new AgentBaseConfigValue();
                    abcv.setConfigId(abc.getId());
                    abcv.setStationId(stationId);
                    abcv.setStatus(AgentBaseConfigValue.AGENT_BASE_VALUE_ENABLE);
                    abcv.setValue(abc.getInitValue());
                    inLst.add(abcv);
                } else {
                    abcv.setStatus(AgentBaseConfigValue.AGENT_BASE_VALUE_ENABLE);
                    updLst.add(abcv);
                }
            } else {
                if (abcv != null) {
                    abcv.setStatus(AgentBaseConfigValue.AGENT_BASE_VALUE_DISABLE);
                    updLst.add(abcv);
                }
            }
        }
        configValueDao.batchSaveAndUpdate(inLst, updLst, stationId);
        SysLogUtil.log("更新站点编号为:" + stationId + "的配置项", LogType.ADMIN_PLATFORM);
        String key = "s_" + stationId + "_all_setting_configs";
        CacheUtil.delCache(CacheType.AGENT_SETTING, key);
    }

    @Override
    public String getSettingValueByKey(String key, Long stationId) {
        try {
            List<Map> stationConfigs = getStationConfigs(stationId, true, true);
            boolean need = false;
            for (Map map : stationConfigs) {
                if (key.equalsIgnoreCase(map.get("key").toString())) {
                    need = true;
                    break;
                }
            }

            if (!need) {
                return null;
            }

            AgentBaseConfigValue v = configValueDao.getConfVal(key, stationId);
            if (v != null)
                return v.getValue();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public boolean isDuLiCaiPiao(Long stationId) {
        return isOn(StationConfig.onoff_du_li_cai_piao.name(), stationId);
    }

    @Override
    public boolean onoff_multi_agent(Long stationId) {
        return isOn(StationConfig.onoff_multi_agent.name(), stationId);
    }

    @Override
    public boolean dianZiYouYiHadOpened(Long stationId) {
        return isOn(StationConfig.onoff_dian_zi_you_yi.name(), stationId);
    }

    @Override
    public boolean zhenRenYuLeHadOpened(Long stationId) {
        return isOn(StationConfig.onoff_zhen_ren_yu_le.name(), stationId);
    }

    @Override
    public boolean isOn(String key, Long stationId) {
        return StringUtils.equals(getSettingValueByKey(key, stationId), BusinessConstant.SWITCH_ON);
    }

    @Override
    public boolean updateSysConfigValue(StationConfig key, String value) {
        return configDao.updateSysConfigValue(key.name(), value);
    }

    @Override
    public void updateRealMoneyConfig() {
        configValueDao.updateRealMoneyConfig();
        platformTransferDao.updateRealInOutMoney();
        CacheUtil.flashDB(CacheType.AGENT_SETTING);
    }

    @Override
    public String[] getLotteryGroupSort(Long stationId) {
        String lotteryGroupSort = getSettingValueByKey(StationConfig.lottery_group_sort.name(), stationId);
        if (StringUtils.isEmpty(lotteryGroupSort)) {
            lotteryGroupSort = "1,2,3,5,6,7";
        }
        String version = StationConfigUtil.get(stationId, StationConfig.lottery_template_name);
        if (StringUtils.equals("v2", version) || StringUtils.equals("v2_1", version)) {
            if (lotteryGroupSort.indexOf("8") < 0) {
                lotteryGroupSort = lotteryGroupSort + ",8";    //8为快乐十分
            }
            if (lotteryGroupSort.indexOf("9") < 0) {
                lotteryGroupSort = lotteryGroupSort + ",9";    //9 PC蛋蛋
            }
        }
        if (StringUtils.equals("v5", version)) {
            if (lotteryGroupSort.indexOf("8") < 0) {
                //判断快乐十分下是否有彩种，没有彩种不显示分组 ???????
                List<BcLottery> list = lotteryDao.findListByViewGroup(stationId, 8, 5);
                if (list != null && list.size() > 0) {

                    for (BcLottery bcLottery : list) {
                        if (bcLottery.getModelStatus().equals(BcLottery.STATUS_ENABLE) && bcLottery.getStatus().equals(BcLottery.STATUS_ENABLE)) {
                            lotteryGroupSort = lotteryGroupSort + ",8";
                            break;
                        }
                    }


                }
            }
            if (lotteryGroupSort.indexOf("9") < 0) {
                //判断快乐十分下是否有彩种，没有彩种不显示分组
                List<BcLottery> list = lotteryDao.findListByViewGroup(stationId, 9, 5);
				/*if(list!=null && list.size()>0){
					lotteryGroupSort = lotteryGroupSort + ",9";
				}*/
                if (list != null && list.size() > 0) {
                    for (BcLottery bcLottery : list) {
                        if (bcLottery.getModelStatus().equals(BcLottery.STATUS_ENABLE) && bcLottery.getStatus().equals(BcLottery.STATUS_ENABLE)) {
                            lotteryGroupSort = lotteryGroupSort + ",9";
                            break;
                        }
                    }
                }
            }
        }
        return lotteryGroupSort.split(",");
    }

    @Override
    public List<AgentBaseConfigValue> getAllConfValsByStationId(long stationId) {
        return configValueDao.getConfVals(stationId, 0L);
    }

    @Override
    public void deleteAgentConfigValByStationId(Long stationId) {
        configValueDao.deleteAgentConfigValByStationId(stationId);

    }
}
