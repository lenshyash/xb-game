package com.game.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.game.dao.QzGroupDao;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysAccountInfoDao;
import com.game.model.AdminDictionary;
import com.game.model.Qzgroup;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.service.AdminDictionaryService;
import com.game.service.QzGroupService;
import com.game.service.SysAccountService;
import com.game.util.StationUtil;

@Service
public class QzgroupServiceImpl implements QzGroupService{
	
	@Autowired
	private QzGroupDao qzGroupDao;
	@Autowired
	private SysAccountService sysAccountService;
	@Autowired
	SysAccountDailyMoneyDao dailyMoneyDao;
	@Autowired
	private SysAccountInfoDao sysAccountInfoDao;
	
	@Override
	public Page<Map> getGroupPage(Long stationId,String groupName) {
		Page<Map> page = qzGroupDao.getPage(stationId,groupName);
		return page;
	}

	@Override
	public Qzgroup getOneGroup(String groupName,Long stationId) {
		if(StringUtil.isEmpty(groupName)) {
			throw new GenericException("群组名称不能为空");
		}
		Qzgroup group = qzGroupDao.getGroupByName(groupName,stationId);
		return group;
	}

	@Override
	public void moveAccountToGroup(String groupName,String account) {
		Long stationId = StationUtil.getStationId();
		SysAccount sysAccount = sysAccountService.findOneByAccountAndStationId(account, stationId);
		String parents = sysAccount.getParents();
		if (StringUtil.isEmpty(parents)) {
			parents = ",";
		}
		parents = parents+sysAccount.getId() + ",";
		List<SysAccount> allDownMember = sysAccountService.getAllDownMember(account, parents, stationId);
		qzGroupDao.batchUpdateGroup(groupName, stationId, allDownMember);
	}
	
	public void save(Qzgroup qzgroup) {
		qzGroupDao.save(qzgroup);
	}
	
	public void closeGroup(Long stationId,String groupName) {
		//设置群组状态为2 关闭
		qzGroupDao.closeGroup(stationId, groupName);
		//找出所有属于这个群的用户info
		List<SysAccountInfo> list = sysAccountInfoDao.getAccountInfoByGroupName(groupName, stationId);
		//batch更新群组名称为null
		qzGroupDao.batchCloseGroupSysaccountInfo(stationId, list);
		
	}
}


