package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.dao.MemberLevelBaseDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysStationDao;
import com.game.dao.SysStationDomainDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.MemberLevelBase;
import com.game.model.SysAccount;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.service.ImService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.AESUtil;
import com.game.util.AuthTokenUtil;
import com.game.util.MemberLevelUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;
import com.game.util.WebUtil;

@Service
public class ImServiceImpl implements ImService {

	public static String securitySalt = "lks234#$(gfdg>r4w";

	@Autowired
	private SysStationDomainDao stationDomainDao;

	@Autowired
	private SysStationDao stationDao;

	@Autowired
	private SysAccountDao sysAccountDao;

	@Autowired
	private MemberLevelBaseDao levelBaseDao;
	
	private String getRequestToken() {
		String chatOn = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		//获取当前用户适用聊天室token
		String token = getApplicableToken();
		Map<String,String> chatMap = new HashMap<>();
		if (StringUtil.isNotEmpty(token)) {
			chatMap = AuthTokenUtil.authChatToken(token);
		}
		/*
		Long stationId = StationUtil.getStationId();
		String platToken = StationUtil.getDomainPlatToken();
		Map<String,String> chatMap = new HashMap<>();
		if (StringUtil.isEmpty(platToken)) {
			chatMap = AuthTokenUtil.getChatToken();
			String newToken = chatMap.get("newToken");
			if (StringUtil.isEmpty(newToken)) {
				throw new GenericException("获取聊天室token失败");
			}
			stationDomainDao.updateDomainChatToken(stationId, platToken);
			platToken = newToken;
		}
		String[] temp = platToken.split(",");
		if (temp != null && temp.length < 2) {
			throw new GenericException("聊天室token格式错误");
		}*/
		// 组装用户token
		MemberLevelBase level = UserUtil.getMemberLevel();
		MemberLevelBase defaultLevel = MemberLevelUtil.getDefaultLevel();
		Map<String, Object> map = new HashMap<>();
		map.put("token",token);
		map.put("platformId",chatMap.get("platformId"));
		map.put("name", UserUtil.getUserAccount());
		map.put("level", 0);
		 //没有用户等级采用站点默认等级
	     if(level !=null && StringUtil.isNotEmpty(level.getLevelName())){
	    	 map.put("grade", level.getLevelName());
	     }else{
	    	 map.put("grade", defaultLevel.getLevelName());
	     }
		map.put("accountId", UserUtil.getUserId());
		map.put("accountType", UserUtil.getType());
		String json = JSONObject.toJSONString(map);
		return AESUtil.encrypt(json, securitySalt);
	}
	private Map<String,String> getChatInfo() {
		String chatOn = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_chat);
		if (chatOn == null || !"on".equals(chatOn) || !checkChatOpen()) {
			throw new GenericException("聊天室功能未开启");
		}
		//获取当前用户适用聊天室token
		String token = getApplicableToken();
		//判断token缓存是否过期
		String redisKey = Contants.CHAT_MEMBER_TOKEN +":::"+token;
		String chatMapStr  = RedisAPI.getCache(redisKey);
		Map<String,String> chatMap = new HashMap<>();
		if(StringUtil.isEmpty(chatMapStr)){
			if (StringUtil.isNotEmpty(token)) {
				chatMap = AuthTokenUtil.authChatToken(token);
				RedisAPI.addCache(redisKey, JSONObject.toJSONString(chatMap), Contants.CHAT_MEMBER_TOKEN_TIMEOUT);
			}
		}else{
			chatMap = JSONObject.parseObject(chatMapStr, chatMap.getClass());
		}
		
		// 组装用户token
//		MemberLevelBase level = UserUtil.getMemberLevel()
		SysAccount acc = sysAccountDao.get(UserUtil.getUserId());
		MemberLevelBase level = MemberLevelUtil.getLevel(acc.getLevelGroup());
		MemberLevelBase defaultLevel = MemberLevelUtil.getDefaultLevel();
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		map.put("platformId",chatMap.get("platformId"));
		map.put("name", UserUtil.getUserAccount());
		map.put("level", 0);
		 //没有用户等级采用站点默认等级
	     if(level !=null && StringUtil.isNotEmpty(level.getLevelName())){
	    	 map.put("grade", level.getLevelName());
	     }else{
	    	 map.put("grade", defaultLevel.getLevelName());
	     }
		map.put("accountId", UserUtil.getUserId());
		map.put("accountType", UserUtil.getType());
		String json = JSONObject.toJSONString(map);
		Map<String,String> resultMap = new HashMap<>();
		resultMap.put("token",AESUtil.encrypt(json, securitySalt));
		resultMap.put("domain", chatMap.get("domain"));
		return resultMap;
	}
	@Override
	public String login() {
		Map<String,String> map = getChatInfo();
		String domain = map.get("domain");
		String token = map.get("token");
		if(StringUtil.isEmpty(domain)){
			domain = StationConfigUtil.getSys(StationConfig.sys_chat_url);
		}else{
			domain = "https://"+domain;
		}
		
		List<SysStationDomain> list =  stationDomainDao.getListByStationType(StationUtil.getStationId(), SysStationDomain.TYPE_CHAT);
		if(list!=null && list.size()>0){
			domain = list.get(0).getDomain();
	    }
		String loginUrl =domain+ "?token=" + token;
		if (StationUtil.isMobileStation()) {
			loginUrl = domain+ "/iphoneChat/index.html?token=" + token;
		}
		return loginUrl;
	}

	public static void main(String[] args) {
		String securitySalt = "lks234#$(gfdg>r4w";
		String veriy = "H4+tlJOduF4Io6U3Sr9yJbMAc9yQ44vaOms6lgOIRwpSElZALYM6DGy3AmgqfXBn5Mq9iVyX6drA3+7bbJGJVHPhgi4bcQvA+zLnMRYjo9rP0NFPW/cIbNg4u+fbgOlzzqUQah5ZxqdUj3LiM1zvbmACKfPB8txUluHyirA71YeuVHMmMOFXcT35kdbsanih";
		System.out.println(AESUtil.decrypt(veriy, securitySalt));
	}

	@Override
	public void shareOrder(final SysAccount user, final JSONObject obj) {
		if (user == null || obj == null) {
			throw new GenericException("非常请求！");
		}
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
			throw new GenericException("此账号不能使用此功能");
		}
		JSONArray arr = obj.getJSONArray("data");
		if(arr == null) {
			throw new GenericException("非常请求！");
		}
		BigDecimal shareMoney = StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.share_order_money_limit));
		for (int i = 0; i < arr.size(); i++) {
			if(arr.getJSONObject(i).getBigDecimal("money").compareTo(shareMoney) !=1) {
				throw new GenericException("超过分享注单限制金额");
			}
		}
		//获取推送对象参数
		String chatId = obj.getString("chatId");
		final String groupId = obj.getString("groupId");
		final String toUserId = obj.getString("toUserId");
		obj.remove("chatId");
		obj.remove("groupId");
		obj.remove("toUserId");
		if(StringUtil.isEmpty(chatId) && StringUtil.isEmpty(groupId) && StringUtil.isEmpty(toUserId)){
			chatId = user.getImChatId();
		}
		final String imChatId = chatId;
		final String mSessionId = obj.getString("mSessionId");
		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("params", obj.toJSONString()));
				ps.add(new BasicNameValuePair("imUserId", user.getImUserId()));
				ps.add(new BasicNameValuePair("imChatId", imChatId));
				ps.add(new BasicNameValuePair("groupId", groupId));
				ps.add(new BasicNameValuePair("toUserId", toUserId));
				ps.add(new BasicNameValuePair("mSessionId", mSessionId));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_im_url) + "/user/operation/shareOrder?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
	}

	@Override
	public void shareReport( final SysAccount user, final JSONObject obj) {
		if (user == null || obj == null) {
			throw new GenericException("非常请求！");
		}
		
		if(StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)){
			throw new GenericException("此账号不能使用此功能");
		}
		//获取推送对象参数
		String chatId = obj.getString("chatId");
		final String groupId = obj.getString("groupId");
		final String toUserId = obj.getString("toUserId");
		obj.remove("chatId");
		obj.remove("groupId");
		obj.remove("toUserId");
		if(StringUtil.isEmpty(chatId) && StringUtil.isEmpty(groupId) && StringUtil.isEmpty(toUserId)){
			chatId = user.getImChatId();
		}
		final String imChatId = chatId;
		final String mSessionId = obj.getString("mSessionId");
		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};

			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("params", obj.toJSONString()));
				ps.add(new BasicNameValuePair("imUserId", user.getImUserId()));
				ps.add(new BasicNameValuePair("imChatId", imChatId));
				ps.add(new BasicNameValuePair("groupId", groupId));
				ps.add(new BasicNameValuePair("toUserId", toUserId));
				ps.add(new BasicNameValuePair("mSessionId", mSessionId));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_im_url) + "/user/operation/shareReport?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
	}

	@Override
	public Map generateChatRoom(Long id, String platformNo, String domain,Long chatNum) {
		Map<String,String> map = new HashMap<>();
		String chatOn = StationConfigUtil.get(id, StationConfig.onoff_im_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		SysStation station = stationDao.get(id);
		String url = WebUtil.getDomain(domain);
		map = AuthTokenUtil.createNewImChannel(station,platformNo,url,chatNum);
		map.put("domain", domain+"/admin/agent/index");
		return map;
	}
	

	@Override
	public Map initImUser(Long stationId, String account, Long accountId,String ip,String levelName,String imChatId) {
		final String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		final String fstationId = stationId+"";
		final String flevelName = levelName;
		final String faccount = account+"";
		final String faccountId=  accountId +"";
		final String fip = ip;
		final String fChatId=imChatId;
		Map<String,String> map = new HashMap<>();
		String chatOn = StationConfigUtil.get(stationId, StationConfig.onoff_im_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};
			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("authToken", token));
				ps.add(new BasicNameValuePair("stationId", fstationId));
				ps.add(new BasicNameValuePair("account", faccount));
				ps.add(new BasicNameValuePair("accountId",faccountId));
				ps.add(new BasicNameValuePair("levelName", flevelName));
				ps.add(new BasicNameValuePair("ip", fip));
				ps.add(new BasicNameValuePair("imChatId", fChatId));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_im_url) + "/api/system/createUser?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
		JSONObject data = json.getJSONObject("data");
		map.put("level", data.getString("level"));
		map.put("imUserId", data.getString("imUserId"));
		map.put("connIp",data.getString("connIp"));
		map.put("connPort",data.getString("connPort"));
		map.put("head",data.getString("head"));
		map.put("nickName",data.getString("nickName"));
		//保存im用户ID
		sysAccountDao.updateImUserId(accountId,data.getString("imUserId"));
		return map;
	}
	@Override
	public Map getChatConfig(Long stationId,String imChatId,String levelName,String imUserId) {
		final String token = StationConfigUtil.getSys(StationConfig.sys_real_center_token);
		final String fstationId = stationId+"";
		final String fChatId=imChatId;
		final String fLevelName = levelName;
		final String fimUserId = imUserId;
		Map<String,String> map = new HashMap<>();
		String chatOn = StationConfigUtil.get(stationId, StationConfig.onoff_im_chat);
		if (chatOn == null || !"on".equals(chatOn)) {
			throw new GenericException("聊天室功能未开启");
		}
		String content = new RequestProxy() {
			public List<Header> getHeaders() {
				List<Header> headerList = new ArrayList<Header>();
				headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
				headerList.add(new BasicHeader("Connection", "close")); // 短链接
				return headerList;
			};
			public List<NameValuePair> getParameters() {
				List<NameValuePair> ps = new ArrayList<>();
				ps.add(new BasicNameValuePair("authToken", token));
				ps.add(new BasicNameValuePair("stationId", fstationId));
				ps.add(new BasicNameValuePair("imChatId", fChatId));
				ps.add(new BasicNameValuePair("levelName", fLevelName));
				ps.add(new BasicNameValuePair("imUserId", fimUserId));
				return ps;
			};
		}.doRequest(StationConfigUtil.getSys(StationConfig.sys_im_url) + "/api/system/getChatConfig?", PostType.POST,
				true);
		JSONObject json = JSONObject.parseObject(content);
		if(!StringUtil.equals(json.getInteger("code"), 1)) {
			throw new GenericException(json.getString("msg"));
		}
		JSONObject data = json.getJSONObject("data");
		map.put("level", data.getString("level"));
		map.put("connIp", data.getString("connIp"));
		map.put("connPort", data.getString("connPort"));
		map.put("head",data.getString("head"));
		map.put("nickName",data.getString("nickName"));
		return map;
	}
	/**
	 * 获取用户聊天室 （优先规则 1用户指定聊天室 2域名绑定聊天室 3 站点默认聊天室）
	 * @return
	 */
	@Override
	public String getApplicableToken() {
		Long stationId = StationUtil.getStationId();
		String token = "";
		Map<String,Object> account = sysAccountDao.getAccountById(UserUtil.getUserId(),stationId);
		if(StringUtil.isNotEmpty((String)account.get("imChatId"))){
			token =  account.get("imChatId").toString();
		}
		String defaultToken = StationConfigUtil.get(stationId, StationConfig.basic_info_new_im_chat_id);
		if(StringUtil.isEmpty((String)account.get("chatToken"))
				&& StringUtil.isNotEmpty(defaultToken)){
			token = defaultToken;
		}	
		if(StringUtil.isEmpty(token)){
			String msg = StationConfigUtil.get(StationConfig.chat_notconfigured);
			if (StringUtil.isNotEmpty(msg)) {
				throw new GenericException(msg);
			}
			throw new GenericException("暂未配置聊天室，请联系客服人员");
		}
		return token;
		
	}

	public boolean checkChatOpen(){
		boolean flag=true;
		StringBuffer requestUrl = UserUtil.getRequest().getRequestURL();
		String domainUrl = WebUtil.getDomain(requestUrl.toString());
		SysStationDomain domain = stationDomainDao.getDomainByFolder(domainUrl, StationUtil.getDomainFolder(), StationUtil.getStationId());
		if(domain!=null 
				&& domain.getIsChatSwitch()!=null 
				&& domain.getIsChatSwitch().intValue()==SysStationDomain.STATUS_DISABLE){
			flag = false;
		}
		return flag;
	}
	
}
