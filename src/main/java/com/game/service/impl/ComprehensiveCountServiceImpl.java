package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.dao.ComprehensiveCountDao;
import com.game.dao.MnyMoneyDao;
import com.game.dao.SysStationDao;
import com.game.dao.platform.ProxyDailyBettingStatisticsDao;
import com.game.model.AgentComprehensiveCount;
import com.game.model.SysStation;
import com.game.service.ComprehensiveCountService;
import com.game.service.SysAccountService;
import com.game.util.DateUtil;

@Repository
public class ComprehensiveCountServiceImpl implements ComprehensiveCountService {
	private Logger logger = Logger.getLogger(ComprehensiveCountServiceImpl.class);
	@Autowired
	private ComprehensiveCountDao ccDao;
	@Autowired
	private SysStationDao stDao;

	@Autowired
	private SysAccountService accountService;
	@Autowired
	private ProxyDailyBettingStatisticsDao pdbsDao;
	@Autowired
	private MnyMoneyDao moneyDao;

	@Override
	public synchronized Map getComprehensiveCountMap(Long stationId) {
		String json = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, "ComprehensiveCount_" + stationId);
		if (json != null) {
			return JSON.parseObject(json, Map.class);
		}
		Calendar now = Calendar.getInstance();
		// 昨日统计记录
		now.add(Calendar.DAY_OF_MONTH, -1);
		Map map_yes = ccDao.getComprehensiveCount(stationId, now.getTime());
		// 得到当日统计记录
		now.add(Calendar.DAY_OF_MONTH, 1);
		Map map_tdy = ccDao.getComprehensiveCount(stationId, now.getTime());
		if (map_tdy == null) {
			map_tdy = new HashMap();
		}
		dataHandler(map_tdy, map_yes);
		initYearReport(map_tdy, stationId, now);
		map_tdy.put("allBalance", moneyDao.getStationMoney(stationId,0L));
		CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, "ComprehensiveCount_" + stationId, map_tdy);
		return map_tdy;
	}

	/**
	 * 
	 * @param map_tdy
	 */
	private void initYearReport(Map map_tdy, Long stationId, Calendar now) {
		// 截至当前时间的12个月统计集合
		now.add(Calendar.DAY_OF_MONTH, 1);
		Date endTime = now.getTime();
		now.add(Calendar.MONTH, -11);
		now.set(Calendar.DAY_OF_MONTH, 1);
		Date startTime = now.getTime();

		map_tdy.put("allyear", ccDao.getMonthReports(stationId, startTime, endTime));
	}

	/**
	 * 数据组装
	 * 
	 * @param map_tdy
	 * @param map_yes
	 */
	private void dataHandler(Map map_tdy, Map map_yes) {
		BigDecimal betNumYes = BigDecimal.ZERO;// 昨日投注
		BigDecimal winNumYes = BigDecimal.ZERO;// 昨日中奖
		BigDecimal agentRebateYes = BigDecimal.ZERO;// 昨日返点
		BigDecimal memberRebateYes = BigDecimal.ZERO;// 昨日反水
		BigDecimal betNumTdy = BigDecimal.ZERO;// 今日投注
		BigDecimal winNumTdy = BigDecimal.ZERO;// 今日中奖
		BigDecimal agentRebateTdy = BigDecimal.ZERO;// 今日返点
		BigDecimal memberRebateTdy = BigDecimal.ZERO;// 今日反水
		BigDecimal bunkoTdy = BigDecimal.ZERO;// 今日盈亏
		BigDecimal bunkoYes = BigDecimal.ZERO;// 昨天盈亏
		Long registerAccountYes = 0l;
		if (map_tdy != null) {
			betNumTdy = StringUtil.toBigDecimal(map_tdy.get("betAmount"));
			winNumTdy = StringUtil.toBigDecimal(map_tdy.get("awardAmount"));
			agentRebateTdy = StringUtil.toBigDecimal(map_tdy.get("agentRebate"));
			memberRebateTdy = StringUtil.toBigDecimal(map_tdy.get("memberRebate"));
			bunkoTdy = sum(betNumTdy, getNegate(winNumTdy), getNegate(agentRebateTdy), getNegate(memberRebateTdy));
			if (map_yes != null) {
				betNumYes = StringUtil.toBigDecimal(map_yes.get("betAmount"));
				winNumYes = StringUtil.toBigDecimal(map_yes.get("awardAmount"));
				agentRebateYes = StringUtil.toBigDecimal(map_yes.get("agentRebate"));
				memberRebateYes = StringUtil.toBigDecimal(map_yes.get("memberRebate"));

				bunkoYes = sum(betNumYes, getNegate(winNumYes), getNegate(agentRebateYes), getNegate(memberRebateYes));
				registerAccountYes = StringUtil.toLong(map_yes.get("registerAccount"));
			}
			map_tdy.put("bunkoTdy", bunkoTdy.floatValue());
			map_tdy.put("betAmountYes", betNumYes.floatValue());
			map_tdy.put("winAmountYes", winNumYes.floatValue());
			map_tdy.put("bunkoYes", bunkoYes.floatValue());
			map_tdy.put("registerAccountYes", registerAccountYes);
			map_tdy.put("online", accountService.getOnlineCount());// 在线人数
			map_tdy.put("accountCount", getLongFromMap(map_tdy, "agentCount") + getLongFromMap(map_tdy, "memberCount"));
		}
	}

	/**
	 * 统计
	 * 
	 * @return
	 */
	private BigDecimal sum(BigDecimal... secg) {
		BigDecimal total = BigDecimal.ZERO;

		if (secg != null && secg.length > 0) {
			for (int i = 0; i < secg.length; i++) {
				total = total.add(secg[i]);
			}
		}
		return total;
	}

	private BigDecimal getNegate(BigDecimal val) {
		if (val == null) {
			return BigDecimal.ZERO;
		}
		return val.negate();
	}

	/**
	 * 定时统计站点的统计概况
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	@Override
	public void executeStatistics() {
		List<SysStation> sts = stDao.findByStatus(SysStation.STATUS_ENABLE);
		if (sts == null || sts.size() == 0) {
			return;
		}

		Calendar now = Calendar.getInstance();
//		Date endTime = null;
		Date startTime = null;
		if (now.get(Calendar.HOUR_OF_DAY) == 0 && now.get(Calendar.MINUTE) < 5) {
			// 如果当前时间为凌晨0点5分之前，则先统计昨天的数据
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
//			endTime = now.getTime();
			now.add(Calendar.DAY_OF_MONTH, -1);
			startTime = now.getTime();
		} else {
			// 统计从凌晨00:00:00开始到当前的数据
//			endTime = now.getTime();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			startTime = now.getTime();
		}
		Map<Long, Map> accountCountMaps = accountService.countStation(DateUtil.toDateStr(startTime), null);
//		Map<Long, Map> moneyRecordMaps = ccDao.statisticMoneyRecordForOneDay(startTime, endTime);
		// Map<Long, Map> thirdRecordMaps =
		// ccDao.statisticThirdForOneDay(startTime, endTime);
//		Map<Long, Map> proxyDayMaps = pdbsDao.statisticForOneDay(startTime);
		for (SysStation station : sts) {
//			doStatistics(station, startTime, accountCountMaps.get(station.getId()), moneyRecordMaps.get(station.getId()), proxyDayMaps.get(station.getId()));
			doStatistics(station, startTime, accountCountMaps.get(station.getId()),null,null);
		}
	}

	/**
	 * 计算每个站点的概况
	 * 
	 * @param station
	 * @param startTime
	 * @param accountCountMap
	 * @param moneyRecordMap
	 * @param thirdRecordMap
	 */
	private void doStatistics(SysStation station, Date startTime, Map accountCountMap, Map moneyRecordMap, Map proxyDayMaps) {
		try {
			long stationId = station.getId();
			AgentComprehensiveCount acc = ccDao.queryModel(stationId, startTime);
			if (acc == null) {
				acc = new AgentComprehensiveCount();
				acc.setStationId(stationId);
				acc.setCountDate(startTime);
			}
			acc.setAgentCount(getLongFromMap(accountCountMap, "agentCount"));
			acc.setMemberCount(getLongFromMap(accountCountMap, "memberCount"));
			acc.setRegisterAccount(getLongFromMap(accountCountMap, "registerCount"));
//			acc.setBetAmount(getBigDecimalFromMap(moneyRecordMap, "betAmount").add(getBigDecimalFromMap(proxyDayMaps, "realBetAmount")).add(getBigDecimalFromMap(proxyDayMaps, "egameBetAmount")).abs());
//			acc.setAwardAmount(getBigDecimalFromMap(moneyRecordMap, "awardAmount").add(getBigDecimalFromMap(proxyDayMaps, "realAwardAmount")).add(getBigDecimalFromMap(proxyDayMaps, "egameAwardAmount")).abs());
//			acc.setDepositAmount(getBigDecimalFromMap(moneyRecordMap, "depositAmount").abs());
//			acc.setWithdrawAmount(getBigDecimalFromMap(moneyRecordMap, "withdrawAmount").abs());
//			acc.setAgentRebate(getBigDecimalFromMap(moneyRecordMap, "agentRebate").abs());
//			acc.setMemberRebate(getBigDecimalFromMap(moneyRecordMap, "memberRebate").abs());
//			acc.setLotteryProfit(getBigDecimalFromMap(moneyRecordMap, "lotteryProfit").negate());
//			acc.setSportProfit(getBigDecimalFromMap(moneyRecordMap, "sportProfit").negate());
//			// 真人电子盈亏需要扣除反水返点
//			acc.setRealGameProfit(getBigDecimalFromMap(proxyDayMaps, "realBetAmount").subtract(getBigDecimalFromMap(proxyDayMaps, "realAwardAmount")).subtract(getBigDecimalFromMap(moneyRecordMap, "realGameProfit")));
//			acc.setElectronProfit(getBigDecimalFromMap(proxyDayMaps, "egameBetAmount").subtract(getBigDecimalFromMap(proxyDayMaps, "egameAwardAmount")).subtract(getBigDecimalFromMap(moneyRecordMap, "electronProfit")));
//			acc.setMarkSixProfit(getBigDecimalFromMap(moneyRecordMap, "markSixProfit").negate());
//			acc.setDepositGiftAcmount(getBigDecimalFromMap(moneyRecordMap, "depositGiftAcmount").abs());
			if (acc.getId() == null) {
				ccDao.insert(acc);
			} else {
				ccDao.update(acc);
			}
		} catch (Exception e) {
			logger.error("站点统计概况发生错误" + station.getFloder(), e);
		}
	}

	private BigDecimal getBigDecimalFromMap(Map map, String key) {
		BigDecimal b = BigDecimal.ZERO;
		if (map != null && map.get(key) != null) {
			try {
				b = (BigDecimal) map.get(key);
			} catch (Exception e) {
				try {
					b = new BigDecimal((String) map.get(key));
				} catch (Exception e1) {
				}
			}
		}
		return b;
	}

	public long getLongFromMap(Map map, String key) {
		long l = 0;
		if (map != null && map.get(key) != null) {
			try {
				l = (Long) map.get(key);
			} catch (Exception e) {
				try {
					l = (Integer) map.get(key);
				} catch (Exception e1) {
				}
			}
		}
		return l;
	}


	private void doStats(List<SysStation> sts, Date startTime, Date endTime) {
		Map<Long, Map> accountCountMaps = accountService.countStation(DateUtil.toDateStr(startTime), endTime);
//		Map<Long, Map> moneyRecordMaps = ccDao.statisticMoneyRecordForOneDay(startTime, endTime);
		// Map<Long, Map> thirdRecordMaps =
		// ccDao.statisticThirdForOneDay(startTime, endTime);
//		Map<Long, Map> proxyDayMaps = pdbsDao.statisticForOneDay(startTime);
		for (SysStation station : sts) {
			if (station.getStatus() == SysStation.STATUS_ENABLE && station.getCreateDatetime().getTime() < endTime.getTime()) {
//				doStatistics(station, startTime, accountCountMaps.get(station.getId()), moneyRecordMaps.get(station.getId()), proxyDayMaps.get(station.getId()));
				doStatistics(station, startTime, accountCountMaps.get(station.getId()), null,null);
			}
		}
	}

	@Override
	public void statsStations(Long stationId, String startDate, String endDate) {
		List<SysStation> sts = null;
		if (stationId == null) {
			sts = stDao.findByStatus(SysStation.STATUS_ENABLE);
			if (sts == null || sts.size() == 0) {
				return;
			}

		} else {
			SysStation st = stDao.findOneById(stationId);
			if (st == null || !st.getStatus().equals(SysStation.STATUS_ENABLE)) {
				return;
			}
			sts = new ArrayList<>();
			sts.add(st);
		}
		Date start = null;
		Date end = null;
		if (!StringUtil.isEmpty(startDate)) {
			start = DateUtil.toDate(startDate);
		}
		if (start == null) {// 如果start为空，则使用域名注册时间为最早的日期
			start = new Date();
			for (SysStation station : sts) {
				if (station.getCreateDatetime().getTime() < start.getTime()) {
					start = station.getCreateDatetime();
				}
			}
		}
		if (!StringUtil.isEmpty(endDate)) {
			end = DateUtil.toDate(endDate);
		}
		if (end == null) {
			Calendar now = Calendar.getInstance();
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);
			end = now.getTime();
		}
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Date startTime = null;
		Date endTime = null;
		long endTimeMillis = end.getTime();
		while (c.getTimeInMillis() <= endTimeMillis) {
			startTime = c.getTime();
			c.add(Calendar.DAY_OF_MONTH, 1);
			endTime = c.getTime();
			doStats(sts, startTime, endTime);
		}

	}
}
