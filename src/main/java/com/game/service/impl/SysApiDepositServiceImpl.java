package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.MnyBetNumberDao;
import com.game.dao.SysApiDepositDao;
import com.game.model.BetNumRecord;
import com.game.model.SysApiDeposit;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyMoneyService;
import com.game.service.SysApiDepositService;
import com.game.util.IPSeeker;

@Service
public class SysApiDepositServiceImpl implements SysApiDepositService {
	@Autowired
	private SysApiDepositDao sysApiDepositDao;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;

	@Override
	public Page<SysApiDeposit> getPage(String account, Date beginDate, Date endDate, String orderId, Long stationId) {
		Page<SysApiDeposit> p = sysApiDepositDao.getPage(account, beginDate, endDate, orderId, stationId);
		if (p != null && p.getList() != null) {
			IPSeeker ips = IPSeeker.getInstance();
			String ipArea;
			for (SysApiDeposit d : p.getList()) {
				if (StringUtils.isNotEmpty(d.getIp())) {
					ipArea = ips.getCountry(d.getIp());
					if (ipArea.equals("IANA")) {
						ipArea = "";
					}
					d.setIpStr(ipArea);
				}
			}
		}
		return p;
	}

	@Override
	public void deposit(Long stationId, String account, Long accountId, String ip, BigDecimal money,
			BigDecimal betMultiple, String orderId, String remark) {
		SysApiDeposit sad = new SysApiDeposit();
		sad.setStationId(stationId);
		sad.setAccount(account);
		sad.setAccountId(accountId);
		sad.setCreateDatetime(new Date());
		sad.setIp(ip);
		sad.setMoney(money);
		sad.setOrderId(orderId);
		sad.setRemark(remark);
		sad.setBetMultiple(betMultiple);
		sysApiDepositDao.insert(sad);
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setAccountId(accountId);
		moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_BY_SYS_API);
		moneyVo.setMoney(money);
		moneyVo.setRemark(remark);
		moneyVo.setFee(new BigDecimal(0));
		moneyService.updMnyAndRecord(moneyVo);
		if (betMultiple.compareTo(BigDecimal.ZERO) > 0) {
			// 更新会员提款的判断所需要的数据
			mnyBetNumberDao.updateDrawCheck(accountId, stationId, money.multiply(betMultiple),
					BetNumRecord.TYPE_API_DEPOSIT, "系统接口存款", true);
		}
	}
}
