package com.game.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MnyBetNumberDao;
import com.game.dao.MnyExchangeConfigDao;
import com.game.model.BetNumRecord;
import com.game.model.MnyComRecord;
import com.game.model.MnyExchangeConfig;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.dictionary.ScoreRecordType;
import com.game.model.vo.ExchangeConfigVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.model.vo.MnyScoreVo;
import com.game.service.MnyExchangeService;
import com.game.service.MnyMoneyService;
import com.game.service.MnyScoreService;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class MnyExchangeServiceImpl implements MnyExchangeService {

	@Autowired
	MnyExchangeConfigDao exchangeConfigDao;

	@Autowired
	MnyScoreService scoreService;

	@Autowired
	MnyMoneyService moneyService;
	
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Override
	public Page getConfigPage(ExchangeConfigVo ecvo) {
		return exchangeConfigDao.getExchangeConfigPage(ecvo);
	}

	@Override
	public void updStatus(Long id, Long status) {
		MnyExchangeConfig saveConf = exchangeConfigDao.get(id);
		if (saveConf == null) {
			throw new GenericException("配置不存在");
		}

		if (StringUtil.equals(status, MnyExchangeConfig.STATUS_ENABLED)) {
			MnyExchangeConfig checkConf = new MnyExchangeConfig();
			checkConf.setStatus(status);
			checkConf.setType(saveConf.getType());
			checkConf.setStationId(saveConf.getStationId());

			if (exchangeConfigDao.isNotUnique(checkConf, "status,type,stationId")) {
				throw new GenericException("同类配置只能启用一个！");
			}
		}

		if (!StringUtil.equals(saveConf.getStatus(), status)) {
			saveConf.setStatus(status);
			exchangeConfigDao.save(saveConf);
		}

	}

	@Override
	public void saveConfig(MnyExchangeConfig config) {
		MnyExchangeConfig saveConf = null;
		String name = config.getName();
		Long type = config.getType();
		BigDecimal denominator = config.getDenominator();
		BigDecimal numerator = config.getNumerator();
		BigDecimal maxVal = config.getMaxVal();
		BigDecimal minVal = config.getMinVal();
		BigDecimal betNum = config.getBetNum();
		if (StringUtil.isEmpty(name)) {
			throw new GenericException("配置名称不能为空！");
		}

		if (Validator.isNull(type)) {
			throw new GenericException("配置类型不能为空！");
		}
		if (Validator.isNull(denominator)) {
			throw new GenericException("比例分母不能为空！");
		}
		if (Validator.isNull(numerator)) {
			throw new GenericException("比例分子不能为空！");
		}
		if (Validator.isNull(maxVal)) {
			throw new GenericException("单次最大值不能为空！");
		}
		if (Validator.isNull(minVal)) {
			throw new GenericException("单次最小值不能为空！");
		}

		if (config.getId() != null) {
			saveConf = exchangeConfigDao.get(config.getId());
			saveConf.setName(name);
			saveConf.setDenominator(denominator);
			saveConf.setMaxVal(maxVal);
			saveConf.setMinVal(minVal);
			saveConf.setNumerator(numerator);
			saveConf.setType(type);
			saveConf.setBetNum(betNum);
		} else {
			saveConf = config;
			saveConf.setStationId(StationUtil.getStationId());
		}
		saveConf.setStatus(MnyExchangeConfig.STATUS_DISABLED);
		exchangeConfigDao.save(saveConf);
	}

	@Override
	public void delete(Long id) {
		MnyExchangeConfig mecf = exchangeConfigDao.get(id);
		if (!StringUtil.equals(mecf.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (exchangeConfigDao.delete(id) < 1) {
			throw new GenericException("删除失败！");
		}
	}

	@Override
	public List<MnyExchangeConfig> getProgressConfig(ExchangeConfigVo ecvo) {
		return exchangeConfigDao.getProgressConfig(ecvo);
	}

	@Override
	public void exchange(Long typeId, BigDecimal amount) {
		MnyExchangeConfig config = exchangeConfigDao.get(typeId);
		if (config == null || !StringUtil.equals(config.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (amount == null || amount.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("输入额度有误！");
		}

		ScoreRecordType scoreRecordType = null;
		MoneyRecordType moneyRecordType = null;
		BigDecimal score = BigDecimal.ZERO;
		BigDecimal money = BigDecimal.ZERO;
		BigDecimal numerator = config.getNumerator();
		BigDecimal denominator = config.getDenominator();
		BigDecimal maxVal = config.getMaxVal();
		BigDecimal minVal = config.getMinVal();
		BigDecimal betNum = config.getBetNum();
		if (maxVal == null || maxVal.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("兑换配置有误，请联系站点客服！");
		}

		if (minVal == null || minVal.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("兑换配置有误，请联系站点客服！");
		}

		if (numerator == null || numerator.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("兑换配置有误，请联系站点客服！");
		}

		if (denominator == null || denominator.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("兑换配置有误，请联系站点客服！");
		}

		if (amount.compareTo(minVal) == -1) {
			throw new GenericException("兑换额度必须大等于" + minVal.doubleValue());
		}

		if (amount.compareTo(maxVal) == 1) {
			throw new GenericException("兑换额度必须小等于" + maxVal.doubleValue());
		}

		if (StringUtil.equals(config.getType(), MnyExchangeConfig.TYPE_MNY_TO_SCORE)) {
			scoreRecordType = ScoreRecordType.EXCHANGE_MNY_TO_SCORE;
			moneyRecordType = MoneyRecordType.EXCHANGE_MNY_TO_SCORE;
			money = amount;
			score = money.multiply(denominator).divide(numerator).setScale(0, BigDecimal.ROUND_FLOOR);
		} else if (StringUtil.equals(config.getType(), MnyExchangeConfig.TYPE_SCORE_TO_MNY)) {
			scoreRecordType = ScoreRecordType.EXCHANGE_SCORE_TO_MNY;
			moneyRecordType = MoneyRecordType.EXCHANGE_SCORE_TO_MNY;
			
			
			score = amount;
			money = score.multiply(denominator).divide(numerator).setScale(2, BigDecimal.ROUND_HALF_UP);
			//判断是否存在打码量
			if(betNum != null && betNum.compareTo(BigDecimal.ZERO) == 1){
				BigDecimal relbetNum = money.multiply(betNum).setScale(2, BigDecimal.ROUND_HALF_UP);
				String remark = "积分兑换现金叠加打码量";
				//叠加打码量
				if (!mnyBetNumberDao.addDrawNeed(UserUtil.getUserId(), relbetNum, BetNumRecord.TYPE_Points_for_cash, remark, null)) {
					throw new GenericException("操作数额错误！");
				}
			}
		} else {
			throw new GenericException("未知兑换类型，请联系站点客服！");
		}

		Long curUId = UserUtil.getUserId();
		MnyScoreVo scoreVo = new MnyScoreVo();
		scoreVo.setScore(score);
		scoreVo.setAccountId(curUId);
		scoreVo.setScoreRecordType(scoreRecordType);
		scoreVo.setRemark(scoreRecordType.getName());

		MnyMoneyVo mmvo = new MnyMoneyVo();
		mmvo.setMoney(money);
		mmvo.setAccountId(curUId);
		mmvo.setMoneyRecordType(moneyRecordType);
		mmvo.setRemark(moneyRecordType.getName());

		scoreService.updScoreAndRecord(scoreVo);
		moneyService.updMnyAndRecord(mmvo);
	}
	
	@Override
	public MnyExchangeConfig getOne(Long id, Long stationId) {
		if (id != null) {
			MnyExchangeConfig it = exchangeConfigDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

}