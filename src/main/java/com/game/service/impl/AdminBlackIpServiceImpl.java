package com.game.service.impl;

import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.game.cache.redis.RedisAPI;
import com.game.dao.platform.AdminBlackIpDao;
import com.game.model.platform.AdminBlackIp;
import com.game.service.AdminblackipService;

@Service
public class AdminBlackIpServiceImpl implements AdminblackipService {
	
	@Autowired
	private AdminBlackIpDao adminBlackIpDao;
	
	private static final int ADMIN_BLACK_IP_REDIS_DB = 12;
	private static final int REDIS_EXPIRE_SENCOND = 1800; //先缓存30分钟
	
	@Override
	public Page page(String ip) {
		Page<AdminBlackIp> page = adminBlackIpDao.getPage(ip);
		return page;
	}
	
	public void add(AdminBlackIp adminBlackIp) {
//		boolean notUnique = adminBlackIpDao.isNotUnique(adminBlackIp, "ip");
//		if(notUnique) {
//			throw new GenericException("IP [" + adminBlackIp.getIp() + "]已经存在");
//		}
		adminBlackIpDao.addOne(adminBlackIp);
		
		//添加也要删除 要不然获取到的还是null
//		String key = "admin_black_ip_"+adminBlackIp.getIp();
		String key = "admin_black_ip_list";
		
		RedisAPI.delCache(ADMIN_BLACK_IP_REDIS_DB, key);
	}
	
	public void delete(Long id) {
		
		String ip = adminBlackIpDao.get(id).getIp();
//		String key = "admin_black_ip_"+ip;
		String key = "admin_black_ip_list";
		
		adminBlackIpDao.delete(id);
		RedisAPI.delCache(ADMIN_BLACK_IP_REDIS_DB, key);
	}
	
	/**
	 * 换list方式去做
	 */
 	public AdminBlackIp getOne(String ip) {
		String key = "admin_black_ip_"+ip;
 		
		String cache = RedisAPI.getCache(key, ADMIN_BLACK_IP_REDIS_DB);
		
		if(cache!=null) {
			AdminBlackIp adminBlackIp = JSON.parseObject(cache, AdminBlackIp.class);
			return adminBlackIp;
		}
		
		AdminBlackIp one = adminBlackIpDao.getOne(ip);
		RedisAPI.addCache(key, JSON.toJSONString(one), REDIS_EXPIRE_SENCOND, ADMIN_BLACK_IP_REDIS_DB);
		
		return one;
		
	}

	@Override
	public List<AdminBlackIp> getList() {
		String key = "admin_black_ip_list";
		
		String cache = RedisAPI.getCache(key);
		if(cache!=null) {
			List<AdminBlackIp> array = JSONArray.parseArray(cache, AdminBlackIp.class);
			return array;
		}
		
		List<AdminBlackIp> list = adminBlackIpDao.getList();
		RedisAPI.addCache(key, JSON.toJSONString(list), REDIS_EXPIRE_SENCOND, ADMIN_BLACK_IP_REDIS_DB);
		
		return list;
	}
	
}
