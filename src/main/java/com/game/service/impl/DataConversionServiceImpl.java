package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MnyMoneyDao;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysStationDao;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.SysStation;
import com.game.service.DataConversionService;
import com.game.util.DateUtil;

@Repository
public class DataConversionServiceImpl implements DataConversionService {
	private Logger logger = Logger.getLogger(DataConversionServiceImpl.class);
	@Autowired
	private SysAccountDailyMoneyDao dailyMoneyDao;
	@Autowired
	private SysStationDao stationDao;
	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private MnyMoneyDao moneyDao;

	@Override
	public void startMergeDailyData(Date start, Date end, Long stationId) {
		logger.info("开始合并每日金额" + DateUtil.toDateStr(start) + " - " + DateUtil.toDateStr(end) + " stationId=" + stationId);
		if (stationId != null) {
			doMergeDailyDateForStation(start, end, stationId);
		} else {
			List<SysStation> sts = stationDao.findByStatus(SysStation.STATUS_ENABLE);
			for (SysStation st : sts) {
				if (st == null || st.getId() == null) {
					continue;
				}
				doMergeDailyDateForStation(start, end, st.getId());
			}
		}
		logger.info("合并每日金额完成" + DateUtil.toDateStr(start) + " - " + DateUtil.toDateStr(end) + " stationId=" + stationId);
	}

	private void doMergeDailyDateForStation(Date start, Date end, Long stationId) {
		if (start == null || end == null || stationId == null) {
			logger.info("合并每日金额时，参数为空");
			return;
		}
		List<Date[]> list = getDayList(start, end);// 日期列表
		List<SysAccountDailyMoney> dmList = null;
		Map<String, SysAccountDailyMoney> dmRebateMap = dailyMoneyDao.getAccountDailyMoneyFromMemberRollbackRecord(stationId, list.get(0)[0], list.get(list.size()-1)[1]);
		Map<Long, SysAccount> accMap = new HashMap<>();
		Map<Long, BigDecimal> balanceMap = new HashMap<>();
		for (Date[] ds : list) {
			dmList = dailyMoneyDao.getAccountDailyMoneyFromMnyMoneyRecord(stationId, ds[0], ds[1]);
			logger.info("开始合并数据 时间范围" + DateUtil.toDateStr(ds[0]) + " - " + DateUtil.toDateStr(ds[1]) + " stationId=" + stationId + " 条数=" + dmList.size());
			saveDailyDate(dmList, ds[0], stationId, dmRebateMap, accMap, balanceMap);
		}
		/**
		 * 保存只有反水记录的用户日统计
		 */
		saveOnlyRebateInfo(dmRebateMap, accMap, balanceMap);
	}
	/**
	 * 保存只有反水记录的用户日统计
	 */
	private void saveOnlyRebateInfo(Map<String, SysAccountDailyMoney> dmRebateMap, Map<Long, SysAccount> accMap, Map<Long, BigDecimal> balanceMap) {
		if (!dmRebateMap.isEmpty()) {
			SysAccountDailyMoney old = null;
			SysAccount acc = null;
			for (SysAccountDailyMoney dm : dmRebateMap.values()) {
				old = dailyMoneyDao.findOneByAccountIdAndStatDate(dm.getAccountId(), dm.getStatDate());
				if (old == null) {
					acc = accMap.get(dm.getAccountId());
					if (acc == null) {
						acc = accountDao.getFromCache(dm.getAccountId());
						balanceMap.put(dm.getAccountId(), moneyDao.getBalance(dm.getAccountId()));
					}
					if (acc == null) {
						continue;
					}
					dm.setAccount(acc.getAccount());
					dm.setAccountType(acc.getAccountType());
					dm.setParents(acc.getParents());
					dm.setStationId(acc.getStationId());
					dm.setBalance(balanceMap.get(dm.getAccountId()));
					dm.setAgentId(acc.getAgentId());
					dm.setAgentName(acc.getAgentName());
					dailyMoneyDao.insert(dm);
				} else {
					dailyMoneyDao.updateAmount(dm);
				}
			}
		}
	}

	private void saveDailyDate(List<SysAccountDailyMoney> dmList, Date statDate, Long stationId, Map<String, SysAccountDailyMoney> dmRebateMap, Map<Long, SysAccount> accMap, Map<Long, BigDecimal> balanceMap) {
		if (dmList == null) {
			return;
		}
		SysAccount acc = null;
		SysAccountDailyMoney old = null;
		SysAccountDailyMoney rebate = null;
		String key = null;
		for (SysAccountDailyMoney dm : dmList) {
			old = dailyMoneyDao.findOneByAccountIdAndStatDate(dm.getAccountId(), statDate);
			key = dm.getAccountId() + "_" + DateUtil.toDateStr(statDate);
			rebate = dmRebateMap.get(key);
			if (rebate != null) {
				dmRebateMap.remove(key);
				dm.setLotteryRebateAmount(rebate.getLotteryRebateAmount());
				dm.setRealRebateAmount(rebate.getRealRebateAmount());
				dm.setEgameRebateAmount(rebate.getEgameBetAmount());
				dm.setSportsRebateAmount(rebate.getSportsRebateAmount());
				dm.setMarkSixRebateAmount(rebate.getMarkSixRebateAmount());
			}
			if (old == null) {
				acc = accMap.get(dm.getAccountId());
				if (acc == null) {
					acc = accountDao.getFromCache(dm.getAccountId());
					balanceMap.put(dm.getAccountId(), moneyDao.getBalance(dm.getAccountId()));
				}
				if (acc == null) {
					continue;
				}
				dm.setStatDate(statDate);
				dm.setAccount(acc.getAccount());
				dm.setAccountType(acc.getAccountType());
				dm.setParents(acc.getParents());
				dm.setStationId(acc.getStationId());
				dm.setBalance(balanceMap.get(dm.getAccountId()));
				dm.setAgentId(acc.getAgentId());
				dm.setAgentName(acc.getAgentName());
				dailyMoneyDao.insert(dm);
			} else {
				dm.setStatDate(statDate);
				dailyMoneyDao.updateAmount(dm);
			}
		}
	}

	private List<Date[]> getDayList(Date startTime, Date endTime) {
		Calendar start = Calendar.getInstance();
		start.setTime(startTime);
		start.set(Calendar.HOUR_OF_DAY, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		startTime = start.getTime();

		start.add(Calendar.DAY_OF_MONTH, 1);

		long endl = endTime.getTime();
		List<Date[]> list = new ArrayList<>();
		Date[] ds = null;
		while (start.getTimeInMillis() <= endl) {
			ds = new Date[2];
			ds[0] = startTime;
			ds[1] = start.getTime();
			startTime = start.getTime();
			start.add(Calendar.DAY_OF_MONTH, 1);
			list.add(ds);
		}
		ds = new Date[2];
		ds[0] = startTime;
		ds[1] = start.getTime();
		list.add(ds);
		return list;
	}

}
