package com.game.service.impl;

import java.util.Date;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.ProxyDailyBettingStatisticsDao;
import com.game.dao.platform.ProxyManualRebateRecordDao;
import com.game.model.platform.ProxyDailyBettingStatistics;
import com.game.service.ProxyManualRebateRecordService;
import com.game.util.DateUtil;

@Service
public class ProxyManualRebateRecordServiceImpl implements ProxyManualRebateRecordService{

	@Autowired
	private ProxyManualRebateRecordDao pmrrDao;
	@Autowired
	private ProxyDailyBettingStatisticsDao pmbsDao;
	
	@Override
	public Page page(String startTime, String endTime, String account, Long stationId) {
		if (org.apache.commons.lang3.StringUtils.isEmpty(startTime) && org.apache.commons.lang3.StringUtils.isEmpty(endTime)) {
			startTime = DateUtil.toDateStr(new Date());
			endTime = DateUtil.toDateStr(new Date());
		}
		return pmrrDao.page(DateUtil.toDate(startTime), DateUtil.toDate(endTime), account, stationId);
	}

}
