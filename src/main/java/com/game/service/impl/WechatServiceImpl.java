package com.game.service.impl;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.dao.MemberWechatDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountInfoDao;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.MemberWechat;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.AccountVo;
import com.game.service.SysAccountService;
import com.game.service.WechatService;
import com.game.util.MD5Util;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class WechatServiceImpl implements WechatService{

	@Autowired
	private MemberWechatDao wechatDao;
	
	@Autowired
	private SysAccountDao accountDao;
	
	@Autowired
	private SysAccountInfoDao accountInfoDao;
	
	@Autowired
	private SysAccountService accountService;
	
	@Override
	public int callback(String code) {
		String appId = StringUtil.trim2Empty(StationConfigUtil.get(StationConfig.wechat_appid));
		String secret = StringUtil.trim2Empty(StationConfigUtil.get(StationConfig.wechat_appsecret));
		String content = doGet("https://api.weixin.qq.com/sns/oauth2/access_token?appid="+appId+"&secret="+secret+"&code="+code+"&grant_type=authorization_code");
		JSONObject json = JSONObject.parseObject(content);
		if(json.containsKey("errcode")){
			throw new GenericException("step1:["+json.getIntValue("errcode")+"]"+json.getString("errmsg"));
		}

		content = doGet("https://api.weixin.qq.com/sns/userinfo?access_token="+json.getString("access_token")+"&openid="+json.getString("openid")+"&lang=zh_CN");
		json = JSONObject.parseObject(content);
		if(json.containsKey("errcode")){
			throw new GenericException("step2:["+json.getIntValue("errcode")+"]"+json.getString("errmsg"));
		}
		String openId = json.getString("openid");
		MemberWechat wechat = wechatDao.getWechatByOpenId(openId, StationUtil.getStationId());
		if(wechat != null){//自动登陆
			long accountId = wechat.getAccountId();
			SysAccount account = accountDao.get(accountId);
			SysAccountInfo accountInfo = accountInfoDao.get(accountId);
			accountService.updateAccountLoginStatus(account,accountInfo, SystemConfig.SESSION_MEMBER_KEY, "微信授权登陆成功！");
			return 1;
		}
		String sid = UserUtil.getSession().getId();
		RedisAPI.addCache(sid+":wechat_auth", content, 60 * 30);
		//json.toJavaObject(MemberWechat.class);
		return 2;
	}
	
	private String doGet(String url){
		String content = new RequestProxy() {
		}.doRequest(url, PostType.GET);
		return content;
	}

	@Override
	public void addAccount(String account, String pwd) {
		String sid = UserUtil.getSession().getId();
		String json = RedisAPI.getCache(sid+":wechat_auth");
		if(StringUtil.isEmpty(json)){
			throw new GenericException("授权信息已经过期");
		}
		MemberWechat mw = JSONObject.parseObject(json, MemberWechat.class);
		AccountVo regMemVo = new AccountVo();
		regMemVo.setPwd(pwd);
		regMemVo.setRpwd(pwd);
		regMemVo.setAccount(account);
		regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
		regMemVo.setAgentId(StationUtil.getStationAgentId());
		regMemVo.setStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
		SysAccount member  = accountService.saveAccount(regMemVo);
		SysAccountInfo memberInfo = accountInfoDao.get(member.getId());
		mw.setAccountId(member.getId());
		mw.setAccountStatus(MemberWechat.ACCOUNT_STATUS_BINDED);
		mw.setStationId(StationUtil.getStationId());
		wechatDao.insert(mw);
		accountService.updateAccountLoginStatus(member, memberInfo,SystemConfig.SESSION_MEMBER_KEY, "微信授权登陆成功！");
	}

	@Override
	public void bindAccount(String account, String pwd) {
		String sid = UserUtil.getSession().getId();
		String json = RedisAPI.getCache(sid+":wechat_auth");
		if(StringUtil.isEmpty(json)){
			throw new GenericException("授权信息已经过期");
		}
		MemberWechat wc = JSONObject.parseObject(json, MemberWechat.class);
		wc.setStationId(StationUtil.getStationId());
		wc.setAccountStatus(MemberWechat.ACCOUNT_STATUS_BINDED);
		
		SysAccount member = accountDao.getLoginAccount(account.trim().toLowerCase(), SysAccount.ACCOUNT_PLATFORM_MEMBER);
		if (member == null) {
			throw new GenericException("账户或者密码错误");
		}
		SysAccountInfo memberInfo = accountInfoDao.get(member.getId());
		String userPwd = member.getPassword();
	
		String newPwd = MD5Util.getMD5String(account, pwd);
		String oldPwd = MD5Util.getOldMD5String(account, pwd);
		if (!newPwd.equals(StringUtil.toUpperCase(userPwd)) && !oldPwd.equals(StringUtil.toUpperCase(userPwd))) {
			throw new GenericException("账户或者密码错误");
		}
		
		wc.setAccountId(member.getId());
		
		if(wechatDao.isNotUnique(wc, "stationId,accountId")){
			throw new GenericException("账号["+account+"]已被其他微信号绑定");
		}
		
		if(wechatDao.isNotUnique(wc, "stationId,openid")){
			throw new GenericException("当前微信已绑定过其他账号");
		}
		wechatDao.insert(wc);
		accountService.updateAccountLoginStatus(member, memberInfo,SystemConfig.SESSION_MEMBER_KEY, "微信授权登陆成功！");
	}
}
