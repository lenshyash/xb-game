package com.game.service.impl;

import java.util.List;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.dao.SysCacheDao;
import com.game.dao.SysStationDao;
import com.game.model.SysCache;
import com.game.model.SysStation;
import com.game.service.SysCacheService;

@Repository
public class SysCacheServiceImpl implements SysCacheService {

	@Autowired
	private SysCacheDao cacheDao;

	@Autowired
	private SysStationDao stDao;

	@Override
	public Page<SysCache> getPage() {
		return cacheDao.getPage();
	}

	@Override
	public void saveCache(SysCache cache) {
		Long cacheId = cache.getId();
		if (cacheId != null) {// 修改
			SysCache oldCache = cacheDao.get(cacheId);
			oldCache.setDb(cache.getDb());
			oldCache.setDataType(cache.getDataType());
			oldCache.setKey(cache.getKey());
			oldCache.setRemark(cache.getRemark());
			oldCache.setName(cache.getName());
			oldCache.setTimeout(cache.getTimeout());
			oldCache.setExpression(cache.getExpression());
			cacheDao.save(oldCache);
		} else { // 新增
			cacheDao.save(cache);
		}
	}

	@Override
	public void delCache(long cacheId) {
		cacheDao.delete(cacheId);
	}

	@Override
	public List<SysCache> getAll() {
		return cacheDao.getAll();
	}

	@Override
	public void clearRedisKeys() {
		// 项目启动清除提款和充值的缓存值
		List<SysStation> sts = stDao.findByStatus(SysStation.STATUS_ENABLE);
		if (sts == null || sts.size() == 0) {
			return;
		}
		String comKey = "";
		String drawKey = "";
		Long stId = 0l;
		for (SysStation sysStation : sts) {
			stId = sysStation.getId();
			comKey = CacheUtil.toKey(CacheType.AGENT_STATION_COM_COUNT, stId + "");
			drawKey = CacheUtil.toKey(CacheType.AGENT_STATION_DRAW_COUNT, stId + "");
			RedisAPI.delCache(comKey, drawKey);
		}
	}
}
