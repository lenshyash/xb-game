package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.platform.MemberDepositStrategyDao;
import com.game.model.MemberActiveRecord;
import com.game.model.platform.MemberDepositStrategy;
import com.game.service.MemberDepositStrategyService;

@Repository
public class MemberDepositStrategyServiceImpl implements MemberDepositStrategyService {
	@Autowired
	private MemberDepositStrategyDao memberDepositStrategyDao;

	@Override
	public Page page(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return memberDepositStrategyDao.page(stationId);
	}

	@Override
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		if (id == null || id <= 0 || modelStatus == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		MemberDepositStrategy mds = memberDepositStrategyDao.get(id);
		if (mds == null || !StringUtil.equals(mds.getStationId(), stationId)) {
			throw new GenericException("参数不正确!");
		}
		if (StringUtil.equals(modelStatus, MemberDepositStrategy.STATUS_ENABLED)) {
			mds.setStatus(MemberDepositStrategy.STATUS_ENABLED);
			if (memberDepositStrategyDao.isNotUnique(mds, "stationId,depositType,depositCount,status")) {
				throw new GenericException("已经存在此充值频率的启用策略配置");
			}
		}

		memberDepositStrategyDao.openCloseH(modelStatus, id, stationId);
	}

	@Override
	public void addSave(MemberDepositStrategy alb) {
		if (alb.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		alb.setCreateDatetime(new Date());

		if (alb.getBetMultiple() == null) {
			throw new GenericException("流水倍数不能为空!");
		}

		if (alb.getBetMultiple().compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("流水倍数不能小于零!");
		}

		if (alb.getDepositCount() == null) {
			throw new GenericException("存款频率不能为空!");
		}

		if (StringUtil.equals(alb.getGiveType(), MemberDepositStrategy.GIVE_TYPE_MONEY)) {
			if (alb.getGiveValue() == null) {
				throw new GenericException("赠送额度不能为空!");
			}
			alb.setRollBackRate(0f);
			alb.setUpperLimit(BigDecimal.ZERO);
		} else if (StringUtil.equals(alb.getGiveType(), MemberDepositStrategy.GIVE_TYPE_RATIO)) {
			alb.setGiveValue(BigDecimal.ZERO);
			if (alb.getRollBackRate() == null) {
				throw new GenericException("赠送比例不能为空!");
			}
			if (alb.getUpperLimit() == null) {
				throw new GenericException("赠送上限不能为空!");
			}
		}

		alb.setStatus(MemberDepositStrategy.STATUS_DISABLED);
		memberDepositStrategyDao.save(alb);
	}

	@Override
	public void eidtSave(MemberDepositStrategy alb) {
		if (alb.getId() == null || alb.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		if (alb.getBetMultiple() == null) {
			throw new GenericException("流水倍数不能为空!");
		}

		if (alb.getBetMultiple().compareTo(BigDecimal.ZERO)<0) {
			throw new GenericException("流水倍数不能小于零!");
		}

		if (alb.getDepositCount() == null) {
			throw new GenericException("存款频率不能为空!");
		}

		if (StringUtil.equals(alb.getGiveType(), MemberDepositStrategy.GIVE_TYPE_MONEY)) {
			if (alb.getGiveValue() == null) {
				throw new GenericException("赠送额度不能为空!");
			}
			alb.setRollBackRate(0f);
			alb.setUpperLimit(BigDecimal.ZERO);
		} else if (StringUtil.equals(alb.getGiveType(), MemberDepositStrategy.GIVE_TYPE_RATIO)) {
			alb.setGiveValue(BigDecimal.ZERO);
			if (alb.getRollBackRate() == null) {
				throw new GenericException("赠送比例不能为空!");
			}
			if (alb.getUpperLimit() == null) {
				throw new GenericException("赠送上限不能为空!");
			}
		}

		MemberDepositStrategy albs = memberDepositStrategyDao.get(alb.getId());
		if (!alb.getStationId().equals(albs.getStationId())) {
			throw new GenericException("站点错误！");
		}
		albs.setMemo(alb.getMemo());
		albs.setStartTime(alb.getStartTime());
		albs.setEndTime(alb.getEndTime());
		albs.setStatus(MemberDepositStrategy.STATUS_DISABLED);
		albs.setDepositType(alb.getDepositType());
		albs.setGiveType(alb.getGiveType());
		albs.setBetMultiple(alb.getBetMultiple());
		albs.setValueType(alb.getValueType());
		albs.setDepositCount(alb.getDepositCount());
		albs.setGiveValue(alb.getGiveValue());
		albs.setRollBackRate(alb.getRollBackRate());
		albs.setUpperLimit(alb.getUpperLimit());
		memberDepositStrategyDao.update(albs);
	}

	@Override
	public void delete(Long id, Long stationId) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		memberDepositStrategyDao.delete(id, stationId);
	}

	@Override
	public MemberDepositStrategy getOne(Long id, Long stationId) {
		if (id != null) {
			MemberDepositStrategy ma = memberDepositStrategyDao.get(id);
			if (ma != null && (stationId == null || ma.getStationId().equals(stationId))) {
				return ma;
			}
		}
		return null;
	}
}
