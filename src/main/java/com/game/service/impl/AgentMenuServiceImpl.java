package com.game.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.LogType;
import com.game.dao.AgentMenuDao;
import com.game.dao.AgentMenuGroupDao;
import com.game.model.AgentMenu;
import com.game.model.AgentMenuGroup;
import com.game.model.AgentUserGroup;
import com.game.model.SysAccount;
import com.game.model.vo.AgentMenuNode;
import com.game.model.vo.MenuVo;
import com.game.service.AgentMenuService;
import com.game.service.AgentUserGroupService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentMenuServiceImpl implements AgentMenuService {

	@Autowired
	private AgentMenuDao agentMenuDao;
	@Autowired
	private AgentMenuGroupDao agentMenuGroupDao;
	@Autowired
	private AgentUserGroupService agentUserGroupService;

	@Override
	public List<MenuVo> getNavMenuVo(Long groupId) {
		if (groupId == null) {
			return null;
		}
		Long stationId = StationUtil.getStationId();
		String key = "stationId_" + stationId + "_groupId_" + groupId;
		List<MenuVo> menuVos = null;
		String json = CacheUtil.getCache(CacheType.AGENT_MENU, key);
		if (json != null) {
			menuVos = JSON.parseArray(json, MenuVo.class);
		}
		if (menuVos == null || menuVos.isEmpty()) {
			List<AgentMenu> menu = agentMenuDao.getGroupPermissionMenu(groupId, stationId);
			if (menu != null && !menu.isEmpty()) {
				menuVos = toNavMenuVo(menu);
				CacheUtil.addCache(CacheType.AGENT_MENU, key, menuVos);
			}
		}
		return menuVos;
	}

	private List<MenuVo> toNavMenuVo(List<AgentMenu> menuList) {
		List<MenuVo> list = null;
		List<MenuVo> children = null;
		MenuVo mv = null;
		Map<Long, List<MenuVo>> listMap = new HashMap<>();
		for (AgentMenu am : menuList) {
			list = listMap.get(am.getParentId().longValue());
			if (list == null) {
				list = new ArrayList<>();
				listMap.put(am.getParentId().longValue(), list);
			}
			mv = toMenuVo(am);
			children = listMap.get(am.getId().longValue());
			if (children == null) {
				children = new ArrayList<>();
				listMap.put(am.getId().longValue(), children);
			}
			mv.setChildren(children);
			list.add(mv);
		}
		for (Long l : listMap.keySet()) {
			list = listMap.get(l);
			Collections.sort(list);
		}
		list = listMap.get(0L);
		list.get(0).setSpread(true);
		return list;
	}

	private MenuVo toMenuVo(AgentMenu am) {
		MenuVo v = new MenuVo();
		v.setId(am.getId());
		v.setTitle(am.getName());
		v.setHref(am.getUrl());
		v.setIcon(am.getIcon());
		v.setSortNo(am.getSort());
		v.setSpread(false);
		return v;
	}

	public AgentMenuNode toMenuNode(List<AgentMenu> menu) {
		AgentMenuNode node = new AgentMenuNode();
		if (menu == null)
			return node;
		Map<Long, List<AgentMenuNode>> nodeMap = new HashMap<Long, List<AgentMenuNode>>();
		for (int i = 0; i < menu.size(); i++) {
			AgentMenu m = menu.get(i);
			long parentId = StringUtil.toLong(m.getParentId());
			List<AgentMenuNode> nodes = nodeMap.get(parentId);
			if (nodes == null) {
				nodes = new ArrayList<AgentMenuNode>();
				nodeMap.put(parentId, nodes);
			}
			nodes.add(new AgentMenuNode(m));
		}
		setChildNodes(node, nodeMap, 0L);
		return node;
	}

	/**
	 * 设置子节点
	 * 
	 * @param node
	 * @param nodeMap
	 * @param parentId
	 */
	private void setChildNodes(AgentMenuNode node, Map<Long, List<AgentMenuNode>> nodeMap, Long parentId) {

		List<AgentMenuNode> nodes = nodeMap.get(parentId);
		if (nodes == null || nodes.size() == 0) {
			return;
		}

		node.setNodes(nodes);
		Collections.sort(nodes);
		for (int i = 0; i < nodes.size(); i++) {
			AgentMenuNode menuNode = nodes.get(i);
			AgentMenu curMenu = menuNode.getCurNode();
			Long id = curMenu.getId();
			setChildNodes(menuNode, nodeMap, id);
		}
	}

	public Page<Map> getMenuPage(Long parentId) {
		return agentMenuDao.getMenuPage(parentId);
	}

	@Override
	public List<AgentMenu> getGroupPermissionMenu(Long groupId, Long stationId) {
		return agentMenuDao.getGroupPermissionMenu(groupId, stationId);
	}

	@Override
	public void saveMenu(AgentMenu menu) {
		Long menuId = menu.getId();
		long level = menu.getLevel().longValue();
		if (level == 1L) {
			menu.setModulePath(null);
			menu.setUrl(null);
		} else if (level == 3L) {
			menu.setModulePath(null);
		}
		if (menuId != null) { // 保存
			AgentMenu oldMenu = agentMenuDao.get(menuId);
			oldMenu.setIcon(menu.getIcon());
			oldMenu.setLevel(level);
			oldMenu.setModulePath(menu.getModulePath());
			oldMenu.setUrl(menu.getUrl());
			oldMenu.setRemark(menu.getRemark());
			oldMenu.setName(menu.getName());
			oldMenu.setSort(menu.getSort());
			oldMenu.setStatus(menu.getStatus());
			oldMenu.setType(menu.getType());
			agentMenuDao.save(oldMenu);
			SysLogUtil.log("更新平台站点菜单:" + menu.getName(), LogType.ADMIN_PLATFORM);
		} else {// 新增
			agentMenuDao.save(menu);
			SysLogUtil.log("新增平台站点菜单:" + menu.getName(), LogType.ADMIN_PLATFORM);
		}

	}

	@Override
	public AgentMenuNode getAllLevelThirdMenu() {
		return toMenuNode(agentMenuDao.getMenuByLevel(3));
	}

	@Override
	public AgentMenuNode getStationMenus() {
		Long groupId = 0L;
		SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
			groupId = 0L;
		} else {
			if (acc.getGroupId() == null || acc.getGroupId() <= 0) {
				return toMenuNode(null);
			}
			groupId = acc.getGroupId();
		}
		return toMenuNode(agentMenuDao.getGroupPermissionMenu(groupId, StationUtil.getStationId()));
	}

	@Override
	public List<AgentMenuGroup> getAgentPermission(Long agentId, Long stationId) {
		return agentMenuGroupDao.getAgentPermission(agentId, stationId);
	}

	@Override
	public void saveAgentPermission(Long groupId, Long stationId, String ids) {
		if (stationId == null || stationId <= 0) {
			throw new GenericException("请选择站点！");
		}
		if (StringUtils.isEmpty(ids)) {
			throw new GenericException("没有提交修改数据！");
		}
		if (groupId > 0) {
			AgentUserGroup g = agentUserGroupService.getGroupById(groupId);
			if (g == null) {
				throw new GenericException("请选择组别！");
			}
			if (g.getType() != AgentUserGroup.TYPE_EDITABLE && !UserUtil.isSuperAgent()) {
				throw new GenericException("该组别不能编辑！");
			}
		}
		String[] idStrs = ids.split(",");
		Set<Long> idSet = new HashSet<>();
		for (String id : idStrs) {
			idSet.add(NumberUtils.toLong(id, 0));
		}
		List<AgentMenu> menuList = agentMenuDao.getRootPermissionMenu();
		List<AgentMenuGroup> curLst = agentMenuGroupDao.getAgentPermission(groupId, stationId);
		List<Long> delIds = new ArrayList<Long>();
		List<AgentMenuGroup> ms = new ArrayList<AgentMenuGroup>();

		Map<Long, AgentMenuGroup> curMap = new HashMap<>();
		if (curLst != null) {
			for (AgentMenuGroup ag : curLst) {
				curMap.put(ag.getMenuId(), ag);
			}
		}
		AgentMenuGroup amg = null;
		for (AgentMenu am : menuList) {
			amg = curMap.get(am.getId());
			if (idSet.contains(am.getId())) {
				if (amg == null) {
					amg = new AgentMenuGroup();
					amg.setGroupId(groupId);
					amg.setMenuId(am.getId());
					amg.setStationId(stationId);
					ms.add(amg);
				}
			} else {
				if (amg != null) {
					delIds.add(am.getId());
				}
			}
		}
		agentMenuGroupDao.batchOpe(groupId, stationId, delIds, ms);
		if (StationUtil.isAdminStation()) {
			SysLogUtil.log("更新平台站点编号为:" + stationId + "的权限", LogType.ADMIN_PLATFORM);
		} else if (StationUtil.isAgentStation()) {
			SysLogUtil.log("更新用户组别编号为:" + groupId + "的权限", LogType.AGENT_SYSTEM);
		}
	}
}
