package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.dao.MemberDepositInfoDao;
import com.game.dao.MemberLevelBaseDao;
import com.game.dao.MemberLevelLogDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysMessageDao;
import com.game.dao.SysUserMessageDao;
import com.game.model.MemberDepositInfo;
import com.game.model.MemberLevelBase;
import com.game.model.MemberLevelLog;
import com.game.model.SysAccount;
import com.game.model.SysMessage;
import com.game.model.SysUserMessage;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MemberLevelService;
import com.game.service.MnyMoneyService;
import com.game.util.MemberLevelUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class MemberLevelServiceImpl implements MemberLevelService {

	@Autowired
	private MemberLevelBaseDao levelBaseDao;

	@Autowired
	private MemberLevelLogDao levelLogDao;

	@Autowired
	MemberDepositInfoDao depositInfoDao;

	@Autowired
	SysAccountDao accountDao;

	@Autowired
	SysMessageDao messageDao;

	@Autowired
	SysUserMessageDao userMessageDao;

	@Autowired
	MnyMoneyService mnyMoneyService;

	@Override
	public Page<MemberLevelBase> getLevelPage(Long stationId, Integer status) {
		return levelBaseDao.getPage(stationId, status);
	}

	@Override
	public void saveLevel(MemberLevelBase level) {
		if (level.getId() == null || level.getId() <= 0) {
			if (level.getDepositMoney() == null) {
				throw new GenericException("存款金额不能为空！");
			}
			if (level.getDepositMoney().compareTo(BigDecimal.ZERO) < 0) {
				throw new GenericException("存款金额大于等于0！");
			}
			if (level.getGiftMoney() != null && level.getGiftMoney().compareTo(BigDecimal.ZERO) < 0) {
				throw new GenericException("晋级彩金赠送则必须大于0！");
			}
			if (levelBaseDao.isNotUnique(level, "stationId,depositMoney")) {
				throw new GenericException("已存在此存款金额的等级！");
			}
			level.setCreateDatetime(new Date());
			level.setMemberCount(0);
			level.setStatus(MemberLevelBase.STATUS_ENALBED);
			level.setLevelDefault(MemberLevelBase.LEVEL_DEFALUT_NONE);
			levelBaseDao.insert(level);
			SysLogUtil.log("添加会员等级" + level.getLevelName() + " 金额" + level.getDepositMoney(), LogType.AGENT_SYSTEM);
		} else {
			MemberLevelBase old = levelBaseDao.get(level.getId());
			if (old == null || !Objects.equals(StationUtil.getStationId(), old.getStationId())) {
				throw new GenericException("等级不存在");
			}
			if (old.getLevelDefault() != MemberLevelBase.LEVEL_DEFALUT) {
				if (level.getDepositMoney() == null) {
					throw new GenericException("存款金额不能为空！");
				}
				if (level.getDepositMoney().compareTo(BigDecimal.ZERO) < 0) {
					throw new GenericException("存款金额大于等于0！");
				}
				if (levelBaseDao.isNotUnique(level, "stationId,depositMoney")) {
					throw new GenericException("已存在此存款金额的等级！");
				}
				old.setDepositMoney(level.getDepositMoney());
				old.setGiftMoney(level.getGiftMoney());
			}
			old.setLevelName(level.getLevelName());
			old.setRemark(level.getRemark());
			old.setIcon(level.getIcon());
			levelBaseDao.update(old);
			SysLogUtil.log(
					"修改会员等级" + level.getLevelName() + " 金额" + level.getDepositMoney() + " 晋级彩金" + level.getGiftMoney(),
					LogType.AGENT_SYSTEM);
		}
		CacheUtil.delCache(CacheType.STTAION_MEMBER_LEVEL, "_" + level.getStationId());
	}

	@Override
	public void updLevelStatus(Long id, Integer status) {
		if (id == null || id <= 0) {
			throw new GenericException("非法请求");
		}
		MemberLevelBase level = levelBaseDao.get(id);

		if (level == null || !Objects.equals(StationUtil.getStationId(), level.getStationId())) {
			throw new GenericException("非法请求");
		}

		if (status == MemberLevelBase.STATUS_DISABLED && level.getLevelDefault() == MemberLevelBase.LEVEL_DEFALUT) {
			throw new GenericException("默认等级不能被禁用！");
		}

		if (status == MemberLevelBase.STATUS_DISABLED && level.getMemberCount() > 0) {
			throw new GenericException("该等级成员数量不为0，不能被禁用！");
		}

		if (!StringUtil.equals(level.getStatus(), status)) {
			level.setStatus(status);
			levelBaseDao.save(level);
			String str = (status == MemberLevelBase.STATUS_DISABLED) ? "禁用" : "启用";
			SysLogUtil.log("修改会员等级状态" + level.getLevelName() + " 状态=" + str, LogType.AGENT_SYSTEM);
		}
		CacheUtil.delCache(CacheType.STTAION_MEMBER_LEVEL, "_" + StationUtil.getStationId());
	}

	@Override
	public List<MemberLevelBase> getStationLevels(Long stationId) {
		String key = "_" + stationId;
		String json = CacheUtil.getCache(CacheType.STTAION_MEMBER_LEVEL, key);
		if (StringUtils.isNotEmpty(json)) {
			return JSONArray.parseArray(json, MemberLevelBase.class);
		}
		List<MemberLevelBase> list = levelBaseDao.getStationLevels(stationId);
		if (list != null) {
			CacheUtil.addCache(CacheType.STTAION_MEMBER_LEVEL, key, JSON.toJSONString(list));
		}
		return list;
	}

	@Override
	public void addLevelCount(Long levelId) {
		if (!levelBaseDao.addLevelCount(levelId)) {
			throw new GenericException("操作失败！");
		}
	}

	@Override
	public void checkDepositForLevel(Long accountId) {
		SysAccount user = accountDao.get(accountId);
		if (user == null) {
			throw new GenericException("非法请求！");
		}

		Long levelGroup = user.getLevelGroup();

		MemberLevelBase level = null;
		MemberLevelBase afterLevel = null;
		List<MemberLevelBase> levels = getStationLevels(StationUtil.getStationId());
		MemberDepositInfo mdInfo = depositInfoDao.get(accountId);
		BigDecimal depostTotal = mdInfo.getDepositTotal();
		for (MemberLevelBase memberLevelBase : levels) {
			if (StringUtil.equals(levelGroup, memberLevelBase.getId())) {
				level = memberLevelBase;
			}

			if (depostTotal.compareTo(memberLevelBase.getDepositMoney()) != -1) {
				afterLevel = memberLevelBase;
			}
		}
		if (afterLevel == null || afterLevel.getDepositMoney() == null) {
			return;
		}
		// 升级后金额小于当前金额不升级。
		if (afterLevel.getDepositMoney().compareTo(level.getDepositMoney()) < 1) {
			return;
		}

		Long levelId = afterLevel.getId();
		if (!StringUtil.equals(levelId, levelGroup)) {
			accountDao.updateLevelGroup(accountId, levelId);
			addLevelCount(levelId);
			subLevelCount(levelGroup);
			MemberLevelLog log = new MemberLevelLog();
			String afterName = afterLevel.getLevelName();
			String giftDesc = "";
			BigDecimal giftMoney = afterLevel.getGiftMoney();
			if (giftMoney != null && giftMoney.compareTo(BigDecimal.ZERO) == 1) {
				giftDesc = ",获得该等级的晋级彩金：" + giftMoney.floatValue() + "元";
				MnyMoneyVo mvo = new MnyMoneyVo();
				mvo.setAccountId(accountId);
				mvo.setMoney(giftMoney);
				mvo.setMoneyRecordType(MoneyRecordType.LEVEL_UP_GIFT);
				mvo.setRemark("晋级到" + afterName + "获得晋级彩金");
				mnyMoneyService.updMnyAndRecord(mvo);
			}
			String content = "尊敬的会员:" + user.getAccount() + ",您累计充值：" + depostTotal.floatValue() + ",达到" + afterName
					+ "升级要求,从" + level.getLevelName() + "升级到" + afterName + giftDesc;
			Long stationId = user.getStationId();
			Date curDate = new Date();
			log.setAccountId(accountId);
			log.setCreateDatetime(curDate);
			log.setStationId(stationId);
			log.setContent(content);
			levelLogDao.save(log);
			SysMessage message = new SysMessage();
			SysUserMessage userMessage = new SysUserMessage();
			message.setMessage(content + ",感谢您对本站一直以来的支持！");
			message.setTitle("恭喜，您已经升级成为" + afterName + "!");
			message.setStationId(stationId);
			message.setCreateDatetime(curDate);
			message.setType(SysMessage.TYPE_DEFAULT);
			messageDao.save(message);

			userMessage.setAccountId(accountId);
			userMessage.setAccount(user.getAccount());
			userMessage.setMessageId(message.getId());
			userMessage.setStatus(SysUserMessage.STATUS_UNREAD);
			userMessageDao.save(userMessage);
			// 更改当前SESSION里的等级ID
			// HttpSession session = SysUtil.getSession();
			// SysAccount member = (SysAccount)SysUtil.getCurrentUser();
			// member.setLevel(levelId);
			// session.setAttribute(SystemConfig.SESSION_MEMBER_KEY, member);
			RedisAPI.delCache(Contants.AGENT_MESSAGE_COUNT + accountId);
			RedisAPI.delCache(Contants.MEMBER_LEVEL_INFO_MEMBER_ID + accountId);
		}
	}

	@Override
	public void subLevelCount(Long levelId) {
		if (levelId == null) {
			return;
		}
		if (!levelBaseDao.subLevelCount(levelId)) {
			throw new GenericException("操作失败！");
		}
	}

	@Override
	public void levelMove(Long curId, Long nextId) {

		if (StringUtil.isEmpty(curId) || StringUtil.isEmpty(nextId)) {
			throw new GenericException("非法请求！");
		}

		if (StringUtil.equals(curId, nextId)) {
			return;
		}

		Long stationId = StationUtil.getStationId();

		MemberLevelBase curLevel = levelBaseDao.get(curId);
		if (curLevel == null || !StringUtil.equals(stationId, curLevel.getStationId())) {
			throw new GenericException("非法请求！");
		}
		MemberLevelBase nextLevel = levelBaseDao.get(nextId);

		if (nextLevel == null || !StringUtil.equals(stationId, nextLevel.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (StringUtil.equals(nextLevel.getStatus(), MemberLevelBase.STATUS_DISABLED)) {
			throw new GenericException("不能转到已经被禁用的等级!");
		}

		accountDao.levelChange(curId, nextId, stationId);
		SysLogUtil.log("从等级 " + curLevel.getLevelName() + " 迁移会员到等级 " + nextLevel.getLevelName(), LogType.AGENT_SYSTEM);
	}

	@Override
	public void levelDel(Long id) {

		if (StringUtil.isEmpty(id)) {
			throw new GenericException("非法请求！");
		}

		Long stationId = StationUtil.getStationId();

		MemberLevelBase curLevel = levelBaseDao.get(id);
		if (curLevel == null || !Objects.equals(stationId, curLevel.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (curLevel.getMemberCount() > 0) {
			throw new GenericException("成员数量不为0，不能删除，请先转移成员！");
		}

		if (curLevel.getLevelDefault() == MemberLevelBase.LEVEL_DEFALUT) {
			throw new GenericException("默认等级不能删除！");
		}

		levelBaseDao.delete(id);
		CacheUtil.delCache(CacheType.STTAION_MEMBER_LEVEL, "_" + curLevel.getStationId());
	}

	@Override
	public MemberLevelBase getOne(Long id, Long stationId) {
		if (id != null) {
			MemberLevelBase it = levelBaseDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

	@Override
	public void reStatUserNum() {
		accountDao.levelChange(0L, 0L, StationUtil.getStationId());
	}

	@Override
	public void saveDefaultLevel(MemberLevelBase level) {
		levelBaseDao.insert(level);
	}

	@Override
	public JSONObject getCurrentLevelJson() {
		if (!UserUtil.isLogin()) {
			return null;
		}
		Long accountId = UserUtil.getUserId();
		String key = Contants.MEMBER_LEVEL_INFO_MEMBER_ID + accountId;
		JSONObject info = new JSONObject();
		String value = RedisAPI.getCache(key);
		if (StringUtil.isNotEmpty(value)) {
			info = JsonUtil.toBean(value, JSONObject.class);
		}
		MemberLevelBase current = MemberLevelUtil.getLevel(accountDao.get(accountId).getLevelGroup());
		MemberLevelBase next = MemberLevelUtil.getNextLevel(current.getId());
		BigDecimal need = null;
		String nextName = null;
		BigDecimal gifMoney = null;
		BigDecimal depositMoney = null;
		if (next == null || next.getDepositMoney() == null) {
			need = BigDecimal.ZERO;
			gifMoney = BigDecimal.ZERO;
			nextName = "";
		} else {
			need = next.getDepositMoney().subtract(current.getDepositMoney());
			nextName = next.getLevelName();
			gifMoney = next.getGiftMoney();
			depositMoney = next.getDepositMoney();
		}

		MemberDepositInfo mdInfo = depositInfoDao.get(accountId);
		if (mdInfo != null && mdInfo.getDepositTotal() != null && next!=null
				&& mdInfo.getDepositTotal().compareTo(BigDecimal.ZERO) > 0) {
			need = next.getDepositMoney().subtract(mdInfo.getDepositTotal());
		}
		info.put("need", need);
		info.put("current", current.getLevelName());
		info.put("next", nextName);
		info.put("gifMoney", gifMoney);
		info.put("icon", current.getIcon());
		info.put("nextMoney",depositMoney);
		if(mdInfo!=null
				&& mdInfo.getDepositTotal() != null
				&& mdInfo.getDepositTotal().compareTo(BigDecimal.ZERO) > 0){
			info.put("total", mdInfo.getDepositTotal());
		}
		
		RedisAPI.addCache(Contants.MEMBER_LEVEL_INFO_MEMBER_ID + accountId, JsonUtil.toJson(info),
				Contants.MEMBER_LEVEL_INFO_MEMBER_ID_TIMEOUT);
		return info;
	}

	@Override
	public String importMember(MemberLevelBase level, String content, Long importType) {

		if (StringUtil.equals(importType, MemberLevelBase.IMPORT_TYPE_AGENT)) {
			if (StringUtil.isEmpty(content)) {
				throw new GenericException("上级代理不存在");
			}

			AccountVo avo = new AccountVo();
			avo.setStationId(level.getStationId());
			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			avo.setAccount(content);
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null) {
				throw new GenericException("上级代理不存在");
			}
			accountDao.updateLevelGroupsByParens(level.getId(), UserUtil.getChildren(agent), level.getStationId());
			reStatUserNum();
			return "";

		} else {
			if (StringUtil.isEmpty(content)) {
				throw new GenericException("成员为空");
			}

			String[] sonAccounts = content.split(",| |\n");
			if (sonAccounts == null || sonAccounts.length == 0) {
				throw new GenericException("成员为空");
			}
			int success[] = accountDao.batchUpdateLevelGroup(level.getId(), sonAccounts, level.getStationId());
			String failedAccount = "";
			for (int i = 0; i < success.length; i++) {
				if (success[i] == 0) {
					if (StringUtil.isNotEmpty(failedAccount)) {
						failedAccount += ",";
					}
					failedAccount += sonAccounts[i];
				}
			}
			if (StringUtil.isNotEmpty(failedAccount)) {
				failedAccount += "为导入失败的成员";
			}
			reStatUserNum();
			return failedAccount;
		}
	}
}
