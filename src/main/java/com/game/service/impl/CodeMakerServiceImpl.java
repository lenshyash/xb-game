package com.game.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.SysCodeMakerDao;
import com.game.model.SysCodeMaker;
import com.game.service.CodeMakerService;

@Repository
public class CodeMakerServiceImpl implements CodeMakerService{

	@Autowired
	private SysCodeMakerDao codeDao;
	
//	@Override
//	public int getCode(String code) {
//		SysCodeMaker sysCode = codeDao.get(code);
//		if(sysCode == null){
//			return 0;
//		}
//		return sysCode.getSeq().intValue();
//	}

	@Override
	public void addCode(String code, long value) {
		SysCodeMaker codeMaker = new SysCodeMaker();
		codeMaker.setKey(code);
		codeMaker.setSeq(value);
		codeMaker.setCreateDatetime(new Date());
		codeDao.insert(codeMaker);
	}

	@Override
	public long createNextCode(String code) {
		long seq = codeDao.getNextVal(code);
		if(seq != -1){
			return seq;
		}
		addCode(code,1);
		return 1;
	}


	@Override
	public void updateCode(String code, long value) {
		if(codeDao.updateSeq(code,value) == 0){
			SysCodeMaker codeMaker = new SysCodeMaker();
			codeMaker.setKey(code);
			codeMaker.setSeq(value);
			codeMaker.setCreateDatetime(new Date());
			codeDao.insert(codeMaker);
		}
	}
}
