package com.game.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.MemberBackwaterStrategyLevelDao;
import com.game.model.MemberBackwaterStrategyLevel;
import com.game.service.MemberBackwaterStrategyLevelService;

@Service
public class MemberBackwaterStrategyLevelServiceImpl implements MemberBackwaterStrategyLevelService {
	@Autowired
	private MemberBackwaterStrategyLevelDao depositLevelDao;

	@Override
	public List<MemberBackwaterStrategyLevel> findByStrategyId(Long strategyId, Long stationId) {
		return depositLevelDao.findByStrategyId(strategyId, stationId);
	}

	@Override
	public Set<Long> getLevelIdsByStrategyId(Long strategyId, Long stationId) {
		List<Long> ilist = depositLevelDao.getLevelIdsByStrategyId(strategyId, stationId);
		if (ilist != null && !ilist.isEmpty()) {
			return new HashSet<>(ilist);
		}
		return null;
	}

	@Override
	public void batchInsert(List<MemberBackwaterStrategyLevel> dllist) {
		if (dllist != null && !dllist.isEmpty()) {
			depositLevelDao.batchInsert(dllist);
		}
	}

	@Override
	public void batchDelete(Long strategyId, Long stationId, List<Long> levelIdList) {
		if (levelIdList != null && !levelIdList.isEmpty()) {
			depositLevelDao.batchDelete(strategyId, stationId, levelIdList);
		}
	}

	@Override
	public void deleteByStrategyId(Long strategyId, Long stationId) {
		depositLevelDao.deleteByStrategyId(strategyId, stationId);
	}
}
