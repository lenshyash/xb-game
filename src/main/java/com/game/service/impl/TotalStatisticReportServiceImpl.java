package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.dao.SysAccountDao;
import com.game.dao.TotalStatisticReportDao;
import com.game.http.PostType;
import com.game.http.RequestProxy;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.model.vo.TotalStatisticVo;
import com.game.service.TotalStatisticReportService;
import com.game.util.AuthTokenUtil;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

@Repository
public class TotalStatisticReportServiceImpl implements TotalStatisticReportService {
	private Logger logger = Logger.getLogger(TotalStatisticReportServiceImpl.class);
	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private TotalStatisticReportDao tsReportDao;

	private String getCacheKey(Long stationId, String account, String agentName, Date startTime, Date endTime, Integer pageNo, Integer pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(stationId);
		sb.append("_a_").append(account);
		sb.append("_b_").append(agentName);
		if (startTime != null) {
			sb.append("_s_").append(DateUtil.toDateStr(startTime));
		}
		if (endTime != null) {
			sb.append("_e_").append(DateUtil.toDateStr(endTime));
		}
		sb.append("_p_").append(pageNo);
		sb.append("_ps_").append(pageSize);
		return sb.toString();
	}

	@Override
	public synchronized Page<TotalStatisticVo> getTotalStatistic(Long stationId, String account, String agentName, Date startTime, Date endTime, Integer pageNo, Integer pageSize) {
		if (stationId == null) {
			return null;
		}
		String key = getCacheKey(stationId, account, agentName, startTime, endTime, pageNo, pageSize);
		String jsons = CacheUtil.getCache(CacheType.TEAM_TOTAL_STATISTIC, key);
		Page<TotalStatisticVo> page = null;
		if (jsons != null) {
			try {
				JSONObject obj = JSON.parseObject(jsons);
				List<TotalStatisticVo> list = JSON.parseArray(obj.getString("results"), TotalStatisticVo.class);
				page = new Page<>(obj.getIntValue("start"), obj.getIntValue("totalCount"), obj.getIntValue("pageSize"), list);
				return page;
			} catch (Exception e) {
			}
		}
		String parentIds = null;
		Long agentId = null;
		if (StringUtils.isNotEmpty(agentName)) {
			AccountVo avo = new AccountVo();
			avo.setStationId(stationId);
			avo.setAccount(agentName);
			avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null || (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId()) && agent.getLevel() <= UserUtil.getLevel())) {
				throw new GenericException("代理不存在！");
			}
			parentIds = agent.getParents();
			if (StringUtil.isEmpty(parentIds)) {
				parentIds = ",";
			}
			parentIds = parentIds + agent.getId() + ",";
			agentId = agent.getId();
		}
		page = tsReportDao.findTeamStatistic(stationId, account, parentIds, agentId, startTime, endTime);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<TotalStatisticVo> list = page.getList();
			getThirdAmount(list, stationId, startTime, endTime);
			TotalStatisticVo xiaoJi = new TotalStatisticVo();
			xiaoJi.setAccount("小计:");
			for (TotalStatisticVo v : list) {
				calcXiaoJi(v, xiaoJi);
			}
			list.add(xiaoJi);
			TotalStatisticVo zongJi = tsReportDao.getTotalStatistic(stationId, account, parentIds, agentId, startTime, endTime);
			calcThirdTotal(zongJi, stationId, account, parentIds, startTime, endTime);
			list.add(zongJi);// 总计
			CacheUtil.addCache(CacheType.TEAM_TOTAL_STATISTIC, key, page);
		}
		return page;
	}

	private void calcThirdTotal(TotalStatisticVo zongJi, Long stationId, String account, String parentIds, Date startTime, Date endTime) {
		String real = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		String egame = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_zi_you_yi);
		String sports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports);
		String thirdLottery = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_lottery);
		String chess = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_chess);
		if (!"on".equals(real) && !"on".equals(egame) && !"on".equals(sports) && !"on".equals(thirdLottery) && !"on".equals(chess)) {
			return;
		}
		Long accountId = null;
		if (StringUtils.isNotEmpty(account)) {
			SysAccount acc = accountDao.findOneByAccountAndStationId(account, stationId);
			if (acc != null) {
				accountId = acc.getId();
			}
		}
		if ("on".equals(real)) {
			Map map = getThirdTotal(stationId, accountId, parentIds, startTime, endTime, BusinessConstant.BET_TYPE_REAL);

			zongJi.setAwardAmount(BigDecimalUtil.addAll(zongJi.getAwardAmount(), (BigDecimal) map.get("awardAmount")));
			zongJi.setBettingAmount(BigDecimalUtil.addAll(zongJi.getBettingAmount(), (BigDecimal) map.get("bettingAmount")));
			zongJi.setTeamProfit(BigDecimalUtil.addAll(zongJi.getTeamProfit(), BigDecimalUtil.subtract((BigDecimal) map.get("bettingAmount"), (BigDecimal) map.get("awardAmount"))));
		}
		if ("on".equals(egame)) {
			Map map = getThirdTotal(stationId, accountId, parentIds, startTime, endTime, BusinessConstant.BET_TYPE_EGAME);
			zongJi.setAwardAmount(BigDecimalUtil.addAll(zongJi.getAwardAmount(), (BigDecimal) map.get("awardAmount")));
			zongJi.setBettingAmount(BigDecimalUtil.addAll(zongJi.getBettingAmount(), (BigDecimal) map.get("bettingAmount")));
			zongJi.setTeamProfit(BigDecimalUtil.addAll(zongJi.getTeamProfit(), BigDecimalUtil.subtract((BigDecimal) map.get("bettingAmount"), (BigDecimal) map.get("awardAmount"))));
		}
		if ("on".equals(sports)) {
			Map map = getThirdTotal(stationId, accountId, parentIds, startTime, endTime, BusinessConstant.BET_TYPE_SPORT_THIRD);
			zongJi.setAwardAmount(BigDecimalUtil.addAll(zongJi.getAwardAmount(), (BigDecimal) map.get("awardAmount")));
			zongJi.setBettingAmount(BigDecimalUtil.addAll(zongJi.getBettingAmount(), (BigDecimal) map.get("bettingAmount")));
			zongJi.setTeamProfit(BigDecimalUtil.addAll(zongJi.getTeamProfit(), BigDecimalUtil.subtract((BigDecimal) map.get("bettingAmount"), (BigDecimal) map.get("awardAmount"))));

		}
		
		if ("on".equals(thirdLottery)) {
			Map map = getThirdTotal(stationId, accountId, parentIds, startTime, endTime, BusinessConstant.BET_TYPE_LOTTERY_THIRD);
			zongJi.setAwardAmount(BigDecimalUtil.addAll(zongJi.getAwardAmount(), (BigDecimal) map.get("awardAmount")));
			zongJi.setBettingAmount(BigDecimalUtil.addAll(zongJi.getBettingAmount(), (BigDecimal) map.get("bettingAmount")));
			zongJi.setTeamProfit(BigDecimalUtil.addAll(zongJi.getTeamProfit(), BigDecimalUtil.subtract((BigDecimal) map.get("bettingAmount"), (BigDecimal) map.get("awardAmount"))));

		}
		
		if ("on".equals(chess)) {
			Map map = getThirdTotal(stationId, accountId, parentIds, startTime, endTime, BusinessConstant.BET_TYPE_CHESS);
			zongJi.setAwardAmount(BigDecimalUtil.addAll(zongJi.getAwardAmount(), (BigDecimal) map.get("awardAmount")));
			zongJi.setBettingAmount(BigDecimalUtil.addAll(zongJi.getBettingAmount(), (BigDecimal) map.get("bettingAmount")));
			zongJi.setTeamProfit(BigDecimalUtil.addAll(zongJi.getTeamProfit(), BigDecimalUtil.subtract((BigDecimal) map.get("bettingAmount"), (BigDecimal) map.get("awardAmount"))));

		}
		
		
	}

	/**
	 * 计算小计
	 * 
	 * @param vo
	 * @param xiaoJi
	 */
	private void calcXiaoJi(TotalStatisticVo vo, TotalStatisticVo xiaoJi) {
		xiaoJi.setBettingAmount(BigDecimalUtil.addAll(xiaoJi.getBettingAmount(), vo.getBettingAmount()));
		xiaoJi.setAwardAmount(BigDecimalUtil.addAll(vo.getAwardAmount(), xiaoJi.getAwardAmount()));
		xiaoJi.setDepositAmount(BigDecimalUtil.addAll(xiaoJi.getDepositAmount(), vo.getDepositAmount()));
		xiaoJi.setWithdrawAmount(BigDecimalUtil.addAll(xiaoJi.getWithdrawAmount(), vo.getWithdrawAmount()));
		xiaoJi.setRebateAmount(BigDecimalUtil.addAll(xiaoJi.getRebateAmount(), vo.getRebateAmount()));
		xiaoJi.setRebateAgentAmount(BigDecimalUtil.addAll(xiaoJi.getRebateAgentAmount(), vo.getRebateAgentAmount()));
		xiaoJi.setDepositGiftAmount(BigDecimalUtil.addAll(xiaoJi.getDepositGiftAmount(), vo.getDepositGiftAmount()));
		xiaoJi.setManualDepositAmount(BigDecimalUtil.addAll(xiaoJi.getManualDepositAmount(), vo.getManualDepositAmount()));
		xiaoJi.setManualWithdrawAmount(BigDecimalUtil.addAll(xiaoJi.getManualWithdrawAmount(), vo.getManualWithdrawAmount()));
		xiaoJi.setBalance(BigDecimalUtil.addAll(xiaoJi.getBalance(), vo.getBalance()));
		// xiaoJi.setTeamProfit(BigDecimalUtil.addAll(xiaoJi.getTeamProfit(),
		// vo.getTeamProfit()));
	}

	/**
	 * 计算真人跟电子的投注和派奖金额
	 * 
	 * @param list
	 * @param stationId
	 * @param startTime
	 * @param endTime
	 */
	private void getThirdAmount(List<TotalStatisticVo> list, Long stationId, Date startTime, Date endTime) {
		String real = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_zhen_ren_yu_le);
		String egame = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_dian_zi_you_yi);
		String sports = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_third_sports);
		if (!"on".equals(real) && !"on".equals(egame)&& !"on".equals(sports)) {// 真人跟电子都没开通
			return;
		}
		List<Long> accountIdList = new ArrayList<>();
		List<String> parentsList = new ArrayList<>();
		for (TotalStatisticVo v : list) {
			if (v.getAccountType() == null) {
				continue;
			}
			if (v.getAccountType().intValue() == SysAccount.ACCOUNT_PLATFORM_MEMBER) {
				accountIdList.add(v.getAccountId());
			} else if (v.getAccountType().intValue() == SysAccount.ACCOUNT_PLATFORM_AGENT) {
				parentsList.add(v.getParents());
			}
		}
		if (accountIdList != null && !accountIdList.isEmpty()) {
			Map map = getMemberReport(stationId, startTime, endTime, accountIdList, null);
			if (map != null && !map.isEmpty()) {
				Map om = null;
				for (TotalStatisticVo v : list) {
					om = (Map) map.get(v.getAccountId().toString());
					if (om != null) {
						v.setBettingAmount(BigDecimalUtil.addAll(v.getBettingAmount(), (BigDecimal) om.get("bettingMoney")));
						v.setAwardAmount(BigDecimalUtil.addAll(v.getAwardAmount(), (BigDecimal) om.get("winMoney")));
					}
				}
			}
		}
		if (parentsList != null && !parentsList.isEmpty()) {
			Map map = getMemberReport(stationId, startTime, endTime, null, parentsList);
			if (map != null && !map.isEmpty()) {
				for (TotalStatisticVo v : list) {
					if (map.containsKey(v.getParents())) {
						v.setTeamProfit(BigDecimalUtil.addAll(v.getTeamProfit(), (BigDecimal) map.get(v.getParents())));
					}
				}
			}
		}
	}

	private Map getMemberReport(Long stationId, Date startTime, Date endTime, List<Long> accountIdList, List<String> parentsList) {
		Map map = null;
		final String recordVo = getRecordVoJson(stationId, null, null, startTime, endTime, accountIdList, parentsList);
		try {
			String json = new RequestProxy() {

				public List<Header> getHeaders() {
					List<Header> headerList = new ArrayList<Header>();
					headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
					headerList.add(new BasicHeader("Connection", "close")); // 短链接
					return headerList;
				};

				public List<NameValuePair> getParameters() {
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("recordVo", recordVo));
					params.add(new BasicNameValuePair("token", AuthTokenUtil.getToken()));
					return params;
				};
			}.doRequest(StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/tenant/getMemberReport.do", PostType.POST, true);
			if (json == null) {
				return map;
			}
			JSONArray arr = JSON.parseArray(json);
			if (arr.size() == 0) {
				return map;
			}
			map = new HashMap();
			Map<String, Object> om = null;
			JSONObject obj = null;
			for (int i = 0, len = arr.size(); i < len; i++) {
				obj = arr.getJSONObject(i);
				if (accountIdList != null) {
					om = new HashMap();
					om.put("winMoney", obj.getBigDecimal("winMoney"));
					om.put("bettingMoney", obj.getBigDecimal("bettingMoney"));
					map.put(obj.getInteger("accountId").toString(), om);
					continue;
				}
				if (parentsList != null) {
					map.put(obj.getString("parents"), obj.getBigDecimal("money"));
				}
			}
		} catch (Exception e) {
			logger.error("获取电子全局报表发生错误", e);
		}
		return map;
	}

	/**
	 * 
	 * @return
	 */
	private String getRecordVoJson(Long stationId, Long accountId, String parentIds, Date startTime, Date endTime, List<Long> accountIdList, List<String> parentsList) {
		JSONObject json = new JSONObject();
		json.put("stationId", stationId);
		json.put("begin", startTime);
		json.put("end", endTime);
		if (accountId != null) {
			json.put("userId", accountId);
		}
		if (StringUtils.isNotEmpty(parentIds)) {
			json.put("parents", parentIds);
		}
		if (accountIdList != null) {
			JSONArray arr = new JSONArray();
			for (Long aid : accountIdList) {
				arr.add(aid);
			}
			json.put("accountGroups", arr);
		}
		if (parentsList != null) {
			JSONArray arr = new JSONArray();
			for (String p : parentsList) {
				arr.add(p);
			}
			json.put("parentgroups", arr);
		}
		return json.toJSONString();
	}

	/**
	 * 获取真人 电子总计
	 * 
	 * @param paramVo
	 * @return
	 */
	private Map getThirdTotal(Long stationId, Long accountId, String parentIds, Date startTime, Date endTime, int type) {

		String url = null;
		String typeName = "";
		switch (type) {
			case BusinessConstant.BET_TYPE_REAL:
				url = StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/tenant/getLiveGlobalReport.do";
				typeName = "真人";
				break;
			case BusinessConstant.BET_TYPE_EGAME:
				url = StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/tenant/getEgameGlobalReport.do";
				typeName = "电子";
				break;
			case BusinessConstant.BET_TYPE_SPORT_THIRD:
				url = StationConfigUtil.getSys(StationConfig.sys_real_center_url) + "/api/tenant/getSportsGlobalReport.do";
				typeName = "三方体育";
				break;
			default:
				break;
		}
		Map map = new HashMap();
		final String recordVo = getRecordVoJson(stationId, accountId, parentIds, startTime, endTime, null, null);
		try {
			String json = new RequestProxy() {

				public List<Header> getHeaders() {
					List<Header> headerList = new ArrayList<Header>();
					headerList.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
					headerList.add(new BasicHeader("Connection", "close")); // 短链接
					return headerList;
				};

				public List<NameValuePair> getParameters() {
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("recordVo", recordVo));
					params.add(new BasicNameValuePair("token", AuthTokenUtil.getToken()));
					return params;
				};
			}.doRequest(url, PostType.POST, true);
			JSONObject jsonObj = JSONObject.parseObject(json);
			map.put("bettingAmount", jsonObj.get("bettingMoney"));
			map.put("awardAmount", jsonObj.get("winMoney"));
		} catch (Exception e) {
			logger.error("获取"+typeName+"全局报表发生错误", e);
		}
		return map;
	}
}
