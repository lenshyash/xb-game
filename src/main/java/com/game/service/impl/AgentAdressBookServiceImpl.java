package com.game.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.LogType;
import com.game.dao.platform.AgentAdressBookDao;
import com.game.model.platform.AgentAdressBook;
import com.game.service.AgentAdressBookService;
import com.game.util.PoiUtil;
import com.game.util.SysLogUtil;

@Service
public class AgentAdressBookServiceImpl implements AgentAdressBookService {
	@Autowired
	private AgentAdressBookDao agentAdressBookDao;

	
	public Page page(Long stationId,String account) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentAdressBookDao.page(stationId,account);
	}
	
	public void save(AgentAdressBook adressBook) {
		agentAdressBookDao.save(adressBook);
	}
	
	public int checkUnique(Long stationId,Long accountId, String contactName,String contactPhone) {
		int i = agentAdressBookDao.checkUnique(stationId, accountId, contactName, contactPhone);
		return i;
	}
	
	public void export(Long stationId, String account){
		
		String title = "会员通讯录记录";
		String[] rowsName = new String[] { "序号", "会员", "联系人姓名", "联系人手机"};
		List<Map> list = agentAdressBookDao.export(stationId, account);
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < list.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(list.get(i).get("account"));
			objs[2] = getStrNull(list.get(i).get("contactName"));
			objs[3] = getStrNull(list.get(i).get("contactPhone"));

			dataList.add(objs);
		}
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出" + title, LogType.AGENT_FINANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getStrNull(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		return obj.toString();
	}
}
