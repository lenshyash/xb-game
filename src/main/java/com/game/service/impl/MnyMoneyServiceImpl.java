package com.game.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

import com.alibaba.fastjson.JSONObject;
import com.game.core.SystemConfig;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.IndexOperation;
import com.game.elasticsearch.operations.UpdateOperation;
import com.game.model.lottery.BcLotteryData;
import com.game.rocketmq.RocketMqUtil;
import com.game.rocketmq.TagEnum;
import com.game.util.*;
import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.StationConfig;
import com.game.dao.MnyMoneyDao;
import com.game.dao.MnyMoneyRecordDao;
import com.game.dao.ProxyMultiRebateSettingDao;
import com.game.dao.SysAccountDailyMoneyDao;
import com.game.dao.SysAccountDao;
import com.game.dao.SysAccountGuideMoneyDao;
import com.game.model.DailyMoney;
import com.game.model.MnyMoney;
import com.game.model.MnyMoneyRecord;
import com.game.model.SysAccount;
import com.game.model.SysAccountDailyMoney;
import com.game.model.SysAccountGuideMoney;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.lottery.LotteryEnum;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyMoneyService;

@Repository
public class MnyMoneyServiceImpl implements MnyMoneyService {

    @Autowired
    SysAccountDao sysAccountDao;

    @Autowired
    MnyMoneyDao moneyDao;

    @Autowired
    private ProxyMultiRebateSettingDao pmrsDao;

    @Autowired
    MnyMoneyRecordDao moneyRecordDao;

    @Autowired
    SysAccountDailyMoneyDao dailyMoneyDao;

    @Autowired
    SysAccountGuideMoneyDao guideMoneyDao;

    public BigDecimal updateMoney(MnyMoneyVo moneyVo) {
        BigDecimal money = moneyVo.getMoney();
        if (money == null || money.compareTo(BigDecimal.ZERO) < 1) {
            throw new GenericException("金额格式有误！");
        }
        money = money.setScale(6, RoundingMode.FLOOR);
        if (!moneyVo.getMoneyRecordType().isAdd()) {
            money = money.negate();
            moneyVo.setMoney(money);
        }
        BigDecimal[] results = null;
        try {
            results = moneyDao.updateMoney(moneyVo.getAccountId(), money);
        } catch (SQLException sqle) {
            throw new GenericException("操作失败！", sqle);
        }
        if (results == null || results.length != 2) {
            throw new GenericException("未知异常");
        }
        if (results[0] != null && results[0].intValue() == 0) {
            throw new GenericException("余额不足");
        }
        if (results[1] == null) {
            throw new GenericException("金额记录不存在");
        }
        return results[1];
    }

    /**
     * 修改余额宝相关金额
     *
     * @param moneyVo
     * @return
     */
    @Override
    public BigDecimal updateBalanceGem(MnyMoneyVo moneyVo) {
        BigDecimal bgMoney = moneyVo.getBgMoney();
        BigDecimal bgIncome = moneyVo.getBgIncome();
        if (bgMoney == null || bgMoney.compareTo(BigDecimal.ZERO) < 1) {
            throw new GenericException("金额格式有误！");
        }
        if (bgIncome == null) {
            bgIncome = BigDecimal.ZERO;
        }
        bgMoney = bgMoney.setScale(6, RoundingMode.FLOOR);
        bgIncome = bgIncome.setScale(6, RoundingMode.FLOOR);
        if (moneyVo.getMoneyRecordType().equals(MoneyRecordType.BALANCE_GEM_TRAN_OUT)) {
            bgMoney = bgMoney.negate();
            moneyVo.setMoney(bgMoney);
        }
        BigDecimal[] results = null;
        try {
            results = moneyDao.updateBalanceGem(moneyVo.getAccountId(), bgMoney, bgIncome);
        } catch (SQLException sqle) {
            throw new GenericException("操作失败！", sqle);
        }
        if (results == null || results.length != 3) {
            throw new GenericException("未知异常");
        }
        if (results[0] != null && results[0].intValue() == 0) {
            throw new GenericException("余额宝余额不足");
        }
        if (results[1] == null) {
            throw new GenericException("金额记录不存在");
        }
        return exactDecimal(results[1]);
    }

    @Override
    public BigDecimal checkMoney(Long accountId, BigDecimal money) {

        if (accountId == 0) {
            throw new GenericException("该用户不存在！");
        }

        MnyMoney mnyMoney = moneyDao.get(accountId);
        BigDecimal beforeMoney = BigDecimal.ZERO;
        if (mnyMoney != null) {
            beforeMoney = mnyMoney.getMoney();
        }
        BigDecimal afterMoney = beforeMoney.subtract(money.abs());
        BigDecimal zero = BigDecimal.ZERO;

        if (afterMoney.compareTo(zero) < 0) {
            throw new GenericException("账户余额不足！");
        }

        return mnyMoney.getMoney();
    }

    @Override
    public Map getMoneyByAccount(String account) {

        return getMoneyByAccount(account, StationUtil.getStationId());
    }

    @Override
    public Map getMoneyByAccount(String account, Long stationId) {
        if (StringUtil.isEmpty(account)) {
            throw new GenericException("会员账号不能为空！");
        }
        Map accountMap = moneyDao.getMoneyByAccount(account.trim().toLowerCase(), stationId);

        if (accountMap == null || accountMap.isEmpty()) {
            throw new GenericException("会员不存在！");
        }

        return accountMap;
    }

    @Override
    public MnyMoney getMoneyById(Long accountId) {

        String redisKey = Contants.MNY_MONEY_ACCOUNT_ID + accountId;
        MnyMoney mnyMoney = JsonUtil.toBean(RedisAPI.getCache(redisKey), MnyMoney.class);
        if (mnyMoney != null) {
            return mnyMoney;
        }
        mnyMoney = moneyDao.get(accountId);
        if (mnyMoney == null) {
            throw new GenericException("非法请求！");
        }
        SysAccount ac = sysAccountDao.get(accountId);
        if ("on".equalsIgnoreCase(StationConfigUtil.get(ac.getStationId(), StationConfig.onoff_member_rate_random))) {
            String parents = ac.getParents();
            Long agentId = 0l;
            BigDecimal rate = new BigDecimal(100);
            BigDecimal selfRate = ac.getRate();
            BigDecimal topRate = BigDecimal.ZERO;
            //上级不为空
            if (StringUtil.isNotEmpty(parents)) {
                agentId = StringUtil.toLong(parents.split(",")[1]);
                if (Validator.isNotNull(agentId)) {
					/*ProxyMultiRebateSetting pmrs = pmrsDao.get(agentId);
					if(pmrs != null) {
						topRate = pmrs.getGameShare();
					}*/
                    SysAccount parent = sysAccountDao.get(agentId);
                    if (parent != null && parent.getRate() != null) {
                        topRate = parent.getRate();
                    } else {
                        selfRate = BigDecimal.ZERO;
                        topRate = selfRate;
                    }

                }
            } else {
                selfRate = BigDecimal.ZERO;
                topRate = selfRate;
            }

            mnyMoney.setRate(rate.subtract(topRate).add(selfRate));
        }
        RedisAPI.addCache(redisKey, JsonUtil.toJson(mnyMoney), Contants.MNY_MONEY_ACCOUNT_ID_TIMEOUT);
        return mnyMoney;
    }

    @Override
    public BigDecimal getRate(BigDecimal selfRate, SysAccount ac) {
	/*	String redisKey = Contants.MNY_MONEY_ACCOUNT_ID + accountId;
		MnyMoney mnyMoney = JsonUtil.toBean(RedisAPI.getCache(redisKey), MnyMoney.class);*/
//		SysAccount ac = sysAccountDao.get(UserUtil.getUserId());
        String parents = ac.getParents();
        Long agentId = 0l;
        BigDecimal rate = new BigDecimal(100);
        BigDecimal topRate = BigDecimal.ZERO;
        //上级不为空
        if (StringUtil.isNotEmpty(parents)) {
            agentId = StringUtil.toLong(parents.split(",")[1]);
            if (Validator.isNotNull(agentId)) {
				/*ProxyMultiRebateSetting pmrs = pmrsDao.get(agentId);
				if(pmrs != null) {
					topRate = pmrs.getGameShare();
				}*/
                SysAccount parent = sysAccountDao.get(agentId);
                if (parent != null && parent.getRate() != null) {
                    topRate = parent.getRate();
                } else {
					/*selfRate = BigDecimal.ZERO;
					topRate = selfRate;*/
                    topRate = ac.getRate();
                }
            }
        } else {
//			selfRate = BigDecimal.ZERO;
//			topRate =  BigDecimal.ZERO;
			/*selfRate = BigDecimal.ZERO;
			topRate = selfRate;*/
            topRate = ac.getRate();
        }
        if (topRate.compareTo(ac.getRate()) < 0) {
            topRate = ac.getRate();
        }
        return rate.subtract(topRate).add(selfRate);
    }


    public BigDecimal exactDecimal(BigDecimal money) {
        Integer scale = 6;
        Integer scaleType = BigDecimal.ROUND_FLOOR;
        if (money == null) {
            return BigDecimal.ZERO;
        }
        return money.setScale(scale, scaleType);
    }

    @Override
    public MnyMoneyRecord updMnyAndRecord(MnyMoneyVo moneyVo) {
        return updMnyAndRecord(moneyVo, StationUtil.getStationId());
    }

    @Override
    public MnyMoneyRecord updMnyAndRecord(MnyMoneyVo moneyVo, Long stationId) {

        Long accountId = moneyVo.getAccountId();
        if (Validator.isNull(accountId)) {
            throw new GenericException("非法请求！");
        }
        SysAccount account = sysAccountDao.getFromCache(accountId);
        if (account == null
                || (Validator.isNotNull(stationId) && !StringUtil.equals(account.getStationId(), stationId))) {
            throw new GenericException("非法请求！");
        }
        moneyVo.setStationId(account.getStationId());
        moneyVo.setAccount(account.getAccount());

        BigDecimal beforeMoney = exactDecimal(updateMoney(moneyVo));
        if (Objects.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
            // 试玩账号不记录
            return null;
        }
        BigDecimal afterMoney = exactDecimal(beforeMoney.add(moneyVo.getMoney()));
        // 有手续费减去手续费。
        if (Validator.isNotNull(moneyVo.getFee()) && (BigDecimal.ZERO).compareTo(moneyVo.getFee()) == -1) {
            afterMoney = afterMoney.subtract(moneyVo.getFee());
        }
        BigDecimal money = moneyVo.getMoney();
        MoneyRecordType moneyType = moneyVo.getMoneyRecordType();
        MnyMoneyRecord record = new MnyMoneyRecord();
        record.setAccountId(moneyVo.getAccountId());
        record.setMoney(money);
        record.setType(moneyType.getType());
        record.setBeforeMoney(beforeMoney);
        record.setAfterMoney(afterMoney);
        record.setFee(moneyVo.getFee());
        record.setRemark(moneyVo.getRemark());
        record.setStationId(moneyVo.getStationId());
        record.setOrderId(moneyVo.getOrderId());
        record.setParents(account.getParents());
        record.setAccount(account.getAccount());
        record.setCreateDatetime(new Date());
        if (moneyVo.getBizDatetime() != null) {
            record.setBizDatetime(moneyVo.getBizDatetime());
        } else {
            record.setBizDatetime(new Date());
        }
        record.setId(moneyRecordDao.getId());
        User user = UserUtil.getCurrentUser();
        SysAccount acc = null;
        if (!StationUtil.isAdminStation()) {
            acc = (SysAccount) user;
        }
        if (acc != null) {
            record.setOperatorId(acc.getId());
            if (acc.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) {
                record.setOperatorName("--");
            } else {
                record.setOperatorName(acc.getAccount());
            }
        }

        //多级代理返点使用当前时间
        if (StringUtil.isNotEmpty(moneyType) && moneyType == MoneyRecordType.PROXY_MULTI_REBATE_ADD) {
            dailyHandler(moneyVo, new Date(), account, afterMoney, money);
        } else {
            dailyHandler(moneyVo, record.getBizDatetime(), account, afterMoney, money);
        }

        //投递棋牌帐变记录
        RocketMqUtil.sendOneOrder(TagEnum.MNY_MONEY_INS, JSONObject.toJSONBytes(record), record.getStationId());
        //如果是改单操作，金额变动在原帐变记录上调整
        if (moneyVo.getMoneyRecordType().equals(MoneyRecordType.LOTTERY_CHANGE_OREDE_SUB) ||
                moneyVo.getMoneyRecordType().equals(MoneyRecordType.LOTTERY_CHANGE_OREDE_ADD)) {
            //根据订单号查询原帐变记录
            MnyMoneyRecord oldRecord = moneyRecordDao.getByOrderIdByLottery(moneyVo.getOrderId(), moneyVo.getStationId());
            if (oldRecord != null) {
                if (!moneyVo.getMoneyRecordType().isAdd()) {
                    money = money.add(oldRecord.getMoney());
                } else {
                    money = oldRecord.getMoney().add(money);
                }
                oldRecord.setAfterMoney(oldRecord.getBeforeMoney().add(money));
                oldRecord.setMoney(money);
                moneyRecordDao.save(oldRecord);
                //同步数据到es
                ElasticApi.updateOne(new UpdateOperation<MnyMoneyRecord>(EsEnum.MNY_MONEY_RECORD.getIndexTag(), EsEnum.MNY_MONEY_RECORD.getType(),
                        oldRecord.getId().toString()).setSource(oldRecord));
                return oldRecord;
            }
        }
        //同步数据到es
        if (SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN) {
            record.setAccountType(account.getAccountType());
            record.setReportType(account.getReportType());
            moneyRecordDao.insert(record);
            record.setMoneyType(record.getType());
            MnyMoneyRecord newData = new MnyMoneyRecord();
            BeanDtoVoUtils.beanToBean(record, newData);
            ElasticApi.addQueue(new IndexOperation<>(EsEnum.MNY_MONEY_RECORD.getIndexTag(), EsEnum.MNY_MONEY_RECORD.getType(), newData));
            return record;
        } else {
            return moneyRecordDao.insert(record);
        }

    }

    @Override
    public void saveCash(Long accountId, String money, BigDecimal moneyOld, Long userId, Long stationId) {
        if (accountId == null || StringUtils.isEmpty(money) || moneyOld == null || stationId == null
                || userId == null) {
            throw new GenericException("参数不正确!");
        }
        // Map<Object, Object> map = sysAccountDao.getAccountById(userId,
        // stationId);
        // if (map == null) {
        // throw new GenericException("会员不存在");
        // }
        // BigDecimal cash = (BigDecimal) map.get("money");// 充值前余额
        BigDecimal cash = moneyDao.getBalance(userId);
        if (cash == null || moneyOld.compareTo(cash) > 0) {
            throw new GenericException("额度不足,请及时充值!");
        }
        MnyMoneyVo moneyVo = new MnyMoneyVo();
        moneyVo.setStationId(StationUtil.getStationId());
        moneyVo.setAccountId(accountId);
        moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_ARTIFICIAL);// 加款
        moneyVo.setMoney(moneyOld);
        moneyVo.setFee(new BigDecimal(0));
        updMnyAndRecord(moneyVo);
        moneyVo = new MnyMoneyVo();
        moneyVo.setStationId(StationUtil.getStationId());
        moneyVo.setAccountId(UserUtil.getUserId());
        moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ARTIFICIAL);// 扣钱
        moneyVo.setMoney(moneyOld);
        updMnyAndRecord(moneyVo);
    }

    public void dailyHandler(MnyMoneyVo moneyVo, Date statDate, SysAccount user, BigDecimal balance,
                             BigDecimal money) {

        if (moneyVo.getMoneyRecordType() == MoneyRecordType.MEMBER_ROLL_BACK_ADD
                && StringUtils.isEmpty(moneyVo.getOrderId()) && moneyVo.getBetType() == null) {
            // 会员批量反水不需要统计
            return;
        }

        DailyMoney daily = new SysAccountDailyMoney();
        if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
            daily = new SysAccountGuideMoney();
        }

        daily.setAccount(user.getAccount());
        daily.setAccountId(user.getSysUserId());
        daily.setParents(user.getParents());
        daily.setAccountType(user.getAccountType());
        daily.setReportType(user.getReportType());
        daily.setBalance(balance);
        daily.setStationId(user.getStationId());
        daily.setAgentId(user.getAgentId());
        daily.setAgentName(user.getAgentName());
        daily.setStatDate(statDate);
        long times = 1L;
        if (moneyVo.getSize() != null && moneyVo.getSize() > 0) {
            times = moneyVo.getSize();
        }
        Boolean firstDeposit = moneyVo.getFirstDeposit();
        if (firstDeposit != null && firstDeposit.booleanValue()) {
            daily.setFirstDeposit(1l);
            daily.setFirstDepositAmount(moneyVo.getMoney());
        }

        Boolean firstDraw = moneyVo.getFirstWithdraw();
        if (firstDraw != null && firstDraw.booleanValue()) {
            daily.setFirstWithdraw(1l);
            daily.setFirstWithdrawAmount(moneyVo.getMoney());
        }
        switch (moneyVo.getMoneyRecordType()) {
            case DEPOSIT_ARTIFICIAL:
                daily.setDepositHandlerTimes(times);
                daily.setDepositArtificial(money);
                break;
            case MONEY_CHANGE_ADD:
                daily.setDepositArtificial(money);
                break;
            case WITHDRAW_ARTIFICIAL:
            case MONEY_CHANGE_SUB:
                daily.setWithdrawArtificial(money);
                break;
            case WITHDRAW_CANCLE:
            case WITHDRAW_ONLINE_FAILED:
                times = -times;
            case WITHDRAW_ONLINE:
                daily.setWithdrawAmount(money);
                daily.setWithdrawTimes(times);
                break;
            case WITHDRAW_FAILED_ROLLBACK:
                daily.setWithdrawAmount(money);
                daily.setWithdrawTimes(times);
                break;
            case DEPOSIT_CANCLE:
                times = -times;
            case DEPOSIT_ONLINE_THIRD:
                daily.setThirdDepositAmount(money);
            case DEPOSIT_ONLINE_FAST:
            case DEPOSIT_ONLINE_BANK:
                if (StringUtil.equals(MnyMoneyVo.HANDLERTYPE_ARTIFICIAL, moneyVo.getHandlerType())) {
                    daily.setDepositHandlerArtificial(money);
                    daily.setDepositHandlerArtificialTimes(times);
                }
                daily.setDepositAmount(money);
                daily.setDepositTimes(times);
                break;
            case DEPOSIT_BY_SYS_API:
                daily.setSysApiDepositAmount(money);
                daily.setSysApiDepositTimes(times);
                break;
            case DEPOSIT_GIFT_ACTIVITY:
                daily.setDepositGiftAmount(money);
                daily.setDepositGiftTimes(times);
                break;
            case WITHDRAW_GIFT_ACTIVITY:
                daily.setWithdrawGiftAmount(money);
                daily.setWithdrawGiftTimes(times);
                break;
            case REGISTER_GIFT_ACTIVITY:
                daily.setRegisterGiftAmount(money);
                break;
            case LEVEL_UP_GIFT:
                daily.setLevelUpGiftAmount(money);
                break;
            case SPORT_CANCEL_ORDER:
                times = -times;
            case SPORT_BETTING:
                daily.setSportsBetTimes(times);
                daily.setSportsBetAmount(money);
                break;
            case SPORT_ROLLBACK:
                times = -times;
            case SPORT_AWARD:
                daily.setSportsWinTimes(times);
                daily.setSportsWinAmount(money);
                break;
            case LOTTERY_ORDER_CANCEL:
            case LOTTERY_JOINT_PURCHASE_PATE:
            case LOTTERY_JOINT_INVALID:
            case LOTTERY_JOINT_CANCEL:
                times = -times;
            case LOTTERY_JOINT_PURCHASE:
            case LOTTERY_BETTING:
                if (LotteryEnum.isSysLot(moneyVo.getCode())) {
                    daily.setSysLotteryBetTimes(times);
                    daily.setSysLotteryBetAmount(money);
                } else {
                    daily.setLotteryBetTimes(times);
                    daily.setLotteryBetAmount(money);
                }
                break;

            case LOTTERY_ROLLBACK:
                times = -times;
            case LOTTERY_AWARD:
            case LOTTERY_JOINT_AWARD:
                if (LotteryEnum.isSysLot(moneyVo.getCode())) {
                    daily.setSysLotteryWinTimes(times);
                    daily.setSysLotteryWinAmount(money);
                } else {
                    daily.setLotteryWinTimes(times);
                    daily.setLotteryWinAmount(money);
                }
                break;

            case MARK_SIX_ORDER_CANCEL:
                times = -times;
            case MARK_SIX_BETTING:
                if (StringUtils.equals(moneyVo.getCode(), LotteryEnum.SFLHC.name()) || StringUtils.equals(moneyVo.getCode(),
                        LotteryEnum.WFLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.TMLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.HKMHLHC.name())) {
                    daily.setSfMarkSixBetTimes(times);
                    daily.setSfMarkSixBetAmount(money);
                } else {
                    daily.setMarkSixBetTimes(times);
                    daily.setMarkSixBetAmount(money);
                }
                break;
            case MARK_SIX_ROLLBACK:
                times = -times;
            case MARK_SIX_AWARD:
                if (StringUtils.equals(moneyVo.getCode(), LotteryEnum.SFLHC.name()) || StringUtils.equals(moneyVo.getCode(),
                        LotteryEnum.WFLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.TMLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.HKMHLHC.name())) {
                    daily.setSfMarkSixWinTimes(times);
                    daily.setSfMarkSixWinAmount(money);
                } else {
                    daily.setMarkSixWinTimes(times);
                    daily.setMarkSixWinAmount(money);
                }
                break;

            case MEMBER_ROLL_BACK_ADD:
            case MEMBER_ROLL_BACK_SUB:
                if (moneyVo.getBetType() != null) {
                    switch (moneyVo.getBetType()) {
                        case 1:// 彩票
                            daily.setLotteryRebateAmount(money);
                            break;
                        case 11:// 系统彩
                            daily.setSysLotteryRebateAmount(money);
                            break;
                        case 2:// 真人
                            daily.setRealRebateAmount(money);
                            break;
                        case 3:// 电子
                            daily.setEgameRebateAmount(money);
                            break;
                        case 4:// 体育
                            daily.setSportsRebateAmount(money);
                            break;
                        case 5:// 六合彩特码B
                        case 6:// 六合彩正码B
                            daily.setMarkSixRebateAmount(money);
                            break;
                        case 15:// 十分六合彩特码B
                        case 16:// 十分六合彩正码B
                            daily.setSfMarkSixRebateAmount(money);
                        case 99:// 三方体育
                            daily.setThirdSportsRebateAmount(money);
                            break;
                        case 90:// 棋牌
                            daily.setChessRebateAmount(money);
                            break;
                        case 91:// 三方彩票
                            daily.setThirdLotteryRebateAmount(money);
                            break;
                    }
                } else {
                    CodeType codeType = getCodeType(moneyVo.getOrderId());
                    if (codeType == CodeType.Sports) {
                        daily.setSportsRebateAmount(money);
                    } else if (codeType == CodeType.Lottery) {
                        daily.setLotteryRebateAmount(money);
                    } else if (codeType == CodeType.Marksix) {
                        daily.setMarkSixRebateAmount(money);
                    } else if (codeType == CodeType.Egame) {
                        daily.setEgameRebateAmount(money);
                    } else if (codeType == CodeType.Real) {
                        daily.setRealRebateAmount(money);
                    } else if (codeType == CodeType.Hunter) {
                        daily.setHunterRebateAmount(money);
                    } else if (codeType == CodeType.ThirdLottery) {
                        daily.setThirdLotteryRebateAmount(money);
                    } else if (codeType == CodeType.ThirdSport) {
                        daily.setThirdSportsRebateAmount(money);
                    } else if (codeType == CodeType.Chess) {
                        daily.setChessRebateAmount(money);
                    }
                }
                break;

            case PROXY_REBATE_ADD:
            case PROXY_REBATE_SUB:
            case PROXY_MULTI_REBATE_ADD:
            case PROXY_MULTI_REBATE_SUB:
                daily.setProxyRebateAmount(money);
                break;

            case ACTIVE_AWARD:
                daily.setActiveAwardAmount(money);
                daily.setActiveAwardTimes(times);
                break;
            case LOTTERY_CHANGE_OREDE_ADD:
            case LOTTERY_CHANGE_OREDE_SUB:
                if (LotteryEnum.isSysLot(moneyVo.getCode())) {
                    if (StringUtils.equals(moneyVo.getCode(), LotteryEnum.SFLHC.name()) || StringUtils.equals(moneyVo.getCode(),
                            LotteryEnum.WFLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.TMLHC.name()) || StringUtils.equals(moneyVo.getCode(), LotteryEnum.HKMHLHC.name())) {
                        //系统六合
                        daily.setSfMarkSixBetAmount(money);
                    } else {
                        //系统彩
                        daily.setSysLotteryBetAmount(money);
                    }
                } else {
                    if (StringUtils.equals(moneyVo.getCode(), LotteryEnum.LHC.name()) || StringUtils.equals(moneyVo.getCode(),
                            LotteryEnum.AMLHC.name())) {
                        //官方六合
                        daily.setMarkSixBetAmount(money);
                    } else {
                        //官方彩
                        daily.setLotteryBetAmount(money);
                    }
                }

                break;
            default:
                break;
        }

        if (StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
            guideMoneyDao.insert((SysAccountGuideMoney) daily);
        } else {
            dailyMoneyDao.insert((SysAccountDailyMoney) daily);
        }
    }

    private CodeType getCodeType(String orderId) {
        if (orderId == null) {
            return null;
        }
        if (StringUtil.isNotEmpty(orderId)) {
            orderId = orderId.toLowerCase();
        }
        if (orderId.indexOf("ag_hunter_") != -1) {
            return CodeType.Hunter;
        }
        if (orderId.indexOf("ag_xin_") != -1 || orderId.indexOf("bbin_eg_") != -1 || orderId.indexOf("mg_eg_") != -1 || orderId.indexOf("pt_eg_") != -1 || orderId.indexOf("cq9_eg_") != -1 || orderId.indexOf("jdb_eg_") != -1 || orderId.indexOf("ttg_eg_") != -1 || orderId.indexOf("mw_eg_") != -1 || orderId.indexOf("isb_eg_") != -1 || orderId.indexOf("ebet_eg_") != -1) {
            return CodeType.Egame;
        }

        if (orderId.indexOf("ag_live_") != -1 || orderId.indexOf("bbin_") != -1 || orderId.indexOf("mg_live_") != -1 || orderId.indexOf("pt_live_") != -1 || orderId.indexOf("allbet_live_") != -1 || orderId.indexOf("bg_live_") != -1 || orderId.indexOf("vr_live_") != -1 || orderId.indexOf("ky_live_") != -1 || orderId.indexOf("ebet_live_") != -1) {
            return CodeType.Real;
        }

        if (orderId.indexOf("ky_live_") != -1) {
            return CodeType.Chess;
        }

        if (orderId.indexOf("vr_live_") != -1) {
            return CodeType.ThirdLottery;
        }

        if (orderId.indexOf("m8_sp_") != -1 || orderId.indexOf("ibc_sp_") != -1) {
            return CodeType.ThirdSport;
        }
        orderId = orderId.substring(0, 1).toUpperCase();

        return CodeType.getCodeType(orderId);
    }

    @Override
    public DailyMoney findByAccountAndDate(Long accountId, Date formatDate) {
        SysAccount account = sysAccountDao.get(accountId);
        if (account == null) {
            return null;
        }
        DailyMoney dailyMoney = null;
        if (StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
            dailyMoney = guideMoneyDao.findOneByAccountIdAndStatDate(accountId, formatDate);
        } else {
            dailyMoney = dailyMoneyDao.findOneByAccountIdAndStatDate(accountId, formatDate);
        }
        return dailyMoney;
    }

    @Override
    public BigDecimal fastTransfer(Long accountId) {
        MnyMoney money = this.getMoneyById(accountId);
        BigDecimal tansMoney = money.getMoney();
        if (tansMoney != null && tansMoney.compareTo(new BigDecimal(0)) == 1) {
            MnyMoneyVo vo = new MnyMoneyVo();
            vo.setAccountId(accountId);
            vo.setMoney(tansMoney);
            vo.setMoneyRecordType(MoneyRecordType.THIRD_OFFLINE_AUTO_TRAN_OUT);
            vo.setRemark("三方登出自动下分");
            this.updMnyAndRecord(vo);
        }
        return tansMoney;

    }
}