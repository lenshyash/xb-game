package com.game.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.platform.AgentAppKeyConfigDao;
import com.game.model.AgentAppKeyConfig;
import com.game.service.AgentAppKeyConfigService;

@Service
public class AgentAppKeyConfigServiceImpl implements AgentAppKeyConfigService {

	@Autowired
	private AgentAppKeyConfigDao agentAppKeyConfigDao;

	@Override
	public Page page(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentAppKeyConfigDao.page(stationId);
	}

	@Override
	public void openCloseH(Long id, Long stationId, Long status) {
		if (id == null || id <= 0 || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentAppKeyConfigDao.getStationId(id);
		if (!stationId.equals(staId)) {
			throw new GenericException("站点错误！");
		}
		agentAppKeyConfigDao.openCloseH(id, stationId, status);
	}

	@Override
	public void addSave(AgentAppKeyConfig apc) {
		if (apc.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		agentAppKeyConfigDao.save(apc);
	}

	@Override
	public void eidtSave(AgentAppKeyConfig apc) {
		if (apc.getId() == null || apc.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		AgentAppKeyConfig apcs = agentAppKeyConfigDao.get(apc.getId());
		if (!apc.getStationId().equals(apcs.getStationId())) {
			throw new GenericException("站点错误！");
		}
		apcs.setName(apc.getName());
		apcs.setUrl(apc.getUrl());
		apcs.setVersion(apc.getVersion());
		apcs.setStatus(apc.getStatus());
		agentAppKeyConfigDao.update(apcs);
	}

	@Override
	public void delete(Long id, Long stationId) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentAppKeyConfigDao.delete(id, stationId);
	}

	@Override
	public AgentAppKeyConfig getVersionAndStationId(String version, Long stationId) {
		if (StringUtils.isEmpty(version) || stationId == null) {
			throw new GenericException("参数不能为空");
		}
		return agentAppKeyConfigDao.getVersionAndStationId(version, stationId);
	}

	@Override
	public AgentAppKeyConfig getOne(Long id, Long stationId) {
		if (id != null) {
			AgentAppKeyConfig a = agentAppKeyConfigDao.get(id);
			if (a != null && (stationId == null || stationId.equals(a.getStationId()))) {
				return a;
			}
		}
		return null;
	}
}
