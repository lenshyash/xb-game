package com.game.service.impl;

import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.game.constant.LogType;
import com.game.dao.AdminUserDao;
import com.game.dao.AdminUserGroupDao;
import com.game.model.AdminUserGroup;
import com.game.service.AdminUserGroupService;
import com.game.util.SysLogUtil;

@Repository
public class AdminUserGroupServiceImpl implements AdminUserGroupService {

	@Autowired
	private AdminUserGroupDao userGroupDao;

	@Autowired
	private AdminUserDao userDao;

	@Override
	public Page getGroups() {
		return userGroupDao.getGroups();
	}

	@Override
	public void saveGroup(AdminUserGroup group) {

		if (userGroupDao.isNotUnique(group, "name")) {
			throw new GenericException("已经存在名为：\"" + group.getName() + "\"的用户组别！");
		}
		if (group != null && Validator.isNotNull(group.getId())) {
			SysLogUtil.log("更新总控系统用户组别:" + group.getName(), LogType.ADMIN_SYSTEM);
		} else {
			SysLogUtil.log("新增总控系统用户组别:" + group.getName(), LogType.ADMIN_SYSTEM);
		}
		userGroupDao.save(group);

	}

	@Override
	public void deleteGroup(String ids) {
		if (userDao.isUsedGroup(ids)) {
			throw new GenericException("您要删除的组别正在被用户使用！");
		}
		SysLogUtil.log("删除总控系统用户组别,编号为:" + ids, LogType.ADMIN_SYSTEM);
		userGroupDao.deletes(ids);
	}

	@Override
	public AdminUserGroup getGroupById(Long groupId) {
		return userGroupDao.getGroupById(groupId);
	}

	@Override
	public List<AdminUserGroup> getGroupCombo() {
		return userGroupDao.getGroupAll();
	}

	@Override
	public void del(Integer id) {
		if (!StringUtils.isEmpty(id) && id > 0) {
			SysLogUtil.log("删除总控系统用户组别,编号为:" + id, LogType.ADMIN_SYSTEM);
			userGroupDao.delete(id);
		} else {
			throw new GenericException("参数不正确!");
		}

	}

}
