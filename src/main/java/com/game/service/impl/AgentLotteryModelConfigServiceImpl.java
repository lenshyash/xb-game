package com.game.service.impl;

import java.math.BigDecimal;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.constant.LogType;
import com.game.dao.platform.AgentLotteryModelConfigDao;
import com.game.model.lottery.LotteryEnum;
import com.game.model.platform.AgentLotteryModelConfig;
import com.game.service.AgentLotteryModelConfigService;
import com.game.util.SysLogUtil;

@Service
public class AgentLotteryModelConfigServiceImpl implements AgentLotteryModelConfigService {
	public static int BC_LOTTERY_DATA_DB_INDEX = 6;// redis 缓存 db 存到6库
	public static String CACHE_KEY_PREFIX = "AGENT_LOTTERY_MODEL_CONFIG_";// redis 缓存key前缀
	
	@Autowired
	private AgentLotteryModelConfigDao agentLotteryModelConfigDao;

	@Override
	public Page page(Integer type, Long stationId,String name) {
		return agentLotteryModelConfigDao.page(type, stationId,name,LotteryEnum.getSysLottery());
	}

	@Override
	public AgentLotteryModelConfig getOne(Long id, Long stationId) {
		return agentLotteryModelConfigDao.getOne(id, stationId);
	}

	@Override
	public void save(AgentLotteryModelConfig almc, Long stationId) {
		//判断是否已经存在站点彩种模式配置记录
//		AgentLotteryModelConfig old = agentLotteryModelConfigDao.getOne(almc.getBcId(),stationId);
		if(almc==null) {
			throw new GenericException("参数错误");
		}
		BigDecimal percentage = almc.getPercentage();
		if(almc.getLotteryMode()==2L && (StringUtil.isEmpty(percentage) || (percentage.compareTo(new BigDecimal("30"))<0 || percentage.compareTo(new BigDecimal("100"))>0))) {
			throw new GenericException("百分比必须大于等于30或者小于100等于");
		}
		almc.setStationId(stationId);
//		almc.setLotCode(old.getLotCode());
//		almc.setStationId(stationId);
//		almc.setLotteryTpe(old.getLotteryTpe());
		if(almc.getId()!=null) {
			//更新
			agentLotteryModelConfigDao.update(almc);
		}else {
			//插入
			AgentLotteryModelConfig one = agentLotteryModelConfigDao.getOne(almc.getLotCode(), stationId);
			if(one!=null) {
				throw new GenericException("已存在相同配置");
			}
			
			agentLotteryModelConfigDao.save(almc);
		}
		String key = "LOTTERY_MODE_"+stationId+"_"+almc.getLotCode();
		
		RedisAPI.delCache(key);
		
		String a = "";
		Long lotteryMode = almc.getLotteryMode();
		if(lotteryMode.equals(1L)) {
			a = "随机";
		}else if(lotteryMode.equals(2L)) {
			a = "百分比";
		}else if(lotteryMode.equals(3L)) {
			a = "正式站推送";
		}
		
		String key2 = CACHE_KEY_PREFIX + stationId+"_"+almc.getLotCode();
		CacheUtil.delCache(CacheType.BC_LOTTERY, key2);
		
		SysLogUtil.log("调整"+almc.getLotName()+"开奖模式"+a+"，参数：" + JSON.toJSONString(almc), LogType.AGENT_SYSTEM);
		
	}

	@Override
	public AgentLotteryModelConfig findByStationCode(Long stationId, String lotCode) {
		String key = CACHE_KEY_PREFIX + stationId+"_"+lotCode;
		AgentLotteryModelConfig cache = CacheUtil.getCache(CacheType.BC_LOTTERY, key, AgentLotteryModelConfig.class);
		if(StringUtil.isNotEmpty(cache)) {
			return cache;
		}

		AgentLotteryModelConfig model = agentLotteryModelConfigDao.getOne(lotCode, stationId);
		if(model!=null) {
			CacheUtil.addCache(CacheType.BC_LOTTERY, key, model);
		}
		return model;
	}
}
