package com.game.service.impl;

import java.util.Date;
import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.MemberActiveAwardDao;
import com.game.dao.SysProductDao;
import com.game.model.SysLog;
import com.game.model.SysProduct;
import com.game.model.vo.ProductVo;
import com.game.service.SysProductService;
import com.game.util.StationUtil;

@Repository
public class SysProductServiceImpl implements SysProductService {

	@Autowired
	SysProductDao productDao;

	@Autowired
	MemberActiveAwardDao awardDao;

	@Override
	public Page getPage(ProductVo pvo) {
		return productDao.getProductPage(pvo);
	}

	@Override
	public void saveProduct(SysProduct product) {
		SysProduct saveProduct = null;
		if (product.getId() != null) {
			saveProduct = productDao.get(product.getId());
			saveProduct.setProductName(product.getProductName());
			saveProduct.setProductDesc(product.getProductDesc());
			saveProduct.setPrice(product.getPrice());
		} else {
			saveProduct = product;
			saveProduct.setStationId(StationUtil.getStationId());
			saveProduct.setCreateDatetime(new Date());
		}
		productDao.save(saveProduct);
	}

	@Override
	public void delProduct(ProductVo pvo) {

		Long productId = pvo.getProductId();

		if (Validator.isNull(productId)) {
			throw new GenericException("该商品已删除或者不存在！");
		}
		SysProduct product = productDao.get(productId);

		if (product == null || !StringUtil.equals(product.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}

		// 该商品正在别的奖项引用的数量
		Integer awardCount = awardDao.getCountByUsingProduct(productId);
		if (awardCount > 0) {
			throw new GenericException("该商品正在被" + awardCount + "个奖项引用，请修改这些奖项引用，再删除该商品！");
		}

		productDao.delete(pvo.getProductId());
	}

	@Override
	public List getCombo(ProductVo productVo) {
		return productDao.getProductList(productVo);
	}
	@Override
	public SysProduct getOne(Long id, Long stationId) {
		if (id != null) {
			SysProduct it = productDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}
}
