package com.game.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.MemberRedPacketDao;
import com.game.dao.MemberRedPacketFictitiousRecordDao;
import com.game.dao.MemberRedPacketRecordDao;
import com.game.dao.MnyBetNumberDao;
import com.game.dao.MnyComRecordDao;
import com.game.dao.SysAccountDao;
import com.game.model.BetNumRecord;
import com.game.model.MemberLevelBase;
import com.game.model.MemberRedPacket;
import com.game.model.MemberRedPacketFictitiousRecord;
import com.game.model.MemberRedPacketLevel;
import com.game.model.MemberRedPacketRecord;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.MnyMoneyVo;
import com.game.readpacket.RedPacketUtil;
import com.game.service.MemberLevelService;
import com.game.service.MemberRedPacketLevelService;
import com.game.service.MemberRedPacketService;
import com.game.service.MnyMoneyService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.RedSortEnum;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class MemberRedPacketServiceImpl implements MemberRedPacketService {
	private static final int MEMBER_RED_PACKET_REDIS_DB = 8;
	@Autowired
	private MemberRedPacketDao redPacketDao;
	@Autowired
	private MemberRedPacketRecordDao redPacketRecordDao;
	@Autowired
	private MemberRedPacketFictitiousRecordDao redPacketRecordFictitiousDao;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MemberRedPacketLevelService redPacketLevelService;
	@Autowired
	private MemberLevelService levelService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private MemberRedPacketFictitiousRecordDao fictitiousRecordDao;
	@Autowired
	private MnyComRecordDao mnyComRecordDao;
	
	@Override
	public Page<MemberRedPacket> getPage(Long stationId) {
		Page<MemberRedPacket> page = redPacketDao.getPage(stationId);
		if (page != null && page.getList() != null) {
			List<MemberLevelBase> levelList = levelService.getStationLevels(stationId);
			if (levelList != null && !levelList.isEmpty()) {
				Map<Long, String> map = new HashMap<>();
				StringBuilder sb = null;
				for (MemberLevelBase b : levelList) {
					map.put(b.getId(), b.getLevelName());
				}
				Set<Long> rpLevelSet = null;
				for (MemberRedPacket rp : page.getList()) {
					rpLevelSet = redPacketLevelService.findLevelsByRedPacketId(rp.getId());
					if (rpLevelSet != null && !rpLevelSet.isEmpty()) {
						sb = new StringBuilder();
						for (Long l : rpLevelSet) {
							sb.append(map.get(l)).append(",");
						}
						sb.deleteCharAt(sb.length() - 1);
						rp.setLevelNames(sb.toString());
					}
				}
			}
			
		}
		return page;
	}

	@Override
	public Page<MemberRedPacketRecord> getRecordPage(String account, Long stationId, Date begin, Date end) {
		return redPacketRecordDao.getRecordPage(stationId, account, begin, end);
	}

	private MemberRedPacket getById(Long id) {
		if (id == null || id <= 0)
			return null;
		String key = "MEMBER_RED_PACKET_red_packet_" + id;
		String json = RedisAPI.getCache(key, MEMBER_RED_PACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseObject(json, MemberRedPacket.class);
		}

		MemberRedPacket rp = redPacketDao.get(id);
		if (rp != null) {
			RedisAPI.addCache(key, JSON.toJSONString(rp), 0, MEMBER_RED_PACKET_REDIS_DB);
		}
		return rp;
	}

	@Override
	public void updStatus(Long redPacketId, Integer status) {
		MemberRedPacket rp = getById(redPacketId);
		if (rp == null) {
			throw new GenericException("红包不存在");
		}
		if (status == MemberRedPacket.STATUS_ENDALBED) {
			if (rp.getEndDatetime().before(new Date())) {
				throw new GenericException("启用失败,红包结束时间小于当前时间！");
			}
			if (redPacketDao.checkDatetime(rp.getStationId(), status, rp.getBeginDatetime(), rp.getEndDatetime())) {
				throw new GenericException("启用失败,该红包时间和其他启用红包时间冲突！");
			}
		}

		if (rp.getStatus() != status) {
			redPacketDao.updStatus(redPacketId, status);
			String statusStr = "禁用";
			if (status == MemberRedPacket.STATUS_ENDALBED) {
				statusStr = "启用";
				initRedPacketRedis(rp);
			}
			SysLogUtil.log("修改红包,名称：" + rp.getTitle() + "状态为：" + statusStr, LogType.AGENT_FINANCE);
			RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_red_packet_" + redPacketId);
			RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_red_packet_station_" + rp.getStationId());
		}
	}

	public void initRedPacketRedis(MemberRedPacket rp) {
		if (RedisAPI.exists("MEMBER_RED_PACKET_money_list_" + rp.getId(), MEMBER_RED_PACKET_REDIS_DB)) {
			return;
		}
		List<MemberRedPacketRecord> rlist = redPacketRecordDao.getRecordList(rp.getStationId(), null, rp.getId(), null,
				null, null);
		BigDecimal money = BigDecimal.ZERO;
		String[] accIdArr = null;
		int i = 0;
		if (rlist != null) {
			accIdArr = new String[rlist.size()];
			for (MemberRedPacketRecord r : rlist) {
				if (r.getMoney() != null) {
					money = money.add(r.getMoney());
				}
				accIdArr[i] = r.getAccountId().toString();
				i++;
			}
		}
		money = rp.getTotalMoney().subtract(money);
		if (money.compareTo(BigDecimal.ZERO) > 0) {
			List<BigDecimal> moneyList = RedPacketUtil.splitRedPackets(money, rp.getTotalNumber() - i,
					rp.getMinMoney());
			String[] arr = new String[moneyList.size()];
			int j = 0;
			for (BigDecimal b : moneyList) {
				arr[j] = b.toString();
				j++;
			}
			RedisAPI.rpush("MEMBER_RED_PACKET_money_list_" + rp.getId(), MEMBER_RED_PACKET_REDIS_DB, arr);
			if (accIdArr != null && accIdArr.length>0) {
//				RedisAPI.sadd("MEMBER_RED_PACKET_accid_Set_" + rp.getId(), MEMBER_RED_PACKET_REDIS_DB, accIdArr);
			}
		}
	}

	@Override
	public MemberRedPacket saveRedPacket(MemberRedPacket mrp, Long[] groupLevelIds) {
		return saveRedPacket(mrp, groupLevelIds,null,null,null,null);
	}
	
	@Override
	public MemberRedPacket saveRedPacket(MemberRedPacket mrp, Long[] groupLevelIds,
			Long createType,Long recordNum,BigDecimal beginMoney,BigDecimal endMoney) {
		if (mrp == null) {
			throw new GenericException("参数错误");
		}
		if (StringUtil.isEmpty(mrp.getTitle())) {
			throw new GenericException("标题不能为空");
		}
		if (mrp.getTotalMoney() == null || BigDecimal.ZERO.compareTo(mrp.getTotalMoney()) >= 0) {
			throw new GenericException("红包金额不能为空！");
		}

		if (mrp.getTotalNumber() == null || mrp.getTotalNumber() <= 0) {
			throw new GenericException("红包个数不能为空！");
		}
		if (mrp.getIpNumber() == null || mrp.getIpNumber() <= 0) {
			throw new GenericException("限制IP次数不能为空！");
		}

		if (mrp.getMinMoney() == null || BigDecimal.ZERO.compareTo(mrp.getMinMoney()) >= 0) {
			mrp.setMinMoney(new BigDecimal("0.01"));
		}
		if (BigDecimalUtil.multiplyInts(mrp.getMinMoney(), mrp.getTotalNumber()).compareTo(mrp.getTotalMoney()) > 0) {
			throw new GenericException("红包金额不能小于红包个数乘于最小金额！");
		}

		Date curDate = new Date();

		if (mrp.getBeginDatetime() == null || curDate.after(mrp.getBeginDatetime())) {
			throw new GenericException("开始时间不能为空！且不能小于当前时间！");
		}

		if (mrp.getEndDatetime() == null || mrp.getBeginDatetime().after(mrp.getEndDatetime())) {
			throw new GenericException("结束时间不能为空！且不能小于开始时间！");
		}
//		if (groupLevelIds == null || groupLevelIds.length == 0) {
//			throw new GenericException("至少选择一个会员等级！");
//		}
		if (mrp.getTodayDeposit() != null && mrp.getTodayDeposit().compareTo(BigDecimal.ZERO) == -1) {
			throw new GenericException("限制当天充值金额错误");
		}
		if (recordNum >= 50) {
			throw new GenericException("虚拟红包不能大于50条");
		}
		mrp.setRemainMoney(mrp.getTotalMoney());
		mrp.setRemainNumber(mrp.getTotalNumber());
		mrp.setStationId(StationUtil.getStationId());
		mrp.setStatus(MemberRedPacket.STATUS_DISABLED);
		redPacketDao.insert(mrp);
		
		//创建虚拟红包记录业务
		createFictitiousRecord(createType,mrp.getId(),mrp.getTitle(),recordNum, beginMoney, endMoney,mrp.getBeginDatetime());
		
		List<MemberRedPacketLevel> llist = new ArrayList<>();
		MemberRedPacketLevel l = null;
		if (groupLevelIds != null && groupLevelIds.length != 0) {
			Set<Long> groupLevelSet = new HashSet<>();
			for (Long glId : groupLevelIds) {
				if (!groupLevelSet.contains(glId)) {
					l = new MemberRedPacketLevel();
					l.setMemberLevelId(glId);
					l.setPacketId(mrp.getId());
					l.setStationId(mrp.getStationId());
					llist.add(l);
					groupLevelSet.add(glId);
				}
			}
			redPacketLevelService.batchInsert(llist, groupLevelSet);
		}
		
		SysLogUtil.log("新增红包,名称：" + mrp.getTitle(), LogType.AGENT_FINANCE);
		return mrp;
	}
	
	private void createFictitiousRecord(Long createType,Long redPacketId,String title,Long recordNum,BigDecimal beginMoney,BigDecimal endMoney
			,Date startDate) {
		if(Validator.isNull(createType) || StringUtil.equals(1L, createType)) {
			return;
		}
		
		if (Validator.isNull(recordNum) || recordNum.intValue() < 1) {
			throw new GenericException("红包记录至少要一条！");
		}
		
		if (Validator.isNull(beginMoney) || Validator.isNull(endMoney)
				|| beginMoney.compareTo(BigDecimal.ZERO) < 1
				|| endMoney.compareTo(BigDecimal.ZERO) < 1) {
			throw new GenericException("金额区间格式错误！");
		}
		
		if(beginMoney.compareTo(endMoney) == 1) {
			throw new GenericException("金额区间最大金额必须大于最小金额！");
		}
		
		List<MemberRedPacketFictitiousRecord> batchRecordList = new ArrayList<>();
		MemberRedPacketFictitiousRecord r = null;
		List<Map> moneys = getRandomMoneys(recordNum,beginMoney,endMoney);
		Long stationId = StationUtil.getStationId();
		for (Map map : moneys) {
			r = new MemberRedPacketFictitiousRecord();
			r.setAccount(map.get("account").toString());
			r.setMoney(StringUtil.toBigDecimal(map.get("money")));
			r.setStationId(stationId);
			r.setCreateDatetime(startDate);
			r.setRedPacketId(redPacketId);
			r.setRedPacketName(title);
			batchRecordList.add(r);
		}
		if(!batchRecordList.isEmpty()) {
			fictitiousRecordDao.batchInsert(batchRecordList);
		}
	}
	
	private List<Map> getRandomMoneys(Long recordNum,BigDecimal beginMoney,BigDecimal endMoney){
		List<Map> moneys = new ArrayList<>();
		//差额
		BigDecimal difference = endMoney.subtract(beginMoney);
		BigDecimal randomMoeny = null;
		int bs = beginMoney.scale();
		int es = endMoney.scale();
		int us = 2;
		//取最大和小最金额的最大小数位
		if(bs > us || es > us) {
			if(bs > es) {
				us = bs;
			}else {
				us = es;
			}
		}
		
		Random rd = new Random();
		float rf = 0f;
		for (int i = 0; i < recordNum.intValue(); i++) {
			rf = rd.nextFloat();
			randomMoeny = beginMoney.add(
					StringUtil.toBigDecimal(rf).multiply(difference))
					.setScale(us, BigDecimal.ROUND_DOWN);
			moneys.add(MixUtil.newHashMap("money",randomMoeny,"account",getRandomAccount()));
		}
		
		return moneys;
	}
	
	private String getRandomAccount() {
		//随便生成姓名
		Random rd = new Random();
		int length = Contants.PYSTR.length;
		int pyindex = 0;
		String account ="";
		pyindex = (int)(rd.nextFloat()*(length))+1;
		if(pyindex != 1) {
			pyindex = length%pyindex;
		}
		account += Contants.PYSTR[pyindex];
		return account;
	}

	@Override
	public MemberRedPacket getCurrentRedPacket(Long stationId) {
		List<MemberRedPacket> redPackets = getRedPacketList(stationId);
		if (redPackets == null || redPackets.isEmpty()) {
			return null;
		}
		Collections.sort(redPackets,new Comparator<MemberRedPacket>() {
			@Override
			public int compare(MemberRedPacket o1, MemberRedPacket o2) {
				return o1.getEndDatetime().compareTo(o2.getEndDatetime());
			}
		});
		Date curDate = new Date();
		for (MemberRedPacket memberRedPacket : redPackets) {
			if (memberRedPacket.getEndDatetime().after(curDate)) {
				Map<Object,Object> map = redPacketRecordDao.getMoneyAndCount(memberRedPacket.getId());
				if(map != null){
					int num = memberRedPacket.getTotalNumber() - Integer.valueOf(map.get("num").toString());
					BigDecimal money = memberRedPacket.getRemainMoney().subtract(new BigDecimal(String.valueOf(map.get("money"))==""?"0":String.valueOf(map.get("money"))));
					if(num >= 0 && money != null){
						memberRedPacket.setRemainNumber(num);
						memberRedPacket.setRemainMoney(money);
					}
				}
				return memberRedPacket;
			}
		}
		return null;
	}

	@Override
	public List<MemberRedPacket> getRedPacketList(Long stationId) {
		String key = "MEMBER_RED_PACKET_red_packet_station_" + stationId;
		String json = RedisAPI.getCache(key, MEMBER_RED_PACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSONArray.parseArray(json, MemberRedPacket.class);
		}
		List<MemberRedPacket> list = redPacketDao.getRedPacketList(stationId, MemberRedPacket.STATUS_ENDALBED);

		if (list != null) {
			RedisAPI.addCache(key, JSON.toJSONString(list), 0, MEMBER_RED_PACKET_REDIS_DB);
		}
		return list;
	}

	/**
	 * 抢红包
	 */
	@Override
	public BigDecimal grabRedPacket(Long id) {
		
		if (Validator.isNull(id)) {
			throw new GenericException("活动已结束");
		}
		MemberRedPacket rp = getById(id);
		if (rp == null || !Objects.equals(rp.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("活动已结束");
		}
		Date curDate = new Date();
		if (rp.getBeginDatetime().after(curDate)) {
			throw new GenericException("活动未开始!");
		}
		if (rp.getEndDatetime().before(curDate)) {
			throw new GenericException("活动已结束");
		}
		
		//先判断红包额度问题
		Map<Object,Object> map = redPacketRecordDao.getMoneyAndCount(rp.getId());
		if(map != null){
			int num = rp.getTotalNumber() - Integer.valueOf(map.get("num").toString());
			BigDecimal money = rp.getRemainMoney().subtract(new BigDecimal(String.valueOf(map.get("money"))==""?"0":String.valueOf(map.get("money"))));
			if(num >= 0 && money != null){
				rp.setRemainNumber(num);
				rp.setRemainMoney(money);
			}
		}
		if(rp.getRemainMoney().compareTo(BigDecimal.ZERO) == 0){
			throw new GenericException("您来晚一步,红包已被抢完!");
		}
		
		String ip = UserUtil.getIpAddress();
//		String idGrabCountKey = "MEMBER_RED_PACKET_ID_" + id + "_" + UserUtil.getUserId();//一个会员只能抢一次红包
//		int idcount = NumberUtils.toInt(RedisAPI.getCache(idGrabCountKey, MEMBER_RED_PACKET_REDIS_DB));
//		if (idcount >= 0) {
//			throw new GenericException("您已经参与过该活动！");
//		}
		
		String ipGrabCountKey = "MEMBER_RED_PACKET_IP_" + rp.getId() + "_ip_" + ip;//ip限制
		int ipcount = NumberUtils.toInt(RedisAPI.getCache(ipGrabCountKey, MEMBER_RED_PACKET_REDIS_DB));
		if (ipcount >= rp.getIpNumber()) {
			throw new GenericException("您的IP已经参与过该活动！");
		}
		SysAccount acc = accountDao.findOneByAccountIdAndStationId(UserUtil.getUserId(),rp.getStationId());
		if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
			throw new GenericException("此账号不能参与该活动！");
		}
		
		Set<Long> memberLevelSet = redPacketLevelService.findLevelsByRedPacketId(id);
		if (memberLevelSet != null && !memberLevelSet.isEmpty()) {
			if (acc.getLevelGroup() == null || !memberLevelSet.contains(acc.getLevelGroup())) {
				throw new GenericException("你当前等级不能参与抢红包！");
			}
		}
		BigDecimal t = null;
		if(rp.getTodayDeposit()!=null && rp.getTodayDeposit().compareTo(BigDecimal.ZERO) == 1) {
			//判断今日有没有充值
			t = mnyComRecordDao.countDayPayBigDecimal(acc.getId(), new Date(), rp.getStationId());
			if(t == null) {
				t = BigDecimal.ZERO;
			}
			
			if(rp.getTodayDeposit().compareTo(t) == 1) {
				throw new GenericException("您距达到抢红包资格还需要充值【"+rp.getTodayDeposit().subtract(t).floatValue()+"】！");
			}
		}
		//

//		if (RedisAPI.sadd("MEMBER_RED_PACKET_accid_Set_" + id, MEMBER_RED_PACKET_REDIS_DB,
//				acc.getId().toString()) == 0) {
//			throw new GenericException("你已参加过该活动！");
//		}
		
		
		BigDecimal returnBig = BigDecimalUtil
				.toBigDecimal(RedisAPI.lpop("MEMBER_RED_PACKET_money_list_" + id, MEMBER_RED_PACKET_REDIS_DB));
		//如果金额为空 将红包初始化
		if (returnBig == null) {
			initRedPacketRedis(rp);
			returnBig = BigDecimalUtil
					.toBigDecimal(RedisAPI.lpop("MEMBER_RED_PACKET_money_list_" + id, MEMBER_RED_PACKET_REDIS_DB));
		}

		MemberRedPacketRecord record = new MemberRedPacketRecord();
		record.setAccountId(acc.getId());
		record.setAccount(acc.getAccount());
		record.setCreateDatetime(curDate);
		record.setMoney(returnBig);
		record.setRedPacketId(id);
		record.setStationId(rp.getStationId());
		record.setRedPacketName(rp.getTitle());
		record.setIp(ip);
		record.setStatus(MemberRedPacketRecord.STATUS_UNTREATED);
		
		//去掉红包记录表索引后的处理   只能抢一个红包   使用多个红包后再打开这个注释
//		if(!"on".equals(grapmanyonoff)) {
//			//手动判断红包记录表中有没有
//			int j = redPacketRecordDao.accountRedPacketCount(acc.getId(), id);
//			if(j>0) {
//				throw new GenericException("抢红包只能参与一次！");
//			}
//		}
		try {
			redPacketRecordDao.save(record);
		} catch (Exception e) {
			throw new GenericException("抢红包只能参与一次！");
		}
		ipcount = RedisAPI.incrby(ipGrabCountKey, 1, MEMBER_RED_PACKET_REDIS_DB).intValue();
		if (ipcount == 1) {// 一个会员一天只能抢一次 同一个红包
			RedisAPI.expire(ipGrabCountKey, 86400, MEMBER_RED_PACKET_REDIS_DB);
		}
		return returnBig;
	}

	@Override
	public List<MemberRedPacketRecord> getMarqueeRecord(Long stationId, Long redPacketId) {
		
		MemberRedPacket curRedPacket = getCurrentRedPacket(stationId);
		boolean catchFlag = false;
		Long searchId = 0l;
		//红包开始后一分钟内不返回记录
		if(curRedPacket != null) {
			Calendar c = Calendar.getInstance();
			Date now = c.getTime();
			Date begin = curRedPacket.getBeginDatetime();
			c.setTime(begin);
			c.add(Calendar.SECOND, -10);
			Date beginB10 = c.getTime();
			c.add(Calendar.SECOND, 40);
			Date beginA30 = c.getTime();
			if(now.after(beginB10) && now.before(beginA30) ){
				return null;
			}else if(begin.before(now)) {
				searchId = redPacketId;
				catchFlag = true;
			}
		}
		
		String marqueeKey = "MEMBER_RED_PACKET_red_packet_record_" + stationId + "_rpid_" + redPacketId+"_cf"+catchFlag;
		String json = RedisAPI.getCache(marqueeKey, MEMBER_RED_PACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSONArray.parseArray(json, MemberRedPacketRecord.class);
		}
		
		//真假红包记录数
		String redpacketLimint = StationConfigUtil.get(StationConfig.member_red_packet_record_limit);
		List<MemberRedPacketFictitiousRecord> flist = null;
		List<MemberRedPacketRecord> newList = new ArrayList<>();
		MemberRedPacketRecord r = null;
		
		Integer realLimit = 20;
		Integer fictitiousLimit = 0;
		String[] limit = null;
		if(StringUtil.isNotEmpty(redpacketLimint)) {
			limit = redpacketLimint.split(",");
			if(limit != null && limit.length > 1) {
				realLimit = StringUtil.toInteger(limit[0], 20);
				fictitiousLimit = StringUtil.toInteger(limit[1], 0);
				flist = redPacketRecordFictitiousDao.getRecordList(stationId,redPacketId, fictitiousLimit,catchFlag);
			}
		}
		
		//假记录条数
		if(flist != null && !flist.isEmpty()) {
			for (MemberRedPacketFictitiousRecord f : flist) {
				r = new MemberRedPacketRecord();
				r.setAccount(f.getAccount());
				r.setMoney(f.getMoney());
				r.setCreateDatetime(f.getCreateDatetime());
				r.setStationId(f.getStationId());
				newList.add(r);
			}
		}
		
		//真实红包记录
		List<MemberRedPacketRecord> marquees = redPacketRecordDao.getRecordList(stationId,
				MemberRedPacketRecord.STATUS_SUCCESS, searchId, null, null, realLimit);
		if (marquees != null && !marquees.isEmpty()) {
			newList.addAll(marquees);
		}
		
		//增加红包排序规则
		
		if(newList.isEmpty()) {
			return newList;
		}
		String sort = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.set_red_sort);
		//降序\默认
		if((StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.DROP.getValue())) || StringUtils.isEmpty(sort)) {
			Collections.sort(newList,new Comparator<MemberRedPacketRecord>() {
				@Override
				public int compare(MemberRedPacketRecord o1, MemberRedPacketRecord o2) {
					return o2.getMoney().compareTo(o1.getMoney());
				}
			});
		}
		//升序
		if(StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.RISE.getValue())) {
			Collections.sort(newList,new Comparator<MemberRedPacketRecord>() {
				@Override
				public int compare(MemberRedPacketRecord o1, MemberRedPacketRecord o2) {
					return o1.getMoney().compareTo(o2.getMoney());
				}
			});
		}
		//无序
		if(StringUtils.isNotEmpty(sort) && sort.equals(RedSortEnum.DISORDERLY.getValue())) {
			Collections.shuffle(newList);
		}
		RedisAPI.addCache(marqueeKey, JSON.toJSONString(newList), 10, MEMBER_RED_PACKET_REDIS_DB);
		return newList;
	}

	@Override
	public MemberRedPacketRecord getRedPacketRecord(Long id, Long stationId) {
		MemberRedPacketRecord record = redPacketRecordDao.get(id);
		if (record == null || !StringUtil.equals(record.getStationId(), stationId)) {
			return null;
		}
		return record;
	}

	@Override
	public void handlerRecord(Long id, Integer status, String remark) {
		MemberRedPacketRecord record = redPacketRecordDao.get(id);
		if (record == null || !Objects.equals(record.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求");
		}
		record.setStatus(status);
		if (StringUtil.isEmpty(remark)) {
			remark = "";
		}
		record.setRemark("手动结算：" + remark);
		if (!balanceAndRecord(record)) {
			throw new GenericException("处理失败！");
		}
		SysLogUtil.log("手动结算红包：" + record.getRedPacketName() + " account=" + record.getAccount() + "  备注：" + remark,
				LogType.AGENT_FINANCE);
	}

	/**
	 * 发放奖金并写账变记录
	 * 
	 * @param record
	 */
	@Override
	public boolean balanceAndRecord(MemberRedPacketRecord record) {
		MemberRedPacket rp = getById(record.getRedPacketId());
		if(rp==null){
			//红包删除中奖记录作废
			if(StringUtil.equals(record.getStatus(), MemberRedPacketRecord.STATUS_SUCCESS)) {
				record.setStatus(MemberRedPacketRecord.STATUS_CANCELED);
			}
		}
		
		boolean success = redPacketRecordDao.hanlderUntreated(record) > 0;
		if (success && StringUtil.equals(record.getStatus(), MemberRedPacketRecord.STATUS_SUCCESS)) {
			if (record.getMoney() == null || record.getMoney().compareTo(BigDecimal.ZERO) <= 0) {
				return success;
			}
			String remark = "抢红包活动中，获得红包：" + record.getRedPacketName()+" 中奖金额：" + record.getMoney() + "元";
			MnyMoneyVo mvo = new MnyMoneyVo();
			mvo.setAccountId(record.getAccountId());
			mvo.setMoney(record.getMoney());
			mvo.setMoneyRecordType(MoneyRecordType.ACTIVE_AWARD);
			mvo.setRemark(remark);
			mvo.setOrderId(record.getId() + "");
			mvo.setBizDatetime(record.getCreateDatetime());
			moneyService.updMnyAndRecord(mvo);
			
			if (rp.getBetRate() != null && rp.getBetRate().compareTo(BigDecimal.ZERO) > 0) {
				// 取得消费比例 红包打码量
				mnyBetNumberDao.updateDrawCheck(record.getAccountId(), record.getStationId(),
						record.getMoney().multiply(rp.getBetRate()), BetNumRecord.TYPE_RED_PACKET, "红包开奖", false);
				// mnyBetNumberDao.addDrawNeed(record.getAccountId(),
				// record.getMoney().multiply(rp.getBetRate()),
				// BetNumRecord.TYPE_RED_PACKET, "红包开奖", record.getCreateDatetime());
			}
			String key = "MEMBER_RED_PACKET_red_packet_record_" + record.getStationId() + "_rpid_"
					+ record.getRedPacketId();
			RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, key);
		}
		return success;
	}

	/**
	 * 活动中奖记录处理器
	 * 
	 * @param active
	 */
	@Override
	public List<MemberRedPacketRecord> getUntreatedRecords(Date end, Integer limit) {
		return redPacketRecordDao.getRecordList(null, MemberRedPacketRecord.STATUS_UNTREATED, null, null, end, limit);
	}

	@Override
	public void delRedPacket(Long id) {
		if (id == null || id <= 0)
			return;
		MemberRedPacket old = getById(id);
		if (old == null)
			return;
		redPacketDao.delete(id);
		redPacketLevelService.deleteByRedPacketId(id);
		SysLogUtil.log("删除红包,名称：" + old.getTitle(), LogType.AGENT_FINANCE);
		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_red_packet_" + id);
		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_red_packet_station_" + old.getStationId());
		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB,
				"MEMBER_RED_PACKET_red_packet_record_" + old.getStationId() + "_rpid_" + id);
		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_money_list_" + id);
//		RedisAPI.delCache(MEMBER_RED_PACKET_REDIS_DB, "MEMBER_RED_PACKET_accid_Set_" + id);
//		RedisAPI.delCacheByPrefix("MEMBER_RED_PACKET_IP_" + id, MEMBER_RED_PACKET_REDIS_DB);
	}

	@Override
	public Page<MemberRedPacketFictitiousRecord> getFictitiousPage(Long stationId, Date begin, Date end) {
		return redPacketRecordFictitiousDao.getRecordPage(stationId, begin, end);
	}
}
