package com.game.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.MemberBalanceGemStrategyLevelDao;
import com.game.dao.MnyComStrategyLevelDao;
import com.game.service.MemberBalanceGemStrategyLevelService;
import com.game.service.MnyComStrategyLevelService;

@Service
public class MemberBalanceGemStrategyLevelServiceImpl implements MemberBalanceGemStrategyLevelService {
	@Autowired
	private MemberBalanceGemStrategyLevelDao memberBalanceGemStrategyLevelDao;
	
	@Override
	public Set<Long> getLevelSet(Long strategyId, Long stationId) {
		List<Long> list =memberBalanceGemStrategyLevelDao.findLevels(strategyId,stationId);
		if(list==null || list.isEmpty()){
			return new HashSet<>();
		}
		return new HashSet<>(list);
	}
}
