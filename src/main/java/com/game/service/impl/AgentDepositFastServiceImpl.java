package com.game.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.AgentDepositFastDao;
import com.game.dao.SysAccountDao;
import com.game.model.AgentDepositFast;
import com.game.model.AgentDepositLevel;
import com.game.model.SysAccount;
import com.game.model.SysPayPlatform;
import com.game.service.AgentDepositFastService;
import com.game.service.AgentDepositLevelService;
import com.game.service.SysPayPlatformService;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentDepositFastServiceImpl implements AgentDepositFastService {

	@Autowired
	private AgentDepositFastDao adfDao;

	@Autowired
	private SysAccountDao accountDao;
	@Autowired
	private AgentDepositLevelService depositLevelService;
	@Autowired
	private SysPayPlatformService payPlatformService;
	
	@Override
	public Page getFastPage(Long payPlatformId) {
		return adfDao.getPage(payPlatformId);
	}

	@Override
	public void save(AgentDepositFast fast, Long[] groupLevelIds) {
		if (fast == null) {
			throw new GenericException("数据异常");
		}
		Set<Long> levelIdSet = null;
		Long stationId = StationUtil.getStationId();
		if (fast.getId() != null && fast.getId() > 0) {
			AgentDepositFast saveFast = adfDao.get(fast.getId());
			saveFast.setPayAccount(fast.getPayAccount());
			saveFast.setPayUserName(fast.getPayUserName());
			saveFast.setMin(fast.getMin());
			saveFast.setMax(fast.getMax());
			saveFast.setStatus(fast.getStatus());
			saveFast.setSortNo(fast.getSortNo());
			saveFast.setQrCodeImg(fast.getQrCodeImg());
			saveFast.setPayPlatformId(fast.getPayPlatformId());
			saveFast.setIcon(fast.getIcon());
			saveFast.setFrontLabel(fast.getFrontLabel());
			saveFast.setQrcodeDesc(fast.getQrcodeDesc());
			saveFast.setPayStarttime(fast.getPayStarttime());
			saveFast.setPayEndtime(fast.getPayEndtime());
			saveFast.setRandomStatus(fast.getRandomStatus());
			saveFast.setWalletType(fast.getWalletType());
			saveFast.setRate(fast.getRate());
			saveFast.setPlatformAddress(fast.getPlatformAddress());

			SysLogUtil.log("修改快速入款卡号：" + saveFast.getPayUserName() + "  持卡人：" + saveFast.getPayAccount(),
					LogType.AGENT_FINANCE);
			adfDao.update(saveFast);
			levelIdSet = depositLevelService.getLevelIdsByDepositId(saveFast.getId(), stationId,
					AgentDepositLevel.TYPE_FAST);
		} else {
			fast.setStationId(StationUtil.getStationId());
			SysLogUtil.log("修改快速入款卡号：" + fast.getPayUserName() + "  持卡人：" + fast.getPayAccount(),
					LogType.AGENT_FINANCE);
			adfDao.insert(fast);
		}
		if (groupLevelIds != null) {
			List<AgentDepositLevel> dllist = new ArrayList<>();
			AgentDepositLevel dl = null;
			for (Long lid : groupLevelIds) {
				if (levelIdSet != null && levelIdSet.contains(lid)) {
					levelIdSet.remove(lid);
					continue;
				}
				dl = new AgentDepositLevel();
				dl.setDepositId(fast.getId());
				dl.setStationId(stationId);
				dl.setMemberLevelId(lid);
				dl.setType(AgentDepositLevel.TYPE_FAST);
				dllist.add(dl);
			}
			depositLevelService.batchInsert(dllist);
			if(levelIdSet!=null && !levelIdSet.isEmpty()){
				depositLevelService.batchDelete(fast.getId(), stationId, AgentDepositLevel.TYPE_FAST, new ArrayList<>(levelIdSet));
			}
		}
	}

	@Override
	public List<Map> getStationFasts() {
		return getStationFasts(null);
	}
	@Override
	public List<Map> getStationFasts(String code) {
		List<Map> fastPays = adfDao.getFasts(code);
		List<Map> newOP = new ArrayList();
		Object payStartTime = "";
		Object payEndTime = "";
		Date startTime = null;
		Date endTime = null;
		Date now = new Date();
		String curDateStr = DateUtil.getCurrentDate();
		for (Map map : fastPays) {
			payStartTime = map.get("payStarttime");
			payEndTime = map.get("payEndtime");
			if(StringUtil.isEmpty(payStartTime) && StringUtil.isEmpty(payEndTime)) {
				newOP.add(map);
				continue;
			}
			if(StringUtil.isNotEmpty(payStartTime) ) {
				startTime = DateUtil.parseDate(curDateStr +" "+payStartTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(StringUtil.isNotEmpty(payEndTime) ) {
				endTime = DateUtil.parseDate(curDateStr +" "+payEndTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(startTime == null && endTime == null) {
				newOP.add(map);
				continue;
			}
			if(startTime == null && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
			
			if(endTime == null && now.after(startTime)) {
				newOP.add(map);
				continue;
			}
			if(now.after(startTime) && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
		}
		List<Map> resultPays = new ArrayList<Map>();
		Set<Long> levelIdSet = null;
		SysAccount user = accountDao.get(UserUtil.getUserId());
		Map<String,List<Map>> handlerMap = new HashMap<>();
		
		boolean show = isShowGuestPayment(StationConfig.onoff_show_pay_quick);
		
		for (Map pay : newOP) {
			levelIdSet=depositLevelService.getLevelIdsByDepositId(getLong(pay.get("id")), user.getStationId(), AgentDepositLevel.TYPE_FAST);
			if ((levelIdSet == null || levelIdSet.isEmpty() || levelIdSet.contains(user.getLevelGroup())) && show) {
				
				if(StringUtil.equals(StringUtil.toLong(pay.get("randomStatus")),AgentDepositFast.RANNDOM_ENABLE)) {
					containsSet(pay,handlerMap);
				}else {
					resultPays.add(pay);
				}
			}
		}
		List<Map> randomList = null;
		Random random = new Random();
		if(!handlerMap.isEmpty()) {
			for (String key : handlerMap.keySet()) {
				randomList = handlerMap.get(key);
				if(randomList != null && !randomList.isEmpty()) {
					resultPays.add(randomList.get(random.nextInt(randomList.size())));
				}
			}
		}
		
		return resultPays;
	}
	
	/**
	 * 是否显示引导账号充值的判断
	 * @param payEnum
	 * @param string
	 * @return
	 */
	private boolean isShowGuestPayment(Enum payEnum) {
		Boolean flag = true;

		String string = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.guide_show_payment);

		Boolean guest = UserUtil.isGuestAccount();
		// 是引导账号
		if (guest) {
			// 有配置东西
			if (StringUtil.isNotEmpty(string)) {
				if (!string.contains("1") && payEnum.equals(StationConfig.onoff_show_pay_third)) {
					flag = false;
				} else if (!string.contains("2") && payEnum.equals(StationConfig.onoff_show_pay_quick)) {
					flag = false;
				} else if (!string.contains("3") && payEnum.equals(StationConfig.onoff_show_pay_normal)) {
					flag = false;
				}
			} else {
				//没选的情况
				flag = false;
			}
		}
		return flag;
	}
	
	private void containsSet(Map pay,Map<String,List<Map>> handlerMap) {
		//同类型启用随机先放到集合里
		String typeKey = pay.get("payPlatformId").toString();
		if(!handlerMap.containsKey(typeKey)) {
			handlerMap.put(typeKey, new ArrayList<Map>());
		}
		handlerMap.get(typeKey).add(pay);
	}
	
	@Override
	public List<Map> periodPay() {
		List<Map> fastPays = adfDao.getFasts(null);
		List<Map> newOP = new ArrayList();
		Object payStartTime = "";
		Object payEndTime = "";
		Date startTime = null;
		Date endTime = null;
		Date now = new Date();
		String curDateStr = DateUtil.getCurrentDate();
		for (Map map : fastPays) {
			payStartTime = map.get("payStarttime");
			payEndTime = map.get("payEndtime");
			if(StringUtil.isEmpty(payStartTime) && StringUtil.isEmpty(payEndTime)) {
				newOP.add(map);
				continue;
			}
			if(StringUtil.isNotEmpty(payStartTime) ) {
				startTime = DateUtil.parseDate(curDateStr +" "+payStartTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(StringUtil.isNotEmpty(payEndTime) ) {
				endTime = DateUtil.parseDate(curDateStr +" "+payEndTime.toString(), "yyyy-MM-dd HH:mm:ss");
			}
			if(startTime == null && endTime == null) {
				newOP.add(map);
				continue;
			}
			if(startTime == null && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
			
			if(endTime == null && now.after(startTime)) {
				newOP.add(map);
				continue;
			}
			if(now.after(startTime) && now.before(endTime)) {
				newOP.add(map);
				continue;
			}
		}
		List<Map> resultPays = new ArrayList<Map>();
		Set<Long> levelIdSet = null;
		SysAccount user = accountDao.get(UserUtil.getUserId());
		Map<String,List<Map>> handlerMap = new HashMap<>();
		
		boolean show = isShowGuestPayment(StationConfig.onoff_show_pay_quick);
		
		for (Map pay : newOP) {
			levelIdSet=depositLevelService.getLevelIdsByDepositId(getLong(pay.get("id")), user.getStationId(), AgentDepositLevel.TYPE_FAST);
			if ((levelIdSet == null || levelIdSet.isEmpty() || levelIdSet.contains(user.getLevelGroup())) && show) {
				if(StringUtil.equals(StringUtil.toLong(pay.get("randomStatus")),AgentDepositFast.RANNDOM_ENABLE)) {
					containsSet(pay,handlerMap);
				}else {
					resultPays.add(pay);
				}
			}
		}
		
		List<Map> randomList = null;
		Random random = new Random();
		if(!handlerMap.isEmpty()) {
			for (String key : handlerMap.keySet()) {
				randomList = handlerMap.get(key);
				if(randomList != null && !randomList.isEmpty()) {
					resultPays.add(randomList.get(random.nextInt(randomList.size())));
				}
			}
		}
		return resultPays;

	}
	private Long getLong(Object val) {
		if(val instanceof Integer){
			return new Long ((Integer)val);
		}
		if(val instanceof Long){
			return (Long)val;
		}
		return null;
	}

	@Override
	public void delFast(long id) {
		AgentDepositFast fast = adfDao.get(id);
		SysPayPlatform payPlatform = payPlatformService.getPayPlatform(fast.getPayPlatformId());
		adfDao.delete(id);
		depositLevelService.deleteByDeposiId(id,StationUtil.getStationId(),AgentDepositLevel.TYPE_FAST);
		SysLogUtil.log("删除快速入款：" + payPlatform.getName(),
				LogType.AGENT_FINANCE);
	}

	@Override
	public AgentDepositFast getOne(Long id, Long stationId) {
		if (id != null) {
			AgentDepositFast ma = adfDao.get(id);
			if (ma != null && (stationId == null || ma.getStationId().equals(stationId))) {
				SysPayPlatform form = payPlatformService.getPayPlatform(ma.getPayPlatformId());
				ma.setIconCss(form.getIconCss());
				return ma;
			}
		}
		return null;
	}

	@Override
	public void updateStatus(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		adfDao.updateStatus(status, id, stationId);
	}
}
