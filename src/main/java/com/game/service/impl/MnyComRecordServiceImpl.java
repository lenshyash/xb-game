package com.game.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.game.core.SystemConfig;
import com.game.dao.*;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.contant.EsEnum;
import com.game.elasticsearch.operations.IndexOperation;
import com.game.elasticsearch.operations.UpdateOperation;
import com.game.elasticsearch.repository.ComRecordEsRepository;
import com.game.model.*;
import com.game.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.jdbc.model.User;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.DataReader;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.platform.MemberDepositStrategyDao;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.dictionary.ScoreRecordType;
import com.game.model.platform.MemberDepositStrategy;
import com.game.model.vo.AccountVo;
import com.game.model.vo.DepositStrategyVo;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.model.vo.MnyScoreVo;
import com.game.service.MnyComRecordService;
import com.game.service.MnyComStrategyService;
import com.game.service.MnyMoneyService;
import com.game.service.MnyScoreService;
import com.game.service.SysMessageService;

@Repository
public class MnyComRecordServiceImpl implements MnyComRecordService {
	private Logger logger = Logger.getLogger(MnyComRecordService.class);

	@Autowired
	MnyComRecordDao comRecordDao;

	@Autowired
	SysAccountDao accountDao;
	
	@Autowired
	SysAccountInfoDao accountInfoDao;

	@Autowired
	AgentDepositBankDao bankDao;

	@Autowired
	AgentDepositFastDao fastDao;

	@Autowired
	AgentDepositOnlineDao onlineDao;

	@Autowired
	AgentDepositVirtaulDao virtaulDao;

	@Autowired
	MnyMoneyService moneyService;

	@Autowired
	SysPayPlatformDao payPlatformDao;

	@Autowired
	MemberDepositStrategyDao memberDepositStrategyDao;

	@Autowired
	MnyMoneyRecordDao moneyRecordDao;

	@Autowired
	MemberDepositInfoDao depositInfoDao;

	@Autowired
	MnyScoreService scoreService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Autowired
	private MnyComStrategyService comStrategyService;
	@Autowired
	private SysMessageService sysMessageService;

	@Autowired
	MemberLevelBaseDao levelBaseDao;
	@Autowired
	ComRecordEsRepository comRecordEsRepository;

	@Override
	public Page<MnyComRecord> getPage(MnyComRecordVo mcrVo) {

		if (StringUtil.isNotEmpty(mcrVo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(mcrVo.getStationId());
			avo.setAccount(mcrVo.getAgentName());
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			mcrVo.setParents(parents + agent.getId() + ",");
			mcrVo.setSearchSelf(true);
			mcrVo.setSelfId(agent.getId());
		}

		Long status = mcrVo.getStatus();
		if (Validator.isNotNull(status)) {
			if (StringUtil.equals(status, MnyComRecord.STATUS_UNTREATED)) {
				mcrVo.setLockFlag(MnyComRecord.LOCK_FLAG_LOCKED);
			} else if (StringUtil.equals(status, 4l)) {
				mcrVo.setStatus(MnyComRecord.STATUS_UNTREATED);
				mcrVo.setLockFlag(MnyComRecord.LOCK_FLAG_UNLOCKED);
			}
		}
		Page<MnyComRecord> page =comRecordDao.getPage(mcrVo);
//		if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
//			page = comRecordEsRepository.getPage(mcrVo);
//		}
		if (StationUtil.isAgentStation() && page != null && page.getList() != null) {
			AgentDepositBank b = null;
			AgentDepositFast fast = null;
			for (MnyComRecord r : page.getList()) {
				if (r.getType() == 7) {// 公司转账
					b = bankDao.get(r.getPayId());
					if (b != null && (r.getStationId().equals(b.getStationId()))) {
						r.setBankAccount(b.getCreatorName());
					}
				} else if (r.getType() == 6) {
					fast = fastDao.get(r.getPayId());
					if (fast != null && (r.getStationId().equals(fast.getStationId()))) {
						r.setBankAccount(fast.getPayUserName());
					}
				}
			}
		}
		return page;
	}
	@Override
	public Page<MnyComRecord> getPage2(MnyComRecordVo mcrVo) {

		if (StringUtil.isNotEmpty(mcrVo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(mcrVo.getStationId());
			avo.setAccount(mcrVo.getAgentName());
			SysAccount agent = accountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			mcrVo.setParents(parents + agent.getId() + ",");
			mcrVo.setSearchSelf(true);
			mcrVo.setSelfId(agent.getId());
		}

		Long status = mcrVo.getStatus();
	/*	if (Validator.isNotNull(status)) {
			if (StringUtil.equals(status, MnyComRecord.STATUS_UNTREATED)) {
				mcrVo.setLockFlag(MnyComRecord.LOCK_FLAG_LOCKED);
			} else if (StringUtil.equals(status, 4l)) {
				mcrVo.setStatus(MnyComRecord.STATUS_UNTREATED);
				mcrVo.setLockFlag(MnyComRecord.LOCK_FLAG_UNLOCKED);
			}
		}*/
		Page<MnyComRecord> page = comRecordDao.getPage(mcrVo);
		if (StationUtil.isAgentStation() && page != null && page.getList() != null) {
			SysAccount acc = null;
			AgentDepositBank b = null;
			AgentDepositFast fast = null;
			for (MnyComRecord r : page.getList()) {
				if (r.getModifyUserId() != null) {
					acc = accountDao.getFromCache(r.getModifyUserId());
					if (acc != null) {
						if (acc.getAccountType().equals(SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
							r.setModifyUser("--");
						} else {
							r.setModifyUser(acc.getAccount());
						}
					}
				}
				if (r.getType() == 7) {// 公司转账
					b = bankDao.get(r.getPayId());
					if (b != null && (r.getStationId().equals(b.getStationId()))) {
						r.setBankAccount(b.getCreatorName());
					}
				} else if (r.getType() == 6) {
					fast = fastDao.get(r.getPayId());
					if (fast != null && (r.getStationId().equals(fast.getStationId()))) {
						r.setBankAccount(fast.getPayUserName());
					}
				}
			}
		}
		return page;
	}
	@Override
	public MnyComRecord deposit(MnyMoneyVo moneyVo) {

		// 判断数据合理性
		SysAccount member = accountDao.getFromCache(moneyVo.getAccountId());
		if (member == null || member.getAccountType().longValue() == SysAccount.ACCOUNT_PLATFORM_TEST_GUEST) {
			throw new GenericException("会员不存在！");
		}
		Long stationId = StationUtil.getStationId();

		if (moneyVo.getPayId() == null) {
			throw new GenericException("非法请求");
		}
		BigDecimal money = moneyVo.getMoney();
		if (money == null || money.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("充值金额格式错误！");
		}
		// 验证2次充值时间间隔
		validDepositInterval(moneyVo.getAccountId(), stationId);
		// 输入安全验证
		SecurityCheckUtil.check(moneyVo.getRemark(), moneyVo.getDepositor());

		String payName = getNameAndCheckMoney(moneyVo.getMoneyRecordType(), moneyVo.getPayId(), money);
		//验证是否存在未处理充值限制
		String string = StationConfigUtil.get(stationId, StationConfig.not_handle_com_record);
		long RecordType = moneyVo.getMoneyRecordType().getType();
		int i = 0;
		if("on".equals(string)) {
			if(RecordType==6) {
				i = comRecordDao.getRecordCountByStatus(stationId, member.getId(), MnyComRecord.STATUS_UNTREATED);
			}else if(RecordType==7) {
				i = comRecordDao.getRecordCountByStatus(stationId, member.getId(), MnyComRecord.STATUS_UNTREATED);
			}
			if(i>0) {
				throw new GenericException("有尚未处理的订单"+i+"笔");
			}
		}
		BigDecimal rate = null;
		BigDecimal count = null;
		//快速入款数字货币充值处理
		if(moneyVo.getMoneyRecordType().equals(MoneyRecordType.DEPOSIT_ONLINE_FAST)){
			//获取通道
			AgentDepositFast fast = fastDao.get(moneyVo.getPayId());
			if(fast!=null){
				SysPayPlatform platform = payPlatformDao.get(fast.getPayPlatformId());
				if("virtual".equals(platform.getIconCss())){
					count = money;
					//金额等于充值货币数量*货币汇率
					money = BigDecimalUtil.multiply(money,fast.getRate());
					rate = fast.getRate();
				}
			}

		}

		// 保存入款记录
		MnyComRecord record = new MnyComRecord();
		record.setType(moneyVo.getMoneyRecordType().getType());
		record.setMemberId(moneyVo.getAccountId());
		record.setMoney(money);
		record.setAccount(member.getAccount());
		if(rate!=null){
			record.setRate(rate);
		}
		if(count!=null){
			record.setVirtualCurrencyAmount(count);
		}
		long orderNo = Snowflake.getOrderNo();
		record.setOrderNo(orderNo + "");
		//添加新的支付账号字段
		if(moneyVo.getMoneyRecordType().equals(MoneyRecordType.DEPOSIT_ONLINE_FAST)){
			record.setPayAccount(moneyVo.getRemark());
		}
		record.setStationId(stationId);
		record.setLockFlag(MnyComRecord.LOCK_FLAG_UNLOCKED);
		record.setPayId(moneyVo.getPayId());
		record.setPayName(payName);
		record.setStauts(MnyComRecord.STATUS_UNTREATED);
		record.setDepositor(moneyVo.getDepositor());
		record.setBankCode(moneyVo.getBankcode());
		record.setUserName(UserUtil.getUserAccount());
		comRecordDao.save(record);
//		//同步充值记录到es
//		if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
//			Map map = accountDao.getAccountById(record.getMemberId(),record.getStationId());
//			record.setUserName((String) map.get("userName"));
//			record.setAgentName((String) map.get("agentName"));
//			record.setAccountType(Long.valueOf(map.get("accountType")+""));
//			if(map.containsKey("reportType")){
//				record.setReportType(Long.valueOf(map.get("reportType")+""));
//			}
//			if(map.containsKey("abnormalFlag")){
//				record.setAbnormalFlag(Long.valueOf(map.get("abnormalFlag")+""));
//			}
//			record.setParentNames((String)map.get("parentNames"));
//			record.setParents((String)map.get("parents"));
//			MemberLevelBase level=levelBaseDao.get(Long.valueOf(map.get("levelGroup")+""));
//			record.setLevelName(level.getLevelName());
//			MnyComRecord newData = new MnyComRecord();
//			BeanDtoVoUtils.beanToBean(record,newData);
//			ElasticApi.addQueue(new IndexOperation<>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),newData));
//		}

		addCountOfUntreated(stationId);
		
		//引导账号直接充值成功  第三方支付不给
		boolean autoonoff = "on".equals(StationConfigUtil.get(stationId, StationConfig.guide_account_deposit_auto_onoff));
		if(UserUtil.isGuestAccount() && autoonoff && StringUtil.isNotEmpty(moneyVo.getMoneyRecordType()) && moneyVo.getMoneyRecordType() !=MoneyRecordType.DEPOSIT_ONLINE_THIRD
				&& moneyVo.getMoneyRecordType() !=MoneyRecordType.DEPOSIT_VIRTUAL_BANK) {
			MnyComRecord order = getByOrderNo(orderNo+"");
			
			MnyComRecordVo cvrVo = new MnyComRecordVo();
			cvrVo.setStatus(MnyComRecord.STATUS_SUCCESS);
			cvrVo.setFee(BigDecimal.ZERO);
			cvrVo.setMoney(money);
			cvrVo.setId(order.getId());
			cvrVo.setOpDesc("会员自动充值");
			
			comHandlerNoNeedLock(cvrVo);
		}

		
		return record;
	}

	/**
	 * 验证2次充值时间间隔
	 * 
	 * @param accountId
	 * @param stationId
	 */
	private void validDepositInterval(Long accountId, Long stationId) {
		String depositIntervalTimes = StationConfigUtil.get(stationId, StationConfig.deposit_interval_times);
		if (StringUtils.isNoneEmpty(depositIntervalTimes)) {
			int seconds = NumberUtils.toInt(depositIntervalTimes, 0);
			if (seconds > 0) {
				Calendar c = Calendar.getInstance();
				c.add(Calendar.SECOND, -seconds);
				long count = comRecordDao.countPayCount(accountId, c.getTime(), stationId);
				if (count > 0) {
					throw new GenericException(depositIntervalTimes + "秒内只能充值一次");
				}
			}
		}
	}

	private String getNameAndCheckMoney(MoneyRecordType type, long id, BigDecimal money) {
		Long payPlatformId = 0l;
		BigDecimal min = BigDecimal.ZERO;
		BigDecimal max = BigDecimal.ZERO;

		if (type == MoneyRecordType.DEPOSIT_ONLINE_BANK) {
			AgentDepositBank bank = bankDao.get(id);
			if (bank != null) {
				if(bank.getStatus()!=bank.STATUS_ENABLE) {
					throw new GenericException("充值已关闭");
				}
				payPlatformId = bank.getPayPlatformId();
				min = bank.getMin();
				max = bank.getMax();
				// checkRoleDeposit(bank.getLevelGroup());//TODO
				
			}
		} else if (type == MoneyRecordType.DEPOSIT_ONLINE_FAST) {
			AgentDepositFast fast = fastDao.get(id);
			if (fast != null) {
				if(fast.getStatus()!=fast.STATUS_ENABLE) {
					throw new GenericException("充值已关闭");
				}
				payPlatformId = fast.getPayPlatformId();
				min = fast.getMin();
				max = fast.getMax();
				// checkRoleDeposit(fast.getLevelGroup());//TODO
			}
		} else if (type == MoneyRecordType.DEPOSIT_ONLINE_THIRD) {
			AgentDepositOnline online = onlineDao.get(id);
			if (online != null) {
				if(online.getStatus()!=online.STATUS_ENABLE) {
					throw new GenericException("充值已关闭");
				}
				payPlatformId = online.getPayPlatformId();
				min = online.getMin();
				max = online.getMax();
				// checkRoleDeposit(online.getLevelGroup());//TODO
			}
		}  else if (type == MoneyRecordType.DEPOSIT_VIRTUAL_BANK) {
			AgentDepositVirtual virtual = virtaulDao.get(id);
			if (virtual != null) {
				if(virtual.getStatus()!=virtual.STATUS_ENABLE) {
					throw new GenericException("充值已关闭");
				}
				payPlatformId = virtual.getPayPlatformId();
				min = virtual.getMin();
				max = virtual.getMax();
			}
		} else {
			throw new GenericException("非常请求！");
		}

		if (min != null && money.compareTo(min) == -1) {
			throw new GenericException("充值金额必须不小于:" + min.floatValue() + "！");
		}
		if (max != null) {
			//随机金额的设定    可在原来最大基础上增加0.5 
			BigDecimal max2 = max.add(new BigDecimal("0.5"));
			if(money.compareTo(max2) == 1) {
				throw new GenericException("充值金额必须不大于:" + max.floatValue() + "！");
			}
		}
		SysPayPlatform pay = payPlatformDao.get(payPlatformId);
		if (pay != null) {
			return pay.getName();
		} else {
			throw new GenericException("支付类型不存在！");
		}
	}

	@Override
	public void lock(long comId, long lockFlag) {
		User curUser = UserUtil.getCurrentUser();
		MnyComRecord com = comRecordDao.get(comId);
		if (com == null) {
			throw new GenericException("存款记录不存在！");
		}

		// 判断该记录是否被其他用户锁定
		if (com.getModifyUserId() != null && StringUtil.equals(com.getLockFlag(), MnyComRecord.LOCK_FLAG_LOCKED)
				&& !StringUtil.equals(curUser.getSysUserId(), com.getModifyUserId())) {
			SysAccount locker = accountDao.get(com.getModifyUserId());
			throw new GenericException("该记录已是" + locker.getAccount() + "锁定,您无权修改！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(com.getStauts(), MnyComRecord.STATUS_UNTREATED)
				|| StringUtil.equals(com.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}
		long oldLock = MnyComRecord.LOCK_FLAG_LOCKED;
		if (MnyComRecord.LOCK_FLAG_LOCKED == lockFlag) {
			oldLock = MnyComRecord.LOCK_FLAG_UNLOCKED;
		} else {
			lockFlag = MnyComRecord.LOCK_FLAG_UNLOCKED;
		}

		int flag = comRecordDao.updateLock(comId, oldLock, lockFlag, curUser.getSysUserId());
		if (flag != 1) {
			throw new GenericException("此记录已经被处理过！");
		}
//		//同步状态到es记录
//		if(SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN){
//			MnyComRecord mcr =new MnyComRecord();
//			mcr.setId(comId);
//			mcr.setLockFlag(lockFlag);
//			mcr.setModifyUserId(curUser.getSysUserId());
//			mcr.setModifyDatetime(new Date());
//			mcr.setModifyUser(accountInfoDao.getUserNameByAccount(curUser.getSysUserId()));
//			ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));
//		}
		if (StringUtil.equals(MnyComRecord.LOCK_FLAG_LOCKED, lockFlag)) {
			SysLogUtil.log("锁定编号为:" + comId + "的充值记录", LogType.AGENT_FINANCE);
			subCountOfUntreated(com.getStationId());
		} else {
			addCountOfUntreated(com.getStationId());
			SysLogUtil.log("取消锁定编号为:" + comId + "的充值记录", LogType.AGENT_FINANCE);
		}

	}

	@Override
	public void comHandler(MnyComRecordVo mcrVo) {
		Long comId = mcrVo.getId();
		Long status = mcrVo.getStatus();
		Long stationId = StationUtil.getStationId();
		BigDecimal fee = mcrVo.getFee();
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setHandlerType(MnyMoneyVo.HANDLERTYPE_ARTIFICIAL);
		User curUser = UserUtil.getCurrentUser();
		MnyComRecord mcr = comRecordDao.get(comId);
		if (mcr == null) {
			throw new GenericException("存款记录不存在！");
		}

		if (stationId != null && !StringUtil.equals(stationId, mcr.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (!StringUtil.equals(mcr.getLockFlag(), MnyComRecord.LOCK_FLAG_LOCKED)) {
			throw new GenericException("该记录未锁定！");
		}
		// 判断该记录是否被其他用户锁定
		if (!StringUtil.equals(curUser.getSysUserId(), mcr.getModifyUserId())) {
			SysAccount locker = accountDao.get(mcr.getModifyUserId());
			throw new GenericException("该记录已是" + locker.getLoginAccount() + "锁定,您无权修改！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mcr.getStauts(), MnyComRecord.STATUS_UNTREATED)
				|| StringUtil.equals(mcr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}

		SecurityCheckUtil.check(mcrVo.getOpDesc());

		Long oldStatus = mcr.getStauts();
		mcr.setStauts(status);
		mcr.setLockFlag(MnyComRecord.LOCK_FLAG_COMPLETED);
		mcr.setOpDesc(mcrVo.getOpDesc());
		mcr.setHandlerType(moneyVo.getHandlerType());
		if (mcrVo.getMoney() != null && mcrVo.getMoney().compareTo(mcr.getMoney()) != 0) {
			SysLogUtil.log("处理充值请求修改金额为:" + mcrVo.getMoney().toString() + "，原金额为：" + mcr.getMoney(),
					LogType.AGENT_FINANCE);
			mcr.setMoney(mcrVo.getMoney());
		}
		mcr.setModifyDatetime(new Date());
		mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT_NONE);
		if(mcrVo.getVirtualCurrencyAmount()!=null){
			mcr.setVirtualCurrencyAmount(mcrVo.getVirtualCurrencyAmount());
		}
		if(mcrVo.getRate()!=null){
			mcr.setRate(mcrVo.getRate());
		}
		Long depositCount = 1l;
		//充值成功判断是否是首充
		if(mcrVo.getStatus()==MnyComRecord.STATUS_SUCCESS){
			// 更新存款总额和次数
			MemberDepositInfo mdInfo = new MemberDepositInfo();
			mdInfo.setAccountId(mcrVo.getAccountId());
			mdInfo.setDepositTotal(mcr.getMoney());
			mdInfo.setDepositCount(depositCount);
			depositCount = depositInfoDao.handlerDeposit(mcr.getMemberId(), depositCount, mcr.getMoney());
			//是否是首充
			mcrVo.setFirstDeposit(StringUtil.equals(depositCount, 1l));
			if(mcrVo.getFirstDeposit()){
				mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT);
			}
			//是第几次充值
			mcr.setComTimes(depositCount);
		}
		int flag = comRecordDao.updatePayResult(mcr, oldStatus);
		if (flag == 1) {
//			//同步状态到es记录
//			ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));
			String title = "";
			String content = "";
			// 批准就加款，并增加账变
			if (StringUtil.equals(status, MnyComRecord.STATUS_SUCCESS)) {
				mcrVo.setAccountId(mcr.getMemberId());
				mcrVo.setDepositDate(mcr.getCreateDatetime());
				mcrVo.setType(mcr.getType());
				mcrVo.setStationId(stationId);
				mcrVo.setMoney(mcr.getMoney());
				// 更新会员提款的判断数据
				mcrVo.setDepositCount(depositCount);
				calcBetNumAndDepositGift(mcrVo);
				
				moneyVo.setAccount(mcr.getAccount());
				moneyVo.setAccountId(mcr.getMemberId());
				moneyVo.setMoney(mcr.getMoney());
				MoneyRecordType mrType = MoneyRecordType.getMoneyRecordType(mcr.getType());
				moneyVo.setMoneyRecordType(mrType);
				moneyVo.setFee(fee);
				SysLogUtil.log("批准编号为:" + comId + "会员" + mcr.getAccount() + "的充值记录", LogType.AGENT_FINANCE);
				moneyVo.setOrderId(mcr.getOrderNo());
				moneyVo.setBizDatetime(mcr.getCreateDatetime());
				moneyVo.setFirstDeposit(mcrVo.getFirstDeposit());
				
				moneyService.updMnyAndRecord(moneyVo);
				
				title = "恭喜，您的充值记录已批准";
				content = "恭喜，您充值订单" + mcr.getOrderNo() + "已审核通过。";
			} else {
				SysLogUtil.log("未批准编号为:" + comId + "会员" + mcr.getAccount() + "的充值记录", LogType.AGENT_FINANCE);
				title = "很抱歉，您的充值记录未被批准";
				content = "很抱歉，您充值订单" + mcr.getOrderNo() + "未被批准。";
				if (StringUtils.isNotEmpty(mcrVo.getOpDesc())) {
					content += "原因：" + mcrVo.getOpDesc();
				}
			}

			String rechargeRecord = StationConfigUtil.get(stationId, StationConfig.onoff_recharge_record);
			if (StringUtils.isEmpty(rechargeRecord) || "on".equals(rechargeRecord)) {
				sysMessageService.sendUserMessage(title, content, stationId, mcr.getMemberId(), mcr.getAccount());
			}
		}
	}
	
	/**
	 * 直接处理订单 不用锁定
	 */
	@Override
	public void comHandlerNoNeedLock(MnyComRecordVo mcrVo) {
		Long comId = mcrVo.getId();
		Long status = mcrVo.getStatus();
		Long stationId = StationUtil.getStationId();
		BigDecimal fee = mcrVo.getFee();
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		moneyVo.setHandlerType(MnyMoneyVo.HANDLERTYPE_ARTIFICIAL);
		User curUser = UserUtil.getCurrentUser();
		MnyComRecord mcr = comRecordDao.get(comId);
		if (mcr == null) {
			throw new GenericException("存款记录不存在！");
		}

		if (stationId != null && !StringUtil.equals(stationId, mcr.getStationId())) {
			throw new GenericException("非法请求！");
		}

		if (!StringUtil.equals(mcr.getLockFlag(), MnyComRecord.LOCK_FLAG_UNLOCKED)) {
			throw new GenericException("该记录不是未锁定状态！");
		}

		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mcr.getStauts(), MnyComRecord.STATUS_UNTREATED)
				|| StringUtil.equals(mcr.getLockFlag(), MnyDrawRecord.LOCK_FLAG_COMPLETED)) {
			throw new GenericException("此记录已经被处理过！");
		}

		SecurityCheckUtil.check(mcrVo.getOpDesc());

		Long oldStatus = mcr.getStauts();
		mcr.setStauts(status);
		mcr.setLockFlag(MnyComRecord.LOCK_FLAG_COMPLETED);
		mcr.setOpDesc(mcrVo.getOpDesc());
		mcr.setHandlerType(moneyVo.getHandlerType());
		if (mcrVo.getMoney() != null && mcrVo.getMoney().compareTo(mcr.getMoney()) != 0) {
			SysLogUtil.log("处理充值请求修改金额为:" + mcrVo.getMoney().toString() + "，原金额为：" + mcr.getMoney(),
					LogType.AGENT_FINANCE);
			mcr.setMoney(mcrVo.getMoney());
		}
		mcr.setModifyDatetime(new Date());
		mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT_NONE);
		Long depositCount = 1l;
		//充值成功判断是否是首充
		if(mcrVo.getStatus()==MnyComRecord.STATUS_SUCCESS){
			// 更新存款总额和次数
			MemberDepositInfo mdInfo = new MemberDepositInfo();
			mdInfo.setAccountId(mcrVo.getAccountId());
			mdInfo.setDepositTotal(mcr.getMoney());
			mdInfo.setDepositCount(depositCount);
			depositCount = depositInfoDao.handlerDeposit(mcr.getMemberId(), depositCount, mcr.getMoney());
			//是否是首充
			mcrVo.setFirstDeposit(StringUtil.equals(depositCount, 1l));
			if(mcrVo.getFirstDeposit()){
				mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT);
			}
			//是第几次充值
			mcr.setComTimes(depositCount);
		}
		int flag = comRecordDao.updatePayResult(mcr, oldStatus);
		if (flag == 1) {
			//同步状态到es记录
//			ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));

			String title = "";
			String content = "";
			// 批准就加款，并增加账变
			if (StringUtil.equals(status, MnyComRecord.STATUS_SUCCESS)) {
				mcrVo.setAccountId(mcr.getMemberId());
				mcrVo.setDepositDate(mcr.getCreateDatetime());
				mcrVo.setType(mcr.getType());
				mcrVo.setStationId(stationId);
				mcrVo.setMoney(mcr.getMoney());
				// 更新会员提款的判断数据
				mcrVo.setDepositCount(depositCount);
				calcBetNumAndDepositGift(mcrVo);
				
				moneyVo.setAccount(mcr.getAccount());
				moneyVo.setAccountId(mcr.getMemberId());
				moneyVo.setMoney(mcr.getMoney());
				MoneyRecordType mrType = MoneyRecordType.getMoneyRecordType(mcr.getType());
				moneyVo.setMoneyRecordType(mrType);
				moneyVo.setFee(fee);
				SysLogUtil.log("会员自动充值:" + comId + "会员" + mcr.getAccount() + "的充值记录", LogType.AGENT_FINANCE);
				moneyVo.setOrderId(mcr.getOrderNo());
				moneyVo.setBizDatetime(mcr.getCreateDatetime());
				moneyVo.setFirstDeposit(mcrVo.getFirstDeposit());
				
				moneyService.updMnyAndRecord(moneyVo);
				
				title = "恭喜，您的充值记录已批准";
				content = "恭喜，您充值订单" + mcr.getOrderNo() + "已审核通过。";
			} else {
				SysLogUtil.log("未批准编号为:" + comId + "会员" + mcr.getAccount() + "的充值记录", LogType.AGENT_FINANCE);
				title = "很抱歉，您的充值记录未被批准";
				content = "很抱歉，您充值订单" + mcr.getOrderNo() + "未被批准。";
				if (StringUtils.isNotEmpty(mcrVo.getOpDesc())) {
					content += "原因：" + mcrVo.getOpDesc();
				}
			}

			String rechargeRecord = StationConfigUtil.get(stationId, StationConfig.onoff_recharge_record);
			if (StringUtils.isEmpty(rechargeRecord) || "on".equals(rechargeRecord)) {
				sysMessageService.sendUserMessage(title, content, stationId, mcr.getMemberId(), mcr.getAccount());
			}
		}
	}

	@Override
	public Integer getMcrByAccountAndPayId(Long accountId, Long payId) {
		return comRecordDao.getMcrByAccountAndPayId(accountId,payId);
	}

	@Override
	public MnyComRecord getByMoneyAndAccount(BigDecimal money, Long accountId) {
		return comRecordDao.getByMoneyAndAccount(money,accountId);
	}

	@Override
	public Integer getCountOfUntreated(final long stationId) {

		return CacheUtil.getNull2Set(new DataReader<Integer>() {
			public Integer getData() {
				return getCountOfUntreatedByDb();
			}
		}, Integer.class, CacheType.AGENT_STATION_COM_COUNT, stationId + "");
	}

	private Integer getCountOfUntreatedByDb() {
		MnyComRecordVo mcrVo = new MnyComRecordVo();
		mcrVo.setStationId(StationUtil.getStationId());
		mcrVo.setStatus(MnyDrawRecord.STATUS_UNTREATED);
		mcrVo.setLockFlag(MnyDrawRecord.LOCK_FLAG_UNLOCKED);
		return comRecordDao.getCountOfUntreated(mcrVo);
	}

	@Override
	public void addCountOfUntreated(long stationId) {
		CacheUtil.delCache(CacheType.AGENT_STATION_COM_COUNT, stationId + "");
	}

	@Override
	public void subCountOfUntreated(long stationId) {
		addCountOfUntreated(stationId);
	}

	@Override
	public MnyComRecord getByOrderNo(String orderNo) {
		return comRecordDao.getByOrderNo(orderNo);
	}

	@Override
	public void onlinepayComHandler(MnyComRecordVo mcrVo) {
		Long comId = mcrVo.getId();
		Long status = mcrVo.getStatus();
		BigDecimal fee = mcrVo.getFee();
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setHandlerType(MnyMoneyVo.HANDLERTYPE_NOTIFY);
		MnyComRecord mcr = comRecordDao.get(comId);
		
		if (mcr == null) {
			throw new GenericException("存款记录不存在！");
		}
		// 判断记录是否处理未处理状态
		if (!StringUtil.equals(mcr.getStauts(), MnyComRecord.STATUS_UNTREATED)) {
			throw new GenericException("此记录已经被处理过！");
		}
		
		//检查异步回调金额 实际金额以异步为准
		Boolean checkNotifyMoneyFlag = mcrVo.getCheckNotifyMoneyFlag();
		if(StringUtil.isNotEmpty(checkNotifyMoneyFlag) && checkNotifyMoneyFlag) {
			mcr.setMoney(mcrVo.getMoney());
			//修改在线支付订单金额
			comRecordDao.updateComMoney(mcrVo);
		}
		
		long oldStatus = mcr.getStauts();
		mcr.setStauts(status);
		mcr.setLockFlag(MnyComRecord.LOCK_FLAG_COMPLETED);
		mcr.setOpDesc(mcrVo.getOpDesc());
		mcr.setRemark(mcrVo.getRemark());
		mcr.setHandlerType(moneyVo.getHandlerType());
		if(mcrVo.getMoney()!=null){
			mcr.setMoney(mcrVo.getMoney());
		}
		Long depositCount = 1l;
		if(mcrVo.getStatus()==MnyComRecord.STATUS_SUCCESS){
		// 更新存款总额和次数
			MemberDepositInfo mdInfo = new MemberDepositInfo();
			mdInfo.setAccountId(mcrVo.getAccountId());
			mdInfo.setDepositTotal(mcr.getMoney());
			mdInfo.setDepositCount(depositCount);
			depositCount = depositInfoDao.handlerDeposit(mcr.getMemberId(), depositCount, mcr.getMoney());
			//是否是首充
			mcrVo.setFirstDeposit(StringUtil.equals(depositCount, 1l));
			mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT_NONE);
			if(mcrVo.getFirstDeposit()){
				mcr.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT);
			}
			//是第几次充值
			mcr.setComTimes(depositCount);
		}
		int flag = comRecordDao.updatePayResult(mcr, oldStatus);
		// 批准就加款，并增加账变
		if (flag == 1 && StringUtil.equals(status, MnyComRecord.STATUS_SUCCESS)) {
			//同步状态到es记录
//			ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));
			mcrVo.setAccountId(mcr.getMemberId());
			mcrVo.setDepositDate(mcr.getCreateDatetime());
			mcrVo.setType(mcr.getType());
			mcrVo.setStationId(mcr.getStationId());
			mcrVo.setMoney(mcr.getMoney());
			
			// 更新会员提款的判断数据
			mcrVo.setDepositCount(depositCount);
			calcBetNumAndDepositGift(mcrVo);
			
			moneyVo.setAccount(mcr.getAccount());
			moneyVo.setAccountId(mcr.getMemberId());
			moneyVo.setStationId(mcr.getStationId());
			moneyVo.setMoney(mcr.getMoney());
			if(mcrVo.getMoney()!=null){
				moneyVo.setMoney(mcrVo.getMoney());
			}
			MoneyRecordType mrType = MoneyRecordType.getMoneyRecordType(mcr.getType());
			moneyVo.setMoneyRecordType(mrType);
			moneyVo.setFee(fee);
			moneyVo.setRemark("订单号：" + mcr.getOrderNo());
			moneyVo.setOrderId(mcr.getOrderNo());
			moneyVo.setBizDatetime(mcr.getCreateDatetime());
			moneyVo.setFirstDeposit(mcrVo.getFirstDeposit());
			moneyService.updMnyAndRecord(moneyVo);
		}
	}

	private void calcBetNumAndDepositGift(MnyComRecordVo mcrVo) {
		long depositType = mcrVo.getType();
		Long memberId = mcrVo.getAccountId();
		boolean gift = false;
//		Long depositCount = 1l;
		BigDecimal curDrawNeed = BigDecimal.ZERO;
		BigDecimal money = mcrVo.getMoney().setScale(2, RoundingMode.HALF_UP);
		// 手动加款和存款赠送，不计入存款总额和次数
		if (MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType() != depositType) {
			// 更新存款总额和次数
//			MemberDepositInfo mdInfo = new MemberDepositInfo();
//			mdInfo.setAccountId(memberId);
//			mdInfo.setDepositTotal(money);
//			mdInfo.setDepositCount(depositCount);
//			depositCount = depositInfoDao.handlerDeposit(memberId, depositCount, money);
//			
//			//是否是首充
//			mcrVo.setFirstDeposit(StringUtil.equals(depositCount, 1l));
			// 旧的存款策略
			DepositStrategyVo dsvo = new DepositStrategyVo();
			dsvo.setDepositDatetime(mcrVo.getDepositDate());
			dsvo.setDepositType(depositType);
			dsvo.setStationId(mcrVo.getStationId());
			// 获得赠送策略集合
			MemberDepositStrategy moneyTypeMds = matchMemberDepositStrategy(
					memberDepositStrategyDao.getStrategys(dsvo));
			if (moneyTypeMds != null) {
				// 累加提款需要的打码量
				curDrawNeed = handlerMemberDepositStrategy(memberId, money, moneyTypeMds);
				gift = true;
			}
		}
		if (!gift) {
			curDrawNeed = handerComStrategy(mcrVo, money, mcrVo.getDepositCount());
		}
		// 存款赠送，不计入存款总额和次数
		if (MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType() != depositType) {
			// 检验会员是否能升级
			MemberLevelUtil.checkDepositForLevel(memberId);
		}
		handerDrawNeed(memberId, mcrVo.getStationId(), money,mcrVo.getGiftMoney(), curDrawNeed, mcrVo.getCheckBetNum(),
				mcrVo.getBetNumMultiple(), depositType);
	}

	private void handerDrawNeed(Long accountId, Long stationId, BigDecimal money,BigDecimal giftMoney, BigDecimal curDrawNeed,
			Long betNumFlag, BigDecimal betNumMultiple, long depositType) {
		if ((depositType == MoneyRecordType.DEPOSIT_ARTIFICIAL.getType()
				|| depositType == MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType())
				&& (betNumFlag == null || MnyComRecordVo.CHECKBETNUM_ENABLED != betNumFlag)) {
			// 手动加款和存款赠送如果不需要计算打码量，则直接返回
			return;
		}
		if(giftMoney == null) {
			giftMoney = BigDecimal.ZERO;
		}
		// 所需打码量计算规则， 先使用存款策略、为空时 使用人工加款手动输入的， 如果为空 在使用 站点配置的
		if (curDrawNeed.compareTo(BigDecimal.ZERO) < 1) {
			if (betNumMultiple == null || betNumMultiple.compareTo(BigDecimal.ZERO) <= 0) {
				// 取得消费比例
				betNumMultiple = StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.consume_rate));
			}
			// 记算此时会员如果提款，需要多少的打码量
			curDrawNeed = money.multiply(betNumMultiple);
		}
		// 打码量为0不执行数据库操作
		if (curDrawNeed.compareTo(BigDecimal.ZERO) == 0) {
			return;
		}
		// 更新会员提款的判断所需要的数据
		mnyBetNumberDao.updateDrawCheck(accountId, stationId, curDrawNeed, BetNumRecord.TYPE_DEPOSIT, "用户存款", true,giftMoney);
	}

	/**
	 * 计算新的赠送策略
	 * 
	 * @param mcrVo
	 * @param money
	 * @param depositCount
	 * @return
	 */
	private BigDecimal handerComStrategy(MnyComRecordVo mcrVo, BigDecimal money, Long depositCount) {
		BigDecimal curDrawNeed = BigDecimal.ZERO;
		long dayDepositCount = comRecordDao.countDayPayCount(mcrVo.getAccountId(), mcrVo.getDepositDate(),
				mcrVo.getStationId());// 今日存款次数
		List<MnyComStrategy> mcsList = comStrategyService.filterList(mcrVo.getAccountId(), depositCount, dayDepositCount,
				mcrVo.getType(), money, mcrVo.getDepositDate(), mcrVo.getStationId());
		if(mcsList == null) {
			return curDrawNeed;
		}
		for (MnyComStrategy mcs : mcsList) {
			if (mcs != null) {
				BigDecimal amount = BigDecimal.ZERO;
				if (mcs.getGiftType() == MnyComStrategy.GIFT_TYPE_FIXED) {// 固定数额
					amount = mcs.getGiftValue();
				} else {// 浮动比例
					amount = money.multiply(mcs.getGiftValue()).divide(BigDecimalUtil.HUNDRED).setScale(2,
							BigDecimal.ROUND_FLOOR);
					BigDecimal limit = mcs.getUpperLimit();
					// 如果超过最大限制，取最大限制的值
					if (limit != null && limit.compareTo(BigDecimal.ZERO) > 0 && amount.compareTo(limit) > 0) {
						amount = limit;
					}
				}
				if (amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
					if (mcs.getValueType() == MnyComStrategy.VALUE_TYPE_MONEY) {
						MnyMoneyVo mvo = new MnyMoneyVo();
						mvo.setAccountId(mcrVo.getAccountId());
						mvo.setMoney(amount);
						mvo.setMoneyRecordType(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY);
						if (mcs.getGiftType() == MnyComStrategy.GIFT_TYPE_FIXED) {
							mvo.setRemark("存款赠送" + mcs.getGiftValue());
						} else {
							mvo.setRemark("存款赠送" + mcs.getGiftValue() + "%");
						}
						mcrVo.setGiftMoney(amount);
						moneyService.updMnyAndRecord(mvo);
	
					} else {
						MnyScoreVo scoreVo = new MnyScoreVo();
						scoreVo.setAccountId(mcrVo.getAccountId());
						scoreVo.setScore(amount);
						scoreVo.setScoreRecordType(ScoreRecordType.DEPOSIT_GIFT_ACTIVITY);
						if (mcs.getGiftType() == MnyComStrategy.GIFT_TYPE_FIXED) {
							scoreVo.setRemark("存款赠送" + mcs.getGiftValue());
						} else {
							scoreVo.setRemark("存款赠送" + mcs.getGiftValue() + "%");
						}
						// scoreVo.setRemark("存款" + money + "赠送,策略：" + mcs.getDesc());
						scoreService.updScoreAndRecord(scoreVo);
					}
					if (mcs.getBetMultiple() != null && mcs.getBetMultiple().compareTo(BigDecimal.ZERO) == 1) {
						// 根据打码量倍数得到打码量
						curDrawNeed = curDrawNeed.add((amount.add(money)).multiply(mcs.getBetMultiple()).setScale(0, RoundingMode.UP));
					}
				}
			}
		}
		return curDrawNeed;
	}

	/**
	 * 处理相应赠送策略，并返回此策略生成的出款打码量
	 * 
	 * @param mds
	 * @return
	 */
	public BigDecimal handlerMemberDepositStrategy(Long memberId, BigDecimal money, MemberDepositStrategy mds) {
		BigDecimal checkNum = BigDecimal.ZERO;
		BigDecimal amount = BigDecimal.ZERO;
		if (mds == null) {
			return checkNum;
		}
		BigDecimal limit = mds.getUpperLimit();
		if (limit == null) {
			limit = BigDecimal.ZERO;
		}

		// 根据赠送方式得到赠送额度
		if (StringUtil.equals(mds.getGiveType(), MemberDepositStrategy.GIVE_TYPE_MONEY)) {// 固定额度
			amount = mds.getGiveValue();
		} else if (StringUtil.equals(mds.getGiveType(), MemberDepositStrategy.GIVE_TYPE_RATIO)) {// 浮云比例
			amount = money.multiply(new BigDecimal(mds.getRollBackRate())).divide(new BigDecimal(100)).setScale(2,
					BigDecimal.ROUND_FLOOR);
			// 如果超过最大限制，取最大限制的值
			if (limit.compareTo(BigDecimal.ZERO) == 1 && amount.compareTo(limit) == 1) {
				amount = mds.getUpperLimit();
			}
		}

		if (amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
			String remark = "存款" + money + "赠送";
			Long depositCount = mds.getDepositCount();
			if (!StringUtil.equals(depositCount, MemberDepositStrategy.DEPOSIT_COUNT_DEFAULT)) {
				remark = "第" + depositCount + "次存款,赠送";
			}
			if (StringUtil.equals(mds.getValueType(), MemberDepositStrategy.VALUE_TYPE_MONEY)) {
				remark += "彩金:" + amount;
				MnyMoneyVo mvo = new MnyMoneyVo();
				mvo.setAccountId(memberId);
				mvo.setMoney(amount);
				mvo.setMoneyRecordType(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY);
				mvo.setRemark(remark);
				moneyService.updMnyAndRecord(mvo);

				if (mds.getBetMultiple() != null) {
					// 根据打码量倍数得到打码量
					checkNum = (amount.add(money)).multiply(mds.getBetMultiple()).setScale(0, BigDecimal.ROUND_CEILING);
				}
			} else if (StringUtil.equals(mds.getValueType(), MemberDepositStrategy.VALUE_TYPE_SCORE)) {
				remark += "积分:" + amount;
				MnyScoreVo scoreVo = new MnyScoreVo();
				scoreVo.setAccountId(memberId);
				scoreVo.setScore(amount);
				scoreVo.setScoreRecordType(ScoreRecordType.DEPOSIT_GIFT_ACTIVITY);
				scoreVo.setRemark(remark);
				scoreService.updScoreAndRecord(scoreVo);
			}
		}
		return checkNum;
	}

	/**
	 * 返回符合的策略
	 * 
	 * @param depositMoney
	 * @return 如果没有匹配到策略则返回为空
	 */
	private MemberDepositStrategy matchMemberDepositStrategy(List<MemberDepositStrategy> datas) {
		MemberDepositStrategy highestMds = null;

		if (datas != null && !datas.isEmpty()) {// 存在满足策略的 筛选出最符合策略的
			Collections.sort(datas, new Comparator<MemberDepositStrategy>() {
				@Override
				public int compare(MemberDepositStrategy o1, MemberDepositStrategy o2) {
					Long dcOnw = o1.getDepositCount();
					Long dcTwo = o2.getDepositCount();
					return dcTwo.compareTo(dcOnw);
				}
			});
			for (MemberDepositStrategy memberDepositStrategy : datas) {
				if (highestMds == null) {
					highestMds = memberDepositStrategy;
				} else {
					break;
				}
			}
		}
		return highestMds;
	}

	@Override
	public void artificial(MnyComRecordVo mcrVo) {
		try {
			Long accountId = mcrVo.getAccountId();
			BigDecimal money = mcrVo.getMoney();
			String remark = mcrVo.getRemark();
			Long type = mcrVo.getType();
			BigDecimal betNumMultiple = StringUtil.toBigDecimal(mcrVo.getBetNumMultiple());

			Boolean betNumFlag = false;
			if (StringUtil.equals(mcrVo.getCheckBetNum(), MnyComRecordVo.CHECKBETNUM_ENABLED)) {
				betNumFlag = true;
			}

			// 扣款的时候赠送金额限制为0,打码量倍数问题不用判断
			if (StringUtil.equals(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType(), type) || StringUtil.equals(MoneyRecordType.WITHDRAW_GIFT_ACTIVITY.getType(), type)) {
				betNumFlag = false;
			}

			// 扣款时金额不能小于0，加款时赠送金额为0时，金额不能小于0
			if (money.compareTo(BigDecimal.ZERO) < 1) {
				throw new GenericException("金额格式有误！");
			}

			MoneyRecordType mrt = MoneyRecordType.getMoneyRecordType(type);
			if (mrt == null) {
				throw new GenericException("操作类型不正确");
			}
			if (!type.equals(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType())
					&& !type.equals(MoneyRecordType.DEPOSIT_ARTIFICIAL.getType())
					&& !type.equals(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY.getType())
					&& !type.equals(MoneyRecordType.WITHDRAW_GIFT_ACTIVITY.getType())) {
				throw new GenericException("操作类型不正确");
			}
			if (betNumFlag && betNumMultiple.compareTo(BigDecimal.ZERO) == -1) {
				throw new GenericException("打码量倍数格式有误！");
			}

			SysAccount user = accountDao.getFromCache(accountId);
			if (user == null) {
				throw new GenericException("该用户不存在！");
			}

			if (!Validator.equals(mcrVo.getStationId(), user.getStationId())) {
				throw new GenericException("非法请求！");
			}
			String addAccount = StringUtil.trim2Empty(mcrVo.getAccount());
			if (!user.getAccount().equalsIgnoreCase(addAccount)) {
				throw new GenericException("输入查询的会员[" + addAccount + "]和加钱会员[" + user.getAccount() + "]不一致！");
			}
			
			Long depositCount = 1l;
			if (!StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE) && StringUtil.equals(MoneyRecordType.DEPOSIT_ARTIFICIAL.getType(), type)) {
				// 更新存款总额和次数
				
				MemberDepositInfo mdInfo = new MemberDepositInfo();
				mdInfo.setAccountId(mcrVo.getAccountId());
				mdInfo.setDepositTotal(money);
				mdInfo.setDepositCount(depositCount);
				depositCount = depositInfoDao.handlerDeposit(mcrVo.getAccountId(), depositCount, money);
				
				//是否是首充
				mcrVo.setFirstDeposit(StringUtil.equals(depositCount, 1l));
			}
			mcrVo.setDepositCount(depositCount);
			
			
			//不是引导账号才走以下逻辑
			if(!StringUtil.equals(user.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
				// 人工加款
				if (!StringUtil.equals(MoneyRecordType.WITHDRAW_ARTIFICIAL.getType(), type) && !StringUtil.equals(MoneyRecordType.WITHDRAW_GIFT_ACTIVITY.getType(), type)) {
					calcBetNumAndDepositGift(mcrVo);
				}
				//增加对应的人工加款充值记录
				if (MoneyRecordType.DEPOSIT_ARTIFICIAL == mrt) {
					saveArtificialMnyComRecord(accountId, money, user.getAccount(), remark, user.getStationId(),mcrVo.getFirstDeposit(),depositCount);
				}
			}
			
			MnyMoneyVo moneyVo = new MnyMoneyVo();
			moneyVo.setStationId(StationUtil.getStationId());
			moneyVo.setAccountId(accountId);
			moneyVo.setMoneyRecordType(mrt);
			moneyVo.setMoney(money);
			moneyVo.setRemark(remark);
			moneyVo.setFee(new BigDecimal(0));
			moneyVo.setBizDatetime(mcrVo.getDepositDate());
			moneyVo.setFirstDeposit(mcrVo.getFirstDeposit());
			moneyService.updMnyAndRecord(moneyVo);

			StringBuilder sb = new StringBuilder("为会员:");
			sb.append(user.getAccount()).append(mrt.getName()).append(money);
			if (betNumFlag) {
				sb.append(";打码量倍数:").append(betNumMultiple);
			}
			sb.append(";操作原因:").append(remark).append("<br>操作员：").append(UserUtil.getUserAccount());
			SysLogUtil.log(sb.toString(), LogType.AGENT_FINANCE);
		} catch (Exception e) {
			logger.error("手动加款失败", e);
			throw e;
		}
	}

	@Override
	public void moneyChange(long outAccId, long inAccId, BigDecimal money, Long checkBetNum, BigDecimal betNumMultiple,
			String remark) {
		try {
			Long stationId = StationUtil.getStationId();

			Boolean betNumFlag = false;
			if (StringUtil.equals(checkBetNum, MnyComRecordVo.CHECKBETNUM_ENABLED)) {
				betNumFlag = true;
			}
			
			String moneyError = "金额格式有误！";
			// 扣款时金额不能小于0，加款时赠送金额为0时，金额不能小于0
			if (money.compareTo(BigDecimal.ZERO) < 1) {
				throw new GenericException(moneyError);
			}
			String regex = StationConfigUtil.get(StationConfig.money_manager_money_validation);
				
			if (StringUtil.isNotEmpty(regex) && !Pattern.matches(regex, money.toString())) {
				String errorMsg = StationConfigUtil.get(StationConfig.money_manager_validation_error_msg);
				if(StringUtil.isNotEmpty(errorMsg)){
					moneyError = errorMsg;
				}
				throw new GenericException(moneyError);
			}

			if (betNumFlag && betNumMultiple.compareTo(BigDecimal.ZERO) == -1) {
				throw new GenericException("打码量倍数格式有误！");
			}

			SysAccount userOut = accountDao.getFromCache(outAccId);
			if (userOut == null) {
				throw new GenericException("转出会员不存在！");
			}
			SysAccount userIn = accountDao.getFromCache(inAccId);
			if (userIn == null) {
				throw new GenericException("输入会员不存在！");
			}

			if (!Validator.equals(stationId, userOut.getStationId())
					|| !Validator.equals(stationId, userIn.getStationId())) {
				throw new GenericException("非法请求！");
			}
			saveArtificialMnyComRecord(inAccId, money, userIn.getAccount(), remark, userIn.getStationId(), "站内转入",
					MoneyRecordType.MONEY_CHANGE_ADD.getType(),false,null);

			Date now = new Date();
			Long depositCount = 1l;
			BigDecimal curDrawNeed = BigDecimal.ZERO;
			// 更新存款总额和次数
			MemberDepositInfo mdInfo = new MemberDepositInfo();
			mdInfo.setAccountId(inAccId);
			mdInfo.setDepositTotal(money);
			mdInfo.setDepositCount(depositCount);
			depositCount = depositInfoDao.handlerDeposit(inAccId, depositCount, money);

			MemberLevelUtil.checkDepositForLevel(inAccId);

			if (betNumFlag && betNumMultiple.compareTo(BigDecimal.ZERO) == 1) {
				curDrawNeed = betNumMultiple.multiply(money).setScale(2);
				// 更新会员提款的判断所需要的数据
				mnyBetNumberDao.updateDrawCheck(inAccId, stationId, curDrawNeed, BetNumRecord.TYPE_MONEYCHANGE_ADD,
						"站内额度转入", true);
			}
			MnyMoneyVo moneyOutVo = new MnyMoneyVo();
			moneyOutVo.setStationId(StationUtil.getStationId());
			moneyOutVo.setAccountId(outAccId);
			moneyOutVo.setMoneyRecordType(MoneyRecordType.MONEY_CHANGE_SUB);
			moneyOutVo.setMoney(money);
			moneyOutVo.setRemark(remark);
			moneyOutVo.setFee(new BigDecimal(0));
			moneyOutVo.setBizDatetime(now);
			moneyService.updMnyAndRecord(moneyOutVo);

			MnyMoneyVo moneyInVo = new MnyMoneyVo();
			moneyInVo.setStationId(StationUtil.getStationId());
			moneyInVo.setAccountId(inAccId);
			moneyInVo.setMoneyRecordType(MoneyRecordType.MONEY_CHANGE_ADD);
			moneyInVo.setMoney(money);
			moneyInVo.setRemark(remark);
			moneyInVo.setFee(new BigDecimal(0));
			moneyInVo.setBizDatetime(now);
			moneyService.updMnyAndRecord(moneyInVo);

			StringBuilder sb = new StringBuilder("会员[" + userOut.getAccount() + "]转出:").append(money);
			sb.append("会员[" + userIn.getAccount() + "]转入:").append(money);
			if (betNumFlag) {
				sb.append(";打码量倍数:").append(betNumMultiple);
			}
			sb.append(";操作原因:").append(remark).append("<br>操作员：").append(UserUtil.getUserAccount());
			SysLogUtil.log(sb.toString(), LogType.AGENT_FINANCE);
		} catch (Exception e) {
			logger.error("站内转换失败", e);
			throw e;
		}
	}

	private void saveArtificialMnyComRecord(Long accountId, BigDecimal money, String account, String remark,
			Long stationId,boolean firstDeposit,Long depositCount) {
		saveArtificialMnyComRecord(accountId, money, account, remark, stationId, "手动加款",
				MoneyRecordType.DEPOSIT_ARTIFICIAL.getType(),firstDeposit,depositCount);
	}

	private void saveArtificialMnyComRecord(Long accountId, BigDecimal money, String account, String remark,
			Long stationId, String remark2, long type,boolean firstDeposit,Long depositCount) {
		if (StringUtils.equals(StationConfigUtil.get(stationId, StationConfig.deposit_artificial_2_com_record),
				"off")) {
			return;
		}
		MnyComRecord record = new MnyComRecord();
		record.setType(type);
		record.setMemberId(accountId);
		record.setMoney(money);
		record.setAccount(account);
		record.setOrderNo(Snowflake.getOrderNo() + "");
		record.setRemark(remark2);
		record.setStationId(stationId);
		record.setLockFlag(MnyComRecord.LOCK_FLAG_COMPLETED);
		record.setStauts(MnyComRecord.STATUS_SUCCESS);
		record.setCreateUserId(UserUtil.getUserId());
		Date d = new Date();
		record.setCreateDatetime(d);
		record.setModifyDatetime(d);
		record.setModifyUserId(UserUtil.getUserId());
		record.setOpDesc(remark);
		record.setFlagActive(1L);
		record.setHandlerType(2L);
		record.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT_NONE);
		record.setComTimes(depositCount);
		if(firstDeposit){
			record.setFirstDeposit(MnyComRecord.FIRST_DEPOSIT);
		}
		comRecordDao.saveOne(record);
//		//同步充值记录到es
//		comRecordDao.insertOne(record);
//		MnyComRecord newData = new MnyComRecord();
//		BeanDtoVoUtils.beanToBean(record,newData);
//		ElasticApi.addQueue(new IndexOperation<>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),newData));

	}

	@Override
	public void export(MnyComRecordVo mcrVo) {
		String title = "会员充值记录";
		String[] rowsName = new String[] { "序号", "会员","所属代理", "单号", "支付平台", "付款金额(/元)", "申请时间", "类型", "状态", "操作", "操作说明","处理时间","会员存款账户","入款商户账户" };
		List<Map> list = comRecordDao.getList(mcrVo);
		if (StationUtil.isAgentStation() && list != null) {
			AgentDepositBank b = null;
			AgentDepositFast fast = null;
			System.out.println(JSON.toJSONString(list));
			for (Map r : list) {
				int type = 0;
				Object typeo = r.get("type");
				System.out.println(typeo);
				if(typeo!=null) {
					type = Integer.parseInt(typeo.toString());
					Integer stationId = (Integer)r.get("stationId");
					if (type == 7) {// 公司转账
						if(r.get("payId")!=null) {
							int i = Integer.parseInt(r.get("payId").toString());
							b = bankDao.get(i);
							if (b != null && (stationId==b.getStationId().intValue())) {
								r.put("bankAccount",b.getCreatorName());
							}
						}
					} else if (type == 6) {
						if(r.get("payId")!=null) {
							int i = Integer.parseInt(r.get("payId").toString());
							fast = fastDao.get(i);
							if (fast != null && (stationId==fast.getStationId().intValue())) {
								r.put("bankAccount",fast.getPayUserName());
							}
						}
					}else if(5==type){
						r.put("bankAccount","在线充值");
					}
				}else {
					r.put("bankAccount","未知方式");
				}
			}
		}
		
		
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < list.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(list.get(i).get("account"));
			objs[2] = getStrNull(list.get(i).get("agentName"));
			objs[3] = getStrNull(list.get(i).get("orderNo"));
			objs[4] = getStrNull(list.get(i).get("payName"));
			objs[5] = getStrNull(list.get(i).get("money"));
			objs[6] = getStrNull(list.get(i).get("createDatetime"));
			objs[7] = getType(list.get(i).get("type"));
			objs[8] = getStatus(list.get(i).get("status"));
			objs[9] = getLockFlag(list.get(i).get("lockFlag"));
			objs[10] = getStrNull(list.get(i).get("opDesc"));
			objs[11] = getStrNull(list.get(i).get("modifyDatetime"));
			objs[12] = getStrNull(list.get(i).get("depositor"))!=""?getStrNull(list.get(i).get("depositor")):getStrNull(list.get(i).get("remark"));
			objs[13] = getStrNull(list.get(i).get("bankAccount"));
			dataList.add(objs);
		}
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出" + title, LogType.AGENT_FINANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStrNull(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		return obj.toString();
	}

	/**
	 * 是否操作
	 * 
	 * @param obj
	 * @return
	 */
	private String getLockFlag(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		if ("1".equals(obj.toString()) || "2".equals(obj.toString())) {
			return "未操作";
		} else {
			return "已操作";
		}
	}

	/**
	 * 1 : "处理中", 2 : "充值成功", 3 : "充值失败", 4 : "已取消"
	 * 
	 * @param obj
	 * @return
	 */
	private String getStatus(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		if ("1".equals(obj.toString())) {
			return "处理中";
		} else if ("2".equals(obj.toString())) {
			return "充值成功";
		} else if ("3".equals(obj.toString())) {
			return "充值失败";
		} else if ("4".equals(obj.toString())) {
			return "已取消";
		} else {
			return "未知状态";
		}
	}

	/**
	 * 1 : "人工加款", 2 : "人工扣款", 3 : "在线取款失败", 4 : "在线取款", 5 : "在线支付", 6 : "快速入款", 7 :
	 * "一般入款", 8 : "体育投注", 9 : "二级代理反水加钱", 10 : "二级代理反水扣钱", 11 : "二级代理反点加钱", 12 :
	 * "二级代理反点扣钱", 13 : "多级代理反点加钱", 14 : "一多级代理反点扣钱", 15 : "三方额度转入系统额度", 16 :
	 * "系统额度转入三方额度", 130 : "彩票投注"
	 * 
	 * @param obj
	 * @return
	 */
	private String getType(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		String name = "未知类型";
		int code = Integer.valueOf(obj.toString());
		switch (code) {
		case 1:
			name = "人工加款";
			break;
		case 2:
			name = "人工扣款";
			break;
		case 3:
			name = "在线取款失败";
			break;
		case 4:
			name = "在线取款";
			break;
		case 5:
			name = "在线支付";
			break;
		case 6:
			name = "快速入款";
			break;
		case 7:
			name = "一般入款";
			break;
		case 8:
			name = "体育投注";
			break;
		case 9:
			name = "二级代理反水加钱";
			break;
		case 10:
			name = "二级代理反水扣钱";
			break;
		case 11:
			name = "二级代理反点加钱";
			break;
		case 12:
			name = "二级代理反点扣钱";
			break;
		case 13:
			name = "多级代理反点加钱";
			break;
		case 14:
			name = "一多级代理反点扣钱";
			break;
		case 15:
			name = "三方额度转入系统额度";
			break;
		case 16:
			name = "系统额度转入三方额度";
			break;
		case 130:
			name = "彩票投注";
			break;
		}
		return name;
	}

	@Override
	public Long getComTotalMoney(Long stationId, Long userId) {
		return comRecordDao.getComTotalMoney(stationId, userId);
	}

	@Override
	public MnyComRecord getOne(Long id, Long stationId) {
		if (id != null) {
			MnyComRecord it = comRecordDao.get(id);
			if (it != null && (stationId == null || it.getStationId().equals(stationId))) {
				return it;
			}
		}
		return null;
	}

	@Override
	public void autoExpireStatus(Long stationId) {
		String timeoutStr = StationConfigUtil.get(stationId, StationConfig.deposit_record_timeout);
		if (StringUtils.isEmpty(timeoutStr)) {
			return;
		}
		if (!timeoutStr.matches("^[\\d]+(\\.[\\d]{1,2})?$")) {
			return;
		}
		BigDecimal timeout = BigDecimalUtil.toBigDecimal(timeoutStr);
		if (timeout == null || timeout.compareTo(BigDecimal.ZERO) <= 0) {
			return;
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, -timeout.multiply(new BigDecimal(3600)).intValue());
		comRecordDao.updateStatusToExpired(c.getTime(), stationId);
	}

	@Override
	public void artificialAddOne(SysAccount account, BigDecimal money, BigDecimal betNumMultiple, BigDecimal giftMoney,
			BigDecimal giftBetNumMultiple, String remark) {
		if (account == null) {
			throw new GenericException("该用户不存在！");
		}
		if ((money == null || money.compareTo(BigDecimal.ZERO) < 0)
				&& (giftMoney == null || giftMoney.compareTo(BigDecimal.ZERO) < 0)) {
			// 操作金额和赠送金额不能同时为0
			throw new GenericException("金额格式有误！");
		}
		if (!Validator.equals(StationUtil.getStationId(), account.getStationId())) {
			throw new GenericException("非法请求！");
		}
		StringBuilder sb = new StringBuilder("为会员:");
		sb.append(account.getAccount());
		MnyMoneyVo moneyVo = null;
		if (money.compareTo(BigDecimal.ZERO) > 0) {
			moneyVo = new MnyMoneyVo();
			moneyVo.setStationId(StationUtil.getStationId());
			moneyVo.setAccountId(account.getId());
			moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_ARTIFICIAL);
			moneyVo.setMoney(money);
			moneyVo.setRemark(remark);
			moneyVo.setFee(new BigDecimal(0));
			moneyVo.setBizDatetime(new Date());
			moneyService.updMnyAndRecord(moneyVo);
			Long depositCount = 1L;
			
			//批量引导
			Boolean firstDeposit = false; 
			if (!StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE) 
					) {
				// 更新存款总额和次数
				MemberDepositInfo mdInfo = new MemberDepositInfo();
				mdInfo.setAccountId(account.getId());
				mdInfo.setDepositTotal(money);
				mdInfo.setDepositCount(depositCount);
				depositCount = depositInfoDao.handlerDeposit(account.getId(), depositCount, money);
				//是否是首充
				firstDeposit = StringUtil.equals(depositCount, 1l);
				moneyVo.setFirstDeposit(firstDeposit);
			}
			
			//判断是否晋升
			if (!StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
				saveArtificialMnyComRecord(account.getId(), money, account.getAccount(), remark, account.getStationId(),firstDeposit,depositCount);
				
				MemberLevelUtil.checkDepositForLevel(account.getId());
			}
			sb.append("手动加款 ").append(money);
			if (betNumMultiple != null && betNumMultiple.compareTo(BigDecimal.ZERO) > 0) {
				sb.append(";打码量倍数:").append(betNumMultiple);
				betNumMultiple = money.multiply(betNumMultiple).setScale(0, BigDecimal.ROUND_CEILING);
				mnyBetNumberDao.updateDrawCheck(account.getId(), account.getStationId(), betNumMultiple,
						BetNumRecord.TYPE_DEPOSIT, "手动加款", true);
			}
		}
		if (giftMoney != null && giftMoney.compareTo(BigDecimal.ZERO) > 0) {
			moneyVo = new MnyMoneyVo();
			moneyVo.setStationId(StationUtil.getStationId());
			moneyVo.setAccountId(account.getId());
			moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_GIFT_ACTIVITY);
			moneyVo.setMoney(giftMoney);
			moneyVo.setRemark(remark);
			moneyVo.setFee(new BigDecimal(0));
			moneyVo.setBizDatetime(new Date());
			moneyService.updMnyAndRecord(moneyVo);
			sb.append(";赠送金额:").append(giftMoney);
			if (giftBetNumMultiple != null && giftBetNumMultiple.compareTo(BigDecimal.ZERO) > 0) {
				giftBetNumMultiple = giftMoney.multiply(giftBetNumMultiple).setScale(0, BigDecimal.ROUND_CEILING);
				mnyBetNumberDao.updateDrawCheck(account.getId(), account.getStationId(), giftBetNumMultiple,
						BetNumRecord.TYPE_DEPOSIT, "手动加款赠送", false);
			}
		}
		
		sb.append(";操作原因:").append(remark).append("<br>操作员：").append(UserUtil.getUserAccount());
		SysLogUtil.log(sb.toString(), LogType.AGENT_FINANCE);
	}

	@Override
	public void artificialSubOne(SysAccount account, BigDecimal money, String remark) {
		if (account == null) {
			throw new GenericException("该用户不存在！");
		}
		if (money.compareTo(BigDecimal.ZERO) < 1) {
			throw new GenericException("金额格式有误！");
		}
		if (!Validator.equals(StationUtil.getStationId(), account.getStationId())) {
			throw new GenericException("非法请求！");
		}
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(StationUtil.getStationId());
		moneyVo.setAccountId(account.getId());
		moneyVo.setMoneyRecordType(MoneyRecordType.WITHDRAW_ARTIFICIAL);
		moneyVo.setMoney(money);
		moneyVo.setRemark(remark);
		moneyVo.setFee(new BigDecimal(0));
		moneyVo.setBizDatetime(new Date());
		moneyService.updMnyAndRecord(moneyVo);
		StringBuilder sb = new StringBuilder("为会员:");
		sb.append(account.getAccount()).append("手动扣款 ").append(money);
		sb.append(";操作原因:").append(remark).append("<br>操作员：").append(UserUtil.getUserAccount());
		SysLogUtil.log(sb.toString(), LogType.AGENT_FINANCE);
	}

	@Override
	public void cancelHandler(MnyComRecordVo mcrVo) {

		if (!"on".equals(StationConfigUtil.get(StationConfig.deposit_handler_cancle))) {
			throw new GenericException("非法请求！");
		}

		Long comId = mcrVo.getId();
		Long stationId = StationUtil.getStationId();
		BigDecimal fee = mcrVo.getFee();
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(stationId);
		MnyComRecord mcr = comRecordDao.get(comId);
		if (mcr == null) {
			throw new GenericException("存款记录不存在！");
		}

		if (stationId != null && !StringUtil.equals(stationId, mcr.getStationId())) {
			throw new GenericException("非法请求！");
		}

		SecurityCheckUtil.check(mcrVo.getOpDesc());

		Long oldStatus = mcr.getStauts();

		if (!StringUtil.equals(oldStatus, MnyComRecord.STATUS_SUCCESS)) {
			throw new GenericException("只能撒消充值成功记录！");
		}
		int flag = comRecordDao.cancleResult(comId, mcrVo.getOpDesc());
		if (flag == 1) {
//			//同步状态到es
//			mcr.setStauts(MnyComRecord.STATUS_FAILED);
//			mcr.setModifyDatetime(new Date());
//			mcr.setOpDesc(mcrVo.getOpDesc());
//			ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));

			moneyVo.setAccount(mcr.getAccount());
			moneyVo.setAccountId(mcr.getMemberId());
			moneyVo.setMoney(mcr.getMoney());
			moneyVo.setMoneyRecordType(MoneyRecordType.DEPOSIT_CANCLE);
			moneyVo.setHandlerType(mcr.getHandlerType());
			moneyVo.setFee(fee);
			moneyVo.setRemark(mcrVo.getOpDesc());
			SysLogUtil.log("撒消编号为:" + comId + "会员" + mcr.getAccount() + "的充值记录", LogType.AGENT_FINANCE);
			moneyVo.setOrderId(mcr.getOrderNo());
			moneyVo.setBizDatetime(mcr.getCreateDatetime());
			moneyService.updMnyAndRecord(moneyVo);
		}

	}

	@Override
	public void moneyChangeOfMember(String account, BigDecimal money, String rePassword) {
		Long inAccId = null;
		Long outAccId = UserUtil.getUserId();
		Long stationId = StationUtil.getStationId();
		
		String memChgs = StationConfigUtil.get(stationId, StationConfig.money_manager_member_list);
		boolean isZJGLOnOff = false;
		if(StringUtil.isNotEmpty(memChgs)) {
			String[] memCheArr = memChgs.split(",");
			if(memCheArr.length > 0 && Arrays.asList(memCheArr).contains(UserUtil.getUserAccount())) {
				isZJGLOnOff = true;
			}
		}
		
		if(!isZJGLOnOff) {
			throw new GenericException("非法操作");
		}
		
		SysAccountInfo outAccInfo = accountInfoDao.get(outAccId);
		if (outAccInfo == null || StringUtil.isEmpty(outAccInfo.getReceiptPwd()) || StringUtil.isEmpty(rePassword)) {
			throw new GenericException("资金密码错误");
		}
		
		rePassword = MD5Util.getMD5String(outAccInfo.getAccount(), rePassword);
		if (!rePassword.equals(outAccInfo.getReceiptPwd())) {
			throw new GenericException("资金密码错误");
		}
		
		Map inAccount = accountDao.queryAccount(account, stationId);
		if(inAccount == null || inAccount.isEmpty()) {
			throw new GenericException("转移会员不存在");
		}
		inAccId = StringUtil.toLong(inAccount.get("id"));
		
		moneyChange(outAccId, inAccId, money, MnyComRecordVo.CHECKBETNUM_ENABLED,
				StringUtil.toBigDecimal(StationConfigUtil.get(StationConfig.consume_rate)), "");
	}
	@Override
	public void reback(Long comId) {
		User curUser = UserUtil.getCurrentUser();
		MnyComRecord com = comRecordDao.get(comId);
		if (com == null) {
			throw new GenericException("存款记录不存在！");
		}
		if(com.getModifyUserId() != null && (!StringUtil.equals(com.getLockFlag(), MnyComRecord.LOCK_FLAG_COMPLETED)
				&& !StringUtil.equals(com.getStatus(), MnyComRecord.STATUS_FAILED))) {
			throw new GenericException("当前订单状态无法回滚");
		}
		// 判断该记录是否被其他用户锁定
		if (com.getModifyUserId() != null && StringUtil.equals(com.getLockFlag(), MnyComRecord.LOCK_FLAG_COMPLETED)
				&& StringUtil.equals(com.getStatus(), MnyComRecord.STATUS_FAILED)
				&& !StringUtil.equals(curUser.getSysUserId(), com.getModifyUserId())) {
			SysAccount locker = accountDao.get(com.getModifyUserId());
			throw new GenericException("该订单已由" + locker.getAccount() + "批准失败,您无权修改！");
		}
		long lockFlag = MnyComRecord.LOCK_FLAG_LOCKED;
		int flag = comRecordDao.reback(comId,lockFlag);
		if (flag != 1) {
			throw new GenericException("此记录已经被处理过！");
		}
		MnyComRecord mcr= new MnyComRecord();
		mcr.setId(comId);
		mcr.setLockFlag(lockFlag);
		mcr.setStauts(MnyComRecord.STATUS_UNTREATED);
		mcr.setModifyDatetime(new Date());
//		//同步状态到es记录
//		ElasticApi.updateOne(new UpdateOperation<MnyComRecord>(EsEnum.MNY_COM_RECORD.getIndexTag(),EsEnum.MNY_COM_RECORD.getType(),mcr.getId().toString()).setSource(mcr));


		SysLogUtil.log("回滚编号为:" + comId + "的充值记录", LogType.AGENT_FINANCE);
//		if (StringUtil.equals(MnyComRecord.LOCK_FLAG_LOCKED, lockFlag)) {
//			
//			subCountOfUntreated(com.getStationId());
//		} else {
//			addCountOfUntreated(com.getStationId());
//			SysLogUtil.log("回滚编号为:" + comId + "的充值记录", LogType.AGENT_FINANCE);
//		}
		
	}
	
	@Override
	public void saveRemark(Long id, String remark) {
		comRecordDao.saveRemark(id, remark);
	}


}