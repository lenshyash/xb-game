package com.game.service.impl;

import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.SysPayPlatformDao;
import com.game.model.SysPayPlatform;
import com.game.model.vo.PayPlatformVo;
import com.game.service.SysPayPlatformService;
import com.game.util.SysLogUtil;

@Repository
public class SysPayPlatformServiceImpl implements SysPayPlatformService {

	@Autowired
	SysPayPlatformDao payPlatformDao;

	@Override
	public Page getPage(PayPlatformVo ppvo) {
		return payPlatformDao.getPaysPage(ppvo);
	}

	@Override
	public void savePayPlatform(SysPayPlatform payPlatform) {
		Long ppId = payPlatform.getId();
		String name = payPlatform.getName();
		String iconCss = payPlatform.getIconCss();
		if (StringUtil.isEmpty(name)) {
			throw new GenericException("支付平台名称不能为空！");
		}
		if (StringUtil.isEmpty(iconCss)) {
			throw new GenericException("显示图标样式不能为空！");
		}
		SysPayPlatform saveSpp = null;
		if (Validator.isNotNull(ppId)) {
			saveSpp = payPlatformDao.get(ppId);
			saveSpp.setName(name);
			saveSpp.setIconCss(iconCss);
			
			if(payPlatform.getSortNo() != null){
				saveSpp.setSortNo(payPlatform.getSortNo());
			}
			
			saveSpp.setStatus(payPlatform.getStatus());
			saveSpp.setCompany(payPlatform.getCompany());
			saveSpp.setType(payPlatform.getType());
			SysLogUtil.log("更新总控系统支付平台:" + name, LogType.ADMIN_SYSTEM);
		} else {
			saveSpp = payPlatform;
			SysLogUtil.log("新增总控系统支付平台:" + name, LogType.ADMIN_SYSTEM);
		}

		payPlatformDao.save(saveSpp);

	}

	@Override
	public void delPayPlatform(Long ppId) {
		payPlatformDao.delete(ppId);
		SysLogUtil.log("删除总控系统支付平台,编号为:" + ppId, LogType.ADMIN_SYSTEM);
	}

	@Override
	public void updStatus(Long ppId, Long status) {
		if (Validator.isNull(ppId)) {
			throw new GenericException("非法请求");
		}
		SysPayPlatform spp = payPlatformDao.get(ppId);
		spp.setStatus(status);
		payPlatformDao.save(spp);
	}

	@Override
	public List<Map> getPaysCombo(PayPlatformVo ppVo) {
		return payPlatformDao.getPays(ppVo);
	}

	@Override
	public SysPayPlatform getPayPlatform(Long payPlatformId) {
		return payPlatformDao.get(payPlatformId);
	}

}
