package com.game.service.impl;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.constant.StationConfig;
import com.game.dao.ProxyMultiRebateSettingDao;
import com.game.dao.SysAccountDao;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.dao.platform.ProxyMultiRebateRecordDao;
import com.game.model.ProxyMultiRebateSetting;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MnyMoneyService;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.util.StationConfigUtil;

@Repository
public class ProxyMultiRebateCoreServiceImpl implements ProxyMultiRebateCoreService {
	@Autowired
	private BcLotteryOrderDao bcLotteryOrderDao;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private SysAccountDao sysAccountDao;
	@Autowired
	private ProxyMultiRebateSettingDao proxyMultiRebateSettingDao;
	@Autowired
	private ProxyMultiRebateRecordDao proxyMultiRebateRecordDao;

	/**
	 * 计算结算程序 多级返点时调用 (通用,注意 proxyMultiRebateRecord记录值必须完善)
	 * 
	 * @param backRecord
	 * @throws Exception
	 * @throws SQLException
	 */
	@Override
	public Integer updateDateAndAddMoney4MultiRebate(ProxyMultiRebateRecord proxyMultiRebateRecord) throws Exception {
		Long stationId = proxyMultiRebateRecord.getStationId();
		List<ProxyMultiRebateSetting> multiRebateSettings = this.proxyMultiRebateSettingDao
				.getProxyRebateSettingByAccount(proxyMultiRebateRecord.getChild(), stationId);
		if (multiRebateSettings != null) {
			HashMap<String, ProxyMultiRebateSetting> hashMap = new HashMap<String, ProxyMultiRebateSetting>();
			for (ProxyMultiRebateSetting proxyMultiRebateSetting : multiRebateSettings) {
				hashMap.put(proxyMultiRebateSetting.getAccount(), proxyMultiRebateSetting);
			}
			String account = proxyMultiRebateRecord.getChild();// 从会员开始算
			ProxyMultiRebateSetting multiRebateSetting = null;
			ProxyMultiRebateSetting parentMultiRebateSetting = null;
			List<ProxyMultiRebateRecord> lists = new ArrayList<ProxyMultiRebateRecord>(multiRebateSettings.size() - 1);
			BigDecimal difference = null;
			BigDecimal rebateMoney = null;
			BigDecimal betMoney = proxyMultiRebateRecord.getBetMoney();
			BigDecimal selfRebateRate = null;
			BigDecimal childRebateRate = null;
			Long betId = proxyMultiRebateRecord.getBetId();
			String betOrderId = proxyMultiRebateRecord.getBetOrderId();
			String betAccount = proxyMultiRebateRecord.getChild();
			Date orderDatetime = proxyMultiRebateRecord.getBetOrderDatetime();
			ProxyMultiRebateRecord multiRebateRecord = null;
			String agent_name = null;
			int flag = 0;
			flag = this.bcLotteryOrderDao.updateBcLotteryOrdertById4MultiRebate(betId, stationId,
					proxyMultiRebateRecord.getCreateDatetime(), proxyMultiRebateRecord.getBetOrderDatetime(),
					ProxyMultiRebateRecord.STATUS_ALREADY_REBATE, ProxyMultiRebateRecord.STATUS_NOT_REBATE);// 已经返点
			if (flag == 1) {
				while ((multiRebateSetting = hashMap.get(account)) != null) {
					agent_name = multiRebateSetting.getAgent_name();
					parentMultiRebateSetting = hashMap.get(agent_name);
					if (parentMultiRebateSetting != null) {
						//判断是否开启代理返点赔率合并开关,开启后使用赔率值进行返点操作
						if("on".equals(StationConfigUtil.get(stationId, StationConfig.onoff_agent_merge_Rebate_odds_switch))){
							selfRebateRate = parentMultiRebateSetting.getDynamicRate();
							childRebateRate = multiRebateSetting.getDynamicRate();
						}else{
							selfRebateRate = parentMultiRebateSetting.getGameShare();
							childRebateRate = multiRebateSetting.getGameShare();
						}
						difference = selfRebateRate.subtract(childRebateRate);
						rebateMoney = difference.multiply(betMoney).divide(new BigDecimal("100")).setScale(6,
								BigDecimal.ROUND_DOWN);// 上级减去自身的返点点数
														// 就是上级所得点数
						if (rebateMoney.compareTo(BigDecimal.ZERO) < 1) {// 返点金额为0不需要保存
							account = agent_name;
							continue;
						}
						multiRebateRecord = new ProxyMultiRebateRecord();
						multiRebateRecord.setAccount(agent_name);
						multiRebateRecord.setBetId(betId);
						multiRebateRecord.setBetMoney(betMoney);
						multiRebateRecord.setChild(account);
						multiRebateRecord.setChildRebateRate(childRebateRate.floatValue());
						multiRebateRecord.setCreateDatetime(new Date());
						multiRebateRecord.setRebateMoney(rebateMoney);
						multiRebateRecord.setSelfRebateRate(selfRebateRate.floatValue());
						multiRebateRecord.setStationId(stationId);
						multiRebateRecord.setBetOrderId(betOrderId);
						multiRebateRecord.setBetOrderDatetime(orderDatetime);
						multiRebateRecord.setRebateType(ProxyMultiRebateRecord.REBATE_TYPE_1);
						SysAccount acc = sysAccountDao.findOneByAccountAndStationId(agent_name, stationId);
						MnyMoneyVo moneyVo = new MnyMoneyVo();
						moneyVo.setStationId(acc.getStationId());
						moneyVo.setAccount(agent_name);
						moneyVo.setAccountId(acc.getId());
						moneyVo.setMoney(rebateMoney);
						moneyVo.setStationId(stationId);
						moneyVo.setMoneyRecordType(MoneyRecordType.PROXY_MULTI_REBATE_ADD);
						moneyVo.setRemark("彩票投注 投注人[" + betAccount + "] 订单号[" + betOrderId + "] 投注金额[" + betMoney
								+ "],代理[" + agent_name + "]返点 金额[" + rebateMoney + "]");
						// logger.info("彩票投注 投注人[" + betAccount + "] 订单号[" + betOrderId + "] 投注金额[" +
						// betMoney + "],代理[" + agent_name + "]返点 金额[" + rebateMoney + "] 返点比例[" +
						// difference + "]");
						moneyVo.setOrderId(betOrderId);
						moneyVo.setBizDatetime(orderDatetime);
						mnyMoneyService.updMnyAndRecord(moneyVo);
						lists.add(multiRebateRecord);
					}
					account = agent_name;
					multiRebateSetting = null;
					difference = null;
					rebateMoney = null;
					selfRebateRate = null;
					childRebateRate = null;
					agent_name = null;
				}
				if (lists.size() > 0) {
					this.proxyMultiRebateRecordDao.batchInsert(lists);
				}
				return 1;
			} else {
				throw new RuntimeException("返点失败,当前订单状态不为返点,触发事物回滚");
			}
		}
		return 0;
	}
	
	/**
	 * 固定返点给直属上级时调用 (通用,注意 proxyMultiRebateRecord记录值必须完善)
	 * 
	 * @param backRecord
	 * @throws SQLException
	 */
	@Override
	public Integer directSuperordinateFixedRebate(ProxyMultiRebateRecord proxyMultiRebateRecord) {
		Long stationId = proxyMultiRebateRecord.getStationId();
		
		String account = proxyMultiRebateRecord.getChild();// 从会员开始算
		BigDecimal difference = BigDecimal.ZERO;
		BigDecimal rebateMoney = null;
		BigDecimal betMoney = proxyMultiRebateRecord.getBetMoney();
		Long betId = proxyMultiRebateRecord.getBetId();
		String agentName = null;
		int flag = 0;
		flag = this.bcLotteryOrderDao.updateBcLotteryOrdertById4MultiRebate(betId, stationId,
				proxyMultiRebateRecord.getCreateDatetime(), proxyMultiRebateRecord.getBetOrderDatetime(),
				ProxyMultiRebateRecord.STATUS_ALREADY_REBATE, ProxyMultiRebateRecord.STATUS_NOT_REBATE);// 已经返点
		List<ProxyMultiRebateRecord> lists = new ArrayList<ProxyMultiRebateRecord>();
		if (flag == 1) {
			//直属上级百分比
			String directPer = StationConfigUtil.get(stationId, StationConfig.proxy_fixed_rabate_per);
			//普通上级百分比
			String topPer = StationConfigUtil.get(stationId, StationConfig.top_agent_rabate_per);
			//获取向上返点的层数 
			String layers = StationConfigUtil.get(stationId, StationConfig.proxy_fixed_rebate_layers);
			//默认向上2层
			Integer rlayers = 2;
			if(StringUtil.isNotEmpty(layers)){
				rlayers = Integer.parseInt(layers);
			}
			SysAccount childAcc = sysAccountDao.findOneByAccountAndStationId(account, stationId);
			if(childAcc==null || childAcc.getAgentId() ==null ){
				return 0;
			}
			String parents = childAcc.getParentNames();
			List<String> topList = new ArrayList();
			if(StringUtil.isNotEmpty(parents)){
				parents = parents.substring(1,parents.length()-1);
				topList = Arrays.asList(parents.split(","));
			}
			//如果上级层数大于两级开始遍返点
			if(!topList.isEmpty()){
				for(int i=0;i<topList.size() ;i++){
					agentName = topList.get(topList.size()-i-1);
					if(StringUtil.isEmpty(agentName)){
						break;
					}
					//直属上级
					if(i==0){
						difference = new BigDecimal(directPer);
					//层级范围内或最上级
					} else if(i>0 && (i<=rlayers-1 || i==topList.size()-1)){
						difference = new BigDecimal(topPer);
					} else {
						continue;
					}
					rebateMoney = difference.multiply(betMoney).divide(new BigDecimal("100")).setScale(6,
							BigDecimal.ROUND_DOWN);// 上级减去自身的返点点数
													// 就是上级所得点数
					if (rebateMoney.compareTo(BigDecimal.ZERO) < 1) {// 返点金额为0不需要保存
						return 0;
					}
					//向直属上级返点
					this.comRabateRecord(proxyMultiRebateRecord, agentName, betId, betMoney, account, rebateMoney);
				}
			}
			return 1;
		} else {
			throw new RuntimeException("返点失败,当前订单状态不为返点,触发事物回滚");
		}
	}
	public static void main(String[] args) {
		BigDecimal difference = BigDecimal.ZERO;
		BigDecimal rebateMoney = null;
		BigDecimal betMoney = new BigDecimal(100);
		//直属上级百分比
		String directPer = "0.2";
		//普通上级百分比
		String topPer = "0.1";
		Integer rlayers = 3;
		String agentName = null;
		String parents =",A,B,C,D,E,F,";
		List<String> topList = new ArrayList();
		if(StringUtil.isNotEmpty(parents)){
			parents = parents.substring(1,parents.length()-1);
			topList = Arrays.asList(parents.split(","));
		}
		//如果上级层数大于两级开始遍返点
		if(!topList.isEmpty()){
			for(int i=0;i<topList.size() ;i++){
				agentName = topList.get(topList.size()-i-1);
				if(StringUtil.isEmpty(agentName)){
					break;
				}
				//直属上级
				if(i==0){
					difference = new BigDecimal(directPer);
				//层级范围内或最上级
				} else if(i>0 && (i<=rlayers-1 || i==topList.size()-1)){
					difference = new BigDecimal(topPer);
				} else {
					continue;
				}
				rebateMoney = difference.multiply(betMoney).divide(new BigDecimal("100")).setScale(6,
						BigDecimal.ROUND_DOWN);// 上级减去自身的返点点数
												// 就是上级所得点数
				System.out.println("当前层级："+agentName+",百分比："+difference+",返点金额："+rebateMoney);
			}
		}
		
	}
	private void comRabateRecord(ProxyMultiRebateRecord proxyMultiRebateRecord,String agent_name,Long betId,BigDecimal betMoney,String account,BigDecimal rebateMoney){
		ProxyMultiRebateRecord multiRebateRecord = new ProxyMultiRebateRecord();
		String betOrderId = proxyMultiRebateRecord.getBetOrderId();
		String betAccount = proxyMultiRebateRecord.getChild();
		Date orderDatetime = proxyMultiRebateRecord.getBetOrderDatetime();
		Long stationId = proxyMultiRebateRecord.getStationId();
		multiRebateRecord.setAccount(agent_name);
		multiRebateRecord.setBetId(betId);
		multiRebateRecord.setBetMoney(betMoney);
		multiRebateRecord.setChild(account);
		multiRebateRecord.setCreateDatetime(new Date());
		multiRebateRecord.setRebateMoney(rebateMoney);
		multiRebateRecord.setStationId(stationId);
		multiRebateRecord.setBetOrderId(betOrderId);
		multiRebateRecord.setBetOrderDatetime(orderDatetime);
		multiRebateRecord.setRebateType(ProxyMultiRebateRecord.REBATE_TYPE_3);
		SysAccount acc = sysAccountDao.findOneByAccountAndStationId(agent_name, stationId);
		MnyMoneyVo moneyVo = new MnyMoneyVo();
		moneyVo.setStationId(acc.getStationId());
		moneyVo.setAccount(agent_name);
		moneyVo.setAccountId(acc.getId());
		moneyVo.setMoney(rebateMoney);
		moneyVo.setStationId(stationId);
		moneyVo.setMoneyRecordType(MoneyRecordType.PROXY_MULTI_REBATE_ADD);
		moneyVo.setRemark("彩票投注 投注人[" + betAccount + "] 订单号[" + betOrderId + "] 投注金额[" + betMoney
				+ "],代理[" + agent_name + "]返点 金额[" + rebateMoney + "]");
		moneyVo.setOrderId(betOrderId);
		moneyVo.setBizDatetime(orderDatetime);
		mnyMoneyService.updMnyAndRecord(moneyVo);
		this.proxyMultiRebateRecordDao.insert(multiRebateRecord);
	}
	
	

	/**
	 * 取消多级返点时调用 (参数必须)
	 */
	@Override
	public Integer updateDateAndSubMoney4MultiRebate(Long betId, String account, Long stationId) throws Exception {
		List<ProxyMultiRebateRecord> list = this.proxyMultiRebateRecordDao.getMultiRebateByBetId(betId, stationId);
		if (list != null && list.size() > 0) {
			BigDecimal rebateMoney = null;
			BigDecimal betMoney = list.get(0).getBetMoney();
			String agent_name = null;
			int flag = 0;
			flag = this.bcLotteryOrderDao.updateBcLotteryOrdertById4MultiRebate(betId, stationId,
					list.get(0).getCreateDatetime(), list.get(0).getBetOrderDatetime(),
					ProxyMultiRebateRecord.STATUS_CANCLE_REBATE, ProxyMultiRebateRecord.STATUS_ALREADY_REBATE);
			if (flag == 1) {
				for (ProxyMultiRebateRecord proxyMultiRebateRecord : list) {
					rebateMoney = proxyMultiRebateRecord.getRebateMoney();
					agent_name = proxyMultiRebateRecord.getAccount();
					SysAccount acc = sysAccountDao.findOneByAccountAndStationId(agent_name, stationId);
					MnyMoneyVo moneyVo = new MnyMoneyVo();
					moneyVo.setStationId(acc.getStationId());
					moneyVo.setAccount(agent_name);
					moneyVo.setAccountId(acc.getId());
					moneyVo.setMoney(rebateMoney);
					moneyVo.setStationId(stationId);
					moneyVo.setMoneyRecordType(MoneyRecordType.PROXY_MULTI_REBATE_SUB);
					if (account != null) {
						moneyVo.setRemark("彩票投注 投注人[" + account + "]  记录id[" + betId + "] 投注金额[" + betMoney + "],代理["
								+ agent_name + "]返点 回滚金额[" + rebateMoney + "]");
					} else {
						moneyVo.setRemark("彩票投注   记录id[" + betId + "] 投注金额[" + betMoney + "],代理[" + agent_name
								+ "]返点 回滚金额[" + rebateMoney + "]");
					}
					moneyVo.setOrderId(proxyMultiRebateRecord.getBetOrderId());
					moneyVo.setBizDatetime(proxyMultiRebateRecord.getBetOrderDatetime());
					mnyMoneyService.updMnyAndRecord(moneyVo);
					this.proxyMultiRebateRecordDao.deleteById(proxyMultiRebateRecord.getId());
				}
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
			// throw new RuntimeException("返点回滚失败");
		}
	}
	
	@Override
	public void flushGameShare(Long agentId) {
		String redisKey = "multi_daili_gameshare_uid:" + agentId;
		RedisAPI.delCache(redisKey);
	}

	@Override
	public BigDecimal getGameShare(Long accountId) {

		String redisKey = "multi_daili_gameshare_uid:" + accountId;
		String gameShare = RedisAPI.getCache(redisKey);
		BigDecimal share = null;
		if (StringUtil.isNotEmpty(gameShare)) {
			return StringUtil.toBigDecimal(gameShare);
		} else {
			Map shareMap = proxyMultiRebateSettingDao.getAccountRebate(accountId);
			Object shareObj = shareMap.get("gameShare");
			if (shareObj != null) {
				share = StringUtil.toBigDecimal(shareObj);
				RedisAPI.addCache(redisKey, share.toString(), 3600);
			}
		}

		return share;
	}

	/**
	 * 彩票赢钱反水到代理线
	 */
	@Override
	public void winFoRebateToAgent(BigDecimal baseWinMoney,BigDecimal winMoney,BigDecimal curOdds, BcLotteryOrder order) {
		Long stationId = order.getStationId();
		List<ProxyMultiRebateRecord> lists = new ArrayList<ProxyMultiRebateRecord>();
		BigDecimal difference = null;
		BigDecimal rebateMoney = null;
		BigDecimal betMoney = order.getBuyMoney();
		BigDecimal selfRebateRate = null;
		BigDecimal childRebateRate = null;
		BigDecimal topRebateRate = null;
		BigDecimal differenceWinMoney = baseWinMoney.subtract(winMoney);
		Long betId = order.getId();
		String betOrderId = order.getOrderId();
		String betAccount = order.getAccount();
		Date orderDatetime = order.getCreateTime();
		ProxyMultiRebateRecord multiRebateRecord = null;
		//得到当前会员
		SysAccount acc = sysAccountDao.get(order.getAccountId());
		if (acc == null) {
			return;
		}
		//得到自身点数
		childRebateRate = acc.getRate();
		String parents = acc.getParents();
		String parentNames = acc.getParentNames();
		if (StringUtil.isEmpty(parents)) {
			return;
		}
		List<String> parentNameList = Arrays.asList(parentNames.split(","));
		List<String> parentList = Arrays.asList(parents.split(","));
		String account = acc.getAccount();
		String agent_name = null;
		Long agentId = 0l;
		Long topAgentId = StringUtil.toLong(parentList.get(1));
		//得到最上级代理赔率
		SysAccount topAgent = sysAccountDao.get(topAgentId);
		topRebateRate = topAgent.getRate();
//		topRebateRate =  getGameShare(topAgentId);
		BigDecimal per = null;
		BigDecimal balance = differenceWinMoney;
		BigDecimal fd = new BigDecimal(100);
		BigDecimal boomRate = acc.getRate();
		for (int i = parentList.size() - 1; i > 0; i--) {
			//如果到代理余额为0，结束返水操作
			if(balance.compareTo(BigDecimal.ZERO)<1) {
				break;
			}
			agent_name = parentNameList.get(i);
			agentId = StringUtil.toLong(parentList.get(i));
			if(StringUtil.equals(topAgentId, agentId)) {
				selfRebateRate = topRebateRate;
				difference = selfRebateRate.subtract(childRebateRate);
				rebateMoney = balance;
			}else {
//				selfRebateRate = getGameShare(agentId);
				//得到上级代理赔率
				SysAccount agent = sysAccountDao.get(agentId);
				selfRebateRate = agent.getRate();
				difference = selfRebateRate.subtract(childRebateRate);
				per = difference.divide(topRebateRate.subtract(boomRate), 4, RoundingMode.DOWN);
				rebateMoney = differenceWinMoney.multiply(per).setScale(2, RoundingMode.DOWN);// 上级减去自身的返点点数
				// 就是上级所得点数
				if (rebateMoney.compareTo(BigDecimal.ZERO) < 1) {// 返点金额为0不需要保存
					continue;
				}
				balance = balance.subtract(rebateMoney);
			}
			
			if(per != null) {
				fd = per.multiply(new BigDecimal("100"));
			}
			
			multiRebateRecord = new ProxyMultiRebateRecord();
			multiRebateRecord.setAccount(agent_name);
			multiRebateRecord.setBetId(betId);
			multiRebateRecord.setBetMoney(betMoney);
			multiRebateRecord.setChild(account);
			multiRebateRecord.setChildRebateRate(childRebateRate.floatValue());
			multiRebateRecord.setCreateDatetime(new Date());
			multiRebateRecord.setRebateMoney(rebateMoney);
			multiRebateRecord.setSelfRebateRate(selfRebateRate.floatValue());
			multiRebateRecord.setStationId(stationId);
			multiRebateRecord.setBetOrderId(betOrderId);
			multiRebateRecord.setBetOrderDatetime(orderDatetime);
			//定义返点类型
			multiRebateRecord.setRebateType(ProxyMultiRebateRecord.REBATE_TYPE_2);

			MnyMoneyVo moneyVo = new MnyMoneyVo();
			moneyVo.setAccount(agent_name);
			moneyVo.setAccountId(agentId);
			moneyVo.setMoney(rebateMoney);
			moneyVo.setStationId(stationId);
			moneyVo.setMoneyRecordType(MoneyRecordType.PROXY_MULTI_REBATE_ADD);
			moneyVo.setRemark("投注人[" + betAccount + "] 订单号[" + betOrderId + "] 投注金额[" + betMoney + "] 中奖金额[" + winMoney + "],上级代理["
					+ agent_name + "] 自身赔率["+curOdds+"] 中奖/代理线占比/金额[" + winMoney+"/"+fd.floatValue()+"%/"+rebateMoney + "]");
			moneyVo.setOrderId(betOrderId);
			moneyVo.setBizDatetime(orderDatetime);
			mnyMoneyService.updMnyAndRecord(moneyVo);
			lists.add(multiRebateRecord);
			account = agent_name;
			childRebateRate = selfRebateRate;
		}
		if (lists.size() > 0) {
			this.proxyMultiRebateRecordDao.batchInsert(lists);
		}
	}
}
