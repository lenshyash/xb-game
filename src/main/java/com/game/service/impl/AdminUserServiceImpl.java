package com.game.service.impl;

import javax.servlet.http.HttpSession;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.core.SystemConfig;
import com.game.dao.AdminUserDao;
import com.game.model.AdminUser;
import com.game.service.AdminUserService;
import com.game.util.MD5Util;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;
import com.game.util.VerifyCodeUtil;

@Repository
public class AdminUserServiceImpl implements AdminUserService {

	@Autowired
	private AdminUserDao userDao;

	public AdminUser doLogin(String account, String pwd,String verifyCode) {
		
		VerifyCodeUtil.isCheckSuccess(verifyCode);
		String password = MD5Util.getMD5String(account, pwd);
		AdminUser user = userDao.getLoginUser(account);
		if (user == null) {
			throw new GenericException("账户不存在");
		}

		if (!password.equals(StringUtil.toUpperCase(user.getPassword()))) {
			throw new GenericException("密码错误");
		}

		if (StringUtil.equals(user.getStatus(), AdminUser.STATUS_DISABLED)) {
			throw new GenericException("账户已被禁用");
		}
		HttpSession session = SysUtil.getSession();
		user.setLoginIp(UserUtil.getIpAddress());//设置登录IP
		session.setAttribute(SystemConfig.SESSION_ADMIN_KEY, user);
		SysLogUtil.loginLog("成功登录！", user.getId(), user.getAccount());
		return user;
	}

	public Page getUsers() {
		Page page = userDao.getUsers(UserUtil.getUserId());
		return page;
	}

	public void saveUser(AdminUser user, String pwd, String rpwd) {

		// 新增用户时要判断密码是否为空
		if (StringUtil.isEmpty(pwd) && Validator.isNull(user.getId())) {
			throw new GenericException("密码不能为空!");
		}
		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		AdminUser saveUser = userDao.get(user.getId());
		if (saveUser == null) {
			saveUser = user;
			SysLogUtil.log("新增总控系统用户:" + user.getAccount(), LogType.ADMIN_SYSTEM);
		} else {
			if (!UserUtil.isSuperAdmin() && "root".equals(user.getAccount())) {
				throw new GenericException("您无权修改该用户权限！");
			}

			saveUser.setGroupId(user.getGroupId());
			saveUser.setStatus(user.getStatus());
			SysLogUtil.log("更新总控系统用户:" + user.getAccount(), LogType.ADMIN_SYSTEM);
		}
		if (StringUtil.isNotEmpty(pwd)) {
			pwd = MD5Util.getMD5String(user.getAccount(), pwd);
			saveUser.setPassword(pwd);
		}
		userDao.save(saveUser);

	}

	public void deleteUser(String ids) {
		SysLogUtil.log("删除总控系统用户编号为:" + ids, LogType.ADMIN_SYSTEM);
		userDao.fakeDeletes(ids);
	}

	@Override
	public AdminUser getUserById(Long userId) {
		return null;
	}

	@Override
	public void closeOrOpen(Integer status, Integer id) {
		if (StringUtil.isEmpty(status) || StringUtil.isEmpty(id)) {
			throw new GenericException("参数不正确!");
		}
		if (status == 1) {
			userDao.closeOrOpen(2, id);
			SysLogUtil.log("启用总控系统用户编号为:" + id, LogType.ADMIN_SYSTEM);
		} else {
			userDao.closeOrOpen(1, id);
			SysLogUtil.log("禁用总控系统用户编号为:" + id, LogType.ADMIN_SYSTEM);
		}
	}

	@Override
	public void del(Integer id) {
		if (StringUtil.isEmpty(id)) {
			throw new GenericException("参数不正确!");
		}
		userDao.delete(id);
		SysLogUtil.log("删除总控系统用户编号为:" + id, LogType.ADMIN_SYSTEM);
	}

	@Override
	public void updpwd(long userId, String opwd, String pwd, String rpwd) {

		if (!StringUtil.equals(userId, UserUtil.getUserId())) {
			throw new GenericException("非法请求！");
		}

		if (StringUtil.isEmpty(pwd)) {
			throw new GenericException("密码不能为代空!");
		}
		if (StringUtil.isEmpty(rpwd)) {
			throw new GenericException("确认密码不能为代空!");
		}
		if (StringUtil.isEmpty(opwd)) {
			throw new GenericException("旧密码不能为代空!");
		}

		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (Validator.isNull(userId)) {
			throw new GenericException("该用户不存在或已删除!");
		}

		AdminUser account = userDao.get(userId);

		if (account == null) {
			throw new GenericException("该用户不存在或已删除!");
		}

		opwd = MD5Util.getMD5String(account.getAccount(), opwd);
		if (!opwd.equals(StringUtil.toUpperCase(account.getPassword()))) {
			throw new GenericException("旧密码错误!");
		}

		pwd = MD5Util.getMD5String(account.getAccount(), pwd);
		account.setPassword(pwd);
		userDao.save(account);
		SysLogUtil.log("修改总控系统用户:" + account.getAccount() + "的密码", LogType.ADMIN_SYSTEM);
	}

}
