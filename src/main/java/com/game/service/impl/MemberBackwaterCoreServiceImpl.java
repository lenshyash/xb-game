package com.game.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.MnyBetNumberDao;
import com.game.dao.SysAccountDao;
import com.game.dao.platform.MemberBackwaterRecordDao;
import com.game.dao.platform.MemberBackwaterStrategyDao;
import com.game.dao.platform.MemberBackwaterStrategyLevelDao;
import com.game.model.BetNumRecord;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.platform.MemberBackwaterStrategy;
import com.game.model.vo.MnyMoneyVo;
import com.game.service.MemberBackwaterCoreService;
import com.game.service.MnyMoneyService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Service
public class MemberBackwaterCoreServiceImpl implements MemberBackwaterCoreService {
	@Autowired
	private MemberBackwaterRecordDao backwarterRecordDao;
	@Autowired
	private MnyMoneyService mnyMoneyService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Autowired
	private MemberBackwaterStrategyDao backwaterStrategyDao;
	@Autowired
	private SysAccountDao sysAccountDao;
	@Autowired
	private MemberBackwaterStrategyLevelDao strategyLevelDao;

	@Override
	public int cancel(MemberBackwaterRecord r) {
		if (r.getBackwaterStatus() != MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY) {
			throw new GenericException("该记录还未反水，不能回滚");
		}
		int flag = backwarterRecordDao.updateStatus(r.getBetDate(), r.getAccount(), r.getBetType(), r.getStationId(), r.getBackwaterStatus(), MemberBackwaterRecord.STATUS_CANCLE_ROLL_BACK,UserUtil.getUserAccount(),UserUtil.getUserId(),r.getBackwaterRoomId());
		if (flag == 1) {
			String remark = "取消反水，" + r.getAccount() + "在" + DateUtil.toDateStr(r.getBetDate()) + "投注" + getBetTypeName(r.getBetType()) + " 金额：" + r.getBetMoney().setScale(2, RoundingMode.DOWN);
			if (r.getBackwaterMoney() != null && r.getBackwaterMoney().compareTo(BigDecimal.ZERO) > 0) {
				MnyMoneyVo moneyVo = new MnyMoneyVo();
				moneyVo.setAccount(r.getAccount());
				moneyVo.setMoney(r.getBackwaterMoney().setScale(6, RoundingMode.DOWN));
				moneyVo.setStationId(r.getStationId());
				moneyVo.setAccountId(r.getAccountId());
				moneyVo.setMoneyRecordType(MoneyRecordType.MEMBER_ROLL_BACK_SUB);
				moneyVo.setRemark(remark);
				moneyVo.setBetType(r.getBetType());
				moneyVo.setBizDatetime(r.getBetDate());
				mnyMoneyService.updMnyAndRecord(moneyVo);
			}
			if (r.getAccountId() != null && r.getDrawNeed() != null && r.getDrawNeed().compareTo(BigDecimal.ZERO) > 0) {
				// 取消反水，需要扣除取款打码量
				mnyBetNumberDao.addDrawNeed(r.getAccountId(), r.getDrawNeed().negate(), BetNumRecord.TYPE_BACKWATER, remark, null);
			}
			SysLogUtil.log(remark, LogType.AGENT_FINANCE);
		}
		return 1;
	}

	public void manualRollbackOne(MemberBackwaterRecord r, Long stationId, BigDecimal money) {
		if (r == null || !r.getStationId().equals(stationId)) {
			throw new GenericException("反水记录不存在");
		}
		if (r.getBackwaterStatus() == MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY) {
			throw new GenericException("该记录已经反水");
		}
		if (DateUtil.isToday(r.getBetDate())) {
			throw new GenericException("该记录为今日投注记录，投注金额还没有完全统计好，还不能反水");
		}
		r.setOperator(UserUtil.getUserAccount());
		r.setOperatorId(UserUtil.getUserId());
		int oldStatus = r.getBackwaterStatus();
		MemberBackwaterStrategy s = filterBackwaterStrategy(r);
		if ((s == null || s.getRate() == null) && (money==null || money.compareTo(BigDecimal.ZERO) <= 0)) {
			r.setBackwaterStatus(MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
			r.setBackwaterRate(BigDecimal.ZERO);
			r.setBackwaterMoney(BigDecimal.ZERO);
			r.setBackwaterDesc("没用匹配的反水策略");
			backwarterRecordDao.updateBackInfo(r, oldStatus);
			SysLogUtil.log("反水，" + r.getAccount() + "在" + DateUtil.toDateStr(r.getBetDate()) + "投注" + getBetTypeName(r.getBetType()) + " 金额：" + r.getBetMoney().setScale(2, RoundingMode.DOWN), LogType.AGENT_FINANCE);
			return;
		}
		
		if(money==null){
			r.setBackwaterRate(s.getRate());
			money = s.getRate().multiply(r.getBetMoney()).divide(BigDecimalUtil.HUNDRED);
			r.setBackwaterDesc("匹配策略 [有效投注=" + r.getBetMoney() + "元,反水比例=" + s.getRate() + "%, 打码量倍数=" + s.getMultiple() + ", 上限=" + s.getUpperLimit() + "元]");
		}else{
			if(s!=null){
				r.setBackwaterRate(s.getRate());
			}
			r.setBackwaterDesc("手动反水");
		}
		money=money.setScale(6, RoundingMode.DOWN);
		r.setBackwaterMoney(money);
		r.setBackwaterStatus(MemberBackwaterRecord.STATUS_ALREADY_ROLL_BACK_AND_MONEY);
		
		if (money.compareTo(BigDecimal.ZERO) <= 0) {// 反水金额为0，不记录
			r.setDrawNeed(BigDecimal.ZERO);
			backwarterRecordDao.updateBackInfo(r, oldStatus);
			SysLogUtil.log("反水，" + r.getAccount() + "在" + DateUtil.toDateStr(r.getBetDate()) + "投注" + getBetTypeName(r.getBetType()) + " 金额：" + r.getBetMoney().setScale(2, RoundingMode.DOWN), LogType.AGENT_FINANCE);
			return;
		} 
		
		if (s.getUpperLimit() != null && s.getUpperLimit().compareTo(BigDecimal.ZERO) > 0) {
			// 如果策略有设置反水上限，且上限值大于0，则需要判断是否超过上限，超过就只反上限金额
			if (s.getUpperLimit().compareTo(money) < 0) {
				money = s.getUpperLimit();
				r.setBackwaterMoney(money);
			}
		}
		// 反水需要的打码量，反水金额乘于 策略中的打码量倍数
		BigDecimal drawNeed = BigDecimal.ZERO;
		if (s!=null && s.getMultiple() != null && s.getMultiple().compareTo(BigDecimal.ZERO) > 0) {
			drawNeed = BigDecimalUtil.multiply(money, s.getMultiple()).setScale(2, RoundingMode.UP);
		}
		r.setDrawNeed(drawNeed);
		
		//彩票和系统彩如果开启动态赔率的话，反水暂时是未领状态（只针对彩票和系统彩）
		if((r.getBetType().intValue() == 1 ||r.getBetType().intValue() == 11) 
				&& "on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random))
				&& "on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_rate_random_water_choice))) {
			//如果开启动态赔率的话，反水奖励暂时是未领取的。
			r.setBackwaterStatus(MemberBackwaterRecord.STATUS_MONEY_UNCLAIMED);
			backwarterRecordDao.updateBackInfo(r, oldStatus);
		}else {
			backWaterAddMoney(r, oldStatus);
		}
	}

	@Override
	public void backWaterAddMoney(MemberBackwaterRecord r, int oldStatus) {
		Long stationId = r.getStationId();
		BigDecimal money = r.getBackwaterMoney();
		BigDecimal drawNeed = r.getDrawNeed();
		int flag = backwarterRecordDao.updateBackInfo(r, oldStatus);
		if (flag == 1) {
			MnyMoneyVo moneyVo = new MnyMoneyVo();
			moneyVo.setAccount(r.getAccount());
			moneyVo.setMoney(money);
			moneyVo.setStationId(stationId);
			moneyVo.setAccountId(r.getAccountId());
			moneyVo.setMoneyRecordType(MoneyRecordType.MEMBER_ROLL_BACK_ADD);
			moneyVo.setRemark("反水，" + r.getAccount() + "在" + DateUtil.toDateStr(r.getBetDate()) + "投注" + getBetTypeName(r.getBetType()) + " 金额：" + r.getBetMoney().setScale(2, RoundingMode.DOWN));
			moneyVo.setBetType(r.getBetType());
			moneyVo.setBizDatetime(r.getBetDate());
			mnyMoneyService.updMnyAndRecord(moneyVo);
			if (r.getAccountId() != null && drawNeed != null && drawNeed.compareTo(BigDecimal.ZERO) > 0) {
				// 反水，需要添加取款打码量
				mnyBetNumberDao.updateDrawCheck(r.getAccountId(), stationId, drawNeed, BetNumRecord.TYPE_BACKWATER,
						moneyVo.getRemark(), false);
			}
			SysLogUtil.log(moneyVo.getRemark(), LogType.AGENT_FINANCE);
		}
	}

	private String getBetTypeName(Integer betType) {
		switch (betType) {
			case 1:
				return "彩票";
			case 11:
//				return "系统彩";
				return "彩票";
			case 2:
				return "真人";
			case 3:
				return "电子";
			case 4:
				return "体育";
			case 5:
				return "六合特码B";
			case 6:
				return "六合正码B";
		}
		return "";
	}

	@Override
	public MemberBackwaterStrategy filterBackwaterStrategy(MemberBackwaterRecord r) {
		int type = r.getBetType();
//		if (type == 11) {
//			type = 1;
//		}
		List<MemberBackwaterStrategy> strategyList = null;
		if(r.getBackwaterRoomId() != null && r.getBackwaterRoomId()>0){
			strategyList = backwaterStrategyDao.findByTypeFromCache(type, r.getStationId(), r.getBackwaterRoomId());
		}else{
			strategyList = backwaterStrategyDao.findByTypeFromCache(type, r.getStationId());
		}
		if (strategyList != null && !strategyList.isEmpty()) {
			BigDecimal money = r.getBetMoney();// 反水根据投注金额来计算
			MemberBackwaterStrategy s = null;
			SysAccount account = sysAccountDao.get(r.getAccountId());
			List<Long> levelIds = null;
			for (MemberBackwaterStrategy mrs : strategyList) {
				levelIds = strategyLevelDao.getLevelIdsByStrategyId(mrs.getId(), r.getStationId());
				if (money.compareTo(mrs.getQualified()) >= 0 
						&& checkLevel(levelIds,account.getLevelGroup())) {// 满足大于该条件的有效投注值
					s = mrs;
				}
			}
			return s;
		}
		return null;
	}
	
	private boolean checkLevel(List<Long> levelIds,Long levelGroup) {
		if(levelIds == null || levelIds.isEmpty() ) {
			return false;
		}
		for (Long level : levelIds) {
			if(StringUtil.equals(level, levelGroup)) {
				return true;
			}
		}
		return false;
	}

}
