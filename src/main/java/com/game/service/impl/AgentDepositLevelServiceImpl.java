package com.game.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.AgentDepositLevelDao;
import com.game.model.AgentDepositLevel;
import com.game.service.AgentDepositLevelService;

@Service
public class AgentDepositLevelServiceImpl implements AgentDepositLevelService {
	@Autowired
	private AgentDepositLevelDao depositLevelDao;

	@Override
	public List<AgentDepositLevel> findByDepositId(Long depositId, Long stationId, int type) {
		return depositLevelDao.findByDepositId(depositId, stationId, type);
	}

	@Override
	public Set<Long> getLevelIdsByDepositId(Long depositId, Long stationId, int type) {
		List<Long> ilist = depositLevelDao.getLevelIdsByDepositId(depositId, stationId, type);
		if (ilist != null && !ilist.isEmpty()) {
			return new HashSet<>(ilist);
		}
		return null;
	}

	@Override
	public void batchInsert(List<AgentDepositLevel> dllist) {
		if (dllist != null && !dllist.isEmpty()) {
			depositLevelDao.batchInsert(dllist);
		}
	}

	@Override
	public void batchDelete(Long depositId, Long stationId, int type, List<Long> levelIdList) {
		if (levelIdList != null && !levelIdList.isEmpty()) {
			depositLevelDao.batchDelete(depositId, stationId, type, levelIdList);
		}
	}

	@Override
	public void deleteByDeposiId(Long depositId, Long stationId, int type) {
		depositLevelDao.deleteByDeposiId(depositId, stationId, type);
	}
}
