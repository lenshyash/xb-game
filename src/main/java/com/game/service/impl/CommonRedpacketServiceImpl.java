package com.game.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.game.cache.redis.RedisAPI;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.dao.CommonRedpacketDao;
import com.game.dao.CommonRedpacketRecordDao;
import com.game.dao.MnyBetNumberDao;
import com.game.dao.SysAccountDao;
import com.game.model.BetNumRecord;
import com.game.model.CommonRedpacket;
import com.game.model.CommonRedpacketRecord;
import com.game.model.SysAccount;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.vo.CommonRedpacketVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.readpacket.RedPacketUtil;
import com.game.service.CommonRedpacketService;
import com.game.service.MnyMoneyService;
import com.game.util.BigDecimalUtil;
import com.game.util.DateUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Repository
public class CommonRedpacketServiceImpl implements CommonRedpacketService {
	private static final int COMMON_REDPACKET_REDIS_DB = 8;
	@Autowired
	private CommonRedpacketDao redpacketDao;
	@Autowired
	private CommonRedpacketRecordDao redpacketRecordDao;
	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Autowired
	private SysAccountDao accountDao;

	@Override
	public Page<CommonRedpacket> getPage(Long stationId) {
		Page<CommonRedpacket> page = redpacketDao.getPage(stationId);
		return page;
	}

	@Override
	public Page<CommonRedpacketRecord> getRecordPage(String account, Long stationId, Date begin, Date end) {
		return redpacketRecordDao.getRecordPage(stationId, account, begin, end);
	}

	private CommonRedpacket getById(Long id) {
		if (id == null || id <= 0)
			return null;
		String key = "COMMON_REDPACKET_red_packet_" + id;
		String json = RedisAPI.getCache(key, COMMON_REDPACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSON.parseObject(json, CommonRedpacket.class);
		}

		CommonRedpacket rp = redpacketDao.get(id);
		if (rp != null) {
			RedisAPI.addCache(key, JSON.toJSONString(rp), 0, COMMON_REDPACKET_REDIS_DB);
		}
		return rp;
	}

	public void initRedpacketRedis(CommonRedpacket rp) {
		if (RedisAPI.exists("COMMON_REDPACKET_money_list_" + rp.getId(), COMMON_REDPACKET_REDIS_DB)) {
			return;
		}
		List<CommonRedpacketRecord> rlist = redpacketRecordDao.getRecordList(rp.getStationId(), null, rp.getId(), null,
				null, null);
		BigDecimal money = BigDecimal.ZERO;
		String[] accIdArr = null;
		int i = 0;
		if (rlist != null) {
			accIdArr = new String[rlist.size()];
			for (CommonRedpacketRecord r : rlist) {
				if (r.getMoney() != null) {
					money = money.add(r.getMoney());
				}
				accIdArr[i] = r.getAccountId().toString();
				i++;
			}
		}
		money = rp.getTotalMoney().subtract(money);
		if (money.compareTo(BigDecimal.ZERO) > 0) {
			List<BigDecimal> moneyList = RedPacketUtil.splitRedPackets(money, rp.getTotalNumber() - i,
					rp.getMinMoney());
			String[] arr = new String[moneyList.size()];
			int j = 0;
			for (BigDecimal b : moneyList) {
				arr[j] = b.toString();
				j++;
			}
			RedisAPI.rpush("COMMON_REDPACKET_money_list_" + rp.getId(), COMMON_REDPACKET_REDIS_DB, arr);
			if (accIdArr != null && accIdArr.length > 0) {
				RedisAPI.sadd("COMMON_REDPACKET_accid_Set_" + rp.getId(), COMMON_REDPACKET_REDIS_DB, accIdArr);
			}
		}
	}

	@Override
	public CommonRedpacket createRedpacket(CommonRedpacket mrp) {
		if (mrp == null) {
			throw new GenericException("参数错误");
		}
		if (mrp.getTotalMoney() == null || BigDecimal.ZERO.compareTo(mrp.getTotalMoney()) >= 0) {
			throw new GenericException("红包金额不能为空！");
		}

		if (mrp.getTotalNumber() == null || mrp.getTotalNumber() <= 0) {
			throw new GenericException("红包个数不能为空！");
		}

		if (Validator.isNull(mrp.getStationId())) {
			throw new GenericException("站点ID不能为空！");
		}

		if (mrp.getMinMoney() == null || BigDecimal.ZERO.compareTo(mrp.getMinMoney()) >= 0) {
			mrp.setMinMoney(new BigDecimal("0.01"));
		}
		if (BigDecimalUtil.multiplyInts(mrp.getMinMoney(), mrp.getTotalNumber()).compareTo(mrp.getTotalMoney()) > 0) {
			throw new GenericException("红包金额不能小于红包个数乘于最小金额！");
		}

		if (StringUtil.isEmpty(mrp.getTitle())) {
			mrp.setTitle("恭喜发财，大吉大利");
		}

		SysAccount acc = accountDao.get(mrp.getAccountId());

		if (acc == null) {
			throw new GenericException("非法请求！");
		}
		Long accountType = acc.getAccountType();
		Long reportType = acc.getReportType();
//		if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE, accountType) 
		if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST, accountType) 
				|| StringUtil.equals(acc.getRedpacketLimit(), SysAccount.REDPACKET_DISABLED)) {
			throw new GenericException("暂无发送红包权限");
		}
		//判断红包权限
		String redPrem = StationConfigUtil.get(mrp.getStationId(),StationConfig.chat_red_packet_permission);
		if(StringUtil.isNotEmpty(redPrem)){
			if("off".equals(redPrem)){
				throw new GenericException("您暂未开通发送红包权限");
			}else if("goff".equals(redPrem)&&
					( StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE, accountType)
							||StringUtil.equals(SysAccount.REPORT_TYPE_GUIDE, acc.getReportType())) ){
				throw new GenericException("您暂未开通发送红包权限");
			}
		}
		
		//设置了引导账号的密码
		String password = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.guide_send_red_packet_password);
		//判断引导账号
		Boolean guide = false;
		if(accountType.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE) || reportType.equals(SysAccount.REPORT_TYPE_GUIDE)) {
			guide = true;
		}
		//开启了密码 进行验证
		if(guide && StringUtil.isNotEmpty(password)) {
			if(!password.equalsIgnoreCase(mrp.getRedPacketPassword())) {
				throw new GenericException("红包密码不匹配");
			}
		}
		
		Calendar c = Calendar.getInstance();
		mrp.setCreateDatetime(c.getTime());
		c.add(Calendar.DAY_OF_MONTH, 1);
		mrp.setEndDatetime(c.getTime());
		mrp.setRemainMoney(mrp.getTotalMoney());
		mrp.setRemainNumber(mrp.getTotalNumber());
		mrp.setCreator(acc.getAccount());
		mrp.setStatus(CommonRedpacket.STATUS_ENDALBED);
		redpacketDao.insert(mrp);
		if (mrp.getId() != null) {
			String remark = "发红包，金额为：" + mrp.getTotalMoney() + "元";
			MnyMoneyVo mvo = new MnyMoneyVo();
			mvo.setAccountId(mrp.getAccountId());
			mvo.setMoney(mrp.getTotalMoney());
			mvo.setMoneyRecordType(MoneyRecordType.REDPACKET_SUB);
			mvo.setRemark(remark);
			mvo.setOrderId(mrp.getId() + "");
			mvo.setBizDatetime(mrp.getCreateDatetime());
			moneyService.updMnyAndRecord(mvo,null);
		}
		initRedpacketRedis(mrp);
		return mrp;
	}

	@Override
	public List<CommonRedpacket> getRedpacketList(Long stationId) {
		String key = "COMMON_REDPACKET_red_packet_station_" + stationId;
		String json = RedisAPI.getCache(key, COMMON_REDPACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSONArray.parseArray(json, CommonRedpacket.class);
		}
		List<CommonRedpacket> list = redpacketDao.getRedpacketList(stationId, CommonRedpacket.STATUS_ENDALBED);

		if (list != null) {
			RedisAPI.addCache(key, JSON.toJSONString(list), 3600 * 24, COMMON_REDPACKET_REDIS_DB);
		}
		return list;
	}

	/**
	 * 抢红包
	 */
	@Override
	public Map grabRedpacket(Long redpacketId, Long userId) {
		try {
			CommonRedpacketRecord r = redpacketRecordDao.getAccountRecordById(redpacketId,userId);
			if(r != null) {
				return MixUtil.newHashMap("success",true,"content",redpacketInfo(redpacketId, userId));
			}
			if (Validator.isNull(redpacketId)) {
				throw new GenericException("活动已结束");
			}
			CommonRedpacket rp = getById(redpacketId);
			if (rp == null) {
				throw new GenericException("活动已结束");
			}
			Date curDate = new Date();
			if (rp.getEndDatetime().before(curDate)) {
				throw new GenericException("活动已结束");
			}
	
			SysAccount acc = accountDao.findOneByAccountIdAndStationId(userId, rp.getStationId());
			if(acc == null 
//					|| StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE, acc.getAccountType()) 
					|| StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST, acc.getAccountType()) 
					|| StringUtil.equals(acc.getRedpacketLimit(), SysAccount.REDPACKET_DISABLED)) {
				throw new GenericException("暂无抢红包权限");
			}
			// 先判断红包额度问题
			Map<Object, Object> map = redpacketRecordDao.getMoneyAndCount(rp.getId());
			if (map != null) {
				int num = rp.getTotalNumber() - Integer.valueOf(map.get("num").toString());
				BigDecimal money = rp.getRemainMoney().subtract(StringUtil.toBigDecimal(map.get("money")));
				if (num >= 0 && money != null) {
					rp.setRemainNumber(num);
					rp.setRemainMoney(money);
				}
			}
			if (rp.getRemainMoney().compareTo(BigDecimal.ZERO) == 0) {
				throw new GenericException("您来晚一步,红包已被抢完!");
			}
	
			if (Objects.equals(acc.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
				throw new GenericException("此账号不能参与该活动！");
			}
	
			if (RedisAPI.sadd("COMMON_REDPACKET_accid_Set_" + redpacketId, COMMON_REDPACKET_REDIS_DB,
					acc.getId().toString()) == 0) {
				throw new GenericException("你已参加过该活动！");
			}
	
			BigDecimal returnBig = BigDecimalUtil
					.toBigDecimal(RedisAPI.lpop("COMMON_REDPACKET_money_list_" + redpacketId, COMMON_REDPACKET_REDIS_DB));
			if (returnBig == null) {
				throw new GenericException("您来晚一步！");
			}
	
			CommonRedpacketRecord record = new CommonRedpacketRecord();
			record.setAccountId(acc.getId());
			record.setAccount(acc.getAccount());
			record.setCreateDatetime(curDate);
			record.setMoney(returnBig);
			record.setRedpacketId(redpacketId);
			record.setStationId(rp.getStationId());
			record.setRedpacketName(rp.getTitle());
			record.setStatus(CommonRedpacketRecord.STATUS_UNTREATED);
			if(StringUtil.isNotEmpty(acc.getImChatId())) {
				record.setImChatId(acc.getImChatId());
			}
			if(StringUtil.isNotEmpty(acc.getImUserId())) {
				record.setImUserId(acc.getImUserId());
			}
			try {
				redpacketRecordDao.save(record);
			} catch (Exception e) {
				throw new GenericException("抢红包只能参与一次！");
			}
			// 发放红包金额
			balanceAndRecord(record);
			// 更新红包信息
			redpacketDao.grabRedPacket(redpacketId, returnBig);
			return MixUtil.newHashMap("success",true,"content",redpacketInfo(redpacketId, userId));
		} catch (Exception e) {
			return MixUtil.newHashMap("success",false,"msg",e.getMessage(),"content",redpacketInfo(redpacketId, userId));
		}
	}

	@Override
	public List<CommonRedpacketRecord> getMarqueeRecord(Long stationId, Long RedpacketId) {

		String marqueeKey = "COMMON_REDPACKET_red_packet_record_" + stationId + "_rpid_" + RedpacketId;
		String json = RedisAPI.getCache(marqueeKey, COMMON_REDPACKET_REDIS_DB);
		if (StringUtils.isNotEmpty(json)) {
			return JSONArray.parseArray(json, CommonRedpacketRecord.class);
		}

		List<CommonRedpacketRecord> marquees = redpacketRecordDao.getRecordList(stationId,
				CommonRedpacketRecord.STATUS_SUCCESS, RedpacketId, null, null, 50);
		// 增加红包排序规则
		if (marquees.isEmpty()) {
			return marquees;
		}
		RedisAPI.addCache(marqueeKey, JSON.toJSONString(marquees), 10, COMMON_REDPACKET_REDIS_DB);
		return marquees;
	}

	@Override
	public CommonRedpacketRecord getRedpacketRecord(Long id, Long stationId) {
		CommonRedpacketRecord record = redpacketRecordDao.get(id);
		if (record == null || !StringUtil.equals(record.getStationId(), stationId)) {
			return null;
		}
		return record;
	}

	/**
	 * 发放奖金并写账变记录
	 * 
	 * @param record
	 */
	@Override
	public boolean balanceAndRecord(CommonRedpacketRecord record) {

		boolean success = redpacketRecordDao.hanlderUntreated(record) > 0;
		if (success) {
			if (record.getMoney() == null || record.getMoney().compareTo(BigDecimal.ZERO) <= 0) {
				return success;
			}
			String remark = "抢红包，获得金额：" + record.getMoney().setScale(2, RoundingMode.DOWN) + "元";
			MnyMoneyVo mvo = new MnyMoneyVo();
			mvo.setAccountId(record.getAccountId());
			mvo.setMoney(record.getMoney());
			mvo.setMoneyRecordType(MoneyRecordType.REDPACKET_ADD);
			mvo.setRemark(remark);
			mvo.setOrderId(record.getId() + "");
			mvo.setBizDatetime(record.getCreateDatetime());
			moneyService.updMnyAndRecord(mvo,null);
			BigDecimal rate = StringUtil
					.toBigDecimal(StationConfigUtil.get(record.getStationId(), StationConfig.consume_rate));
			// 取得消费比例 红包打码量
			mnyBetNumberDao.updateDrawCheck(record.getAccountId(), record.getStationId(),
					record.getMoney().multiply(rate), BetNumRecord.TYPE_RED_PACKET, "拼手气红包", false);
			String key = "COMMON_REDPACKET_red_packet_record_" + record.getStationId() + "_rpid_"
					+ record.getRedpacketId();
			RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB, key);
		}
		return success;
	}

	/**
	 * 活动中奖记录处理器
	 * 
	 * @param activeR
	 */
	@Override
	public List<CommonRedpacketRecord> getUntreatedRecords(Date end, Integer limit) {
		return redpacketRecordDao.getRecordList(null, CommonRedpacketRecord.STATUS_UNTREATED, null, null, end, limit);
	}

	@Override
	public void delRedpacket(Long id) {
		if (id == null || id <= 0)
			return;
		CommonRedpacket old = getById(id);
		if (old == null)
			return;
		redpacketDao.delete(id);
		SysLogUtil.log("删除红包,名称：" + old.getTitle(), LogType.AGENT_FINANCE);
		RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB, "COMMON_REDPACKET_red_packet_" + id);
		RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB, "COMMON_REDPACKET_red_packet_station_" + old.getStationId());
		RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB,
				"COMMON_REDPACKET_red_packet_record_" + old.getStationId() + "_rpid_" + id);
		RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB, "COMMON_REDPACKET_money_list_" + id);
		RedisAPI.delCache(COMMON_REDPACKET_REDIS_DB, "COMMON_REDPACKET_accid_Set_" + id);
	}

	@Override
	public CommonRedpacketVo redpacketInfo(Long redpacketId, Long userId) {
		CommonRedpacketVo cr = new CommonRedpacketVo();
		CommonRedpacket cur = redpacketDao.get(redpacketId);
		List<CommonRedpacketRecord> records = getMarqueeRecord(cur.getStationId(), redpacketId);
		cr.setRecords(records);
		cr.setRedpacket(cur);
		return cr;
	}

	@Override
	public void redpacketExpireReturnMoney(Long stationId) {
		List<CommonRedpacket> redpackets = redpacketDao.getExpireRedpacketList(stationId);
		for (CommonRedpacket record : redpackets) {
			boolean success = redpacketDao.expireRedPacket(record.getId()) > 0;
			if (success) {
				if (record.getRemainMoney().compareTo(BigDecimal.ZERO) == 1) {
					String remark = DateUtil.formatDate(record.getCreateDatetime(), "yyyy-MM-dd HH:mm:ss")
							+ "发的红包过期，退还金额：" + record.getRemainMoney().setScale(2, RoundingMode.DOWN) + "元";
					MnyMoneyVo mvo = new MnyMoneyVo();
					mvo.setAccountId(record.getAccountId());
					mvo.setMoney(record.getRemainMoney());
					mvo.setMoneyRecordType(MoneyRecordType.REDPACKET_RETURN);
					mvo.setRemark(remark);
					mvo.setOrderId(record.getId() + "");
					mvo.setBizDatetime(new Date());
					moneyService.updMnyAndRecord(mvo);
					BigDecimal rate = StringUtil
							.toBigDecimal(StationConfigUtil.get(record.getStationId(), StationConfig.consume_rate));
					// 取得消费比例 红包打码量
					mnyBetNumberDao.updateDrawCheck(record.getAccountId(), record.getStationId(),
							record.getRemainMoney().multiply(rate), BetNumRecord.TYPE_RED_PACKET, "红包过期退还", false);
				}
			}
		}
	}
}
