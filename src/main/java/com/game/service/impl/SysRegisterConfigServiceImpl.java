package com.game.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.SysRegisterConfigDao;
import com.game.dao.SysRegisterConfigGroupDao;
import com.game.model.SysRegisterConfig;
import com.game.model.SysRegisterConfigGroup;
import com.game.model.vo.RegisterConfigVo;
import com.game.service.SysRegisterConfigService;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;

@Repository
public class SysRegisterConfigServiceImpl implements SysRegisterConfigService {

	@Autowired
	private SysRegisterConfigDao srcDao;
	@Autowired
	private SysRegisterConfigGroupDao srcGroupDao;

	@Override
	public void updStatus(Long srcId, Long status) {
		SysRegisterConfig src = srcDao.get(srcId);
		if (src.getStatus() != status) {
			src.setStatus(status);
			if (StringUtil.equals(status, SysRegisterConfig.STATUS_DISABLE)) {
				SysLogUtil.log("禁用编号为:" + srcId + "注册配置", LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("启用编号为:" + srcId + "注册配置", LogType.ADMIN_PLATFORM);
			}
			srcDao.save(src);
		}
	}

	@Override
	public Page<Map> getPageConfig(RegisterConfigVo rcvo) {
		return srcDao.getPage(rcvo);
	}

	@Override
	public void saveConfig(SysRegisterConfig config) {
		SysRegisterConfig saveConfig = srcDao.get(config.getId());
		String regex = config.getRegex();
		if (StringUtil.isNotEmpty(regex)) {
			regex = regex.replaceAll("\\\\+", "\\\\");
		}
		if (saveConfig == null) {
			saveConfig = config;
		} else {
			saveConfig.setShow(config.getShow());
			saveConfig.setValidate(config.getValidate());
			saveConfig.setRequired(config.getRequired());
			saveConfig.setRegex(regex);
			saveConfig.setUnique(config.getUnique());
			saveConfig.setSource(config.getSource());
			saveConfig.setName(config.getName());
			saveConfig.setKey(config.getKey());
			saveConfig.setType(config.getType());
			saveConfig.setStatus(config.getStatus());
		}
		if (StringUtil.isNotEmpty(saveConfig.getSource())) {
			saveConfig.setSource(saveConfig.getSource().replaceAll("，", ","));
		}

		if (srcDao.isNotUnique(saveConfig, "key,platform")) {
			throw new GenericException("属性英文已存在！");
		}
		if (StringUtil.isEmpty(saveConfig.getId())) {
			SysLogUtil.log("新增注册配置:" + saveConfig.getName(), LogType.ADMIN_PLATFORM);
		} else {
			SysLogUtil.log("修改注册配置:" + saveConfig.getName(), LogType.ADMIN_PLATFORM);
		}
		srcDao.save(saveConfig);
	}

	@Override
	public void delConfig(Long srcId) {
		srcDao.delete(srcId);
		SysLogUtil.log("删除编号为:" + srcId + "注册配置:", LogType.ADMIN_PLATFORM);
	}

	@Override
	public List<Map> getStationRegConf(RegisterConfigVo rcvo) {
		rcvo.setStationId(StationUtil.getStationId());
		rcvo.setStatus(SysRegisterConfig.STATUS_ENABLE);
		if (Validator.isNull(rcvo.getPlatform())) {
			rcvo.setPlatform(SysRegisterConfig.TYPE_MEMBER);
		}
		return srcDao.getStationRegConf(rcvo);
	}

	@Override
	public List<Map> getStationRegVals() {
		Long stationId = StationUtil.getStationId();
		return srcGroupDao.getRegConfValsByStationId(stationId);
	}

	@Override
	public List<Map> getRegConfVal(RegisterConfigVo rcvo) {
		Long stationId = StationUtil.getStationId();
		rcvo.setStationId(stationId);
		rcvo.setStatus(SysRegisterConfig.STATUS_ENABLE);
		return srcGroupDao.getRegConfVal(rcvo);
	}

	@Override
	public void updateProp(Long id, String prop, Long value,String name,String remindText) {
		if ((StringUtils.isEmpty(prop) || value == null) &&(StringUtil.isEmpty(name) && StringUtil.isEmpty(remindText))) {
			throw new GenericException("参数为空！");
		}
		
		SysRegisterConfigGroup old = srcGroupDao.getOneByExample(id, StationUtil.getStationId());
		if (old != null) {
			if(StringUtil.isNotEmpty(prop)&&value!=null){
				int updateFlag = srcGroupDao.updateProp(old.getId(), prop, value);
				if (updateFlag == 1)
					SysLogUtil.log("修改注册配置id=" + id + "  " + prop + " = " + value, LogType.AGENT_SYSTEM);
			}else{
				int updateFlag = srcGroupDao.updateText(old.getId(), name, remindText);
				if (updateFlag == 1){
					SysLogUtil.log("修改注册配置id=" + id + " name="+name+" remindText="+remindText, LogType.AGENT_SYSTEM);
				}
			}
		} else {
			SysRegisterConfig conf = srcDao.get(id);
			old = new SysRegisterConfigGroup();
			old.setConfigId(id);
			old.setStationId(StationUtil.getStationId());
			old.setShowVal(conf.getShow());
			old.setRequiredVal(1L);
			old.setRequiredVal(1L);
			old.setValidateVal(1L);
			old.setUniqueVal(1L);
			if(prop!=null){
				switch (prop) {
				case "show":
					old.setShowVal(value);
					break;
				case "required":
					old.setRequiredVal(value);
					break;
				case "validate":
					old.setValidateVal(value);
					break;
				case "unique":
					old.setUniqueVal(value);
					break;
				default:
				}
			}
			boolean flag= true;
			if(StringUtil.isNotEmpty(name)){
				old.setNameVal(name);
				flag= false;
			}
			if(StringUtil.isNotEmpty(remindText)){
				old.setRemindText(remindText);
				flag = false;
			}
			srcGroupDao.insert(old);
			if(flag){
				SysLogUtil.log("新增注册配置id=" + id + "  " + prop + " = " + value, LogType.AGENT_SYSTEM);
			}else{
				SysLogUtil.log("修改注册配置id=" + id + " name="+name+" remindText="+remindText, LogType.AGENT_SYSTEM);
			}
			
		}
	}

	@Override
	public void saveStationConfGroup(List<Map> datas) {
		if (datas == null || datas.size() == 0) {
			throw new GenericException("保存数据为空！");
		}
		Long stationId = StationUtil.getStationId();
		Long platform = StringUtil.toLong(datas.get(0).get("platform"));
		if (platform.longValue() == 0l) {
			throw new GenericException("数据异常！");
		}
		Long configId = -1l;
		Long showVal = 1l;
		Long validateVal = 1l;
		Long requiredVal = 1l;
		Long uniqueVal = 1l;
		// String confName = "";
		List<SysRegisterConfigGroup> saveDatas = new ArrayList<SysRegisterConfigGroup>();
		SysRegisterConfigGroup entity = null;
		for (Map paramData : datas) {
			configId = StringUtil.toLong(paramData.get("id"));
			if (configId.longValue() == 0l) {
				throw new GenericException("数据异常！");
			}

			showVal = getVal(paramData.get("showVal"));
			validateVal = getVal(paramData.get("validateVal"));
			requiredVal = getVal(paramData.get("requiredVal"));
			uniqueVal = getVal(paramData.get("uniqueVal"));
			entity = new SysRegisterConfigGroup();
			entity.setConfigId(configId);
			entity.setStationId(stationId);
			entity.setRequiredVal(requiredVal);
			entity.setShowVal(showVal);
			entity.setValidateVal(validateVal);
			entity.setUniqueVal(uniqueVal);
			saveDatas.add(entity);
		}

		srcGroupDao.delStationConfByPlat(stationId, platform);
		srcGroupDao.batchInsert(saveDatas);
		SysLogUtil.log("修改注册配置", LogType.AGENT_SYSTEM);
	}

	private Long getVal(Object obj) {
		Long res = 1l;
		if (obj == null) {
			return res;
		}
		res = StringUtil.toLong(obj);
		if (res.longValue() == 0l) {
			return 1l;
		}
		return res;
	}
}