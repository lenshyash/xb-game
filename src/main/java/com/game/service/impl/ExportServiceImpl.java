package com.game.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.SysAccountDao;
import com.game.model.SysAccount;
import com.game.model.vo.AccountVo;
import com.game.service.ExportService;
import com.game.util.PoiUtil;
import com.game.util.SysLogUtil;

@Repository
public class ExportServiceImpl implements ExportService {

	@Autowired
	SysAccountDao accountDao;

	@Override
	public void export(AccountVo avo,Boolean exportAll) {
		String title = "用户信息";
		String[] rowsName = new String[] { "序号", "用户账号", "用户类型", "用户姓名", "银行卡号" };
		List<Map> acLst = accountDao.getExportDate(avo,exportAll);
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < acLst.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(acLst.get(i).get("account"));
			objs[2] = getTypeName(acLst.get(i).get("accountType"));
			objs[3] = getStrNull(acLst.get(i).get("userName"));
			objs[4] = getStrNull(acLst.get(i).get("cardNo"));
			dataList.add(objs);
		}
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出会员银行信息", LogType.AGENT_FINANCE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getTypeName(Object obj) {
		String type = getStrNull(obj);
		if (type.equals(SysAccount.ACCOUNT_PLATFORM_AGENT + "")) {
			return "代理";
		} else if (type.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER + "")) {
			return "会员";
		}
		return "";
	}

	private String getStrNull(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		return obj.toString();
	}

}
