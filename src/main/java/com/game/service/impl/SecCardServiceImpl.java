package com.game.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.game.dao.SecCardDao;
import com.game.dao.SysAccountDao;
import com.game.model.SecCard;
import com.game.model.SysAccount;
import com.game.service.SecCardService;

@Service
public class SecCardServiceImpl implements SecCardService{
	
	private static String[] PASSWORD_KEYS = new String[]{"A","B","C","D","E","F","G","H","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9"};
	
	@Autowired
	private SecCardDao cardDao;
	
	@Autowired
	private SysAccountDao accountDao;
	
	@Override
	public SecCard createCard() {
		SecCard card = new SecCard();
		card.setCreateDatetime(new Date());
		card.setUseCount(0L);
		String pwd = JSONObject.toJSONString(randomPassword());
		card.setCardPassword(pwd);
		card.setCode(randomCardNo());
		int i = 0;
		//判断卡号数据是否存在，若存在重新生成卡号   最多重复三次
		while(cardDao.isNotUnique(card, "code") && i < 3){
			card.setCode(randomCardNo());
			i++;
		}
		cardDao.save(card);
		return card;
	}
	
	@Override
	public SecCard getCard(long id) {
		return cardDao.get(id);
	}
	
	/**
	 * 生成密保卡
	 * @return
	 */
	private String[][] randomPassword(){
		String [][] pwdArr = new String[8][8];
		for (int i = 0; i < pwdArr.length; i++) {
			for (int j = 0; j < pwdArr[i].length; j++) {
				pwdArr[i][j] = PASSWORD_KEYS[(int) (Math.random() * PASSWORD_KEYS.length)] ;
			}
		}
		return pwdArr;
	}
	
	private String randomCardNo(){
		Random random = new Random();
		return ""+(random.nextInt(8999)+1000)+(random.nextInt(8999)+1000)+(random.nextInt(8999)+1000)+(random.nextInt(8999)+1000);
	}

	@Override
	public void bind(String cardNo, long userId,long stationType) {
		cardNo = cardNo.replaceAll(" ", "");//替换所有空格
		SysAccount account = accountDao.getFromCache(userId);
		if(account == null){
			throw new GenericException("账号不存在");
		}
		
		if(StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)){
			throw new GenericException("会员账号不能绑定密保卡");
		}
		SecCard card = cardDao.getCardByCode(cardNo);
		if(card == null){
			throw new GenericException("密保卡不存在");
		}
		long cardId = card.getId();
		Long stationId = card.getStationId();
		if(stationId != null && !StringUtil.equals(stationId, account.getStationId())){
			throw new GenericException("密保卡已经被其他站点使用");
		}
		
		if(cardDao.hasBind(userId, cardId,stationType)){
			throw new GenericException("该账号已经绑定这张密保卡");
		}
		cardDao.bindCard(userId, cardId,stationType);
		cardDao.updateBindStatus(account.getStationId(), cardId);
	}

	@Override
	public Page getPage() {
		return cardDao.getPage();
	}

	@Override
	public List getAgentUsers(long stationId) {
		return accountDao.getAgentUsers(stationId);
	}

	@Override
	public List<SecCard> getBindCards(long userId) {
		return cardDao.getBindCards(userId, SecCard.STATION_TYPE_AGENT);
	}

	@Override
	public void unbind(long cardId, long userId, long stationType) {
		int rows = cardDao.unbind(cardId, userId, stationType);
		if(rows > 0){
			cardDao.subtrUseCount(cardId);
		}
	}
}
