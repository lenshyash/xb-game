package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryRoomDao;
import com.game.dao.platform.MemberBackwaterStrategyDao;
import com.game.model.MemberBackwaterStrategyLevel;
import com.game.model.lottery.BcLotteryRoom;
import com.game.model.platform.MemberBackwaterStrategy;
import com.game.service.MemberBackwaterStrategyLevelService;
import com.game.service.MemberBackwaterStrategyService;
import com.game.util.BigDecimalUtil;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Service
public class MemberBackwaterStrategyServiceImpl implements MemberBackwaterStrategyService {
	@Autowired
	private MemberBackwaterStrategyDao memberBackwaterStrategyDao;
	@Autowired
	private BcLotteryRoomDao lotteryRoomDao;
	@Autowired
	private MemberBackwaterStrategyLevelService memberBackwaterStrategyService;
	@Autowired
	private MemberBackwaterStrategyLevelService memberBackwaterStrategyLevelService;

	@Override
	public Page page(Integer type, Long stationId) {
		Page<MemberBackwaterStrategy> page = memberBackwaterStrategyDao.page(type, stationId);
		boolean isV4 = "v4".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.lottery_template_name));
		if(isV4){
			if(page.getList() != null && !page.getList().isEmpty()){
				for(MemberBackwaterStrategy strategy : page.getList()){
					strategy.setRoomName(lotteryRoomDao.get(strategy.getRoomId()).getName());
				}
			}
		}
		return page;
	}

	@Override
	public void closeOrOpen(Integer status, Long stationId, Long id) {
		if (id == null || stationId == null)
			return;
		memberBackwaterStrategyDao.closeOrOpen(status, stationId, id);
	}

	@Override
	public MemberBackwaterStrategy getOne(Long id, Long stationId) {
		if (id == null || stationId == null)
			return null;
		return memberBackwaterStrategyDao.getOne(id, stationId);
	}

	@Override
	public void insert(MemberBackwaterStrategy mrbs,Long[] groupLevelIds) {
		if (groupLevelIds == null || groupLevelIds.length == 0) {
			throw new GenericException("请至少选择一个限制等级");
		}
		if (mrbs.getRate() == null) {
			throw new GenericException("反水比例不能为空");
		}
		if (mrbs.getQualified() == null || mrbs.getQualified().compareTo(BigDecimal.ZERO) < 0) {
			throw new GenericException("有效投注不能为空");
		}
		if (mrbs.getRate().compareTo(BigDecimal.ZERO) < 0) {
			throw new GenericException("反水比例不能小于0");
		}
		if (mrbs.getRate().compareTo(BigDecimalUtil.HUNDRED) >= 0) {
			throw new GenericException("反水比例不能大于100");
		}
		if (mrbs.getUpperLimit() == null || mrbs.getUpperLimit().compareTo(BigDecimal.ZERO) <= 0) {
			mrbs.setUpperLimit(BigDecimal.ZERO);
		} else {
			BigDecimal minRate = BigDecimalUtil.divide(BigDecimalUtil.multiply(mrbs.getRate(), mrbs.getQualified()), BigDecimalUtil.HUNDRED);
			if (mrbs.getUpperLimit().compareTo(minRate) <= 0) {
				throw new GenericException("最高返水上限必须大于反水比例乘于有效投注" + minRate);
			}
		}
		if (mrbs.getMultiple() == null) {
			mrbs.setMultiple(BigDecimal.ZERO);
		}
		if (mrbs.getCreateDatetime() == null) {
			mrbs.setCreateDatetime(new Date());
		}
		memberBackwaterStrategyDao.insert(mrbs);
		if (groupLevelIds != null) {
			List<MemberBackwaterStrategyLevel> dllist = new ArrayList<>();
			MemberBackwaterStrategyLevel dl = null;
			for (Long lid : groupLevelIds) {
				dl = new MemberBackwaterStrategyLevel();
				dl.setStrategyId(mrbs.getId());
				dl.setStationId(mrbs.getStationId());
				dl.setMemberLevelId(lid);
				dllist.add(dl);
			}
			memberBackwaterStrategyService.batchInsert(dllist);
		}
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + mrbs.getStationId() + "_s_" + mrbs.getId()+"_level_group");
	}

	@Override
	public void update(MemberBackwaterStrategy mrbs,Long[] groupLevelIds) {
		if (groupLevelIds == null || groupLevelIds.length == 0) {
			throw new GenericException("请至少选择一个限制等级");
		}
		if (mrbs.getRate() == null) {
			throw new GenericException("反水比例不能为空");
		}
		if (mrbs.getQualified() == null || mrbs.getQualified().compareTo(BigDecimal.ZERO) < 0) {
			throw new GenericException("有效投注不能为空");
		}
		if (mrbs.getRate().compareTo(BigDecimal.ZERO) < 0) {
			throw new GenericException("反水比例不能小于0");
		}
		if (mrbs.getRate().compareTo(BigDecimalUtil.HUNDRED) >= 0) {
			throw new GenericException("反水比例不能大于100");
		}
		if (mrbs.getUpperLimit() == null || mrbs.getUpperLimit().compareTo(BigDecimal.ZERO) <= 0) {
			mrbs.setUpperLimit(BigDecimal.ZERO);
		} else {
			BigDecimal minRate = BigDecimalUtil.divide(BigDecimalUtil.multiply(mrbs.getRate(), mrbs.getQualified()), BigDecimalUtil.HUNDRED);
			if (mrbs.getUpperLimit().compareTo(minRate) <= 0) {
				throw new GenericException("最高返水上限必须大于反水比例乘于有效投注" + minRate);
			}
		}
		if (mrbs.getMultiple() == null) {
			mrbs.setMultiple(BigDecimal.ZERO);
		}
		MemberBackwaterStrategy old = getOne(mrbs.getId(), mrbs.getStationId());
		old.setRate(mrbs.getRate());
		old.setMemo(mrbs.getMemo());
		old.setQualified(mrbs.getQualified());
		old.setUpperLimit(mrbs.getUpperLimit());
		old.setStatus(mrbs.getStatus());
		old.setMultiple(mrbs.getMultiple());
		memberBackwaterStrategyDao.update(old);
		Set<Long> levelIdSet = memberBackwaterStrategyLevelService.getLevelIdsByStrategyId(old.getId(), old.getStationId());
		if (groupLevelIds != null) {
			List<MemberBackwaterStrategyLevel> dllist = new ArrayList<>();
			MemberBackwaterStrategyLevel dl = null;
			for (Long lid : groupLevelIds) {
				if (levelIdSet != null && levelIdSet.contains(lid)) {
					levelIdSet.remove(lid);
					continue;
				}
				dl = new MemberBackwaterStrategyLevel();
				dl.setStrategyId(old.getId());
				dl.setStationId(old.getStationId());
				dl.setMemberLevelId(lid);
				dllist.add(dl);
			}
			memberBackwaterStrategyLevelService.batchInsert(dllist);
			if (levelIdSet != null && !levelIdSet.isEmpty()) {
				memberBackwaterStrategyLevelService.batchDelete(old.getId(), old.getStationId(),
						new ArrayList<>(levelIdSet));
			}
		}
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + old.getStationId() + "_s_" + old.getId()+"_level_group");
	}

	@Override
	public void delete(MemberBackwaterStrategy old) {
		memberBackwaterStrategyDao.delete(old);
		memberBackwaterStrategyLevelService.deleteByStrategyId(old.getId(),StationUtil.getStationId());
		CacheUtil.delCache(CacheType.MEMBER_ROLL_BACK_STRATEGY, "s_" + old.getStationId() + "_s_" + old.getId()+"_level_group");
	}
	
	@Override
	public BigDecimal getLotteryFanShui(Long stationId) {
		return memberBackwaterStrategyDao.getLotteryFanShui(stationId);
	}

	/**
	 * v4版本使用
	 */
	public List<BcLotteryRoom> getRoomStationId(Long stationId){
		return lotteryRoomDao.getRoomStationId(stationId);
	}
}
