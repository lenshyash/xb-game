package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.redis.RedisAPI;
import com.game.dao.AgentLotteryQuotoDao;
import com.game.model.AgentLotteryQuoto;
import com.game.service.AgentLotteryQuotoService;

@Repository
public class AgentLotteryQuotoServiceImpl implements AgentLotteryQuotoService{

	@Autowired
	private AgentLotteryQuotoDao agentLotteryQuotoDao;
	
	@Override
	public void updateBuyMoney(Long stationId, BigDecimal money,String type) {
		String key = "agent_lottery_quoto_" + stationId;
		
		AgentLotteryQuoto quoto = agentLotteryQuotoDao.getOneByStationId(stationId);
		if(quoto==null) {
			quoto = new AgentLotteryQuoto();
			quoto.setStationId(stationId);
			quoto.setBuyMoney(money);
			quoto.setWinMoney(BigDecimal.ZERO);
			quoto.setLastUpdateDatetime(new Date());
			agentLotteryQuotoDao.save(quoto);
		}else {
			if (type.equals("2")){
				agentLotteryQuotoDao.updateBuyMoneySubtract(stationId, money);
				quoto.setBuyMoney(quoto.getBuyMoney().subtract(money));
			}else {
				agentLotteryQuotoDao.updateBuyMoney(stationId, money);
				quoto.setBuyMoney(quoto.getBuyMoney().add(money));
			}
		}
		//更新缓存
		CacheUtil.addCache(CacheType.REAL_GAME_USER, key, JSON.toJSONString(quoto));
		
	}
	
	@Override
	public AgentLotteryQuoto getOneByStationId(Long stationId) {
		String key = "agent_lottery_quoto_" + stationId;
		
		String cache = CacheUtil.getCache(CacheType.REAL_GAME_USER, key);
		if(StringUtil.isNotEmpty(cache)) {
			return JSON.parseObject(cache, AgentLotteryQuoto.class);
		}
		
		AgentLotteryQuoto quoto = agentLotteryQuotoDao.getOneByStationId(stationId);
		return quoto;
	}
	
	public Page<Map> getPage() {
		return agentLotteryQuotoDao.getPage();
	}
}