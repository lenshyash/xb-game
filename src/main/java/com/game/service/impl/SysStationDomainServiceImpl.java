package com.game.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.LogType;
import com.game.dao.SysAccountDao;
import com.game.dao.SysStationDomainDao;
import com.game.model.SysAccount;
import com.game.model.SysStation;
import com.game.model.SysStationDomain;
import com.game.model.vo.StationVo;
import com.game.service.SysStationDomainService;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class SysStationDomainServiceImpl implements SysStationDomainService {

	@Autowired
	private SysStationDomainDao sysStationDomainDao;

	@Autowired
	private SysAccountDao accountDao;

	@Override
	public Page<SysStationDomain> getPageForAgent(String domain, String agentName, Long stationId) {
		return sysStationDomainDao.getPageForAgent(domain, agentName, stationId, SysStationDomain.TYPE_MEMBER);
	}

	@Override
	public Page<Map> getPage(StationVo svo) {
		return sysStationDomainDao.getPage(svo);
	}

	@Override
	public void saveDomain(SysStationDomain domain) {
		Long domainId = domain.getId();

		domain.setDomain(StringUtil.trim2Empty(domain.getDomain()));
		if (sysStationDomainDao.isNotUnique(domain, "domain")) {
			throw new GenericException("域名:[" + domain.getDomain() + "]已经存在");
		}
		String folder = null;
		String oldDomail = null;
		SysStationDomain saveDomain = null;
		if (domainId == null) {
			saveDomain = domain;
			SysLogUtil.log("新增平台站点编号为:" + domain.getStationId()+",模板编号为:"+domain.getFolder()+ "的域名:" + domain.getDomain(), LogType.ADMIN_PLATFORM);
		} else {

			if(StringUtil.isNotEmpty(domain.getFolder())) {
				folder = domain.getFolder().trim().toLowerCase();
			}
			saveDomain = sysStationDomainDao.get(domainId);
			oldDomail = saveDomain.getDomain();
			saveDomain.setDomain(domain.getDomain());
			saveDomain.setStationId(domain.getStationId());
			saveDomain.setStatus(domain.getStatus());
			saveDomain.setIsRegSwitch(domain.getIsRegSwitch());
			saveDomain.setIsXlSwitch(domain.getIsXlSwitch());
			saveDomain.setType(domain.getType());
			saveDomain.setDefaultHome(StringUtils.trim(domain.getDefaultHome()));
			saveDomain.setFolder(folder);
			saveDomain.setDomainStationName(domain.getDomainStationName());
			saveDomain.setDomainPlatToken(domain.getDomainPlatToken());
			saveDomain.setIsChatSwitch(domain.getIsChatSwitch());
			saveDomain.setInterfaceLimit(domain.getInterfaceLimit());
			saveDomain.setLimitNum(domain.getLimitNum());
			if(domain.getInterfaceLimit()!=null ){
				String key = "LIMIT:DOMAIN:";
				if(domain.getInterfaceLimit()==SysStationDomain.STATUS_ENABLE){
					CacheUtil.addCache(CacheType.STATION_DOMAIN,key+domain.getDomain(),domain.getLimitNum().toString());
				}else{
					CacheUtil.delCache(CacheType.STATION_DOMAIN,key+domain.getDomain());
				}
			}

			SysLogUtil.log("更新平台站点编号为:" + domain.getStationId() +",模板编号为:"+domain.getFolder()+  "的域名:" + domain.getDomain(), LogType.ADMIN_PLATFORM);
		}
		if (StringUtils.isNotEmpty(StringUtils.trim(domain.getAgentName()))) {
			SysAccount agent = accountDao.getByAccountAndTypeAndStationId(StringUtils.trim(domain.getAgentName()),
					domain.getStationId(), SysAccount.ACCOUNT_PLATFORM_AGENT);
			if (agent == null) {
				throw new GenericException("默认代理不存在！");
			}
			saveDomain.setAgentId(agent.getId());
			saveDomain.setAgentName(agent.getAccount());
		} else {
			saveDomain.setAgentId(null);
			saveDomain.setAgentName(null);
		}
		sysStationDomainDao.save(saveDomain);
		if (oldDomail != null) {
			CacheUtil.delCache(CacheType.STATION_DOMAIN, oldDomail);
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
		}
		CacheUtil.delCache(CacheType.STATION_DOMAIN, saveDomain.getDomain());
	}

	@Override
	public void updStatus(Long domainId, Long status) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("该域名不存在");
		}
		if (domain.getStatus() == null || domain.getStatus().intValue() != status.intValue()) {
			domain.setStatus(status);
			if (StringUtil.equals(status, SysStationDomain.STATUS_DISABLE)) {
				SysLogUtil.log("禁用平台站点域名为:" + domain.getDomain(), LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("启用平台站点域名为:" + domain.getDomain(), LogType.ADMIN_PLATFORM);
			}
			CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
			sysStationDomainDao.save(domain);
		}
	}

	@Override
	public void deleteDomain(Long domainId) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("非法请求!");
		}
		sysStationDomainDao.delete(domainId);
		CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
		CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
		SysLogUtil.log("删除平台站点编号为:" + domainId + "的域名:", LogType.ADMIN_PLATFORM);
	}

	@Override
	public void updateIsRegSwitch(Long domainId, Long isRegSwitch) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("该域名不存在");
		}
		if (domain.getIsRegSwitch() == null || domain.getIsRegSwitch().intValue() != isRegSwitch.intValue()) {
			domain.setIsRegSwitch(isRegSwitch);
			if (StringUtil.equals(isRegSwitch, SysStationDomain.STATUS_DISABLE)) {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的注册关闭", LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的注册开启", LogType.ADMIN_PLATFORM);
			}
			CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
			sysStationDomainDao.save(domain);
		}
	}

	@Override
	public void updateIsXlSwitch(Long domainId, Long isXlSwitch) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("该域名不存在");
		}
		if (domain.getIsXlSwitch() == null || domain.getIsXlSwitch().intValue() != isXlSwitch.intValue()) {
			domain.setIsXlSwitch(isXlSwitch);
			if (StringUtil.equals(isXlSwitch, SysStationDomain.STATUS_DISABLE)) {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的线路检测关闭", LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的线路检测开启", LogType.ADMIN_PLATFORM);
			}
			CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
			sysStationDomainDao.save(domain);
		}
	}

	@Override
	public List<String> getDomain(Long domainId) {
		List<SysStationDomain> lists = sysStationDomainDao.list(domainId);
		List<String> list = new ArrayList<>();
		if (lists != null && lists.size() > 0) {
			for (SysStationDomain s : lists) {
				if (s.getIsXlSwitch() != null && s.getIsXlSwitch() == SysStationDomain.STATUS_ENABLE) {
					list.add(s.getDomain());
				}
			}
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> getAll() {
		return sysStationDomainDao.getAllActive();
	}

	@Override
	public SysStationDomain findOne(Long id, Long stationId) {
		if (id == null) {
			return null;
		}
		return sysStationDomainDao.findOne(id, stationId);
	}

	@Override
	public void updStatusForAgent(Long id, Long status, Long stationId) {
		SysStationDomain old = findOne(id, stationId);
		if (old == null || old.getType() != SysStationDomain.TYPE_MEMBER) {
			return;
		}
		String str = "关闭";
		if (status == SysStationDomain.STATUS_ENABLE) {
			str = "启用";
		} else {
			status = SysStationDomain.STATUS_DISABLE;
		}
		if (old.getStatus() == null || status.intValue() != old.getStatus().intValue()) {
			sysStationDomainDao.updStatus(id, status, stationId);
			CacheUtil.delCache(CacheType.STATION_DOMAIN, old.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ stationId);
			SysLogUtil.log("修改站点域名:" + old.getDomain() + "状态为:" + str, LogType.AGENT_SYSTEM);
		}
	}

	@Override
	public void delete(Long id, Long stationId) {
		SysStationDomain old = findOne(id, stationId);
		if (old == null || old.getType() != SysStationDomain.TYPE_MEMBER) {
			return;
		}
		sysStationDomainDao.delete(id);
		CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ stationId);
		CacheUtil.delCache(CacheType.STATION_DOMAIN, old.getDomain());
		SysLogUtil.log("删除站点域名:" + old.getDomain(), LogType.AGENT_SYSTEM);
	}

	@Override
	public void updateDomainForAgent(Long id, String domain, String agentName, String defaultHome, Long stationId) {
		SysStationDomain old = sysStationDomainDao.findByDomain(domain);
		if (old != null && !old.getId().equals(id)) {
			throw new GenericException("域名:[" + domain + "]已经存在");
		}
		old = findOne(id, stationId);
		if (old == null || old.getType() != SysStationDomain.TYPE_MEMBER) {
			throw new GenericException("域名不存在！");
		}
		Long agentId = null;
		agentName = StringUtils.trim(agentName);
		if (StringUtils.isNotEmpty(agentName)) {
			SysAccount agent = accountDao.getByAccountAndTypeAndStationId(agentName, stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);
			if (agent == null) {
				throw new GenericException("默认代理不存在！");
			}
			agentId = agent.getId();
		}
		defaultHome = StringUtils.trim(defaultHome);
		sysStationDomainDao.updateDomain(id, domain, agentId, agentName, defaultHome, stationId);
		CacheUtil.delCache(CacheType.STATION_DOMAIN, old.getDomain());
		CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ stationId);
		StringBuilder sb = new StringBuilder("修改域名：");
		sb.append(old.getDomain());
		if (!StringUtils.equals(old.getDomain(), domain)) {
			sb.append(",新域名：").append(domain);
		}
		if (!StringUtils.equals(agentName, old.getAgentName())) {
			sb.append(",新代理：").append(agentName);
		}
		if (!StringUtils.equals(defaultHome, old.getDefaultHome())) {
			sb.append(",新首页：").append(defaultHome);
		}
		SysLogUtil.log(sb.toString(), LogType.AGENT_SYSTEM);
	}

	@Override
	public String saveDomain(String domainNames, String agentName, String defaultHome, Long stationId) {
		String[] ds = domainNames.split("\n|,| ");
		StringBuilder info = new StringBuilder("添加域名：");
		StringBuilder result = new StringBuilder();
		SysStationDomain d = null;
		Long agentId = null;
		agentName = StringUtils.trim(agentName);
		if (StringUtils.isNotEmpty(agentName)) {
			SysAccount agent = accountDao.getByAccountAndTypeAndStationId(agentName, stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);
			if (agent == null) {
				throw new GenericException("默认代理不存在！");
			}
			agentId = agent.getId();
		}
		Long createUserId = UserUtil.getUserId();
		for (String dom : ds) {
			try {
				dom = StringUtils.trim(dom);
				if (StringUtils.isNotEmpty(dom)) {
					d = sysStationDomainDao.findByDomain(dom);
					if (d != null) {
						result.append("域名:[").append(dom).append("]已经存在<br>");
						continue;
					}
					d = new SysStationDomain();
					d.setId(null);
					d.setDomain(dom);
					d.setStatus(SysStationDomain.STATUS_ENABLE);
					d.setIsRegSwitch(SysStationDomain.STATUS_ENABLE);
					d.setFlagActive(1L);
					d.setStationId(stationId);
					d.setCreateDatetime(new Date());
					d.setCreateUserId(createUserId);
					d.setAgentId(agentId);
					d.setAgentName(agentName);
					d.setType(SysStationDomain.TYPE_MEMBER);
					d.setDefaultHome(defaultHome);
					d.setIsXlSwitch(SysStationDomain.STATUS_DISABLE);
					sysStationDomainDao.insert(d);
					info.append(dom);
				}
			} catch (Exception e) {
				result.append(e.getMessage()).append("<br>");
			}
		}
		info.append(",代理：").append(agentName).append("，默认首页：").append(defaultHome);
		SysLogUtil.log(info.toString(), LogType.AGENT_SYSTEM);
		return result.toString();
	}

	@Override
	public List<SysStationDomain> getFolderLstDomain() {
		return sysStationDomainDao.getFolderLst();
	}

	@Override
	public List<SysStationDomain> getFilterFolderList() {
		return sysStationDomainDao.getFilterFolderList();
	}

	@Override
	public void updateDomainChatToken(Long domainId, String domainChatToken) {
		sysStationDomainDao.updateDomainChatToken(domainId,domainChatToken);
		
	}
	
	@Override
	public SysStationDomain getDomainByFolder(String domain,String domainFolder, Long stationId) {
		return sysStationDomainDao.getDomainByFolder(domain,domainFolder,stationId);
	}

	@Override
	public List<SysStationDomain> getFolderListByStation(Long stationId) {
		// TODO Auto-generated method stub
		return sysStationDomainDao.getFolderListByStation(stationId);
	}

	@Override
	public List<SysStationDomain> getListByStationType(Long stationId, Long type) {
		// TODO Auto-generated method stub
		return sysStationDomainDao.getListByStationType(stationId,type);
	}

	@Override
	public void updateIsChatSwitch(Long domainId, Long isChatSwitch) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("该域名不存在");
		}
		if (domain.getIsChatSwitch() == null || domain.getIsChatSwitch().intValue() != isChatSwitch.intValue()) {
			domain.setIsChatSwitch(isChatSwitch);
			if (StringUtil.equals(isChatSwitch, SysStationDomain.STATUS_DISABLE)) {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的聊天室关闭", LogType.ADMIN_PLATFORM);
			} else {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的聊天室开启", LogType.ADMIN_PLATFORM);
			}
			CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
			sysStationDomainDao.save(domain);
		}
		
	}

	@Override
	public List<SysStationDomain> getDomainList(Long stationId) {
		 List<SysStationDomain> list = sysStationDomainDao.getDomainList(stationId);
		return list;
	}
	
	@Override
	public void deleteDomainByStationId(Long stationId) {
		sysStationDomainDao.deleteDomainByStationId(stationId);
		
	}

	@Override
	public SysStationDomain getAccessDomain(Long stationId) {
		return sysStationDomainDao.getAccessDomain(stationId);
	}

	@Override
	public void updateInterfaceLimit(Long domainId, Long interfaceLimit) {
		SysStationDomain domain = sysStationDomainDao.get(domainId);
		if (domain == null) {
			throw new GenericException("该域名不存在");
		}
		if (domain.getInterfaceLimit() == null || domain.getInterfaceLimit().intValue() != interfaceLimit.intValue()) {
			domain.setInterfaceLimit(interfaceLimit);
			String key = "LIMIT_DOMAIN_";
			if (StringUtil.equals(interfaceLimit, SysStationDomain.STATUS_DISABLE)) {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的接口访问限制关闭", LogType.ADMIN_PLATFORM);
				CacheUtil.delCache(CacheType.STATION_DOMAIN,key+domain.getDomain());
			} else {
				SysLogUtil.log("平台站点域名为:" + domain.getDomain() + "的接口访问限制开启,当前每秒限制访问次数："+domain.getLimitNum(), LogType.ADMIN_PLATFORM);
				BigDecimal num = domain.getLimitNum();
				if(num==null){
					num = new BigDecimal(3);
				}
				CacheUtil.addCache(CacheType.STATION_DOMAIN,key+domain.getDomain(),num.toString());
			}
			CacheUtil.delCache(CacheType.STATION_DOMAIN, domain.getDomain());
			CacheUtil.delCache(CacheType.STATION_DOMAIN, SysStation.MULTI_FOLDER_KEY+ domain.getStationId());
			sysStationDomainDao.save(domain);
		}

	}

	@Override
	public void saveRemark(Long id, String remark) {
		SysStationDomain domain = sysStationDomainDao.get(id);
		if(domain!=null){
			domain.setRemark(remark);
			sysStationDomainDao.save(domain);
		}
	}
}