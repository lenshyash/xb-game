package com.game.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.SysAccountDao;
import com.game.dao.SysLogDao;
import com.game.dao.SysLoginLogDao;
import com.game.model.SysAccount;
import com.game.model.SysLog;
import com.game.model.SysLoginLog;
import com.game.model.vo.AccountVo;
import com.game.model.vo.SysLogVo;
import com.game.model.vo.SysLoginLogVo;
import com.game.service.SysLogService;
import com.game.util.IPSeeker;
import com.game.util.PoiUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class SysLogServiceImpl implements SysLogService {

	@Autowired
	private SysLogDao logDao;

	@Autowired
	private SysLoginLogDao loginLogDao;
	
	@Autowired
	private SysAccountDao sysAccountDao;

	@Override
	public SysLog addLog(SysLog log) {
		return logDao.save(log);
	}

	@Override
	public SysLoginLog addLoginLog(SysLoginLog log) {
		return loginLogDao.save(log);
	}

	@Override
	public Page getLogsPage(SysLogVo slvo) {
		Page slvop = logDao.getLogsPage(slvo);
		List<Map> slovl = slvop.getList();
		if (slovl != null) {
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : slovl) {
				if(m.get("accountIp")!=null){
					String ip = (String) m.get("accountIp");
					String ipArea = ips.getIpAddr(ip);
					if(StringUtil.isEmpty(ipArea)){
						ipArea = ips.getCountry((String) m.get("accountIp"));
						if (ipArea.equals("IANA")) {
							ipArea = "";
						}
					}
					m.put("address", ipArea);
				}
			}
		}
		return slvop;
	}

	@Override
	public Page getLoginLogPage(SysLoginLogVo slvo) {
		// 平台日志查询，站点为空返回
		if (StringUtil.equals(slvo.getPlatform(), SysLoginLog.PLATFORM_PLATFORM) && Validator.isNull(slvo.getStationId())) {
			return new Page();
		}
		
		if (StringUtil.isNotEmpty(slvo.getAgentName())) {
			AccountVo avo = new AccountVo();
			avo.setStationId(slvo.getStationId());
			avo.setAccount(slvo.getAgentName());
			SysAccount agent = sysAccountDao.queryAccount(avo);
			if (agent == null
					|| (StationUtil.isDailiStation() && !StringUtil.equals(agent.getId(), UserUtil.getUserId())
							&& agent.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1)) {
				throw new GenericException("代理不存在！");
			}
			String parents = agent.getParents();
			if (StringUtil.isEmpty(parents)) {
				parents = ",";
			}
			slvo.setParents(parents + agent.getId() + ",");
			slvo.setSearchSelf(true);
			slvo.setSelfId(agent.getId());
		}
		
		
		Page slvop = loginLogDao.getLoginLogPage(slvo);
		List<Map> slovl = slvop.getList();
		if (slovl != null) {
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : slovl) {
				//优先使用太平洋网络ip获取
				String ip = (String) m.get("accountIp");
				String ipArea = ips.getIpAddr(ip);
				if(StringUtil.isEmpty(ipArea)){
					ipArea = ips.getCountry(ip);
					if (ipArea.equals("IANA")) {
						ipArea = "";
					}
				}
				m.put("address", ipArea);
			}
		}
		return slvop;
	}
	@Override
	public SysLog getOne(Long id, Long stationId) {
		if (id != null) {
			SysLog sl = logDao.get(id);
			if (sl != null && (stationId == null || sl.getStationId().equals(stationId))) {
				return sl;
			}
		}
		return null;
	}

	@Override
	public SysLog saveLog(SysLog log) {
		return logDao.save(log);
	}

	@Override
	public void exportLoginLog(SysLoginLogVo slvo) {
		List<Map> list = loginLogDao.getList(slvo);
		
		if (list != null) {
			//ip地址处理
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : list) {
				String ipArea = ips.getCountry((String) m.get("accountIp"));
				if (ipArea.equals("IANA")) {
					ipArea = "";
				}
				m.put("address", ipArea);
				
				//代理类型处理
				String accountType = m.get("accountType").toString();
				if("1".equals(accountType)) {
					m.put("accountTypeText", "会员");
				}else if("2".equals(accountType)) {
					m.put("accountTypeText", "公司超级管理员");
				}else if("3".equals(accountType)) {
					m.put("accountTypeText", "公司管理员");
				}else if("4".equals(accountType)) {
					m.put("accountTypeText", "代理");
				}else if("5".equals(accountType)) {
					m.put("accountTypeText", "总代理");
				}else if("6".equals(accountType)) {
					m.put("accountTypeText", "试玩账号");
				}else if("9".equals(accountType)) {
					m.put("accountTypeText", "引导账号");
				}
				
			}
		}
		String title = "会员登陆日志";
		String[] rowsName = new String[] { "序号","编号", "账号", "用户类型", "操作记录", "IP", "IP地址", "操作系统", "操作时间" };
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < list.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(list.get(i).get("id"));
			objs[2] = getStrNull(list.get(i).get("account"));
			objs[3] = getStrNull(list.get(i).get("accountTypeText"));
			objs[4] = getStrNull(list.get(i).get("content"));
			objs[5] = getStrNull(list.get(i).get("accountIp"));
			objs[6] = getStrNull(list.get(i).get("address"));
			objs[7] = getStrNull(list.get(i).get("loginOs"));
			objs[8] = getStrNull(list.get(i).get("createDatetime"));
			dataList.add(objs);
		}
		
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出" + title, LogType.AGENT_SYSTEM);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void exportOperationLog(SysLogVo slvo) {
		List<Map> list = logDao.getlist(slvo);
		
		if (list != null) {
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : list) {
				//ip地址处理
				String ipArea = ips.getCountry((String) m.get("accountIp"));
				if (ipArea.equals("IANA")) {
					ipArea = "";
				}
				m.put("address", ipArea);
				
				//日志类型处理
				String logType = m.get("logType").toString();
				if("7".equals(logType)) {
					m.put("logType", "系统");
				}else if("8".equals(logType)) {
					m.put("logType", "财务");
				}else if("9".equals(logType)) {
					m.put("logType", "会员");
				}else if("10".equals(logType)) {
					m.put("logType", "代理");
				}else if("11".equals(logType)) {
					m.put("logType", "体育");
				}else if("12".equals(logType)) {
					m.put("logType", "彩票");
				}else if("13".equals(logType)) {
					m.put("logType", "真人");
				}else if("14".equals(logType)) {
					m.put("logType", "报表");
				}
				
			}
		}
		
		String title = "后台操作日志";
		String[] rowsName = new String[] { "序号", "账号", "日志类型", "操作记录", "IP", "IP地址", "操作时间","操作详情" };
		List<Object[]> dataList = new ArrayList<Object[]>();
		Object[] objs = null;
		for (int i = 0; i < list.size(); i++) {
			objs = new Object[rowsName.length];
			objs[0] = i + "";
			objs[1] = getStrNull(list.get(i).get("account"));
			objs[2] = getStrNull(list.get(i).get("logType"));
			objs[3] = getStrNull(list.get(i).get("logContent"));
			objs[4] = getStrNull(list.get(i).get("accountIp"));
			objs[5] = getStrNull(list.get(i).get("address"));
			objs[6] = getStrNull(list.get(i).get("createDatetime"));
			objs[7] = getStrNull(list.get(i).get("params"));
			dataList.add(objs);
		}
		
		PoiUtil ex = new PoiUtil(title, rowsName, dataList, title);
		try {
			ex.export();
			SysLogUtil.log("导出" + title, LogType.AGENT_SYSTEM);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private String getStrNull(Object obj) {
		if (StringUtil.isEmpty(obj)) {
			return "";
		}
		return obj.toString();
	}


	
	
}
