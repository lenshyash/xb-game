package com.game.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.constant.LogType;
import com.game.dao.AdminGroupMenuDao;
import com.game.model.AdminGroupMenu;
import com.game.service.AdminPermissionService;
import com.game.util.SysLogUtil;

@Repository
public class AdminPermissionServiceImpl implements AdminPermissionService {

	@Autowired
	private AdminGroupMenuDao groupMenuDao;

	@Override
	public List<AdminGroupMenu> getGroupPermission(long groupId) {
		return groupMenuDao.getGroupPermission(groupId);
	}

	@Override
	public void savePermission(long groupId, List<AdminGroupMenu> menus) {
		groupMenuDao.deleteAll(groupId);
		groupMenuDao.batchInsert(menus);
		CacheUtil.delCache(CacheType.ADMIN_GROUP_MENU, groupId + "");
		SysLogUtil.log("更新总控系统用户组别编号为：" + groupId + "的权限", LogType.ADMIN_SYSTEM);
	}

}
