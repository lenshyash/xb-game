package com.game.service.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.StationConfig;
import com.game.dao.MnyBetNumberDao;
import com.game.dao.SysAccountDao;
import com.game.model.BetNumRecord;
import com.game.model.SysAccount;
import com.game.model.vo.ReportParamVo;
import com.game.model.vo.ReportVo;
import com.game.service.MnyBetNumberService;
import com.game.service.SysAccountDailyMoneyService;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;

@Repository
public class MnyBetNumberServiceImpl implements MnyBetNumberService {

	@Autowired
	MnyBetNumberDao betNumberDao;
	@Autowired
	SysAccountDailyMoneyService dailyMoneyService;
	@Autowired
	SysAccountDao sysAccountDao;
	@Override
	public void addBetNumber(Long memberId, BigDecimal betNumber, Integer type, String remark, String orderId, Date bizDatetime) {
		betNumberDao.addBetNumberForMember(memberId, betNumber,type,remark,orderId,bizDatetime);
	}
	@Override
	public void clearZeroBetNumber(Long accountId){
		SysAccount ac = sysAccountDao.get(accountId);
		//如果会员打码量已经为0则忽略
		if(ac.getBetNum().compareTo(BigDecimal.ZERO)==0){
			return;
		}
		//判断会员是否余额为0
		ReportParamVo vo = new ReportParamVo();
		vo.setAccountId(accountId);
		vo.setStationId(StationUtil.getStationId());
		ReportVo map = dailyMoneyService.getGlobalReportByNative(vo);
		//计算存款总额是否小于所有输赢
		BigDecimal depositTotal = map.getDepositTotal().add(map.getManualDepositTotal()).subtract(map.getWithdrawTotal()).subtract(map.getManualWithdrawTotal());
		if(depositTotal.compareTo(map.getAllBunko())==-1){
			//清0会员打码量
			betNumberDao.clearBetNumberForMember(accountId, BigDecimal.ZERO, BetNumRecord.TYPE_SYSTEM_CLEAR_ZERO, "会员余额亏损完毕，打码量自动清0", null, new Date(),ac.getBetNum());
		}
		
	}
}