package com.game.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.game.cache.CacheManager;
import com.game.cache.CacheType;
import com.game.dao.*;
import com.game.model.*;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.SysUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.game.cache.redis.RedisAPI;
import com.game.common.Contants;
import com.game.constant.LogType;
import com.game.constant.StationConfig;
import com.game.core.SystemConfig;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.dao.platform.GeneralizeLinkDao;
import com.game.model.dictionary.MoneyRecordType;
import com.game.model.platform.MemberGenrtLink;
import com.game.model.vo.AccountVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.model.vo.RegisterConfigVo;
import com.game.permission.PermissionManager;
import com.game.secret.otp.OneTimePasswordAlgorithm;
import com.game.service.AgentLoginOtpService;
import com.game.service.MnyMoneyService;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.service.SysAccountService;
import com.game.service.SysRegisterConfigService;
import com.game.user.online.OnlineInfo;
import com.game.user.online.OnlineManager;
import com.game.util.DateUtil;
import com.game.util.IPSeeker;
import com.game.util.MD5Util;
import com.game.util.MemberLevelUtil;
import com.game.util.SecurityCardUtil;
import com.game.util.Sha256Util;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;
import com.game.util.ValidateUtil;
import com.game.util.VerifyCodeUtil;
import com.game.util.WebUtil;
import com.game.util.google.GoogleAuthenticator;

import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

@Repository
public class SysAccountServiceImpl implements SysAccountService {

	@Autowired
	private SysAccountDao sysAccountDao;

	@Autowired
	private SysAccountInfoDao sysAccountInfoDao;

	@Autowired
	private ProxyMultiRebateSettingDao pmrsDao;

	@Autowired
	private SysRegisterConfigService regConfService;

	@Autowired
	private MnyMoneyDao moneyDao;

	@Autowired
	private GeneralizeLinkDao generalizeLinkDao;

	@Autowired
	private SecCardDao cardDao;

	@Autowired
	private AgentWhiteIpDao ipDao;

	@Autowired
	private MnyMoneyService moneyService;
	@Autowired
	private MnyBetNumberDao mnyBetNumberDao;
	@Autowired
	private AgentLoginOtpService agentLoginOtpService;

	@Autowired
	private SysAccountDailyMoneyDao dailyMoneyDao;
	
	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	@Autowired
	private ProxyMultiRebateCoreService proxyMultiRebateCoreService;
	@Autowired
	private MemberDepositInfoDao depositInfoDao;
	@Autowired
	private SysStationDao stationDao;
	@Autowired
	private AgentLoginOtpDao agentLoginOtpDao;

	@Autowired
	private SysLoginLogDao sysLoginLogDao;
	
	@Override
	public Page<Map> getPage(AccountVo avo) {
		if (Validator.isNull(avo.getStationId())) {
			return new Page();
		}

		return sysAccountDao.getPage(avo);
	}

	@Override
	public void updPwd(Long id, String pwd, String rpwd) {
		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (id == null || id == 0l) {
			throw new GenericException("该用户不存在或已删除!");
		}

		SysAccount account = sysAccountDao.get(id);

		if (account == null) {
			throw new GenericException("该用户不存在或已删除!");
		}

		if (!StationUtil.isAdminStation() && !StringUtil.equals(account.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求!");
		}

		if (StationUtil.isAgentStation()
				&& StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
			Long curLevel = UserUtil.getLevel();
			if (curLevel == null || account.getLevel() == null) {
				throw new GenericException("用户等级为空!");
			}
			if (curLevel.intValue() > account.getLevel().intValue()) {
				throw new GenericException("没有修改权限!");
			}
		}

		if (StringUtil.isNotEmpty(pwd)) {
			pwd = MD5Util.getMD5String(account.getAccount(), pwd);
			account.setPassword(pwd);
		}

		if (StationUtil.isAdminStation()) {
			SysLogUtil.log("修改站点:" + account.getAccount() + "用户:" + account.getAccount() + "的密码",
					LogType.ADMIN_PLATFORM);
		} else if (StationUtil.isAgentStation()) {
			account.setLoginOddFlag(SysAccount.LOGIN_ODD_LIFT);
			SysLogUtil.log("修改用户:" + account.getAccount() + "的密码", LogType.AGENT_SYSTEM);
		} else if (StationUtil.isMemberStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, account.getAccountType())) {
			account.setLoginOddFlag(SysAccount.LOGIN_ODD_LIFT);
			SysLogUtil.log("修改用户:" + account.getAccount() + "的密码", LogType.AGENT_MEMBER);
		} else if (StationUtil.isDailiStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, account.getAccountType())) {
			SysLogUtil.log("修改用户:" + account.getAccount() + "的密码", LogType.AGENT_AGENT);
		}

		sysAccountDao.save(account);
	}

	@Override
	public void updCurUserPwd(String opwd, String pwd, String rpwd, Long pwdType) {

		Boolean repwdFlag = false;
		Object obj = UserUtil.getSession().getAttribute(SystemConfig.SESSION_DAILI_KEY + "repwdFlag");
		if (obj != null) {
			repwdFlag = (Boolean) obj;
		}
		if (repwdFlag) {
			pwdType = SysAccount.PASSWORD_TYPE_LOGIN;
		}
		if (!ValidateUtil.isPassword(pwd)) {
			throw new GenericException("密码格式不正确！");
		}
		if (StringUtil.isEmpty(pwd)) {
			throw new GenericException("密码不能为代空!");
		}
		if (StringUtil.isEmpty(rpwd)) {
			throw new GenericException("确认密码不能为代空!");
		}
		if (!repwdFlag && StringUtil.isEmpty(opwd)) {
			throw new GenericException("旧密码不能为代空!");
		}

		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}
		if(pwdType==SysAccount.PASSWORD_TYPE_RECEIPT) {
			//纯数字密码位数
			Long stationId = StationUtil.getStationId();
			String lengthText = "";
			lengthText = StationConfigUtil.get(stationId, StationConfig.member_draw_password_length);
			if(StringUtil.isNotEmpty(lengthText)) {
				String pattern = "^[0-9]{"+lengthText+"}$";
				boolean b = Pattern.matches(pattern, pwd);
				if(b==false) {
					throw new GenericException("取款密码只能由"+lengthText+"位纯数字组成");
				}
				
			}
		}
		String curPwd = "";
		String curAccount = UserUtil.getUserAccount();
		SysAccount account = null;
		SysAccountInfo info = null;
		if (StringUtil.equals(pwdType, SysAccount.PASSWORD_TYPE_LOGIN)) {
			account = sysAccountDao.get(UserUtil.getUserId());
			if (account == null) {
				throw new GenericException("该用户不存在或已删除!");
			}
			curPwd = account.getPassword();
		} else if (StringUtil.equals(pwdType, SysAccount.PASSWORD_TYPE_RECEIPT)) {
			info = sysAccountInfoDao.get(UserUtil.getUserId());
			if (info == null) {
				throw new GenericException("该用户不存在或已删除!");
			}
			curPwd = info.getReceiptPwd();
		}
		if (!repwdFlag) {
			opwd = MD5Util.getMD5String(curAccount, opwd);
			if (!opwd.equals(StringUtil.toUpperCase(curPwd))) {
				throw new GenericException("旧密码错误!");
			}
		}

		pwd = MD5Util.getMD5String(curAccount, pwd);
		if (StringUtil.equals(pwdType, SysAccount.PASSWORD_TYPE_LOGIN)) {
			account.setPassword(pwd);
			sysAccountDao.save(account);
//			if (StationUtil.isAgentStation()) {
				SysLogUtil.log("修改密码", LogType.AGENT_SYSTEM);
//			}

		} else if (StringUtil.equals(pwdType, SysAccount.PASSWORD_TYPE_RECEIPT)) {
			info.setReceiptPwd(pwd);
			sysAccountInfoDao.save(info);
//			if (StationUtil.isAgentStation()) {
				SysLogUtil.log("修改取款密码", LogType.AGENT_SYSTEM);
//			}
		}
		if (repwdFlag) {
			UserUtil.getSession().setAttribute(SystemConfig.SESSION_DAILI_KEY + "repwdFlag", false);
		}
		HttpSession session = SysUtil.getSession();
		session.removeAttribute("agent_mush_reset_pwd");
	}

	@Override
	public void updRepPwd(Long id, String pwd, String rpwd) {
		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (id == null || id == 0l) {
			throw new GenericException("该用户不存在或已删除!");
		}

		SysAccount account = sysAccountDao.getFromCache(id);

		if (account == null) {
			throw new GenericException("该用户不存在或已删除!");
		}
		if (!StationUtil.isAdminStation() && !StringUtil.equals(account.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求!");
		}
		//纯数字密码位数
		Long stationId = StationUtil.getStationId();
		String lengthText = "";
		lengthText = StationConfigUtil.get(stationId, StationConfig.member_draw_password_length);
		if(StringUtil.isNotEmpty(lengthText)) {
			String pattern = "^[0-9]{"+lengthText+"}$";
			boolean b = Pattern.matches(pattern, pwd);
			if(b==false) {
				throw new GenericException("取款密码只能由"+lengthText+"位纯数字组成");
			}
			
		}
		SysAccountInfo accountInfo = sysAccountInfoDao.get(id);

		if (StationUtil.isAgentPage()
				&& StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) {
			Long curLevel = UserUtil.getLevel();
			if (curLevel == null || account.getLevel() == null) {
				throw new GenericException("用户等级为空!");
			}
			if (curLevel.intValue() < account.getLevel().intValue()) {
				throw new GenericException("没有修改权限!");
			}
		}

		if (StringUtil.isNotEmpty(pwd)) {
			pwd = MD5Util.getMD5String(account.getAccount(), pwd);
			accountInfo.setReceiptPwd(pwd);
		}
		if (StationUtil.isAdminStation()) {
			SysLogUtil.log("修改站点:" + account.getAccount() + "用户:" + account.getAccount() + "的取款密码",
					LogType.ADMIN_PLATFORM);
		} else if (StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, account.getAccountType())) {
			SysLogUtil.log("修改用户:" + account.getAccount() + "的取款密码", LogType.AGENT_SYSTEM);
		} else if (StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, account.getAccountType())) {
			SysLogUtil.log("修改用户:" + account.getAccount() + "的取款密码", LogType.AGENT_MEMBER);
		} else if (StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, account.getAccountType())) {
			SysLogUtil.log("修改用户:" + account.getAccount() + "的取款密码", LogType.AGENT_AGENT);
		}
		sysAccountInfoDao.save(accountInfo);

	}

	@Override
	public void updStatus(SysAccount account) {

		Long accountId = account.getId();
		Long status = account.getAccountStatus();
		SysAccount saveAccount = null;

		if (Validator.isNull(accountId)) {
			throw new GenericException("该用户不存在或已删除!");
		}

		if (Validator.isNull(status)) {
			throw new GenericException("参数异常!");
		}

		saveAccount = sysAccountDao.get(accountId);

		if (!StationUtil.isAdminStation()
				&& !StringUtil.equals(saveAccount.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求!");
		}

		saveAccount.setAccountStatus(status);
		if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_DISABLED) && StationUtil.isAdminStation()) {
			SysLogUtil.log("禁用站点:" + saveAccount.getStationId() + "用户:" + saveAccount.getAccount(),
					LogType.ADMIN_PLATFORM);
			OnlineManager.forcedOffLine(saveAccount);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_DISABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, saveAccount.getAccountType())) {

			SysLogUtil.log("禁用用户:" + saveAccount.getAccount(), LogType.AGENT_SYSTEM);
			OnlineManager.forcedOffLine(saveAccount);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_DISABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, saveAccount.getAccountType())) {

			SysLogUtil.log("禁用用户:" + saveAccount.getAccount(), LogType.AGENT_MEMBER);
			OnlineManager.forcedOffLine(saveAccount);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_DISABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, saveAccount.getAccountType())) {

			SysLogUtil.log("禁用用户:" + saveAccount.getAccount(), LogType.AGENT_AGENT);
			OnlineManager.forcedOffLine(saveAccount);
		}

		if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_ENABLED) && StationUtil.isAdminStation()) {
			SysLogUtil.log("启用站点:" + saveAccount.getStationId() + "用户:" + saveAccount.getAccount(),
					LogType.ADMIN_PLATFORM);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_ENABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, saveAccount.getAccountType())) {

			SysLogUtil.log("启用用户:" + saveAccount.getAccount(), LogType.AGENT_SYSTEM);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_ENABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, saveAccount.getAccountType())) {

			SysLogUtil.log("启用用户:" + saveAccount.getAccount(), LogType.AGENT_MEMBER);
		} else if (StringUtil.equals(status, SysAccount.ACCOUNT_STATUS_ENABLED) && StationUtil.isAgentStation()
				&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, saveAccount.getAccountType())) {

			SysLogUtil.log("启用用户:" + saveAccount.getAccount(), LogType.AGENT_AGENT);
		}

		sysAccountDao.save(saveAccount);

	}

	@Override
	public List<SysAccount> getAccountsByType(Long type) {
		return sysAccountDao.queryAccountsByType(type);
	}

	@Override
	public SysAccount doLoginForAgent(String account, String pwd, String otpPwd, String verifyCode,String googleCode) {
		validOneTimePassword(account, otpPwd);
		validGoogleCode(account,googleCode);
		SysAccount acc = doLogin(account, pwd, verifyCode, SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
		if ("a123a123".equals(pwd) || "aa123123".equals(pwd) || pwd.matches("^([0-9a-zA-Z])(\\1)+$")
				|| pwd.equals("123456")) {
			HttpSession session = SysUtil.getSession();
			session.setAttribute("agent_mush_reset_pwd", true);
		}
		return acc;
	}

	private void validGoogleCode(String account, String googleCode) {
		boolean googleFlag = "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_bg_login_google_verify_code));
		Long stationId = StationUtil.getStationId();
		//判断租户后台是否开启谷歌验证码开关
		if(StationUtil.isAgentStation() && googleFlag){
			//获取站点后台验证码配置
			AgentLoginOtp otp = agentLoginOtpDao.findGoogleOtp(stationId,AgentLoginOtp.type_two);
			if(otp==null){
				throw new GenericException("暂未绑定谷歌邮箱,请联系客服");
			}
			AgentLoginOtpEscape escapes = agentLoginOtpService.findOneEscape(stationId, account);
			if (escapes != null) {
				return;
			}
			if (StringUtils.isEmpty(googleCode)) {
				throw new GenericException("请输入谷歌验证码");
			}
			//校验谷歌验证码
			if(!GoogleAuthenticator.authcode(googleCode, otp.getPassword())){
				throw new GenericException("谷歌验证码校验失败");
			}
		}
	}

	private void validOneTimePassword(String account, String otpPwd) {
		if (StringUtils.equals("on", StationConfigUtil.get(StationConfig.onoff_agent_login_otp_key))) {
			SysStation station = StationUtil.getStation();
			// 动态密码验证
			List<AgentLoginOtp> otps = agentLoginOtpService.find(station.getId());
			if (otps != null && !otps.isEmpty()) {
				AgentLoginOtpEscape escapes = agentLoginOtpService.findOneEscape(station.getId(), account);
				if (escapes != null) {
					return;
				}
				if (StringUtils.isEmpty(otpPwd)) {
					throw new GenericException("请输入动态密码");
				}
				String otpPwd1 = null;
				for (AgentLoginOtp otp : otps) {
					try {
						otpPwd1 = OneTimePasswordAlgorithm.getOneTimePassword(otp.getIccid(), otp.getPassword(),
								station.getFloder(), 0);
						if (StringUtils.equals(otpPwd, otpPwd1)) {
							return;
						}
						otpPwd1 = OneTimePasswordAlgorithm.getOneTimePassword(otp.getIccid(), otp.getPassword(),
								station.getFloder(), -1);
						if (StringUtils.equals(otpPwd, otpPwd1)) {
							return;
						}
					} catch (Exception e) {
					}
				}
				throw new GenericException("动态密码错误");
			}
		}
	}
	
	private boolean checkAdminIP(String ip) {
		if("127.0.0.1".equals(ip)) {
			return true;
		}
		String whiteIps = StationConfigUtil.get(StationConfig.agent_super_admin_white_ip);
		if(StringUtil.isEmpty(whiteIps)) {
			return true;
		}
		String[] wipArr = whiteIps.split(",");
		for (int i = 0; i < wipArr.length; i++) {
			if(checkIP(ip,  wipArr[i])) {
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIP(String ip, String whiteIp) {
		String[] ipArr = ip.split("\\.");
		String[] wipArr = whiteIp.split("\\.");
		if (ipArr.length != 4 || wipArr.length != 4) {
			return false;
		}

		for (int i = 0; i < wipArr.length; i++) {
			String w1 = wipArr[i];
			if ("*".equals(StringUtil.trim2Null(w1))) {
				continue;
			}
			String w2 = ipArr[i];
			if (w1 == null || w2 == null) {
				return false;
			}
			if (StringUtil.toInt(w2.trim()) != StringUtil.toInt(w1.trim())) {
				return false;
			}
		}
		return true;
	}

	private boolean checkIP(String ip, AgentWhiteIp whiteIp) {
		return checkIP(ip,whiteIp.getIp());
	}

	@Override
	public SysAccount doLoginForDaili(String account, String pwd, String verifyCode) {
		return doLogin(account, pwd, verifyCode, SysAccount.ACCOUNT_PLATFORM_AGENT);
	}

	@Override
	public SysAccount doLoginForMember(String account, String pwd, String verifyCode) {
		return doLogin(account, pwd, verifyCode, SysAccount.ACCOUNT_PLATFORM_MEMBER);
	}

	@Override
	public SysAccount doLoginForMobile(String account, String pwd) {
		return doLogin(account, pwd, "", SysAccount.ACCOUNT_PLATFORM_MEMBER);
	}
	@Override
	public SysAccount doLoginForMobile(String account, String pwd, String verifyCode,String autoLogin) {
		return doLogin(account, pwd, verifyCode, SysAccount.ACCOUNT_PLATFORM_MEMBER,autoLogin);
	}
	@Override
	public SysAccount doLogin(String account, String pwd, String verifyCode, long type) {
		return doLogin(account, pwd, verifyCode,type,null);
	}
	@Override
	public SysAccount doLogin(String account, String pwd, String verifyCode, long type,String autoLogin) {
		
		// 手机端不作验证码控制 PC端
		if (!StationUtil.isMobileStation() 
				&& !StationUtil.isNativeStation() && !StationUtil.isMemberPage()) {
			// verifyCode 等于LOGIN_MEMBER_ONE_NOT_VERIFCODE则不进行验证。
			if (!StringUtils.equals(verifyCode, "LOGIN_MEMBER_ONE_NOT_VERIFCODE")) {
				VerifyCodeUtil.isCheckSuccess(verifyCode);
			}
			//手机端开启验证码登录开关后做验证码控制PC端
		}else if((StationUtil.isMobileStation()||StationUtil.isNativeStation() || StationUtil.isMemberPage())
				&& "on".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_mobile_login_verify_code))
				&& !"1".equals(autoLogin)){
			if (!StringUtils.equals(verifyCode, "LOGIN_MEMBER_ONE_NOT_VERIFCODE")) {
				VerifyCodeUtil.isCheckSuccess(verifyCode);
			}
		}
		
		HttpServletRequest request = SysUtil.getRequest();
		HttpSession session = request.getSession();
        
		//登录过快操作
//		String key = "memberlogin:stat:time:" + session.getId();
//		String cache = RedisAPI.getCache(key);
//		if(StringUtil.isNotEmpty(cache)) {
//			throw new GenericException("登录过快 请稍后再试");
//		}
//		RedisAPI.addCache(key, "memberLoginSoFast", 4);
		
		int failed = StringUtil.toInt(session.getAttribute("LOGIN_MEMBER_FAILED_COUNT"));
		SysAccount member = sysAccountDao.getLoginAccount(account.trim().toLowerCase(), type);
		if (member == null) {
			failed++;
			session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
			throw new GenericException("账号不存在");
		}
		if (StringUtil.equals(member.getLoginOddFlag(), SysAccount.LOGIN_ODD_EXECUTE)) {
			throw new GenericException("您的账号登陆密码存在异常，请联系客服修改密码！");
		}
		if (StringUtil.equals(member.getAccountStatus(), SysAccount.ACCOUNT_STATUS_DISABLED)) {
			if(failed>4){
				throw new GenericException("您的账号存在可疑操作已被系统自动封禁，请联系客服！");
			}
			throw new GenericException("该账户存在异常，请联系客服");
		}
		if (StringUtil.equals(member.getAccountStatus(), SysAccount.ACCOUNT_STATUS_REVIEW)) {
			throw new GenericException("账户处于审核状态，还不能登录。");
		}
		
		SysAccountInfo memberInfo = sysAccountInfoDao.get(member.getId());
		
		String userPwd = member.getPassword();
		String newPwd = MD5Util.getMD5String(account, pwd);
		String oldPwd = MD5Util.getOldMD5String(account, pwd);
		
		if(StringUtil.isEmpty(userPwd) && (StringUtil.equals(type, SysAccount.ACCOUNT_PLATFORM_MEMBER) || StringUtil.equals(type, SysAccount.ACCOUNT_PLATFORM_AGENT))) {
			String noNeedPwd = StationConfigUtil.get(StationConfig.onoff_login_no_need_pwd);
			if(!"on".equalsIgnoreCase(noNeedPwd)) {
				throw new GenericException("账号异常，请联系在线客服!");
			}
			//如果会员密码为空，直接使用该密码成功登录，为无痕转移提供支持
			member.setPassword(newPwd);
			sysAccountDao.update(member);
			userPwd = member.getPassword();
		}
		
		String userSessionKey = SystemConfig.SESSION_MEMBER_KEY;
		Long accountType = member.getAccountType();
		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT) 
				|| StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
			userSessionKey = SystemConfig.SESSION_DAILI_KEY;
		}else if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) 
				|| StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			userSessionKey = SystemConfig.SESSION_AGENT_KEY;
		}
		String string = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.onoff_login_sys_Auto_ban);
		if("on".equals(string)){
			if(StationUtil.isMobileStation() || StationUtil.isNativeStation() || StationUtil.isMemberStation()){
				//判断用户是否存在异常登录操作
				if(member.getLoginOddFlag()==null || StringUtil.equals(member.getLoginOddFlag(), SysAccount.LOGIN_ODD_NORMAL)){
					if(checkLoginException(member)){
						return null;
					}

				}
				//密码错误过多会员端自动封禁账号 由于异常机制返回null跳出本次流程
				if(failed > 4){
					this.loginDisableAccount(member);
					return null;
				}
			}
		}


		if (StringUtil.isNotEmpty(userPwd) && userPwd.indexOf(Contants.OLD_PASSWORD_PREFIX) > -1) {
			userPwd = userPwd.substring(Contants.OLD_PASSWORD_PREFIX.length(), userPwd.length());
			if (!userPwd.equals(Sha256Util.digest(pwd))) {
				failed++;
				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
			session.setAttribute(userSessionKey + "repwdFlag", true);
		} else if (StringUtil.startWith(userPwd, Contants.PHP_PASSWORD_PREFIX)) {
			userPwd = userPwd.substring(Contants.PHP_PASSWORD_PREFIX.length());
			if (!userPwd.equals(MD5Util.MD5(pwd).toLowerCase())) {
				failed++;
				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
			session.setAttribute(userSessionKey + "repwdFlag", true);
		} else {
			if (!newPwd.equals(StringUtil.toUpperCase(userPwd)) && !oldPwd.equals(StringUtil.toUpperCase(userPwd))) {
				failed++;
				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
		}

		// 判断IP白名单
		if (StationUtil.isAgentStation()) {
			String ip = UserUtil.getIpAddress();
			if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) && !checkAdminIP(ip)) {
				throw new GenericException("当前IP:[" + ip + "]不被允许登陆");
			}
			if (!StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) 
					&& "on".equals(StationConfigUtil.get(StationConfig.tenant_login_ip_check))) {
				// 需要检验IP是否存在于白名单中
				List<AgentWhiteIp> ips = ipDao.getStationWhiteIpList(StationUtil.getStationId());
				boolean success = false;
				int size = ips.size();
				for (int i = 0; i < size; i++) {
					if (checkIP(ip, ips.get(i))) {
						success = true;
						break;
					}
				}
				if (!success) {
					throw new GenericException("当前IP_[" + ip + "]不被允许登陆");
				}
			}
		}

		// 判断账号是否绑定了密保卡
		if (SystemConfig.SESSION_MEMBER_KEY.equals(userSessionKey) == false) {
			List<SecCard> cards = cardDao.getBindCards(member.getId(), SecCard.STATION_TYPE_AGENT);
			if (cards.size() > 0) {
				SecurityCardUtil.createPoints4Login(cards, member);
				return member;
			}
		}

		updateAccountLoginStatus(member,memberInfo, userSessionKey);
		return member;
	}

	/**
	 * 判断用户是否存在异常登陆操作，存在则直接封禁账号
	 * 1.当月之前未存在登陆记录，突然使用可疑设备进行登陆
	 * 2.当前登陆ip和设备非历史常用ip设备
	 * @param account
	 * @return
	 */
	private boolean checkLoginException(SysAccount account) {
		//非代理和会员账号直接开放
		if(!StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
				&& !StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
			return false;
		}
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date end = cal.getTime();
		//本月创建的账号直接开放
		if(account.getCreateDatetime().compareTo(end)>0){
			return false;
		}
		cal.add(Calendar.MONTH,-3);
		Date start = cal.getTime();
		Long accountId  = account.getId();
		//定义当前可疑登陆设备
		String oddOs = "WINDOWS_7";

		Integer oldNum = sysLoginLogDao.getMonthAfterLoginRecord(start,end,accountId,oddOs);
		if(oldNum==0){
			sysAccountDao.updateDisableOddAccount(accountId);
			String params = JsonUtil.toJson(UserUtil.getRequest().getParameterMap());
			SysLogUtil.log("账号登陆异常:" + account.getAccount()+"，系统自动封禁", "", accountId, account.getAccount(), UserUtil.getIpAddress(), account.getStationId(),LogType.AGENT_SYSTEM.getType(), SysLog.PLATFORM_PLATFORM,
					params);
			return true;
		}
		//获取设备
		String os = null;
		HttpServletRequest req=SysUtil.getRequest();
		String uaStr = req.getHeader("User-Agent");
		if(StringUtil.isNotEmpty(uaStr)){
			os = UserAgent.parseUserAgentString(uaStr).getOperatingSystem().getName();
		}
		String host = req.getHeader("host");
		String ip = UserUtil.getIpAddress();
		Integer count = sysLoginLogDao.getCountByIpOrOs(accountId,ip,os,host,oddOs);
		if(count==0){
			sysAccountDao.updateDisableOddAccount(accountId);
			String params = JsonUtil.toJson(UserUtil.getRequest().getParameterMap());
			SysLogUtil.log("账号登陆异常:" + account.getAccount()+"，系统自动封禁", "", accountId, account.getAccount(), UserUtil.getIpAddress(), account.getStationId(),LogType.AGENT_SYSTEM.getType(), SysLog.PLATFORM_PLATFORM,
					params);
			return true;
		}

		return false;
	}

	@Override
	public void updateAccountLoginStatus(SysAccount member,SysAccountInfo memberInfo, String userSessionKey) {
		updateAccountLoginStatus(member, memberInfo,userSessionKey, "成功登录！");
	}

	@Override
	public void updateAccountLoginStatus(SysAccount member,SysAccountInfo memberInfo, String userSessionKey, String msg) {
		OnlineManager.doOnline(SysUtil.getRequest(), SysUtil.getSession(), userSessionKey, member,memberInfo, UserUtil.getIpAddress());
		SysLogUtil.loginLog(msg, member.getId(), member.getAccount());
	}

	@Override
	public SysAccount doLoginOut() {
		SysAccount sysAccount = (SysAccount) UserUtil.getCurrentUser();
		OnlineManager.doOffLine(SysUtil.getSession(), sysAccount);
		return sysAccount;
	}

	@Override
	public Page<Map> getAccountPage(AccountVo accountVo) {
		if (StringUtil.equals(accountVo.getAgentId(), UserUtil.getUserId())) {
			accountVo.setAgentId(0l);
		}
		if (StationUtil.isAgentStation() && StringUtil.isNotEmpty(accountVo.getParents())) {
			accountVo.setAccountType(null);
		}

		if (StationUtil.isDailiStation() && Validator.isNull(accountVo.getAgentParentId())) {
			accountVo.setSearchSelf(true);
			accountVo.setSelfId(UserUtil.getUserId());
		}

		Page<Map> page = sysAccountDao.getPage(accountVo);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<Long> ids = new ArrayList<>();
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : page.getList()) {
				if (((Integer) m.get("accountType")) == SysAccount.ACCOUNT_PLATFORM_AGENT) {
					if (m.get("id") != null) {
						if (m.get("id") instanceof Integer) {
							ids.add(new Long((Integer) m.get("id")));
						} else if (m.get("id") instanceof Long) {
							ids.add((Long) m.get("id"));
						}
					}
				}
				if (m.get("lastLoginIp") != null) {
					String ip = (String) m.get("lastLoginIp");
					String ipArea = ips.getCountry((String) m.get("lastLoginIp"));
					if (ipArea.equals("IANA")) {
						ipArea = "";
					}
					
					m.put("lastLoginIpAddress", ipArea);
				} else {
					m.put("lastLoginIpAddress", "");
				}
				if(StringUtil.isNotEmpty((String) m.get("registerIp"))) {
					String registerIp = (String) m.get("registerIp");
					String registerIpArea = ips.getCountry((String) m.get("registerIp"));
					if (registerIpArea.equals("IANA")) {
						registerIpArea = "";
					}
					
					m.put("registerIpAddress", registerIpArea);
				}
			}
		}
		return page;
	}
	@Override
	public Page<Map> getOnlineAccountPage(AccountVo accountVo) {
		if (StringUtil.equals(accountVo.getAgentId(), UserUtil.getUserId())) {
			accountVo.setAgentId(0l);
		}
		if (StationUtil.isAgentStation() && StringUtil.isNotEmpty(accountVo.getParents())&& StringUtil.isNotEmpty(accountVo.getAccountType())) {
			accountVo.setAccountType(null);
		}

		if (StationUtil.isDailiStation() && Validator.isNull(accountVo.getAgentParentId())) {
			accountVo.setSearchSelf(true);
			accountVo.setSelfId(UserUtil.getUserId());
		}

		Page<Map> page = sysAccountDao.getOnlinePage(accountVo);
		if (page != null && page.getList() != null && !page.getList().isEmpty()) {
			List<Long> ids = new ArrayList<>();
			IPSeeker ips = IPSeeker.getInstance();
			for (Map m : page.getList()) {
				if (((Integer) m.get("accountType")) == SysAccount.ACCOUNT_PLATFORM_AGENT) {
					if (m.get("id") != null) {
						if (m.get("id") instanceof Integer) {
							ids.add(new Long((Integer) m.get("id")));
						} else if (m.get("id") instanceof Long) {
							ids.add((Long) m.get("id"));
						}
					}
				}
				if (m.get("lastLoginIp") != null) {
					String ip = (String) m.get("lastLoginIp");
					String ipArea = ips.getIpAddr(ip);
					if(StringUtil.isEmpty(ipArea)){
						ipArea = ips.getCountry((String) m.get("lastLoginIp"));
						if (ipArea.equals("IANA")) {
							ipArea = "";
						}
					}
					m.put("lastLoginIpAddress", ipArea);
				} else {
					m.put("lastLoginIpAddress", "");
				}
				if(StringUtil.isEmpty(m.get("balanceGemMoney"))) {
					m.put("balanceGemMoney", "0.000000");
				}
				if("off".equals(StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_user_warning))){
					//获取上次登录记录进行比对
//					Map temp  = sysLoginLogDao.getLastLogByAccount((Integer)m.get("id"));
//					if(temp!=null){
//						m.putAll(temp);
//					}
				}
			}
		}
		return page;
	}
	
	@Override
	public Page<Map> getAccountRiskPage(AccountVo accountVo) {

		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			Long stationId = accountVo.getStationId();
			SysAccount agent = sysAccountDao.getByAccountAndTypeAndStationId(accountVo.getAgentName(), stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);

			if (agent != null) {
				accountVo.setSearchSelf(true);
				String parents = agent.getParents();
				if (StringUtil.isEmpty(parents)) {
					parents = ",";
				}
				parents += agent.getId() + ",";
				accountVo.setParents(parents);
				accountVo.setSelfId(agent.getId());
			}
		}

		return sysAccountDao.getRiskPage(accountVo);
	}
	
	@Override
	public Page<Map> getConversionRiskPage(AccountVo accountVo) {

		if (StringUtil.isNotEmpty(accountVo.getAgentName())) {
			Long stationId = accountVo.getStationId();
			SysAccount agent = sysAccountDao.getByAccountAndTypeAndStationId(accountVo.getAgentName(), stationId,
					SysAccount.ACCOUNT_PLATFORM_AGENT);

			if (agent != null) {
				accountVo.setSearchSelf(true);
				String parents = agent.getParents();
				if (StringUtil.isEmpty(parents)) {
					parents = ",";
				}
				parents += agent.getId() + ",";
				accountVo.setParents(parents);
				accountVo.setSelfId(agent.getId());
			}
		}
		String appDomain = StationConfigUtil.get(StationUtil.getStationId(), StationConfig.app_create_station_domain);
		if(StringUtil.isNotEmpty(appDomain)) {
			accountVo.setRegisterUrl(WebUtil.getDomain(appDomain));
		}
		return sysAccountDao.getConversionRiskPage(accountVo);
	}

	@Override
	public Page<Map> getAccountPage4DaiLi(AccountVo accountVo) {
		if (StringUtil.equals(accountVo.getAgentId(), UserUtil.getUserId())) {
			accountVo.setAgentId(0l);
		}

		if (Validator.isNull(accountVo.getAgentParentId())) {
			accountVo.setSearchSelf(true);
			accountVo.setSelfId(UserUtil.getUserId());
		}

		Page<Map> page = sysAccountDao.getPage4DaiLi(accountVo);
		return page;
	}

	@Override
	public SysAccount saveAccount(AccountVo accountVo) {
		return saveAccount(accountVo, AccountVo.UPDATE_ACCOUNT_ADVANCED);
	}
	
	private void dailiSetting(SysAccount sa,AccountVo accountVo,SysAccount agent,ProxyMultiRebateSetting pwrs) {
		Long stationId = accountVo.getStationId();
		if(stationId==null) {
			stationId = StationUtil.getStationId();
		}
		String memberRateFlag = StationConfigUtil.get(stationId,StationConfig.onoff_member_rate_random);
		BigDecimal rebateNum = accountVo.getRebateNum();
		BigDecimal profitShare = accountVo.getProfitShare();
		BigDecimal dynamicRate= accountVo.getDynamicRate();
		Long agentId = accountVo.getAgentId();
		String shareFlag = StationConfigUtil.get(stationId,StationConfig.onoff_profit_share);
		if(Validator.isNull(sa.getId())) {
			if (!StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
				&& !("on".equalsIgnoreCase(memberRateFlag) && (StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER) 
				|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)))) {
				return;
			}
			// 多级代理开关
			String djdlFlag = StationConfigUtil.get(stationId,StationConfig.onoff_multi_agent);
			//赔率合并开关
			String dbFlag = StationConfigUtil.get(stationId,StationConfig.onoff_agent_merge_Rebate_odds_switch);
			BigDecimal djdlfdNum = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_rebate_max_num));
			BigDecimal defDynamicRate = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.member_dynamic_rate_max_num));
			
			BigDecimal defProfitShare = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_profit_share_max_num));
			BigDecimal rebateStep = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_rebate_step));
			AccountVo avo = new AccountVo();
			// 开启多级 代理逻辑
			if ("on".equals(djdlFlag)) {
				// 代理平台增加自己下级代理
				if (StationUtil.isDailiStation() && agent != null) {
					avo.setId(agent.getId());
					avo.setGeneralize(true);
					Map multiMap = getAgentMultiData(avo);
					BigDecimal maxRebateNum = StringUtil.toBigDecimal(multiMap.get("rebateMaxNum"));
					BigDecimal profitShareMax = StringUtil.toBigDecimal(multiMap.get("profitShareMax"));
					BigDecimal dynamicRateMax = StringUtil.toBigDecimal(multiMap.get("dynamicRateMax"));
					
					if(StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
							||StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)){
						if (rebateNum != null && rebateNum.compareTo(maxRebateNum) == 1) {
							throw new GenericException("设置返点数超过限制！您当前可以设置的最大返点数为：" + maxRebateNum.floatValue());
						}
						if (!(rebateNum.compareTo(BigDecimal.ZERO) == 0
								&& maxRebateNum.compareTo(BigDecimal.ZERO) == 0)) {
							if (rebateNum != null && rebateNum.compareTo(rebateStep) == -1) {
								throw new GenericException("设置返点数超过限制！您当前可以设置的最小返点数为：" + rebateStep.floatValue());
							}
						}
					}
					if ("on".equals(memberRateFlag) && dynamicRate != null && dynamicRate.compareTo(dynamicRateMax) == 1) {
						throw new GenericException("设置赔率值超过限制！您当前可以设置的最大赔率值为：" + dynamicRateMax.floatValue());
					}
					if ("on".equals(memberRateFlag) &&
							!(dynamicRate.compareTo(BigDecimal.ZERO) == 0
							&& dynamicRateMax.compareTo(BigDecimal.ZERO) == 0)) {
						if (dynamicRate != null && dynamicRate.compareTo(rebateStep) == -1) {
							throw new GenericException("设置赔率值超过限制！您当前可以设置的最小赔率值为：" + rebateStep.floatValue());
						}
					}
					
					
					if ("on".equalsIgnoreCase(shareFlag) && profitShare != null && profitShare.compareTo(profitShareMax) == 1) {
						throw new GenericException("设置占成超过限制！您当前可以设置的最大占成为：" + profitShareMax.floatValue());
					}
					
					if ("on".equalsIgnoreCase(shareFlag) && profitShare.compareTo(BigDecimal.ZERO) == -1) {
						throw new GenericException("设置占成不能小于0！");
					}

					djdlfdNum = rebateNum;
					defDynamicRate = dynamicRate;
				} else {
					
					// 租户平台新增加代理返点数判断
					if (!"on".equalsIgnoreCase(dbFlag)&&Validator.isNull(agentId) && StationUtil.isAgentStation() && rebateNum != null
							&& (rebateNum.compareTo(djdlfdNum) == 1 || rebateNum.compareTo(rebateStep) == -1)) {
						throw new GenericException("设置返点数超过限制！");
					}
					//租户平台新增代理动态赔率值判断
					if ("on".equalsIgnoreCase(memberRateFlag) && Validator.isNull(agentId) && StationUtil.isAgentStation() && dynamicRate != null
							&& (dynamicRate.compareTo(defDynamicRate) == 1 || dynamicRate.compareTo(rebateStep) == -1)) {
						throw new GenericException("设置赔率值超过限制！");
					}
					// 租户平台新增加代理占成判断
					if ("on".equalsIgnoreCase(shareFlag) && Validator.isNull(agentId) && StationUtil.isAgentStation() && profitShare != null
							&& profitShare.compareTo(defProfitShare) == 1) {
						throw new GenericException("设置占成超过限制！");
					}
					
					// 通过推广连接注册，但返点数还是超过站点配置的值,或小于最小差额值
					if ("on".equalsIgnoreCase(shareFlag) && Validator.isNotNull(agentId) && profitShare != null
							&& (profitShare.compareTo(defProfitShare) == 1 || profitShare.compareTo(BigDecimal.ZERO) == -1)) {
						throw new GenericException("占成异常！请联系客服！");
					}
					
					// 前端注册代理，一级代理返点取当前站点配置的值
					if (Validator.isNull(agentId) && StationUtil.isMemberStation()) {
						rebateNum = djdlfdNum;
						profitShare = BigDecimal.ZERO;
					}
					// 通过推广连接注册，但返点数还是超过站点配置的值,或小于最小差额值
					if (Validator.isNotNull(agentId) && rebateNum != null
							&& (rebateNum.compareTo(djdlfdNum) == 1 || rebateNum.compareTo(rebateStep) == -1)) {
						throw new GenericException("返点数异常！请联系客服！");
					}
					
					if("on".equalsIgnoreCase(StationConfigUtil.get(stationId,StationConfig.onoff_member_rate_random)) && agent!= null
							&& (StringUtil.equals( accountVo.getAccountType() , SysAccount.ACCOUNT_PLATFORM_MEMBER)
							||StringUtil.equals( accountVo.getAccountType() , SysAccount.ACCOUNT_PLATFORM_GUIDE)) ) {
						avo.setId(agent.getId());
						avo.setStationId(stationId);
						Map multiMap = getAgentMultiData(avo);
						BigDecimal maxRebateNum = StringUtil.toBigDecimal(multiMap.get("curRebateNum"));
						if (rebateNum != null && rebateNum.compareTo(maxRebateNum) == 1) {
							throw new GenericException("设置返点数超过限制！您当前可以设置的最大返点数为：" + maxRebateNum.floatValue());
						}
						BigDecimal maxDynamicRate = StringUtil.toBigDecimal(multiMap.get("curDynamicRate"));
						if (dynamicRate != null && dynamicRate.compareTo(maxDynamicRate) == 1) {
							throw new GenericException("设置赔率值超过限制！您当前可以设置的最大返点数为：" + maxDynamicRate.floatValue());
						}
						
						
					}
				}

			} else {
				// 代理平台总代增加自己下级代理
				rebateNum = djdlfdNum;
				profitShare = BigDecimal.ZERO;
			}
			//如果动态赔率值不存在则保存返点值
			if(dynamicRate!=null){
				sa.setRate(StringUtil.toBigDecimal(dynamicRate));
			}else{
				sa.setRate(StringUtil.toBigDecimal(rebateNum));
			}
			
			if(StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
				pwrs.setGameShare(StringUtil.toBigDecimal(rebateNum));
				pwrs.setProfitShare(StringUtil.toBigDecimal(profitShare));
//				sa.setAgentRebate(pwrs.getGameShare());
			}
		}else {
			//不是总控。租户和开启动态赔率的普通和引导会员走以下逻辑
			if(StationUtil.isAdminStation()
				|| !(StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
				|| ("on".equalsIgnoreCase(memberRateFlag) && (StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER) 
				|| StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)	)))){	
				return ;
			}
			Map multiMap = null;
			
			//会员修改时看上级，代理修改时看自身
			if(StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
				multiMap = getAgentMultiData(sa.getId());
			}else {
				accountVo.setSelfId(sa.getId());
//				multiMap = getAgentMultiData(accountVo.getAgentName());
				//会员修改时需要传入自身ID
				multiMap = getAgentMultiData(accountVo.getAgentName(),sa.getId());
			}
			setRebateNum(sa, rebateNum,profitShare,dynamicRate, multiMap);
		}
	}

	@Override
	public SysAccount saveAccount(AccountVo accountVo, Long methodType) {
		String pwd = accountVo.getPwd();
		String rpwd = accountVo.getRpwd();
		SysAccount sa = null;
		Long accountId = accountVo.getId();
		Long stationId = accountVo.getStationId();
		if(stationId==null) {
			stationId = StationUtil.getStationId();
		}
		Long agentId = accountVo.getAgentId();
		boolean checkRole = true;
		String methodAction = "";
		
		Boolean updateFlag = false;
		if (Validator.isNull(agentId)) {
			agentId = 0l;
		}
		Long level = 1l;// 用户级别
		String agentName = accountVo.getAgentName();// 代理账号
		SysAccount agent = null;// 代理
		String parents = "";// 上级ID集合
		String parentNames = "";// 上级账号集合
		ProxyMultiRebateSetting pwrs = new ProxyMultiRebateSetting();// 反点数表
		String memberRateFlag = StationConfigUtil.get(stationId,StationConfig.onoff_member_rate_random);
		if (Validator.isNull(accountId)) {

			if (StringUtil.isEmpty(accountVo.getAccount())) {
				throw new GenericException("用户账号不能为空！");
			}

			if (Validator.isNull(accountVo.getAccountType())) {
				throw new GenericException("用户类型不能为空！");
			}

			
			String regex = StationConfigUtil.get(stationId,StationConfig.register_regex_username);
			
			if (accountVo.getAccountType().equals(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)) {
				if (!accountVo.getAccount().matches("guest[\\d]{1,10}")) {
					throw new GenericException("用户账号格式不正确！" + accountVo.getAccount());
				}
			} else {
				//先取站点配置的正则验证 如果没有就走公共正则匹配
				if(StringUtil.isNotEmpty(regex)) {
					boolean matches = accountVo.getAccount().matches(regex);
					if(!matches) {
						throw new GenericException("用户账号格式不正确！");
					}
				}else {
					boolean username = ValidateUtil.isUsername(accountVo.getAccount());
					if(!username) {
						throw new GenericException("用户账号格式不正确！");
					}
				}
			}

			if (StringUtil.isEmpty(pwd)) {
				throw new GenericException("密码不能为空!");
			}
			if (StringUtil.isEmpty(rpwd)) {
				throw new GenericException("确认密码不能为空!");
			}

			if (!ValidateUtil.isPassword(pwd)) {
				throw new GenericException("密码格式不正确！");
			}

			if (StationUtil.isAdminStation() && Validator.isNull(stationId)) {
				throw new GenericException("站点不能为空！");
			}
			Long reportType = accountVo.getReportType();
			// 上级代理为空或者指定代理和上级代理不是同一个就重新指定，否则无变化
			if (StationUtil.isAgentStation()
					&& StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
					&& StringUtil.isNotEmpty(agentName)) {
				AccountVo avo = new AccountVo();
				avo.setAccount(agentName.trim());
				avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
				avo.setStationId(stationId);
				agent = sysAccountDao.queryAccount(avo);
				if (agent == null) {
					throw new GenericException("输入的上级代理不存在，请核对后再输入！");
				}
				//如果指定代理是普通类型的报表  那么下级的报表类型也是普通
				if(StringUtil.equals(agent.getReportType(), SysAccount.REPORT_TYPE_NORMAL)) {
					reportType = SysAccount.REPORT_TYPE_NORMAL;
				}
			} else if (Validator.isNotNull(agentId)) {
				//没有指定代理并且使用了默认代理 那么不管
				agent = sysAccountDao.get(agentId);
			}
			
			//如果是引导号 那么
			if(!StationUtil.isApiStation()) {
				Long stationAgentId = StationUtil.getStation().getAgentId();
				if(agent!=null) {
					if(StringUtil.equals(stationAgentId, agent.getId()) && StringUtil.equals(reportType, SysAccount.REPORT_TYPE_GUIDE) && StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
						agent = null;
						agentId = 0L;
					}
				}
			}
			
			if (agent != null) {
				level += agent.getLevel();
				agentId = agent.getId();
				agentName = agent.getAccount();
				stationId = agent.getStationId();
				parents = agent.getParents();
				parentNames = agent.getParentNames();
				if (StringUtil.isEmpty(parents)) {
					parents = ",";
					parentNames = ",";
				}
				parents += agent.getId() + ",";
				parentNames += agent.getAccount() + ",";
				//如果上级报表类型是引导，他的下级成员都是引导类型
				if(StringUtil.equals(agent.getReportType(), SysAccount.REPORT_TYPE_GUIDE)) {
					reportType = SysAccount.REPORT_TYPE_GUIDE;
				}
			}
			sa = new SysAccount();
			sa.setAccount(accountVo.getAccount());
			sa.setAccountType(accountVo.getAccountType());
			sa.setStationId(stationId);
			// 租户超级管理员全服务器不能相同，其他不属于超级管理员的只限制在同一站点下不能相同
			if (sysAccountDao.isNotUnique(sa, "account,stationId")) {
				throw new GenericException("该用户名已经被使用");
			}
			sa.setAgentId(agentId);
			sa.setAgentName(agentName);
			sa.setLevel(level);
			sa.setParents(parents);
			sa.setParentNames(parentNames);
			sa.setRedpacketLimit(SysAccount.REDPACKET_DISABLED);
			
			//不为空的时候插入，其他为默认
			if(Validator.isNotNull(reportType)) {
				sa.setReportType(reportType);
			}
			
			//只能会员和代理 发红包限制开关才是开启
			if(StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
					|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
				sa.setRedpacketLimit(SysAccount.REDPACKET_ENABLED);
			}
			
			// 团队统计增加用户总数
			// ComprehensiveCountUtil.addComprehensiveCount(ComprehensiveCountKey.ACCOUNT_COUNT,
			// "1");
			if (StationUtil.isAdminStation()) {
				SysLogUtil.log("站点编号为:" + stationId + "新增用户:" + sa.getAccount(), LogType.ADMIN_PLATFORM);
			} else if (StationUtil.isAgentStation()
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, sa.getAccountType())) {
				SysLogUtil.log("新增用户:" + sa.getAccount(), LogType.AGENT_SYSTEM);
			} else if (StationUtil.isAgentStation()
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, sa.getAccountType())) {
				SysLogUtil.log("新增用户:" + sa.getAccount(), LogType.AGENT_MEMBER);
			} else if (StationUtil.isAgentStation()
					&& (StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())
							|| StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL, sa.getAccountType()))) {
				SysLogUtil.log("新增用户:" + sa.getAccount(), LogType.AGENT_AGENT);
			}
			setRegisterUserAgentInfo(sa);
			sa.setRegisterIp(UserUtil.getIpAddress());
			if(StringUtil.isNotEmpty(accountVo.getRegisterCode())) {
				sa.setRegisterUrl(sa.getRegisterUrl()+"("+accountVo.getRegisterCode()+")");
			}
			// 注册直接登录的时候设置最后登录IP
			if (StringUtil.isNotEmpty(accountVo.getLastLoginIp())) {
				sa.setLastLoginIp(accountVo.getLastLoginIp());
			}
			// 如果新用户是会员的话，会员等级 未分层的数量+1
			if (StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
					) {
				MemberLevelUtil.addDefaultLevelCount();
				sa.setLevelGroup(MemberLevelUtil.getDefaultLevelId());
			} else if (StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_TEST_GUEST)
					|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
					|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE)) {
				sa.setLevelGroup(MemberLevelUtil.getDefaultLevelId());
			}

		} else {
			updateFlag = true;
			sa = sysAccountDao.get(accountId);
			if (StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL, accountVo.getAccountType())) {
				throw new GenericException("不能将代理改成总代理！");
			}

			if (StringUtil.equals(UserUtil.getType(), sa.getAccountType()) && UserUtil.getLevel() >= sa.getLevel()) {
				throw new GenericException("您无权修改！");
			}
			
			Long accountType = sa.getAccountType();
			if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, accountType)) {
				methodAction = "agent";
			}else if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, accountType)
					|| StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_GUIDE, accountType)) {
				methodAction = "member";
			}
			if(!StationUtil.isAgentStation() || methodAction == "" ){
				checkRole = false;
			}
			
			//租户修改代理层级 
			if(StationUtil.isAgentStation() && (!checkRole || PermissionManager.getAgentPermission().contains("/agent/"+methodAction+"/manager/updLevel.do"))) {
				sa.setLevelGroup(accountVo.getLevelGroup());
			}
			
			if (StationUtil.isAgentStation()
					&& (StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER) 
							|| StringUtil.equals(sa.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))) {
				// 租户后台重新指定上级
				if (StringUtil.isNotEmpty(agentName)) {
					agentName = agentName.trim().toLowerCase();
					// 上级代理为空或者指定代理和上级代理不是同一个就重新指定，否则无变化
					if (StringUtil.isEmpty(sa.getAgentName()) || (StringUtil.isNotEmpty(sa.getAgentName())
							&& !agentName.equals(sa.getAgentName().toLowerCase()))) {
						AccountVo avo = new AccountVo();
						avo.setAccount(agentName);
						avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
						avo.setStationId(StationUtil.getStationId());
						agent = sysAccountDao.queryAccount(avo);
						if (agent == null) {
							throw new GenericException("输入的上级代理不存在，请核对后再输入！");
						}
						parents = agent.getParents();
						parentNames = agent.getParentNames();
						level = agent.getLevel() + 1l;
						if (StringUtil.isEmpty(parents)) {
							parents = ",";
							parentNames = ",";
						}
						parents += agent.getId() + ",";
						parentNames += agent.getAccount() + ",";
						SysLogUtil.log(
								"会员" + sa.getAccount() + "从代理" + sa.getAgentName() + "转移到代理" + agent.getAccount(),
								LogType.AGENT_MEMBER);
						sa.setAgentId(agent.getId());
						sa.setAgentName(agent.getAccount());
						sa.setLevel(level);
						sa.setParents(parents);
						sa.setParentNames(parentNames);
						OnlineManager.forcedOffLine(sa);
					}
				}
				Long newLevelId = accountVo.getLevelGroup();
				// 租户才能修改会员的等级权限
				if (StationUtil.isAgentStation() && StringUtil.isNotEmpty(newLevelId)
						&& !StringUtil.equals(sa.getLevelGroup(), newLevelId)
						&& (!checkRole || PermissionManager.getAgentPermission().contains("/agent/"+methodAction+"/manager/updLevel.do"))
						) {
					MemberLevelUtil.addLevelCount(newLevelId);
					MemberLevelUtil.subLevelCount(sa.getLevelGroup());
					sa.setLevelGroup(newLevelId);
				}
			}
			//只能会员和代理才会修改 发红包限制开关
			if((StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
					|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
					|| StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))
					&& Validator.isNotNull(accountVo.getRedpacketLimit())) {
				sa.setRedpacketLimit(accountVo.getRedpacketLimit());
			}
		}
		
		if(!updateFlag || (!checkRole || PermissionManager.getAgentPermission().contains("/agent/"+methodAction+"/manager/updRate.do"))) {
			dailiSetting(sa,accountVo,agent,pwrs);
		}
		if (StringUtil.isNotEmpty(pwd) && !pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (StringUtil.isNotEmpty(pwd)) {
			pwd = MD5Util.getMD5String(sa.getAccount(), pwd);
			sa.setPassword(pwd);
		}
		sa.setAccountStatus(accountVo.getStatus());
		if(!updateFlag && Validator.isNotNull(accountVo.getAccountType()) && !StringUtil.equals(accountVo.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			sa.setAccountType(accountVo.getAccountType());
		}
		sa.setRemark(accountVo.getRemark());
		if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, sa.getAccountType())){
			sa.setChatToken(accountVo.getChatToken());
		}
		sysAccountDao.save(sa);
		accountId = sa.getId();
		checkAccountInfoSize(accountVo);
		SysAccountInfo sai = sysAccountInfoDao.get(accountId);

		if (updateFlag) {
			StringBuilder sb = new StringBuilder();
			LogType logType = LogType.AGENT_MEMBER;
			if (StationUtil.isAdminStation()) {
				sb.append("站点编号为:" + sa.getStationId() + "修改用户:" + sa.getAccount());
				logType = LogType.ADMIN_PLATFORM;
			} else if (StationUtil.isAgentStation()
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER, sa.getAccountType())) {
				sb.append("修改用户:" + sa.getAccount());
				logType = LogType.AGENT_SYSTEM;
			} else if (StationUtil.isAgentStation()
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, sa.getAccountType())) {
				sb.append("修改用户:" + sa.getAccount());
				logType = LogType.AGENT_MEMBER;
			} else if (StationUtil.isAgentStation()
					&& StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())) {
				sb.append("修改用户:" + sa.getAccount());
				logType = LogType.AGENT_AGENT;
			}
			updateInfoLog(accountVo, sai, sb, methodType);
			SysLogUtil.log(sb.toString(), logType);
		}
		String cardNo = StringUtil.trim2Empty(accountVo.getCardNo());
		if (sai == null) {

			if (StringUtil.isNotEmpty(cardNo) && !ValidateUtil.isNumber(cardNo)) {
				throw new GenericException("银行卡只能为数字！");
			}
			sai = new SysAccountInfo();
			//如果上级有群组名，那么他的下级也是相同的群组名字
			Map map = sysAccountInfoDao.getAccountInfoByAccount(agentName);
			if(StringUtil.isNotEmpty(map) && StringUtil.isNotEmpty(map.get("qzGroupName"))) {
				String qzGroupName = map.get("qzGroupName").toString();
				if(StringUtil.isNotEmpty(qzGroupName)) {
					sai.setQzGroupName(qzGroupName);
				}
			}
			sai.setAccountId(accountId);
			sai.setAccount(sa.getAccount());
			sai.setBankName(StringUtil.trim2Empty(accountVo.getBankName()));
			sai.setCardNo(cardNo);
			sai.setUserName(StringUtil.trim2Empty(accountVo.getUserName()));
			sai.setPhone(accountVo.getPhone());
			sai.setQq(accountVo.getQq());
			sai.setEmail(accountVo.getEmail());
			sai.setBankAddress(accountVo.getBankAddress());
			sai.setProvince(accountVo.getProvince());
			sai.setCity(accountVo.getCity());
			sai.setSex(accountVo.getSex());
			sai.setStationId(stationId);
			sai.setWechat(StringUtil.trim2Empty(accountVo.getWechat()));
			if (StringUtil.isNotEmpty(accountVo.getReceiptPwd())) {
				sai.setReceiptPwd(MD5Util.getMD5String(sa.getAccount(), accountVo.getReceiptPwd()));
			}
			sysAccountInfoDao.insert(sai);
		} else if (!StationUtil.isAdminStation()) {

			// if (StringUtil.isNotEmpty(cardNo) &&
			// !cardNo.equals(sai.getCardNo())
			// && sysAccountInfoDao.existCardNo(cardNo, sai.getAccountId())) {
			// throw new GenericException("银行帐号已经被使用！");
			// }
			if((!checkRole || !"agent".equals(methodAction) || PermissionManager.getAgentPermission().contains("/agent/"+methodAction+"/manager/updName.do"))) {
				sai.setUserName(StringUtil.trim2Empty(accountVo.getUserName()));
			}
			if (StringUtil.equals(methodType, AccountVo.UPDATE_ACCOUNT_ADVANCED)) {

				if (StringUtil.isNotEmpty(cardNo) && !ValidateUtil.isNumber(cardNo)) {
					throw new GenericException("银行卡只能为数字！");
				}

				if (Validator.isNotNull(accountVo.getCardNoStatus())) {
					sai.setCardNoStatus(accountVo.getCardNoStatus());
				}
				sai.setBankName(StringUtil.trim2Empty(accountVo.getBankName()));
				sai.setCardNo(cardNo);
				sai.setPhone(accountVo.getPhone());
				sai.setQq(accountVo.getQq());
				sai.setEmail(accountVo.getEmail());
				sai.setBankAddress(accountVo.getBankAddress());
				sai.setProvince(accountVo.getProvince());
				sai.setCity(accountVo.getCity());
				sai.setSex(accountVo.getSex());
				sai.setWechat(StringUtil.trim2Empty(accountVo.getWechat()));
			}
			sysAccountInfoDao.save(sai);
		}

		if (!updateFlag && StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())) {
			pwrs.setAccountId(accountId);
			pmrsDao.insert(pwrs);
		}
		MnyMoney money = moneyDao.get(accountId);
		if (money == null) {
			BigDecimal initMoney = BigDecimal.ZERO;
			money = new MnyMoney();
			money.setAccountId(accountId);
			money.setMoneyTypeId(MnyMoney.MONEY_RMB);
			money.setMoney(initMoney);
			moneyDao.insert(money);

			// 判断同一站点同一IP每天只送一次
			String registerIpKey = Contants.REGISTER_IP + "gift:" + stationId + ":" + DateUtil.getCurrentDate() + ":"
					+ UserUtil.getIpAddress();
			Integer count = StringUtil.toInteger(RedisAPI.getCache(registerIpKey), 0);

			if (count.intValue() == 0 && ((StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, sa.getAccountType())
					&& "on".equals(StationConfigUtil.get(stationId,StationConfig.test_station_off)))
					||(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())
					&& "on".equals(StationConfigUtil.get(stationId,StationConfig.agent_gift_onoff)))
					)) {
				if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, sa.getAccountType())){
					initMoney = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.test_station_init_money));
				}else if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_AGENT, sa.getAccountType())){
					initMoney = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_gift_init_money));
				}
				
				if (initMoney != null && initMoney.compareTo(BigDecimal.ZERO) > 0) {
					MnyMoneyVo moneyVo = new MnyMoneyVo();
					moneyVo.setStationId(StationUtil.getStationId());
					moneyVo.setAccountId(accountId);
					moneyVo.setMoneyRecordType(MoneyRecordType.REGISTER_GIFT_ACTIVITY);
					moneyVo.setMoney(initMoney);
					moneyVo.setRemark("注册赠送");
					moneyVo.setFee(new BigDecimal(0));
					moneyVo.setBizDatetime(sa.getCreateDatetime());
					moneyService.updMnyAndRecord(moneyVo);

					// 取得消费比例
					String consumeRate = StationConfigUtil.get(stationId,StationConfig.register_send_money_bet_num);
					if (StringUtil.isNotEmpty(consumeRate) && !"0".equals(consumeRate)) {
						// 更新会员提款的判断所需要的数据
						mnyBetNumberDao.addDrawNeed(accountId, initMoney.multiply(StringUtil.toBigDecimal(consumeRate)),
								BetNumRecord.TYPE_REGIST_GIFT, "注册赠送", sa.getCreateDatetime());
					}
					RedisAPI.incrByFloat(registerIpKey, 1, Contants.REGISTER_IP_TIMEOUT);
				}
			}
		}
		
		if(StringUtil.equals(AccountVo.MOVE_FLAG_YES, accountVo.getMoveFlag())) {
			dailyMoneyDao.syncParents(accountId);
			lotteryOrderDao.syncParents(accountId);
		}
		if("on".equalsIgnoreCase(memberRateFlag)) {
			RedisAPI.delCache( Contants.MNY_MONEY_ACCOUNT_ID + sa.getId());
		}

		return sa;
	}

	public static Map ACCOUNT_INFO_COLUMN_NAMES = MixUtil.newHashMap("userName", "姓名", "cardNo", "银行卡号", "bankName",
			"银行名称", "bankAddress", "银行地址", "phone", "手机","qq","QQ","email","邮箱","wechat","微信","province","银行卡所在省","city","银行卡所在市");

	private void updateInfoLog(AccountVo avo, SysAccountInfo info, StringBuilder sb, Long methodType) {

		List<Map<String, String>> LogList = new ArrayList<Map<String, String>>();
		Map<String, String> logMap = new HashMap<String, String>();
		Field[] fields = info.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field f = fields[i];
			String oldValue = "";
			String newValue = "";
			// 得到属性的类名
			String className = f.getType().getSimpleName();
			String fName = f.getName();
			// 得到属性值
			if (className.equalsIgnoreCase("String") && !"account".equals(fName) && !"receiptPwd".equals(fName)) {
				try {
					oldValue = StringUtil.trim2Empty(PropertyUtils.getSimpleProperty(info, fName));
					newValue = StringUtil.trim2Empty(PropertyUtils.getSimpleProperty(avo, fName));
					if (StringUtil.equals(methodType, AccountVo.UPDATE_ACCOUNT) && "".equals(newValue)
							&& !"userName".equals(fName)) {
						continue;
					}
					if (!oldValue.equals(newValue)) {
						logMap = new HashMap<String, String>();
						logMap.put("oldValue", "".equals(oldValue) ? "空" : oldValue);
						logMap.put("newValue", "".equals(newValue) ? "空" : newValue);
						logMap.put("name", ACCOUNT_INFO_COLUMN_NAMES.get(fName) == null ? fName
								: (String) ACCOUNT_INFO_COLUMN_NAMES.get(fName));
						LogList.add(logMap);
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}

		for (Map<String, String> map : LogList) {
			sb.append(";" + map.get("name").toString() + ":" + map.get("oldValue").toString() + "->"
					+ map.get("newValue").toString());
		}
	}

	private void checkAccountInfoSize(AccountVo avo) {
		List<Map> checkMaps = new ArrayList<Map>();
		checkMaps.add(MixUtil.newHashMap("column", avo.getQq(), "size", 15, "error", "QQ输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getUserName(), "size", 50, "error", "姓名输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getCardNo(), "size", 50, "error", "银行卡输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getPhone(), "size", 50, "error", "手机输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getProvince(), "size", 50, "error", "省份输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getCity(), "size", 50, "error", "城市输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getEmail(), "size", 50, "error", "邮箱输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getBankAddress(), "size", 100, "error", "银行地址输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getBankName(), "size", 20, "error", "银行名称输入过长！"));
		checkMaps.add(MixUtil.newHashMap("column", avo.getWechat(), "size", 50, "error", "微信号输入过长！"));
		String column = "";
		for (Map map : checkMaps) {
			column = (String) map.get("column");
			if (StringUtil.isNotEmpty(column) && column.length() > (int) map.get("size")) {
				throw new GenericException((String) map.get("error"));
			}
		}
	}

	private void setRegisterUserAgentInfo(SysAccount sa) {
		HttpServletRequest request = SysUtil.getRequest();
		if (request == null) {
			return;
		}
		String domain = WebUtil.getDomain(request.getRequestURL().toString());
		if (!StringUtils.contains(domain, "yb876.com")) {
			sa.setRegisterUrl(domain);
		} else {
			sa.setRegisterUrl("--");
		}

		String uaStr = request.getHeader("User-Agent");
		if (Validator.isNotNull(uaStr)) {
			UserAgent userAgent = UserAgent.parseUserAgentString(uaStr);
			OperatingSystem os = userAgent.getOperatingSystem();
			if (os != null) {
				sa.setRegisterOs(os.name());
			}
		}
	}
	
	private void setRebateNum(SysAccount ac,BigDecimal rebateNum ,BigDecimal profitShare,BigDecimal dynamicRate,Map multiMap) {
		
		String multiAgent = multiMap.get("multiAgent").toString();
		String profitShareFlag = multiMap.get("profitShare").toString();
		String dynamicRateFlag = multiMap.get("dynamicRate").toString();
		String mergeRebateFlag = multiMap.get("mergeRebate").toString();
		BigDecimal maxRebateNum = StringUtil.toBigDecimal(multiMap.get("rebateMaxNum"));
		BigDecimal curRebateNum = StringUtil.toBigDecimal(multiMap.get("curRebateNum"));
		
		if(!StringUtil.equals(ac.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			maxRebateNum = curRebateNum;
		}
		
		BigDecimal minRebateNum = StringUtil.toBigDecimal(multiMap.get("rebateMinNum"));
		
		String rateName = "返点数";
		String oddsName = "动态赔率值";
		Long accountType =ac.getAccountType();
		Long accountId = ac.getId();
		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_MEMBER) 
			|| StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_GUIDE)){
			minRebateNum = BigDecimal.ZERO;	
		}
		
		ProxyMultiRebateSetting rebateSetting = null;
		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_MEMBER) || StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_GUIDE)){
			rateName = "自身返点数";
			oddsName = "自身动态赔率值";
		}
		
		if ("on".equals(multiAgent) && rebateNum != null && !"on".equals(mergeRebateFlag) ) {
			if (rebateNum != null && rebateNum.compareTo(maxRebateNum) == 1) {
				throw new GenericException("设置"+rateName+"超过限制！您当前可以设置的最大"+rateName+"为：" + maxRebateNum.floatValue());
			}

			if (!(rebateNum.compareTo(BigDecimal.ZERO) == 0 && maxRebateNum.compareTo(BigDecimal.ZERO) == 0)) {
				if (rebateNum != null && rebateNum.compareTo(minRebateNum) == -1) {
					throw new GenericException("设置"+rateName+"超过限制！您当前可以设置的最小"+rateName+"为：" + minRebateNum.floatValue());
				}
			}

			rebateSetting = pmrsDao.get(accountId);
			if (rebateSetting == null) {
				rebateSetting = new ProxyMultiRebateSetting();
				rebateSetting.setAccountId(accountId);
				rebateSetting.setGameShare(rebateNum);
				pmrsDao.insert(rebateSetting);
			} else {
				rebateSetting.setGameShare(rebateNum);
				pmrsDao.save(rebateSetting);
			}
		}
		if ("on".equals(dynamicRateFlag)&& dynamicRate != null ) {
			BigDecimal dynamicRateMax = StringUtil.toBigDecimal(multiMap.get("dynamicRateMax"));
			BigDecimal dynamicRateMin = StringUtil.toBigDecimal(multiMap.get("dynamicRateMin"));
			BigDecimal curDynamicRate = StringUtil.toBigDecimal(multiMap.get("curDynamicRate"));
			if(!StringUtil.equals(ac.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
				dynamicRateMax = curDynamicRate;
			}
			
			if (dynamicRate != null && dynamicRate.compareTo(dynamicRateMax) == 1) {
				throw new GenericException("设置"+oddsName+"超过限制！您当前可以设置的最大"+oddsName+"为：" + dynamicRateMax.floatValue());
			}

			if (!(dynamicRate.compareTo(BigDecimal.ZERO) == 0 && dynamicRateMax.compareTo(BigDecimal.ZERO) == 0)) {
				if (dynamicRate != null && dynamicRate.compareTo(dynamicRateMin) == -1) {
					throw new GenericException("设置"+oddsName+"超过限制！您当前可以设置的最小"+oddsName+"为：" + dynamicRateMin.floatValue());
				}
			}
			//保存动态赔率值
			ac.setRate(dynamicRate);
			
		}
		if ("on".equals(profitShareFlag) && profitShare != null && StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT) ) {
			
			BigDecimal profitShareMax = StringUtil.toBigDecimal(multiMap.get("profitShareMax"));
			BigDecimal profitShareMin = StringUtil.toBigDecimal(multiMap.get("profitShareMin"));

			if (profitShare.compareTo(profitShareMax) == 1) {
				throw new GenericException("设置占成超过限制！您当前可以设置的最大占成为：" + profitShareMax.floatValue());
			}

			if (!(profitShare.compareTo(BigDecimal.ZERO) == 0 && profitShare.compareTo(BigDecimal.ZERO) == 0)) {
				if (profitShare != null && profitShare.compareTo(profitShareMin) == -1) {
					throw new GenericException("设置占成超过限制！您当前可以设置的最小占成为：" + profitShareMin.floatValue());
				}
			}
			if (rebateSetting == null) {
				rebateSetting = pmrsDao.get(accountId);
			}
			if (rebateSetting == null) {
				rebateSetting = new ProxyMultiRebateSetting();
				rebateSetting.setAccountId(accountId);
				rebateSetting.setGameShare(BigDecimal.ZERO);
				rebateSetting.setProfitShare(profitShare);
				pmrsDao.insert(rebateSetting);
			} else {
				rebateSetting.setProfitShare(profitShare);
				pmrsDao.save(rebateSetting);
			}
		}
		
		proxyMultiRebateCoreService.flushGameShare(accountId);
		sysAccountDao.save(ac);
	}
	
	@Override
	public SysAccount register(Map atMap, String json, Long platform) {
		// 验证随机码
		String verifyCode = atMap.get("verifyCode") == null ? "" : atMap.get("verifyCode").toString();
		//注册验证码开关 不是on进去验证
		String onoffVerify = StationConfigUtil.get(StationUtil.getStationId(),StationConfig.onoff_register_verify_code);
		if(!"on".equals(onoffVerify)) {
			VerifyCodeUtil.isCheckSuccess(verifyCode);
		}
		
		// 目前没有推广连接，所有注册会员都归到 默认代理下。
		Long agentId = 0l;
		if (
			(StringUtil.isEmpty(atMap.get("agentId")) && StringUtil.equals(platform, SysRegisterConfig.TYPE_MEMBER))
				||(
					StringUtil.equals(platform, SysRegisterConfig.TYPE_AGENT) && 
					"on".equals(StationConfigUtil.get(StationConfig.onoff_multi_agent))
					)
			) {
			// 域名绑定默认代理优先级高于站点绑定的默认代理
			agentId = StationUtil.getDomainAgentId();
			if (Validator.isNull(agentId)) {
				agentId = StationUtil.getStationAgentId();
			}
		}
		Long stationId = StationUtil.getStationId();
		String ip = UserUtil.getIpAddress();

		// 判断同一站点同一IP每天只能注册账号个数
		String registerIpKey = Contants.REGISTER_IP + ":" + stationId + ":" + DateUtil.getCurrentDate() + ":" + ip;
		Integer limit = StringUtil.toInteger(StationConfigUtil.get(StationConfig.same_ip_register_num), 0);
		Integer count = StringUtil.toInteger(RedisAPI.getCache(registerIpKey), 0);

		if (limit != 0 && count >= limit) {
			throw new GenericException("同一IP当天之内只能注册" + limit + "个用户");
		}
		
		MemberGenrtLink lk = getMemberGenrtLink(stationId, atMap);
		String dafaPromocode = StationConfigUtil.get(StationConfig.onoff_register_dafa_promocode);

		if(lk != null && lk.getStatus().intValue() == 1) {
			throw new GenericException("推广码不存在！");
		}
		
		if(lk != null && "on".equals(dafaPromocode)) {
			if(lk.getType().intValue() == 1) {
				platform = SysRegisterConfig.TYPE_AGENT;
			}else if(lk.getType().intValue() == 2) {
				platform = SysRegisterConfig.TYPE_MEMBER;
			}
		}
		
		RegisterConfigVo rcvo = new RegisterConfigVo();
		rcvo.setPlatform(platform);
		rcvo.setShowVal(SysRegisterConfigGroup.VALUE_ENALBED);
		List<Map> regConfs = regConfService.getRegConfVal(rcvo);
		Long requiredVal = 0l;
		Long validateVal = 0l;
		Long uniqueVal = 0l;
		String curValue = "";
		String key = "";
		String name = "";
		String regex = "";
		List<Map> uniqueMap = new ArrayList<Map>();
		SysAccountInfo uniqueInfo = new SysAccountInfo();
		uniqueInfo.setStationId(stationId);
		String pwd = atMap.get("password") == null ? "" : atMap.get("password").toString();
		String rpwd = atMap.get("rpassword") == null ? "" : atMap.get("rpassword").toString();

		// 取出站点注册配置进行验证
		for (Map conf : regConfs) {
			requiredVal = StringUtil.toLong(conf.get("requiredVal"));
			validateVal = StringUtil.toLong(conf.get("validateVal"));
			uniqueVal = StringUtil.toLong(conf.get("uniqueVal"));
			key = conf.get("key").toString();
			name = conf.get("name").toString();
			curValue = StringUtil.trim2Empty(atMap.get(key));
			regex = conf.get("regex").toString();

			if (key.equals("promoCode")) {
				if (requiredVal.longValue() == SysRegisterConfigGroup.VALUE_ENALBED
						&& (lk == null || (SysRegisterConfig.TYPE_MEMBER == platform && lk.getType() != 2 )
								|| (SysRegisterConfig.TYPE_AGENT == platform && lk.getType() != 1))) {
					throw new GenericException("推广码不存在");
				}
				if (StringUtils.isEmpty(curValue) && lk != null) {
					curValue = lk.getLinkKey();
				}
			}

			if (requiredVal.longValue() == SysRegisterConfigGroup.VALUE_ENALBED && StringUtil.isEmpty(curValue)) {
				throw new GenericException(name + "不能为空");
			}

			if (validateVal.longValue() == SysRegisterConfigGroup.VALUE_ENALBED) {
				if (StringUtil.isNotEmpty(regex)) {
					regex = regex.replaceAll("\\\\+", "\\\\");
				}

				if (StringUtil.isNotEmpty(regex) && !Pattern.matches(regex, curValue)) {
					throw new GenericException(name + "格式错误！");
				}
			}

			if (StringUtil.equals(uniqueVal, SysRegisterConfigGroup.VALUE_ENALBED) && StringUtil.isNotEmpty(curValue)) {
				uniqueMap.add(MixUtil.newHashMap("key", key, "name", name, "value", curValue));
				try {
					PropertyUtils.setProperty(uniqueInfo, key, curValue);
				} catch (Exception e) {
					throw new GenericException("注册失败！");
				}
			}
		}

		if (!uniqueMap.isEmpty()) {
			for (Map map : uniqueMap) {
				if (sysAccountInfoDao.isNotUnique(uniqueInfo, "stationId," + map.get("key").toString())) {
					throw new GenericException(
							map.get("name").toString() + "【" + map.get("value").toString() + "】已经被注册" + ",请更换再重试！");
				}
			}
		}

		AccountVo regMemVo = JsonUtil.toBean(json, AccountVo.class);
		regMemVo.setPwd(pwd);
		regMemVo.setRpwd(rpwd);
		regMemVo.setStationId(stationId);
		regMemVo.setStatus(SysAccount.ACCOUNT_STATUS_ENABLED);

		String sessionKeyType = "";
		if (StringUtil.equals(platform, SysRegisterConfig.TYPE_MEMBER)) {
			regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
			sessionKeyType = SystemConfig.SESSION_MEMBER_KEY;
			if (lk != null && StringUtil.equals(lk.getType(), 2)) {
				agentId = lk.getUserId();
			}
		} else if (StringUtil.equals(platform, SysRegisterConfig.TYPE_AGENT)) {
			if (!StringUtils.equals("off", StationConfigUtil.get(StationConfig.onoff_register_daili_checked))) {
				regMemVo.setStatus(SysAccount.ACCOUNT_STATUS_REVIEW);
			}
			if (lk != null && StringUtil.equals(lk.getType(), 1)) {
				agentId = lk.getUserId();
				regMemVo.setRebateNum(StringUtil.toBigDecimal(lk.getCpRolling()));// 如果是会员注册则没有返点数
			}
			
			
			regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			sessionKeyType = SystemConfig.SESSION_DAILI_KEY;
		} else {
			throw new GenericException("非法请求！");
		}
		//判断是否开启动态赔率
		if("on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_member_rate_random))
				&& lk!=null && lk.getDynamicRate()!=null) {
			regMemVo.setDynamicRate(StringUtil.toBigDecimal(lk.getDynamicRate()));//如果是会员保存到动态赔率值
			
		}else if("on".equals(StationConfigUtil.get(StationConfig.onoff_member_rate_random)) 
				&& "on".equals(StationConfigUtil.get(StationConfig.register_account_max_rate)) ){//无推广链接注册使用最大动态赔率
			String string = StationConfigUtil.get(StationConfig.member_dynamic_rate_max_num);
			regMemVo.setDynamicRate(new BigDecimal(string));
		}
		//判断是否开占成计算
		if("on".equalsIgnoreCase(StationConfigUtil.get(StationConfig.onoff_profit_share))
				&& lk!=null && lk.getProfitShare()!=null) {
			regMemVo.setProfitShare(StringUtil.toBigDecimal(lk.getProfitShare()));
		}
		
		//站点开启注册会员转成代理开关，会员全部成代理
		if(StringUtil.equals(SysAccount.ACCOUNT_PLATFORM_MEMBER, regMemVo.getAccountType()) 
				&& "on".equals(StationConfigUtil.get(StationConfig.onoff_register_account_trun_agent))) {
			regMemVo.setRebateNum(regMemVo.getDynamicRate());
			regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			sessionKeyType = SystemConfig.SESSION_DAILI_KEY;
		}
		
		if(lk != null) {
			regMemVo.setRegisterCode(lk.getLinkKey());
		}
		regMemVo.setAgentId(agentId);
		regMemVo.setLastLoginIp(ip);

		// 保存用户
		SysAccount sa = saveAccount(regMemVo);
		SysAccountInfo accountInfo= sysAccountInfoDao.get(sa.getId());
		if (sa.getAccountStatus().longValue() == SysAccount.ACCOUNT_STATUS_ENABLED) {
			updateAccountLoginStatus(sa,accountInfo, sessionKeyType);
		}

		RedisAPI.incrByFloat(registerIpKey, 1, Contants.REGISTER_IP_TIMEOUT);
		return sa;
	}

	private MemberGenrtLink getMemberGenrtLink(Long stationId, Map atMap) {
		String promoCode = (String) atMap.get("promoCode");
		if (StringUtils.isNotEmpty(promoCode)) {
			return generalizeLinkDao.getByLinkKeyAndStationId(stationId, promoCode);
		}
		long linkId = NumberUtils.toLong(RedisAPI.getCache(Contants.registerKey + SysUtil.getSession().getId()), 0);
		if (linkId == 0) {
			linkId = NumberUtils.toLong((String) atMap.get("linkId"), 0);
		}
		if (linkId != 0) {
			return generalizeLinkDao.get(linkId);
		}
		return null;
	}

	@Override
	public Map getAccountById(Long accountId, Long stationId) {
		Map accountMap = sysAccountDao.getAccountById(accountId, stationId);
		if (accountMap == null) {
			throw new GenericException("会员不存在");
		}
		return accountMap;
	}
	
	@Override
	public Map getAccountAndDrawComById(Long accountId, Long stationId) {
		Map accountMap = sysAccountDao.getAccountAndDrawComById(accountId, stationId);
		if (accountMap == null) {
			throw new GenericException("会员不存在");
		}
		IPSeeker ips = IPSeeker.getInstance();
		if(StringUtil.isNotEmpty((String) accountMap.get("registerIp"))){
			String ip = (String) accountMap.get("registerIp");
			String ipArea = ips.getIpAddr(ip);
			if(StringUtil.isEmpty(ipArea)){
				ipArea = ips.getCountry(ip);
				if (ipArea.equals("IANA")) {
					ipArea = "";
				}
			}
			accountMap.put("registerIp",ip+"("+ipArea+")");
		}
		return accountMap;
	}

	@Override
	public Map loadOnlineMember(Long accountId) {
		Map member = sysAccountDao.getAccountById(accountId, StationUtil.getStationId());
		if (member == null) {
			return null;
		}
		if (!StringUtil.equals(StringUtil.toLong(member.get("accountType")), SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
			throw new GenericException("非法请求！");
		}
		return member;
	}

	@Override
	public void updPwdByMember(String opwd, String pwd, String rpwd, Long updType) {
		Boolean repwdFlag = false;
		Object obj = UserUtil.getSession().getAttribute(SystemConfig.SESSION_MEMBER_KEY + "repwdFlag");
		if (obj != null) {
			repwdFlag = (Boolean) obj;
		}
		if (repwdFlag) {
			updType = SysAccount.PASSWORD_TYPE_LOGIN;
		}
		Long id = UserUtil.getUserId();
		String userPwd = "";
		if (!repwdFlag && StringUtil.isEmpty(opwd)) {
			throw new GenericException("旧密码不能为空!");
		}
		if (StringUtil.isEmpty(pwd)) {
			throw new GenericException("新密码不能为空!");
		}
		if (StringUtil.isEmpty(rpwd)) {
			throw new GenericException("确认密码不能为空!");
		}

		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}
		String at = "";
		SysAccount account = null;
		SysAccountInfo info = null;

		// 修改取款密码
		if (StringUtil.equals(updType, SysAccount.PASSWORD_TYPE_RECEIPT)) {
			if (!ValidateUtil.isDrawPassword(pwd)) {
				throw new GenericException("密码格式不正确！");
			}
			//纯数字密码位数
			Long stationId = StationUtil.getStationId();
			String lengthText = "";
			lengthText = StationConfigUtil.get(stationId, StationConfig.member_draw_password_length);
			if(StringUtil.isNotEmpty(lengthText)) {
				String pattern = "^[0-9]{"+lengthText+"}$";
				boolean b = Pattern.matches(pattern, pwd);
				if(b==false) {
					throw new GenericException("取款密码只能由"+lengthText+"位纯数字组成");
				}
				
			}
			info = sysAccountInfoDao.get(id);
			at = info.getAccount();
			userPwd = info.getReceiptPwd();
		} else {
			// 修改登录密码
			if (!ValidateUtil.isPassword(pwd)) {
				throw new GenericException("密码格式不正确！");
			}
			account = sysAccountDao.get(id);
			if (account == null) {
				throw new GenericException("该用户不存在或已删除!");
			}
			userPwd = account.getPassword();
			at = account.getAccount();

		}
		if (!repwdFlag) {
			opwd = MD5Util.getMD5String(at, opwd);
			if (!opwd.equals(userPwd.toUpperCase())) {
				throw new GenericException("旧密码错误!");
			}
		}

		pwd = MD5Util.getMD5String(at, pwd);
		if (StringUtil.equals(updType, SysAccount.PASSWORD_TYPE_RECEIPT)) {
			info.setReceiptPwd(pwd);
			sysAccountInfoDao.save(info);
		} else {
			account.setPassword(pwd);
			sysAccountDao.save(account);
		}
		if (repwdFlag) {
			UserUtil.getSession().setAttribute(SystemConfig.SESSION_MEMBER_KEY + "repwdFlag", false);
		}
	}

	@Override
	public void updRepwdByAccount(String pwd, String rpwd) {
		Long id = UserUtil.getUserId();
		if (StringUtil.isEmpty(pwd)) {
			throw new GenericException("密码不能为空!");
		}
		if (StringUtil.isEmpty(rpwd)) {
			throw new GenericException("确认密码不能为空!");
		}

		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (!ValidateUtil.isPassword(pwd)) {
			throw new GenericException("密码格式不正确！");
		}
		//纯数字密码位数
		Long stationId = StationUtil.getStationId();
		String lengthText = "";
		lengthText = StationConfigUtil.get(stationId, StationConfig.member_draw_password_length);
		if(StringUtil.isNotEmpty(lengthText)) {
			String pattern = "^[0-9]{"+lengthText+"}$";
			boolean b = Pattern.matches(pattern, pwd);
			if(b==false) {
				throw new GenericException("取款密码只能由"+lengthText+"位纯数字组成");
			}
			
		}
		SysAccountInfo account = sysAccountInfoDao.get(id);
		if (account == null) {
			throw new GenericException("该用户不存在或已删除!");
		}
		pwd = MD5Util.getMD5String(account.getAccount(), pwd);
		account.setReceiptPwd(pwd);
		sysAccountInfoDao.save(account);
	}

	@Override
	public Page<SysAccount> getStationAgentList(AccountVo accountVo) {
		SysAccount acc = (SysAccount) UserUtil.getCurrentUser();
		SysStation station = StationUtil.getStation();
		if (Objects.equals(station.getAccountId(), acc.getId())) {
			return sysAccountDao.getStationAgentPage(station.getId(), acc.getId(),accountVo);
		}
		return sysAccountDao.getStationAgentPage(station.getId(), null,accountVo);
	}

	@Override
	public void saveAgentUser(SysAccount user, String pwd, String rpwd) {
		String account = user.getAccount();
		Long accountId = user.getId();
		// 新增用户时要判断密码是否为空
		if (StringUtil.isEmpty(pwd) && Validator.isNull(accountId)) {
			throw new GenericException("密码不能为空!");
		}
		if (!pwd.equals(rpwd)) {
			throw new GenericException("两次密码不一致!");
		}

		if (!ValidateUtil.isPassword(pwd) && Validator.isNull(accountId)) {
			throw new GenericException("密码格式不正确！");
		}
		
		String regex = StationConfigUtil.get(StationConfig.register_regex_username);
		//先取站点配置的正则验证 如果没有就走公共正则匹配
		if(StringUtil.isNotEmpty(regex)) {
			boolean matches = account.matches(regex);
			if(!matches) {
				throw new GenericException("用户账号格式不正确！");
			}
		}else {
			boolean username = ValidateUtil.isUsername(account);
			if(!username) {
				throw new GenericException("用户账号格式不正确！");
			}
		}

		if (StringUtil.isNotEmpty(pwd)) {
			pwd = MD5Util.getMD5String(account, pwd);
			user.setPassword(pwd);
		}
		SysAccount agent = sysAccountDao.get(UserUtil.getUserId());

		SysAccount save = null;
		if (Validator.isNull(accountId)) {
			save = user;
			save.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER);
			save.setStationId(agent.getStationId());
			// 是会员判断站点下是否存在相同会员，是租户就判断是否存在相同账号
			if (sysAccountDao.isNotUnique(save, "account,stationId")) {
				throw new GenericException("该用户名已经被使用");
			}
			save.setAgentId(agent.getId());
			save.setAgentName(agent.getAccount());
			save.setLevel(agent.getLevel() + 1l);
			save.setParents("");
			save.setParentNames("");
			SysLogUtil.log("新增后台用户:" + save.getAccount(), LogType.AGENT_SYSTEM);
		} else {
			save = sysAccountDao.get(accountId);
			save.setGroupId(user.getGroupId());
			SysLogUtil.log("修改后台用户:" + save.getAccount(), LogType.AGENT_SYSTEM);
		}
		sysAccountDao.save(save);
		SysAccountInfo sai = sysAccountInfoDao.get(save.getId());
		if (sai == null) {
			sai = new SysAccountInfo();
			sai.setAccountId(save.getId());
			sai.setAccount(save.getAccount());
			sysAccountInfoDao.insert(sai);
		}

		MnyMoney money = moneyDao.get(save.getId());
		if (money == null) {
			money = new MnyMoney();
			money.setAccountId(save.getId());
			money.setMoneyTypeId(MnyMoney.MONEY_RMB);
			money.setMoney(new BigDecimal(0));
			moneyDao.insert(money);
		}
	}

	@Override
	public void delAgentUser(Long id) {
		// if (Validator.isNull(id)) {
		// throw new GenericException("该记录不存在");
		// }
		// SysAccount delAt = sysAccountDao.get(id);
		//
		// if (delAt == null || !StringUtil.equals(delAt.getStationId(),
		// StationUtil.getStationId())) {
		// throw new GenericException("非法请求！");
		// }
		//
		// if (delAt.getLevel() < UserUtil.getLevel()) {
		// throw new GenericException("权限不够！");
		// }
		//
		// sysAccountDao.delete(id);
		// sysAccountInfoDao.delete(id);
		// moneyDao.delete(id);
		// SysLogUtil.log("删除后台用户,编号为" + id, LogType.AGENT_SYSTEM);
	}

	@Override
	public SysAccountInfo getAccountInfo(Long id) {
		return sysAccountInfoDao.get(id);
	}

	@Override
	public void saveBankInfo(AccountVo avo, boolean flag) {
		SysAccountInfo info = sysAccountInfoDao.get(avo.getId());

		if (StringUtil.isEmpty(avo.getBankName())) {
			throw new GenericException("出款银行名称为空！");
		}
		if (StringUtil.isEmpty(avo.getUserName())) {
			throw new GenericException("持卡人姓名为空！");
		}
		if (StringUtil.isEmpty(avo.getCardNo())) {
			throw new GenericException("出款银行帐号为空！");
		}
		if (!ValidateUtil.isNumber(avo.getCardNo().trim())) {
			throw new GenericException("出款银行帐号只能为数字！");
		}
		if (flag) {
			if (StringUtil.isEmpty(avo.getReceiptPwd())) {
				throw new GenericException("取款密码为空！");
			}

			if (StringUtil.isEmpty(info.getReceiptPwd())) {
				throw new GenericException("用户未设置取款密码！");
			}

			if (!info.getReceiptPwd().equals(MD5Util.getMD5String(info.getAccount(), avo.getReceiptPwd()))) {
				throw new GenericException("取款密码不正确！");
			}
		}
		// if (sysAccountInfoDao.existCardNo(avo.getCardNo(), avo.getId())) {
		// throw new GenericException("银行帐号已经被使用！");
		// }

		String oldUserName = info.getUserName();
		if (StringUtils.isNotEmpty(oldUserName)) {
			if (!oldUserName.equals(avo.getUserName())) {
				throw new GenericException("持卡人姓名不一致！");
			}
		}

		RegisterConfigVo rcvo = new RegisterConfigVo();
		if (StationUtil.isDailiStation()) {
			rcvo.setPlatform(SysRegisterConfig.TYPE_AGENT);
		} else {
			rcvo.setPlatform(SysRegisterConfig.TYPE_MEMBER);
		}

		List<Map> regConfs = regConfService.getRegConfVal(rcvo);
		Long uniqueVal = 0l;
		String columnKey = "";
		SysAccountInfo uniqueInfo = new SysAccountInfo();
		uniqueInfo.setAccountId(avo.getId());
		uniqueInfo.setStationId(info.getStationId());
		uniqueInfo.setCardNo(avo.getCardNo());
		uniqueInfo.setUserName(avo.getUserName());

		// 取出站点注册配置进行验证
		for (Map conf : regConfs) {
			uniqueVal = StringUtil.toLong(conf.get("uniqueVal"));
			columnKey = (String) conf.get("key");
			if (("cardNo".equals(columnKey) || "userName".equals(columnKey))
					&& StringUtil.equals(uniqueVal, SysRegisterConfigGroup.VALUE_ENALBED)) {
				if (sysAccountInfoDao.isNotUnique(uniqueInfo, "stationId," + columnKey)) {
					throw new GenericException(conf.get("name").toString() + "已经被使用！请更换再重试！");
				}
			}
		}

		
		//已经存在银行卡和姓名就不允许修改
		if(StringUtil.isNotEmpty(info.getCardNo()) || StringUtil.isNotEmpty(info.getBankName())) {
			throw new GenericException("不允许再次修改银行卡或姓名 请联系客服");
		}
		
		String province = StringUtil.trim2Empty(avo.getProvince());
		String city = StringUtil.trim2Empty(avo.getCity());
		String address = StringUtil.trim2Empty(avo.getBankAddress());
		String bankAddress = "";
		if (StringUtil.isNotEmpty(province)) {
			bankAddress += province;
		}
		if (StringUtil.isNotEmpty(city)) {
			bankAddress += city;
		}
		if (StringUtil.isNotEmpty(address)) {
			bankAddress += address;
		}

		info.setUserName(avo.getUserName());
		info.setBankAddress(bankAddress);
		info.setBankName(avo.getBankName());
		info.setCity(city);
		info.setProvince(province);
		info.setCardNo(avo.getCardNo());
		if(StringUtils.isNotEmpty(avo.getPhone())){
			info.setPhone(avo.getPhone());
		}
		sysAccountInfoDao.save(info);
	}
	
	@Override
	public Map getAgentMultiData(String account) {
		AccountVo avo = new AccountVo();
		avo.setAccount(account);
		return getAgentMultiData(avo);
	}
	
	@Override
	public Map getAgentMultiData(Long accountId) {
		AccountVo avo = new AccountVo();
		avo.setId(accountId);
		return getAgentMultiData(avo);
	}
	public Map getAgentMultiData(String account,Long accountId) {
		AccountVo avo = new AccountVo();
		//设置上级用户账号
		avo.setAccount(account);
		//设置当前修改用户id
		avo.setSelfId(accountId);
		return getAgentMultiData(avo);
	}
	@Override
	public Map getAgentMultiData(AccountVo avo) {
		Long stationId = avo.getStationId();
		if(stationId==null) {
			stationId = StationUtil.getStationId();
		}
		String multiAgent = StationConfigUtil.get(stationId,StationConfig.onoff_multi_agent);
		String profitShare = StationConfigUtil.get(stationId,StationConfig.onoff_profit_share);
		String dynamicRate = StationConfigUtil.get(stationId,StationConfig.onoff_member_rate_random);
		String mergeRebate = StationConfigUtil.get(stationId,StationConfig.onoff_agent_merge_Rebate_odds_switch);
		String agentRebate = StationConfigUtil.get(stationId,StationConfig.agent_lottery_rebate_choice);
		if(StringUtil.isEmpty(mergeRebate)) {
			mergeRebate = "off";
		}
		
		if(StringUtil.isEmpty(profitShare)) {
			profitShare = "off";
		}
		if(StringUtil.isEmpty(dynamicRate)) {
			dynamicRate = "off";
		}
		if(StringUtil.isEmpty(agentRebate) || "on".equals(mergeRebate)) {
			agentRebate = "off";
		}
		BigDecimal rebateMaxNum = null;
		BigDecimal rebateMinNum = null;
		BigDecimal curRebateNum = null;
		BigDecimal rebateStep = null;
		BigDecimal totalRebateNum = null;

		BigDecimal profitShareMax = null;
		BigDecimal profitShareMin = null;
		BigDecimal curProfitShare = BigDecimal.ZERO;
		BigDecimal profitShareTotal = null;
		
		/**
		 * 定义动态赔率相关值
		 */
		BigDecimal dynamicRateMax = null;
		BigDecimal dynamicRateMin = null;
		BigDecimal curDynamicRate = null;
		BigDecimal dynamicRateTotal = null;

		

		Long accountId = avo.getId();
		String accountName = avo.getAccount();

		if (StationUtil.isDailiStation() && Validator.isNull(accountId)) {
			accountId = UserUtil.getUserId();
		}
		//多级代理关闭可能因为开启了返点赔率合并开关
		if ("on".equals(multiAgent) || ("off".equals(multiAgent))&&"on".equals(mergeRebate)) {
			rebateMaxNum = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_rebate_max_num));
			profitShareMax = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_profit_share_max_num));
			dynamicRateMax = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.member_dynamic_rate_max_num));
			
			if (accountId != null || StringUtil.isNotEmpty(accountName)) {
				rebateStep = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.agent_rebate_step));
				SysAccount account = null;
				if (Validator.isNotNull(accountId)) {
					account = sysAccountDao.get(accountId);
				}
				if (StringUtil.isNotEmpty(accountName)) {
					account = sysAccountDao.findOneByAccountAndStationId(accountName, StationUtil.getStationId());
				}
				if (account == null) {
					throw new GenericException("非法请求！");
				}
				
				if(StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
					return null;
				}
				accountId = account.getId();
				if (StationUtil.isDailiStation() && !StringUtil.equals(account.getId(), UserUtil.getUserId())
						&& account.getParents().indexOf("," + UserUtil.getUserId() + ",") == -1) {
					throw new GenericException("非法请求！");
				}

				/*if ((!StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
						&& !StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL))) {
					curRebateNum = account.getRate();
				} else {
					Map accountShareMap = pmrsDao.getAccountRebate(accountId);
					if(accountShareMap != null) {
						curProfitShare = StringUtil.toBigDecimal(accountShareMap.get("profitShare"));
						curRebateNum = StringUtil.toBigDecimal(accountShareMap.get("gameShare"));
						curDynamicRate = StringUtil.toBigDecimal(accountShareMap.get("gameRate"));
					}
				}*/
				//获取代理当前返点值
				if ((StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)
						|| StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL))) {
					Map accountShareMap = pmrsDao.getAccountRebate(accountId);
					if(accountShareMap != null) {
						curProfitShare = StringUtil.toBigDecimal(accountShareMap.get("profitShare"));
						curRebateNum = StringUtil.toBigDecimal(accountShareMap.get("gameShare"));
					}
				}
				//获取代理当前动态赔率值
				curDynamicRate = account.getRate();
				//如果上级没有赔率值则设置为满赔率
				if(curDynamicRate==null){
					curDynamicRate = dynamicRateMax;
				}

				if (Validator.isNull(account.getAgentId())) {
					totalRebateNum = rebateMaxNum;
					profitShareTotal = profitShareMax;
					dynamicRateTotal = dynamicRateMax;
				} else {
					Map agentShareMap = pmrsDao.getAccountRebate(account.getAgentId());
					//动态赔率获取上级rate
					SysAccount agentAccount  = sysAccountDao.findOneByAccountIdAndStationId(account.getAgentId(), StationUtil.getStationId());
					if(agentShareMap!= null ) {
						totalRebateNum = StringUtil.toBigDecimal(agentShareMap.get("gameShare"));
						profitShareTotal = StringUtil.toBigDecimal(agentShareMap.get("profitShare"));
					}
					dynamicRateTotal = agentAccount.getRate();
				}

				// 得到请求用户的下级最大返点并加上返点差额 begin
				if (avo.getGeneralize() == null || !avo.getGeneralize()) {
					Map maxMap = pmrsDao.getChildrenMaxNum(accountId);
					rebateMinNum = StringUtil.toBigDecimal(maxMap.get("gameShare"));
					profitShareMin = StringUtil.toBigDecimal(maxMap.get("profitShare"));
					dynamicRateMin = StringUtil.toBigDecimal(maxMap.get("gameRate"));
					//会员没有下级，默认可设置最小赔率为0
					if (Validator.isNotNull(avo.getSelfId())) {
						SysAccount member = sysAccountDao.get(avo.getSelfId());
						if(member!=null && (StringUtil.equals(member.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)
								||StringUtil.equals(member.getAccountType(), SysAccount.ACCOUNT_PLATFORM_GUIDE))){
							dynamicRateMin = BigDecimal.ZERO;
						}
						
					}
					
				}

				rebateMinNum = nullToZero(rebateMinNum);
				totalRebateNum = nullToZero(totalRebateNum);
				curRebateNum = nullToZero(curRebateNum);
				
				profitShareMin = nullToZero(profitShareMin);
				profitShareTotal = nullToZero(profitShareTotal);
				curProfitShare = nullToZero(curProfitShare);
				
				dynamicRateMin = nullToZero(dynamicRateMin);
				dynamicRateTotal = nullToZero(dynamicRateTotal);
				curDynamicRate = nullToZero(curDynamicRate);

				// 如果已经12点全分完，最小可分点数不用再加返点差额。
				if (rebateMinNum.compareTo(rebateMaxNum) == -1) {
					rebateMinNum = rebateMinNum.add(rebateStep);
				}
				// 如果已经12点全分完，最小可分点数不用再加返点差额。
				/*if (dynamicRateMin.compareTo(dynamicRateMax) == -1) {
					dynamicRateMin = dynamicRateMin.add(rebateStep);
				}
				*/
				// 得到请求用户的下级最大返点并加上返点差额 end

				// 总代的最大限制为配置值
				if (!StringUtil.equals(account.getAgentId(), 0l) || StationUtil.isAgentStation()
						|| StationUtil.isDailiStation() || StationUtil.isNativeStation()) {
					// 取当前代理的点数 - 上下级返点差额 = 可以分配点数最大值
					BigDecimal zero = new BigDecimal(0);
					if (avo.getGeneralize() != null && avo.getGeneralize()) {
						rebateMaxNum = curRebateNum.subtract(rebateStep);
						profitShareMax = curProfitShare;
						dynamicRateMax = curDynamicRate.subtract(rebateStep);
					} else {
						if (!Validator.isNull(account.getAgentId())) {
							rebateMaxNum = totalRebateNum.subtract(rebateStep);
							profitShareMax = profitShareTotal;
							dynamicRateMax =  dynamicRateTotal.subtract(rebateStep);
						}
					}
					if (rebateMaxNum.compareTo(zero) == -1) {
						rebateMaxNum = zero;
					}
					if (profitShareMax.compareTo(zero) == -1) {
						profitShareMax = zero;
					}
					if (dynamicRateMax.compareTo(zero) == -1) {
						dynamicRateMax = zero;
					}
					
				}
			}
		}else{//没开启多级代理，默认最大赔率
			if(dynamicRate!=null && "on".equals(dynamicRate)){
				dynamicRateMax = StringUtil.toBigDecimal(StationConfigUtil.get(stationId,StationConfig.member_dynamic_rate_max_num));
			}
		}

		// 多级代理返点设置
		return MixUtil.newHashMap("multiAgent", multiAgent, "profitShare", profitShare, "dynamicRate", dynamicRate,
				"mergeRebate",mergeRebate,"agentRebate",agentRebate,"rebateMaxNum",
				rebateMaxNum == null ? 0 : rebateMaxNum.floatValue(), "curRebateNum",
				curRebateNum == null ? 0 : curRebateNum.floatValue(), "rebateMinNum",
				rebateMinNum == null ? 0 : rebateMinNum.floatValue(), "profitShareMax",
				profitShareMax == null ? 0 : profitShareMax.floatValue(), "profitShareMin",
				profitShareMin == null ? 0 : profitShareMin.floatValue(), "curProfitShare",
				curProfitShare == null ? 0 : curProfitShare.floatValue(),"dynamicRateMax",
				dynamicRateMax == null ? 0 : dynamicRateMax.floatValue(), "dynamicRateMin",
				dynamicRateMin == null ? 0 : dynamicRateMin.floatValue(), "curDynamicRate",
				curDynamicRate == null ? 0 : curDynamicRate.floatValue());
	}
	
	private BigDecimal nullToZero(BigDecimal big) {
		return StringUtil.toBigDecimal(big).setScale(2,RoundingMode.FLOOR);
	}

	@Override
	public SysAccount findOneByAccountAndStationId(String account, Long stationId) {

		if (StringUtil.isEmpty(account)) {
			throw new GenericException("会员账号不能为空！");
		}

		SysAccount ac = sysAccountDao.findOneByAccountAndStationId(account, stationId);
		if (ac == null) {
			throw new GenericException("会员"+account+"不存在！");
		}
		return ac;
	}
	
	@Override
	public SysAccount queryDdgd(String account, Long type,Long stationId) {
		return  sysAccountDao.queryDdgd(account, type, stationId);
	}

	@Override
	public void updateRebateNum(Long updAtId, BigDecimal rebateNum, BigDecimal profitShare,BigDecimal dynamicRate) {
		AccountVo avo = new AccountVo();
		avo.setId(updAtId);
		Map multiMap = getAgentMultiData(avo);
		SysAccount sa = sysAccountDao.get(updAtId);
		setRebateNum(sa, rebateNum,profitShare,dynamicRate, multiMap);
	}

	@Override
	public void login4Card(String code) {
		SecurityCardUtil.check(code);
		long userId = StringUtil
				.toLong(RedisAPI.getCache(SecurityCardUtil.getCacheKey(SecurityCardUtil.SECURITY_CARD_LOGIN_USER_ID)));
		if (Validator.isNull(userId)) {
			throw new GenericException("密保卡已过期");
		}
		SysAccount account = sysAccountDao.get(userId);
		if (account == null) {
			throw new GenericException("会员不存在");
		}
		SysAccountInfo accountInfo = sysAccountInfoDao.get(userId);
		String key = null;
		if (StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {// 代理
			key = SystemConfig.SESSION_DAILI_KEY;
		} else if (StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)
				|| StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER)) { // 租户后台会员
			key = SystemConfig.SESSION_AGENT_KEY;
		} else {
			throw new GenericException("用户信息异常");
		}
		updateAccountLoginStatus(account,accountInfo, key);
	}

	@Override
	public void forcedOffLine(Long userId) {
		SysAccount at = sysAccountDao.get(userId);
		if (at == null || !StringUtil.equals(at.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求！");
		}
		
		AccountVo avo = new AccountVo();
		avo.setId(userId);
		avo.setOnline(SysAccount.ONLINE_FLAG_OFF);
		sysAccountDao.updAccountByVo(avo);
		OnlineManager.forcedOffLine(at);
		SysLogUtil.log("将会员:" + at.getAccount() + "踢下线。", LogType.AGENT_MEMBER);
	}

	@Override
	public Integer getOnlineCount() {
		int count = 0;
		if (!UserUtil.isLogin()) {
			return count;
		}
		if (StationUtil.isAgentStation()) {
			count = OnlineManager.getOnlineCount(StationUtil.getStationId(), null);
		} else if (StationUtil.isDailiStation()) {
			count = OnlineManager.getOnlineCount(StationUtil.getStationId(), UserUtil.getChildren());
		}
		return count;
	}

	@Override
	public void setGeneralAgent(Long accountId, String generalAgent) {
		Long stationId = StationUtil.getStationId();
		// 通过账号拿到总代
		AccountVo avo = new AccountVo();
		avo.setAccount(generalAgent);
		avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL);
		avo.setStationId(stationId);
		SysAccount general = sysAccountDao.queryAccount(avo);
		if (general == null) {
			throw new GenericException("总代理不存在！");
		}
		// 得到被指定的代理
		SysAccount agent = sysAccountDao.get(accountId);
		if (agent == null || !StringUtil.equals(agent.getStationId(), stationId)
				|| !StringUtil.equals(agent.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			throw new GenericException("非法请求！");
		}

		if (Validator.isNotNull(agent.getAgentId())) {
			throw new GenericException("该代理已经有所属总代理！");
		}

		Long generalId = general.getId();
		String gAccount = general.getAccount();
		Long agentId = agent.getId();
		String gChildren = "," + generalId;
		String gChildrenName = "," + gAccount;
		String aChildren = "," + agentId + ",";
		// 代理指定它的总代
		agent.setAgentId(generalId);
		agent.setAgentName(gAccount);
		agent.setParents(gChildren + ",");
		agent.setParentNames(gChildrenName + ",");
		sysAccountDao.save(agent);
		// 更新所有该代理的下级的关系
		sysAccountDao.updAgentParents(stationId, aChildren, gChildren, gChildrenName);
	}

	@Override
	public void delGeneralAgent(Long accountId) {
		Long stationId = StationUtil.getStationId();
		// 得到被指定的代理
		SysAccount agent = sysAccountDao.get(accountId);
		if (agent == null || !StringUtil.equals(agent.getStationId(), stationId)
				|| !StringUtil.equals(agent.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			throw new GenericException("非法请求！");
		}
		Long agentId = agent.getId();
		String gChildren = "," + agentId + ",";
		String gChildrenName = "," + agent.getAccount() + ",";
		String aChildren = agent.getParents() + agentId + ",";
		sysAccountDao.delGeneralAgent(accountId);
		// 更新所有该代理的下级的关系
		sysAccountDao.delAgentParents(stationId, aChildren, gChildren, gChildrenName);
	}

	@Override
	public void accountDrawNeedOpe(Long accountId, BigDecimal drawNeed, Long opeType, String remark) {

		if (StringUtil.isEmpty(drawNeed) || drawNeed.compareTo(BigDecimal.ZERO) != 1) {
			throw new GenericException("操作数额不能小于零!");
		}

		if (StringUtil.isEmpty(opeType)) {
			throw new GenericException("操作类型不能为空！");
		}

		SysAccount account = sysAccountDao.get(accountId);

		if (account == null || !StringUtil.equals(account.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求!");
		}

		String opeTypeName = "人工增加";
		int type = BetNumRecord.TYPE_DRAWNEED_ADD;
		if (StringUtil.equals(opeType, Contants.DRAWNEED_OPE_TYPE_SUB)) {
			drawNeed = drawNeed.negate();
			opeTypeName = "人工扣除";
			type = BetNumRecord.TYPE_DRAWNEED_SUB;
		}
		if (!mnyBetNumberDao.addDrawNeed(accountId, drawNeed, type, opeTypeName + ";操作原因:" + remark, null)) {
			throw new GenericException("操作数额错误！");
		}

		SysLogUtil.log("为会员:" + account.getAccount() + opeTypeName + "打码量:" + drawNeed.abs() + ";操作原因:" + remark,
				LogType.AGENT_FINANCE);
	}

	@Override
	public Map queryAccount(String account, Long stationId) {
		Map accountMap = sysAccountDao.queryAccount(account, stationId);
		if (accountMap == null || accountMap.isEmpty()) {
			throw new GenericException("会员不存在！");
		}
		return accountMap;
	}

	@Override
	public boolean checkBankCard(Long userId, String bankCard) {

		SysAccountInfo entity = null;
		Long stationId = StationUtil.getStationId();
		if (Validator.isNull(userId)) {
			entity = new SysAccountInfo();
			entity.setStationId(stationId);
		} else {
			entity = sysAccountInfoDao.get(userId);
		}

		if (Validator.isNotNull(entity.getStationId()) && !StringUtil.equals(entity.getStationId(), stationId)) {
			throw new GenericException("非法请求！");
		}

		if (StringUtil.isEmpty(bankCard) || bankCard.equals(entity.getCardNo())) {
			return false;
		}
		bankCard = StringUtil.trim2Empty(bankCard);
		entity.setCardNo(bankCard);
		return sysAccountInfoDao.isNotUnique(entity, "cardNo,stationId");
	}
	
	@Override
	public String batchMoveAgent(Long stationId,String agentName, String accounts,Long accountType) {
		
		String sonName = "会员";
		StationConfig config = StationConfig.onoff_member_move_agent;
		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			sonName = "子代理";
			config = StationConfig.onoff_agent_move_agent;
		}
		
		if(!"on".equals(StationConfigUtil.get(config))) {
			throw new GenericException(sonName+"批量转移代理开关未开启");
		}
		
		if(StringUtil.isEmpty(agentName)) {
			throw new GenericException("上级代理不存在");
			
		}
		if(agentName.equals(accounts)) {
			throw new GenericException("上级代理和子代理不能相同");
		}
		if(StringUtil.isEmpty(accounts)) {
			throw new GenericException(sonName+"为空");
		}
		AccountVo avo = new AccountVo();
		avo.setStationId(stationId);
		avo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
		avo.setAccount(agentName);
		SysAccount agent = sysAccountDao.queryAccount(avo);
		if(agent == null) {
			throw new GenericException("上级代理不存在");
		}
		String[] sonAccounts = accounts.split(",| |\n");
		if(sonAccounts == null || sonAccounts.length == 0) {
			throw new GenericException(sonName+"为空");
		}
		
		//如果子代理也包含了上级代理，那么把上级代理从子代理中移除
		ArrayList<String> arrayList = new ArrayList<String>();
		for (int i = 0; i < sonAccounts.length; i++) {
			arrayList.add(sonAccounts[i]);
		}
		Iterator<String> iterator = arrayList.iterator();
		while (iterator.hasNext()) {
			String string = iterator.next();
			if(string.equals(agentName)) {
				iterator.remove();
			}
		}
		sonAccounts = arrayList.toArray(new String[arrayList.size()]);
		
		//上级代理必须和子代理或者会员报表类型或者会员类型保持一致 
		StringBuilder accountbuilder = new StringBuilder();
		StringBuilder reportbuilder = new StringBuilder();
		
		for (String account : sonAccounts) {
			//找出所有子代理或者会员
			AccountVo ziAvo = new AccountVo();
			ziAvo.setStationId(stationId);
			ziAvo.setAccount(account);
			SysAccount sysAccount = sysAccountDao.queryAccount(ziAvo);
			if(sysAccount==null) {
				throw new GenericException(account.toString()+"的賬號不存在");
			}
			
			if(sysAccount.getAccountType()!=accountType) {
				accountbuilder.append(account+",");
			}
			if(agent.getReportType()!=sysAccount.getReportType()) {
				reportbuilder.append(account+",");
			}
		}
		
		if(accountbuilder.length()>0) {
			throw new GenericException(accountbuilder.toString()+"的会员为会员类型错误的会员");
		}
		if(reportbuilder.length()>0) {
			throw new GenericException(reportbuilder.toString()+"的会员与上级代理报表类型不一致");
		}
		
		int[] success = sysAccountDao.batchUpdateAgentName(agent,sonAccounts, accountType, stationId);
		String failedAccount ="";
		for (int i = 0; i < success.length; i++) {
			if(success[i] == 0) {
				if(StringUtil.isNotEmpty(failedAccount)) {
					failedAccount +=",";
				}
				failedAccount +=sonAccounts[i];
			}
		}
		if(StringUtil.isNotEmpty(failedAccount)) {
			failedAccount += "为更新失败的"+sonName+"";
		}
		return failedAccount;
	}

	@Override
	public boolean userNameExits(String userName) {
		SysStation station = StationUtil.getStation();
		SysAccountInfo info = new SysAccountInfo();
		info.setStationId(station.getId());
		info.setUserName(userName);
		if (sysAccountInfoDao.isNotUnique(info, "stationId,userName")) {
			return true;
		}
		return false;
	}

	@Override
	public boolean userAccountExits(String userName) {
		SysStation station = StationUtil.getStation();
		SysAccountInfo info = new SysAccountInfo();
		info.setStationId(station.getId());
		info.setAccount(userName);
		if (sysAccountInfoDao.isNotUnique(info, "stationId,account")) {
			return true;
		}
		return false;
	}

	@Override
	public void saveAccountUserName(SysAccountInfo info) {
		sysAccountInfoDao.save(info);
	}

	@Override
	public SysAccount getFromCache(Long accountId, Long stationId) {
		SysAccount sa = sysAccountDao.getFromCache(accountId);
		if (sa != null) {
			if (stationId == null || sa.getStationId().equals(stationId)) {
				return sa;
			}
		}
		return null;
	}

	@Override
	public SysAccount getOne(Long accountId, Long stationId) {
		return sysAccountDao.findOneByAccountIdAndStationId(accountId, stationId);
	}

	private String getTestGuestName(Long stationId) {
		String userName = "guest" + sysAccountDao.getNextTestGuestId();
		while (sysAccountDao.existAccount(userName, stationId)) {
			userName = "guest" + sysAccountDao.getNextTestGuestId();
		}
		return userName;
	}
	

	private SysAccount registerGuestLogic() {
		Long stationId = StationUtil.getStationId();
		if (!StringUtils.equals("on",
				StationConfigUtil.get(StationUtil.getStationId(), StationConfig.on_off_register_test_guest_station))) {
			throw new GenericException("该站点不能试玩");
		}
		String ip = UserUtil.getIpAddress();

		// 判断同一站点同一IP每天只能注册账号个数
		String registerIpKey = Contants.REGISTER_IP + "NativeSysConfigController:" + stationId + ":" + DateUtil.getCurrentDate() + ":" + ip;
		Integer count = StringUtil.toInteger(RedisAPI.getCache(registerIpKey), 0);
		Integer limit = StringUtil.toInteger(StationConfigUtil.get(StationConfig.register_test_guest_station_count), 5);
		if (count.intValue() >= limit.intValue()) {
			throw new GenericException("您一天之内只能注册"+limit.intValue()+"个测试用户");
		}
		String userName = getTestGuestName(stationId);
		Long agentId = StationUtil.getStationAgentId();
		SysAccount acc = new SysAccount();
		acc.setAccount(userName);
		acc.setPassword(MD5Util.getMD5String(userName, userName + "123123"));
		acc.setAccountType(SysAccount.ACCOUNT_PLATFORM_TEST_GUEST);
		acc.setCreateDatetime(new Date());
		acc.setAccountStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
		acc.setAgentId(agentId);
		acc.setStationId(stationId);
		SysAccount agent = sysAccountDao.getFromCache(agentId);
		if (agent != null) {
			acc.setAgentName(agent.getAccount());
			acc.setParentNames("," + agent.getAccount() + ",");
			acc.setParents("," + agentId + ",");
		}
		acc.setLevel(2L);
		acc.setBetNum(BigDecimal.ZERO);
		acc.setLastLoginIp(ip);
		acc.setRegisterIp(ip);
		acc.setDrawNeed(BigDecimal.ZERO);
		acc.setOnline(SysAccount.ONLINE_FLAG_OFF);
		acc.setLevelGroup(MemberLevelUtil.getDefaultLevelId());
		setRegisterUserAgentInfo(acc);
		sysAccountDao.insert(acc);

		SysAccountInfo sai = new SysAccountInfo();
		sai.setAccountId(acc.getId());
		sai.setAccount(acc.getAccount());
		sai.setUserName(StringUtil.trim2Empty(acc.getAccount()));
		sai.setStationId(stationId);
		sysAccountInfoDao.insert(sai);
		MnyMoney money = new MnyMoney();
		money.setAccountId(acc.getId());
		money.setMoneyTypeId(MnyMoney.MONEY_RMB);
		/**
		 * 试玩账号加2000现金
		 */
		String moneyStr = StationConfigUtil.get(StationUtil.getStationId(),
				StationConfig.register_test_guest_init_money);
		if (StringUtils.isEmpty(moneyStr)) {
			moneyStr = "2000";
		}
		money.setMoney(new BigDecimal(moneyStr));
		moneyDao.insert(money);
		// 保存用户
		updateAccountLoginStatus(acc, sai,SystemConfig.SESSION_MEMBER_KEY);

		RedisAPI.incrByFloat(registerIpKey, 1, Contants.REGISTER_IP_TIMEOUT);

		return acc;
	}


	@Override
	public SysAccount registerTestGuestForNative() {
		return registerGuestLogic();
	}

	@Override
	public String registerTestGuest() {
		SysAccount sysAccount = registerGuestLogic();
		return sysAccount.getAccount();
	}

	@Override
	public void updateGroup(Long id, Long stationId, Long groupId,String remark) {
		sysAccountDao.updateGroup(id, stationId, groupId,remark);
	}

	@Override
	public void clearGuestTestData(Date time) {
		sysAccountDao.clearGuestTestData(time);
		if (sysAccountDao.getNextTestGuestId() > 500000) {
			sysAccountDao.getReSetTestGuestId();
		}
	}

	@Override
	public Map<Long, Map> countStation(String startTime, Date endTime) {
		return sysAccountDao.countStation(startTime, endTime);
	}

	@Override
	public void updateUserOffline(Long accountId) {
		sysAccountDao.updateUserOffline(accountId);
	}

	@Override
	public void updateAllOffline() {
		sysAccountDao.updateAllOffline();
	}

	@Override
	public void updateOnlineStatus(Set<Long> ids) {
		if (ids == null || ids.isEmpty())
			return;
		sysAccountDao.updateOnlineStatus(new ArrayList<Long>(ids));
	}

	@Override
	public void saveOnlineInfo(List<OnlineInfo> oiList) {
		sysAccountDao.batchUpdateAccountLoginInfo(oiList);
	}

	@Override
	public void scoreToZero(Long stationId) {
		sysAccountDao.scoreToZero(stationId);
		SysLogUtil.log("将所有用户积分归零", LogType.AGENT_SYSTEM);
	}

	@Override
	public int checkSubAccount(String parAccount, String subAccount, Long stationId) {
		// TODO Auto-generated method stub
		return sysAccountDao.checkSubAccount(parAccount,subAccount,stationId);
	}

	@Override
	public String updateAccountChatToken(String content, String token,Long stationId,String chatName) {
		if (StringUtil.isEmpty(content)) {
			throw new GenericException("成员为空");
		}

		String[] sonAccounts = content.split(",| |\n");
		if (sonAccounts == null || sonAccounts.length == 0) {
			throw new GenericException("成员为空");
		}
		int success[] = sysAccountDao.batchUpdateChatToken( sonAccounts, token,stationId);
		String failedAccount = "";
		String successAccount = "";
		for (int i = 0; i < success.length; i++) {
			if (success[i] == 0) {
				if (StringUtil.isNotEmpty(failedAccount)) {
					failedAccount += ",";
				}
				failedAccount += sonAccounts[i];
			}else if(success[i] == 1){
				if (StringUtil.isNotEmpty(successAccount)) {
					successAccount += ",";
				}
				successAccount += sonAccounts[i];
			}
		}
		if (StringUtil.isNotEmpty(failedAccount)) {
			failedAccount += "为导入失败的成员";
		}
		Map result = MixUtil.newHashMap("failed",failedAccount,"success",successAccount);
		if(StringUtil.isNotEmpty(successAccount)){
			String op = "移除";
			if(StringUtil.isNotEmpty(token)){
				op = "添加";
			}
			SysLogUtil.log(op+"聊天室"+chatName+"会员,账号为："+successAccount,LogType.AGENT_MEMBER);
		}
		return JSONObject.toJSONString(result);
	}

	@Override
	public String getAccountStrByChat(String token, Long stationId) {
		// TODO Auto-generated method stub
		return sysAccountDao.getAccountStrByChat(token,stationId);
	}

	@Override
	public <Map> Page getBalanceGemPage(AccountVo acc) {
		if (Validator.isNull(acc.getStationId())) {
			return new Page();
		}
		return sysAccountDao.getBalanceGemPage(acc);
	}
	@Override
	public List<SysAccount> getAllDownMember(String account,String children, Long stationId) {
		List<SysAccount> list = sysAccountDao.getAllDownMember(account, children, stationId);
		return list;
	}

	@Override
	public void deleteById(Long id) {
		if(id==null){
			return;
		}
		SysAccount acc = sysAccountDao.get(id);
		if(acc==null || StringUtil.isEmpty(acc.getAccount())){
			throw new GenericException("用户不存在");
		}
		//先删除关联表记录
		moneyDao.deleteByAccountId(id);
		sysAccountInfoDao.deleteByAccountId(id);
		depositInfoDao.deleteByAccountId(id);
		sysAccountDao.deleteByAccountId(id);
		SysLogUtil.log("刪除管理員："+acc.getAccount(), LogType.AGENT_SYSTEM);
	}
	
	private void loginDisableAccount(SysAccount account) {
		Long accountId = account.getId();
		if (!StationUtil.isAdminStation()
				&& !StringUtil.equals(account.getStationId(), StationUtil.getStationId())) {
			throw new GenericException("非法请求!");
		}
		sysAccountDao.updateDisableAccount(accountId);
		
		//自动封禁后清除session的attribute 
		SysUtil.getSession().removeAttribute("LOGIN_MEMBER_FAILED_COUNT");
		
		String params = JsonUtil.toJson(UserUtil.getRequest().getParameterMap());
		SysLogUtil.log("可疑账号:" + account.getAccount()+"，系统自动封禁", "", accountId, account.getAccount(), UserUtil.getIpAddress(), account.getStationId(),LogType.AGENT_SYSTEM.getType(), SysLog.PLATFORM_PLATFORM,
				params);
	}

	@Override
	public int updateAccountHeadUrl(Long userId, String headUrl) {
		// TODO Auto-generated method stub
		return sysAccountInfoDao.updateAccountHeadUrl(userId,headUrl);
	}
	public void memberChangeDaili(Long accountId,String rebateNum,String dynamicRate) {
		Long stationId = StationUtil.getStationId();
		
		String change = StationConfigUtil.get(stationId, StationConfig.member_change_daili);
		String duo = StationConfigUtil.get(stationId, StationConfig.onoff_multi_agent);
		//判断
		if(Validator.isNull(accountId)) {
			throw new GenericException("非法请求!");
		}
		if(!"on".equals(change) || !"on".equals(duo)) {
			throw new GenericException("非法请求!");
		}
		SysAccount sysAccount = sysAccountDao.findOneByAccountIdAndStationId(accountId, stationId);
		
		if(!StringUtil.equals(sysAccount.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER)) {
			throw new GenericException("错误的会员类型");
		}
		String maxAgentRebate = StationConfigUtil.get(stationId, StationConfig.agent_rebate_max_num);
		String maxDynRate = StationConfigUtil.get(stationId, StationConfig.member_dynamic_rate_max_num);
		if(StringUtil.isNotEmpty(rebateNum)) {
			if(new BigDecimal(rebateNum).compareTo(new BigDecimal(maxAgentRebate))==1) {
				throw new GenericException("超过了最大返点数限制");
			}
		}
		if(StringUtil.isNotEmpty(dynamicRate)) {
			if(new BigDecimal(dynamicRate).compareTo(new BigDecimal(maxDynRate))==1) {
				throw new GenericException("超过了最大动态赔率限制");
			}
		}
		
		//把会员踢下线
		AccountVo avo = new AccountVo();
		avo.setId(accountId);
		avo.setOnline(SysAccount.ONLINE_FLAG_OFF);
		sysAccountDao.updAccountByVo(avo);
		OnlineManager.forcedOffLine(sysAccount);
		
		//修改账号类型
		sysAccountDao.updateAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT, stationId, accountId, new Date());
		
		//往代理返点点插入新的记录  预设为0
		ProxyMultiRebateSetting pmrs = new ProxyMultiRebateSetting();
		pmrs.setAccountId(accountId);
		pmrs.setGameShare(BigDecimal.ZERO);
		pmrs.setProfitShare(BigDecimal.ZERO);
		pmrs.setFlagActive(sysAccount.getFlagActive());
		pmrsDao.HandldInsertPMRS(pmrs);
		
		//修改动态赔率返点字段 还是原来的赔率值
		String dynonoff = StationConfigUtil.get(stationId, StationConfig.onoff_member_rate_random);
		BigDecimal rate = BigDecimal.ZERO;
		if("on".equals(dynonoff) && StringUtil.isNotEmpty(dynamicRate)) {
			rate=new BigDecimal(dynamicRate);
		}
		sysAccountDao.updataRate(rate, stationId, accountId);
		
		//删除会员缓存
		RedisAPI.delCache(0, "sysAccount_"+accountId.toString());
		
		//写入日志
		SysLogUtil.log("操作员："+UserUtil.getUserAccount()+",操作内容：会员转代理   站点编号为"+stationDao.get(stationId).getFloder()+" 会员账号为"+sysAccount.getAccount(), LogType.AGENT_MEMBER);
	}

	@Override
	public void updateOfflineStatus(Long StationId) {
		// TODO Auto-generated method stub
		sysAccountDao.updOfflineAll(StationId);
	}

	@Override
	public void updateUserOnline(Long accountId) {
		// TODO Auto-generated method stub
		sysAccountDao.updateUserOnline(accountId);
	}
	@Override
	public void updateUserLastLoginIp(Long accountId,String lastLoginIp) {
		// TODO Auto-generated method stub
		sysAccountDao.updateUserLastLoginIp(accountId, lastLoginIp);
	}

	@Override
	public SysAccount registerByThird(Map<String, String> atMap, String json, Long platform) {
		// 目前没有推广连接，所有注册会员都归到 默认代理下。
		Long agentId = 0l;
		Long stationId = Long.valueOf(atMap.get("stationId"));
		SysStation station = stationDao.findOneById(stationId);
		if(station==null) {
			throw new GenericException("站点不存在");
		}
		if (StringUtil.equals(platform, SysRegisterConfig.TYPE_MEMBER)) {
			agentId = station.getAgentId();
		}
		String pwd = atMap.get("password") == null ? "" : atMap.get("password").toString();
		AccountVo regMemVo = JsonUtil.toBean(json, AccountVo.class);
		regMemVo.setPwd(pwd);
		regMemVo.setRpwd(pwd);
		regMemVo.setStationId(stationId);
		regMemVo.setStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
		String ip = UserUtil.getIpAddress();
		String sessionKeyType = "";
		if (StringUtil.equals(platform, SysRegisterConfig.TYPE_MEMBER)) {
			regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_MEMBER);
			sessionKeyType = SystemConfig.SESSION_MEMBER_KEY;
		} else if (StringUtil.equals(platform, SysRegisterConfig.TYPE_AGENT)) {
			if (!StringUtils.equals("off", StationConfigUtil.get(StationConfig.onoff_register_daili_checked))) {
				regMemVo.setStatus(SysAccount.ACCOUNT_STATUS_REVIEW);
			}
			regMemVo.setAccountType(SysAccount.ACCOUNT_PLATFORM_AGENT);
			sessionKeyType = SystemConfig.SESSION_DAILI_KEY;
		} else {
			throw new GenericException("非法请求！");
		}
		regMemVo.setAgentId(agentId);
		regMemVo.setLastLoginIp(ip);
		// 保存用户
		SysAccount sa = saveAccount(regMemVo);
		SysAccountInfo accountInfo= sysAccountInfoDao.get(sa.getId());
		if (sa.getAccountStatus().longValue() == SysAccount.ACCOUNT_STATUS_ENABLED) {
			updateApiAccountLoginStatus(sa,accountInfo, sessionKeyType);
		}
		return sa;
	}



	@Override
	public SysAccount doLoginByThird(String account, String pwd, Long stationId) {
		HttpServletRequest request = SysUtil.getRequest();
		HttpSession session = request.getSession();
//		int failed = StringUtil.toInt(session.getAttribute("LOGIN_MEMBER_FAILED_COUNT"));
		Long type = SysAccount.ACCOUNT_PLATFORM_MEMBER;
		SysAccount member = sysAccountDao.getLoginAccount(account.trim().toLowerCase(), type,stationId);
		if (member == null) {
//			failed++;
//			session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
			throw new GenericException("账号不存在");
		}
		if (StringUtil.equals(member.getAccountStatus(), SysAccount.ACCOUNT_STATUS_DISABLED)) {
//			if(failed>4){
//				throw new GenericException("您的账号存在可疑操作已被系统自动封禁，请联系客服！");
//			}
			throw new GenericException("该账户存在异常，请联系客服");
		}
		if (StringUtil.equals(member.getAccountStatus(), SysAccount.ACCOUNT_STATUS_REVIEW)) {
			throw new GenericException("账户处于审核状态，还不能登录。");
		}
		SysAccountInfo memberInfo = sysAccountInfoDao.get(member.getId());
		String userPwd = member.getPassword();
		String newPwd = MD5Util.getMD5String(account, pwd);
		String oldPwd = MD5Util.getOldMD5String(account, pwd);
		
		String userSessionKey = SystemConfig.SESSION_MEMBER_KEY;
		Long accountType = member.getAccountType();
		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT) 
				|| StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_GENERAL)) {
			userSessionKey = SystemConfig.SESSION_DAILI_KEY;
		}else if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_SUPER) 
				|| StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT_MANAGER)) {
			userSessionKey = SystemConfig.SESSION_AGENT_KEY;
		}
//		if(failed > 4){
//			//密码错误过多会员端自动封禁账号 由于异常机制返回null跳出本次流程
//			String string = StationConfigUtil.get(stationId, StationConfig.onoff_login_sys_Auto_ban);
//			if("on".equals(string)){
//				this.loginDisableAccount(member);
//				return null;
//			}else{
//				throw new GenericException("密码错误次数过多，请稍后再试");
//			}
//		}
		if (StringUtil.isNotEmpty(userPwd) && userPwd.indexOf(Contants.OLD_PASSWORD_PREFIX) > -1) {
			userPwd = userPwd.substring(Contants.OLD_PASSWORD_PREFIX.length(), userPwd.length());
			if (!userPwd.equals(Sha256Util.digest(pwd))) {
//				failed++;
//				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
			session.setAttribute(userSessionKey + "repwdFlag", true);
		} else if (StringUtil.startWith(userPwd, Contants.PHP_PASSWORD_PREFIX)) {
			userPwd = userPwd.substring(Contants.PHP_PASSWORD_PREFIX.length());
			if (!userPwd.equals(MD5Util.MD5(pwd).toLowerCase())) {
//				failed++;
//				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
			session.setAttribute(userSessionKey + "repwdFlag", true);
		} else {
			if (!newPwd.equals(StringUtil.toUpperCase(userPwd)) && !oldPwd.equals(StringUtil.toUpperCase(userPwd))) {
//				failed++;
//				session.setAttribute("LOGIN_MEMBER_FAILED_COUNT",failed);
				throw new GenericException("账户或者密码错误");
			}
		}
		updateApiAccountLoginStatus(member,memberInfo, userSessionKey);
		return member;
	}
	
	public void updateApiAccountLoginStatus(SysAccount member,SysAccountInfo memberInfo, String userSessionKey) {
		OnlineManager.doOnline(SysUtil.getRequest(), SysUtil.getSession(), userSessionKey, member,memberInfo, UserUtil.getIpAddress());
		SysLogUtil.loginLog("自动登录!", member.getId(), member.getAccount(),member.getStationId());
	}

	@Override
	public String updateAccountImChatId(String content, String imChatId, Long stationId) {
		if (StringUtil.isEmpty(content)) {
			throw new GenericException("成员为空");
		}

		String[] sonAccounts = content.split(",| |\n");
		if (sonAccounts == null || sonAccounts.length == 0) {
			throw new GenericException("成员为空");
		}
		int success[] = sysAccountDao.batchUpdateImChatId( sonAccounts, imChatId,stationId);
		String failedAccount = "";
		String successAccount = "";
		for (int i = 0; i < success.length; i++) {
			if (success[i] == 0) {
				if (StringUtil.isNotEmpty(failedAccount)) {
					failedAccount += ",";
				}
				failedAccount += sonAccounts[i];
			}else if(success[i] == 1){
				if (StringUtil.isNotEmpty(successAccount)) {
					successAccount += ",";
				}
				successAccount += sonAccounts[i];
			}
		}
		if (StringUtil.isNotEmpty(failedAccount)) {
			failedAccount += "为导入失败的成员";
		}
		Map result = MixUtil.newHashMap("failed",failedAccount,"success",successAccount);
		return JSONObject.toJSONString(result);
	}

	@Override
	public String getAccountStrByChatId(String imChatId, Long stationId) {
		return sysAccountDao.getAccountStrByImChatId(imChatId,stationId);
	}

	@Override
	public void warningRevise(Integer flag, Long id,String account) {
		sysAccountDao.warningRevise(flag,id);
		String redisKey = "warning_count:::"+StationUtil.getStationId();
		int db = CacheManager.getCacheConifg(CacheType.AUTO_HANDLER_ACCOUNT).getSelectDb();
		RedisAPI.delCache(db,redisKey);
		String opt = "取消";
		if(flag==1){
			opt = "设置";
		}
		SysLogUtil.log(opt+"告警用户，账号为："+account,LogType.AGENT_MEMBER);
	}

	@Override
	public Integer getWarningCount(long stationId) {
		return sysAccountDao.getWarningCount(stationId);
	}

	
	@Override
	public List<Long> queryAccountIdsByType(Long stationId,Long... type){
		return sysAccountDao.queryAccountIdsByType(stationId, type);
	}
	
	@Override
	public void saveRemark(Long accountId, String remark){
		if(Validator.isNull(accountId)) {
			throw new GenericException("会员id为空");
		}
		sysAccountDao.saveRemark(accountId, remark);
		SysLogUtil.log("修改用户备注："+remark,LogType.AGENT_MEMBER);
	}
}
