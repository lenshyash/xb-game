package com.game.service.impl;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.constant.LogType;
import com.game.dao.platform.AgentActivityDao;
import com.game.model.platform.AgentActivity;
import com.game.service.AgentActivityService;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;

@Repository
public class AgentActivityServiceImpl implements AgentActivityService {
	@Autowired
	private AgentActivityDao agentActivityDao;

	@Override
	public Page page(Long stationId) {
		if (stationId == null) {
			throw new GenericException("参数错误");
		}
		return agentActivityDao.page(stationId);
	}

	@Override
	public void openCloseH(Integer modelStatus, Long id, Long stationId) {
		if (id == null || id <= 0 || modelStatus == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentActivityDao.getStationId(id);
		if (!stationId.equals(staId)) {
			throw new GenericException("站点错误！");
		}
		agentActivityDao.openCloseH(modelStatus, id, stationId);
	}

	@Override
	public void openClosek(Integer status, Long id, Long stationId) {
		if (id == null || id <= 0 || status == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentActivityDao.openCloseK(status, id, stationId);
	}

	@Override
	public void addSave(AgentActivity aaty) {
		if (aaty.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		agentActivityDao.save(aaty);
	}

	@Override
	public void eidtSave(AgentActivity aaty) {
		if (aaty.getId() == null || aaty.getStationId() == null) {
			throw new GenericException("参数不正确!");
		}
		Long staId = agentActivityDao.getStationId(aaty.getId());
		if (!aaty.getStationId().equals(staId)) {
			throw new GenericException("站点错误！");
		}
		AgentActivity nAaty = agentActivityDao.get(aaty.getId());
		nAaty.setTitle(aaty.getTitle());
		nAaty.setContent(aaty.getContent());
		nAaty.setTitleImgUrl(aaty.getTitleImgUrl());
		nAaty.setUpdateTime(aaty.getUpdateTime());
		nAaty.setOverTime(aaty.getOverTime());
		nAaty.setModelStatus(aaty.getModelStatus());
		nAaty.setPaiXu(aaty.getPaiXu());
		nAaty.setDomainId(aaty.getDomainId());
		nAaty.setFolderCode(aaty.getFolderCode());
		nAaty.setApplyFlag(aaty.getApplyFlag());
		nAaty.setApplySelected(aaty.getApplySelected());
		agentActivityDao.update(nAaty);
	}

	@Override
	public void delete(Long id, Long stationId) {
		if (id == null || stationId == null) {
			throw new GenericException("参数不正确!");
		}
		agentActivityDao.delete(id, stationId);
		SysLogUtil.log("删除优惠活动：" +id,LogType.AGENT_SYSTEM);
	}

	@Override
	public AgentActivity getOne(Long id, Long stationId) {
		if (id != null) {
			AgentActivity a = agentActivityDao.get(id);
			if (a != null && (stationId == null || a.getStationId().equals(stationId))) {
				return a;
			}
		}
		return null;
	}

}
