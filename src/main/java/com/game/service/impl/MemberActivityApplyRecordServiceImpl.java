package com.game.service.impl;

import java.util.Date;

import org.jay.frame.exception.GenericException;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.game.dao.SysAccountDao;
import com.game.dao.MemberActivityApplyRecordDao;
import com.game.model.MemberActivityApplyRecord;
import com.game.model.SysAccount;
import com.game.model.platform.AgentActivity;
import com.game.service.AgentActivityService;
import com.game.service.MemberActivityApplyRecordService;
import com.game.util.DateUtil;
import com.game.util.StationUtil;

@Service
public class MemberActivityApplyRecordServiceImpl implements MemberActivityApplyRecordService {

	@Autowired
	private MemberActivityApplyRecordDao applyRecordDao;
	@Autowired
	private AgentActivityService agentActivityService;
	@Autowired
	private SysAccountDao sysAccountDao;
	
	@Override
	public void addRecord(Long activityId,String option,Long accountId) {
		Long stationId = StationUtil.getStationId();
		AgentActivity activity = agentActivityService.getOne(activityId, stationId);
		SysAccount account = sysAccountDao.get(accountId);
		
		if(activity==null) {
			throw new GenericException("优惠活动已删除，请刷新重试");
		}
		if(!StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_MEMBER) && !StringUtil.equals(account.getAccountType(), SysAccount.ACCOUNT_PLATFORM_AGENT)) {
			throw new GenericException("会员类型错误");
		}
		if(!StringUtil.equals(activity.getStationId(), account.getStationId())) {
			throw new GenericException("站点ID不匹配，请刷新页面重试");
		}
		//没有开启自助申请
		if(!StringUtil.equals(activity.getApplyFlag(), AgentActivity.APPLY_FLAG_ON)) {
			throw new GenericException("优惠活动申请尚未开启");
		}
		if(StringUtil.isEmpty(activity.getApplySelected()) || !activity.getApplySelected().contains(option)) {
			throw new GenericException("优惠活动申请选项错误");
		}
		//优惠活动已经关闭
		if(!StringUtil.equals(activity.getModelStatus(), 2)) {
			throw new GenericException("优惠活动已关闭，请刷新重试");
		}
		Date curDatetime = new Date();
		if(curDatetime.before(activity.getUpdateTime()) || curDatetime.after(activity.getOverTime())) {
			throw new GenericException("请在优惠活动期间参与！");
		}
		MemberActivityApplyRecord applyRecord = new MemberActivityApplyRecord();
		applyRecord.setAccount(account.getAccount());
		applyRecord.setAccountId(account.getId());
		applyRecord.setActivityId(activity.getId());
		applyRecord.setActivityName(activity.getTitle());
		applyRecord.setActivityOption(option);
		applyRecord.setCreateTime(curDatetime);
		applyRecord.setStationId(stationId);
		//申请的状态是未处理
		applyRecord.setStatus(MemberActivityApplyRecord.STATUS_UNTREATED);
		applyRecordDao.save(applyRecord);
		
	}

	@Override
	public Page<MemberActivityApplyRecord> getPage(Long stationId, String account, Date begin, Date end) {
		return applyRecordDao.getPage(stationId, account, begin, end);
	}

	@Override
	public MemberActivityApplyRecord getOne(Long id) {
		return applyRecordDao.get(id);
	}

	@Override
	public void handlerRecord(Long id, Long stationId, Long status, String remark) {
		MemberActivityApplyRecord record = applyRecordDao.get(id);
		if(!StringUtil.equals(record.getStatus(), MemberActivityApplyRecord.STATUS_UNTREATED)) {
			throw new GenericException("此条记录已被处理，请刷新页面");
		}
		record.setStatus(status);
		record.setRemark(remark);
		applyRecordDao.save(record);
	}
	
}
