package com.game.service.impl;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.cache.DataReader;
import com.game.constant.LogType;
import com.game.dao.AdminMenuDao;
import com.game.dao.SysAccountVirtualAddressDao;
import com.game.model.AdminMenu;
import com.game.model.AdminUser;
import com.game.model.SysAccountVirtualAddress;
import com.game.model.vo.AdminMenuNode;
import com.game.service.AdminMenuService;
import com.game.service.SysAccountVirtualAddressService;
import com.game.util.SysLogUtil;
import com.game.util.UserUtil;
import org.jay.frame.jdbc.Page;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SysAccountVirtualAddressImpl implements SysAccountVirtualAddressService {

	@Autowired
	private SysAccountVirtualAddressDao virtualAddressDao;


	@Override
	public Page getPage() {
		return null;
	}

	@Override
	public SysAccountVirtualAddress getAddressByAccount(Long userId, Long stationId) {
		return virtualAddressDao.getAddressByAccount(userId,stationId);
	}

	@Override
	public void save(SysAccountVirtualAddress newVa) {
		virtualAddressDao.save(newVa);
	}

	@Override
	public SysAccountVirtualAddress getByAddress(String toAddress) {
		return virtualAddressDao.getByAddress(toAddress);
	}
}
