package com.game.service.impl;

import org.jay.frame.jdbc.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.game.dao.AgentOnlinepayLogDao;
import com.game.model.AgentOnlinepayLog;
import com.game.model.vo.OnlinepayLogVo;
import com.game.service.AgentOnlinepayLogService;

@Repository
public class AgentOnlinepayLogServiceImpl implements AgentOnlinepayLogService {

	@Autowired
	AgentOnlinepayLogDao agentOnlinepayLogDao;

	@Override
	public Page getPage(OnlinepayLogVo onlinepayLogVo) {
		return agentOnlinepayLogDao.getPage(onlinepayLogVo);
	}

	@Override
	public void save(AgentOnlinepayLog model) {
		agentOnlinepayLogDao.save(model);
	}

	@Override
	public AgentOnlinepayLog getOnline(long payId) {
		return agentOnlinepayLogDao.get(payId);
	}
}
