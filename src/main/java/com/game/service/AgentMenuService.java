package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentMenu;
import com.game.model.AgentMenuGroup;
import com.game.model.vo.AgentMenuNode;
import com.game.model.vo.MenuVo;

public interface AgentMenuService {

	/**
	 * 读取到三级按钮
	 * 
	 * @param groupId
	 * @return
	 */
	public AgentMenuNode getAllLevelThirdMenu();

	public Page<Map> getMenuPage(Long parentId);

	/**
	 * 读取用户组，下面的所有权限菜单
	 * 
	 * @param groupId
	 * @return
	 */
	public List<AgentMenu> getGroupPermissionMenu(Long groupId,Long stationId);

	public void saveMenu(AgentMenu menu);

	public AgentMenuNode getStationMenus();

	public List<MenuVo> getNavMenuVo(Long groupId);

	public List<AgentMenuGroup> getAgentPermission(Long agentId, Long stationId);

	public void saveAgentPermission(Long agentId, Long stationId, String ids);
}
