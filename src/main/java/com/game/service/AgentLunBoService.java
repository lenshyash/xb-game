package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentLunBo;

public interface AgentLunBoService {

	Page page(Long stationId);

	void openCloseH(Integer modelStatus, Long id, Long stationId);

	void openClosek(Integer status, Long id, Long stationId);

	void addSave(AgentLunBo alb);

	void eidtSave(AgentLunBo alb);

	void delete(Long id, Long stationId);

	AgentLunBo getOne(Long id, Long stationId);

}
