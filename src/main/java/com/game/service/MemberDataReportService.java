package com.game.service;

import com.game.model.vo.MemberDataVo;

public interface MemberDataReportService {

	MemberDataVo getMemberData(String account, Long stationId, String begin, String end);

}
