package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberWhiteIp;

public interface MemberWhiteIpService {

	public void save(MemberWhiteIp frontWhiteIp);

	public void deleteIp(long ipId);

	public void updateStatus(long ipId, long status);

	public Page queryPage(Long type);
	
	public java.util.List<MemberWhiteIp> getStationWhiteIpList(Long type);
}
