package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysStationDomain;
import com.game.model.vo.StationVo;

public interface SysStationDomainService {
	public Page<Map> getPage(StationVo svo);

	public void saveDomain(SysStationDomain domain);

	public void updStatus(Long stationId, Long status);
	
	public void updateIsRegSwitch(Long domainId, Long isRegSwitch);
	
	public void updateIsXlSwitch(Long domainId, Long isXlSwitch);
	
	public void updateIsChatSwitch(Long domainId, Long isChatSwitch);
	
	public void updateDomainChatToken(Long domainId, String domainChatToken);
	
	public void deleteDomain(Long domainId);
	
	/**
	 * 供给前端线路检测使用
	 * @return
	 */
	List<String> getDomain(Long domainId);

	public List<Map<String,Object>> getAll();

	public Page<SysStationDomain> getPageForAgent(String domain, String agentName, Long stationId);

	public SysStationDomain findOne(Long id, Long stationId);

	public void updStatusForAgent(Long id, Long status, Long stationId);

	public void delete(Long id, Long stationId);

	public void updateDomainForAgent(Long id, String domain, String agentName, String defaultHome,Long stationId);

	public String saveDomain(String domainNames, String agentName, String defaultHome, Long stationId);
	
	/**
	 * 获取当前站点所有的带模板域名
	 */
	public List<SysStationDomain> getFolderLstDomain();
	/**
	 * 获取当前站点所有的未配置app链接的模板域名
	 */
	public List<SysStationDomain> getFilterFolderList();

	public SysStationDomain getDomainByFolder(String domain,String domainFolder, Long stationId);

	public  List<SysStationDomain> getFolderListByStation(Long stationId);
	
	public  List<SysStationDomain> getListByStationType(Long stationId,Long type);
	
	public List<SysStationDomain> getDomainList(Long stationId) ;
	
	public void deleteDomainByStationId(Long stationId);

	SysStationDomain getAccessDomain(Long stationId);

    void updateInterfaceLimit(Long domainId, Long interfaceLimit);

	void saveRemark(Long id, String remark);
}
