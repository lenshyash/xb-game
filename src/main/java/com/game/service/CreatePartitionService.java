package com.game.service;

import java.util.Date;

public interface CreatePartitionService {

	void createLotteryPartition(Date date,String type);

	void createProxyMultiRebateRecordPartition(Date date);

//	void createMemberRollBackRecordPartition(Date date);

	void createMemberBackwaterRecordPartition(Date date);

	void createBetNumRecordPartition(Date date);

}
