package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyScoreRecord;
import com.game.model.vo.MnyScoreReocrdVo;
import com.game.model.vo.MnyScoreVo;

public interface MnyScoreService {

	public Page getScoreReocrd(MnyScoreReocrdVo msvo);

	/**
	 * 金额变动
	 */
	public MnyScoreRecord updScoreAndRecord(MnyScoreVo scoreVo);

	public void operatScore(Integer type, String accountName, Integer score,String remark);
}
