package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jay.frame.jdbc.Page;

import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.model.vo.AccountVo;
import com.game.user.online.OnlineInfo;

public interface SysAccountService {
	public Page<Map> getPage(AccountVo avo);

	public SysAccount saveAccount(AccountVo accountVo);
	
	public SysAccount saveAccount(AccountVo accountVo,Long methodType);

	public void updStatus(SysAccount account);

	public SysAccount register(Map atMap, String json, Long platform);

	public void updPwd(Long id, String pwd, String rpwd);

	public void updCurUserPwd(String opwd, String pwd, String rpwd, Long pwdType);

	public void updRepPwd(Long id, String pwd, String rpwd);

	public boolean checkBankCard(Long userId, String bankCard);
	
	public String batchMoveAgent(Long stationId,String agentName, String accounts,Long accountType);

	public void updPwdByMember(String opwd, String pwd, String rpwd, Long updType);

	public void updRepwdByAccount(String pwd, String rpwd);

	public List<SysAccount> getAccountsByType(Long type);

	public SysAccount doLoginForAgent(String account, String pwd,String otpPwd, String verifyCode,String googleCode);

	public SysAccount doLoginForDaili(String account, String pwd, String verifyCode);

	public SysAccount doLoginForMember(String account, String pwd, String verifyCode);

	public SysAccount doLoginForMobile(String account, String pwd);
	
	public SysAccount doLoginByThird(String account, String pwd,Long stationId);
	
	public SysAccount doLoginForMobile(String account, String pwd, String verifyCode,String autoLogin);

	public SysAccount doLogin(String account, String pwd, String verifyCode, long type);
	
	public SysAccount doLogin(String account, String pwd, String verifyCode, long type,String autoLogin);

	public Page<Map> getAccountPage(AccountVo accountVo);
	
	public Page<Map> getOnlineAccountPage(AccountVo accountVo);
	

	public Page<Map> getAccountRiskPage(AccountVo accountVo);
	
	public Page<Map> getConversionRiskPage(AccountVo accountVo);

	public Page<Map> getAccountPage4DaiLi(AccountVo accountVo);

	public SysAccount doLoginOut();

	public Map getAccountById(Long memberId, Long staionId);
	
	public Map getAccountAndDrawComById(Long memberId, Long staionId);

	public Page<SysAccount> getStationAgentList(AccountVo accountVo);

	public void saveAgentUser(SysAccount user, String pwd, String rpwd);

	public void delAgentUser(Long id);

	public SysAccountInfo getAccountInfo(Long id);

	public void saveBankInfo(AccountVo avo, boolean flag);

	public Map getAgentMultiData(AccountVo avo);
	public Map getAgentMultiData(String account);
	public Map getAgentMultiData(Long accountId);

	public Map loadOnlineMember(Long memberId);

	public SysAccount findOneByAccountAndStationId(String account, Long stationId);
	
	public SysAccount queryDdgd(String account, Long type,Long stationId);

	public void updateRebateNum(Long updAtId, BigDecimal rebateNum, BigDecimal profitShare ,BigDecimal dynamicRate);

	public void login4Card(String code);

	public void forcedOffLine(Long userId);

	public Integer getOnlineCount();

	public void accountDrawNeedOpe(Long accountId, BigDecimal drawNeed, Long opeType, String remark);

	public boolean userNameExits(String userName);

	public boolean userAccountExits(String userName);

	public void saveAccountUserName(SysAccountInfo info);

	/**
	 * 为代理指定总代
	 * 
	 * @param accountId
	 * @param generalAgent
	 */
	public void setGeneralAgent(Long accountId, String generalAgent);

	/**
	 * 去除代理指定的总代
	 * 
	 * @param agentId
	 */
	public void delGeneralAgent(Long agentId);

	public Map queryAccount(String account, Long stationId);

	/**
	 * 只返回部分属性值，常变的属性没有返回
	 * 
	 * @param accountId
	 * @param stationId
	 * @return
	 */
	public SysAccount getFromCache(Long accountId, Long stationId);

	public SysAccount getOne(Long accountId, Long stationId);
	
	public void updateAccountLoginStatus(SysAccount member,SysAccountInfo memberInfo, String userSessionKey,String msg);
	
	public void updateAccountLoginStatus(SysAccount member,SysAccountInfo memberInfo, String userSessionKey);

	/**
	 * 注册成为测试用户
	 */
	public String registerTestGuest();

	/**
	 *  注册成为试玩用户--原生手机接口
	 * @return
	 */
	public SysAccount registerTestGuestForNative();

	public void updateGroup(Long id,Long stationId, Long groupId ,String remark);

	public void clearGuestTestData(Date time);

	public Map<Long, Map> countStation(String startTime, Date endTime);

	public void saveOnlineInfo(List<OnlineInfo> oiList);

	public void updateOnlineStatus(Set<Long> ids);

	public void updateUserOffline(Long accountId);

	public void updateAllOffline();

	public void scoreToZero(Long stationId);
	/**
	 * 检查是否是子集账号
	 * @param parAccount 父级代理账号
	 * @param subAccount 当前账号
	 * @param stationId 站点ID
	 * @return
	 */
	public int checkSubAccount(String parAccount, String subAccount, Long stationId);
	/**
	 * 批量调整用户指定聊天室token
	 * @param content
	 * @param token
	 * @return
	 */
	public String updateAccountChatToken(String content, String token,Long stationId,String chatName);
	/**
	 * 获取指定聊天室账号集
	 * @param content
	 * @param token
	 * @return
	 */
	public String getAccountStrByChat(String token,Long stationId);
	
	/**
	 * 批量调整用户新聊天室ID
	 * @param content
	 * @param token
	 * @return
	 */
	public String updateAccountImChatId(String content, String imChatId,Long stationId);
	/**
	 * 获取指定新聊天室账号集
	 * @param content
	 * @param token
	 * @return
	 */
	public String getAccountStrByChatId(String imChatId,Long stationId);
	
	
	/**
	 * 获取会员余额宝列表
	 * @param account
	 * @param stationId
	 * @return
	 */
	public<Map> Page  getBalanceGemPage(AccountVo acc);
	
	public List<SysAccount> getAllDownMember(String account,String children,Long stationId);
	
	/**
	 * 删除指定用户
	 * @param id
	 */
	public void deleteById(Long id);

	public int updateAccountHeadUrl(Long userId, String headUrl);
	
	public void memberChangeDaili(Long accountId,String rebateNum,String dynamicRate);

	public void updateOfflineStatus(Long stationId);

	public void updateUserOnline(Long accountId);

	public SysAccount registerByThird(Map<String, String> params, String paramJson, Long platFormFlag);

	void warningRevise(Integer flag, Long id,String account);

	Integer getWarningCount(long stationId);

	void updateUserLastLoginIp(Long accountId, String lastLoginIp);

	List<Long> queryAccountIdsByType(Long stationId, Long[] type);

	void saveRemark(Long accountId, String remark);
}
