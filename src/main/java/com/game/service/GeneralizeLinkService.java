package com.game.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jay.frame.jdbc.Page;

import com.game.constant.StationConfig;
import com.game.model.platform.MemberGenrtLink;

public interface GeneralizeLinkService {
	
	public Page page(Long stationId,HttpServletRequest request,String userAccount,Integer type , String linkKey);
	
	/**
	 * 根据ID更改状态
	 */
	public void updateStatusById(Long id , Integer status , Long stationId);
	
	/**
	 * 得到5位相同站点不重复的随机字符串
	 */
	public String getRandCode(Long stationId);
	
	public MemberGenrtLink getByStationIdAndLinkCode(Long stationId,String linkCode);
	
	/**
	 * 保存或更新
	 */
	public void updateOrSave(MemberGenrtLink mgl);
	
	/**
	 * 根据id删除
	 */
	public void del(Long id,Long stationId);
	/**
	 * 获取用户默认链接
	 * @param stationId
	 * @param accountId
	 * @param defaultLink
	 * @return
	 */
	public List<MemberGenrtLink> getDefaultLink(Long stationId , Long accountId, Long defaultLink);
	
	public void updateDefaultById(Long id, Long DefaultLink, Long stationId);
}
