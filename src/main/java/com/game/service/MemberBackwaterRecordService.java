package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.vo.BackwaterParam;

public interface MemberBackwaterRecordService {
	public List<MemberBackwaterRecord> collectLotteryDayBetMoney(Date start, Date end, List<Long> sIdList);
	
	public List<MemberBackwaterRecord> collectSysLotteryDayBetMoney(Date start, Date end, List<Long> sIdList);

	public List<MemberBackwaterRecord> collectSportDayBetMoney(Date start, Date end, List<Long> sIdList);

	public List<MemberBackwaterRecord> collectMarkSixDayBetMoney(Date time, Date end, List<Long> sIdList);

	public MemberBackwaterRecord findOne(Date betDate, String account, Integer betType,Long stationId,Long roomId);

	public void saveAll(List<MemberBackwaterRecord> all);

	public Page page(String account, Date startDate, Date endDate, Integer betType, Integer status, Long stationId,Long roomId,Long reportType);
	
	public Page page(BackwaterParam param);

	public int cancel(Integer betType, String account, String betDate, Long stationId,Long roomId);

	public void manualRollback(Integer betType, String account, String betDate, Long stationId,Long roomId, BigDecimal money);

	public void doBackwaterMoneyByBatch(String[] moneys, Long stationId, Date startDate, Date endDate);

	public List<MemberBackwaterRecord> collectLotteryV4DayBetMoney(Date start, Date end, List<Long> sIdList);
	
	public void cashingBackwater(Long id);
	public void autoExpireStatus(Long stationId) ;
}
