package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.SysStation;

public interface ProxyDailyBettingStatisticsService {

//	void collectDailyAmount(Date startDate, Date endDate, List<SysStation> stationList);

	Page page(Date startDate, Date endDate, String account, Long stationId,Integer status,Integer searchType);

	String toProfit(String pointIds, Long accountId, Long stationId, BigDecimal lotteryPoint, BigDecimal markSixPoint, BigDecimal realPoint, BigDecimal egamePoint, BigDecimal sportsPoint, BigDecimal chessPoint,BigDecimal esportsPoint);

	void collectDailyAmount2(Date startDate, Date endDate, List<Long> sIdList);

}
