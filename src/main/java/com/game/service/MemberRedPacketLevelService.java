package com.game.service;

import java.util.List;
import java.util.Set;

import com.game.model.MemberRedPacketLevel;

public interface MemberRedPacketLevelService {

	void batchInsert(List<MemberRedPacketLevel> llist,Set<Long> groupLevelSet);

	void deleteByRedPacketId(Long redPacketId);

	Set<Long> findLevelsByRedPacketId(Long redPacketId);

}
