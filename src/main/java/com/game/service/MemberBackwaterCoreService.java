package com.game.service;

import java.math.BigDecimal;

import com.game.model.platform.MemberBackwaterRecord;
import com.game.model.platform.MemberBackwaterStrategy;

public interface MemberBackwaterCoreService {

	int cancel(MemberBackwaterRecord r);

	void manualRollbackOne(MemberBackwaterRecord r, Long stationId, BigDecimal money);

	MemberBackwaterStrategy filterBackwaterStrategy(MemberBackwaterRecord r);
	
	public void backWaterAddMoney(MemberBackwaterRecord r, int oldStatus);
}
