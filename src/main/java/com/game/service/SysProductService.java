package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.SysProduct;
import com.game.model.vo.ProductVo;

public interface SysProductService {

	public Page getPage(ProductVo productVo);
	
	public List getCombo(ProductVo productVo);

	public void saveProduct(SysProduct product);

	public void delProduct(ProductVo productVo);

	public SysProduct getOne(Long id, Long stationId);

}
