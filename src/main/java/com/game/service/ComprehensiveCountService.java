package com.game.service;

import java.util.Map;

/**
 * 团队统计接口
 * 
 * @author admin
 *
 */
public interface ComprehensiveCountService {

	public Map getComprehensiveCountMap(Long stationId);

	public void executeStatistics();

	public void statsStations(Long stationId, String startDate, String endDate);
}
