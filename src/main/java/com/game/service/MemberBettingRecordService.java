package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.vo.LotteryParamVo;
import com.game.model.vo.SearchRecordParam;

public interface MemberBettingRecordService {
	
	public Page getSportBettingRecord(SearchRecordParam params);

	public Page getLotteryBettingRecord(LotteryParamVo params);
}
