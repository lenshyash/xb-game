package com.game.service;

import java.util.List;

import com.game.model.AdminGroupMenu;
import com.game.model.AgentMenuGroup;
import com.game.model.vo.AgentMenuVo;

public interface AdminPermissionService {

	public List<AdminGroupMenu> getGroupPermission(long groupId);

	public void savePermission(long groupId, List<AdminGroupMenu> menus);
}
