package com.game.service;

import java.math.BigDecimal;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentLotteryModelConfig;

public interface AgentLotteryModelConfigService {

	Page page(Integer type, Long stationId, String name);


	AgentLotteryModelConfig getOne(Long id, Long stationId);


	void save(AgentLotteryModelConfig mrbs,Long stationId);


	AgentLotteryModelConfig findByStationCode(Long stationId, String lotCode);


	

}
