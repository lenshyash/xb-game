package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.ProxyMultiRebateRecord;

public interface ProxyMultiRebateRecordService {
	public Page<ProxyMultiRebateRecord> getPageConfig(ProxyMultiRebateRecord proxyMultiRebateRecord,String startTime,String endTime);

	public int cancleProxyMultiRebate(Long betId,Long stationId);
	
}
