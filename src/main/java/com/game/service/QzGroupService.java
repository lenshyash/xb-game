package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.Qzgroup;
import com.game.model.vo.AccountVo;

public interface QzGroupService {
	public Page<Map> getGroupPage(Long stationId,String groupName);
	
	public Qzgroup getOneGroup(String groupName,Long stationId);
	
	public void moveAccountToGroup(String groupName,String account);
	
	public void save(Qzgroup qzgroup);
	
	public void closeGroup(Long stationId,String groupName);
}
