package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysAccount;
import com.game.model.SysAccountGuideMoney;
import com.game.model.vo.ReportParamVo;
import com.game.model.vo.ReportVo;
import com.game.model.vo.TotalStatisticVo;

public interface SysAccountGuideMoneyService {

	public Page getFinanceReport(ReportParamVo paramVo);

	public Page getRiskReport(ReportParamVo paramVo);

	public ReportVo getGlobalReport(ReportParamVo paramVo);

	public Page<TotalStatisticVo> getTotalStatistic(Long stationId, String account, String agentName, Date startTime, Date endTime,Long searcType, Integer pageNo, Integer pageSize);

	public Map getComprehensiveCountMap(Long stationId);

	public int saveThirdAmount(SysAccountGuideMoney dailyMoney, SysAccount acc);

	public SysAccountGuideMoney findOneByAccountIdAndStatDate(Long accountId, Date date);

	/**
	 * 获取用户上次取款成功后的后台手动处理存款数据
	 * 
	 * @param userId
	 * @return
	 */
	public Map getLastSucDepositHandlerData(Long userId);

	/**
	 * 第三版获取当天彩票和六合彩的中奖派奖
	 * 
	 * @param id
	 * @param date
	 * @return
	 */
	public List<Map> findByLotteryAndMarkWinTotal(Long stationId, Date statDate, Date endDate);

	public List<Map> getGlobalReportForAdmin(Date date, Date tomorrow);

	public Page getDayReport(ReportParamVo paramVo);
	/**
	 * 获取彩票版当日统计记录
	 * @param stationId
	 * @param date
	 * @param account
	 * @return
	 */
	public Map getLotteryAmountCount(Long stationId,String account , Date start,Date end);
	
	public ReportVo getGlobalReportByNative(ReportParamVo paramVo);

	public void synDailiData(Long stationId, String end, String begin);
}
