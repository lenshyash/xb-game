package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentSignRecord;
import com.game.model.AgentSignRule;

public interface SignService {
	
	public Page getRulePage();
	
	public Page getSignRecords();
	
	public void delRule(long id);
	
	public void saveRule(AgentSignRule rule);
	
	/**
	 * 会员签到
	 */
	public AgentSignRecord sign();
	
	/**
	 * 获取当前会员当前月份和上个月的签到信息
	 * @return
	 */
	public List<AgentSignRecord> getMemberSignRecord();
	
	public List<AgentSignRecord> zxqd(Long stationId);

	public AgentSignRule getOne(Long id, Long stationId);
	
	public List<Map<String, String>> signList(String signYear,String signMonth,String signDay,Long userId);

	public  Page<Map> zxqdRecord(Long stationId);
	
}
