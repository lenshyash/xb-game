package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentAdressBook;

public interface AgentAdressBookService {

	Page page(Long stationId,String account);
	
	public void save(AgentAdressBook adressBook);
	
	public int checkUnique(Long stationId,Long accountId, String contactName,String contactPhone);
	
	public void export(Long stationId, String account);
}
