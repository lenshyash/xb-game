package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentLotteryBetLimit;



public interface AgentLotteryBetLimitService {
	public Page getPage(Long stationId);
	
	public void updateStatus(Long id,Long status,Long stationId);
	
	public void save(AgentLotteryBetLimit betLimit);
	
	List<AgentLotteryBetLimit> getActiveList(Long stationId);
	
	public void delete(Long id);
	
	public AgentLotteryBetLimit getOne(Long stationId,String groupName,String code,Long limitType,Long type);
}
