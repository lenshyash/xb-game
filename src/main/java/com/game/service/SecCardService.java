package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.SecCard;
import com.game.model.SysAccount;

public interface SecCardService {
	
	public SecCard createCard();
	
	public SecCard getCard(long id);

	public void bind(String cardNo,long userId,long stationType);
	
	public Page getPage();
	
	public List getAgentUsers(long stationId);
	
	public List<SecCard> getBindCards(long userId);
	
	public void unbind(long cardId,long userId,long stationType);
}
