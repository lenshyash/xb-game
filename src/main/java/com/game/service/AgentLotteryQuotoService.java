package com.game.service;

import java.math.BigDecimal;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentLotteryQuoto;

public interface AgentLotteryQuotoService {

	void updateBuyMoney(Long stationId, BigDecimal money,String type);

	AgentLotteryQuoto getOneByStationId(Long stationId);
	
	public Page<Map> getPage();
	
}
