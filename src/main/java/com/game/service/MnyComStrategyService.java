package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyComStrategy;

public interface MnyComStrategyService {

	Page<MnyComStrategy> getPage(Integer depositType, Integer giftType, Integer valueType, Date begin, Date end,
			Long stationId);

	MnyComStrategy getOne(Long id, Long stationId);

	void delete(Long id, Long stationId);

	void addSave(MnyComStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId);

	void updStatus(Integer status, Long id, Long stationId);

	void update(MnyComStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId);

	MnyComStrategy filter(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId);
	
	List<MnyComStrategy> filterList(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId);

	List<MnyComStrategy> getByType(int type, Long stationId);

}
