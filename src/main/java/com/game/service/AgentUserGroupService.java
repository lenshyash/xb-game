package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentUserGroup;

public interface AgentUserGroupService {
	
	public Page<AgentUserGroup> getGroups();
	
	public List<AgentUserGroup> getGroupCombo(boolean setPerm);
	
	public void saveGroup(AgentUserGroup group);
	
	public void del(Long id);
	
	public AgentUserGroup getGroupById(Long groupId);
	
}
