package com.game.service;

import java.math.BigDecimal;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.lottery.BcLotteryRoom;
import com.game.model.platform.MemberBackwaterStrategy;

public interface MemberBackwaterStrategyService {

	Page page(Integer type, Long stationId);

	void closeOrOpen(Integer status, Long stationId, Long id);

	MemberBackwaterStrategy getOne(Long id, Long stationId);

	void insert(MemberBackwaterStrategy mrbs, Long[] groupLevelId);

	void update(MemberBackwaterStrategy mrbs, Long[] groupLevelId);

	void delete(MemberBackwaterStrategy old);

	BigDecimal getLotteryFanShui(Long stationId);
	
	List<BcLotteryRoom> getRoomStationId(Long stationId);

}
