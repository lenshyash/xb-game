package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.constant.StationConfig;
import com.game.model.AgentBaseConfig;
import com.game.model.AgentBaseConfigGroup;
import com.game.model.AgentBaseConfigValue;

public interface AgentBaseConfigService {
	
	public boolean updateSysConfigValue(StationConfig key,String value);
	
	public AgentBaseConfigValue getConfValByKey(long stationId, String key);
	
	public AgentBaseConfig getSysConfigByKey(String key);

	public Page<AgentBaseConfig> getPageConfig(String name,Long platform,String groupName,Long groupId);

	public Page<AgentBaseConfigGroup> getPageConfigGroup(Long platform,String groupName);

	public List<AgentBaseConfigGroup> getConfigGroupLst(Long platform);

	public List<AgentBaseConfig> getConfigAll();

	public List<AgentBaseConfig> getSysConfigs();

	public List<Map> getStationConfigs(long stationId);
	
	public List<Map> getStationConfigs(long stationId,boolean allStatus,boolean cache);
	
	public void saveAgentConfVals(Long stationId, String ids);

	public List<AgentBaseConfigValue> getConfValsByStationId(long stationId);

	public void saveConfig(AgentBaseConfig abc);
	
	public void saveSysConfigVal(Long id,String value);

	public void saveConfigGroup(AgentBaseConfigGroup cg);

	public void saveConfigValue(Long id,String value,Long stationId);

	public void updateConfigValue(String key, String value, Long stationId);

	public void updStatus(long confId, long status);

	public void delete(long acId);

	public void delGroup(long acId);

	/**
	 * 根据站点配置的参数名获取参数值
	 * 
	 * @param key
	 * @param stationId
	 * @return
	 */
	public String getSettingValueByKey(String key, Long stationId);

	/**
	 * 判断站点是否是独立彩票系统
	 * 
	 * @param stationId
	 * @return
	 */
	boolean isDuLiCaiPiao(Long stationId);
	
	/**
	 * 多级代理开关
	 * @param stationId
	 * @return
	 */
	boolean onoff_multi_agent(Long stationId);

	/**
	 * 开启电子娱乐
	 * 
	 * @param stationId
	 * @return
	 */
	boolean dianZiYouYiHadOpened(Long stationId);

	/**
	 * 开启真人娱乐
	 * 
	 * @param stationId
	 * @return
	 */
	boolean zhenRenYuLeHadOpened(Long stationId);

	/**
	 * 开关是否打开状态
	 * 
	 * @param key
	 * @param stationId
	 */
	boolean isOn(String key, Long stationId);

	public void updateRealMoneyConfig();
	
	public String[] getLotteryGroupSort(Long stationId);
	
	public List<AgentBaseConfigValue> getAllConfValsByStationId(long stationId) ;
	
	public void deleteAgentConfigValByStationId(Long stationId);
}
