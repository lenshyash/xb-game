package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DataConversionService {
	
	public void startMergeDailyData(Date start, Date end,Long stationId);

}
