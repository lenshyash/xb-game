package com.game.service;

import com.game.model.AppUpdate;

import java.util.List;

/**
 * @author johnson
 * app 版本检测service
 */
public interface AppCheckUpdateService {

    /**
     * 获取所有版本更新
     * @return
     */
    public List<AppUpdate> getAppUpdates();

    /**
     * 根据旧版本号获取最新一个版本信息
     * @param version
     * @return
     */
    public List<AppUpdate> getLastUpdateInfo(String version);

    /**
     * 删除版本信息
     * @param version
     */
    public void deleteAppUpdate(String version);
    /**
     * 获取app最新版本
     * @param version
     */
    public AppUpdate getLastAppVerByFlag(String flag,Long stationId,String domain,String type);
    /**
     * 获取app最新版本
     * @param version
     */
    public AppUpdate saveAppUpdate(AppUpdate app);

}
