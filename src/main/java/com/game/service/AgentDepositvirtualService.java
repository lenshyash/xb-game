package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentDepositOnline;
import com.game.model.AgentDepositVirtual;

public interface AgentDepositvirtualService {
	public Page getOnlinePage(String name, Long status);

	public void delOnline(long id);

	public AgentDepositVirtual getOnline(long payId);

	public void updateStatus(Integer status, Long id, Long stationId);

	public void save(AgentDepositVirtual online, Long[] groupLevelIds);

	List<AgentDepositVirtual> getVirtuals(Long status);

    List<Map> getStationOnlines(String showTypeMobile);

    List<Map> getStationOnlines(String showTypeMobile,String code);
}
