package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentWhiteIp;

public interface AgentWhiteIpService {
	
	public void save(AgentWhiteIp agentWhiteIp);
	
	public void deleteIp(long ipId);
	
	public void updateStatus(long ipId,long status);
	
	public Page queryPage();
}
