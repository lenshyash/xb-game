package com.game.service;

import java.util.Set;

public interface MnyComStrategyLevelService {

	Set<Long> getLevelSet(Long strategyId, Long stationId);

}
