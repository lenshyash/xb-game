package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberBalanceGemRecord;
import com.game.model.MemberBalanceGemStrategy;
import com.game.model.SysAccount;
import com.game.model.vo.MnyMoneyVo;

public interface MemberBalanceGemRecordService {
	/**
	 * 获取记录
	 * @param mcrVo
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Page<MemberBalanceGemRecord> getPage(MemberBalanceGemRecord mcrVo,String startTime,String endTime);
	/**
	 * 计算每日盈利
	 * @param start
	 * @param end
	 * @param stationId
	 */
	public void collectBalanceGemRecord(Date start, Date end, Long stationId);
	/**
	 * 转入转出操作
	 * @param money
	 * @param isAdd
	 */
	public void transferBalanceGem(BigDecimal money, boolean isAdd);
	/**
	 * 获取当前会员适用站点策略
	 * @param accountId
	 * @param stationId
	 * @param list
	 * @param lastAmount
	 * @return
	 */
	public MemberBalanceGemStrategy filterByMemberLevelandMoney(Long accountId, Long stationId, List<MemberBalanceGemStrategy> list,BigDecimal lastAmount);
	/**
	 * 计算策略的万分收益
	 * @param strategy
	 * @return
	 */
	public MemberBalanceGemStrategy countTheThousand(MemberBalanceGemStrategy strategy);
	/**
	 * 获取会员昨日余额宝收益
	 * @param time
	 * @param userId
	 * @return
	 */
	public BigDecimal getYesterdayIncome(Date time, Long userId);
}
