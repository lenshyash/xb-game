package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentProfitShareRecord;

public interface AgentProfitShareRecordService {

	public Map findOneMap(Long recordId,Long stationId);

	public Page page(String account, Date startDate, Date endDate, Integer status, Long stationId);
	
	public void saveAll(List<AgentProfitShareRecord> all);
	
}
