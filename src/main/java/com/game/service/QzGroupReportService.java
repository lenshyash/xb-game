package com.game.service;

import java.util.Date;
import java.util.Map;

import org.jay.frame.jdbc.Page;

public interface QzGroupReportService {
	public Page<Map> getGroupPage(Long stationId,String LotCode,String groupName,String qihao,Date begin,Date end);
}
