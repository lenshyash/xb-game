package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentAppKeyConfig;


public interface AgentAppKeyConfigService
{

	Page page(Long stationId);

	void openCloseH(Long id, Long stationId,Long status);

	void addSave(AgentAppKeyConfig apc);

	void eidtSave(AgentAppKeyConfig apc);

	void delete(Long id, Long stationId);
	
	AgentAppKeyConfig getVersionAndStationId(String version,Long stationId);

	AgentAppKeyConfig getOne(Long id, Long stationId);

}
