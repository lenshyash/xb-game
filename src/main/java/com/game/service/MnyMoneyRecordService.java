package com.game.service;

import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyMoneyRecord;
import com.game.model.vo.MnyMoneyRecordVo;

public interface MnyMoneyRecordService {

	public Page<Map> getMoneyRecord(MnyMoneyRecordVo moneyRecordVo);
	
	MnyMoneyRecord getById(Long id);
	
	public MnyMoneyRecord getByOrderId(String orderId,Long stationId);
	
	public Page<Map> getAccountxiMaPage(MnyMoneyRecordVo moneyVo);

}
