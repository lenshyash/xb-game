package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.game.model.BetNumRecord;
import com.game.model.vo.BetNumRecordVo;
import org.jay.frame.jdbc.Page;

public interface BetNumRecordService {

	Page getPage(String account, Date beginDate, Date endDate, Integer type, Long stationId);

	Page getBetNumRecordByApi(BetNumRecordVo recordVo);

    BigDecimal getBetNumByDate(BetNumRecordVo recordVo);

	BetNumRecord getLastOneRecord(BetNumRecordVo recordVo);
}
