package com.game.service;

import java.util.Date;

import org.jay.frame.jdbc.Page;

import com.game.model.vo.TotalStatisticVo;

public interface TotalStatisticReportService {

	public Page<TotalStatisticVo> getTotalStatistic(Long stationId, String account, String agentName, Date startTime, Date endTime,Integer pageNo,Integer pageSize);

}
