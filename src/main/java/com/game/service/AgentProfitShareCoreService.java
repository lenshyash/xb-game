package com.game.service;

import java.util.Date;
import java.util.List;

import com.game.model.SysStation;

public interface AgentProfitShareCoreService {
	public void collectAgentProfitShareRecord(Date start, Date end, Long stationId);
}
