package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.alibaba.fastjson.JSONObject;
import com.game.model.MemberLevelBase;

public interface MemberLevelService {

	public Page<MemberLevelBase> getLevelPage(Long stationId, Integer status);

	public void saveLevel(MemberLevelBase level);

	public void addLevelCount(Long levelId);

	public void subLevelCount(Long levelId);

	public void updLevelStatus(Long levelId, Integer status);

	public List<MemberLevelBase> getStationLevels(Long stationId);

	public void checkDepositForLevel(Long accountId);

	public void levelMove(Long curId, Long nextId);

	public void levelDel(Long id);

	public MemberLevelBase getOne(Long id, Long stationId);

	public void reStatUserNum();

	public void saveDefaultLevel(MemberLevelBase level);
	
	public String importMember(MemberLevelBase level,String content,Long importType);
	
	public JSONObject getCurrentLevelJson();
}
