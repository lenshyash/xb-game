package com.game.service;

import java.util.Date;

import org.jay.frame.jdbc.Page;

import com.game.model.SysMessage;
import com.game.model.vo.MessageVo;

public interface SysMessageService {
	public Page getMessagePage(MessageVo mvo);

	public void updMessage(SysMessage message);

	public void sendMessage(MessageVo mvo);

	public void delMessage(Long messageId);

	public void delUserMessage(Long userMessageId);

	public Integer getMessageCount();

	public void readMessage(Long userMessageId);

	public SysMessage getOne(Long id, Long stationId);
	
	public void sendUserMessage(String title,String content,Long stationId,Long accountId,String account);
	
	public void  delUserNewMessage(Long messageId);
	
	public void deleteSysUserMessageByDate(Date createDatetime);
}
