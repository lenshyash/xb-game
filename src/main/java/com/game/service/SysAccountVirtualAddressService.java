package com.game.service;

import com.game.model.AdminUser;
import com.game.model.SysAccountVirtualAddress;
import org.jay.frame.jdbc.Page;

public interface SysAccountVirtualAddressService {

	public Page getPage();

	public SysAccountVirtualAddress getAddressByAccount(Long userId, Long stationId);


	void save(SysAccountVirtualAddress newVa);

    SysAccountVirtualAddress getByAddress(String toAddress);
}
