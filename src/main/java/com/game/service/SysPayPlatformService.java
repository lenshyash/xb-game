package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysPayPlatform;
import com.game.model.vo.PayPlatformVo;

public interface SysPayPlatformService {

	public Page getPage(PayPlatformVo ppvo);

	public void savePayPlatform(SysPayPlatform payPlatform);

	public void delPayPlatform(Long ppId);

	public void updStatus(Long ppId, Long status);
	
	public List<Map> getPaysCombo(PayPlatformVo ppVo);

	public SysPayPlatform getPayPlatform(Long payPlatformId);

}
