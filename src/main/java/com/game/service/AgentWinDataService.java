package com.game.service;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentWinData;

public interface AgentWinDataService {
	
	Page page(Long stationId);
	
	void addSave(AgentWinData awd);
	
	void delete(Long id, Long stationId);

}
