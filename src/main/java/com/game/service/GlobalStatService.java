package com.game.service;

import java.util.List;
import java.util.Map;

public interface GlobalStatService {

	List<Map<String,Object>> getOnLineCountMap();

}
