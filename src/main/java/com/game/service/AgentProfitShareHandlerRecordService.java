package com.game.service;

import java.util.Date;

import org.jay.frame.jdbc.Page;

public interface AgentProfitShareHandlerRecordService {

	public Page page(String account,String agentName,String parentNames, Date startDate, Date endDate, Integer status, Long stationId);
	
	public void doProfitShare(Long stationId, String[] ids,String[] moneys);
	
}
