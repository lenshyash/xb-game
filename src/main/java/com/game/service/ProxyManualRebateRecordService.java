package com.game.service;

import java.util.Map;

import org.jay.frame.jdbc.Page;

public interface ProxyManualRebateRecordService {
	Page page(String startTime, String endTime, String account, Long stationId);
}
