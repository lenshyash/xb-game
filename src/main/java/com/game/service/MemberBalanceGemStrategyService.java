package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberBalanceGemStrategy;

public interface MemberBalanceGemStrategyService {

	Page<MemberBalanceGemStrategy> getPage( Date begin, Date end,
			Long stationId);

	MemberBalanceGemStrategy getOne(Long id, Long stationId);

	void delete(Long id, Long stationId);

	void addSave(MemberBalanceGemStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId);

	void updStatus(Integer status, Long id, Long stationId);

	void update(MemberBalanceGemStrategy com, Long[] groupLevelIds, String startDate, String endDate, Long stationId);

	MemberBalanceGemStrategy filter(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId);
	
	List<MemberBalanceGemStrategy> filterList(Long accountId, long depositCount, long dayDepoitCount, long moneyRecordType,
			BigDecimal money, Date depositDate, Long stationId);

	List<MemberBalanceGemStrategy> getByType(int type, Long stationId);
	/**
	 * 获取站点策略
	 * @param stationId
	 * @return
	 */
	List<MemberBalanceGemStrategy> findStationStrategy(Long stationId);

}
