package com.game.service;

import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;

import com.game.model.lottery.BcLottery;
import com.game.model.vo.LotteryGroupVo;

public interface LotteryService {
	/**
	 * 以下第三版彩票使用
	 * @param model
	 * @return
	 */
	public List<BcLottery> getLottery(Model model);
	
	public List<BcLottery> getLotteryList();
	
	public Map<String,String> getLotteryMap();
	
	public void headerLot(Model model,String navClass);
	/**
	 * 获取大类Type集合
	 * @return
	 */
	public List<LotteryGroupVo> getBigGroup(Integer indentify);
	
}
