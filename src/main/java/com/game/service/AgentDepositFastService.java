package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentDepositFast;

public interface AgentDepositFastService {
	public Page getFastPage(Long payPlatformId);

	public void save(AgentDepositFast fast, Long[] groupLevelId);

	public List<Map> getStationFasts();
	
	public void delFast(long id);

	public AgentDepositFast getOne(Long id, Long stationId);

	public void updateStatus(Integer status, Long id, Long stationId);
	
	public List<Map> periodPay();

	public List<Map> getStationFasts(String temp);
}
