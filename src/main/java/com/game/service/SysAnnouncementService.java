package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.SysAnnouncement;
import com.game.model.vo.AnnouncementVo;

public interface SysAnnouncementService {
	public Page<Map> getPage(AnnouncementVo anctVo);

	public void saveAnnouncement(SysAnnouncement announcement, Long[] stationIds);

	public void deleteAnc(Long ancId);

	/**
	 * 租户后台公告
	 * 
	 * @param stationId
	 * @return
	 */
	public String getAgentNotices(Long stationId);

	public List<Long> getStationIds(Long announcementId);
	
	/**
	 * 获得最近的公告
	 * @param count（条数）
	 * @return
	 */
	public List<Map> getLastAnnouncement(Integer count);
 }
