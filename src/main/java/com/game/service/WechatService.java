package com.game.service;


public interface WechatService {
	
	public int callback(String code);
	
	public void addAccount(String account,String pwd);
	
	public void bindAccount(String account,String pwd);
}
