package com.game.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentArticle;


public interface AgentArticleService {

	public Page page(Long stationId, Integer code);
	
	String publicInfo(Integer code);
	
	public List<Map> getStationAtcs(Long stationId);
	
	public List<Map<String,Object>> getArticleForMobile(Integer code, Long stationId, Date overTime);
	
	public void openCloseH(Integer modelStatus, Long id, Long stationId);
	
	public void openClosek(Integer status, Long id, Long stationId);
	
	public void addSave(AgentArticle aacle);
	
	public void eidtSave(AgentArticle aacle);
	
	public void delete(Long id, Long stationId,Integer code);
	
	public AgentArticle getOne(Long id);

	public List<AgentArticle> listV3(Long id, int i);
}
