package com.game.service;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.Page;

import com.game.model.AgentDepositOnline;

public interface AgentDepositOnlineService {
	public Page getOnlinePage(String name, Long status);

	public List<Map> getStationOnlines(String showType);

	public void delOnline(long id);

	public AgentDepositOnline getOnline(long payId);

	public void updateStatus(Integer status, Long id, Long stationId);

	public void save(AgentDepositOnline online, Long[] groupLevelIds);
	
	public AgentDepositOnline getOnlineByMerCode(String merCode);

	public List<Map> getStationOnlines(String showType, String oindex);

}
