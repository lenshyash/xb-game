package com.game.service;

import java.util.Date;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberActivityApplyRecord;

public interface MemberActivityApplyRecordService {
	public void addRecord(Long activityId,String selected,Long accountId);
	
	public Page<MemberActivityApplyRecord> getPage(Long stationId,String account,Date begin,Date end);
	
	public MemberActivityApplyRecord getOne(Long id);
	
	public void handlerRecord(Long id,Long stationId,Long status,String remark);
	
}
