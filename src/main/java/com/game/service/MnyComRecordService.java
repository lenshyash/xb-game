package com.game.service;

import java.math.BigDecimal;

import org.jay.frame.jdbc.Page;

import com.game.model.MnyComRecord;
import com.game.model.SysAccount;
import com.game.model.vo.MnyComRecordVo;
import com.game.model.vo.MnyMoneyVo;
import com.game.model.vo.WithdrawCheckVo;

public interface MnyComRecordService {
	public Page<MnyComRecord> getPage(MnyComRecordVo mcrVo);
	
	public Page<MnyComRecord> getPage2(MnyComRecordVo mcrVo);
	/**
	 * 人工入款
	 * 
	 * @param moneyVo
	 */
	public MnyComRecord deposit(MnyMoneyVo moneyVo);

	/**
	 * 锁定和解锁操作
	 * 
	 * @param comId
	 * @param lockFlag
	 */
	public void lock(long comId, long lockFlag);

	/**
	 * 确认取消提款
	 * 
	 * @param comId
	 */
	public void comHandler(MnyComRecordVo mcrVo);

	/**
	 * 获取未处理的充值数量
	 * 
	 * @return
	 */
	public Integer getCountOfUntreated(final long stationId);

	/**
	 * 增加未处理的充值数量
	 * 
	 * @return
	 */
	public void addCountOfUntreated(final long stationId);

	/**
	 * 减去未处理的充值数量
	 * 
	 * @return
	 */
	public void subCountOfUntreated(final long stationId);

	/**
	 * 根据订单号查询订单
	 * 
	 * @param orderNo
	 */
	public MnyComRecord getByOrderNo(String orderNo);

	/**
	 * 在线充值
	 * 
	 * @param mcrVo
	 */
	public void onlinepayComHandler(MnyComRecordVo mcrVo);

	/**
	 * 充值成功处理更新会员出款所需打码量和投注打码量
	 * 
	 * @param mcrVo
	 */
	// public void handlerAdvancedDeposit(MnyComRecordVo mcrVo);

	/**
	 * 人工加扣款
	 * 
	 * @param mcrVo
	 */
	public void artificial(MnyComRecordVo mcrVo);
	/**
	 * 站内额度转换
	 * 
	 * @param mcrVo
	 */
	public void moneyChange(long outAccId, long inAccId, BigDecimal money, Long checkBetNum, BigDecimal betNumMultiple,
			String remark);

	/**
	 * 会员充值记录导出功能
	 */
	public void export(MnyComRecordVo mcrVo);

	/**
	 * 会员充值总额
	 * 
	 * @param stationId
	 * @param userId
	 * @return
	 */
	public Long getComTotalMoney(Long stationId, Long userId);

	public MnyComRecord getOne(Long id, Long stationId);

	/**
	 * 站点充值状态定时自动过期
	 */
	public void autoExpireStatus(Long stationId);

	/**
	 * 手动加款
	 * 
	 * @param account
	 * @param money
	 * @param betNumMultiple
	 * @param giftMoney
	 * @param giftBetNumMultiple
	 * @param remark
	 */
	public void artificialAddOne(SysAccount account, BigDecimal money, BigDecimal betNumMultiple, BigDecimal giftMoney,
			BigDecimal giftBetNumMultiple, String remark);

	/**
	 * 手动批量扣款
	 * 
	 * @param account
	 * @param money
	 * @param remark
	 */
	public void artificialSubOne(SysAccount account, BigDecimal money, String remark);
	
	/**
	 * 撒消成功充值
	 */
	public void cancelHandler(MnyComRecordVo mcrVo);
	
	/**
	 * 会员站内转换
	 */
	public void moneyChangeOfMember(String account,BigDecimal money ,String rePassword);

	public void reback(Long comId);

	void saveRemark(Long id, String remark);

	void comHandlerNoNeedLock(MnyComRecordVo mcrVo);


	Integer getMcrByAccountAndPayId(Long accountId, Long payId);

    MnyComRecord getByMoneyAndAccount(BigDecimal money, Long accountId);
}
