package com.game.service;

import java.util.List;

import com.game.model.SysStation;

/**
 * 非Controller调用接口
 * @author admin
 *
 */
public interface FrameService {
	
	public SysStation getStation(String domain);

	public SysStation findOneById(Long stationId);
	
	/**
	 * 获取多模板所有模板
	 * @param stationId
	 * @return
	 */
	public List<String> getStationFolderByMulti(Long stationId);
	
}
