package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.game.model.DailyMoney;
import com.game.model.MnyMoney;
import com.game.model.MnyMoneyRecord;
import com.game.model.SysAccount;
import com.game.model.vo.MnyMoneyVo;

public interface MnyMoneyService {

	//@Deprecated
	//public BigDecimal updateMoney(MnyMoneyVo moneyVo);
	/**
	 * 金额变动
	 */
	public MnyMoneyRecord updMnyAndRecord(MnyMoneyVo moneyVo);
	
	public MnyMoneyRecord updMnyAndRecord(MnyMoneyVo moneyVo,Long stationId);

	public BigDecimal checkMoney(Long memberId, BigDecimal money);

	public Map getMoneyByAccount(String account);
	
	public Map getMoneyByAccount(String account,Long stationId);
	
	public MnyMoney getMoneyById(Long accountId);
	
	/**
	 * 代理平台上级给下级加款
	 */
	public void saveCash(Long accountId,String money,BigDecimal moneyOld,Long userId,Long stationId);

	public DailyMoney findByAccountAndDate(Long memberId, Date formatDate);
	
	public BigDecimal getRate(BigDecimal selfRate,SysAccount ac);
	/**
	 * 余额宝金额操作
	 * @param moneyVo
	 * @return
	 */
	public BigDecimal updateBalanceGem(MnyMoneyVo moneyVo);
	
	public void dailyHandler(MnyMoneyVo moneyVo, Date statDate, SysAccount user, BigDecimal balance,BigDecimal money);
	
	public BigDecimal updateMoney(MnyMoneyVo moneyVo);
	
	public BigDecimal exactDecimal(BigDecimal moneyDecimal);

	public BigDecimal fastTransfer(Long id);
}
