package com.game.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.MemberRedPacket;
import com.game.model.MemberRedPacketFictitiousRecord;
import com.game.model.MemberRedPacketRecord;

public interface MemberRedPacketService {

	public Page<MemberRedPacket> getPage(Long stationId);

	public Page<MemberRedPacketRecord> getRecordPage(String account, Long stationId, Date begin, Date end);
	
	public Page<MemberRedPacketFictitiousRecord> getFictitiousPage(Long stationId, Date begin, Date end);

	public MemberRedPacket saveRedPacket(MemberRedPacket mrp, Long[] groupLevelId);
	
	public MemberRedPacket saveRedPacket(MemberRedPacket mrp, Long[] groupLevelId,
			Long createType,Long recordNum,BigDecimal beginMoney,BigDecimal endMoney);

	public void updStatus(Long redPacketId, Integer status);

	public MemberRedPacket getCurrentRedPacket(Long stationId);

	public BigDecimal grabRedPacket(Long redPacketId);

	public List<MemberRedPacket> getRedPacketList(Long stationId);

	public List<MemberRedPacketRecord> getMarqueeRecord(Long stationId, Long redPacketId);

	public MemberRedPacketRecord getRedPacketRecord(Long id, Long stationId);

	public void handlerRecord(Long recordId, Integer status, String remark);

	public boolean balanceAndRecord(MemberRedPacketRecord record);

	public void delRedPacket(Long id);

	public void initRedPacketRedis(MemberRedPacket rp);

	public List<MemberRedPacketRecord> getUntreatedRecords(Date end, Integer limit);

}
