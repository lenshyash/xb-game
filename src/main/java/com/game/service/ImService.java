package com.game.service;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.game.model.SysAccount;

public interface ImService {
	
	public String login();
	
	public void shareOrder(SysAccount user,final JSONObject obj);
	
	public void shareReport(SysAccount user, final JSONObject obj);

	public Map generateChatRoom(Long id, String folder, String domain,Long chatNum);
	
	public String getApplicableToken();
	
	public Map initImUser(Long stationId, String account, Long accountId,String ip,String levelName,String imChatId);
	public Map getChatConfig(Long stationId,String imChatId,String levelName,String imUserId);
}
