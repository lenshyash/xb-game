package com.game.service;

import java.util.List;

import org.jay.frame.jdbc.Page;

import com.game.model.platform.AgentKeFu;
import com.game.model.platform.AgentLunBo;

public interface AgentKeFuService {

	Page page(Long stationId);
	
	void addSave(AgentKeFu kefu);
	
	AgentKeFu getOne(Long id);
	
	void delete(Long id);
	
	void openCloseH(Long id ,Integer status);
	
	List<AgentKeFu> getActiveList(Long stationId,Integer type);
}
