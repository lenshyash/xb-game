package com.game.spring.datasource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * created on 2016-07-05 11:51
 *
 * @author zhouyu
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
	private Logger logger = Logger.getLogger(this.getClass());

	@Override
	protected Object determineCurrentLookupKey() {
//		logger.info("DynamicDataSource : " + DataSourceHolder.getDataSource());
		return DataSourceHolder.getDataSource();
	}

}
