package com.game.job.rebate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.model.SysStation;
import com.game.service.AgentProfitShareCoreService;
import com.game.service.SysStationService;

public class DailyBetStatForAgentProfitShareJob {
	private final Logger logger = Logger.getLogger(DailyBetStatForAgentProfitShareJob.class);
	@Autowired
	private SysStationService stationService;
	@Autowired
	private AgentProfitShareCoreService backwaterRecordService;

	public void work() {
		try {
			logger.info("开始汇总代理占成数据");
			collectDayBetMoney();
			logger.info("汇总代理占成数据结束");
		} catch (Exception e) {
			logger.error("汇总代理占成数据发生错误", e);
		}
	}

	private void collectDayBetMoney() {
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Date end = now.getTime();
		now.add(Calendar.DAY_OF_MONTH, -2);
		Date start = now.getTime();
		List<SysStation> sIdList = stationService.getAllActive();
		if(sIdList == null || sIdList.isEmpty()) {
			return;
		}
		for (SysStation sysStation : sIdList) {
			backwaterRecordService.collectAgentProfitShareRecord(start, end, sysStation.getId());
		}
	}
}
