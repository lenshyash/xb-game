package com.game.job.rebate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.constant.StationConfig;
import com.game.model.SysStation;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.service.SysStationService;
import com.game.util.StationConfigUtil;

public class DailyBetStatForBackwaterAndProxyRebateJob {
	private final Logger logger = Logger.getLogger(DailyBetStatForBackwaterAndProxyRebateJob.class);
	@Autowired
	private SysStationService stationService;
	@Autowired
	private MemberBackwaterRecordService backwaterRecordService;
	@Autowired
	private ProxyDailyBettingStatisticsService proxyDailyBettingStatisticsService;

	public void work() {
		try {
			logger.info("开始汇总会员日投注金额");
			collectDayBetMoney();
			logger.info("汇总会员日投注金额结束");
		} catch (Exception e) {
			logger.error("汇总会员日投注金额发生错误", e);
		}
		try {
			logger.info("开始汇总代理日投注金额，反水金额");
			collectProxyDailyMoney();
			logger.info("汇总代理日投注金额，反水金额结束");
		} catch (Exception e) {
			logger.error("汇总代理日投注金额，反水金额发生错误", e);
		}
	}

	private void collectDayBetMoney() {
		List<Long> sIdList = getStationIdList();
		if (sIdList == null)
			return;
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Date end = now.getTime();
		now.add(Calendar.DAY_OF_MONTH, -2);
		Date start = now.getTime();

		now.add(Calendar.DAY_OF_MONTH, -5);// 六合彩获取一周内的数据
		Date startLHC = now.getTime();

		List<MemberBackwaterRecord> all = new ArrayList<>();
		List<MemberBackwaterRecord> list = null;
		// 汇总昨天派奖的所有投注信息
		try {
			list = backwaterRecordService.collectMarkSixDayBetMoney(startLHC, end, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总六合投注金额发生错误");
		}
		try {
			list = backwaterRecordService.collectLotteryDayBetMoney(start, end, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总彩票投注金额发生错误");
		}
		
		try {
			list = backwaterRecordService.collectSysLotteryDayBetMoney(start, end, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总系统彩票投注金额发生错误");
		}
		
		try {
			list = backwaterRecordService.collectSportDayBetMoney(start, end, sIdList);
			if (list != null && !list.isEmpty()) {
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总体育投注金额发生错误");
		}
		try {
			list = backwaterRecordService.collectLotteryV4DayBetMoney(start,end,sIdList);
			if(list != null && !list.isEmpty()){
				all.addAll(list);
			}
		} catch (Exception e) {
			logger.error("汇总v4版本投注金额发生错误");
		}
		backwaterRecordService.saveAll(all);
	}

	/**
	 * 获取需要反水的站点id
	 * 
	 * @return
	 */
	private List<Long> getStationIdList() {
		List<SysStation> stationList = stationService.getAllActive();
		if (stationList == null || stationList.isEmpty()) {
			logger.info("站点列表为空，不需要计算反水");
			return null;
		}
		List<Long> sIdList = new ArrayList<>();
		
		for (SysStation s : stationList) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_multi_agent), "on")
					&& StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_platform_backwater), "off")) {
				continue;
			}
			
			sIdList.add(s.getId());
		}
		if (sIdList.isEmpty()) {
			logger.info("需要反水的站点为空，不需要计算反水");
			return null;
		}
		return sIdList;
	}
	
	private void collectProxyDailyMoney() {
		List<Long> sIdList = getStationIdList();
		if (sIdList == null)
			return;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date endDate = calendar.getTime();
		calendar.add(Calendar.DAY_OF_MONTH, -5);
		Date startDate = calendar.getTime();
		proxyDailyBettingStatisticsService.collectDailyAmount2(startDate, endDate, sIdList);
	}
}
