package com.game.job.rebate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.constant.StationConfig;
import com.game.model.SysStation;
import com.game.model.platform.MemberBackwaterRecord;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.MemberBalanceGemRecordService;
import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.service.SysStationService;
import com.game.util.StationConfigUtil;

public class BalanceGemRebateJob {
	private final Logger logger = Logger.getLogger(BalanceGemRebateJob.class);
	@Autowired
	private SysStationService stationService;
	@Autowired
	private MemberBalanceGemRecordService balanceGemRecordService;

	public void work() {
		try {
			logger.info("开始汇总会员余额宝返利金额");
			collectDayBetMoney();
			logger.info("汇总会员余额宝返利金额结束");
		} catch (Exception e) {
			logger.error("汇总会员余额宝返利金额发生错误", e);
		}
	}
	
	private void collectDayBetMoney() {
		//计算前一天的余额宝盈利
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, -1);
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Date start = now.getTime();
		now.set(Calendar.HOUR_OF_DAY, 23);
		now.set(Calendar.MINUTE, 59);
		now.set(Calendar.SECOND, 59);
		now.set(Calendar.MILLISECOND, 0);
		Date end = now.getTime();
		List<Long> sIdList = getStationIdList();
		if(sIdList == null || sIdList.isEmpty()) {
			return;
		}
		for (Long stationId : sIdList) {
			balanceGemRecordService.collectBalanceGemRecord(start, end, stationId);
		}
	}

	/**
	 * 获取需要进行反利的站点id
	 * 
	 * @return
	 */
	private List<Long> getStationIdList() {
		List<SysStation> stationList = stationService.getAllActive();
		if (stationList == null || stationList.isEmpty()) {
			logger.info("站点列表为空，不需要计算余额宝返利");
			return null;
		}
		List<Long> sIdList = new ArrayList<>();
		
		for (SysStation s : stationList) {
			//未设置平台反水开关的时候，多级代理会员不反水，设置了平台反水开关的时候，平台开关优于多级代理开关
			if(!StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_balance_gem), "on")
					&& !StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.onoff_balance_gem), "ban")) {
				continue;
			}
			
			sIdList.add(s.getId());
		}
		if (sIdList.isEmpty()) {
			logger.info("需要余额宝返利站点为空");
			return null;
		}
		return sIdList;
	}
	
}
