package com.game.job.rebate;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.service.ProxyDailyBettingStatisticsService;
import com.game.util.DateUtil;

public class ProxyRebateJob {
	private static final Logger logger = Logger.getLogger(ProxyRebateJob.class);

	@Autowired
	private ProxyDailyBettingStatisticsService proxyDailyBettingStatisticsService;

	public void work() {// 每天凌晨3点计算代理昨天的投注，中奖、返点金额
//		Calendar calendar = Calendar.getInstance();
//		calendar.set(Calendar.HOUR_OF_DAY, 0);
//		calendar.set(Calendar.MINUTE, 0);
//		calendar.set(Calendar.SECOND, 0);
//		calendar.set(Calendar.MILLISECOND, 0);
//		Date endDate = calendar.getTime();
//		calendar.add(Calendar.DAY_OF_MONTH, -1);
//		Date startDate = calendar.getTime();
//		
//		logger.info("计算代理昨天的投注，中奖、返点金额,时间段[" + DateUtil.formatDate(startDate, DateUtil.DATETIME_FORMAT_STR) + "]至[" + DateUtil.formatDate(endDate, DateUtil.DATETIME_FORMAT_STR) + "]");
//		proxyDailyBettingStatisticsService.collectDailyAmount(startDate, endDate, null);
//		logger.info("代理昨天的投注，中奖、返点金额计算完");
	}

}
