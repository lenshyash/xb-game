package com.game.job.rebate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.game.constant.BusinessConstant;
import com.game.constant.StationConfig;
import com.game.dao.lottery.BcLotteryOrderDao;
import com.game.model.SysStation;
import com.game.model.lottery.BcLotteryOrder;
import com.game.model.platform.ProxyMultiRebateRecord;
import com.game.service.ProxyMultiRebateCoreService;
import com.game.service.SysStationService;
import com.game.util.StationConfigUtil;

public class ProxyMultiRebateJob {
	private static final Logger logger = Logger.getLogger(ProxyMultiRebateJob.class);
	
	@Autowired
	private ProxyMultiRebateCoreService proxyMultiRebateCoreService;
	@Autowired
	private BcLotteryOrderDao lotteryOrderDao;
	@Autowired
	private SysStationService stationService;

	public void work() {
		try {
			logger.info("多级代理 彩票返点--start---");
            executeProxyMultiRebate();
			logger.info("多级代理 彩票返点--end---");
		} catch (Exception e) {
			logger.error("多级代理 彩票返点发生错误", e);
		}
	}
	

    private void executeProxyMultiRebate() {
        List<SysStation> stationList = stationService.getAllActive();
        if (stationList == null || stationList.isEmpty()) {
            logger.info("站点列表为空，不需要计算反水");
            return;
        }
        Calendar calendar = Calendar.getInstance();
        Date endDate = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date startDate = calendar.getTime();
        for (SysStation station : stationList) {
            try {
                if (!StringUtils.equals(StationConfigUtil.get(station.getId(), StationConfig.onoff_multi_agent), "on")) {
                    logger.info("站点使用一级代理机制，不需要计算多级代理返点，站点＝" + station.getFloder());
                    continue;
                }
                
                if (StringUtils.equals(StationConfigUtil.get(station.getId(), StationConfig.agent_lottery_rebate_choice), "off") && !StringUtils.equals(StationConfigUtil.get(station.getId(), StationConfig.onoff_proxy_fixed_rebate), "on")) {
                    logger.info("站点未开启多级租户代理反点，不需要计算多级代理返点，站点＝" + station.getFloder());
                    continue;
                }
                logger.info("开始计算多级代理返点，站点＝" + station.getFloder());
                doProxyMultiRebate(station, startDate, endDate, BusinessConstant.STATUS_NOT_ROLL);
                logger.info("多级代理返点计算结束，站点＝" + station.getFloder());
            } catch (Exception e) {
                logger.info("多级代理返点发生错误，站点＝" + station.getFloder(), e);
            }
        }
    }

    private void doProxyMultiRebate(SysStation station, Date startDate, Date endDate, Integer rollBackStatus) {
        logger.info("计算多级代理返点,站点=" + station.getFloder());
        List<BcLotteryOrder> bets = lotteryOrderDao.get4Rebate(station.getId(), startDate, endDate, rollBackStatus, false);
        if (bets != null && bets.size() > 0) {
            logger.info("需要多级代理返点的彩票记录条数=" + bets.size());
            ProxyMultiRebateRecord proxyMultiRebateRecord = null;
            for (BcLotteryOrder bcLotteryOrder : bets) {
                try {
                    proxyMultiRebateRecord = new ProxyMultiRebateRecord();
                    proxyMultiRebateRecord.setChild(bcLotteryOrder.getAccount());// 从下级开始
                    proxyMultiRebateRecord.setBetId(bcLotteryOrder.getId());
                    proxyMultiRebateRecord.setStationId(bcLotteryOrder.getStationId());
                    proxyMultiRebateRecord.setBetMoney(bcLotteryOrder.getBuyMoney());
                    proxyMultiRebateRecord.setBetOrderId(bcLotteryOrder.getOrderId());
                    proxyMultiRebateRecord.setBetOrderDatetime(bcLotteryOrder.getCreateTime());
                    //判断是否开启固定返点开关,开启后按固定返点百分比向直属上级返点
                    if("on".equals(StationConfigUtil.get(bcLotteryOrder.getStationId(), StationConfig.onoff_proxy_fixed_rebate))){
                        proxyMultiRebateCoreService.directSuperordinateFixedRebate(proxyMultiRebateRecord);
                    }else{
                        proxyMultiRebateCoreService.updateDateAndAddMoney4MultiRebate(proxyMultiRebateRecord);
                    }
                } catch (Exception e) {
                    logger.error("多级代理返点失败" + JSON.toJSONString(bcLotteryOrder), e);
                }
            }
        }
        logger.info("多级代理返点结束,站点=" + station.getFloder());
    }
}
