package com.game.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.constant.StationConfig;
import com.game.model.SysStation;
import com.game.service.SysAccountService;
import com.game.service.SysAccountWarningService;
import com.game.service.SysStationService;
import com.game.user.online.OnlineInfo;
import com.game.user.online.OnlineManager;
import com.game.util.CodeMaker;
import com.game.util.CodeType;
import com.game.util.StationConfigUtil;

public class AutoUserWarningJob {
	private static final Logger logger = Logger.getLogger(AutoUserWarningJob.class);
	
	private static boolean completed = false;
	@Autowired
	private SysAccountWarningService accountWarningService;
	@Autowired
	private SysStationService stationService;
	

	public void work() {
		if (completed) {
			try {
				completed = false;
				logger.info("用户预警检查开始");
				userWarning();
				logger.info("用户预警检查结束");

			} catch (Exception e) {
				logger.error("用户预警检查发生错误", e);
			} finally {
				completed = true;
			}
		}
	}

	private void userWarning() {
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DAY_OF_MONTH, -7);
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		Date start = now.getTime();
	
		List<Long> sIdList = getStationIdList();
		if(sIdList == null || sIdList.isEmpty()) {
			return;
		}
		for (Long id : sIdList) {
			accountWarningService.executeUserWarning(start,id);
		}
	}

	/**
	 * 获取已启用站点
	 * 
	 * @return
	 */
	private List<Long> getStationIdList() {
		List<SysStation> stationList = stationService.getAllActive();
		if (stationList == null || stationList.isEmpty()) {
			logger.info("站点列表为空，不需要统计预警用户");
			return null;
		}
		List<Long> sIdList = new ArrayList<>();
		for (SysStation s : stationList) {
			if(!StringUtils.equals(StationConfigUtil.get(s.getId(), StationConfig.on_off_user_warning), "off")) {
				continue;
			}
			sIdList.add(s.getId());
		}
		if (sIdList.isEmpty()) {
			logger.info("需要统计预警用户站点为空");
			return null;
		}
		return null;
	}
	
}
