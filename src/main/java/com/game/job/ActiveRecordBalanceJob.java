package com.game.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.constant.StationConfig;
import com.game.model.MemberActiveAward;
import com.game.model.MemberActiveRecord;
import com.game.service.AgentBaseConfigService;
import com.game.service.MemberActiveService;

public class ActiveRecordBalanceJob {
	private static final Logger logger = Logger.getLogger(ActiveRecordBalanceJob.class);

	private static boolean completed = true;

	@Autowired
	private MemberActiveService activeService;
	@Autowired
	private AgentBaseConfigService configService;
	
	public void work() {
		if (completed) {
			completed = false;
			try {
				logger.info("活动中奖记录派奖开始");
				int totalSize = 0;
				int successSize = 0;
				List<MemberActiveRecord> records = activeService.getUntreatedRecords();
				if (records != null && !records.isEmpty()) {
					totalSize = records.size();
					successSize = totalSize;
					Map<Long,Boolean> map=new HashMap<>();
					Boolean b=null;
					for (MemberActiveRecord memberActiveRecord : records) {
						try {
							b = map.get(memberActiveRecord.getStationId());
							if(b==null) {
								b=StringUtils.equals("off", configService.getSettingValueByKey(StationConfig.onoff_wheel_auto_award.name(),memberActiveRecord.getStationId()));
								map.put(memberActiveRecord.getStationId(), b);
							}
							if(b) {
								successSize--;
								continue;
							}
							memberActiveRecord.setStatus(MemberActiveRecord.STATUS_SUCCESS);
							memberActiveRecord.setRemark("自动结算");
							boolean success = false;
							//大转自动开奖增加积分
							if(StringUtil.equals(memberActiveRecord.getAwardType(), MemberActiveAward.AWARD_TYPE_MONEY)){
								success = activeService.balanceAndRecord(memberActiveRecord);
							}else if(StringUtil.equals(memberActiveRecord.getAwardType(), MemberActiveAward.AWARD_TYPE_SCORE)){
								success = activeService.scoreAndRecord(memberActiveRecord);
							}
							if(success) {
								successSize--;
							}
						} catch (Exception e) {
							logger.error("活动中奖记录派奖发生错误", e);
						}
					}
				}
				logger.error("活动中奖记录派奖 , 总处理条数:" + totalSize + ",成功处理条数:" + successSize);
			} catch (Exception e) {
				logger.error("活动中奖记录派奖发生错误ALL", e);
			} finally {
				completed = true;
			}
		} else {
			logger.info("上次任务[活动中奖记录派奖]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}
}
