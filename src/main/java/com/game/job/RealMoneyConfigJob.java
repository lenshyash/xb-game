package com.game.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.service.AgentBaseConfigService;

public class RealMoneyConfigJob {
	private Logger logger = Logger.getLogger(RealMoneyConfigJob.class);
	@Autowired
	private AgentBaseConfigService configService;

	public void updateRealMoneyConfig() {
		try {
			logger.error("开始修改真人额度");
			configService.updateRealMoneyConfig();
		} catch (Exception e) {
			logger.error("修改真人额度发生错误", e);
		}
	}
}
