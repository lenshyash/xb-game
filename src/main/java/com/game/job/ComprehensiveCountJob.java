package com.game.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.service.ComprehensiveCountService;

public class ComprehensiveCountJob {
	private static final Logger logger = Logger.getLogger(ComprehensiveCountJob.class);

	private static boolean completed = true;

	@Autowired
	private ComprehensiveCountService ccService;

	public void work() {
		if (completed) {
			completed = false;
			try {
				logger.info("统计概况开始:" + System.currentTimeMillis());
				ccService.executeStatistics();
				logger.info("统计概况结束:" + System.currentTimeMillis());
			}catch(Exception e){
				logger.error("统计概况发生错误",e);
			} finally {
				completed = true;
			}
		} else {
			logger.info("上次任务[统计概况]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}
}
