package com.game.job;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.service.CreatePartitionService;
import com.game.util.DateUtil;

public class CreatePartitionJob {
	private Logger logger = Logger.getLogger(CreatePartitionJob.class);
	@Autowired
	private CreatePartitionService createPartitionService;

	public void createTable() {
		Calendar c = Calendar.getInstance();
		doCreate(c.getTime());
		// 创建下个月分表
		c.add(Calendar.MONTH, 1);
		doCreate(c.getTime());
	}

	public void doCreate(Date date) {
		String dateStr = DateUtil.formatDate(date, "yyyy-MM-dd");
		try {
			logger.info("开始创建bc_lottery_data的分表 date=" + dateStr);
			createPartitionService.createLotteryPartition(date,"data");
			logger.info("成功创建bc_lottery_data的分表 date=" + dateStr);
		} catch (Exception e) {
			logger.error("创建bc_lottery_data的分表发生异常 date=" + dateStr, e);
		}
		try {
			logger.info("开始创建bc_lottery_order的分表 date=" + dateStr);
			createPartitionService.createLotteryPartition(date,"order");
			logger.info("成功创建bc_lottery_order的分表 date=" + dateStr);
		} catch (Exception e) {
			logger.error("创建bc_lottery_order的分表发生异常 date=" +dateStr, e);
		}
		try {
			logger.info("开始创建proxy_multi_rebate_record的分表 date=" + dateStr);
			createPartitionService.createProxyMultiRebateRecordPartition(date);
			logger.info("成功创建proxy_multi_rebate_record的分表 date=" + dateStr);
		} catch (Exception e) {
			logger.error("创建proxy_multi_rebate_record的分表发生异常 date=" +dateStr, e);
		}
//		try {
//			logger.info("开始创建member_roll_back_record的分表 date=" + dateStr);
//			createPartitionService.createMemberRollBackRecordPartition(date);
//			logger.info("成功创建member_roll_back_record的分表 date=" + dateStr);
//		} catch (Exception e) {
//			logger.error("创建member_roll_back_record的分表发生异常 date=" + dateStr, e);
//		}
		try {
			logger.info("开始创建member_backwater_record的分表 date=" + dateStr);
			createPartitionService.createMemberBackwaterRecordPartition(date);
			logger.info("成功创建member_backwater_record的分表 date=" + dateStr);
		} catch (Exception e) {
			logger.error("创建member_backwater_record的分表发生异常 date=" + dateStr, e);
		}
		
		try {
			logger.info("开始创建bet_num_record的分表 date=" + dateStr);
			createPartitionService.createBetNumRecordPartition(date);
			logger.info("成功创建bet_num_record的分表 date=" + dateStr);
		} catch (Exception e) {
			logger.error("创建bet_num_record的分表发生异常 date=" + dateStr, e);
		}
	}
}
