package com.game.job;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.game.constant.StationConfig;
import com.game.model.MemberRedPacketRecord;
import com.game.service.AgentBaseConfigService;
import com.game.service.MemberRedPacketService;

public class RedPacketRecordHandlerJob {
	private static final Logger logger = Logger.getLogger(RedPacketRecordHandlerJob.class);

	private static boolean completed = true;

	@Autowired
	private MemberRedPacketService redPacketService;
	@Autowired
	private AgentBaseConfigService configService;

	public void work() {
		if (completed) {
			completed = false;
			try {
				logger.info("红包记录派奖开始");
				Calendar c = Calendar.getInstance();
				List<MemberRedPacketRecord> records = redPacketService.getUntreatedRecords(c.getTime(), 100);
				if (records == null || records.isEmpty()) {
					logger.info("红包记录为空，不需要派奖");
					return;
				}
				Map<Long,Boolean> map=new HashMap<>();
				Boolean b=null;
				String onoff=null;
				for (MemberRedPacketRecord record : records) {
					b = map.get(record.getStationId());
					if(b==null) {
						onoff=configService.getSettingValueByKey(StationConfig.onoff_red_packet_auto_award.name(),record.getStationId());
						b=StringUtils.equals("off", onoff);
						map.put(record.getStationId(), b);
					}
					if(b) {
						continue;
					}
					record.setStatus(MemberRedPacketRecord.STATUS_SUCCESS);
					record.setRemark("自动结算");
					try {
						redPacketService.balanceAndRecord(record);
					} catch (Exception e) {
						logger.error("红包派奖错误" + JSON.toJSONString(record), e);
					}
				}
				logger.info("抢红包记录派奖结束");
			} catch (Exception e) {
				logger.error("抢红包记录派奖发生错误", e);
			} finally {
				completed = true;
			}
		}
	}
}
