package com.game.job;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.cache.CacheType;
import com.game.cache.CacheUtil;
import com.game.model.MnyDrawRecord;
import com.game.model.SysStation;
import com.game.service.CommonRedpacketService;
import com.game.service.MemberBackwaterRecordService;
import com.game.service.MnyComRecordService;
import com.game.service.MnyDrawRecordService;
import com.game.service.SysStationService;

public class AutoExpiredStatusJob {
	private static final Logger logger = Logger.getLogger(AutoExpiredStatusJob.class);

	private static boolean completed = true;
	private static boolean completed2 = true;
	private static boolean completed3 = true;

	@Autowired
	private MnyComRecordService mnyComService;

	@Autowired
	private MnyDrawRecordService mnyDrawService;
	
	@Autowired
	private MemberBackwaterRecordService memberBackWaterRecordService;
	
	@Autowired
	private SysStationService stationService;
	
	@Autowired
	private CommonRedpacketService commonRedpacketService;

	public void work() {
		mnyComRecordExpired();
		mnyDrawRecordExpired();
		memberBackWaterExpired();
		//手气红包过期
		redpacketExpire();
	}

	/**
	 * 充值记录自动过期
	 */
	private void mnyComRecordExpired() {
		if (completed) {
			completed = false;
			try {
				logger.info("充值记录自动过期开始:" + System.currentTimeMillis());
				List<SysStation> sts = stationService.getAllActive();
				Long stationId = null;
				for (SysStation sysStation : sts) {
					stationId = sysStation.getId();
					mnyComService.autoExpireStatus(stationId);
					CacheUtil.delCache(CacheType.AGENT_STATION_COM_COUNT,  stationId + "");
				}
				
				logger.info("充值记录自动过期结束:" + System.currentTimeMillis());
			} catch (Exception e) {
				logger.error("充值记录自动过期发生错误", e);
			} finally {
				completed = true;
			}
		} else {
			logger.info("上次任务[充值记录自动过期]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}

	/**
	 * 提款记录自动过期
	 */
	private void mnyDrawRecordExpired() {
		if (completed) {
			completed = false;
			try {
				logger.info("提款记录自动过期开始:" + System.currentTimeMillis());
				List<SysStation> sts = stationService.getAllActive();
				Long stationId = null;
				for (SysStation sysStation : sts) {
					stationId = sysStation.getId();
					List<MnyDrawRecord> records = mnyDrawService.getUntreatedReocrd(stationId);
					if (records != null && !records.isEmpty()) {
						logger.info("需要过期的提款记录数:" + records.size());
						
						for (MnyDrawRecord mnyDrawRecord : records) {
							mnyDrawService.autoExpiredStatus(mnyDrawRecord);
						}
					}
				}

				logger.info("提款记录自动过期结束:" + System.currentTimeMillis());
			} catch (Exception e) {
				logger.error("提款记录自动过期发生错误", e);
			} finally {
				completed = true;
			}
		} else {
			logger.info("上次任务[提款记录自动过期]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}
	
	/**
	 * 动态赔率版会员反水自动过期
	 */
	private void memberBackWaterExpired() {
		if (completed2) {
			completed2 = false;
			try {
				logger.info("动态赔率版会员反水自动过期开始:" + System.currentTimeMillis());
				List<SysStation> sts = stationService.getAllActive();
				Long stationId = null;
				for (SysStation sysStation : sts) {
					stationId = sysStation.getId();
					memberBackWaterRecordService.autoExpireStatus(stationId);
				}

				logger.info("动态赔率版会员反水自动过期结束:" + System.currentTimeMillis());
			} catch (Exception e) {
				logger.error("动态赔率版会员反水自动过期发生错误", e);
			} finally {
				completed2 = true;
			}
		} else {
			logger.info("上次任务[动态赔率版会员反水自动过期]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}
	
	/**
	 * 聊天室手气红包过期还钱
	 */
	private void redpacketExpire() {
		if (completed3) {
			completed3 = false;
			try {
				logger.info("聊天室手气红包过期还钱开始:" + System.currentTimeMillis());
				List<SysStation> sts = stationService.getAllActive();
				Long stationId = null;
				for (SysStation sysStation : sts) {
					stationId = sysStation.getId();
					commonRedpacketService.redpacketExpireReturnMoney(stationId);
				}

				logger.info("聊天室手气红包过期还钱结束:" + System.currentTimeMillis());
			} catch (Exception e) {
				logger.error("聊天室手气红包过期还钱发生错误", e);
			} finally {
				completed3 = true;
			}
		} else {
			logger.info("上次任务[聊天室手气红包过期还钱]还未执行结束,跳过本次任务,等待下次任务开启");
		}
	}
}
