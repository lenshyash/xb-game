package com.game.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.game.constant.StationConfig;
import com.game.model.SysStation;
import com.game.service.SysAccountService;
import com.game.service.SysStationService;
import com.game.user.online.OnlineInfo;
import com.game.user.online.OnlineManager;
import com.game.util.CodeMaker;
import com.game.util.CodeType;
import com.game.util.StationConfigUtil;

public class AutoHandlerOnlineJob {
	private static final Logger logger = Logger.getLogger(AutoHandlerOnlineJob.class);

	private static boolean completed = true;
	private static int lastH = 0;
	@Autowired
	private SysAccountService accountService;
	@Autowired
	private SysStationService stationService;

	public void work() {
		if (completed) {
			try {
				completed = false;
				if (!CodeMaker.GENERATE_BY_DB) {
					Date now = new Date();
					// 更新数据库注单单号
					CodeType[] cts = CodeType.values();
					for (int i = 0; i < cts.length; i++) {
						CodeMaker.saveSeqCache(cts[i], now);
					}
				}
				//每一小时执行一次
//				Calendar c = Calendar.getInstance();
//				if (c.get(Calendar.HOUR_OF_DAY) > lastH ) {
//					lastH = c.get(Calendar.HOUR_OF_DAY);
//					accountService.updateAllOffline();
//				}
				logger.info("用户在线状态维护开始");
				List<OnlineInfo> oiList = OnlineManager.getOnlineInfos(1500);
				if (oiList != null && !oiList.isEmpty()) {
					accountService.saveOnlineInfo(oiList);
				}
				List<SysStation> stationList = stationService.getAllActive();
				if (!stationList.isEmpty() && stationList.size()>0) {
					for(SysStation station:stationList) {
						OnlineManager.upholdUserOnlineStatus(station.getId());
					}
				}
//				accountService.updateOnlineStatus(OnlineManager.getOnlineUserIds());
				logger.info("用户在线状态维护结束");
			} catch (Exception e) {
				logger.error("用户在线状态维护发生错误", e);
			} finally {
				completed = true;
			}
		}
	}
}
