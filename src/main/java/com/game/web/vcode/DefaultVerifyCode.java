package com.game.web.vcode;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.game.util.Num2ChineseNumUtil;
import com.game.util.UnicodeUtil;
import com.game.util.UserUtil;

public class DefaultVerifyCode {
	private static char[] numCodeSequence = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	private static char[] addNumCodeSequence = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	private static char[] codeSequence = { '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
			'h', 'k', 'm', 'n', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y' };

	private static Font[] fonts = new Font[] { new Font("Times New Roman", Font.ITALIC | Font.BOLD, 20),
			new Font("Verdana", Font.ITALIC | Font.BOLD, 20) };
	private static Font[] chineseFonts = new Font[] { new Font("宋体", Font.ITALIC | Font.BOLD, 20),
			new Font("宋体", Font.ITALIC | Font.BOLD, 20) };
	
	static private Font getRandomFont(Random random) {
		return fonts[random.nextInt(fonts.length)];
	}
	static private Font getRandomFont2(Random random) {
		return chineseFonts[random.nextInt(chineseFonts.length)];
	}
	
	public static String createVerifyJPGCode(int width, int height, int codeCount, boolean bianKuang, boolean ganRao,
			boolean onlyNum) throws ServletException, java.io.IOException {
		HttpServletResponse resp = UserUtil.getResponse();
		// 定义图像buffer
		BufferedImage buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = buffImg.createGraphics();
		// 创建一个随机数生成器类
		Random random = new Random();
		// 将图像填充为白色
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		// 画边框。
		if (bianKuang) {
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, width - 1, height - 1);
		}
		// randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
		StringBuffer randomCode = new StringBuffer();
		// 随机产生codeCount数字的验证码。
		String strRand = null;
		int x = width / (codeCount + 1);
		for (int i = 0; i < codeCount; i++) {
			// 得到随机产生的验证码数字。
			if (onlyNum) {
				strRand = String.valueOf(numCodeSequence[random.nextInt(numCodeSequence.length)]);
			} else {
				strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
			}
			// 用随机产生的颜色将验证码绘制到图像中。
			g.setColor(getColor(random));
			g.setFont(getRandomFont(random));
			g.drawString(strRand, (i) * x + x / 2, height-6);
			// 将产生的四个随机数组合在一起。
			randomCode.append(strRand);
		}
		if (ganRao) {
			// 写一个字符到bi
			for (int i = 0; i < 8; i++) {
				g.setColor(getColor(random));
				// 画线
				g.drawLine(random.nextInt(60), random.nextInt(30), random.nextInt(60), random.nextInt(30));
			}
		}
		// 禁止图像缓存。
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", 0);
		resp.setContentType("image/jpeg");
		// 将图像输出到Servlet输出流中。
		ServletOutputStream sos = resp.getOutputStream();
		try {
			ImageIO.write(buffImg, "jpeg", sos);
		} catch (Exception e) {
			//不打印内部错误的
		}
		sos.close();
		return randomCode.toString();
	}

	private static Color getColor(Random random) {
		return new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
	}
	
	public static String createVerifyJPGCodeByAdd(int width, int height, int codeCount, boolean bianKuang, boolean ganRao,
			boolean onlyNum) throws ServletException, java.io.IOException {
		HttpServletResponse resp = UserUtil.getResponse();
		// 定义图像buffer
		BufferedImage buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = buffImg.createGraphics();
		// 创建一个随机数生成器类
		Random random = new Random();
		// 将图像填充为白色
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		// 画边框。
		if (bianKuang) {
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, width - 1, height - 1);
		}
		// randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
		StringBuffer randomCode = new StringBuffer();
		// 随机产生codeCount数字的验证码。
		//使用新的加发验证码
		String strRand1 = String.valueOf(addNumCodeSequence[random.nextInt(addNumCodeSequence.length)]);
		String strRand2 = String.valueOf(addNumCodeSequence[random.nextInt(addNumCodeSequence.length)]);
		
		String addResult = getAddResult(strRand1, strRand2);
		String addText = getAddText(strRand1, strRand2);
		
		codeCount = addText.length();
		int x = width / (codeCount + 1);
		for (int i = 0; i < codeCount; i++) {
			// 用随机产生的颜色将验证码绘制到图像中。
			g.setColor(getColor(random));
			g.setFont(getRandomFont2(random));
			g.drawString(Character.toString(addText.charAt(i)), (i) * x + x / 2, height-6);
            /** **随机缩放文字并将文字旋转指定角度* */
           
           //实例化AffineTransform类的对象
           AffineTransform trans = new AffineTransform();
           //进行旋转
           trans.rotate(random.nextInt(15) * 3.14 / 180, 26 * i + 8, 7);
           
           // 缩放文字
           float scaleSize = random.nextFloat() +0.7f;
           if (scaleSize > 1f) scaleSize = 1f;
           trans.scale(scaleSize, scaleSize);   //进行缩放
           g.setTransform(trans);
			// 将产生的四个随机数组合在一起。
			randomCode.append(Character.toString(addText.charAt(i)));
		}
		//
		if (ganRao) {
			// 写一个字符到bi
			for (int i = 0; i < 4; i++) {
				g.setColor(getColor(random));
				// 画线
				g.drawLine(random.nextInt(60), random.nextInt(30), random.nextInt(60), random.nextInt(30));
			}
		}
		// 禁止图像缓存。
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", 0);
		resp.setContentType("image/jpeg");
		// 将图像输出到Servlet输出流中。
		ServletOutputStream sos = resp.getOutputStream();
		try {
			ImageIO.write(buffImg, "jpeg", sos);
		} catch (Exception e) {
			//不打印内部错误的
		}
		sos.close();
		return addResult.toString();
	}
	
	public static void main(String[] args) {
		String a = "1+1=3";
		char charAt = a.charAt(2);
		String string = Character.toString(charAt);
		System.out.println(string);
	}
	
    /**
     * 加法运算结果
     *
     * @return 运算验证码
     */
    public static String getAddResult(String x, String y) {
    	return new BigDecimal(x).add(new BigDecimal(y)).toString();
    }
    /**
     * 加法运算文本
     *
     * @param x 变量x
     * @param y 变量y
     */
    private static String getAddText(String x, String y) {
    	StringBuilder builder = new StringBuilder();
    	
    	String unicodeX = Num2ChineseNumUtil.bigDecimal2chineseNum(new BigDecimal(x));
    	String unicodeY = Num2ChineseNumUtil.bigDecimal2chineseNum(new BigDecimal(y));
    	
    	builder.append(unicodeX);
    	builder.append("\u52a0");
    	builder.append(unicodeY);
    	
        return builder.toString();
    }
}
