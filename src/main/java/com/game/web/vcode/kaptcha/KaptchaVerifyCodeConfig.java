package com.game.web.vcode.kaptcha;

import java.awt.Color;
import java.awt.Font;
import java.util.Properties;

import com.google.code.kaptcha.GimpyEngine;
import com.google.code.kaptcha.NoiseProducer;
import com.google.code.kaptcha.text.TextProducer;
import com.google.code.kaptcha.text.WordRenderer;
import com.google.code.kaptcha.util.Config;
import com.google.code.kaptcha.util.Configurable;

public class KaptchaVerifyCodeConfig extends Config {
	private WordRenderer renderer;
	private NoiseProducer noiseProducer;
	private GimpyEngine gimpyEngine;
	private Font[] fonts;
	private int charLength = 4;
	private int charSpace = 2;
	private char[] charString;
	private TextProducer textProducer;
	private int fontSize = 24;
	private Color colorFrom;
	private Color colorTo;
	private int width;
	private int height;

	public KaptchaVerifyCodeConfig() {
		this(new Properties());
	}

	public KaptchaVerifyCodeConfig(Properties properties) {
		super(properties);
	}

	public void setRenderer(WordRenderer renderer) {
		this.renderer = renderer;
		setConfigurable(renderer);
	}

	public void setNoiseProducer(NoiseProducer noiseProducer) {
		this.noiseProducer = noiseProducer;
		setConfigurable(noiseProducer);
	}

	public void setFonts(Font[] fonts) {
		this.fonts = fonts;
	}

	public void setCharLength(int charLength) {
		this.charLength = charLength;
	}

	public void setCharSpace(int charSpace) {
		this.charSpace = charSpace;
	}

	public void setCharString(char[] charString) {
		this.charString = charString;
	}

	public void setTextProducer(TextProducer textProducer) {
		this.textProducer = textProducer;
		setConfigurable(textProducer);
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}

	public void setGimpyEngine(GimpyEngine gimpyEngine) {
		this.gimpyEngine = gimpyEngine;
		setConfigurable(gimpyEngine);
	}

	public void setColorFrom(Color colorFrom) {
		this.colorFrom = colorFrom;
	}

	public void setColorTo(Color colorTo) {
		this.colorTo = colorTo;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isBorderDrawn() {
		return false;
	}

	public Color getTextProducerFontColor() {
		return Color.BLACK;
	}

	public GimpyEngine getObscurificatorImpl() {
		return gimpyEngine;
	}

	public WordRenderer getWordRendererImpl() {
		return renderer;
	}

	public NoiseProducer getNoiseImpl() {
		return noiseProducer;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getTextProducerFontSize() {
		return fontSize;
	}

	public Font[] getTextProducerFonts(int fontSize) {
		return fonts;
	}

	public int getTextProducerCharLength() {
		return charLength;
	}

	public int getTextProducerCharSpace() {
		return charSpace;
	}

	public char[] getTextProducerCharString() {
		return charString;
	}

	public TextProducer getTextProducerImpl() {
		return textProducer;
	}
	public Color getBackgroundColorFrom()
	{
		return colorFrom;
	}

	/** */
	public Color getBackgroundColorTo()
	{
		return colorTo;
	}

	
	private void setConfigurable(Object object) {
		if (object instanceof Configurable) {
			((Configurable) object).setConfig(this);
		}
	}
}
