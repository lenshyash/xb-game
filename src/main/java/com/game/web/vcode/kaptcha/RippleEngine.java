package com.game.web.vcode.kaptcha;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.google.code.kaptcha.GimpyEngine;
import com.google.code.kaptcha.NoiseProducer;
import com.google.code.kaptcha.util.Configurable;
import com.jhlabs.image.DiffuseFilter;
import com.jhlabs.image.RippleFilter;
import com.jhlabs.image.ShearFilter;
import com.jhlabs.image.TransformFilter;

public class RippleEngine extends Configurable implements GimpyEngine {
	public BufferedImage getDistortedImage(BufferedImage baseImage) {
		NoiseProducer noiseProducer = getConfig().getNoiseImpl();
		BufferedImage distortedImage = new BufferedImage(baseImage.getWidth(), baseImage.getHeight(),
				BufferedImage.TYPE_INT_ARGB);

		Graphics2D graphics = (Graphics2D) distortedImage.getGraphics();

		RippleFilter rippleFilter = new RippleFilter();
		rippleFilter.setWaveType(RippleFilter.SINE);
		rippleFilter.setXAmplitude(2.6f);
		rippleFilter.setYAmplitude(1.7f);
		rippleFilter.setXWavelength(15);
		rippleFilter.setYWavelength(5);
		rippleFilter.setEdgeAction(TransformFilter.NEAREST_NEIGHBOUR);
		DiffuseFilter bf = new DiffuseFilter();
		bf.setScale(0.8f);

		ShearFilter sf = new ShearFilter();
		sf.setYAngle(-0.05f);
		
		BufferedImage effectImage = bf.filter(baseImage, null);
		effectImage = sf.filter(baseImage, null);
		effectImage = rippleFilter.filter(effectImage, null);

		graphics.drawImage(effectImage, 0, 0, null, null);

		graphics.dispose();

		noiseProducer.makeNoise(distortedImage, .1f, .1f, .25f, .25f);
		noiseProducer.makeNoise(distortedImage, .1f, .25f, .5f, .9f);
		return distortedImage;
	}
}
