package com.game.web.vcode.kaptcha;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.game.util.UserUtil;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.impl.NoNoise;
import com.google.code.kaptcha.impl.ShadowGimpy;
import com.google.code.kaptcha.impl.WaterRipple;
import com.google.code.kaptcha.text.impl.DefaultTextCreator;

public class KaptchaVerifyCode {
	/**
	 * 纯数字扭曲
	 */
	private static DefaultKaptcha numberKaptcha = null;
	static {
		numberKaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new DefaultTextCreator());
		config.setCharString(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new ShadowGimpy());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		int fontSize = 26;
		config.setFonts(new Font[] { new Font("Times New Roman", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Trattatello", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Arial", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Verdana", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		config.setWidth(80);
		config.setHeight(35);
		numberKaptcha.setConfig(config);
	}

	public static String createNumberVerifyCode() throws IOException {
		String text = numberKaptcha.createText();
		ImageIO.write(numberKaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	/**
	 * 5个纯数字扭曲
	 */
	private static DefaultKaptcha number5Kaptcha = null;
	static {
		number5Kaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new DefaultTextCreator());
		config.setCharString(new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' });
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new WaterRipple());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		config.setCharLength(5);
		int fontSize = 24;
		config.setFonts(new Font[] { new Font("Times New Roman", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Arial", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Verdana", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		config.setWidth(80);
		config.setHeight(30);
		number5Kaptcha.setConfig(config);
	}

	public static String createNumber5VerifyCode() throws IOException {
		String text = number5Kaptcha.createText();
		ImageIO.write(number5Kaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	/**
	 * 字符扭曲
	 */
	private static DefaultKaptcha charKaptcha = null;
	static {
		charKaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new DefaultTextCreator());
		config.setCharString("abcde234hijkrst5678gfymnpwx".toCharArray());
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new RippleEngine());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		int fontSize = 26;
		config.setFonts(new Font[] { new Font("Times New Roman", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Trattatello", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Arial", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Verdana", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		config.setWidth(80);
		config.setHeight(30);
		charKaptcha.setConfig(config);
	}

	public static String createCharVerifyCode() throws IOException {
		String text = charKaptcha.createText();
		ImageIO.write(charKaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	/**
	 * 5个字符扭曲
	 */
	private static DefaultKaptcha char5Kaptcha = null;
	static {
		char5Kaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new DefaultTextCreator());
		config.setCharString("abcde234hijkrst5678gfymnpwx".toCharArray());
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new WaterRipple());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		config.setCharLength(5);
		int fontSize = 24;
		config.setFonts(new Font[] { new Font("Times New Roman", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Arial", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Trattatello", Font.ITALIC | Font.BOLD, fontSize),
				new Font("Verdana", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		config.setWidth(80);
		config.setHeight(30);
		char5Kaptcha.setConfig(config);
	}

	public static String createChar5VerifyCode() throws IOException {
		String text = char5Kaptcha.createText();
		ImageIO.write(char5Kaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	private static String chinese_str = "的一了是我不在人们有来他这上着个地到大里说就去子得也和那要下看天时过出小么起你都把好还多没为又可家学只以主会样年想生同老中十从自面前头道它后然走很像见两用她国动进成回什边作对开而己些现山民候经发工向事命给长水几义三声于高手知理眼志点心战二问但身方实吃做叫当住听革打呢真全才四已所敌之最光产情路分总条白话东席次亲如被花口放儿常气五第使写军吧文运再果怎定许快明行因别飞外树物活部门无往船望新带队先力完却站代员机更九您每风级跟笑啊孩万少直意夜比阶连车重便斗马哪化太指变社似士者干石满日决百原拿群究各六本思解立河村八难早论吗根共让相研今其书坐接应关信觉步反处记将千找争领或师结块跑谁草越字加脚紧爱等习阵怕月青半火法题建赶位唱海七女任件感准张团屋离色脸片科倒睛利世刚且由送切星导晚表够整认响雪流未场该并底深刻平伟忙提确近亮轻讲农古黑告界拉名呀土清阳照办史改历转画造嘴此治北必服雨穿内识验传业菜爬睡兴形量咱观苦体众通冲合破友度术饭公旁房极南枪读沙岁线野坚空收算至政城劳落钱特围弟胜教热展包歌类渐强数乡呼性音答哥际旧神座章帮啦受系令跳非何牛取入岸敢掉忽种装顶急林停息句区衣般报叶压慢叔背细";
	/**
	 * 中文扭曲
	 */
	private static DefaultKaptcha normalChinalKaptcha = null;
	static {
		normalChinalKaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new ChineseText());
		config.setCharString(chinese_str.toCharArray());
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new NoGimpyEngine());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		config.setWidth(100);
		config.setHeight(30);
		int fontSize = 20;
		config.setFonts(new Font[] { new Font("SimHei", Font.ITALIC | Font.BOLD, fontSize),
				new Font("SimSun", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		normalChinalKaptcha.setConfig(config);
	}

	public static String createNormalChineseVerifyCode() throws IOException {
		String text = normalChinalKaptcha.createText();
		ImageIO.write(normalChinalKaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	/**
	 * 中文扭曲
	 */
	private static DefaultKaptcha chineseKaptcha = null;
	static {
		chineseKaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new ChineseText());
		config.setCharString(chinese_str.toCharArray());
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new RippleEngine());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		config.setWidth(100);
		config.setHeight(30);
		int fontSize = 20;
		config.setFonts(new Font[] { new Font("SimHei", Font.ITALIC | Font.BOLD, fontSize),
				new Font("SimSun", Font.ITALIC | Font.BOLD, fontSize),
				new Font("AaStrawberrySister", Font.ITALIC | Font.BOLD, fontSize),
				new Font("AaMissLemon", Font.ITALIC | Font.BOLD, fontSize),
				new Font("woziku-mmxysm-CN6967", Font.ITALIC | Font.BOLD, fontSize),
				new Font("JST", Font.ITALIC | Font.BOLD, fontSize),
				new Font("STHupo", Font.ITALIC | Font.BOLD, fontSize),
				new Font("ZoomlaYingXing-A024", Font.ITALIC | Font.BOLD, fontSize),
				new Font("STXingkai", Font.ITALIC | Font.BOLD, fontSize) });
		config.setFontSize(fontSize);
		chineseKaptcha.setConfig(config);
	}

	public static String createChineseVerifyCode() throws IOException {
		String text = chineseKaptcha.createText();
		ImageIO.write(chineseKaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

	/**
	 * 中文扭曲2
	 */
	private static DefaultKaptcha chinese1Kaptcha = null;
	static {
		chinese1Kaptcha = new DefaultKaptcha();
		KaptchaVerifyCodeConfig config = new KaptchaVerifyCodeConfig();
		config.setTextProducer(new ChineseText());
		config.setCharString(chinese_str.toCharArray());
		config.setRenderer(new KaptchaWordRenderer());
		config.setNoiseProducer(new NoNoise());
		config.setGimpyEngine(new WaterRipple());
		Color color = new Color(247, 247, 247);
		config.setColorFrom(color);
		config.setColorTo(color);
		config.setWidth(100);
		config.setHeight(30);
		int fontSize = 20;
		config.setFonts(new Font[] {new Font("SimHei", Font.ITALIC | Font.BOLD, fontSize),
				new Font("SimSun", Font.ITALIC | Font.BOLD, fontSize)});
		config.setFontSize(fontSize);
		chinese1Kaptcha.setConfig(config);
	}

	public static String createChinese1VerifyCode() throws IOException {
		String text = chinese1Kaptcha.createText();
		ImageIO.write(chinese1Kaptcha.createImage(text), "jpg", UserUtil.getResponse().getOutputStream());
		return text;
	}

}
