package com.game.web.vcode.kaptcha;

import java.awt.image.BufferedImage;

import com.google.code.kaptcha.GimpyEngine;
import com.google.code.kaptcha.util.Configurable;

public class NoGimpyEngine extends Configurable implements GimpyEngine {
	public BufferedImage getDistortedImage(BufferedImage baseImage) {
		return baseImage;
	}
}
