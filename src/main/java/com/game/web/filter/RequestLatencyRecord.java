package com.game.web.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * @author terry.jiang[taoj555@163.com] on 2021-04-25.
 */
@Data
@AllArgsConstructor
public class RequestLatencyRecord {

    private String path;

    private Long latency;

    private String bean;

    private String method;

    private Long requestTime;
}
