
package com.game.web.filter;

import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSONObject;
import com.game.constant.StationConfig;
import com.game.model.AgentOnlinepayLog;
import com.game.service.AgentOnlinepayLogService;
import com.game.util.HttpUtils;
import com.game.util.StationConfigUtil;
import com.game.util.StationUtil;
import com.game.util.UserUtil;

public class OnlinepayNotifyInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private AgentOnlinepayLogService agentOnlinepayLogService;

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception arg3) throws Exception {
		Long stationId = StationUtil.getStationId();
		String onoff_log_onlinepay_notify = StationConfigUtil.get(stationId, StationConfig.onoff_log_onlinepay_notify);
		if("on".equals(onoff_log_onlinepay_notify)){
			/*
			ByteArrayOutputStream inBuffer = new ByteArrayOutputStream();
			InputStream input = request.getInputStream();
			byte[] tmp = new byte[1024];
			int len = 0;
			while ((len = input.read(tmp)) > 0) {
				inBuffer.write(tmp, 0, len);
			}
			byte[] requestData = inBuffer.toByteArray();
			String httpContent = new String(requestData, "UTF-8");
			*/
			String httpContent = (String) request.getSession().getAttribute("HTTP_BODY");

			String notifyIp = UserUtil.getIpAddress();
			String notifyUrl = HttpUtils.getCurrentUrl(request);
			String method = request.getMethod().toUpperCase();
			String payType = notifyUrl.substring(notifyUrl.indexOf("/onlinepay/") + "/onlinepay/".length(), notifyUrl.indexOf("/notify.do"));

			Map<String, String[]> parameMap = request.getParameterMap();
			String parameJson = JSONObject.toJSONString(parameMap);

			JSONObject headers = new JSONObject();
			Enumeration<String> headerNames = request.getHeaderNames();
			while(headerNames.hasMoreElements()){
				String headerName = headerNames.nextElement();
				String headerValue = request.getHeader(headerName);
				headers.put(headerName, headerValue);
			}
			String headerJson = headers.toJSONString();

			AgentOnlinepayLog model = new AgentOnlinepayLog();
			model.setContent(httpContent);
			model.setCreateDatetime(new Date());
			model.setHeaders(headerJson);
			model.setMethod(method);
			model.setNotifyIp(notifyIp);
			model.setNotifyUrl(notifyUrl);
			model.setParams(parameJson);
			model.setPayType(payType);
			model.setStationId(stationId);
			agentOnlinepayLogService.save(model);
			// System.out.println(httpContent);
			// System.out.println(notifyIp);
			// System.out.println(notifyUrl);
			// System.out.println(method);
			// System.out.println(payType);
			// System.out.println(parameJson);
			// System.out.println(headerJson);
		}
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2, ModelAndView view) throws Exception {
		// System.out.println(2);
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		// System.out.println(3);
		return true;
	}

}
