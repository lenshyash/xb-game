package com.game.web.filter;

import java.util.regex.Pattern;

public class XssUtil {

	private static Pattern[] patterns = new Pattern[] {
			// Script, Style fragments
//			Pattern.compile("(<style>|<script>)((.|[\\r\\n])*?)(</style>|</script>)", Pattern.CASE_INSENSITIVE),
			// // src='...', href='...', style='...'
			// Pattern.compile("(style|href|src)((\\s|[\\r\\n])*?)=(\\s|[\\r\\n])*(\\'|\\\")((.|[\\r\\n])*?)(\\'|\\\")",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // DOM
			// Pattern.compile("(document|attr|element|nodemap|nodelist|text|entity|namemap).(.*?)[\\s|=|)]",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // lonely script and style tags
			// Pattern.compile("</script>|</style>", Pattern.CASE_INSENSITIVE),
			// Pattern.compile("<script(.*?)|<style(.*?)",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // eval(...), expression(...)
			// Pattern.compile("(eval|expression)\\((.*?)\\)",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // javascript:... , vbscript:...
			// Pattern.compile("(javascript|vbscript):",
			// Pattern.CASE_INSENSITIVE),
			// // on...(...)=...
			// Pattern.compile("on((.|[\\r\\n])*?)=((.|[\\r\\n])*?)[\\'|\\\"]((.|[\\r\\n])*?)[\\'|\\\"]",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // body, img, iframe, input, link, table, td, div, a tag
			// Pattern.compile("<(meta|html|body|iframe|img|input|link|object|a|div|td|th|span)((.|[\\r\\n])*)>",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // html tags
			// Pattern.compile("<((.|[\\r\\n])*?)>((.|[\\r\\n])*?)</((.|[\\r\\n])*)>",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // CDATA
			// Pattern.compile("<!\\[CDATA((.|[\\r\\n])*?)\\]>",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// // everything between < & > if escaped from above patterns
			// Pattern.compile("<((.|[\\r\\n])*)(>|\\\")",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// Pattern.compile("\\[((.|[\\r\\n])*)(\\]|\\\")",
			// Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
			// Pattern.compile("<|>|\\[|\\]|&|\"|'|%|\\(|\\)|\\+",
			// Pattern.MULTILINE | Pattern.DOTALL)
	};

	public static String filter(String s) {
//		for (Pattern scriptPattern : patterns) {
//			s = scriptPattern.matcher(s).replaceAll("");
//		}
		s = s.replaceAll("<", "&lt;");
		return s;
	}
}
