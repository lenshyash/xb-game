package com.game.web.filter;

import com.game.core.SystemConfig;
import com.game.elasticsearch.ElasticApi;
import com.game.elasticsearch.operations.IndexOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author terry.jiang[taoj555@163.com] on 2021-04-25.
 */
@Slf4j
public class LatencyRecordInterceptor extends HandlerInterceptorAdapter {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("startTime", System.currentTimeMillis());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if(!SystemConfig.SYS_ELASTICSEARCH_SERVER_OPEN)
            return;
        Long start = (Long) request.getAttribute("startTime");
        long latency = System.currentTimeMillis() - start;
        HandlerMethod hm = (HandlerMethod) handler;
        ResponseBody rspAnnotation = hm.getMethodAnnotation(ResponseBody.class);
        if (rspAnnotation == null)
            return;
        RequestLatencyRecord latencyRecord = new RequestLatencyRecord(request.getServletPath(), latency,
                hm.getBean().getClass().getName()
                , hm.getMethod().getName(), start);
        ElasticApi.addQueue(new IndexOperation<>("request_latency", ElasticApi.DEFAULT_TYPE, latencyRecord));
        log.debug(request.getServletPath() + " latency :" + latency);
    }
}
