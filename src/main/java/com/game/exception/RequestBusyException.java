package com.game.exception;

import org.jay.frame.exception.PermissionCheckException;
import org.jay.frame.util.JsonUtil;

import java.util.HashMap;
import java.util.Map;

public class RequestBusyException extends PermissionCheckException {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public RequestBusyException() {
      super("请求繁忙，请稍后再试");
    }

    public RequestBusyException(String msg) {
      super(msg);
   }

    public String toJsonMsg() {
        Map result = new HashMap();
        result.put("msg", "请求繁忙，请稍后再试");
        result.put("success", false);
        return JsonUtil.toJson(result);
    }

    public boolean logStackTrace() {
        return false;
    }
}
