package com.game.exception;

import java.util.HashMap;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.JsonUtil;

public class HttpResponseException extends GenericException {

	private int code;

	private String content;

	public String toJsonMsg() {
		Map result = new HashMap();
		result.put("msg", "status[" + this.code + "],content:" + content);
		result.put("success", false);
		return JsonUtil.toJson(result);
	}

	public HttpResponseException(int code) {
		this.code = code;
	}

	public HttpResponseException(int code, String content) {
		this.code = code;
		this.content = content;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
