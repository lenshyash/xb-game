package com.game.exception;

import org.jay.frame.exception.GenericException;

import com.game.util.StationUtil;

public class LoginErrorException extends GenericException {

	private static final String HEADER_CEIPSTATE_FREFIX = "ceipstate:6";

	public static final String LOGIN_ERROR_KEY = "loginerror:";
	public static final int LOGIN_ERROR_KEY_TIMEOUT = 24 * 60 * 60;
	public static final long LOGIN_ERROR_OTHER_LOGIN = 1;

	public static final long LOGIN_ERROR_FORCED_OFFLINE = 2;

	public static final long LOGIN_ERROR_IP_CHANGED = 3;

	private String msg;

	private Long errorCode;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public LoginErrorException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public LoginErrorException(String msg, Long errorCode) {
		super(msg);
		this.msg = msg;
		this.errorCode = errorCode;
	}

	public String[] getResponseHeader() {
		return new String[] { HEADER_CEIPSTATE_FREFIX };
	}

	private static String getStationName() {
		String station = "";
		if (StationUtil.isAgentStation()) {
			station = "agent";
		} else if (StationUtil.isDailiStation()) {
			station = "daili";
		}
		return station;
	}

	public static String getRedisKeyBySid(String sid) {
		return sid + ":" + LOGIN_ERROR_KEY + getStationName();
	}

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}
}
