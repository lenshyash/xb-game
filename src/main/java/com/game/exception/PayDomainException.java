package com.game.exception;

import java.util.HashMap;
import java.util.Map;

import org.jay.frame.exception.GenericException;
import org.jay.frame.util.ActionUtil;
import org.jay.frame.util.JsonUtil;


/**
 * 不是一个有效站点
 * @author admin
 *
 */
public class PayDomainException extends GenericException{
	
	private String domain;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public PayDomainException(String domain) {
		super("Pay Domain ["+domain+"] 404 not found");
		this.domain = domain;
	}

    public String toJsonMsg() {
    	Map result = new HashMap(); 
		result.put("msg","Pay Domain 404 not found!");
		result.put("success", false);
		return JsonUtil.toJson(result);
    }
    
	public String[] getResponseHeader() {
		return new String[]{ActionUtil.HEADER_CEIPSTATE_BIZ};
	}
}