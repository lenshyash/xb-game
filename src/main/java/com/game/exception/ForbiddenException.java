package com.game.exception;

import org.jay.frame.exception.GenericException;

public class ForbiddenException extends GenericException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForbiddenException() {
      super("Your IP has been forbidden by server");
    }
   
    public ForbiddenException(String msg) {
      super(msg);
   }
}
