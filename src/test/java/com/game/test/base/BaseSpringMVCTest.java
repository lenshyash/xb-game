package com.game.test.base;

import org.jay.frame.FrameProperites;
import org.jay.frame.filter.SysThreadData;
import org.jay.frame.filter.ThreadVariable;
import org.jay.frame.jdbc.DBType;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试  
@ContextConfiguration   
({"classpath:applicationContext.xml","classpath:game-servlet.xml"}) //加载配置文件  
public class BaseSpringMVCTest {
	 // 模拟request,response  
    protected MockHttpServletRequest request;  
    protected MockHttpServletResponse response;
    
    @Autowired
    protected ApplicationContext ctx;
    
    // 执行测试方法之前初始化模拟request,response  
    @Before    
    public void setUp(){    
        
        FrameProperites.JDBC_SHOW_SQL = true;
        FrameProperites.DB_TYPE = DBType.POSTGRESQL;
 
    }
    
}
