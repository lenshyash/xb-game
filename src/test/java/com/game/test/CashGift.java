package com.game.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.game.util.MD5Util;

public class CashGift {
	public static class OverException extends Exception {

	}

	private final int totalNumber;
	private final int totalMoney;
	private final AtomicInteger index = new AtomicInteger();
	private volatile int remainMoney;
	private volatile int min;
	private volatile int max;

	public CashGift(int totalNumber, int totalMoney) {
		this.totalNumber = totalNumber;
		this.totalMoney = totalMoney;
		this.remainMoney = totalMoney;
		this.min = 1;
		handlerValue();
	}

	protected void handlerValue() {
		int sy = totalNumber - index.get();
		if (sy == 0) {
			return;
		}
		this.max = remainMoney / sy * 2;
	}

	protected int rand(int x) {
		return (int) (Math.random() * x);
	}

	protected int getMoney(int ticket) {

		if (ticket == totalNumber) {
			return remainMoney;
		}

		int current = rand(max);
		if (current < min) {
			current = min;
		}
		remainMoney -= current;
		handlerValue();
		return current;
	}

	public int get() throws OverException {
		int ticket = index.incrementAndGet();
		if (ticket > totalNumber) {
			throw new OverException();
		} else {
			return getMoney(ticket);
		}
	}
}
