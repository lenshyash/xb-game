package com.game.test;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.JsonUtil;
import org.jay.frame.util.MixUtil;
import org.jay.frame.util.StringUtil;
import org.jay.frame.util.Validator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import com.game.model.MemberDepositInfo;
import com.game.model.MemberWithdrawInfo;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;
import com.game.test.base.BaseJunit4Test;
import com.game.test.dao.AccountDao;
import com.game.test.dao.AccountInfoDao;
import com.game.test.dao.DepositInfoDao;
import com.game.test.dao.MoneyDao;
import com.game.test.dao.WithdrawInfoDao;
import com.game.test.util.ReadExcel;
import com.game.util.DateUtil;
import com.game.util.MD5Util;

public class ImportMembers extends BaseJunit4Test {

	private Logger log = Logger.getLogger(ImportMembers.class);

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private AccountInfoDao infoDao;

	@Autowired
	private MoneyDao moneyDao;
	
	@Autowired
	private DepositInfoDao depositInfoDao;
	
	@Autowired
	private WithdrawInfoDao withdrawInfoDao;

	String DATETIME_SDF = "yyyy-MM-dd HH:mm:ss";// 时间格式
	String SYS_ACCOUNT_ACCOUNT = "account";// 用户名
	String SYS_ACCOUNT_AGENTNAME = "agentname";// 上线代理
	String SYS_ACCOUNT_PASSWORD = "password";// 密码
	String SYS_ACCOUNT_REGISTERIP = "registerip";// 注册IP
	String SYS_ACCOUNT_REGISTERURL = "registerurl";// 注册网址
	String SYS_ACCOUNT_REGISTEROS = "registeros";// 注册系统
	String SYS_ACCOUNT_SCORE = "score";// 扣分
	String SYS_ACCOUNT_LASTLOGINIP = "lastloginip";// 最后登录时间
	String SYS_ACCOUNT_LASTLOGINDATETIME = "lastlogintime";// 最后登录时间
	String SYS_ACCOUNT_CREATEDATETIME = "createtime";// 创建时间
	String SYS_ACCOUNT_REMARK = "remark";// 备注
	String SYS_ACCOUNT_BETNUM = "betnum";// 打码量
	String SYS_ACCOUNT_DRAWNEED = "drawneed";// 提款所需
	String SYS_ACCOUNT_RATE = "rate";// 赔率值
	String SYS_ACCOUNT_REPORTTYPE = "reporttype";// 报表类型
	String SYS_ACCOUNT_CHATTOKEN = "levelgroup";// 聊天室token
	String SYS_ACCOUNT_LASTSIGNDATE = "lastsigndate";// 最后签到时间
	String SYS_ACCOUNT_SIGNCOUNT = "signcount";// 签到次数
	

	String SYS_ACCOUNT_INFO_USERNAME = "username";// 真实姓名
	String SYS_ACCOUNT_INFO_CARDNO = "cardno";// 银行卡号
	String SYS_ACCOUNT_INFO_RECEIPTPWD = "receiptpwd";// 取款密码
	String SYS_ACCOUNT_INFO_PHONE = "phone";// 手机
	String SYS_ACCOUNT_INFO_BANKADDRESS = "bankaddress";// 银行地址
	String SYS_ACCOUNT_INFO_QQ = "qq";// QQ
	String SYS_ACCOUNT_INFO_EMAIL = "email";// email
	String SYS_ACCOUNT_INFO_BANKNAME = "bankname";// 银行名称
	String SYS_ACCOUNT_INFO_PROVINCE = "province";// 省份
	String SYS_ACCOUNT_INFO_CITY = "city";// 城市
	String SYS_ACCOUNT_INFO_WECHAT = "wechat";// 微信
	// String SYS_ACCOUNT_INFO_PROMOCODE = "";// 推广码

	String MNY_MONEY_MONEY = "money";// 金额
	String MNY_MONEY_BALANCEGEMMONEY = "balancegemmoney";// 余额宝余额
	String MNY_MONEY_BALANCEGEMINCOME = "balancegemincome";// 余额宝盈利总额
	
	
	String MEMBER_DEPOSIT_INFO_DEPOSITCOUNT  = "depositcount";// 充值次数
	String MEMBER_DEPOSIT_INFO_DEPOSITTOTAL = "deposittotal";// 充值总额
	
	String MEMBER_WITHDRAW_INFO_WITHDRAWCOUNT  = "withdrawcount";// 提款次数
	String MEMBER_WITHDRAW_INFO_WITHDRAWTOTAL = "withdrawtotal";// 提款总额

	long stationId = 102L;//站点ＩＤ
//	String password = "123456";//初始密码
	long levelId = 2;//固定值
	long levelGroup = 447L;//默认分层的ＩＤ
	
//	String zongdaili = "seng1718";
	
	List<Map> accountCols = null;
	List<Map> accountInfoCols = null;
	List<Map> moneyCols = null;
	List<Map> depositInfoCols = null;
	List<Map> withdrawInfoCols = null;
	

	long method_insert = 1;
	long method_batch = 2;

	@Test
	public void testMain() throws Exception {

		accountCols = new ArrayList<>();
		accountCols.add(MixUtil.newHashMap("col", "account", "fileName", SYS_ACCOUNT_ACCOUNT));
		accountCols.add(MixUtil.newHashMap("col", "password", "fileName", SYS_ACCOUNT_PASSWORD));
		accountCols.add(MixUtil.newHashMap("col", "agentName", "fileName", SYS_ACCOUNT_AGENTNAME));
		accountCols.add(MixUtil.newHashMap("col", "registerIp", "fileName", SYS_ACCOUNT_REGISTERIP));
		accountCols.add(MixUtil.newHashMap("col", "registerUrl", "fileName", SYS_ACCOUNT_REGISTERURL));
		accountCols.add(MixUtil.newHashMap("col", "registerOs", "fileName", SYS_ACCOUNT_REGISTEROS));
		accountCols.add(MixUtil.newHashMap("col", "score", "fileName", SYS_ACCOUNT_SCORE));
		accountCols.add(MixUtil.newHashMap("col", "lastLoginDatetime", "fileName", SYS_ACCOUNT_LASTLOGINDATETIME));
		accountCols.add(MixUtil.newHashMap("col", "lastLoginIp", "fileName", SYS_ACCOUNT_LASTLOGINIP));
		accountCols.add(MixUtil.newHashMap("col", "createDatetime", "fileName", SYS_ACCOUNT_CREATEDATETIME));
		accountCols.add(MixUtil.newHashMap("col", "remark", "fileName", SYS_ACCOUNT_REMARK));
		accountCols.add(MixUtil.newHashMap("col", "betNum", "fileName", SYS_ACCOUNT_BETNUM));
		accountCols.add(MixUtil.newHashMap("col", "drawNeed", "fileName", SYS_ACCOUNT_DRAWNEED));
		accountCols.add(MixUtil.newHashMap("col", "rate", "fileName", SYS_ACCOUNT_RATE));
		accountCols.add(MixUtil.newHashMap("col", "reportType", "fileName", SYS_ACCOUNT_REPORTTYPE));
		accountCols.add(MixUtil.newHashMap("col", "chatToken", "fileName", SYS_ACCOUNT_CHATTOKEN));
		accountCols.add(MixUtil.newHashMap("col", "lastSignDate", "fileName", SYS_ACCOUNT_LASTSIGNDATE));
		accountCols.add(MixUtil.newHashMap("col", "signCount", "fileName", SYS_ACCOUNT_SIGNCOUNT));
		

		accountInfoCols = new ArrayList<>();
		accountInfoCols.add(MixUtil.newHashMap("col", "userName", "fileName", SYS_ACCOUNT_INFO_USERNAME));
		accountInfoCols.add(MixUtil.newHashMap("col", "cardNo", "fileName", SYS_ACCOUNT_INFO_CARDNO));
		accountInfoCols.add(MixUtil.newHashMap("col", "receiptPwd", "fileName", SYS_ACCOUNT_INFO_RECEIPTPWD));
		accountInfoCols.add(MixUtil.newHashMap("col", "phone", "fileName", SYS_ACCOUNT_INFO_PHONE));
		accountInfoCols.add(MixUtil.newHashMap("col", "bankAddress", "fileName", SYS_ACCOUNT_INFO_BANKADDRESS));
		accountInfoCols.add(MixUtil.newHashMap("col", "qq", "fileName", SYS_ACCOUNT_INFO_QQ));
		accountInfoCols.add(MixUtil.newHashMap("col", "bankName", "fileName", SYS_ACCOUNT_INFO_BANKNAME));
		accountInfoCols.add(MixUtil.newHashMap("col", "wechat", "fileName", SYS_ACCOUNT_INFO_WECHAT));
		accountInfoCols.add(MixUtil.newHashMap("col", "email", "fileName", SYS_ACCOUNT_INFO_EMAIL));
		accountInfoCols.add(MixUtil.newHashMap("col", "province", "fileName", SYS_ACCOUNT_INFO_PROVINCE));
		accountInfoCols.add(MixUtil.newHashMap("col", "city", "fileName", SYS_ACCOUNT_INFO_CITY));
		
		// accountInfoCols.add(MixUtil.newHashMap("col", "promoCode", "fileName",
		// SYS_ACCOUNT_INFO_PROMOCODE));

		moneyCols = new ArrayList<>();
		moneyCols.add(MixUtil.newHashMap("col", "money", "fileName", MNY_MONEY_MONEY));
		moneyCols.add(MixUtil.newHashMap("col", "balanceGemMoney", "fileName", MNY_MONEY_BALANCEGEMMONEY));
		moneyCols.add(MixUtil.newHashMap("col", "balanceGemIncome", "fileName", MNY_MONEY_BALANCEGEMINCOME));
		
		
		depositInfoCols = new ArrayList<>();
		depositInfoCols.add(MixUtil.newHashMap("col", "depositCount", "fileName", MEMBER_DEPOSIT_INFO_DEPOSITCOUNT));
		depositInfoCols.add(MixUtil.newHashMap("col", "depositTotal", "fileName", MEMBER_DEPOSIT_INFO_DEPOSITTOTAL));
		
		withdrawInfoCols = new ArrayList<>();
		withdrawInfoCols.add(MixUtil.newHashMap("col", "withdrawCount", "fileName", MEMBER_WITHDRAW_INFO_WITHDRAWCOUNT));
		withdrawInfoCols.add(MixUtil.newHashMap("col", "withdrawTotal", "fileName", MEMBER_WITHDRAW_INFO_WITHDRAWTOTAL));
		
		nowDate = new Date();
//		importDailis();
		importMems();
//		importGuides();
	}

	Date nowDate = null;
	Set<String> dailiSet = new HashSet<String>();
	Map<String, Map<String, String>> dataMap = new HashMap<>();

	List<Map> faileList = new ArrayList<>();
	List<SysAccount> accoutnLst = null;
	List<SysAccountInfo> accoutnInfoLst = null;
	List<MnyMoney> moneyLst = null;
	List<MemberDepositInfo> depositLst = null;
	List<MemberWithdrawInfo> withdrawLst = null;

	Map<String, SysAccount> accountMap = new HashMap<>();

	public void importMems() throws Exception {
		handler(SysAccount.ACCOUNT_PLATFORM_MEMBER);
	}

	public void importDailis() throws Exception {
		handler(SysAccount.ACCOUNT_PLATFORM_AGENT);
	}
	
	public void importGuides() throws Exception {
		handler(SysAccount.ACCOUNT_PLATFORM_GUIDE);
	}
	
	public void handler(Long accountType) throws Exception {
		int blank = 0;
		int success = 0;
		int failure = 0;
		List<List<Map<String, String>>> list = ReadExcel.readExcelWithTitle("C:/Users/Administrator/Desktop/102m.xls");
		for (List<Map<String, String>> users : list) {
			accoutnLst = new ArrayList<>();
			accoutnInfoLst = new ArrayList<>();
			moneyLst = new ArrayList<>();
			depositLst = new ArrayList<>();
			withdrawLst = new ArrayList<>();
			
			int length = users.size();
			log.error("一共" + length + "行记录");
			for (int i = 0; i < length; i++) {
				Map<String, String> row = users.get(i);
				dataMap.put(StringUtil.trim2Null(row.get(SYS_ACCOUNT_ACCOUNT)), row);
				dailiSet.add(StringUtil.trim2Null(row.get(SYS_ACCOUNT_AGENTNAME)));
			}

			for (int i = 0; i < length; i++) {
				Map<String, String> row = users.get(i);
				boolean flag = this.saveAccount(row, accountType);
				if (flag) {
					++success;
				} else {
					++failure;
				}
				System.out.println(JsonUtil.toJson(row));
			}
			if (success > 0 && accoutnLst.size() > 0) {
				accountDao.batchInsert(accoutnLst);
				infoDao.batchInsertInfos(accoutnLst, accoutnInfoLst);
				moneyDao.batchInsertMoneys(accoutnLst, moneyLst);
				depositInfoDao.batchInsertMoneys(accoutnLst, depositLst);
				withdrawInfoDao.batchInsertMoneys(accoutnLst, withdrawLst);
				
			}
		}
//		accountDao.updateFlagActive(stationId);
		System.out.println(JsonUtil.toJson(faileList));
	}

	public boolean saveAccount(Map<String, String> userData, Long accountType) {

//		if (StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT)) {
//			return save(userData, accountType, method_insert) != null;
//		}
		return save(userData, accountType, method_insert) != null;
	}

	public <T> T converToModel(List<Map> cols, Map dataMap, Class<T> clazz) {

		T model = null;
		String key = "";
		String fileKey = "";
		Object value = null;
		try {
			model = (T) clazz.newInstance();

			for (Map map : cols) {
				key = map.get("col").toString();
				fileKey = map.get("fileName").toString();
				value = StringUtil.trim2Empty(dataMap.get(fileKey));
				if(value instanceof String) {
					value = value.toString().trim();
				}
				if ("lastLoginDatetime".equalsIgnoreCase(key) 
						|| "createDatetime".equalsIgnoreCase(key)
						|| "lastSignDate".equalsIgnoreCase(key)
						) {
					value = DateUtil.parseDate(value.toString(), DATETIME_SDF);
					if (value == null) {
						value = nowDate;
					}
				} else if ("money".equalsIgnoreCase(key) 
						|| "score".equalsIgnoreCase(key)
						|| "balanceGemMoney".equalsIgnoreCase(key)
						|| "balanceGemIncome".equalsIgnoreCase(key)
						|| "depositTotal".equalsIgnoreCase(key)
						|| "withdrawTotal".equalsIgnoreCase(key)
						|| "rate".equalsIgnoreCase(key)
						|| "drawNeed".equalsIgnoreCase(key)
						|| "betNum".equalsIgnoreCase(key)
						) {
					value = StringUtil.toBigDecimal(value);
				}else if ("reportType".equalsIgnoreCase(key) 
						|| "depositCount".equalsIgnoreCase(key)
						|| "withdrawCount".equalsIgnoreCase(key)
						|| "signCount".equalsIgnoreCase(key)
						){
					value = StringUtil.toLong(value);
				}

				if (value != null && value != "") {
					PropertyUtils.setProperty(model, key, value);
				}
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		return model;
	}

	public SysAccount saveOne(Map<String, String> userData, Long accountType) {

		return save(userData, accountType, method_insert);
	}

	public SysAccount save(Map<String, String> userData, Long accountType, long method) {
		SysAccount account = converToModel(accountCols, userData, SysAccount.class);
		if(account == null || StringUtil.isEmpty(account.getAccount())) {
			return null;
		}
		account.setStationId(stationId);// 站点id
		if (accountMap.containsKey(account.getAccount()) || accountDao.isNotUnique(account, "stationId,account")) {
			faileList.add(MixUtil.newHashMap(account.getAccount(), "用户已存在"));
			return null;
		}

		String daili = account.getAgentName();
//		if(StringUtil.equals(accountType, SysAccount.ACCOUNT_PLATFORM_AGENT) 
//				&& StringUtil.isEmpty(daili) 
//				&& !zongdaili.equals(account.getAccount())) {
//			daili = zongdaili;
//		}
		SysAccount agent = getAgent(daili);
		String parentNames = "";
		String parents = "";
		String agentName = "";
		long agentId = 0l;
		if(agent != null) {
			parentNames = agent.getParentNames();
			parents = agent.getParents();
			agentName = agent.getAccount();
			agentId = agent.getId();
			if (StringUtil.isEmpty(parentNames)) {
				parentNames = "," + agentName + ",";
			} else {
				parentNames += agentName + ",";
			}
			if (StringUtil.isEmpty(parents)) {
				parents = "," + agentId + ",";
			} else {
				parents += agentId + ",";
			}
		}

		account.setLevel(levelId);
		account.setAccountStatus(SysAccount.ACCOUNT_STATUS_ENABLED);
		account.setAccountType(accountType);
//		account.setPassword(MD5Util.getMD5String(account.getAccount(), account.getPassword()));// 密码
		account.setLevelGroup(levelGroup);
		account.setParents(parents);
		account.setParentNames(parentNames);
		account.setAgentName(agentName);
		account.setAgentId(agentId);
		if(account.getCreateDatetime() == null) {
			account.setCreateDatetime(nowDate);
		}
		if(account.getLastLoginDatetime() == null) {
			account.setLastLoginDatetime(nowDate);
		}
		if (StringUtil.equals(method, method_insert)) {
			accountDao.insert(account);
		} else {
			accoutnLst.add(account);
		}

		SysAccountInfo sysAccountInfo = converToModel(accountInfoCols, userData, SysAccountInfo.class);
		sysAccountInfo.setAccountId(account.getId());
		sysAccountInfo.setStationId(stationId);
		sysAccountInfo.setAccount(account.getAccount());
		if (StringUtil.equals(method, method_insert)) {
			infoDao.insert(sysAccountInfo);
		} else {
			accoutnInfoLst.add(sysAccountInfo);
		}

		MnyMoney mnyMoney = converToModel(moneyCols, userData, MnyMoney.class);
		mnyMoney.setAccountId(account.getId());
		mnyMoney.setMoneyTypeId(MnyMoney.MONEY_RMB);
		if (StringUtil.equals(method, method_insert)) {
			moneyDao.insert(mnyMoney);
		} else {
			moneyLst.add(mnyMoney);
		}
		
		MemberDepositInfo depositInfo = converToModel(depositInfoCols, userData, MemberDepositInfo.class);
		depositInfo.setAccountId(account.getId());
		if (StringUtil.equals(method, method_insert)) {
			depositInfoDao.insert(depositInfo);
		} else {
			depositLst.add(depositInfo);
		}
		
		MemberWithdrawInfo withdrawInfo = converToModel(withdrawInfoCols, userData, MemberWithdrawInfo.class);
		withdrawInfo.setAccountId(account.getId());
		if (StringUtil.equals(method, method_insert)) {
			withdrawInfoDao.insert(withdrawInfo);
		} else {
			withdrawLst.add(withdrawInfo);
		}
		accountMap.put(account.getAccount(), account);
		return account;
	}

	public SysAccount getAgent(String daili) {
		if (StringUtil.isEmpty(daili)) {
			return null;
		}
		SysAccount agent = accountMap.get(daili);
		if (agent != null && Validator.isNotNull(agent.getId())) {
			return agent;
		}
		agent = accountDao.getByAccountAndTypeAndStationId(daili, stationId, SysAccount.ACCOUNT_PLATFORM_AGENT);
		if(agent == null) {
			Map dailiMap = dataMap.get(daili);
			if(dailiMap == null || dailiMap.isEmpty()) {
				dailiMap = MixUtil.newHashMap(SYS_ACCOUNT_ACCOUNT,daili);
			}
			agent = this.saveOne(dailiMap, SysAccount.ACCOUNT_PLATFORM_AGENT);
		}
		return agent;
	}

	
}
