package com.game.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jay.frame.util.StringUtil;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.game.test.util.ReadExcel2;

public class CreateEgameJS {

	private Logger log = Logger.getLogger(CreateEgameJS.class);

	@Test
	public void testMain() throws Exception {
		// List<List<Map<String,String>>> list =
		// ReadExcel.readExcelWithTitle("/Users/admin/Downloads/c7.xlsx");
		// List<Map<String,String>> users = list.get(0);
		// for (int i = 294; i < 300; i++) {
		// System.out.println(users.get(i));
		// }
		//
		// saveUser(now,"test10001","王五");
		// updateLevel();
		// importMems();
		createJs();
	}

	public void createJs() throws Exception {
		String[] titles = { "GameID", "ChineseName" };
		File file = null;
		FileWriter fw = null;
		BufferedWriter bw = null;
		List<List<Map<String, String>>> list = ReadExcel2.readExcelWithTitle("/Users/apple/Downloads/qt.xlsx");
		String key = "";
		String value = "";
		List<Map<String, String>> loopList = null;
		Map<String, String> gameMap = null;

		// 普通游戏
		List<Map<String, String>> qtGames = new ArrayList<Map<String, String>>();

		try {
			file = new File("/Users/apple/Documents/qtGame.js");
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (int i = 0; i < list.size(); i++) {
				loopList = list.get(i);
				for (Map<String, String> row : loopList) {
					gameMap = new HashMap<String, String>();

					if (i == 0) {
						gameMap.put("typeid", "1");
					} else if (i == 1) {
						gameMap.put("typeid", "4");
					} else if (i == 2) {
						gameMap.put("typeid", "2");
					} else if (i == 3) {
						gameMap.put("typeid", "3");
					}

					for (int j = 0; j < titles.length; j++) {
						key = titles[j];
						value = row.get(key);
						if (StringUtil.isEmpty(value)) {
							continue;
						}
						createGameMap(key, value, gameMap);
					}
					if (!"0".equals(gameMap.get("DisplayName"))) {
						qtGames.add(gameMap);
					}
				}
			}

			bw.write("qtGames=" + JSON.toJSONString(qtGames));

			bw.flush();
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		log.error("一共" + qtGames.size() + "行记录");
	}

	public void createGameMap(String key, String value, Map<String, String> gameMap) {

		switch (key) {
		case "ChineseName":
			gameMap.put("DisplayName", value);
			break;
		case "GameID":
			gameMap.put("ButtonImagePath", "qt/v2/" + value.replace(" ", "") + ".jpg");
			gameMap.put("LapisId", value);
			break;
		default:
			break;
		}
	}
}
