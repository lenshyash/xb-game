package com.game.test.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;
import com.game.model.SysAccountInfo;

@Repository
public class AccountInfoDao extends JdbcUtilImpl<SysAccountInfo>{
	public void batchInsertInfos(final List<SysAccount> accountList, final List<SysAccountInfo> infos) {
		StringBuilder sql = new StringBuilder("INSERT INTO sys_account_info(");
		sql.append(" account_id,account,station_id,user_name,card_no,receipt_pwd,phone,bank_address,qq,bank_name,wechat)");
		sql.append(" SELECT a.id,a.account,station_id,?,?,?,?,?,?,?,? FROM sys_account a");
		sql.append(" WHERE a.station_id = ? AND a.account = ?;");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccount a = accountList.get(i);
				SysAccountInfo info = infos.get(i);
				int k = 1;
				ps.setString(k++, info.getUserName());
				ps.setString(k++, info.getCardNo());
				ps.setString(k++, info.getReceiptPwd());
				ps.setString(k++, info.getPhone());
				ps.setString(k++, info.getBankAddress());
				ps.setString(k++, info.getQq());
				ps.setString(k++, info.getBankName());
				ps.setString(k++, info.getWechat());
				ps.setLong(k++, a.getStationId());
				ps.setString(k++, a.getAccount());
			}

			@Override
			public int getBatchSize() {
				return infos.size();
			}
		});
	}
}
