package com.game.test.dao;

import java.util.List;
import java.util.Map;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.jay.frame.util.MixUtil;
import org.springframework.stereotype.Repository;

import com.game.model.SysAccount;

@Repository
public class AccountDao extends JdbcUtilImpl<SysAccount>{
	
	public SysAccount getByAccountAndTypeAndStationId(String account, Long stationId, Long accountType) {
		StringBuilder sql_sb = new StringBuilder();
		sql_sb.append("SELECT * FROM sys_account WHERE flag_active >= 1 AND account = :account");

		Map paramMap = MixUtil.newHashMap();
		paramMap.put("account", account);
		sql_sb.append(" AND station_id = :stationId");
		paramMap.put("stationId", stationId);

		sql_sb.append(" AND account_type=:type");
		paramMap.put("type", accountType);
		List<SysAccount> accounts = super.query2Model(sql_sb.toString(), paramMap);
		if (accounts == null || accounts.size() == 0) {
			return null;
		}
		return accounts.get(0);
	}
	
	public void updateFlagActive(Long stationId) {
		StringBuilder sql = new StringBuilder("UPDATE sys_account SET");
		sql.append(" flag_active = 1");
		sql.append(" WHERE flag_active is null and station_id = :stationId");
		super.update(sql.toString(),MixUtil.newHashMap("stationId",stationId));
	}
}