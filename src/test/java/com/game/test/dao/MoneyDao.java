package com.game.test.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.model.MnyMoney;
import com.game.model.SysAccount;

@Repository
public class MoneyDao extends JdbcUtilImpl<MnyMoney>{
	public void batchInsertMoneys(final List<SysAccount> accountList, final List<MnyMoney> moneys) {
		StringBuilder sql = new StringBuilder("INSERT INTO mny_money (");
		sql.append(" account_id,money,money_type_id)");
		sql.append(" SELECT a.id,?,? FROM sys_account a");
		sql.append(" WHERE a.station_id = ? AND account = ?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccount a = accountList.get(i);
				MnyMoney info = moneys.get(i);
				int k = 1;
				ps.setBigDecimal(k++, info.getMoney());
				ps.setLong(k++, info.getMoneyTypeId());
				ps.setLong(k++, a.getStationId());
				ps.setString(k++, a.getAccount());
			}

			@Override
			public int getBatchSize() {
				return moneys.size();
			}
		});
	}
}
