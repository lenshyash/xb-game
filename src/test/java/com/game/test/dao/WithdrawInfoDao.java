package com.game.test.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.model.MemberDepositInfo;
import com.game.model.MemberWithdrawInfo;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;

@Repository
public class WithdrawInfoDao extends JdbcUtilImpl<MemberWithdrawInfo>{
	public void batchInsertMoneys(final List<SysAccount> accountList, final List<MemberWithdrawInfo> infos) {
		StringBuilder sql = new StringBuilder("INSERT INTO member_withdraw_info (");
		sql.append(" account_id,withdraw_count,withdraw_total)");
		sql.append(" SELECT a.id,?,? FROM sys_account a");
		sql.append(" WHERE a.station_id = ? AND account = ?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccount a = accountList.get(i);
				MemberWithdrawInfo info = infos.get(i);
				int k = 1;
				ps.setLong(k++, info.getWithdrawCount());
				ps.setBigDecimal(k++, info.getWithdrawTotal());
				ps.setLong(k++, a.getStationId());
				ps.setString(k++, a.getAccount());
			}

			@Override
			public int getBatchSize() {
				return infos.size();
			}
		});
	}
}
