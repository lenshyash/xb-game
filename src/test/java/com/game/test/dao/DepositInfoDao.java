package com.game.test.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.jay.frame.jdbc.JdbcUtilImpl;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import com.game.model.MemberDepositInfo;
import com.game.model.MnyMoney;
import com.game.model.SysAccount;

@Repository
public class DepositInfoDao extends JdbcUtilImpl<MemberDepositInfo>{
	public void batchInsertMoneys(final List<SysAccount> accountList, final List<MemberDepositInfo> infos) {
		StringBuilder sql = new StringBuilder("INSERT INTO member_deposit_info (");
		sql.append(" account_id,deposit_count,deposit_total)");
		sql.append(" SELECT a.id,?,? FROM sys_account a");
		sql.append(" WHERE a.station_id = ? AND account = ?");
		getJdbcOperations().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				SysAccount a = accountList.get(i);
				MemberDepositInfo info = infos.get(i);
				int k = 1;
				ps.setLong(k++, info.getDepositCount());
				ps.setBigDecimal(k++, info.getDepositTotal());
				ps.setLong(k++, a.getStationId());
				ps.setString(k++, a.getAccount());
			}

			@Override
			public int getBatchSize() {
				return infos.size();
			}
		});
	}
}
