package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;

public class HlbpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		
		map.put("merchantId", "3845551");
		map.put("orderId", "3596229797152768");
		map.put("transAmt", "9999.00");
		map.put("orderTime", DateUtil.getCurrentTime());
		map.put("status", "1");
		map.put("transId", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("returnParams", "3845551");

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"sign".equals(entry.getKey())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		buffer.append("key=de8924b64dcf802c0d6c91294a0518b0");
		
		
		map.put("sign", MD5.encryption(buffer.toString()));
		
		com.game.util.HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/hlbpay/notify.do", map);
		
	}

}
