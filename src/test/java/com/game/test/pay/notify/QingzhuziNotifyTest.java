package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils;
import com.game.util.HttpClientUtils;

public class QingzhuziNotifyTest {

	@Test
	public void test(){
		Map<String, String> map = new HashMap<>();
		
		map.put("money","262.00");
		map.put("bank","ALIPAY");
		map.put("order","3703509181679616");
		String signSrc = String.format("bank=%s&money=%s&order=%s&notify=%s&uid=%s&key=%s", 
				map.get("bank"),map.get("money"),map.get("order"),"http://b.com:8080/game/onlinepay/qingzhuzi/notify.do","1005","34c57b681ca18fdc6246b36d47bdde1a");
		String md5= OnlinepayUtils.MD5(signSrc, "utf-8").toLowerCase();
		
		map.put("md5",md5);
		
		String url = "http://b.com:8080/game/onlinepay/qingzhuzi/notify.do?money="+map.get("money")+"&bank="+map.get("bank")+"&order="+map.get("order")+"&md5="+map.get("md5");
		
		String content = HttpClientUtils.getInstance().sendHttpGet(url);
		
		Assert.assertEquals("ok", content);
	}
}
