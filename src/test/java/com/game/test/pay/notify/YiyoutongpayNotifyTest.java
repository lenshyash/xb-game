package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class YiyoutongpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("billno", "3715049150007296");
		map.put("amount", "11.00");
		map.put("merchant_id", "2000003001");
		map.put("order_date", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("order_id", "0000000000000");
		map.put("success", "SUCCESS");
		
		String buffer = String.format("amount=%s&billno=%s&merchant_id=%s&order_date=%s"
				+ "&order_id=%s&success=%s" + 
				"&sign_key=%s", 
				map.get("amount"),map.get("billno"),map.get("merchant_id"),map.get("order_date"),
				map.get("order_id"),map.get("success"),"ce7adbf27e434a44a1e6f126d770dd0d");
		
		String sign = MD5.encryption(buffer);
		map.put("sign", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://b.com/game/onlinepay/yiyoutongpay/notify.do", map);
	}

}
