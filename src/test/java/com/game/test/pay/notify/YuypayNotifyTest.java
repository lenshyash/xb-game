package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class YuypayNotifyTest {
	
	@Test
	public void test(){
	            String url = "http://d.com:8080/game/onlinepay/yuypay/notify.do";
	            
	            Map<String, String> obj = new HashMap<String,String>();
	            obj.put("orderNo", "3514033142777856");
	            obj.put("status", "200");
	            obj.put("payoverTime", "2017-08-21 19:22:54");
	            obj.put("orderAmount", "10.000000");
	            obj.put("payType", "WEIXIN");
	            obj.put("orderStatus", "SUCCESS");
	            obj.put("sign", "2f88162f90514490528c95d3d51bc83d");
	            
	            HttpClientUtils.getInstance().sendHttpPost(url, obj);

	}

}
