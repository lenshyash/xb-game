package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class ChangchengpayNotifyTest {
	
	@Test
	public void test(){
		
		//?=&=&=&=&=&=&=&=&=&=&=&=&=
		Map<String, String> map = new HashMap<String , String>();
		map.put("merchno", "211440347330004");
		map.put("status", "1");
		map.put("traceno", "3526428548352000");
		map.put("orderno", "10000004555989");
		map.put("merchName", "CC00076A");
		map.put("channelOrderno", "");
		map.put("amount", "1.00");
		map.put("transDate", "2017-08-30");
		map.put("channelTraceno", "");
		map.put("transTime", "13:31:32");
		map.put("payType", "2");
		map.put("signature", "E1CD05406528205571B2275A635A3513");
		map.put("openId", "weixin://wxpay/bizpayurl?pr=PLvs4R8");
		
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/jinanfu_scan/notify.do",map);
	}

}
