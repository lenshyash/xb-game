package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.junit.Test;

import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class Caimao9NotifyTest {

	@Test
	public void test() {

		// 
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("merchant_code", "1111110166");
		map.put("notify_id", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("sign_type", "RSA-S");
		map.put("order_no", "3562296170563584");
		map.put("trade_status", "SUCCESS");
		

		StringBuffer buffer = new StringBuffer();
		
		Set<Entry<String, String>> set = map.entrySet();
		
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"sign".equals(entry.getKey()) && !"sign_type".equals(entry.getKey()) && StringUtils.isNotEmpty(entry.getValue())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		
		try {
			String sign = RSAWithSoftware.signByPrivateKey(buffer.toString(), "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAL4InfiBwkj5QBmhPKzyP/RKAMWNeHk7CljcSuQ6ZlVRfHDOFKkGqHdSytEMF4gCBqgixqK86uI+jES7sXFshv9YFnAU5UToexWzlHPxgSYr6tcj3tcPfPPQcBkXpq9OAKEySL0qfb8EiPkDjyErHmLTXp/JUuAP7nvnkExqNa89AgMBAAECgYAfF8PZHEoQrI+bYfFOjth9/PnBWRNkKo/X2Dt8uj1yKkIFJwLkYQ2BcI90wti6zTIPopkDmYuI9xj2SguPdmVj62yhWhzbQvss8067HN7BRoCuWEAL04vXSWDyM3IAQNWMHrVb+rn07fcJK3GIL/TIx4Yhn7WdRjZR3PAULTVJYQJBAPkT4OjbDTEuzwBS4Jo20NYJLvWkmmIGmWQg6gRDoZnPDkDA+I/45xhOE76BzWwNaywlUAqpz5Kf4Yklc/3KqTUCQQDDUKf7sLSUmUVsYdG1jDjMzGmuKJGJA8lJzlKRIELyV5syn4GQR04BSaV9tkSNBvh1I3adhImnW9PNpbc7XrbpAkBrGo4q8w6wKHWgqQKlrY6NivNjc31jS30oHjMi1EYoEm4JkDESD7CnteMtlzlTDk4YdI7/lteIMDHWPIGtDzlFAkEAkA+mx6NvnodqyGViO8lgf9ZqQJYVunz7dB/lkLu+BWgOxgkWlKezZom+sfnFmEvC6QuuSsLekX3OvMl1XCohOQJBAIw1TDCv2a71UcYG4FEbqlzxQD7J9p5gak7pSa4jX1hGlKAeO+uwDWJ9wk4KaLtQ9r8IawEEbFSBUku8baCPqPk=");
			map.put("sign", sign);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/caimao9/notify.do", map);
		
	}

}
