package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class Dinh5payNotifyTest {
	
	private String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKnz3jeOxgjb/4+gsF0upS8a2hePNWirpUVVghNNe3nNW6DWQhnRIZmWrAH0HZCOnEjS0/na7R+Xxb6jFAJnKr6wt8cRcX2kTyF4VjXkhkBD+KjDqAaBlOEr/VwXlw5yfnC+tpm5X6yitqjtD2jOhDvfVTMWeJUC+CGtCnsV2+UDAgMBAAECgYAGrMtXh0YD5xAchgUnQ8O9G+Lg9gD1AXjWRkt7kpl86THmGUQzW2FkdOSAlyPqH4/H1G/66jxR91YO7LzGc2LcVefiUQb0NgT8PIiLlqiS7u3joSQAorIWjjIeTbVNHs57c8/2R6NxBdt63srjV2ktwC/mGE5bfQXjTes7FLUEYQJBAPHjX2P4Tl+yK5j2tYDWqaHs8as955PzF6yo2f1rhgws4LrIISy6teq0EqvLvCHepjJyocB3HzVIasnzU9zttysCQQCz3iEsDdkKz0KAagggb/ojdK9vLUakKVIfMEGsAxI8MFGHEqnrWrQnJupeIXLC340vaAJ/IrLuXrl9/WTkLx2JAkA+L/epSwBTSAo/ZEYbnpS0BafIH70240uV3z9UEN3AqZUJUER/LG1yruMR+eofRw+xbpk5UybZ2wlhbEaGSTNLAkBFKiiVWSnZx0MqQBup5YVJdwgy9zUP0Tfb+GF4dKHhdS8Ugq/GadWWrJc45SHNH8uZrCQxoTUvBkvLz9GpERkJAkEApF0fwJTzuUZemMgTXfpCnidTc8pIMuf1rOFx8ni9FLI/1nXmhJfP6bt8iQqH5iWCz8WWwhQg+elNSdBreiNZFw==";
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("merchant_code", "1118004517");
		map.put("notify_type", "offline_notify");
		map.put("notify_id", UUID.randomUUID().toString());
		map.put("interface_version", "V3.0");
		map.put("sign_type", "RSA-S");
		map.put("order_no", "3578878726752256");
		map.put("order_time", DateUtil.getCurrentTime());
		map.put("order_amount", "9999.00");
		map.put("trade_status", "SUCCESS");
		map.put("trade_no", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("trade_time", DateUtil.getCurrentTime());
		StringBuffer buffer = new StringBuffer();
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator()	;
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!entry.getKey().equals("sign_type")) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		String signSrc = buffer.substring(0, buffer.length()-1);
		
		try {
			String sign = RSAWithSoftware.signByPrivateKey(signSrc, privateKey);
			map.put("sign", sign);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/dinh5pay/notify.do", map);
		
	}

}
