package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class JufuyunNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("memberid", "10015");
		map.put("orderid", "3692090215630848");
		map.put("amount", "111.00");
		map.put("datetime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("returncode", "00");
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("amount=>").append(map.get("amount"))
		.append("&datetime=>").append(map.get("datetime"))
		.append("&memberid=>").append(map.get("memberid"))
		.append("&orderid=>").append(map.get("orderid"))
		.append("&returncode=>").append(map.get("returncode"));
		buffer.append("&key=").append("WWCpZsdIjujHdCo8reiJKa323Dm2XJ");
		
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/jufuyun/notify.do", map);
	}

}
