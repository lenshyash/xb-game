package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class CmbcposNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		
		map.put("notifyTime", "20161117153333");
		map.put("merNo", "MS0000002134272");
		map.put("transId", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("reqId", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("orderNo", "3509866109634560");
		map.put("amount", "99999900");
		map.put("outOrderNo", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("channelFlag", "00");
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		StringBuffer buffer = new StringBuffer();
		
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"signIn".equals(entry.getKey())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		String signSrc = buffer.substring(0, buffer.length()-1) + "fc18e768e0b83ccd204adc2b808790f4";
		
		String sign =  MD5.encryption(signSrc).toUpperCase();
		
		map.put("signIn", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/cmbcpos/notify.do", map);
	}

}
