package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class AllinepayNotifyTest {
	
	@Test
	public void test(){
		String partner = "10000000";
		String ordernumber = "3627046075303936";
		String orderstatus = "1";
		String paymoney = "10.63";
		String sysnumber = UUID.randomUUID().toString().replaceAll("-", "");
		String attach = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signsrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", 
				partner,ordernumber,orderstatus,paymoney,"70dd74a5957f64465e2f02e53948749a"
				);
		
		String sign = MD5.encryption(signsrc);
		
		String url = "http://d.com:8080/game/onlinepay/allinepay/notify.do?partner="+partner+
				"&ordernumber="+ordernumber+"&orderstatus="+orderstatus+"&paymoney="+paymoney
				+"&sysnumber="+sysnumber+"&attach="+attach+"&sign="+sign;
		
		String content = HttpClientUtils.getInstance().sendHttpGet(url);
		
		
		Assert.assertEquals("ok", content);
	}

}
