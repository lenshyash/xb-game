package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class GaotongpayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String, String>();
		
		map.put("partner", "11078");
		map.put("ordernumber", "3588916818757632");
		map.put("orderstatus", "1");
		map.put("paymoney", "9999.00");
		map.put("sysnumber", UUID.randomUUID().toString().replaceAll("-", ""));
		
		map.put("attach", UUID.randomUUID().toString().replaceAll("-", ""));
		
		String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", 
				map.get("partner"),map.get("ordernumber"),map.get("orderstatus"),map.get("paymoney"),"44b3b6b29d58a487dbafbd6a29fcdd17");
		
		map.put("sign", MD5.encryption(signSrc));
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/gaotongpay/notify.do", map);
	}

}
