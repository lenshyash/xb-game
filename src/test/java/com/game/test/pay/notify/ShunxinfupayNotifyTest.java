package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class ShunxinfupayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("apiName", "PAY_RESULT_NOTIFY");
		map.put("notifyTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("tradeAmt", "3597404418148352");
		map.put("merchNo", "70000201");
		map.put("merchParam", "");
		map.put("orderNo", "3692428606408704");
		map.put("tradeDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("accNo", "012356789");
		map.put("accDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("orderStatus", "1");
		map.put("notifyType", "1");
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("apiName=").append(map.get("apiName"))
		.append("&notifyTime=").append(map.get("notifyTime"))
		.append("&tradeAmt=").append(map.get("tradeAmt"))
		.append("&merchNo=").append(map.get("merchNo"))
		.append("&merchParam=").append(map.get("merchParam"))
		.append("&orderNo=").append(map.get("orderNo"))
		.append("&tradeDate=").append(map.get("tradeDate"))
		.append("&accNo=").append(map.get("accNo"))
		.append("&accDate=").append(map.get("accDate"))
		.append("&orderStatus=").append(map.get("orderStatus"))
		.append("745485d20eb183810811526153e3820a");
		map.put("signMsg", MD5.encryption(buffer.toString()).toUpperCase());
		
		HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/shunxinfupay/notify.do", map);
	}

}
