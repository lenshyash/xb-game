package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class VnetoneNotifyTest {
	
	@Test
	public void test(){
		String spid ="8435";
		String oid = UUID.randomUUID().toString().replaceAll("-", "");
		String sporder = "3563721729509376";
		String mz = "9999.00";
		String spuid = System.currentTimeMillis()+"";
		
		String md5str = oid+sporder+spid+mz+"f768ca3966624ecca5";
		
		String md5 = MD5.encryption(md5str).toUpperCase();
		
		String url ="http://d.com:8080/game/onlinepay/vnetone/notify.do?spid="+spid+"&oid="+oid+"&sporder="+sporder+"&mz="+mz+"&spuid="+spuid+"&md5="+md5;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
		
		
	}

}
