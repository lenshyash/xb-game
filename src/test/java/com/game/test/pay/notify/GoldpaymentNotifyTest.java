package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class GoldpaymentNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("success", "1");
		map.put("appid", "800031");
		map.put("type", "pay");
		map.put("state", "1");
		map.put("order_id", "3668015967488000");
		map.put("billno", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("remark", "abcd");
		map.put("amount", "6700");
		map.put("time", DateUtil.getCurrentTime());
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator  = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!StringUtils.equals("sign", entry.getKey()) && StringUtils.isNotEmpty(entry.getValue())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		buffer.append("key=vcPjVpSWwFNKonaJH8XUDTyrMl23Q6i0");
		
		map.put("sign", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/goldpayment/notify.do",map);

		Assert.assertEquals("success", content);
		
	}

}
