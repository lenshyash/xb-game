package com.game.test.pay.notify;

import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.common.onlinepay.OnlinepayUtils;
import com.game.util.HttpClientUtils;

public class QianfupayNotifyTest {

	@Test
	public void test() {
		String xml = "{'orderid':'3713392874014720','opstate':'0','ovalue':'12'}";
		String url = "http://b.com/game/onlinepay/qianfupay/notify.do";
		Map map = JSONObject.parseObject(xml);
		String signSrc = String.format("orderid=%s&opstate=%s&ovalue=%s%s", map.get("orderid"), map.get("opstate"), map.get("ovalue"), "d5ffc5fa1091498e8f8bb5955a71b195");

		String sign = OnlinepayUtils.MD5(signSrc, "GB2312").toLowerCase();
		map.put("sign", sign);
		String content = HttpClientUtils.getInstance().sendHttpPost(url, map);
	}
}
