package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class YizhibankNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("merchantOutOrderNo", "3525342124394496");
		map.put("merid", "20170818002");
		map.put("msg", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("noncestr", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("orderNo", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("payResult", "1");

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=").append("GMWh5Q4mBa8TE3AhU5rirEeQvYLNvhgU");

		String sign = MD5.encryption(buffer.toString());
		
		map.put("sign", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/yizhibank/notify.do",map);
		
	}

}
