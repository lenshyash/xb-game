package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class WefupayNotifyTest {
	
	@Test
	public void test() throws Exception{
		
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("trade_no", "1000876242");
		map.put("sign_type", "RSA-S");
		map.put("notify_type", "offline_notify");
		map.put("merchant_code", "108777004555");
		map.put("order_no", "3499992095066112");
		map.put("trade_status", "SUCCESS");
		map.put("sign", "EdYW3IJy4vGcDk8L3AxLFeoEvjiHG1VvIRu69iPHNrT9ZYO9jE6LKqsXC1yaXxgzTAF7AF/n9T/KiheIe5AZgVHkK468bOSNHg9wVwMS+wACy9dNGUW3KKKR6yxWQPJScyEv8GFQCDZR5p9SQlCHIXlIdRO/u7fEYY3qRAHjXaU=");
		map.put("order_amount", "1");
		map.put("interface_version", "V3.0");
		map.put("bank_seq_no", "C1046874649");
		map.put("order_time", "2017-08-11 21:18:59");
		map.put("notify_id", "8c2f0fa96f424e208eb245f782a711f2");
		map.put("trade_time", "2017-08-11 21:18:59");
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/wefupay/notify.do",map);
		
	}

}
