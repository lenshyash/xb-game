package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class YunxunpayNotifyTest {

	@Test
	public void test(){
		String partner = "102400001";
		String ordernumber = "3575008149850112";
		String orderstatus = "1";
		String paymoney = "9999.00";
		String sysnumber = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", partner,ordernumber,orderstatus,paymoney,"bdb4602cb32c7ec6621c1b98ba2b9142");
		
		String sign = MD5.encryption(signSrc);
		
		String url = "http://d.com:8080/game/onlinepay/yunxunpay/notify.do?partner="+partner+"&ordernumber="+ordernumber+"&orderstatus="+orderstatus+"&paymoney="+paymoney+"&sysnumber="+sysnumber+"&sign="+sign;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
		
	}
}
