package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class ZhihuifuNotifyTest {

	@Test
	public void test() {
		//merchant_code=&order_no=&trade_status=SUCCESS&sign=&order_amount=10&interface_version=V3.0&bank_seq_no=&order_time=&notify_id=&trade_time=
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("trade_no", "Z1085621628");
		map.put("sign_type", "RSA-S");
		map.put("notify_type", "offline_notify");
		map.put("merchant_code", "Z800888889028");
		map.put("order_no", "3524099055503360");
		map.put("trade_status", "SUCCESS");
		map.put("sign", "dW071urUf30S/4vnAKHk3AcUllWe8u0oVpBaiqsWb9SUkFel9SnQJJeIViDUIew26UDI5AvpmrfDhU7vKxvNQwY/71M41qFvn6Xc2mzfdF1QD1ptnLcAETTQMYR+B5hR0CwkWSwNkWA7pm4TE958vnKKz6/kZqpGQXMuFEiEDqA=");
		map.put("order_amount", "10");
		map.put("interface_version", "V3.0");
		map.put("bank_seq_no", "102542033622201708282126557063");
		map.put("order_time", "2017-08-28 22:01:51");
		map.put("notify_id", "6a4e7fccfd8e46c1a333cd962a980bd2");
		map.put("trade_time", "2017-08-28 22:01:44");

		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/dinpay30/notify.do",map);
		
		
		
	}

}
