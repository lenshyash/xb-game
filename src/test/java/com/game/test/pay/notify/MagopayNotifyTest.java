package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class MagopayNotifyTest {
	
	@Test
	public void test(){
		//code=0000&msg=成功.&order_sn=&down_sn=&status=2&amount=&fee=&trans_time=&sign=
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("code", "0000");
		map.put("msg", "成功.");
		map.put("order_sn","20171012161800216776");
		map.put("down_sn", "3587462034032640");
		map.put("status", "2");
		map.put("amount", "10.00");
		map.put("fee", "0.25");
		map.put("trans_time", "20171012161834");	
		map.put("sign", "93f073de56909ee838f924a0becd84d0");	
		
		
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/magopay/notify.do", map);
	}
}
