package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class ZongxiongwangluoNotifyTest {
	
	@Test
	public void test(){
		String status = "1";
		String number = "3616078028785664";
		String money = "11.35";
		String tradetime = System.currentTimeMillis()+"";
		
		String paysn = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signsrc = "156" + number + money + tradetime + "6ed205045f41c1c74526cd60cdb9bb";
		
		String sign = MD5.encryption(signsrc);
		
		Map<String, String> map = new HashMap<>();
		map.put("status", status);
		map.put("number", number);
		map.put("money", money);
		map.put("tradetime", tradetime);
		map.put("paysn", paysn);
		map.put("sign", sign);
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/zongxiongwangluo/notify.do", map);
		
		System.out.println(content);
		
	}

}
