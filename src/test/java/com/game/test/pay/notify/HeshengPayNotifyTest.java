package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class HeshengPayNotifyTest {
	
	@Test
	public void test(){
		Map<String,String> map = new HashMap<String, String>();
		map.put("service", "TRADE.NOTIFY");
		map.put("merId", "2017072631010041");
		map.put("tradeNo", "3485535855609856");
		map.put("tradeDate", "20170801");
		map.put("opeNo", "44045");
		map.put("opeDate", "20170801");
		map.put("amount", "20.00");
		map.put("status", "1");
		map.put("extra", "");
		map.put("payTime", "20170801161335");
		map.put("sign", "C19A5B7800DFA340B86D518C7219F035");
		map.put("notifyType", "1");
	HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/heshengpay/notify.do", map);
	}

}
