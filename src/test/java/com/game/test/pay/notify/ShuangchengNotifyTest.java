package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.http.client.HttpClient;
import org.bouncycastle.crypto.ec.ECNewPublicKeyTransform;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class ShuangchengNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("buyer_email", "13165478987");
		map.put("buyer_id", "m13055873726@163.com");
		map.put("gmt_create", "2012-06-17 00:00:00");
		map.put("gmt_payment", "2012-06-17 00:00:00");
		map.put("is_success", "T");
		map.put("notify_id", "2c25390c0fb24f3393f846ada3251ac4");
		map.put("notify_time", "2012-06-1700:00:0");
		map.put("notify_type", "WAIT_TRIGGER");
		map.put("out_trade_no", "3485793777387520");
		map.put("order_no", "3485793777387520");
		map.put("payment_type", "1");
		map.put("seller_email", "lxy5166@126.com");
		map.put("seller_id", "100000000000108");
		map.put("subject", "数码产品");
		map.put("total_fee", "1.00");
		map.put("trade_no", "101105170002572");
		map.put("trade_status", "TRADE_FINISHED");
		
		String signSrc = "";
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while(iterator.hasNext()){
			Entry<String, String> entry = iterator.next();
			signSrc += entry.getKey()+"=" + entry.getValue()+ "&";
		}
		signSrc = signSrc.substring(0, signSrc.length()-1);
		String sign = MD5.encryption(signSrc+"68de3e3297a998dg997f9fabfbbb607aa2e73e23gbec27b7d79435bbfe9e4c42");
		map.put("sign", sign);
		map.put("sign_type", "MD5");
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/shuangchengpay/notify.do",map);
	}

}
