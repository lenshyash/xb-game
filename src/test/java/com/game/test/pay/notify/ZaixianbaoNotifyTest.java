package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class ZaixianbaoNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		//=&=&=&=&=&=&=
		map.put("amount", "300.00");
		map.put("merchno", "666110158140006");
		map.put("status", "2");
		map.put("channelTraceno", "8021800863283170831159413539");
		map.put("orderno", "3528347440678912");
		map.put("signature", "C95E18656CA874099E5F3936B108B5EC");
		map.put("channelOrderno", "2026");
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/zaixianbao/notify.do", map);
	
	}

}
