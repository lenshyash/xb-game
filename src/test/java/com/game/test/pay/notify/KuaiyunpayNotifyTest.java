package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class KuaiyunpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("memberid", "10011");
		map.put("amount", "9999.00");
		map.put("orderid", "3717716259047424");
		map.put("datetime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("returncode", "00");
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=").append("dnuo89d7b37991vgljv5q7dc1nhazxaz");
		
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		HttpClientUtils.getInstance().sendHttpPost("http://b.com/game/onlinepay/kuaiyunpay/notify.do", map);
	}

}
