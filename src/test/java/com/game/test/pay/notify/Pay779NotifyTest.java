package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class Pay779NotifyTest {

	@Test
	public void test() {
		String partner = "27640";
		String ordernumber = "3609019134937088";
		String orderstatus = "1";
		String paymoney = "9.11";
		String sysnumber = UUID.randomUUID().toString().replaceAll("-", "");
		String attach = UUID.randomUUID().toString().replaceAll("-", "");

		String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", partner, ordernumber, orderstatus, paymoney, "1234567890");

		String sign = MD5.encryption(signSrc);

		String url = "http://d.com:8080/game/onlinepay/pay779/notify.do?partner=" + partner + "&ordernumber=" + ordernumber + "&orderstatus=" + orderstatus + "&paymoney=" + paymoney + "&sysnumber=" + sysnumber + "&attach=" + attach + "&sign=" + sign;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
	}
}
