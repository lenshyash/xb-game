package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class YafupayNotifyTest {
	
	@Test
	public void test(){
		String version = "3.0";
		String consumerNo = "20994";
		String merOrderNo = "3645441710999552";
		String orderNo = UUID.randomUUID().toString();
		String transAmt = "5.46";
		String orderStatus = "1";
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("version", version);
		map.put("consumerNo", consumerNo);
		map.put("merOrderNo", merOrderNo);
		map.put("orderNo", orderNo);
		map.put("transAmt", transAmt);
		map.put("orderStatus", orderStatus);
		
		StringBuffer buffer = new StringBuffer();
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=").append("2360C56FDD896F54B05096C2F872DD29");
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/yafupay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
