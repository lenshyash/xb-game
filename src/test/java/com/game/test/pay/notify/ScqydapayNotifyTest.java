package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class ScqydapayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("apiName", "PAY_RESULT_NOTIFY");
		map.put("notifyTime", DateUtil.getCurrentDate());
		map.put("tradeAmt", "9999.99");
		map.put("merchNo", "210001110012991");
		map.put("merchParam", "PAY_RESULT_NOTIFY");
		map.put("orderNo", "3586043114145792");
		map.put("tradeDate", DateUtil.getCurrentDate());
		map.put("accNo", UUID.randomUUID().toString());
		map.put("accDate", DateUtil.getCurrentDate());
		map.put("orderStatus", "1");
		map.put("notifyType", "1");

		String signSrc = String.format("apiName=%s&notifyTime=%s&tradeAmt=%s&merchNo=%s&merchParam=%s&orderNo=%s&tradeDate=%s&accNo=%s&accDate=%s&orderStatus=%s%s", map.get("apiName"), map.get("notifyTime"), map.get("tradeAmt"), map.get("merchNo"), map.get("merchParam"), map.get("orderNo"), map.get("tradeDate"), map.get("accNo"), map.get("accDate"), map.get("orderStatus"), "ccf0f635655974cf6610ac642e3d8dd2");

		String sign = MD5.encryption(signSrc).toUpperCase();

		map.put("signMsg", sign);

		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/scqydapay/notify.do", map);
	}

}
