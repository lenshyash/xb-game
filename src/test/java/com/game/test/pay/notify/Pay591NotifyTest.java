package com.game.test.pay.notify;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;

public class Pay591NotifyTest {

	@Test
	public void test(){
		String url = "http://d.com:8080/game/onlinepay/pay591/notify.do?";
		String param = "orderid=3502932429408256&opstate=0&ovalue=999999.00";
		String signsrc = param + "17d32fcf5c8b48528cbcd3b74636cdb9";
		
		String sign = MD5.encryption(signsrc);
		
		param+= "&sign="+sign;
		param+="&sysorderid="+DateUtil.getCurrentDate("yyyyMMdd");
		try {
			param+="&systime="+URLEncoder.encode(DateUtil.getCurrentTime(),"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		param+="&attach=OnlinePay";
		
		com.game.util.HttpClientUtils.getInstance().sendHttpGet(url+param);
	}
}
