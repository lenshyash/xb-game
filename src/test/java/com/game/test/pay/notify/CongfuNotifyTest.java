package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class CongfuNotifyTest {
	
	@Test
	public void test(){
		String payKey = "2b9877146a764bee877c4580325dd0e4";
		String productName = "onlinepay";
		String outTradeNo = "3685425216571392";
		String orderPrice = "10.01";
		String productType = "50000103";
		String tradeStatus = "SUCCESS";
		String successTime = "20171225222500";
		String orderTime = "20171225222500";
		String trxNo = "345676543456787654";
		String remark = "remark";
		// String sign = "";

		SortedMap<String, String> map = new TreeMap<>();
		map.put("payKey", payKey);
		map.put("productName", productName);
		map.put("outTradeNo", outTradeNo);
		map.put("orderPrice", orderPrice);
		map.put("productType", productType);
		map.put("tradeStatus", tradeStatus);
		map.put("successTime", successTime);
		map.put("orderTime", orderTime);
		map.put("trxNo", trxNo);
		map.put("remark", remark);

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator()	;

		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"sign".equals(entry.getKey())) {
				if (!StringUtils.isEmpty(entry.getValue())) {
					buffer.append(entry.getKey()+"="+entry.getValue()+"&");
				}
			}
		}
		buffer.append("paySecret=42f4dbc55f8e4320a669766c66e8b6a5");

		map.put("sign", MD5.encryption(buffer.toString()));

		String content = HttpClientUtils.getInstance().sendHttpPost("http://127.0.0.1:8080/game/onlinepay/congfu/notify.do", map);

		Assert.assertEquals("SUCCESS", content);
	}
}
