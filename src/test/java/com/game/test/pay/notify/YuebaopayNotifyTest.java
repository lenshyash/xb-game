package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class YuebaopayNotifyTest {

	@Test
	public void test() {
		String partner = "880677";
		String ordernumber = "3579171277899776";
		String orderstatus = "1";
		String paymoney = "9999.00";
		String sysnumber = UUID.randomUUID().toString();
		String attach = "OnlinePay";

		String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", partner, ordernumber, orderstatus, paymoney, "d907727164c5b7d530185ece684b18a9");

		String sign = MD5.encryption(signSrc);

		String url = "http://d.com:8080/game/onlinepay/yuebaopay/notify.do?partner=" + partner + "&ordernumber=" + ordernumber + "&orderstatus=" + orderstatus + "&paymoney=" + paymoney + "&sysnumber=" + sysnumber + "&attach=" + attach + "&sign=" + sign;

		HttpClientUtils.getInstance().sendHttpGet(url);

	}

}
