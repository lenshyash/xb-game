package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class YtbaoNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("transDate", DateUtil.getCurrentDate());
		map.put("transTime", DateUtil.getCurrentTime().substring(11));
		map.put("merchno", DateUtil.getCurrentDate());
		map.put("merchName", DateUtil.getCurrentDate());
		map.put("customerno", DateUtil.getCurrentDate());
		map.put("traceno", "3559422447929344");
		map.put("payType", "1");
		map.put("orderno", DateUtil.getCurrentDate());
		map.put("channelOrderno", DateUtil.getCurrentDate());
		map.put("channelTraceno", DateUtil.getCurrentDate());
		map.put("openId", DateUtil.getCurrentDate());
		map.put("status", "1");
		map.put("cust1", DateUtil.getCurrentDate());
		
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"signature".equals(entry.getKey()) || StringUtils.isNotEmpty(entry.getValue())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}

		buffer.append("18677118DD8C086390DE4299121F2769");

		map.put("signature", MD5.encryption(buffer.toString()));
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/ytbao/notify.do", map);
	}

}
