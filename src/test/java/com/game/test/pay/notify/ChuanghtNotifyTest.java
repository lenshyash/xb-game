package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class ChuanghtNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("apiName", "PAY_RESULT_NOTIFY");
		map.put("notifyTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("tradeAmt", "99999.00");
		map.put("merchNo", "1024");
		map.put("merchParam", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("orderNo", "3499685244291072");
		map.put("tradeDate", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("accNo", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("accDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("orderStatus", "1");
		map.put("notifyType", "1");
		
		
		String signSrc = String.format("apiName=%s&notifyTime=%s&tradeAmt=%s&merchNo=%s&merchParam=%s&orderNo=%s&tradeDate=%s&accNo=%s&accDate=%s&orderStatus=%s%s", 
				map.get("apiName"),map.get("notifyTime"),
				map.get("tradeAmt"),map.get("merchNo"),
				map.get("merchParam"),map.get("orderNo"),map.get("tradeDate"),
				map.get("accNo"),map.get("accDate"),map.get("orderStatus"),	"984ddc6e56ef31a8e1fc1b5b20ced33c");
		
		
		map.put("signMsg", MD5.encryption(signSrc).toUpperCase());
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/chuanght/notify.do",map);
	}

}
