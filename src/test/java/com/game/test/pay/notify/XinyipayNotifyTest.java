package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.crypto.dsig.SignatureMethod;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class XinyipayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("orderNo", "3495350646949888");
		map.put("transactionNo", "3495350646949888");
		map.put("amount", "6666.00");
		
		String signSrc = String.format("amount=%s&orderNo=%s&transactionNo=%s#%s", map.get("amount"),map.get("orderNo"),map.get("transactionNo"),"ba8fb1e1b5004cbfa4cb961ea107f087");
	
		String sign = MD5.encryption(signSrc);
		
		map.put("sign", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/xinyipay/notify.do", map);
		
	}

}
