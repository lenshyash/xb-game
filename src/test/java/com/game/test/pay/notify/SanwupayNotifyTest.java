package com.game.test.pay.notify;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;

public class SanwupayNotifyTest {
	
	@Test
	public void notifyTest(){
		String firstSrc="{\"settleFee\":0,\"settlePeriod\":\"T1\",\"sign\":\"6b7b73aeac83b0799c2a542affabe2d8\",\"payType\":\"ALIPAY_QRCODE_PAY\",\"outTradeNo\":\"3483731828017152\",\"signType\":\"MD5\",\"currency\":\"CNY\",\"payedAmount\":0,\"amount\":16000,\"tradeNo\":\"XGYS612339AQYJ\",\"settleType\":\"SELF\",\"merchantNo\":\"GHWN59000UMOJ\",\"status\":\"WAITING_PAY\"}";
		String srcstr = "{\"settleFee\":0,\"settlePeriod\":\"T1\",\"sign\":\"e76b7a875271a58990a83e321550e219\",\"payType\":\"ALIPAY_QRCODE_PAY\",\"outTradeNo\":\"3483731828017152\",\"signType\":\"MD5\",\"currency\":\"CNY\",\"payedAmount\":16000,\"amount\":16000,\"tradeNo\":\"XGYS612339AQYJ\",\"settleType\":\"SELF\",\"merchantNo\":\"GHWN59000UMOJ\",\"status\":\"SETTLED\"}";
		Map<String, Object> paramMap = new HashMap<String,Object>();
		paramMap = JSONObject.parseObject(srcstr, paramMap.getClass());
		
        try {

            CloseableHttpClient httpclient = HttpClients.createDefault();

            HttpPost httpPost = new HttpPost("http://d.com:8080/game/onlinepay/sanwupay/notify.do");
            httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");

            // 解决中文乱码问题
            StringEntity stringEntity = new StringEntity(srcstr, "UTF-8");
            stringEntity.setContentEncoding("UTF-8");

            httpPost.setEntity(stringEntity);

            // CloseableHttpResponse response =
            // httpclient.execute(httpPost);

            System.out.println("Executing request " + httpPost.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                @Override
                public String handleResponse(final HttpResponse response)
                        throws ClientProtocolException, IOException {//
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {

                        HttpEntity entity = response.getEntity();

                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException(
                                "Unexpected response status: " + status);
                    }
                }
            };
            String responseBody = httpclient.execute(httpPost, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);

        } catch (Exception e) {
            System.out.println(e);
        }
	}

}
