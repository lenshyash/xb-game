package com.game.test.pay.notify;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class Puxun101kaNotifyTest {

	/// puxun101ka/notify.do

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("p1_MerId", "8884670");
		map.put("r0_Cmd", "Buy");
		map.put("r1_Code", "1");
		map.put("r2_TrxId", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("r3_Amt", "9999.00");
		map.put("r4_Cur", "RMB");
		map.put("r5_Pid", "OnlinePay");
		map.put("r6_Order", "3525480447199232");
		map.put("r7_Uid", "");
		map.put("r8_MP", "OnlinePay");
		map.put("r9_BType", "2");
		map.put("rp_PayDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"hmac".equals(entry.getKey())) {
				buffer.append(entry.getValue());
			}
		}

		String hmac = hmacSign(buffer.toString(), "2d19ae54f99048c492677d6616e25ca8");
		
		map.put("hmac", hmac);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/puxun101ka/notify.do", map);

	}

	public static String hmacSign(String aValue, String aKey) {
		byte k_ipad[] = new byte[64];
		byte k_opad[] = new byte[64];
		byte keyb[];
		byte value[];
		try {
			keyb = aKey.getBytes("UTF-8");
			value = aValue.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			keyb = aKey.getBytes();
			value = aValue.getBytes();
		}

		Arrays.fill(k_ipad, keyb.length, 64, (byte) 54);
		Arrays.fill(k_opad, keyb.length, 64, (byte) 92);
		for (int i = 0; i < keyb.length; i++) {
			k_ipad[i] = (byte) (keyb[i] ^ 0x36);
			k_opad[i] = (byte) (keyb[i] ^ 0x5c);
		}

		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {

			return null;
		}
		md.update(k_ipad);
		md.update(value);
		byte dg[] = md.digest();
		md.reset();
		md.update(k_opad);
		md.update(dg, 0, 16);
		dg = md.digest();
		return toHex(dg);
	}

	public static String toHex(byte input[]) {
		if (input == null)
			return null;
		StringBuffer output = new StringBuffer(input.length * 2);
		for (int i = 0; i < input.length; i++) {
			int current = input[i] & 0xff;
			if (current < 16)
				output.append("0");
			output.append(Integer.toString(current, 16));
		}

		return output.toString();
	}

}
