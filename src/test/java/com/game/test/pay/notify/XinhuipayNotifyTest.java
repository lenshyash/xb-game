package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils;
import com.game.util.HttpClientUtils;

public class XinhuipayNotifyTest {
	
	@Test
	public void test(){
		String partner = "667095";
		String ordernumber = "3702379349198848";
		String orderstatus = "1";
		String paymoney = "467.00";
		String sysnumber = UUID.randomUUID().toString();
		String attach = "";
		
		String signsrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", 
				partner,ordernumber,orderstatus,paymoney,"09b1c35e5602fffc7c8e4d5f3d3d9e0c");
		
		String sign = OnlinepayUtils.MD5(signsrc, "GB2312").toLowerCase();
		
		Map<String, String> map = new HashMap<>();
		map.put("partner", partner);
		map.put("ordernumber", ordernumber);
		map.put("paymoney", paymoney);
		map.put("orderstatus", orderstatus);
		map.put("sysnumber", sysnumber);
		map.put("attach", attach);
		map.put("sign", sign);
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/xinhuipay/notify.do",map);
		
		Assert.assertEquals("ok", content);
		
	}

}
