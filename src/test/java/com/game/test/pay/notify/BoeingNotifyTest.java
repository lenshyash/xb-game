package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class BoeingNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("amount", "225");
		map.put("appid", "50000641");
		map.put("orderid", "3693856679954432");
		map.put("status", "2");
		
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=3708ccfeb4e6eefec330ca659b6171be");
	
		map.put("signature", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPostJson("http://b.com:8080/game/onlinepay/boeing/notify.do", JSONObject.toJSONString(map));
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
