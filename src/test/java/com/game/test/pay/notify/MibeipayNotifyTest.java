package com.game.test.pay.notify;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Test;

import com.game.util.HttpClientUtils;

public class MibeipayNotifyTest {
	
	@Test
	public void test(){
		String data = "<xml><trade_type><![CDATA[trade.weixin.h5pay]]></trade_type><total_fee><![CDATA[300]]></total_fee><cash_fee><![CDATA[300]]></cash_fee><transaction_id><![CDATA[500001083689629151037440]]></transaction_id><third_trans_id><![CDATA[100217122309561790]]></third_trans_id><out_trade_no><![CDATA[3689629151037440]]></out_trade_no><time_end><![CDATA[20171223202757]]></time_end><mch_id><![CDATA[50000108]]></mch_id><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><nonce_str><![CDATA[IQpyETN6N4L0qS2T]]></nonce_str><sign><![CDATA[976480809EEA511BA351A9F0FA0F2BB3]]></sign></xml>";
		SortedMap<String, String> map = new TreeMap<String, String>();
		try {
			org.dom4j.Document document = DocumentHelper.parseText(data);
			org.dom4j.Element root = document.getRootElement();
			//map.put(root.getName(), root.getText());
			List<Element> roots = root.elements();
			for (Element element : roots) {
				map.put(element.getName(), element.getText());
			}
		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		}
		
		
		
		//HttpClientUtils.getInstance().sendHttpPostJson("http://b.com:8080/game/onlinepay/mibeipay/notify.do", "");
	}

}
