package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class DingjianNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("orderId","3556569362876416");
		map.put("totalFee","9999.00");
		map.put("type","1008");
		map.put("resultCode","200");
		map.put("resultMessage","成功");

		String str = "parter="+"1609"+"&type="+"1008"+"&value="+"9999.00"+"&orderid="+"3556569362876416"+"&callbackurl="+"http://d.com:8080/game/onlinepay/dingjian/notify.do26af6ed1c7b4474286cd940ee7f62646";
		String userSign = MD5.encryption(str);
		
		map.put("sign", userSign);
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		StringBuffer buffer = new StringBuffer();
		
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		HttpClientUtils.getInstance().sendHttpGet("http://d.com:8080/game/onlinepay/dingjian/notify.do?"+buffer.toString());
		
		
	}
}
