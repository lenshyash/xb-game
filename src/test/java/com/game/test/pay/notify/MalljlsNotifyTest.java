package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class MalljlsNotifyTest {

	@Test
	public void test() {
		String service = "TRADE.NOTIFY";
		String merId = "2017102011010089";
		String tradeNo = "3637315102623744";
		String tradeDate = DateUtil.getCurrentDate("yyyyMMdd");
		String opeNo = UUID.randomUUID().toString().replaceAll("-", "");
		String opeDate = DateUtil.getCurrentDate("yyyyMMdd");
		String amount = "91.44";
		String status = "1";
		String extra = UUID.randomUUID().toString().replaceAll("-", "");
		String payTime = DateUtil.getCurrentTime("yyyyMMddHHmmss");

		String srcMsg = String.format("service=%s&merId=%s&tradeNo=%s&tradeDate=%s&opeNo=%s&opeDate=%s&amount=%s&status=%s&extra=%s&payTime=%s", service, merId, tradeNo, tradeDate, opeNo, opeDate, amount, status, extra, payTime);
		
		String sign = MD5.encryption(srcMsg+"e07690d611f7e5be50a38c3168f8301e").toUpperCase();
		
		Map<String, String> map = new HashMap<>();
		
		map.put("service", service);
		map.put("merId", merId);
		map.put("tradeNo", tradeNo);
		map.put("tradeDate", tradeDate);
		map.put("opeNo", opeNo);
		map.put("opeDate", opeDate);
		map.put("amount", amount);
		map.put("status", status);
		map.put("extra", extra);
		map.put("payTime", payTime);
		map.put("sign", sign);
		map.put("notifyType", "1");
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/malljls/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
	
	
	}

}
