package com.game.test.pay.notify;

import java.util.Map.Entry;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class ZdbbillNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();

		map.put("merchant_code", "9000800101");
		map.put("notify_type", "offline_notify");
		map.put("notify_id", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("interface_version", "V3.0");
		map.put("sign_type", "RSA-S");
		map.put("order_no", "3587850664790016");
		map.put("bank_seq_no", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("trade_status", "SUCCESS");
		map.put("order_time", DateUtil.getCurrentTime());
		map.put("trade_time", DateUtil.getCurrentTime());
		map.put("order_amount", "9999.00");
		map.put("trade_no", UUID.randomUUID().toString().replaceAll("-", ""));

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();

		Iterator<Entry<String, String>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!entry.getKey().equals("sign") && !entry.getKey().equals("sign_type")) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		String signSrc = buffer.substring(0, buffer.length()-1);
		try {
			String sign = RSAWithSoftware.signByPrivateKey(signSrc, "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJVpEy4qutDtEgpS6Ycqr1bqPmETKzXDTKgAvbHyykNdDHlbO6dgUVbjc8GhhMNXBA1kWUJOYmLbj0YZemZnr2LLCJaTrEUs45OskVP6p8AYOEUi+rVmZy32i2VsLM54G5x8FWiBDqMVKNaqbR32JNm+HDZa/S0J0ekiow67DPr7AgMBAAECgYAdhx6bSSaSk8e08p3kcLMmYjThyoAHqAlGSp8yw+DlX0ZP1Y/8jDSHD3yZD+rFEDVk0Cgzynn3P50ZJX/jLSqj9h9P514JtYWUc0Xe43mkuk8LIHi+1XJBMflsI5eSSL8rueugU5c2dmi2MpQe7G6BeNZfTzKQRxCSW4G4mmGgQQJBANPcGzVNTHShZiHwrQeZ4HkuXBaFT/EoCR1foVu+ScIzwWdFJ4Ga/5GVDNfekx6GqVtbWEOYRMPda9xUiQ4s968CQQC0iiEbXijgb4Uf2WAVg6ut0+IZ0otI824iK/YW/9/w20aVOLpzhcg+z3exJa11yoEzAOd3+Emk+EGU+JSdprh1AkAeqJ+XWxRRXHn3N2kSzIXbg4X4DFrBP1XyVkB3klYCMJMwaqWI7/FEFBnDs4X+yOYBPnrLbWGmJojRXbpnvsIpAkAth+hxjn2gnH9mWNvykRJq9FTBTmfyLfJIi+20PzOq+qICYBwlpkeqD4iQFIB6hGexBTla+DMad8GSYF6ao8FBAkEAxnjJD4Uw4z4/vHa5CJWhCDWs6dqEbotk8N7gqlgR9Rq7OJJfs1dYc5LdnWbgPjzRgTg1WAj5SJHMrp6ycJBlkg==");
			map.put("sign", sign);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/zdbbill/notify.do",map);
	}

}
