package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class DuodebaoNofityTest {
	
	@Test
	public void test(){
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("trade_no", "C1076377816");
		map.put("sign_type", "RSA-S");
		map.put("orginal_money", "100");
		map.put("notify_type", "offline_notify");
		map.put("merchant_code", "1000810135");
		map.put("order_no", "3589160056064000");
		map.put("trade_status", "SUCCESS");
		map.put("sign", "iCi+In/8aYae7Wwps3eYvAPy6vNLDhsnpcCIu7ymxLRxW/SDHXCbTM10u4zFR6Q7qFkishGnU0uiPDWeqgelZgI/ymCTax272FjIggYutDOPTKvSy/kvVjP8bReC8LT0boSDX0/E/pxFmLCX+lNIp1f3t5d8sFiQ/WvBfP8YuAY=");
		map.put("order_amount", "100");
		map.put("interface_version", "V3.3");
		map.put("bank_seq_no", "W17720171013210521239671");
		map.put("order_time", "2017-10-13 21:05:19");
		map.put("notify_id", "8a19711f3041431b98fa634c7dc7d20c");
		map.put("trade_time", "2017-10-13 21:05:21");
		
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/duodebao/notify.do",map);
		
	}

}
