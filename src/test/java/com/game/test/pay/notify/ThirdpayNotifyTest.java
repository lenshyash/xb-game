package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class ThirdpayNotifyTest {
	
	@Test
	public void test(){
		String return_code = "0";
		String totalFee = "999900";
		String channelOrderId = "3559209030486016";
		String orderId = UUID.randomUUID().toString().replaceAll("-", "");
		String timeStamp = System.currentTimeMillis()+"";
		String transactionId = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signSrc = String.format("channelOrderId=%s&key=%s&orderId=%s&timeStamp=%s&totalFee=%s", channelOrderId,"e8f22de1ddbc2f62785cbd4260e399d2",orderId,timeStamp,totalFee);
		
		String sign = MD5.encryption(signSrc);
		
		String url = "http://d.com:8080/game/onlinepay/thirdpay/notify.do?return_code="+return_code+"&totalFee="+totalFee+"&channelOrderId="+channelOrderId+"&orderId="+orderId
				+"&timeStamp="+timeStamp+"&sign="+sign+"&transactionId="+transactionId;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
	
	}

}
