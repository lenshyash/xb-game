package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class PayezeNotifyTest {
	
	@Test
	public void test(){
		String status = "1";
		String customerid = "10918";
		String sdpayno = UUID.randomUUID().toString().replaceAll("-", "");
		String sdorderno = "3623149426755584";
		String total_fee = "12.95";
		String paytype = "wxh5";
		String remark = UUID.randomUUID().toString().replaceAll("-", "");
		
		
		String signSrc = String.format("customerid=%s&status=%s&sdpayno=%s&sdorderno=%s&total_fee=%s&paytype=%s&%s", customerid, status, sdpayno,sdorderno, total_fee, paytype, "2fb5ae028003fa94aa400ac55a137547efb7e9a7");
		
		String sign = MD5.encryption(signSrc);
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("status", status);
		map.put("customerid", customerid);
		map.put("sdpayno", sdpayno);
		map.put("sdorderno", sdorderno);
		map.put("total_fee", total_fee);
		map.put("paytype", paytype);
		map.put("remark", remark);
		map.put("sign", sign);
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/payeze/notify.do", map);
		
		Assert.assertEquals("success", content);
		
	}

}
