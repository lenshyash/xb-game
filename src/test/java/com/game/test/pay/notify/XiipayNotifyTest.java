package com.game.test.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class XiipayNotifyTest {

	@Test
	public void test() {
		String amount = "9999.00";
		String extra = "3566618218743808";
		String order = UUID.randomUUID().toString().replaceAll("-", "");
		String time = System.currentTimeMillis()+"";
		
		String trade = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signsrc = "amount="+amount+"&extra="+extra+"&order="+order+"&time="+time+"&key=2b35e9f8cd26a4fa591d39cb765f1587";
		
		String sign = MD5.encryption(signsrc);
		
		String url = "http://d.com:8080/game/onlinepay/xiipay/notify.do?amount="+amount+"&extra="+extra+"&order="+order+"&time="+time+"&trade="+trade+"&sign="+sign;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
	}

}
