package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.UUID;

import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

import groovy.lang.MetaClassImpl.Index;

public class IfeepayNotifyTest {

	@Test
	public void test() {
		String merchant_no = "144710001674";
		String order_no = "3607260470216704";
		String order_amount = "999.01";
		String original_amount = "999.01";
		String upstream_settle = "1";
		String result = "S";
		String pay_time = DateUtil.getCurrentTime("yyyyMMddHHmmss");
		String trace_id = UUID.randomUUID().toString().replaceAll("-", "");
		String reserve = "Onlinepay";
		String signsrc = String.format("merchant_no=%s&order_no=%s&order_amount=%s&original_amount=%s&upstream_settle=%s&result=%s&pay_time=%s&trace_id=%s&reserve=%s&key=%s", merchant_no, order_no, order_amount, original_amount, upstream_settle, result, pay_time, trace_id, reserve, "8359aaa5-ad06-11e7-9f73-71f8d998ba5e");

		String sign = MD5.encryption(signsrc);

		String url = "http://d.com:8080/game/onlinepay/ifeepay/notify.do?merchant_no=" + merchant_no + "&order_no=" + order_no + "&order_amount=" + order_amount + "&original_amount=" + original_amount + "&upstream_settle=" + upstream_settle + "&result=" + result + "&pay_time=" + pay_time + "&trace_id=" + trace_id + "&reserve=" + reserve + "&sign=" + sign;

		HttpClientUtils.getInstance().sendHttpGet(url);

	}

	@Test
	public void testBangList(){
		String merchant_no = "144710001674";
		String mode="WEBPAY";
		String key = "8359aaa5-ad06-11e7-9f73-71f8d998ba5e";
		String signSrc = "merchant_no="+merchant_no+"&mode="+mode+"&key="+key;
		String sign = MD5.encryption(signSrc);
		
		String url = "http://pay.ifeepay.com/gateway/queryBankList?merchant_no="+merchant_no+"&mode="+mode+"&sign="+sign;
		
		String content = HttpClientUtils.getInstance().sendHttpGet(url);
		
		JSONObject object = JSONObject.parseObject(content);
		JSONArray array = object.getJSONArray("bank_list");
		for (int i =0 ; i <= array.size();i++) {
			JSONObject object2 = array.getJSONObject(i);
			System.out.println("BankInfo."+object2.getString("BANK_CODE"));
		}
		
		
		
	}

}
