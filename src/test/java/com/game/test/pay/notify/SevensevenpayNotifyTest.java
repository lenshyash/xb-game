package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class SevensevenpayNotifyTest {

	@Test
	public void test() {
		String OrderNo = "3653004087592960";
		String OrderAmount = "100";
		String TimeEnd = DateUtil.getCurrentTime("yyyyMMddHHmmss");
		String Msg = UUID.randomUUID().toString();
		
		String signSrc = String.format("Msg=%s&OrderAmount=%s&OrderNo=%s&TimeEnd=%s&key=%s", Msg,OrderAmount,OrderNo,TimeEnd,"A7074647B072592F16E35B372277BD89");
		
		String sign = MD5.encryption(signSrc);
		
		Map<String, String> map = new HashMap<>();
		map.put("OrderNo", OrderNo);
		map.put("OrderAmount", OrderAmount);
		map.put("TimeEnd", TimeEnd);
		map.put("Msg", Msg);
		map.put("sign", sign);

		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/sevensevenpay/notify.do", map);
		Assert.assertEquals("0000", content);
	}

}
