package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class Rujin8NotifyTest {
	
	@Test
	public void test(){
		//sign=5e456814b686af5077551f608622b2fd&body=&=&=&=&=&=&=&=&=&=&=
		SortedMap<String, String> map = new TreeMap<String,String>();
		
		map.put("sign", "5e456814b686af5077551f608622b2fd");
		map.put("body", "OnlinePay");
		map.put("trade_no", "1710251459142106927914151");
		map.put("notify_time", "20171025150401");
		map.put("merchant_no", "1508820114089506");
		map.put("total_fee", "100.00");
		map.put("out_trade_no", "3605787082885120");
		map.put("obtain_fee", "99.96");
		map.put("trade_status", "SUCCESS");
		map.put("payment_type", "wxpay");
		map.put("payment_bank", "");
		map.put("version", "1.0.3");
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/rujin8/notify.do",map);
		
	}

}
