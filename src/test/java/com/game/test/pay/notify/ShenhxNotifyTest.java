package com.game.test.pay.notify;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class ShenhxNotifyTest {

	@Test
	public void test() throws UnsupportedEncodingException{
		SortedMap<String, String> map = new TreeMap<String,String>();
		
		map.put("store_id", "S600003");
		map.put("mch_id", "S60000340460001");
		map.put("pay_type", "03");
		map.put("out_trade_no", "3508414356490240");
		map.put("trans_amt", "999999.00");
		map.put("ret_code", "00");
		
		String signSrc = "";
		
		Set<Entry<String,String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while(iterator.hasNext()){
			Entry<String, String> entry = iterator.next();
			if (!"sign".equals(entry.getKey())) {
				if (!StringUtils.isEmpty(entry.getValue())) {
					signSrc += entry.getKey()+"="+entry.getValue()+"&";
				}
			}
		}
		
		signSrc += "key=f72112c89419f481ce12af7feed9a9d1";
		
		map.put("sign", MD5.encryption(signSrc).toUpperCase());
		
		Map<String, String> paramsmap = new HashMap<String,String>();
		
		paramsmap.put("reqData", URLEncoder.encode(JSON.toJSONString(map), "utf-8"));
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/shenhx/notify.do", paramsmap);
	}
}
