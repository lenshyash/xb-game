package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class Dinpay30NotifyTest {
	
	@Test
	public void test(){
		Map<String, String> map = new HashMap<String,String>();
		map.put("trade_no", "1588061377");
		map.put("sign_type", "RSA-S");
		map.put("notify_type", "offline_notify");
		map.put("merchant_code", "2010711143");
		map.put("order_no", "3577788622276608");
		map.put("trade_status", "SUCCESS");
		map.put("sign", "FVeUleUb4onhXCGWc8UIrHkvZ/bGsRDrJlzafQskhnFl+39mP0ZZQezb2AqB8S1KHU9YfrPucUy6+dbyjG3I6+serPQn1ECcthxsdqFYgIgyBx1fR6WMaPVDPkQ7U466o9+Nrbhjh/7JBJWkgZeAbYD2dIUyI0RMUtYRXYilQQ4=");
		map.put("order_amount", "100");
		map.put("interface_version", "V3.0");
		map.put("bank_seq_no", "105500019681201710051270472767");
		map.put("order_time", "2017-10-05 20:17:42");
		map.put("notify_id", "3dfe9e05dfe345d489e8028bf4625efa");
		map.put("trade_time", "2017-10-05 20:18:20");
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/dinpay30/notify.do", map);
	}

}
