package com.game.test.pay.notify;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class LiyingpayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("mch_id","30140");
		map.put("out_trade_no","3696639107385344");
		map.put("trade_no",UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("trade_type","WEIXIN");
		map.put("trade_state","SUCCESS");
		map.put("total_fee","358");
		map.put("time_end",DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("nonce_str",UUID.randomUUID().toString().replaceAll("-", ""));
		
		String sign = ""; // 签名

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!StringUtils.equals("body", entry.getKey()) && !StringUtils.equals("attach", entry.getKey())) {
				try {
					buffer.append(entry.getKey()).append("=").append(URLEncoder.encode(entry.getValue(),"UTF-8")).append("&");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		String signSrc = buffer.append("key=").append("0064BCFE839F6BD8E9E5D6FA39A38162").toString();

		sign = OnlinepayUtils.MD5(signSrc, "UTF-8").toUpperCase();
		map.put("sign", sign);
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/liyingpay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
	}

}
