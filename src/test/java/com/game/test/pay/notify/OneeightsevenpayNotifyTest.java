package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class OneeightsevenpayNotifyTest {
	
	@Test
	public void test(){
		String orderid = "3617100681332736";
		String opstate = "0";
		String ovalue = "1000.07";
		String sysorderid = UUID.randomUUID().toString().replaceAll("-", "");
		String systime = DateUtil.getCurrentTime();
		String attach = UUID.randomUUID().toString().replaceAll("-", "");
		String msg = "SUCCESS";
		
		String signsrc = String.format("orderid=%s&opstate=%s&ovalue=%s%s", 
				orderid,opstate,ovalue,"fa29a52cac6647b49bafe42d540fa6e3");
		String sign = MD5.encryption(signsrc);
		Map<String, String> map = new HashMap<>();
		map.put("orderid", orderid);
		map.put("opstate", opstate);
		map.put("ovalue", ovalue);
		map.put("sysorderid", sysorderid);
		map.put("systime", systime);
		map.put("attach", attach);
		map.put("msg", msg);
		map.put("sign", sign);
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/oneeightsevenpay/notify.do", map);
		
		System.out.println(content);
		
	}

}
