package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class HaolianpayNotifyTest {

	@Test
	public void test() {
		String status = "1";
		String customerid = "10891";
		String sdpayno = UUID.randomUUID().toString();
		String sdorderno = "3618890024306688";
		String total_fee = "100.45";
		String paytype = "bank";
		String remark = UUID.randomUUID().toString();

		String signSrc = String.format("customerid=%s&status=%s&sdpayno=%s&sdorderno=%s&total_fee=%s&paytype=%s&%s", customerid, status, sdpayno, sdorderno, total_fee, paytype, "76dc6ce542545ad833cd15e74ae26ad8658fc1c4");

		String sign = MD5.encryption(signSrc);

		SortedMap<String, String> map = new TreeMap<>();
		map.put("status", status);
		map.put("customerid", customerid);
		map.put("sdpayno", sdpayno);
		map.put("sdorderno", sdorderno);
		map.put("total_fee", total_fee);
		map.put("paytype", paytype);
		map.put("remark", remark);
		map.put("sign", sign);
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/haolianpay/notify.do", map);
		
		Assert.assertEquals("success", content);
	}

}
