package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class DcpayNotifyTest {
	
	@Test
	public void test(){
		
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("requestId", "3507159757441024");
		map.put("userId", "123");
		map.put("payNo", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("returnCode", "000000");
		map.put("message", "成功");
		map.put("characterSet", "02");
		
		String jsonstring = JSON.toJSONString(map);
		
		HttpClientUtils.getInstance().sendHttpPostJson("http://d.com:8080/game/onlinepay/dcpay/notify.do",jsonstring);
		
	}

}
