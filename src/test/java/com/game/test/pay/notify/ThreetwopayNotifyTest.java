package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class ThreetwopayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("P_UserId", "1002077");
		map.put("P_OrderId", "3613312239175680");
		map.put("P_FaceValue", "99.82");
		map.put("P_ChannelId", "P_ChannelId");
		map.put("P_PayMoney", "99.82");
		map.put("P_ErrCode", "0");
		map.put("P_Price", "99.82");
		map.put("P_Quantity", "1");
		map.put("P_Description", "99.82");
		map.put("P_ErrMsg", "0");
		
		String signSrc = String.format("%s|%s|%s|%s|%s|%s|%s|%s|%s", 
				
				map.get("P_UserId"),map.get("P_OrderId"),"","",map.get("P_FaceValue"),map.get("P_ChannelId"),map.get("P_PayMoney"),map.get("P_ErrCode"),"xrov97es8vasqn8rrbs8fcyxp65xjhc9");
		
		
		map.put("P_PostKey", MD5.encryption(signSrc));
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/threetwopay/notify.do",map);
	}

}
