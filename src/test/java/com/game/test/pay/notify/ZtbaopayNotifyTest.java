package com.game.test.pay.notify;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class ZtbaopayNotifyTest {
	
	@Test
	public void test() throws Exception{
		SortedMap<String, String> map = new TreeMap<String , String>();
		map.put("merchant_code", "123001002003");
		map.put("notify_type", "offline_notify");
		map.put("notify_id", "e722dceae317466bbf9cc5f1254b8b0a");
		map.put("interface_version", "V3.0");
		map.put("order_no", "3494224204498944");
		map.put("order_time", DateUtil.getCurrentTime());
		map.put("order_amount", "23.00");
		map.put("trade_no", "dhuiyuan");
		map.put("trade_time", DateUtil.getCurrentTime());
		map.put("trade_status", "SUCCESS");
		map.put("bank_seq_no", "123001002003");
		
		String signsrc = "";
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			signsrc += entry.getKey()+"="+entry.getValue()+"&";
		}
		
		String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALIgluJzrKhOw/+sKlUZW+GFISjeXCqNz45rhEd4pRhg92ZDwyJxsIWVMUggCJLjSAke2wmVOiYJB/V9rNwlCzal5BGCSD0y8VckUb8LMv5wnNxr3wjrXf6IbZWsgNOwZg1mo+Cji5LCwoKvYvbZNK33Nb9MwbBh1PHUVP8AsfM3AgMBAAECgYEAr6oyAtse39Dlu+OWz9u1X/+BhyNa82Bs20Au8KkK77LY6NJUw0gpVGOgeUeWDP31kYELdDTlZpMrdS9eZLBnj/QofFTx7GSeod+vV13cgA6rc0yzjTp25Dm7Xzihf15R5JiNIFzlSYC2TLz+HcJoprxY6Pf6I/1qBjZuoC67eEECQQDjDhEI7s010aXXYQy3xwC/RUDosnfMARqRCpYFCYmoyMiUZ7+ohIvWkkCcwHx7VNKnXfmF0ezdXNT2TCKfXj6hAkEAyNXFKkCPtbg+GFqUlxlfta1s7FJuC1b8ZyaA1ygqUK5PJUoEKR9UcDg0uCKx4Zofpm46WCHx8w8M0+Abss8a1wJAA5JqFDDli44zxLKjJ5T63wdw4PhFyDDQQS3gdE3VG5GlDiifrEABjyuX1p90leAcvENPNJq71jOqqgFCni02YQJAQ8q09SA54lNA0qOwyJhOEFtsCxGAB9/i70a18uqh7f4IxUOIyADFVeQDF6zOcqK90EYg96Ltsuf/on1hnCgAnQJBANGvRflfL1Xvelv2jb446Gnq83IwQ6WJvO8z7/awfMmDsC88MI2bE0xcWJ2QPZZEVJkgCmwOXc26G+z0eei/z/U=";
		signsrc = signsrc.substring(0, signsrc.length()-1);
		String sign = RSAWithSoftware.signByPrivateKey(signsrc, privateKey);
		
		map.put("sign", sign);
		map.put("sign_type", "RSA-S");
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/ztbaopay/notify.do", map);
		
	}

}
