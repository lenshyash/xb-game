package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class YingyupayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("resultCode", "00");
		map.put("resCode", "00");
		map.put("resultDesc", "成功");
		map.put("resDesc", "成功");
		
		map.put("nonceStr", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("branchId", "171200033744");
		map.put("createTime", System.currentTimeMillis()+"");
		map.put("orderAmt", "12400");
		map.put("orderNo", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("outTradeNo", "3692239602731008");
		map.put("productDesc", "alipay");
		map.put("payType", "20");
		map.put("status", "02");
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}

		buffer.append("key=").append("b446c9501c1a477b95114c72239b25ce");
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		String content = HttpClientUtils.getInstance().sendHttpPostJson("http://b.com:8080/game/onlinepay/yingyupay/notify.do", JSONObject.toJSONString(map));
		
		JSONObject object = JSONObject.parseObject(content);
		
		Assert.assertEquals("00", object.getString("resCode"));
		
		
		
	}

}
