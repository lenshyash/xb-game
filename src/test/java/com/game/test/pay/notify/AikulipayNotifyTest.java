package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class AikulipayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("merId", "3618544878389248");
		map.put("merOrderId", "3618544878389248");
		map.put("txnAmt", "100.55");
		map.put("currency", "CNY");
		map.put("success", "true");
		map.put("code", "0000");
		map.put("message", "success");
		map.put("attach", "uuid");

		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (StringUtils.isNotEmpty(entry.getValue()) && !"signature".equals(entry.getKey()) && !"signMethod".equals(entry.getKey())) {
				buffer.append("&").append(entry.getKey()).append("=").append(entry.getValue());
			}
		}
		
		buffer.append("293763a6d87cdf58c362d9a804382921");
		
		map.put("signature", MD5.encryption(buffer.toString()).toUpperCase());
		
		map.put("signMethod", "MD5");
		
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/aikulipay/notify.do", map);
	}

}
