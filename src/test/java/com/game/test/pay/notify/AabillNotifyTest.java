package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class AabillNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("payKey", "569a1d976615464e824ecb8d3aa96088");
		map.put("orderPrice", "9999.00");
		map.put("outTradeNo", "3597404418148352");
		map.put("productType", "70000201");
		map.put("orderTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("productName", "OnlinePay");
		map.put("tradeStatus", "SUCCESS");
		map.put("successTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("trxNo", System.currentTimeMillis() + "");
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("paySecret=").append("aac3148f576045ad9827bf976a1d4814");
		
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/aabill/notify.do", map);
	}

}
