package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class HangyipayNotifyTest {
	
	@Test
	public void test(){
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("versionId", "001");
		map.put("businessType", "1200");
		map.put("insCode", "");
		map.put("merId", "936370196060000");
		map.put("transDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("transAmount", "12.97");
		map.put("transCurrency", "156");
		map.put("transChanlName", "MOPAY");
		map.put("openBankName", "");
		map.put("orderId", "3624305800235008");
		map.put("payStatus", "00");
		map.put("payMsg", "");
		map.put("pageNotifyUrl", "");
		map.put("backNotifyUrl", "");
		map.put("orderDesc", "");
		map.put("dev", UUID.randomUUID().toString().replaceAll("-", ""));

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		String signSrc = buffer.substring(0, buffer.length()-1)+"7C321922868C61DC";
		
		map.put("signData", MD5.encryption(signSrc));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/hangyipay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
