package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class One2payNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		
		map.put("returnCode", "0");
		map.put("returnMsg", "Success");
		map.put("resultCode", "0");
		map.put("channel", "wxQR");
		map.put("mchId", "000070050000000051");
		map.put("outTradeNo", "3590251262986240");
		map.put("outChannelNo", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("body", "Online Pay");
		map.put("amount", "9999.00");
		map.put("transTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("status", "02");
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=").append("37a9166297cc420296af028c16f1eca3");
		
		map.put("sign", MD5.encryption(buffer.toString()).toUpperCase());
		
		HttpClientUtils.getInstance().sendHttpPostJson("http://d.com:8080/game/onlinepay/one2pay/notify.do", "{\"amount\":10.00,\"body\":\"Online Pay\",\"channel\":\"alipayQR\",\"mchId\":\"000070050000000062\",\"outChannelNo\":\"2017101400101000000463\",\"outTradeNo\":\"3590587855669248\",\"resultCode\":\"0\",\"returnCode\":\"0\",\"sign\":\"CCFFE16EA6AD8D4353BA5EB2B3593592\",\"status\":\"02\",\"transTime\":\"20171014211745\"}");
		
	}

}
