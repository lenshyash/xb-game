package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class FoupangNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("pay_number", "3528321345308672");
		map.put("orderId", "Yf2017083121363221005518Gt");
		map.put("status", "000000");
		map.put("amount", "100");
		map.put("sign", "7c41ed7208af689a0738ed6d24785afd");

		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/foupang/notify.do", map);
	}

}
