package com.game.test.pay.notify;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bouncycastle.asn1.cms.TimeStampedData;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class XjpayNotifyTest {

	@Test
	public void test() {
		
		String json = "{\\x22errcode\\x22:\\x220\\x22,\\x22orderno\\x22:\\x223557614073530368\\x22,\\x22total_fee\\x22:\\x22100\\x22,\\x22attach\\x22:\\x22b5f47515097049d2a0fe5214040d0d54\\x22,\\x22sign\\x22:\\x22977072D11CC138F45265C48E69DB5DEA\\x22}";
		
		HttpClientUtils.getInstance().sendHttpPostJson("http://d.com:8080/game/onlinepay/xjpay/notify.do", json);
		
		

	}
	
	@Test
	public void testAdd(){
		System.out.println(new BigDecimal(200).multiply(new BigDecimal(100)).add(new BigDecimal("1")).toString());
		System.out.println(new BigDecimal(200).add(new BigDecimal("0.01")).toString());
	}
	
	@Test
	public void testmd5(){
		
		Map<String, String> map = new HashMap<String,String>();
		
		map.put("attach", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("errcode", "0");
		map.put("orderno", "3556366100645888");
		map.put("total_fee", "1000");
		
		String signSrc = String.format("%s%s%s%s%s", map.get("attach"),map.get("errcode"),map.get("orderno"),map.get("total_fee"),"3040b6b20cc25f64759a60431c004587");
		
		map.put("sign", MD5.encryption(signSrc));
		
		System.out.println(JSON.toJSONString(map));
	}

}
