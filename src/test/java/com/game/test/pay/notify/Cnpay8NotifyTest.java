package com.game.test.pay.notify;

import java.util.Map.Entry;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class Cnpay8NotifyTest {
	
	@Test
	public void test(){
		String transDate = DateUtil.getCurrentDate();
		String transTime = DateUtil.getCurrentTime();
		String merchno = "";
		String merchName = "";
		String customerno = "";
		String amount = "47.00";
		String traceno = "3655306495723520";
		String payType = "1";
		String orderno = "3655306495723520";
		String channelOrderno = UUID.randomUUID().toString();
		String channelTraceno = UUID.randomUUID().toString();
		String openId = "openId";
		String status = "1";
		String remark = UUID.randomUUID().toString();
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("transDate", transDate);
		map.put("transTime", transTime);
		map.put("merchno", merchno);
		map.put("merchName", merchName);
		map.put("customerno", customerno);
		map.put("amount", amount);
		map.put("traceno", traceno);
		map.put("payType", payType);
		map.put("orderno", orderno);
		map.put("channelOrderno", channelOrderno);
		map.put("channelTraceno", channelTraceno);
		map.put("openId", openId);
		map.put("status", status);
		map.put("remark", remark);
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String,String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("E327549685EBE154B977D9AD272543E4");
		
		map.put("signature", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/cnpay8/notify.do", map);
		
		Assert.assertEquals("success", content);
		
	}

}
