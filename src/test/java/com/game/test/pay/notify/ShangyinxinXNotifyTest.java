package com.game.test.pay.notify;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

public class ShangyinxinXNotifyTest {
	@Test
	public void test() {
		
		Map<String, String> map = new HashMap<String, String>();
		String aesKey = "f5pwnyk3Lr/NYaCbnWndebLIM8N9JJA24X8qj6+w2HruTnuhsbTETceCiViaWtXzvxQlj9QHY308Su6BagNBiFVBudNQHdS7RuXQDXWc+JKuU0HNgHa+lyMVYL5XlujuB+fXm7iI1Ihfwwq/FjYo1R/MuoWgJyp6MsHFI00bw5RT9fHYjqjgluumbVNLtUmjAeSwsmoEtpYYXqdGbRXWUdI6j9Nucd4+oL9u2tfrJPfKOLeqxfqGeH+tU+Yw9Y4xWqqbU1TfKhf0mkzmbi5CqWhUm+lKfNy+3UFsrhl6NgwxqDwaxPbZGR716SerD80FJ5ZqQHV1daLuvu3se08vxA==";
		String finalMap = "kK96IIQQyymHjmIc2Rbw+BNMI2PX88XqeMcdlZiNH8q78yhbGmJFQ4VencvcCxPe4XNhXivHTDzA/dv1dtJ8XjcIkmSrhh7rDAZuKR0VOrY5B/FiwamKSNcPA07SvsHLWwForwkX0Af2uycRWzZro5vowBaFmJ/o8JwdgTBXRpmuhRyaFdL4vIqTcVDOeuih4dslYDI7lFi0k0y7Ux+kOmQWv+G9SDxhI3io1PP9dvvrxkWRjmXTsACjJJzMhZVuTDj5GmctZp1R+BfSSwgy1u7W4i2pIqxmUW6YnVf7jf0wfzxfUnVUSR4ST0Vfg+fMoubikZNCmcn7LjSXFPqYbvCL3TQ451CKK+239dbqJBnGKqaoC1721XXhHSwm66b9jS9F5Io3QzwxRSH+ApE2+w==";
		map.put("encryptKey", aesKey);
		map.put("Content-Type", "application/vnd.airdata-v2.0+json");
		map.put("merchantId", "120143365");
		map.put("requestId", "3696541714319360");
		getHttpsResponseBysendJson("http://b.com:8080/game/onlinepay/shangyinxinX/notify.do", finalMap, map);
	}
	
	/**
	 * 发送HTTPS POST请求
	 * 
	 * @param 要访问的HTTPS地址,POST访问的参数Map对象
	 * @return 返回响应值
	 */
	public Map getHttpsResponseBysendJson(String url, String jsonParams, Map<String, String> headers) {
		String responseContent = null;
		HttpClient httpClient = new DefaultHttpClient();
		// 创建TrustManager
		X509TrustManager xtm = new X509TrustManager() {
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
			
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			}
			
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
		// 这个好像是HOST验证
		X509HostnameVerifier hostnameVerifier = new X509HostnameVerifier() {
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
			
			public void verify(String arg0, SSLSocket arg1) throws IOException {
			}
			
			public void verify(String arg0, String[] arg1, String[] arg2) throws SSLException {
			}
			
			public void verify(String arg0, X509Certificate arg1) throws SSLException {
			}
		};
		try {
			// TLS1.0与SSL3.0基本上没有太大的差别，可粗略理解为TLS是SSL的继承者，但它们使用的是相同的SSLContext
			SSLContext ctx = SSLContext.getInstance("TLS");
			// 使用TrustManager来初始化该上下文，TrustManager只是被SSL的Socket所使用
			ctx.init(null, new TrustManager[] { xtm }, null);
			// 创建SSLSocketFactory
			SSLSocketFactory socketFactory = new SSLSocketFactory(ctx);
			socketFactory.setHostnameVerifier(hostnameVerifier);
			// 通过SchemeRegistry将SSLSocketFactory注册到我们的HttpClient上
			httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", socketFactory, 443));
			HttpPost httpPost = new HttpPost(url);
			
			StringEntity s = new StringEntity(jsonParams, "utf-8");
			s.setContentType("application/json");
			httpPost.setEntity(s);
			
			// set header
			if (null != headers) {
				Set<Entry<String, String>> headerSet = headers.entrySet();
				Iterator<Entry<String, String>> headerIterator = headerSet.iterator();
				while (headerIterator.hasNext()) {
					Entry<String, String> entry = headerIterator.next();
					String name = entry.getKey();
					String value = entry.getValue();
					httpPost.setHeader(name, value);
				}
			}
			HttpResponse response = httpClient.execute(httpPost);
			String content = "";
			HttpEntity entity = response.getEntity();
			try {
				content = EntityUtils.toString(entity);
			} catch (ParseException e2) {
				e2.printStackTrace();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
			Header[] headersRes = response.getHeaders("encryptKey");
			String encryptKey = headersRes[0].getValue();
			Map map = new HashMap();
			map.put("encryptKey", encryptKey);
			map.put("content", content.split(":")[1].substring(1, content.split(":")[1].length()-1));
			return map;
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 关闭连接,释放资源
			httpClient.getConnectionManager().shutdown();
		}
		return null;
	}

}
