package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils.RSA;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class TimesdataNotifyTest {
	
	@Test
	public void test(){
		Map<String, String> head = new HashMap<>();
		head.put("respCd", "0000");
		head.put("respMsg", "成功");
		head.put("reqNo", "3706239950555136");
		head.put("respNo", UUID.randomUUID().toString().replaceAll("-", ""));
		
		Map<String, String> data = new HashMap<>();
		data.put("cCode", "0");
		data.put("tType", "0");
		data.put("allFee", "1200");
		data.put("processFee", "12");
		data.put("lastFee", "987");
		data.put("tStatus", "1");
		data.put("tChannel", "0");
		data.put("tNo", UUID.randomUUID().toString().replaceAll("-", ""));
		data.put("tCode", UUID.randomUUID().toString().replaceAll("-", ""));
		data.put("productInfo", "");
		data.put("tTime", DateUtil.getCurrentTime());
		data.put("tCreateTime", DateUtil.getCurrentTime());
		
		StringBuffer b = new StringBuffer();
		SortedMap<String, String> sortMap = new TreeMap<String, String>();
		if (null != data) {
			for (Map.Entry<String, String> e : data.entrySet()) {
				if ("sign".equals(e.getKey()) || null == e.getValue())
					continue;
				sortMap.put(e.getKey(), e.getValue() + "");
			}
		}
		for (Map.Entry<String, String> e : head.entrySet()) {
			if ("sign".equals(e.getKey()) || null == e.getValue())
				continue;
			sortMap.put(e.getKey(), e.getValue() + "");
		}
		for (Map.Entry<String, String> e : sortMap.entrySet()) {
			b.append(e.getValue());
		}

		String sign = RSA.sign(b.toString(), "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALOINftNn4dclWaBsxZqoW5Ir4Md4z/kR60InL+q4AC5vb4uvYH/uyEm1SXF7WPMCRYmMrssIzcztKK+XfGomYzidREaxjBbvq5j2Rh5M6ZbKnCwarxfLJOGgHHbaCAnl1C0ZhNxeteMT6g0VhEcOgUVvhv3nXpLynvECgys0ztnAgMBAAECgYEAkHFuvRne2BFtQmPnqolwCEMCswNA6w7HfDgJpUvdnbfTKUiBwC34aNvNce6k6D4osWFaP61rR+aZ1rZC70MvBgl3i+s+NKPjCRPYZ6KO3+NLOON11uzeFkyiFivM3ZIH2VMxvAD3k0L4mXryqp3eZ3ZayKVAe7I7macZyhnPxYECQQDgc269PaHegKVJWTqpQCPQU2QEZehGfrSfjyhi1jKDYQ3xi0H23EOqsUj/Z4D5An0HDYgxSvPq9tn0417dpGC/AkEAzMRvCwZwuhuFUmxQB2Jjf3vA0no7mBd2rc5HHKhpFUBX1kKkzIR0wefgQfZX0NSd5tQBRFELWyjKikUB/6+nWQJALXGRqUUSQe+AB6LGSsBOv/4yUOWlKqi6v1eH0MMXhRwga5hLacTc+nFcm0GEjmYflE78FSbczeTe6kMi0PNYFwJACyUZ4iC8m/SJgh3xESXSwoqKySnr+nA0fms+xZmI9qYk4b2I2LTQ4gjr1MtCplQcMAcT9pLuqkyDEZmF7VOzKQJAWXZFKGzBcE+KMQQXoWPkMBu8fh+HrX9Of2ZkinFQWi6atEel7KrcS7LE+wsVCttJXeGSkjFSdCPdfsS8d0ABHQ==", "utf-8", "MD5withRSA");

		head.put("sign", sign);
		

		StringBuffer xmlBuffer = new StringBuffer("<xml><head>");

		Set<Entry<String, String>> set = head.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, String> entry = (Map.Entry<java.lang.String, String>) iterator.next();
			String key = entry.getKey();
			String value = entry.getValue();
			xmlBuffer.append("<").append(key).append(">").append(value).append("</").append(key).append(">");
		}

		xmlBuffer.append("</head><data>");

		Set<Entry<String, String>> dset = data.entrySet();
		Iterator<Entry<String, String>> diterator = dset.iterator();
		while (diterator.hasNext()) {
			Map.Entry<java.lang.String, String> entry = (Map.Entry<java.lang.String, String>) diterator.next();
			String key = entry.getKey();
			String value = entry.getValue();
			xmlBuffer.append("<").append(key).append(">").append(value).append("</").append(key).append(">");
		}

		xmlBuffer.append("</data></xml>");
		
		String content = HttpClientUtils.getInstance().sendHttpPostXml("http://b.com:8080/game/onlinepay/timesdata/notify.do", xmlBuffer.toString());
		
		Assert.assertEquals("0000", content);
		
		
		
	}

}
