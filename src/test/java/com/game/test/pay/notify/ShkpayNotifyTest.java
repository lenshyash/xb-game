package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class ShkpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();

		map.put("orderid", "3512415173740544");
		map.put("opstate", "0");
		map.put("ovalue", "999999.00");
		map.put("sysorderid", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("systime", DateUtil.getCurrentTime());

		String signSrc = String.format("orderid=%s&opstate=%s&ovalue=%s%s", map.get("orderid"), map.get("opstate"), map.get("ovalue"), "af437dfe73d543c8bd155c10aa175b1a");

		String sign = MD5.encryption(signSrc);
		
		map.put("sign", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/shkpay/notify.do", map);
	}

}
