package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class BldpayNotifyTest {
	
	@Test
	public void test(){
		String transDate = "211510144680007";
		String transTime = "211510144680007";
		String merchno = "211510144680007";
		String merchName = "关公";
		String customerno = "211510144680007";
		String amount = "10.00";
		String traceno = "3664200785233920";
		String payType = "1";
		String orderno = UUID.randomUUID().toString().replaceAll("-", "");
		String channelOrderno = UUID.randomUUID().toString().replaceAll("-", "");
		String channelTraceno = UUID.randomUUID().toString().replaceAll("-", "");
		String openId = UUID.randomUUID().toString().replaceAll("-", "");
		String status = "1";

		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("transDate", transDate);
		map.put("transTime", transTime);
		map.put("merchno", merchno);
		map.put("merchName", merchName);
		map.put("customerno", customerno);
		map.put("amount", amount);
		map.put("traceno", traceno);
		map.put("payType", payType);
		map.put("orderno", orderno);
		map.put("channelOrderno", channelOrderno);
		map.put("channelTraceno", channelTraceno);
		map.put("openId", openId);
		map.put("status", status);
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator()	;
		
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (StringUtils.isNoneEmpty(entry.getValue()) && !"signature".equals(entry.getKey()) && !"null".equals(entry.getValue())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		buffer.append("C20981974A74CC0ECA4163C632A7CC73");
		
		map.put("signature", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/bldpay/notify.do", map);
		
		Assert.assertEquals("success", content);

	}

}
