package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils.RSA;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class AnquanfuNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<>();

		map.put("subject", "Onlinepay");
		map.put("body", "Onlinepay|1|11");
		map.put("trade_status", "1");
		map.put("total_amount", "11");
		map.put("sysd_time", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("trade_time", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("trade_no", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("out_trade_no", "3714726832850944");//,,,,
		map.put("notify_time", DateUtil.getCurrentTime("yyyyMMddHHmmss"));

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}

		String signSrc = buffer.substring(0, buffer.length() - 1);

		String sign = RSA.sign(signSrc, "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMcloo5PPrhL+qAGirIrG2T6h+tCfGMo3pztg5Ir3tyZ/WPo0K0v3Xd0+Y/ahzg8bAzzIKtoKSzXY4C8oRTu3SF5tOz4Ojg8sMwxiIrBCQCeRYOVwbrVJj//dBmmmFn+kwzIw0ABi7JLXxR6lkOuCes3/sYbuqIZQta8jOE5x7NHAgMBAAECgYAbMkKO2LeiB45EiGAskMJ++c/SCxRdnlk4aR9MuP0Id2cpW8QTwrOl+q2SPa+1uFqxMgrGHcVxwEBwdlIxP6WnDP5IragJYPXorrMbmI7oUbUhanhbs6OU7+cHgWmy7UsT9YgcmRy065oAfabqOusAOxuc43ZtQ0lzmgqVlvYQ4QJBAObfAR4QreKhZSoqzuj2XNaLVi8yoOD3vSojMrFFrwDH9vb4P9pbaVORq8iXEQ+BbI9DJITwipTZrez1P0s5/J8CQQDc0qt/EZkyb7EJbnyj4so0m3t/yynbfSdf3yKOFmtoBqim1CZjrQ1K5bgbrLWDUyUvSZ5MJ31adTRhlBimFSBZAkEAo4l+CiT4d1uqBIeUT3ML6jGDGCW9zf3O8kVCiE37HAuJhgHtgShVYIwEai+QNzKPip+eA8k78RqM2BgVAJ2PswJBAKonPvRr2BMQE2eEFlCmGU9OG2yOXNN+7Y8nPdYkmmCO+PeL84TMvLhgqGvSug7WS4nTfUGLEEFanKEv34piLwkCQGvpdIKYVh3U4hQMOFCbu7vpIhMMsVYC0hFPCMP1CE7Oj1A5PZw3q6Hio80EH8yXuSYFEc/8/PoVp6DrToX/WrM=", "UTF-8", "SHA256WithRSA");
		map.put("sign", sign);

		String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/anquanfu/notify.do", map);
		Assert.assertEquals("SUCCESS", content);

	}

}
