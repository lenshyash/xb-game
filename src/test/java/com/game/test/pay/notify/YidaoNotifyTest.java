package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;

import com.game.common.onlinepay.DigestUtil;

public class YidaoNotifyTest {
	
	@Test
	public void test(){
		String p1_MerId= "80115";
		String r0_Cmd= "Buy";
		String r1_Code= "1";
		String r2_TrxId= UUID.randomUUID().toString().replaceAll("-", "");
		String r3_Amt= "9998.99";
		String r4_Cur= "RMB";
		String r5_Pid= "Online Pay";
		String r6_Order= "3606081024870400";
		String r7_Uid= "";
		String r8_MP= "Online Pay";
		String r9_BType= "2";
		
		String signSrc = p1_MerId+r0_Cmd+r1_Code+r2_TrxId+r3_Amt+r4_Cur+r5_Pid+r6_Order+r7_Uid+r8_MP+r9_BType;
		
		String hmac = DigestUtil.hmacSign(signSrc, "641850ba108e448d");
		
		Map<String, String> map = new HashMap<String,String>();
		
		map.put("p1_MerId", p1_MerId);
		map.put("r0_Cmd", r0_Cmd);
		map.put("r1_Code", r1_Code);
		map.put("r2_TrxId", r2_TrxId);
		map.put("r3_Amt", r3_Amt);
		map.put("r4_Cur", r4_Cur);
		map.put("r5_Pid", r5_Pid);
		map.put("r6_Order", r6_Order);
		map.put("r7_Uid", r7_Uid);
		map.put("r8_MP", r8_MP);
		map.put("r9_BType", r9_BType);
		map.put("hmac", hmac);
		
		com.game.util.HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/yidaozf/notify.do", map);
		
		
		
	}

}
