package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class NinebaopayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("apiName", "PAY_RESULT_NOTIFY");
		map.put("notifyTime", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("tradeAmt", "9999.00");
		map.put("merchNo", System.currentTimeMillis() + "");
		map.put("merchParam", System.currentTimeMillis() + "");
		map.put("orderNo", "3567689532393472");
		map.put("tradeDate", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("accNo", UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("accDate", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("orderStatus", "1");
		map.put("notifyType", "1");

		String signSrc = String.format("apiName=%s&notifyTime=%s&tradeAmt=%s&merchNo=%s&merchParam=%s&orderNo=%s&tradeDate=%s&accNo=%s&accDate=%s&orderStatus=%s%s", map.get("apiName"), map.get("notifyTime"), map.get("tradeAmt"), map.get("merchNo"), map.get("merchParam"), map.get("orderNo"), map.get("tradeDate"), map.get("accNo"), map.get("accDate"), map.get("orderStatus"), "c82be4d702a2954386a39b5442f22a3e");
		String sign = MD5.encryption(signSrc);
		
		map.put("signMsg", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/ninebaopay/notify.do",map);
		
	}

}
