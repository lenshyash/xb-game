package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class FiveonepayNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("orderState","01");
		map.put("tranSeqId",UUID.randomUUID().toString().replaceAll("-", ""));
		map.put("orderId","3717632234833920");
		map.put("payTime",DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("orderAmt","1100");
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String,String>> set = map.entrySet();
		
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!StringUtils.equals("sign", entry.getKey())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		buffer.append("key=").append("OYbxS2UwTTrQiuoIWCcSfDJKdIgEourZ");
		
		try {
			String sign = RSAWithSoftware.signByPrivateKey(buffer.toString(), "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJPEivn89sH5zZdf17iTf1Sdqsta0Ug8DNm8uecakeTOn6AJqAFVc0Mp3R/eSvaxb5xr5jvYvz9wTEbNqFQXBhA+34TBLOanmi81MTOjgWC/oHOC/h/Tq4uxMFimM7CX2OJ8ZU0+DsO++51ocEB2x1OYUg3l+t/LOfvOSZpV4fUHAgMBAAECgYBKRUNFGyw7mv8G0pluoFQjh4vM4jzGfchHuGAIFIa35lK88Z2L7hMc8vG9jtFG5TAgAuuZsPuPOzZbcArnGRtmUx1HdKAZLi9D96fRZ+Cpv+NQ3zhsHWBhDkJnGnaz7DQaJwR0DcEwNqbQ+KCQvOa21RmcRXbdfDgr8XhbhSMrwQJBAM32z2daP5cUwOS2X25IMFL0DCzmMdodDT+vsWrL20I5mrh0+KenP2uUZoCMUoaXHyBxXi1wn47gjETm2R634x8CQQC3qmvos40+po2AumyhBDReRZ/s+pa7MU7MP8uouVMO3k5c1apG1/FRRAbxdAGa1ADljG4ycC1fhtouy+H4KVkZAkBxW5PPSGaTWoLH3nArnGOsZFGHLLLnXvH5yVMWeliU3GuBdi+c5noLVcQMoC6TkP06t+qji8bltkS9b2i0dXT1AkEAhDwWP09vBUmDHcibR36weaNlVojBF3uTwSi5Z/wuWcki4GIRm7oEC7+PxC4Crcev8ZWfDX+Im3uLASBXzIVwmQJARjFj3k7FLRucSs6Szie8khWW/weGF6Tf5u3gJxd+jTvF7q3nTMhIDXrbWzbRp/Nye0YKZJGGvolIkJc0vnto5A==");
			map.put("sign", sign);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/fiveonepay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
