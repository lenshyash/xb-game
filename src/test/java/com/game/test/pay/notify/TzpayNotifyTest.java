package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class TzpayNotifyTest {
	
	@Test
	public void test(){
		String status="0000";
		String message="SUCCESS";
		String out_parter_id="80026000014";
		String out_partner_order_id="3652528046295040";
		String tz_order_id=UUID.randomUUID().toString();
		String out_transaction_id=UUID.randomUUID().toString();
		String transaction_type="weixin_js";
		String transaction_status="0000";
		String total_fee="100";
		String time_end=DateUtil.getCurrentTime("yyyyMMddHHmmss");
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("status", status);
		map.put("message", message);
		map.put("out_parter_id", out_parter_id);
		map.put("out_partner_order_id", out_partner_order_id);
		map.put("tz_order_id", tz_order_id);
		map.put("out_transaction_id", out_transaction_id);
		map.put("transaction_type", transaction_type);
		map.put("transaction_status", transaction_status);
		map.put("total_fee", total_fee);
		map.put("time_end", time_end);
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=ccbd95d25ce94014813fefe620eefba3");
		
		map.put("sign", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/tzpay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
