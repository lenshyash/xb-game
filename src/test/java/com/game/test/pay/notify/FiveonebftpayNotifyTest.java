package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils.RSA;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class FiveonebftpayNotifyTest {
	
	@Test
	public void test(){
		String outOrderId="3647271887833088";
		String transAmt="9.87";
		String merchantId="2017111500001425";
		String localOrderId=UUID.randomUUID().toString();
		String transTime=DateUtil.getCurrentTime("yyyyMMddHHmmss");
		String respType="R";
		String respCode="00";
		String respMsg="处理成功";
		
		SortedMap<String, String> map = new TreeMap<>();
		map.put("outOrderId", outOrderId);
		map.put("transAmt", transAmt);
		map.put("merchantId", merchantId);
		map.put("localOrderId", localOrderId);
		map.put("transTime", transTime);
		map.put("respType", respType);
		map.put("respCode", respCode);
		map.put("respMsg", respMsg);
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Entry<java.lang.String, java.lang.String> entry = (Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		String signsrc = buffer.substring(0, buffer.length()-1);
		map.put("sign", RSA.sign(signsrc, "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJSKoTblPPSUYC8oeYVSj+pWph+r29DEQ1Xi35S/Ym7zDLmIQU2KdP2RehUhCXGarca+b8uEAJ42iy5fIfFwXQ5O8sS4MWK5ltY/RQwtAmc9cK0Dz/pxp9R+JPJPcE+yA5k9cFcB8S5y3YU9nQeJWvZ1Zr52Wg9lnT+VmjJ4l22vAgMBAAECgYBa6RWN8XMDBaRweEcXj9ZAx9NYYuhftx62++l2skNeQCazjm7ZNDoxzs13DQconlBo6LinSBzQt/Uq5lJx4CEy4TfaAQoDMgflvegkIaNwMki2rvdUOGXwgP2DPb2HEFWVsjyvDGhDuiDgAv6mKpXrgkVqsyAWGnAwM02og5hnsQJBAPcX8Xc6JalHI30BN5rmwxypsLYGKL1FzNzVpWoEh7FHedCjInUEuFhfyItN6hhLzLSeEDNTdfpjYtVzQIksxxUCQQCZ5UwhtFYR4x8byTKft9VdMA9dKD8M61LCJBb4ISPIiiXimzw6XV2xyiWZacyK6FgtgmdctCffKu+iFDUVVdKzAkAd5f3HyXdpJjx7Ebyit62AOGAzhkdDr8B8MDXVwDqTDu0Ee9jdg/ayatSfVowu6G0apc2cCEtMQt5gqZNzhcGJAkBq61/FWuEhdEY8q8A8LyBRqf5Cnp+Pfj71GtXKdlvCDBjeIzPo6M0bpHIS7+pn55y7oYnmYlQBPHdYh+mJdSL5AkEAvCd7hXWfSmS3BKiqLze9lFtq+O19H3DjCgFhM1XhkBnutCI7mvXzH5oyJr8FrZ5LLJA3JrwTZ45/btuKyeWzoQ==", "utf-8"));
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/fiveonebftpay/notify.do", map);
		
		Assert.assertEquals("success", content);
		
	}

}
