package com.game.test.pay.notify;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class CaifubaoNotifyTest {
	
	 @Test
	 public void test() throws Exception{
		 SortedMap<String, String> transdata = new TreeMap<>();
		 transdata.put("result", "2");
		 transdata.put("transtime", System.currentTimeMillis()+"");
		 transdata.put("finishtime", DateUtil.getCurrentDate());
		 transdata.put("amount", "9.07");
		 transdata.put("merchantorder", "3615823279917056");
		 transdata.put("appno", "190908101");
		 transdata.put("currency", "RMB");
		 transdata.put("paytype", "1");
		 transdata.put("customerno", UUID.randomUUID().toString().replaceAll("-", ""));
		 transdata.put("merchantno", "10541");
		 transdata.put("serialno", "1708171357308887556184742");
		 
		 String transStr = URLEncoder.encode(JSONObject.toJSONString(transdata), "utf-8");
		 
		 String sign = privatekeySign(transStr, "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAI3/mZFArXcFM3fGHHx2G09eIpFsQOMXbC/lMVeLCd86AtMJ5OGgzxSBz1Y+h+TCS9J0FqwtEs26tvYKjTlBKk6/a0DmseKE89DIH6zbuICpdCcJ+iMlo7zFkB37oifqm23aBFqDrRr253igUvj/5QTKH72d7q39CQRZ78a8gXRHAgMBAAECgYAfuy9g8xgQUbw3lrDE60udB8cnwg4Z8SD+7vOAwZQ6Kud4q2xXMy0zQvLHLgAGEZkJ9WEfpo7eJExAXQlD8xj13L1wbIrKjquIP7zofnTJBWPgFuarnA+eDPtahzbrth2yC2C3IsR2c5+85R+xJMEVW6RNHl/rkod7t3+vxTWFSQJBAMHNnVI/3bi/wM0mmWXF7UTS8c+t1CCJHGOj5Y5Fx6zSqVyG1haF2fiLGGS/MTS6MBXLs7w0XcHUShG+tACIkSUCQQC7kdY9ATvRW8d8C5VrwGqOTgKtrDbrUQYAvgdDMLqP0ABpX+WFeb1q1zgHwVRhY764CkIWOHzVVFPvTAXwwgH7AkART6qD1Eyb2hUAvjfpjVp6UVVkNaTDGzw78kHpCXaii1axeyHYdTBZMSOGrEVa7l6pM77kStEtJSdQmnY9rfU5AkAbpPvZU+Q4D/nPq3ljPyG1RYQZiqcxeWiUFW3Gllj4tyRIY4zVZkLniz7Dzdec3vGN+F0TH1IustYID8FUYZOfAkAC2TDc9soKY8xzY1C0k2F1vflOSX1OboHwr9NCNnhGGBOPOF9/heCjV3XOXdFF2yoebWNbGsae2pYkykG2b/ZS");
		 
		 Map<String, String> map = new HashMap<>();
		 map.put("sign", URLEncoder.encode(sign, "utf-8"));
		 map.put("signtype", "RSA");
		 map.put("transdata", transStr);
		 
		 String content = HttpClientUtils.getInstance().sendHttpPostJson("http://d.com:8080/game/onlinepay/caifubao/notify.do", JSONObject.toJSONString(map));
		 System.out.println(content);
		 
	 }
	 
		private String privatekeySign(String transdata, String privatekey) {
			String signStr = null;
			try {
				signStr = sign(transdata.getBytes("UTF-8"), privatekey);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return signStr;
		}

		private String sign(byte[] data, String privateKey) throws Exception {
			byte[] keyBytes = Base64.decodeBase64(privateKey);
			PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
			Signature signature = Signature.getInstance("MD5withRSA");
			signature.initSign(privateK);
			signature.update(data);
			return Base64.encodeBase64String(signature.sign());
		}

}
