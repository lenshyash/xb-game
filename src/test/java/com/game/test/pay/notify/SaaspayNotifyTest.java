package com.game.test.pay.notify;

import java.security.MessageDigest;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;

public class SaaspayNotifyTest {

	@Test
	public void test() {
		// =&=&=&=&=&=&=&=&=
		String reCode = "1";
		String trxMerchantNo = "80026000014";
		String trxMerchantOrderno = "3658166879471616";
		String result = "SUCCESS";
		String productNo = "EBANK-JS";
		String memberGoods = "3658166879471616";
		String amount = "1.00";
		String retMes = "218646299041029G";
		String hmac = "e1620e1c1c7eb1a217ca38db0ed3b32e";

		SortedMap<String, String> map = new TreeMap<>();
		map.put("reCode", reCode);
		map.put("trxMerchantNo", trxMerchantNo);
		map.put("trxMerchantOrderno", trxMerchantOrderno);
		map.put("productNo", productNo);
		map.put("result", result);
		map.put("memberGoods", memberGoods);
		map.put("amount", amount);
		map.put("retMes", retMes);
		map.put("hmac", hmac);

		String signsrc = String.format("reCode=%s&trxMerchantNo=%s&trxMerchantOrderno=%s&result=%s&productNo=%s&&memberGoods=%s&amount=%s&key=%s", map.get("reCode"), map.get("trxMerchantNo"), map.get("trxMerchantOrderno"), map.get("result"), map.get("productNo"), map.get("memberGoods"), map.get("amount"), map.get("retMes"), "30527qicAB4o37F9url9EsEN6V22Io5S5b9mG863IC44Rs591vw1kM5M2C39");

		// StringBuffer buffer = new StringBuffer();
		// Set<Entry<String, String>> set = map.entrySet();
		// Iterator<Entry<String, String>> iterator = set.iterator();
		//
		// while (iterator.hasNext()) {
		// Entry<java.lang.String, java.lang.String> entry =
		// (Entry<java.lang.String, java.lang.String>) iterator.next();
		// if (!entry.getKey().equals("hmac") &&
		// !entry.getKey().equals("retMes")) {
		// buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		// }
		// }

		// buffer.append("key=30527qicAB4o37F9url9EsEN6V22Io5S5b9mG863IC44Rs591vw1kM5M2C39");

		// System.out.println(buffer.toString());
		System.out.println(signsrc);

		String sign = MD5.encryption(signsrc);

		System.out.println(sign);

		Assert.assertEquals(true, sign.equals(map.get("hmac")));

	}

}
