package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class BitepayNotifyTest {
	
	@Test
	public void test(){
		String partner_id = "90080091236";
		String input_charset = "UTF-8";
		String notify_type = "async_notify";
		String order_sn = "345676543456787654";
		String order_amount = "10";
		String order_time = "20171225222500";
		String trade_time = "20171225222500";
		String out_trade_no = "345676543456787654";
		String extend_param = "extend_param";
		String order_status = "1";
		String sign_type = "MD5";
		// String sign = "";

		SortedMap<String, String> map = new TreeMap<>();
		map.put("partner_id", partner_id);
		map.put("input_charset", input_charset);
		map.put("notify_type", notify_type);
		map.put("order_sn", order_sn);
		map.put("order_amount", order_amount);
		map.put("order_time", order_time);
		map.put("trade_time", trade_time);
		map.put("out_trade_no", out_trade_no);
		map.put("extend_param", extend_param);
		map.put("order_status", order_status);
		map.put("sign_type", sign_type);

		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator()	;

		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!"sign".equals(entry.getKey())) {
				if (!StringUtils.isEmpty(entry.getValue())) {
					buffer.append(entry.getKey()+"="+entry.getValue()+"&");
				}
			}
		}
		buffer.append("key=2609DF8D75ADE1B91F66AF054C071E84");

		map.put("sign", MD5.encryption(buffer.toString()));

		String content = HttpClientUtils.getInstance().sendHttpPost("http://127.0.0.1:8080/game/onlinepay/bitepay/notify.do", map);

		Assert.assertEquals("SUCCESS", content);
	}
}
