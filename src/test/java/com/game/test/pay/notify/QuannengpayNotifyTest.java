package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class QuannengpayNotifyTest {
	
	@Test
	public void test(){
		String pay_MerchantNo = "20095914";
		String pay_OrderNo = "3696623720286208";
		String pay_Amount = "10.00";
		String pay_Cur = "1234";
		String pay_Status = "100";
		String pay_PayTime = "20171225222500";
		String pay_DealTime = "20171225222500";
		// String sign = "";

		SortedMap<String, String> map = new TreeMap<>();
		map.put("pay_MerchantNo", pay_MerchantNo);
		map.put("pay_OrderNo", pay_OrderNo);
		map.put("pay_Amount", pay_Amount);
		map.put("pay_Cur", pay_Cur);
		map.put("pay_Status", pay_Status);
		map.put("pay_PayTime", pay_PayTime);
		map.put("pay_DealTime", pay_DealTime);

		map.put("sign", MD5.encryption(String.format("%s%s%s%s", pay_MerchantNo, pay_OrderNo, pay_Amount, "8082CFDB267D9190D9879A12149FA84D")));

		String content = HttpClientUtils.getInstance().sendHttpPost("http://127.0.0.1:8080/game/onlinepay/quannengpay/notify.do", map);

		Assert.assertEquals("SUCCESS", content);
	}
}
