package com.game.test.pay.notify;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class XinduiyixueNotifyTest {
	
	@Test
	public void test(){
		//charset=utf-8&mchid=&outorderno=&totalfee=&outtransactionid=null&sign=&transactionid=&status=100
		String status = "100";
		String charset = "utf-8";
		String transactionid = "XDDD15123656493091";
		String outtransactionid = "null";
		String outorderno = "3662326519498752";
		String totalfee = "1000";
		String mchid = "XD-SH-1510643728";
		
		SortedMap<String, String> map = new TreeMap<>();
		
		map.put("status", status);
		map.put("charset", charset);
		map.put("transactionid", transactionid);
		map.put("outtransactionid", outtransactionid);
		map.put("outorderno", outorderno);
		map.put("totalfee", totalfee);
		map.put("mchid", mchid);

		map.put("sign", "CFB77714DD678CA2ACECFFE30CEE48A2");
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/xinduiyixue/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}

}
