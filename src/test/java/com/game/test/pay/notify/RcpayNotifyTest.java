package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class RcpayNotifyTest{

	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		
		map.put("productId", "0104");
		map.put("transId", "10");
		map.put("merNo", "888350048161158");
		map.put("memo", "Onlinepay");
		map.put("orderNo", "3656523329456128");
		map.put("transAmt", "1000");
		map.put("orderDate", "20171130");
		map.put("notifyUrl", "http://pay5.fcjja.top/onlinepay/rcpay/notify.do");
		map.put("respCode", "0000");
		map.put("respDesc", "交易成功");
		map.put("payId", "100038379400");
		map.put("payTime", "20171130111000");
		
		map.put("signature", "D55D8090C7BF490F5165896DE3198846");
		
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/rcpay/notify.do", map);
		
		Assert.assertEquals("SUCCESS", content);
		
	}
}
