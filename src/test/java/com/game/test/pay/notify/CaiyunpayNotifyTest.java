package com.game.test.pay.notify;

import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;
import com.itrus.util.sign.RSAWithSoftware;

public class CaiyunpayNotifyTest {
	
	 @Test
	 public void test() throws Exception{
		 SortedMap<String, String> transdata = new TreeMap<>();
		 transdata.put("merchant_code", "988000000010");
		 transdata.put("notify_type", "offline_notify");
		 transdata.put("notify_id", "e722dceae317466bbf9cc5f1254b8b0a");
		 transdata.put("interface_version", "9.07");
		 transdata.put("sign_type", "3615823279917056");
		 transdata.put("order_no", "3707792976807936");
		 transdata.put("order_time", "2013-11-01 12:34:54");
		 transdata.put("order_amount", "12.01");
		 transdata.put("trade_no", "1000004817");
		 transdata.put("trade_time", "2013-12-01 12:23:34");
		 transdata.put("trade_status", "SUCCESS");
		 
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String, String>> set = transdata.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator
					.next();
			if (!"sign".equals(entry.getKey()) && !"sign_type".equals(entry.getKey()) && StringUtils.isNotEmpty(entry.getValue())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		String bufferStr = buffer.substring(0, buffer.length() - 1);
		String sign = "";
		try {
			sign = RSAWithSoftware.signByPrivateKey(bufferStr, "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAM6WhFUC24s6fIK6qhlQydU+KQ9vzJhnvlTVwcjSgfLW5izTPYzyNNTMlfYplz7lnFFYAGIkKKtRsbxfEAl+70YYLPMMw4PlHRCsAMUDC2nJsEZImoJIh0qEyO2+4BR4SuWjtdl9UoPYwl7zjUQZNtB5PU6Hyncjw7bzHU/bbK15AgMBAAECgYBYdg1eVaXq/XVZiVLTGkwC2uY8LPRgAnR24T1kn/dzI+BWK3I8y24h/STtzqtsDGgeUN+BJtifW5EPB2nXlxoflT2JA8XzBCk4/UYqtNqK6OU3b8K/Aom3UHCQG7TIsixIVaVePW3qwaexMXzQWabWk6k+u+0zugmSE9zWdjT0gQJBAPc1TWoBAvphY+DSxMpNVhBT48uNdOzNZwS4D00uzTQKRZWjt/Mnec+8Qj7Lsi8V4rhfxToS2bI7YMKF6QbxtnECQQDV72OUAmFMRFN9mhnF6txY9sufoc/oXhw4s/ihwlCzEhsWqFvOT32HItJNfcPh6F5BKdmkc/U1DtN/1CmeZjuJAkAqkzSE+C2iH5nLA6G+0bjr0PbffLpRekKuzmBcQ541wzsevXaV2wugmmSyWfSTJWrJpDzcdpilmJJejh+HAaQRAkAwHctzuqVPlf6BPWuWjCBLEhbyLF6qkKAgULoXB7FG1UeqbOtZF5/QCRDrxOGNqcmpNlRxHHOGH3OKSJmjurKhAkEAk0zlkwhiMKGZg2Et2jXJtKi+gtQtzh6EFHdP2z2fJtROuM2d5krf88NoWgn4OoZpP8zOD4b1Nsdh2RxpPLiUeg==");
		} catch (Exception e) {
			e.printStackTrace();
		}
		transdata.put("sign", sign);
		 String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com/game/onlinepay/caiyunpay/notify.do", transdata);
		 System.out.println(content);
		 
	 }
}
