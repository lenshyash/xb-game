package com.game.test.pay.notify;

import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.codec.digest.HmacUtils;
import org.junit.Test;

import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class GmstoneNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("p1_MerId", "2323");
		map.put("r0_Cmd", "Buy");
		map.put("r1_Code", "1");
		map.put("r2_TrxId", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("r3_Amt", "1234.00");
		map.put("r4_Cur", "RMB");
		map.put("r5_Pid", "OnlinePay");
		map.put("r6_Order", "3495783768131584");
		map.put("rb_BankId", "ABC");
		map.put("rp_PayDate", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("ru_Trxtim", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		
		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		String signsrc = "";
		while(iterator.hasNext()){
			Entry<String, String> entry = iterator.next();
			if (!("rb_BankId".equals(entry.getKey()) || "rp_PayDate".equals(entry.getKey()) || "ru_Trxtim".equals(entry.getKey()))) {
				signsrc += entry.getValue();
			}
		}
		
		String sign = HmacUtils.hmacMd5Hex("T8TsAXUd9sOhnx41TvmQJeVlX489NVBp", signsrc);
		
		map.put("hmac", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/gmstone/notify.do", map);
		
	}

}
