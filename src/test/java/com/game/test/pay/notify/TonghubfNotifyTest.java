package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class TonghubfNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("pid", "1001");
		map.put("trade_no", "20160806151343349021");
		map.put("out_trade_no", "3665254733006848");
		map.put("type", "alipay");
		map.put("name", "VIP");
		map.put("money", "67.00");
		map.put("trade_status", "TRADE_SUCCESS");
		map.put("sign_type", "MD5");
		
		StringBuffer buffer = new StringBuffer();
		Set<Entry<String,String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			if (!StringUtils.equals("sign_type", entry.getKey()) && !StringUtils.equals("sign", entry.getKey())) {
				buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
			}
		}
		
		buffer.append("TxhRGR52THwZwrcRjRg2RBZ4h2czGWcz");
		
		map.put("sign", MD5.encryption(buffer.toString()));
		
		StringBuffer b = new StringBuffer();
		Set<Entry<String,String>> s = map.entrySet();
		Iterator<Entry<String, String>> i = s.iterator();
		while (i.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) i.next();
				b.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		String url = "http://d.com:8080/game/onlinepay/tonghubf/notify.do?"+b.toString();

		String content = HttpClientUtils.getInstance().sendHttpGet(url);
		
		Assert.assertEquals("success", content);
		
	}

}
