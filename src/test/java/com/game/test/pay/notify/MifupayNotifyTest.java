package com.game.test.pay.notify;

import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Test;

import com.game.common.onlinepay.OnlinepayUtils;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class MifupayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<>();
		map.put("code", "1000");
		map.put("merchant", "1001");
		map.put("bank", "ALIPAY");
		map.put("status", "110");
		map.put("billno", "3709102708639744");
		map.put("amount", "110.00");
		map.put("pay_time", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("msg", "成功！");
		map.put("variables", "");
		map.put("sign_type", "MD5");

		String signSrc = String.format("amount=%s&bank=%s&billno=%s&code=%s&merchant=%s&msg=%s&pay_time=%s&sign_type=%s&status=%s&key=%s", map.get("amount"), map.get("bank"), map.get("billno"), map.get("code"), map.get("merchant"), map.get("msg"), map.get("pay_time"), map.get("sign_type"), map.get("status"), "d9af6e85749542d69casd196bfg1");

		map.put("sign", OnlinepayUtils.MD5(signSrc, "utf-8").toLowerCase());

		String content = HttpClientUtils.getInstance().sendHttpPost("http://b.com:8080/game/onlinepay/mifupay/notify.do", map);

		Assert.assertEquals("SUCCESS", content);

	}

}
