package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class AtrustpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("versionId", "1.0");
		map.put("transType", "008");
		map.put("asynNotifyUrl", "");
		map.put("synNotifyUrl", "");
		map.put("merId", "");
		map.put("orderAmount", "9999.00");
		map.put("prdOrdNo", "3574650746079232");
		map.put("orderStatus", "01");
		map.put("payId", System.currentTimeMillis() + "");
		map.put("payTime", DateUtil.getCurrentTime("yyyyMMddHHmmss"));
		map.put("signType", "Md5");
		map.put("merParam", "");

		Set<Entry<String, String>> set = map.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			Map.Entry<java.lang.String, java.lang.String> entry = (Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		buffer.append("key=").append("v1xwLCcDMVbB");

		map.put("signData", MD5.encryption(buffer.toString()));
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/atrustpay/notify.do", map);

	}

}
