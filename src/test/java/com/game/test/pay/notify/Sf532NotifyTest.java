package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Assert;
import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class Sf532NotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<String,String>();
		//memberid=&orderid=&transaction_id=&amount=&datetime=&returncode=00&sign=&=
		map.put("memberid", "10073");
		map.put("orderid", "3628441130993664");
		map.put("datetime", "20171110151218");
		map.put("returncode", "00");
		map.put("transaction_id", "20171110150400489751");
		map.put("amount", "100.00");
		map.put("sign","481FAFBE73FA262219963E4A9002F799");
		map.put("attach", "");
		
		String content = HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/sf532/notify.do",map);
		
		Assert.assertEquals("OK", content);
		
	}

}
