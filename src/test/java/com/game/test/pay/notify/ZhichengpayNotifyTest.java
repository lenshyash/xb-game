package com.game.test.pay.notify;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class ZhichengpayNotifyTest {

	@Test
	public void test() {
		SortedMap<String, String> map = new TreeMap<String, String>();
		map.put("apiName", "PAY_RESULT_NOTIFY");
		map.put("notifyTime	", "20170913164058");
		map.put("tradeAmt", "500.00");
		map.put("merchNo", "70000201");
		map.put("merchParam", "");
		map.put("orderNo", "3706608207218688");
		map.put("tradeDate", "20170913");
		map.put("accNo", "12835466");
		map.put("accDate", "20170913");
		map.put("orderStatus", "1");
		map.put("notifyType", "1");
		
		// apiName,notifyTime,tradeAmt,merchNo,merchParam,orderNo,tradeDate,
		// accNo,accDate,orderStatus
		String paramsStr = String.format(
				"apiName=%s&notifyTime=%s&tradeAmt=%s&merchNo=%s&merchParam=%s&orderNo=%s&tradeDate=%s"
						+ "&accNo=%s&accDate=%s&orderStatus=%s",
				map.get("apiName"), map.get("notifyTime"), map.get("tradeAmt"), map.get("merchNo"),
				map.get("merchParam"), map.get("orderNo"), map.get("tradeDate"), map.get("accNo"), map.get("accDate"),
				map.get("orderStatus"));
		String singMsg = "";
		try {
			singMsg = signByMD5(paramsStr,"423757f8c305c00b2b261e8e9db6a64e").toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("singMsg", singMsg);
		
		HttpClientUtils.getInstance().sendHttpPost("http://b.com/game/onlinepay/zhichengpay/notify.do", map);
	}
	public static String Bytes2HexString(byte[] b) {
		StringBuffer ret = new StringBuffer(b.length);
		String hex = "";
		for (int i = 0; i < b.length; i++) {
			hex = Integer.toHexString(b[i] & 0xFF);

			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			ret.append(hex.toUpperCase());
		}
		return ret.toString();
	}
	public static String signByMD5(String sourceData, String key) throws Exception {
		String data = sourceData + key;
		MessageDigest md5 = MessageDigest.getInstance("MD5");
		byte[] sign = md5.digest(data.getBytes("UTF-8"));

		return Bytes2HexString(sign).toUpperCase();
	}
}
