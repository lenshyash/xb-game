package com.game.test.pay.notify;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.DateUtil;
import com.game.util.HttpClientUtils;

public class EshidaiNotifyTest {
	
	@Test
	public void test(){
		Map<String, String> map = new HashMap<String,String>();
		map.put("service", "TRADE.NOTIFY");
		map.put("merId", "TRADE.NOTIFY");
		map.put("tradeNo", "3487201059424256");
		map.put("tradeDate", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("opeNo", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("opeDate", DateUtil.getCurrentDate("yyyyMMdd"));
		map.put("amount", "98.00");
		map.put("status", "1");
		map.put("extra", "OnlinePay");
		map.put("payTime", DateUtil.getCurrentTime());
		
		String signSrc = "";
		
		signSrc = String.format("service=%s&merId=%s&tradeNo=%s&tradeDate=%s&opeNo=%s&opeDate=%s&amount=%s&status=%s&extra=%s&payTime=%s",map.get("service"), map.get("merId"), map.get("tradeNo"), map.get("tradeDate"), map.get("opeNo"), map.get("opeDate"), map.get("amount"), map.get("status"), map.get("extra"),map.get("payTime"));
		signSrc += "7f4a1e2d64e2632ad744668fe7fb19db";
		
		String sign = MD5.encryption(signSrc);
		
		map.put("sign", sign);
		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/eshidai/notify.do",map);
	}

}
