package com.game.test.pay.notify;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.poi.poifs.filesystem.Entry;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class BcfapiNotifyTest {
	
	@Test
	public void test(){
		SortedMap<String, String> map = new TreeMap<>();
		map.put("merchantNo", "820080815695");
		map.put("orderNo", "3617376268617728");
		map.put("orderMoney", "9.55");
		map.put("status", "SUCCESS");
		map.put("message", "SUCCESS");
		map.put("merchantNo", "820080815695");

		Set<java.util.Map.Entry<String, String>> set = map.entrySet();
		Iterator<java.util.Map.Entry<String, String>> iterator = set.iterator();
		StringBuffer buffer = new StringBuffer();
		while (iterator.hasNext()) {
			java.util.Map.Entry<java.lang.String, java.lang.String> entry = (java.util.Map.Entry<java.lang.String, java.lang.String>) iterator.next();
			buffer.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		
		buffer.append("key=a17b9872c9b545689c426064b2553ba8");
		
		
		map.put("sign", MD5.encryption(buffer.toString()));
		
		String content = HttpClientUtils.getInstance().sendHttpPostJson("http://d.com:8080/game/onlinepay/bcfapi/notify.do", JSONObject.toJSONString(map));
		
		System.out.println(content);
		
	}

}
