package com.game.test.util;

/**
 * 
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel2 {

	public static List<List<Map<String, String>>> readExcelWithTitle(String filepath) throws Exception {
		String fileType = filepath.substring(filepath.lastIndexOf(".") + 1, filepath.length());
		InputStream is = null;
		Workbook wb = null;
		try {
			is = new FileInputStream(filepath);

			if (fileType.equals("xls")) {
				wb = new HSSFWorkbook(is);
			} else if (fileType.equals("xlsx")) {
				wb = new XSSFWorkbook(is);
			} else {
				throw new Exception("读取的不是excel文件");
			}

			List<List<Map<String, String>>> result = new ArrayList<List<Map<String, String>>>();// 对应excel文件

			int sheetSize = wb.getNumberOfSheets();
			for (int i = 1; i < sheetSize; i++) {// 遍历sheet页
				if (i == 5) {
					break;
				}
				Sheet sheet = wb.getSheetAt(i);
				List<Map<String, String>> sheetList = new ArrayList<Map<String, String>>();// 对应sheet页

				List<String> titles = new ArrayList<String>();// 放置所有的标题

				int rowSize = sheet.getLastRowNum() + 1;
				for (int j = 0; j < rowSize; j++) {// 遍历行
					Row row = sheet.getRow(j);
					if (row == null) {// 略过空行
						continue;
					}
					int cellSize = row.getLastCellNum();// 行中有多少个单元格，也就是有多少列

					if (j == 0) {// 第一行是标题行
						titles = new ArrayList<String>();
						for (int k = 0; k < cellSize; k++) {
							Cell cell = row.getCell(k);
							titles.add(cell.toString());
						}
					} else {// 其他行是数据行
						Map<String, String> rowMap = new HashMap<String, String>();// 对应一个数据行
						for (int k = 0; k < titles.size(); k++) {

							try {
								if ("Mobile".equalsIgnoreCase(row.getCell(8).toString())) {
									break;
								}
							} catch (Exception e) {

							}

							Cell cell = row.getCell(k);
							String key = titles.get(k);
							String value = null;
							if (cell != null) {
								try {
									DecimalFormat df = new DecimalFormat("0");
									value = df.format(cell.getNumericCellValue());
								} catch (Exception e) {
									value = cell.toString();
								}
							}

							rowMap.put(key, value);
						}
						if (!rowMap.isEmpty()) {
							sheetList.add(rowMap);
						}
					}
				}
				if (!sheetList.isEmpty()) {
					result.add(sheetList);
				}
			}

			return result;
		} catch (FileNotFoundException e) {
			throw e;
		} finally {
			// if (wb != null) {
			// wb.close();
			// }
			if (is != null) {
				is.close();
			}
		}
	}
}