package com.game.lottery.calculator;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Terry.Jiang on 2021-07-09.
 */
public class SSCWinMoneyCalculatorTest {
    SSCWinMoneyCalculator calculator;
    SCWinMoneyCalculator scWinMoneyCalculator;

    @Before
    public void init() {
        calculator = new SSCWinMoneyCalculator();
        scWinMoneyCalculator = new SCWinMoneyCalculator();
    }

    @Test
    public void test1() {
        for (int i = 0; i < 10; i++) {
            List<String> ret = calculator.getCandidateNumbers("FFC", "202107080051", 163l);
            System.err.println(ret.size()+" <> "+ret.get(0) + " <> " + ret.get(ret.size() / 2 +10) + " <> " + ret.get(ret.size() - 1));
        }

        //ret.forEach(System.err::println);
    }

    @Test
    public void test2() {
        /*for (int i = 0; i < 10; i++) {
            List<String> ret = scWinMoneyCalculator.getCandidateNumbers("FFSC", "202107100051", 163l);
            System.err.println(ret.size()+" <> "+ ret.get(0) + " <> " + ret.get(ret.size() / 2) + " <> " + ret.get(ret.size() - 1));
        }*/
        List<String> ret = scWinMoneyCalculator.getCandidateNumbers("FFSC", "202107100051", 163l);
        System.err.println(ret.size());
        ret.forEach(System.err::println);
    }
}