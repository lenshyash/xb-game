package com.game.controller.phone.pay.notify;

import java.util.UUID;

import org.junit.Test;

import com.game.third.util.MD5;
import com.game.util.HttpClientUtils;

public class SixbpayNotifyTest {
	
	@Test
	public void test(){
		String partner = "17075";
		String ordernumber = "3561880072128512";
		String orderstatus = "1";
		String paymoney = "9999.00";
		String sysnumber = UUID.randomUUID().toString().replaceAll("-", "");
		
		String signSrc = String.format("partner=%s&ordernumber=%s&orderstatus=%s&paymoney=%s%s", partner,ordernumber,orderstatus,paymoney,"7eb02320a5576cbe6621debe3ca379c9");
		String sign = MD5.encryption(signSrc);
		
		String url = "http://d.com:8080/game/onlinepay/sixbpay/notify.do?partner="+partner+"&ordernumber="+ordernumber+"&orderstatus="+orderstatus+"&paymoney="+paymoney+"&sysnumber="+sysnumber+"&sign="+sign;
		
		HttpClientUtils.getInstance().sendHttpGet(url);
	}

}
