package com.game.controller.phone.pay.notify;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import com.game.util.HttpClientUtils;

public class YtbaoNotifyTest {
	
	@Test
	public void test() throws UnsupportedEncodingException{
		SortedMap<String, String> map = new TreeMap<String,String>();
		map.put("amount", "1.00");
		map.put("merchName", URLDecoder.decode("%B1%B1%BE%A9%B9%E2%C8%F0%B1%F9%E2%F9%BF%C6%BC%BC%D3%D0%CF%DE%B9%AB%CB%BE", "GBK"));
		map.put("merchno", "900130245820001");
		map.put("orderno", "32171121100020122071");
		map.put("payType", "8");
		map.put("signature", "859AC2D7C38AC4D29868346AFD22D2D7");
		map.put("status", "1");
		map.put("traceno", "3644258793146368");
		map.put("transDate", "2017-11-21");
		map.put("transTime", "19:14:39");

		
		HttpClientUtils.getInstance().sendHttpPost("http://d.com:8080/game/onlinepay/ytbao/notify.do", map);
	}


}
